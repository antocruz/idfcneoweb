package com.neo.aggregator.serviceimpl;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.neo.aggregator.dto.*;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.neo.aggregator.bank.equitas.AddDocumentMessageBody;
import com.neo.aggregator.bank.equitas.DigiCustLeadwitheKYCMessageBody;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.LeadUpdateMessageBody;
import com.neo.aggregator.bank.equitas.MessageBody;
import com.neo.aggregator.bank.equitas.MessageHeader;
import com.neo.aggregator.bank.equitas.UpdateDigiDocumentMessageBody;
import com.neo.aggregator.bank.equitas.dto.KycRequestDto;
import com.neo.aggregator.bank.equitas.dto.KycResponseDto;
import com.neo.aggregator.bank.equitas.request.AddDocumentReqDto;
import com.neo.aggregator.bank.equitas.request.CreateDigiCustLeadwitheKYCReqDto;
import com.neo.aggregator.bank.equitas.request.CreateDigiCustomerByLeadReqDto;
import com.neo.aggregator.bank.equitas.request.UpdateDigiDocumentDetailsReqDto;
import com.neo.aggregator.bank.equitas.request.UpdateLeadReqDto;
import com.neo.aggregator.bank.equitas.response.AddDocumentResDto;
import com.neo.aggregator.bank.equitas.response.AddDocumentResDto.Body.AddDocResp;
import com.neo.aggregator.bank.equitas.response.CreateDigiCustLeadwitheKYCResDto;
import com.neo.aggregator.bank.equitas.response.CreateDigiCustomerByLeadResDto;
import com.neo.aggregator.bank.equitas.response.UpdateDigiDocumentResDto;
import com.neo.aggregator.bank.equitas.response.UpdateLeadResDto;
import com.neo.aggregator.bank.equitas.response.UpdateLeadResDto.Body.AddressDetailRes;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dao.AddDocumentDetailsDao;
import com.neo.aggregator.dao.AddressDetailsDao;
import com.neo.aggregator.dao.CustomerDetailsDao;
import com.neo.aggregator.dao.DefaultDataDao;
import com.neo.aggregator.dao.LeadDetailsDao;
import com.neo.aggregator.dao.UpdateDocumentDetailsDao;
import com.neo.aggregator.dto.v2.AddressDto;
import com.neo.aggregator.dto.v2.CommunicationDto;
import com.neo.aggregator.dto.v2.KYCDto;
import com.neo.aggregator.dto.v2.KitInfoDto;
import com.neo.aggregator.dto.v2.SpecialDatesDto;
import com.neo.aggregator.dto.yesbank.MerchantOBReq;
import com.neo.aggregator.dto.yesbank.MerchantOBRes;
import com.neo.aggregator.dto.yesbank.NeoPayRequest;
import com.neo.aggregator.dto.yesbank.NeoPayResponse;
import com.neo.aggregator.dto.yesbank.ValidateVpaReq;
import com.neo.aggregator.dto.yesbank.ValidateVpaRes;
import com.neo.aggregator.enums.AddressCategory;
import com.neo.aggregator.enums.CardCategory;
import com.neo.aggregator.enums.EmploymentType;
import com.neo.aggregator.model.AddDocumentDetails;
import com.neo.aggregator.model.AddressDetails;
import com.neo.aggregator.model.CustomerDetails;
import com.neo.aggregator.model.DefaultData;
import com.neo.aggregator.model.LeadDetails;
import com.neo.aggregator.model.UpdateDocumentDetails;
import com.neo.aggregator.service.AggregatorService;
import com.neo.aggregator.utility.PdfGenerator;
import com.neo.aggregator.utility.Utils;
import com.neo.aggregator.validator.RequestValidator;

@Service("equitasServiceImpl")
public class EquitasEbsServiceImpl implements AggregatorService{

	private static final Logger logger = LoggerFactory.getLogger(EquitasEbsServiceImpl.class);
	
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private LeadDetailsDao leadDetailsDao;

	@Autowired
	private AddDocumentDetailsDao addDocumentDetailsDao;

	@Autowired
	private AddressDetailsDao addressDetailsDao;

	@Autowired
	private CustomerDetailsDao customerDetailsDao;

	@Autowired
	private DefaultDataDao defaultDataDao;
	
	@Autowired
	private EquitasCommonService equitasCommonService;
	
	@Autowired
	private DozerBeanMapper dozerBeanMapper;
	
	@Autowired
	private RequestValidator requestValidator;
	
	@Autowired
    private PdfGenerator pGen;
	
	@Autowired
	private UpdateDocumentDetailsDao updateDocumentDetailsDao;

	@Value("${kyc.m2p.eq.url}")
	private String url;
	@Value("${kyc.m2p.eq.leadurl}")
	private String leadUrl;
	@Value("${kyc.m2p.eq.updateleadurl}")
	private String updateLeadUrl;
	@Value("${kyc.m2p.eq.updatedigidocument}")
	private String updateDigiDocument;
	@Value("${kyc.m2p.eq.adddocument}")
	private String addDocument;
	@Value("${kyc.m2p.eq.leadcreationwithekycbio}")
	private String leadCreationWithEkycBio;

	@Override
	public RegistrationResponseDto retailCifCreation(RegistrationRequestV2Dto request) throws NeoException {
		return null;
	}

	@Override
	public AccountCreationResponseDto savingsAccountCreation(AccountCreationRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public BalanceCheckResponseDto fetchAccountBalance(BalanceCheckRequest request) throws NeoException {
		return null;
	}

	@Override
	public FetchAccountStatemetResponse fetchAccountStatement(FetchAccountStatementRequest request)
			throws NeoException {
		return null;
	}

	@Override
	public AccountCreationResponseDto termDepositCreation(AccountCreationRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public AadhaarOtpResponse generateOtpAadhaar(AadhaarOtpRequest request) throws NeoException {
		return null;
	}

	@Override
	public AadhaarOtpResponse validateOtpAadhaar(AadhaarOtpRequest request) throws NeoException {
		return null;
	}

	@Override
	public PanValidationResponse validatePan(PanValidationRequest request) throws NeoException {
		return null;
	}

	@Override
	public RegistrationResponseDto register(RegistrationRequestV2Dto request) throws NeoException {
		return null;
	}

	@Override
	public RegistrationResponseDto addProduct(RegistrationRequestV2Dto request) throws NeoException {
		return null;
	}

	@Override
	public FundTransferResponseDto fundTransfer(FundTransferRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public FundTransferResponseDto paymentStatusCheck(FundTransferRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public AccountModificationResponseDto savingsAccountModification(AccountModificationRequestDto request)
			throws NeoException {
		return null;
	}

	@Override
	public AccountClosureResponseDto tdAccountClosure(AccountClosureRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public AccountInquiryResponseDto tdAccountInquiry(AccountInquiryRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public KYCBioResponseDto bioEKycOneTimeToken(RegistrationRequestV2Dto request, String eKycRefnum)
			throws NeoException {
		return null;
	}

	@Override
	public KYCBioResponseDto getDemographicData(KYCBioRequestDto request, String kycType) throws NeoException {
		return null;
	}

	@Override
	public TDAcctCloseTrailResponseDto tdTrailAccountClosure(TdAccountTrailCloseRequestDto request)
			throws NeoException {
		return null;
	}

	@Override
	public AccountInquiryResponseDto savingsAccountInquiry(AccountInquiryRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public KYCBioResponseDto aadharXmlInvoke(RegistrationRequestV2Dto request) throws NeoException {
		return null;
	}

	@Override
	public KYCBioResponseDto validateAadhaarXml(AadhaarOtpRequest request) throws NeoException {
		return null;
	}

	@Override
	public KYCBioResponseDto eKycBioValidate(KYCBioRequestDto request) throws NeoException {
		KYCBioResponseDto response = new KYCBioResponseDto();
		try {
			if (request != null) {
				if(request.getConsent() == null || !request.getConsent().equalsIgnoreCase("YES")) {
					throw new NeoException("Service Unavailable", "Service Unavailable", null, null,
							"User consent: NO");
				}
				KycResponseDto kycResponse = null;
				KycRequestDto kycRequest = requestValidator.validateBioRequest(request);
				if (kycRequest != null) {
					fetchDefaultData(kycRequest);
					requestValidator.validateEqRequest(kycRequest);
					kycResponse = leadCreateWithEkycBio(kycRequest);
					if (kycResponse.getRslt().equalsIgnoreCase("OK")) {
						kycResponse = updateLeadRegistration(kycRequest);
						if (kycResponse.getRslt().equalsIgnoreCase("OK")) {
							kycResponse = customerRegistration(kycRequest);
							if (kycResponse.getRslt().equalsIgnoreCase("OK")) {
								kycResponse = addDocument(kycRequest);
								if (kycResponse.getRslt().equalsIgnoreCase("OK")) {
									kycRequest.getRegistrationRequestDtoV2().setCustId(kycRequest.getCustId());
									kycResponse.setEntityId(kycRequest.getRegistrationRequestDtoV2().getEntityId());
									kycResponse.setCustLeadId(kycRequest.getCustLeadId());
									kycResponse.setCustId(kycRequest.getCustId());
									kycResponse.setAuthCode(kycRequest.getRegistrationRequestDtoV2().getAuthCode());
									kycResponse.setName(kycRequest.getRegistrationRequestDtoV2().getName());
									kycResponse.setDob(kycRequest.getRegistrationRequestDtoV2().getDob());
									kycResponse.setAdd(kycRequest.getAdd());
									kycResponse.setTitle(kycRequest.getRegistrationRequestDtoV2().getTitle());
									AddressDto addDto = new AddressDto();
									for (AddressDto address : kycRequest.getRegistrationRequestDtoV2()
											.getAddressInfo()) {
										addDto.setAddress1(address.getAddress1());
										addDto.setAddress2(address.getAddress2());
										addDto.setAddress3(address.getAddress3());
										addDto.setCity(
												Objects.toString(kycRequest.getRegistrationRequestDtoV2().getCity(),
														kycRequest.getCity()));
										addDto.setState(
												Objects.toString(kycRequest.getRegistrationRequestDtoV2().getState(),
														kycRequest.getState()));
										addDto.setCountry(
												Objects.toString(kycRequest.getRegistrationRequestDtoV2().getCountry(),
														kycRequest.getCountry()));
										addDto.setPincode(address.getPincode());
									}
									kycResponse.setAddressDto(addDto);
									kycResponse.setRrn(kycRequest.getRegistrationRequestDtoV2().getRrn());
									kycResponse.setPhoto(kycRequest.getRegistrationRequestDtoV2().getPhoto());

									RegistrationRequestDtoV2 rr = createResponse(kycRequest);
									kycResponse.setRegistrationRequestDtoV2(rr);

									if (kycRequest.getRegistrationRequestDtoV2().getCustId() != null) {
										kycResponse.setKycStatus("COMPLETED");
									} else {
										kycResponse.setKycStatus("PENDING");
									}

									for (KYCDto kyc :request.getRegistrationRequestDtoV2().getKycInfo()) {
		                            	if(kyc.getDocumentType().equalsIgnoreCase("PAN")) {
		                            		 pGen.createPdf(kycResponse, kycRequest, "accountOpeningForm");
		                            	}
		                            	else if(kyc.getDocumentType().equalsIgnoreCase("Form 60")) {
		                            		pGen.createPdf(kycResponse, kycRequest, "accountOpeningForm");
		                            		pGen.createPdf(kycResponse, kycRequest, "form60");
		                            	}
		                            } 
									
									response = requestValidator.validateBioResponse(kycResponse);

								}
							}
						}
					}
					
					if (!kycResponse.getRslt().equalsIgnoreCase("OK")) {
						response.setResult(kycResponse.getRslt());
					}
				}
			}

		} catch (Exception e) {
			logger.info("Error in creating ucic/ registration with biometric data: {}", e.getMessage());
			throw new NeoException("Service Unavailable", "Service Unavailable", null, null,
					"Ekyc Biometric Validation Failed");
		}
		return response;

	}
	
	public KycRequestDto fetchDefaultData(KycRequestDto request) {
		DefaultData dd = defaultDataDao.findById(1L).orElse(null);
		request.setAppId(dd.getAppId());
		request.setAssociatedWith(dd.getAssociatedWith());
		request.setIgnoreProbableMatch(dd.getIgnoreProbableMatch());
		request.setEntityType(dd.getEntityType());
		request.setFromMADP(dd.getFromMADP());
		request.setDeDupChkReqByCustCount(dd.getDeDupChkReqByCustCount());
		request.setIsAadhar(dd.getIsAadhar());
		request.setMappedToAccountLead(dd.getMappedToAccountLead());
		request.setPurposeOfCreation(dd.getPurposeOfCreation());
		request.setIsCurrentAddressSameAsPermanent(dd.getIsCurrentAddressSameAsPermanent());
		request.setIsPhtRequired(dd.getIsPhotoRequired());
		request.setBranchId(dd.getBranchId());
		request.setAuthUserId(dd.getAuthUserId());
		request.setPreferredLanguage(dd.getPreferredLanguage());
		request.setCityOfBirth(dd.getCityOfBirth());
		request.setCountryOfBirth(dd.getCountryOfBirth());
		request.setTaxResident(dd.getTaxResident());
		request.setUsrTkn(dd.getUsrTkn());
		request.setUsrNm(dd.getUsrNm());
		request.setUsrPwd(dd.getUsrPwd());
		request.setNationality(dd.getNationality());
		request.setUsrId(dd.getUsrId());
		request.setAuthenticationToken(dd.getAuthenticationToken());
		request.setAuthToken(dd.getAuthenticationToken());
		request.setEntityFlagType(dd.getEntityFlagType());
		request.setSourceBranch(dd.getSourceBranch());
		request.setHomeBranch(dd.getHomeBranch());
		request.setMcc(dd.getMcc());
		request.setPosEntryMode(dd.getPosEntryMode());
		request.setPosCode(dd.getPosCode());
		request.setCaId(dd.getCaId());
		request.setCaTid(dd.getCaTid());
		request.setCaTa(dd.getCaTa());
		request.setPoliticallyExposedPerson(dd.getPoliticallyExposedPerson());
		logger.info("Fetched default data :: {}",  Utils.convertModelToStringWithMaskedAadhar(request));
		return request;

	}
	
	public KycResponseDto leadCreateWithEkycBio(KycRequestDto request) throws NeoException {

		CreateDigiCustLeadwitheKYCReqDto createDigiCustLeadwitheKYCReqDto = null;
		KycResponseDto res = new KycResponseDto();

		if (request != null) {
			LeadDetails leadCreateBio = new LeadDetails();

			try {
				LeadMessageHeader leadMessageHeader = equitasCommonService.buildLeadCreationMessageHeader(request);

				DigiCustLeadwitheKYCMessageBody digiCustLeadwitheKYCMessageBody = equitasCommonService
						.buildDigiCustLeadwitheKYCMessageBody(request);

				createDigiCustLeadwitheKYCReqDto = CreateDigiCustLeadwitheKYCReqDto.builder()
						.digiCustLeadwitheKYCMessageBody(digiCustLeadwitheKYCMessageBody)
						.leadMessageHeader(leadMessageHeader).build();

				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Include.NON_NULL);
				mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
				mapper.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);
				logger.info("Request from Server :: {}", Utils.convertModelToStringWithMaskedAadhar(createDigiCustLeadwitheKYCReqDto));
				String responseBody = postForEntityWithKycHeaders(
						mapper.writeValueAsString(createDigiCustLeadwitheKYCReqDto), leadCreationWithEkycBio,
						HttpMethod.POST);

				CreateDigiCustLeadwitheKYCResDto ress = mapper.readValue(responseBody,
						CreateDigiCustLeadwitheKYCResDto.class);
				logger.info("Response from Server :: {}" + Utils.convertModelToStringWithMaskedAadhar(ress));

				if (ress != null) {
					dozerBeanMapper.map(ress.getMsgBdy(), CreateDigiCustLeadwitheKYCResDto.class);
					if (ress.getMsgHdr().getError() != null) {
						logger.info("Error Message");
						if (ress.getMsgHdr().getError().size() > 1) {
							if (ress.getMsgHdr().getError().get(1).getRsn()
									.contains("Duplicate found")) {
								res.setRslt(ress.getMsgHdr().getError().get(0).getCd() + " "
										+ ress.getMsgHdr().getError().get(1).getRsn() + " "
										+ ress.getMsgHdr().getError().get(0).getRsn() + " ," + " UCIC: "
										+ ress.getMsgBdy().getDedupIndividualCustomerarray().get(0).getUcic()
										+ " Mobile: "
										+ ress.getMsgBdy().getDedupIndividualCustomerarray().get(0).getMobile()
										+ " Pan: " + ress.getMsgBdy().getDedupIndividualCustomerarray().get(0).getPan()
										+ " Aadhar: "
										+ ress.getMsgBdy().getDedupIndividualCustomerarray().get(0).getAadhar());
							}else {
								res.setRslt(ress.getMsgHdr().getError().get(0).getCd() + " "
										+ ress.getMsgHdr().getError().get(1).getRsn() + " "
										+ ress.getMsgHdr().getError().get(0).getRsn());
							}

							return res;
						} else {
							res.setRslt(ress.getMsgHdr().getError().get(0).getCd() + " "
									+ ress.getMsgHdr().getError().get(0).getRsn());
							return res;
						}

					}
					if (ress.getMsgHdr().getRslt().equalsIgnoreCase("OK")) {
						res.setCustLeadId(ress.getMsgBdy().getLeadDetails().getCustomerLeadID());
						res.setEKYCXMLStringRepPayload(ress.getMsgBdy().getKycDetails().geteKYCXMLStringRepPayload());
						res.setRslt(ress.getMsgHdr().getRslt());
						request.setCustLeadId(ress.getMsgBdy().getLeadDetails().getCustomerLeadID());
						request.setEkycxmlreqPayload(ress.getMsgBdy().getKycDetails().geteKYCXMLStringRepPayload());
						equitasCommonService.fetchFromAadhar(
								ress.getMsgBdy().getKycDetails().geteKYCXMLStringRepPayload(), request);

						leadCreateBio.setEntityId(request.getRegistrationRequestDtoV2().getEntityId());
						leadCreateBio.setCnvId(leadMessageHeader.getCnvId());
						leadCreateBio.setCustLeadId(ress.getMsgBdy().getLeadDetails().getCustomerLeadID());
						leadCreateBio.setTimestamp(leadMessageHeader.getTimestamp());
						leadDetailsDao.save(leadCreateBio);
					}
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else {
			res = null;
			return res;
		}
		return res;
	}
	
	public KycResponseDto updateLeadRegistration(KycRequestDto request) throws NeoException {
		UpdateLeadReqDto updateLeadReqDto = null;
		KycResponseDto res = new KycResponseDto();

		if (request != null) {

			AddressDetails addDetails = new AddressDetails();
			try {
				LeadMessageHeader leadMessageHeader = equitasCommonService.buildLeadCreationMessageHeader(request);

				LeadUpdateMessageBody updateLeadMessageBody = equitasCommonService.buildUpdateLeadMessageBody(request);

				updateLeadReqDto = UpdateLeadReqDto.builder().leadUpdateMessageBody(updateLeadMessageBody)
						.leadMessageHeader(leadMessageHeader).build();

				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Include.NON_NULL);
				mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
				mapper.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);

				String responseBody = postForEntity(mapper.writeValueAsString(updateLeadReqDto), updateLeadUrl,
						HttpMethod.POST);

				UpdateLeadResDto ress = mapper.readValue(responseBody, UpdateLeadResDto.class);

				if (ress != null) {
					if (ress.getMsgHdr().getError() != null) {
						logger.info("Error Message");
						if (ress.getMsgHdr().getError().size()>1) {
							res.setRslt(ress.getMsgHdr().getError().get(0).getCd() + " " + ress.getMsgHdr().getError().get(1).getRsn() + " " + ress.getMsgHdr().getError().get(0).getRsn());
							return res;
						}
						else {
							res.setRslt(ress.getMsgHdr().getError().get(0).getCd() + " " + ress.getMsgHdr().getError().get(0).getRsn());
							return res;
						}
					}
					if (ress.getMsgHdr().getRslt().equalsIgnoreCase("OK")) {

						res.setCustLeadId(ress.getMsgBdy().getCustomerLeadID());
						res.setAddress(ress.getMsgBdy().getAddressDetailRes());
						res.setRslt(ress.getMsgHdr().getRslt());

						AddressDetailRes[] address = res.getAddress();
						for (int i = 0; i < ress.getMsgBdy().getAddressDetailRes().length; i++) {
							addDetails.setEntityId(request.getRegistrationRequestDtoV2().getEntityId());
							addDetails.setCnvId(leadMessageHeader.getCnvId());
							addDetails.setTimestamp(leadMessageHeader.getTimestamp());
							addDetails.setCustLeadId(ress.getMsgBdy().getCustomerLeadID());
							addDetails.setAddressId(address[i].getAddressID());
							addDetails.setAddressType(address[i].getAddressType());
							addressDetailsDao.save(addDetails);
						}
					}
				}

			} catch (NeoException e) {
				throw e;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else {
			res = null;
			return res;
		}
		return res;
	}
	
	public KycResponseDto customerRegistration(KycRequestDto request) throws NeoException {

		CreateDigiCustomerByLeadReqDto requestDto = null;
		CreateDigiCustomerByLeadResDto ress = null;
		KycResponseDto res = new KycResponseDto();

		if (request != null) {
			CustomerDetails createCustomer = new CustomerDetails();

			try {
				MessageHeader messageHeader = equitasCommonService.buildMessageHeader(request);

				MessageBody messageBody = equitasCommonService.buildMessageBody(request);

				requestDto = CreateDigiCustomerByLeadReqDto.builder().messageBody(messageBody)
						.messageHeader(messageHeader).build();

				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Include.NON_NULL);
				mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
				mapper.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);
				String responseBody = postForEntity(mapper.writeValueAsString(requestDto), url, HttpMethod.POST);
				ress = mapper.readValue(responseBody, CreateDigiCustomerByLeadResDto.class);

				if (ress != null) {
					if (ress.getHeader().getError() != null) {
						logger.info("Error Message");
						if (ress.getHeader().getError().size()>1) {
							res.setRslt(ress.getHeader().getError().get(0).getCd() + " " + ress.getHeader().getError().get(1).getRsn() + " " + ress.getHeader().getError().get(0).getRsn());
							return res;
						}
						else {
							res.setRslt(ress.getHeader().getError().get(0).getCd() + " " + ress.getHeader().getError().get(0).getRsn());
							return res;
						}
					}
					if (ress.getHeader().getRslt().equalsIgnoreCase("OK")) {
						res.setCustId(ress.getBody().getCustID());
						res.setRslt(ress.getHeader().getRslt());
						request.setCustId(ress.getBody().getCustID());

						createCustomer.setEntityId(request.getRegistrationRequestDtoV2().getEntityId());
						createCustomer.setCnvId(messageHeader.getCnvId());
						createCustomer.setCustLeadId(messageBody.getCustLeadId());
						createCustomer.setTimestamp(messageHeader.getTimestamp());
						createCustomer.setCustLeadId(ress.getBody().getCustID());
						customerDetailsDao.save(createCustomer);

					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else {
			res = null;
			return res;
		}
		return res;
	}
	
	public KycResponseDto addDocument(KycRequestDto request) throws NeoException {

		AddDocumentReqDto addDocumentReqDto = null;
		KycResponseDto res = new KycResponseDto();

		if (request != null) {
			AddDocumentDetails addDocDetails = new AddDocumentDetails();
			try {
				LeadMessageHeader leadMessageHeader = equitasCommonService.buildLeadCreationMessageHeader(request);

				AddDocumentMessageBody addDocumentMessageBody = equitasCommonService
						.buildAddDocumentMessageBody(request);

				addDocumentReqDto = AddDocumentReqDto.builder().addDocumentMessageBody(addDocumentMessageBody)
						.leadMessageHeader(leadMessageHeader).build();

				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Include.NON_NULL);
				mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
				mapper.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);

				String responseBody = postForEntity(mapper.writeValueAsString(addDocumentReqDto), addDocument,
						HttpMethod.POST);

				AddDocumentResDto ress = mapper.readValue(responseBody, AddDocumentResDto.class);

				if (ress != null) {
					if (ress.getMsgHdr().getError() != null) {
						logger.info("Error Message");
						if (ress.getMsgHdr().getError().size()>1) {
							res.setRslt(ress.getMsgHdr().getError().get(0).getCd() + " " + ress.getMsgHdr().getError().get(1).getRsn() + " " + ress.getMsgHdr().getError().get(0).getRsn());
							return res;
						}
						else {
							res.setRslt(ress.getMsgHdr().getError().get(0).getCd() + " " + ress.getMsgHdr().getError().get(0).getRsn());
							return res;
						}
					}
					if (ress.getMsgHdr().getRslt().equalsIgnoreCase("OK")) {
						for (AddDocResp addDocRes : ress.getMsgBdy().getAddDocResp()) {
							request.setDmsDocumentId(addDocRes.getDocIndx());
							request.setDocumentNo(addDocRes.getDocNm());
						}
						res.setRslt(ress.getMsgHdr().getRslt());
						addDocDetails.setCnvId(leadMessageHeader.getCnvId());
						addDocDetails.setTimestamp(leadMessageHeader.getTimestamp());
						addDocDetails.setDocSbCtgry(addDocumentMessageBody.getAddDocRq().get(0).getDocSbCtgry());
						addDocDetails.setDocNm(ress.getMsgBdy().getAddDocResp().get(0).getDocNm());
						addDocDetails.setCustLeadId(String
								.valueOf(addDocumentMessageBody.getAddDocRq().get(0).getDocRefId().get(0).getId()));
						addDocDetails.setDocIndx(ress.getMsgBdy().getAddDocResp().get(0).getDocIndx());
						addDocumentDetailsDao.save(addDocDetails);
					}
				}

			} catch (NeoException e) {
				throw e;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else {
			res = null;
			return res;
		}
		return res;

	}
	
	public KycResponseDto updateDigiDocument(KycRequestDto request) throws NeoException {

		UpdateDigiDocumentDetailsReqDto updateDigiDocumentDetailsReqDto = null;
		KycResponseDto res = new KycResponseDto();

		if (request != null) {
			UpdateDocumentDetails updateDocDetails = new UpdateDocumentDetails();
			try {
				LeadMessageHeader leadMessageHeader = equitasCommonService.buildLeadCreationMessageHeader(request);

				UpdateDigiDocumentMessageBody updateDigiDocumentMessageBody = equitasCommonService
						.buildDigiDocumentMessageBody(request);

				updateDigiDocumentDetailsReqDto = UpdateDigiDocumentDetailsReqDto.builder()
						.updateDigiDocumentMessageBody(updateDigiDocumentMessageBody)
						.leadMessageHeader(leadMessageHeader).build();

				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Include.NON_NULL);
				mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
				mapper.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);

				String responseBody = postForEntity(mapper.writeValueAsString(updateDigiDocumentDetailsReqDto),
						updateDigiDocument, HttpMethod.POST);

				UpdateDigiDocumentResDto ress = mapper.readValue(responseBody, UpdateDigiDocumentResDto.class);

				if (ress != null) {
					if (ress.getMsgHdr().getError() != null) {
						logger.info("Error Message");
						if (ress.getMsgHdr().getError().size()>1) {
							res.setRslt(ress.getMsgHdr().getError().get(0).getCd() + " " + ress.getMsgHdr().getError().get(1).getRsn() + " " + ress.getMsgHdr().getError().get(0).getRsn());
							return res;
						}
						else {
							res.setRslt(ress.getMsgHdr().getError().get(0).getCd() + " " + ress.getMsgHdr().getError().get(0).getRsn());
							return res;
						}
					}
					if (ress.getMsgHdr().getRslt().equalsIgnoreCase("OK")) {

						res.setIsInsert(ress.getMsgBdy().getIsInsert());
						res.setDmsDocIDs(ress.getMsgBdy().getDmsDocIDs());
						res.setIsFirstInserted(ress.getMsgBdy().getIsFirstInserted());
						res.setId(ress.getMsgBdy().getId());
						res.setProceedToNLTable(ress.getMsgBdy().getProceedToNLTable());
						res.setRslt(ress.getMsgHdr().getRslt());

						updateDocDetails.setCnvId(leadMessageHeader.getCnvId());
						updateDocDetails.setTimestamp(leadMessageHeader.getTimestamp());
						updateDocDetails.setIsInsert(ress.getMsgBdy().getIsInsert());
						updateDocDetails.setIsFirstInserted(ress.getMsgBdy().getIsFirstInserted());
						updateDocDetails.setProceedToNLTable(ress.getMsgBdy().getProceedToNLTable());
						updateDocDetails.setDmsDocumentID(ress.getMsgBdy().getDocuments().get(0).getDmsDocumentID());
						updateDocDetails.setDocumentID(ress.getMsgBdy().getDocuments().get(0).getDocumentID());
						updateDocDetails.setDocumentNo(ress.getMsgBdy().getDocuments().get(0).getDocumentNo());
						updateDocDetails.setDocumentType(ress.getMsgBdy().getDocuments().get(0).getDocumentType());
						updateDocDetails
								.setMappedCustomerLead(ress.getMsgBdy().getDocuments().get(0).getMappedCustomerLead());
						updateDocumentDetailsDao.save(updateDocDetails);
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else {
			res = null;
			return res;
		}
		return res;

	}
	
	public RegistrationRequestDtoV2 createResponse(KycRequestDto request) {
		RegistrationRequestDtoV2 rr = new RegistrationRequestDtoV2();
		rr.setEntityId(request.getRegistrationRequestDtoV2().getEntityId());
		rr.setEntityType(request.getRegistrationRequestDtoV2().getEntityType());
		rr.setBusinessType(request.getRegistrationRequestDtoV2().getBusinessType());
		rr.setBusinessId(request.getRegistrationRequestDtoV2().getBusinessId());
		rr.setIsNRICustomer(request.getRegistrationRequestDtoV2().getIsNRICustomer());
		rr.setIsMinor(request.getRegistrationRequestDtoV2().getIsMinor());
		rr.setIsDependant(request.getRegistrationRequestDtoV2().getIsDependant());
		rr.setMaritalStatus(request.getRegistrationRequestDtoV2().getMaritalStatus());
		rr.setEmploymentIndustry(request.getRegistrationRequestDtoV2().getEmploymentIndustry());
		if (EmploymentType.SALARIED.name().equals(request.getRegistrationRequestDtoV2().getEmploymentType().toString())) {
			rr.setEmploymentType(EmploymentType.EMPLOYED);
		} else if (EmploymentType.HOUSEWIFE.name().equals(request.getRegistrationRequestDtoV2().getEmploymentType().toString())) {
			rr.setEmploymentType(EmploymentType.HOUSEWORK);
		} else {
			rr.setEmploymentType(request.getRegistrationRequestDtoV2().getEmploymentType());
		}
		rr.setMotherMaidenName(request.getMotherMaidenName());
		rr.setFatherName(request.getFatherName());
		rr.setGrossAnnualIncome(request.getGrossAnnualIncome());
		rr.setEstimatedAgriculturalIncome(request.getEstimatedAgriculturalIncome());
		rr.setEstimatedNonAgriculturalIncome(request.getEstimatedNonAgriculturalIncome());
		rr.setFirstName(request.getRegistrationRequestDtoV2().getFirstName());
		rr.setLastName(request.getRegistrationRequestDtoV2().getLastName());
		rr.setGender(request.getRegistrationRequestDtoV2().getGender());
		rr.setTitle(request.getRegistrationRequestDtoV2().getTitle());

		if(request.getRegistrationRequestDtoV2().getKitInfo()!=null) {
			List<KitInfoDto> kitInfoList = new ArrayList<>();
			KitInfoDto kitInfo = new KitInfoDto();
			String catagory = null;
			int i=0;
			for (KitInfoDto kit : request.getRegistrationRequestDtoV2().getKitInfo()) {
				kitInfo.setKitNo(kit.getKitNo());
				kitInfo.setCardType(kit.getCardType());
				catagory = request.getRegistrationRequestDtoV2().getKitInfo().get(i).getCardCategory().toString();
				if (catagory.equalsIgnoreCase(CardCategory.PREPAID.toString())) {
					kitInfo.setCardCategory(CardCategory.PREPAID);
				}else if (catagory.equalsIgnoreCase(CardCategory.FOREX.toString())) {
					kitInfo.setCardCategory(CardCategory.FOREX);
				}else if (catagory.equalsIgnoreCase(CardCategory.CREDIT.toString())) {
					kitInfo.setCardCategory(CardCategory.CREDIT);
				}else if (catagory.equalsIgnoreCase(CardCategory.DEBIT.toString())) {
					kitInfo.setCardCategory(CardCategory.DEBIT);
				}else if (catagory.equalsIgnoreCase(CardCategory.MEAL.toString())) {
					kitInfo.setCardCategory(CardCategory.MEAL);
				}else if (catagory.equalsIgnoreCase(CardCategory.FUEL.toString())) {
					kitInfo.setCardCategory(CardCategory.FUEL);
				}else if (catagory.equalsIgnoreCase(CardCategory.GIFT.toString())) {
					kitInfo.setCardCategory(CardCategory.GIFT);
				}
				kitInfo.setCardRegStatus(kit.getCardRegStatus());
				kitInfo.setExpDate(kit.getExpDate());
				kitInfoList.add(kitInfo);
				i++;
			}
			rr.setKitInfo(kitInfoList);
		}
		
		if(request.getRegistrationRequestDtoV2().getCommunicationInfo()!=null) {
			List<CommunicationDto> commList = new ArrayList<>();
			CommunicationDto comm = new CommunicationDto();
			for (CommunicationDto cDto : request.getRegistrationRequestDtoV2().getCommunicationInfo()) {
				comm.setContactNo(cDto.getContactNo());
				comm.setNotification(cDto.getNotification());
				comm.setEmailId(cDto.getEmailId());
				commList.add(comm);
			}
			rr.setCommunicationInfo(commList);
			
		}
		
		if(request.getRegistrationRequestDtoV2().getKycInfo()!=null) {
			List<KYCDto> kycList = new ArrayList<>();
			KYCDto kyc = new KYCDto();
			for (KYCDto kycDto : request.getRegistrationRequestDtoV2().getKycInfo()) {
				kyc.setKycRefNo(kycDto.getKycRefNo());
				kyc.setDocumentType(kycDto.getDocumentType());
				kyc.setDocumentNo(kycDto.getDocumentNo());
				kyc.setDocumentExpiry(kycDto.getDocumentExpiry());
				kycList.add(kyc);
			}
			rr.setKycInfo(kycList);
		}
		
		if(request.getRegistrationRequestDtoV2().getAddressInfo()!=null) {
			List<AddressDto> addList = new ArrayList<>();
			AddressDto addDto = new AddressDto();
			for (AddressDto address : request.getRegistrationRequestDtoV2().getAddressInfo()) {
				addDto.setAddressCategory(AddressCategory.COMMUNICATION);
				addDto.setAddress1(address.getAddress1());
				addDto.setAddress2(address.getAddress2());
				addDto.setAddress3(address.getAddress3());
				addDto.setCity(Objects.toString(request.getRegistrationRequestDtoV2().getCity(), request.getCity()));
				addDto.setState(Objects.toString(request.getRegistrationRequestDtoV2().getState(), request.getState()));
				addDto.setCountry(Objects.toString(request.getRegistrationRequestDtoV2().getCountry(), request.getCountry()));
				addDto.setPincode(address.getPincode());

				addList.add(addDto);
			}
			rr.setAddressInfo(addList);
		}
		
		if(request.getRegistrationRequestDtoV2().getDateInfo()!=null) {
			List<SpecialDatesDto> spDateList = new ArrayList<>();
			SpecialDatesDto spDto = new SpecialDatesDto();

			for (SpecialDatesDto sDate : request.getRegistrationRequestDtoV2().getDateInfo()) {
				spDto.setDateType(sDate.getDateType());
				spDto.setDate(sDate.getDate());

				spDateList.add(spDto);
			}
			rr.setDateInfo(spDateList);
		}
		
		return rr;
	}
	
	public String postForEntityWithKycHeaders(String request, String url, HttpMethod method) {

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
			headers.add(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.name());
			headers.add("Content-Type", "application/json");
			headers.add("bc_id", "821");
			HttpEntity<String> entity = new HttpEntity<>(request, headers);
			ResponseEntity<String> response = restTemplate.exchange(url, method, entity, String.class,
					new Object[] { url });

			return response.getBody();
		} catch (Exception e) {
			logger.info("Post request error: ", e);
		}
		return null;
	}
	
	public String postForEntity(String request, String url, HttpMethod method) {

		try {
			logger.info("Request from Server :: {}", request);
			HttpHeaders headers = new HttpHeaders();
			headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
			headers.add(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.name());

			if (url.equals(updateDigiDocument)) {
				headers.add("Content-Type", "application/json");
			}

			HttpEntity<String> entity = new HttpEntity<>(request, headers);
			ResponseEntity<String> response = restTemplate.exchange(url, method, entity, String.class,
					new Object[] { url });
			logger.info("Response from Server :: {}", response.getBody());

			return response.getBody();
		} catch (Exception e) {
			logger.info("Post request error: ", e);
		}
		return null;
	}
	
	public KycResponseDto addAndUpdateDoc(String doc, String fileName, String entityId) throws NeoException {

		LeadDetails data = leadDetailsDao.findByLatestEntityId(entityId);
		DefaultData dd = defaultDataDao.findById(1L).orElse(null);
		KycResponseDto res = new KycResponseDto();
		KycRequestDto request = new KycRequestDto();
		fetchDefaultData(request);
		request.setCustLeadId(data.getCustLeadId());
		request.setUsrId(request.getUsrId());
		request.setBsPyld(doc);
		request.setAuthToken(dd.getAuthenticationToken());
		if (fileName.equalsIgnoreCase("accountOpeningForm")) {
			request.setDocSbCtgry("ACCOUNT_OPENING_FORM");
			request.setDocTypCd(276);
			request.setDocSbCtgryCd(204);
		} else if (fileName.equalsIgnoreCase("form60")) {
			request.setDocSbCtgry("FORM_60_61");
			request.setDocTypCd(187);
			request.setDocSbCtgryCd(13);
		}

		addDocument(request);
		if (request.getDmsDocumentId() != null) {
			updateDigiDocument(request);
			res.setRslt("OK");
		}

		return res;
	}

	@Override
	public FundTransferResponseDto fundTransferWithOtp(FundTransferRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AccountInquiryResponseDto fetchAccountDetails(AccountInquiryRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CustomerDedupCheckResponseDto dedupCheck(RegistrationRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterAccountDiscovery(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterAcctBalInquiry(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterOtpCreation(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterOtpVerification(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterDebitOrchestration(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public BeneficiaryManagementResDto beneficiaryAddition(BeneficiaryManagementReqDto request) throws NeoException {
		return null;
	}

	@Override
	public FundTransferResponseDto fundTransferValidateOtp(FundTransferRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NeoPayResponse upiPay(NeoPayRequest request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ValidateVpaRes validateVpa(ValidateVpaReq request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MerchantOBRes onboardSubMerchant(MerchantOBReq request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}
}
