package com.neo.aggregator.serviceimpl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.bank.icici.core.IciciCoreExceptionDto;
import com.neo.aggregator.bank.idfc.Utils.IdfcConstants;
import com.neo.aggregator.bank.idfc.request.IdfcFundTransferRequestDto;
import com.neo.aggregator.bank.idfc.request.MDMSearchByFieldsRequestDto;
import com.neo.aggregator.bank.idfc.request.MDMSearchGstIndvCorpRequestDto;
import com.neo.aggregator.bank.idfc.request.PosidexGenericDedupeRequestDto;
import com.neo.aggregator.bank.idfc.response.IdfcFundTransferResponseDto;
import com.neo.aggregator.bank.idfc.response.MDMSearchByFieldsSPResponseDto;
import com.neo.aggregator.bank.idfc.response.MDMSearchByGSTCropResponseDto;
import com.neo.aggregator.bank.idfc.response.PosidexGenericDedupeResponseDto;
import com.neo.aggregator.dto.*;
import com.neo.aggregator.dto.yesbank.*;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.service.AggregatorService;
import com.neo.aggregator.service.IdfcCbsEsbService;
import com.neo.aggregator.utility.FieldMandateCheckUtil;
import com.neo.aggregator.utility.RequiredFieldException;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;

@Service("idfcCsbEbsServiceImpl")
public class IdfcCsbEbsServiceImpl implements AggregatorService, IdfcCbsEsbService {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${neo.keystore.password}")
	private String keyStorePassword;

	@Value("${neo.keystore.path}")
	private Resource keyStoreFile;

	private static final Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(IdfcCsbEbsServiceImpl.class);

	@Override
	public RegistrationResponseDto retailCifCreation(RegistrationRequestV2Dto request) throws NeoException {
		return null;
	}

	@Override
	public AccountCreationResponseDto savingsAccountCreation(AccountCreationRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public BalanceCheckResponseDto fetchAccountBalance(BalanceCheckRequest request) throws NeoException {
		return null;
	}

	@Override
	public FetchAccountStatemetResponse fetchAccountStatement(FetchAccountStatementRequest request)
			throws NeoException {
		return null;
	}

	@Override
	public AccountCreationResponseDto termDepositCreation(AccountCreationRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public AadhaarOtpResponse generateOtpAadhaar(AadhaarOtpRequest request) throws NeoException {
		return null;
	}

	@Override
	public AadhaarOtpResponse validateOtpAadhaar(AadhaarOtpRequest request) throws NeoException {
		return null;
	}

	@Override
	public PanValidationResponse validatePan(PanValidationRequest request) throws NeoException {
		return null;
	}

	@Override
	public RegistrationResponseDto register(RegistrationRequestV2Dto request) throws NeoException {
		return null;
	}

	@Override
	public RegistrationResponseDto addProduct(RegistrationRequestV2Dto request) throws NeoException {
		return null;
	}

	@Override
	public FundTransferResponseDto fundTransfer(FundTransferRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public FundTransferResponseDto paymentStatusCheck(FundTransferRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public AccountModificationResponseDto savingsAccountModification(AccountModificationRequestDto request)
			throws NeoException {
		return null;
	}

	@Override
	public AccountClosureResponseDto tdAccountClosure(AccountClosureRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public AccountInquiryResponseDto tdAccountInquiry(AccountInquiryRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public KYCBioResponseDto bioEKycOneTimeToken(RegistrationRequestV2Dto request, String eKycRefnum)
			throws NeoException {
		return null;
	}

	@Override
	public KYCBioResponseDto eKycBioValidate(KYCBioRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public KYCBioResponseDto getDemographicData(KYCBioRequestDto request, String kycType) throws NeoException {
		return null;
	}

	@Override
	public TDAcctCloseTrailResponseDto tdTrailAccountClosure(TdAccountTrailCloseRequestDto request)
			throws NeoException {
		return null;
	}

	@Override
	public AccountInquiryResponseDto savingsAccountInquiry(AccountInquiryRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public FundTransferResponseDto fundTransferWithOtp(FundTransferRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public FundTransferResponseDto fundTransferValidateOtp(FundTransferRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public KYCBioResponseDto aadharXmlInvoke(RegistrationRequestV2Dto request) throws NeoException {
		return null;
	}

	@Override
	public KYCBioResponseDto validateAadhaarXml(AadhaarOtpRequest request) throws NeoException {
		return null;
	}

	@Override
	public AccountInquiryResponseDto fetchAccountDetails(AccountInquiryRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public CustomerDedupCheckResponseDto dedupCheck(RegistrationRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterAccountDiscovery(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterAcctBalInquiry(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterOtpCreation(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterOtpVerification(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterDebitOrchestration(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public BeneficiaryManagementResDto beneficiaryAddition(BeneficiaryManagementReqDto request) throws NeoException {
		return null;
	}

	@Override
	public NeoPayResponse upiPay(NeoPayRequest request) throws NeoException {
		return null;
	}

	@Override
	public ValidateVpaRes validateVpa(ValidateVpaReq request) throws NeoException {
		return null;
	}

	@Override
	public MerchantOBRes onboardSubMerchant(MerchantOBReq request) throws NeoException {
		return null;
	}

	/**
	 * this is for idfc service call for fund transfer
	 */
	@Override
	public IdfcFundTransferResponseDto idfcFundTransferReq(IdfcFundTransferRequestDto idfcFundTransferRequestDto)
			throws NeoException {

		IdfcFundTransferRequestDto.From from = IdfcFundTransferRequestDto.From.builder().build();
		from.setId("INO89898OOP");

		IdfcFundTransferRequestDto.Nm nm = IdfcFundTransferRequestDto.Nm.builder().build();
		nm.setBranchNm("ALANDUR");
		nm.setTellerNum("9080987890");

		IdfcFundTransferRequestDto.HDProp hdProp = IdfcFundTransferRequestDto.HDProp.builder().build();
		hdProp.setNm(nm);

		IdfcFundTransferRequestDto.HeaderFields headerFields = IdfcFundTransferRequestDto.HeaderFields.builder()
				.build();
		headerFields.setCnvId("4556546");
		headerFields.setMsgId("4556546");
		headerFields.setTimestamp("2020-03-09T17:15:52.096+05:30");
		headerFields.setHdProp(hdProp);

		IdfcFundTransferRequestDto.MessageHeader messageHeader = IdfcFundTransferRequestDto.MessageHeader.builder()
				.build();
		messageHeader.setFrm(from);
		messageHeader.setHeaderFields(headerFields);

		IdfcFundTransferRequestDto.MessageBody messageBody = IdfcFundTransferRequestDto.MessageBody.builder().build();
		messageBody.setTxnId("sdfdsf");
		messageBody.setDbtrAcctId("87569487695876");
		messageBody.setCdtrAcctId("098594864598605");
		messageBody.setDbtrNm("8605");
		messageBody.setAmt("865");
		messageBody.setCcy("INR");
		messageBody.setTxnTp("sdfdioioiu");
		messageBody.setPmtDesc("cvcdsf");
		messageBody.setFinInstnId("IFSC0001");
		messageBody.setBnfcryNm("9789097890");
		messageBody.setBnfcryAddr("dfdsfdscc");
		messageBody.setEmlId("dfs@gmail.com");
		messageBody.setMobNb("+91-9876098789");
		messageBody.setSenderInfo("Raju");
		messageBody.setReceiverInfo("Ram");

		IdfcFundTransferRequestDto idfcFundTransferRequestDto1 = IdfcFundTransferRequestDto.builder()
				.messageBody(messageBody).messageHeader(messageHeader).build();
		
		//	to check the mandatory fields 
		try {
	        if (FieldMandateCheckUtil.validateForNulls(messageBody)) {
	            // Do something that you want to	           
	            logger.info("Validations Successful");
	        }
	    } catch (RequiredFieldException | ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {	       
	        e.printStackTrace();
	    }

		try {
			String body = new ObjectMapper().writeValueAsString(idfcFundTransferRequestDto1);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			String responseBody = postForEntityWithHeaders(body,
					IdfcConstants.BASE_URL+IdfcConstants.IDFC_FUND_TRANSFER, HttpMethod.POST,
					headers, true);
			logger.debug(responseBody);
			IdfcFundTransferResponseDto idfcResponseDto = new ObjectMapper()
					.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
					.readValue(responseBody, IdfcFundTransferResponseDto.class);
			return idfcResponseDto;
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			return null;
		}
	}

	private String postForEntityWithHeaders(String request, String url, HttpMethod method, HttpHeaders headers,
			Boolean logRequired) {
		try {
			if (logRequired.equals(true)) {
				logger.info("Request for Bank Server :: " + request);
			}

			HttpEntity<String> entity = new HttpEntity<>(request, headers);

			ResponseEntity<String> response;
			try {
				response = restTemplate.exchange(url, method, entity, String.class, url);
			} catch (HttpStatusCodeException e) {
				logger.info("Http Status Error code :: " + e.getStatusCode());
				response = ResponseEntity.status(e.getRawStatusCode()).headers(e.getResponseHeaders())
						.body(e.getResponseBodyAsString());
			}

			if (logRequired.equals(true)) {
				logger.info("Response from Bank Server :: " + response.getBody());
			}

			return response.getBody();
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
		}
		return null;
	}

	private RestTemplate sslConfig(String alias, String tlsVersion) throws KeyStoreException, NoSuchAlgorithmException,
			CertificateException, IOException, KeyManagementException, UnrecoverableKeyException {

		KeyStore keyStore = KeyStore.getInstance("JKS");
		keyStore.load(keyStoreFile.getInputStream(), keyStorePassword.toCharArray());

		try {
			keyStoreFile.getInputStream().close();
		} catch (IOException e) {
			logger.info("IOException KeyStore File :: " + e.getMessage());
		}

		SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
				new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy())
						.loadKeyMaterial(keyStore, keyStorePassword.toCharArray(), (aliases, socket) -> alias).build(),
				new String[] { tlsVersion }, null, NoopHostnameVerifier.INSTANCE);

		HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();

		ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
		return new RestTemplate(requestFactory);
	}

	@Override
	public MDMSearchByFieldsSPResponseDto mdmSearchByFieldsRes(MDMSearchByFieldsRequestDto mdmSearchByFieldsRequestDto)
			throws NeoException {
		// TODO Auto-generated method stub
		try {
			String body = new ObjectMapper().writeValueAsString(mdmSearchByFieldsRequestDto);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			String responseBody = postForEntityWithHeaders(body,
					"https://esbmuat.idfcfirstbank.com:31297/searchByFieldsSPReq", HttpMethod.POST, headers, true);
			logger.debug(responseBody);
			MDMSearchByFieldsSPResponseDto mdmSearchByFieldsSPResponseDto = new ObjectMapper()
					.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
					.readValue(responseBody, MDMSearchByFieldsSPResponseDto.class);
			return mdmSearchByFieldsSPResponseDto;
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			return null;
		}

	}

	@Override
	public MDMSearchByGSTCropResponseDto mdmSearchGstIndvCorpRes(
			MDMSearchGstIndvCorpRequestDto mdmSearchGstIndvCorpRequestDto) throws NeoException {
		// TODO Auto-generated method stub
		try {
			String body = new ObjectMapper().writeValueAsString(mdmSearchGstIndvCorpRequestDto);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			String responseBody = postForEntityWithHeaders(body,
					"https://esbmuat.idfcfirstbank.com:31297/searchGSTIndvCorpReq", HttpMethod.POST, headers, true);
			logger.debug(responseBody);
			MDMSearchByGSTCropResponseDto mdmSearchGstIndvCorpRes = new ObjectMapper()
					.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
					.readValue(responseBody, MDMSearchByGSTCropResponseDto.class);
			return mdmSearchGstIndvCorpRes;
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			return null;
		}
	}

	@Override
	public PosidexGenericDedupeResponseDto posidexGenericDedupeRes(
			PosidexGenericDedupeRequestDto posidexGenericDedupeRequestDto) throws NeoException {
		// TODO Auto-generated method stub
		try {
			String body = new ObjectMapper().writeValueAsString(posidexGenericDedupeRequestDto);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			String responseBody = postForEntityWithHeaders(body,
					"https://esbmuat.idfcfirstbank.com:31297/CheckGenericDedupeReq", HttpMethod.POST, headers, true);
			logger.debug(responseBody);
			PosidexGenericDedupeResponseDto posidexGenericDedupeRes = new ObjectMapper()
					.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
					.readValue(responseBody, PosidexGenericDedupeResponseDto.class);
			return posidexGenericDedupeRes;
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			return null;
		}
	}

}
