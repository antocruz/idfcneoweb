package com.neo.aggregator.serviceimpl;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neo.aggregator.constant.ProductConstants;
import com.neo.aggregator.dao.NeoAccountCreationReqResDao;
import com.neo.aggregator.dao.NeoCustomerCreationReqResDao;
import com.neo.aggregator.dao.NeoLienMarkReqResDao;
import com.neo.aggregator.dto.AccountInquiryResponseDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.NeoAccountCreationReqResModel;
import com.neo.aggregator.model.NeoCustomerCreationReqResModel;
import com.neo.aggregator.model.NeoLienMarkReqResModel;
import com.neo.aggregator.utility.NeoExceptionBuilder;
import com.neo.aggregator.utility.NeoExceptionConstant;

@Service
public class AuditServiceImpl {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(AuditServiceImpl.class);

	@Autowired
	private NeoCustomerCreationReqResDao customerCreationDao;

	@Autowired
	private NeoAccountCreationReqResDao accountCreationDao;

	@Autowired
	private NeoLienMarkReqResDao tdLienMarkDao;
	
	@Autowired
	private NeoExceptionBuilder exceptionBuilder;

	@Transactional
	public void saveCustomerCreation(NeoCustomerCreationReqResModel request) {

		try {
			logger.info("Saving Customer Creation Req Res for entity Id ::" + request.getEntityId());
			customerCreationDao.saveAndFlush(request);
			logger.info("Saved Customer Creation Req Res for entity Id ::" + request.getEntityId());
		} catch (Exception e) {
			logger.info("Saving Customer Creation Req Res Failed ::", e);
		}

	}

	@Transactional
	public void saveAccountCreation(NeoAccountCreationReqResModel request) {

		try {
			logger.info("Saving Account Creation Req Res for entity Id ::" + request.getEntityId());
			accountCreationDao.saveAndFlush(request);
			logger.info("Saved Account Creation Req Res for entity Id ::" + request.getEntityId());
		} catch (Exception e) {
			logger.info("Saving Account Creation Req Res Failed ::", e);
		}

	}

	@Transactional
	public void saveTdLienMark(NeoLienMarkReqResModel request) {
		
		try {
			logger.info("Saving TD Lien Mark Req Res for entity Id ::" + request.getEntityId());
			tdLienMarkDao.saveAndFlush(request);
			logger.info("Saved TD Lien Mark Req Res for entity Id ::" + request.getEntityId());
		} catch (Exception e) {
			logger.info("Saving TD Lien Mark Req Res Failed ::", e);
		}

	}
	
	public AccountInquiryResponseDto fetchAccountDetailByAccountNo(String accountNo) throws NeoException {

		AccountInquiryResponseDto responseDto = new AccountInquiryResponseDto();

		try {
			List<NeoAccountCreationReqResModel> accountDetailList = accountCreationDao.findByAccountIdAndStatus(accountNo,"SUCCESS");

			if (accountDetailList == null || accountDetailList.isEmpty()) {
				logger.info("Account details not found");
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.ACCOUNT_NUMBER_NOT_FOUND, accountNo, null, Locale.US, null, null,ProductConstants.GENERIC);
			}
			
			NeoAccountCreationReqResModel accountDetail = accountDetailList.get(0);

			responseDto.setAccountNo(accountDetail.getAccountId());
			responseDto.setAccountCurrency(accountDetail.getAccountCurrency());
			responseDto.setSchemeCode(accountDetail.getSchemeCode());
			responseDto.setModeOfOpertion(accountDetail.getModeOfOperation());
			responseDto.setPartnerName(accountDetail.getTenant());
			
		} catch(NeoException ex) {
			throw ex;
		} catch (Exception e) {
			logger.info("Exception occured ::", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.UNABLE_FETCH_ACCOUNT, accountNo, null, Locale.US, null, null,ProductConstants.GENERIC);
		}

		return responseDto;

	}

}
