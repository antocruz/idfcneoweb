package com.neo.aggregator.serviceimpl;

import java.util.HashMap;
import java.util.Map;

import javax.jms.Queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.dao.NotificationAlertDao;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.NotificationRequestDto;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.NotificationAlertModel;

@Service("notificationAlertService")
public class NotificationAlertService {
	
	@Autowired
	private NotificationAlertDao alertDao;
	
	@Autowired
	private JmsMessagingTemplate jmsMessagingTemplate;

	@Autowired
	@Qualifier("notificationAlert")
	private Queue queue;
	
	@Autowired
	private ObjectMapper mapper;
	
	private static final Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(NotificationAlertService.class);
	
	@Async("neoPoolExecutor")
	public void notifyExceptionAlert(NeoException exception,String tenant,String requestPayload,String responsePayload,String product) {
		
		try {
			
			NotificationAlertModel alertModel = alertDao.findByBusinessAndErrorCode(tenant, exception.getErrorCode());
			
			if(alertModel!=null && alertModel.getAlertEnabled()) {
				
				logger.info("Alert enabled for error code ::"+alertModel.getErrorCode());
			
				NotificationRequestDto request = new NotificationRequestDto();

				Map<String, String> emailNotifyData = new HashMap<>();
				
				emailNotifyData.put("to_email", alertModel.getToAddress());
				emailNotifyData.put("requestPayload", requestPayload);
				emailNotifyData.put("responsePayload", responsePayload);
				emailNotifyData.put("service", product);
				emailNotifyData.put("subject", alertModel.getSubject());
				emailNotifyData.put("errorCode", exception.getErrorCode());
				emailNotifyData.put("shortMessage", exception.getShortMessage());
				emailNotifyData.put("detailMessage", exception.getDetailMessage());
				
				String business = tenant.concat("_alert");

				request.setTransactionType(exception.getErrorCode());
				request.setServerNotifyData(emailNotifyData);
				request.setNotificationType("C");
				request.setBusiness(business.toUpperCase());
				request.setEmailNotifyData(emailNotifyData);
				request.setMobileNo(alertModel.getToMobile());

				String msg = mapper.writeValueAsString(request);
				logger.info("Sending data to Queue ::" + msg);
				this.jmsMessagingTemplate.convertAndSend(queue, msg);
			}
			
		} catch(Exception e) {
			logger.info("Exception occured when notify alert::"+e.getMessage());
		}
		
	}

}
