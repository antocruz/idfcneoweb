package com.neo.aggregator.serviceimpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Map;
import java.util.StringJoiner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.constant.ProductConstants;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.NeoResponse;
import com.neo.aggregator.dto.RegistrationRequestV2Dto;
import com.neo.aggregator.dto.RegistrationResponseDto;
import com.neo.aggregator.dto.neolego.PanDataDto;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.service.AggregatorInterfaceService;
import com.neo.aggregator.service.AggregatorService;
import com.neo.aggregator.service.NeoLegoService;
import com.neo.aggregator.utility.NeoExceptionBuilder;
import com.neo.aggregator.utility.NeoExceptionConstant;
import com.neo.aggregator.utility.TenantContextHolder;

@Service
public class NeoLegoServiceImpl implements NeoLegoService {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(NeoLegoServiceImpl.class);

	@Value("${neolego.validatepan.url}")
	private String panValidateURL;

	@Value("${neo.agg.core.url}")
	private String neoCoreUrl;

	@Autowired
	private AggregatorInterfaceService interfaceService;

	@Autowired
	private RestTemplate restTemplate;

	@Value("${neolego.aadrilla.apikey}")
	private String apiKey;
	
	@Autowired
	private NeoExceptionBuilder exceptionBuilder;

	@SuppressWarnings("unchecked")
	@Override
	public PanDataDto validatePan(String request) throws NeoException {

		ObjectMapper mapper = new ObjectMapper();
		PanDataDto panData = new PanDataDto();

		try {

			Map<String, String> requestData = mapper.readValue(request, Map.class);

			StringBuilder bearerToken = new StringBuilder("Bearer" + " " + apiKey);
			StringBuilder urlBuilder = new StringBuilder(panValidateURL);

			String panNumber = requestData.get("idNumber");

			StringJoiner sj = new StringJoiner("&");
			sj.add("pan" + "=" + panNumber);

			URL url = new URL(urlBuilder.toString());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Authorization", bearerToken.toString());

			conn.setDoOutput(true);

			byte[] body = sj.toString().getBytes(StandardCharsets.UTF_8);
			OutputStream out = conn.getOutputStream();
			out.write(body);

			BufferedReader responseRdr;
			StringBuilder response = new StringBuilder();
			String line;

			if (conn.getResponseCode() >= 200 && conn.getResponseCode() <= 300) {
				responseRdr = new BufferedReader(new InputStreamReader(conn.getInputStream()));

				while ((line = responseRdr.readLine()) != null) {
					response.append(line);
				}

				Map<String, String> responseData = mapper.readValue(response.toString(), Map.class);

				Map<String, String> validationMsg = mapper
						.readValue(mapper.writeValueAsString(responseData.get("data")), Map.class);

				panData.setPannumber(validationMsg.get("pan_number"));
				panData.setFirstName(validationMsg.get("first_name"));
				panData.setMiddleName(validationMsg.get("middle_name"));
				panData.setLastName(validationMsg.get("last_name"));
				panData.setTitle(validationMsg.get("pan_holder_title"));
				panData.setLastUpdated(validationMsg.get("pan_last_updated"));
				panData.setNameOnCard(validationMsg.get("name_on_card"));

				if (validationMsg.get("pan_status").equals("VALID")) {
					panData.setValid(Boolean.TRUE);
					panData.setExists("E");
				} else {
					panData.setValid(Boolean.FALSE);
					panData.setExists("0");
				}

				panData.setMessage(validationMsg.get("msg"));
				panData.setValidationId(String.valueOf(validationMsg.get("id")));

			} else {
				responseRdr = new BufferedReader(new InputStreamReader(conn.getErrorStream()));

				while ((line = responseRdr.readLine()) != null) {
					response.append(line);
				}

				Map<String, String> responseData = mapper.readValue(response.toString(), Map.class);

				panData.setPannumber(panNumber);
				panData.setValid(Boolean.FALSE);
				panData.setExists("0");
				panData.setMessage(responseData.get("error"));
			}

			responseRdr.close();
			conn.disconnect();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return panData;
	}

	@Override
	public RegistrationResponseDto register(RegistrationRequestV2Dto request) throws NeoException {

		String tenant = TenantContextHolder.getNeoTenant();

		AggregatorService serivce = interfaceService.findServiceBasedonTenant(tenant);

		return serivce.register(request);
	}

	@Override
	public RegistrationResponseDto addProduct(RegistrationRequestV2Dto request) throws NeoException {
		String tenant = TenantContextHolder.getNeoTenant();

		AggregatorService serivce = interfaceService.findServiceBasedonTenant(tenant);

		return serivce.addProduct(request);
	}

	@Override
	public RegistrationResponseDto registerWithCore(RegistrationRequestV2Dto request) throws NeoException {

		String tenant = TenantContextHolder.getNeoTenant();
		String kitNo = null;

		AggregatorService serivce = interfaceService.findServiceBasedonTenant(tenant);

		RegistrationResponseDto response = serivce.register(request);

		if (response != null && response.getStatus() != null && !response.getStatus().equals("SUCCESS"))
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.REGISTRATION_FAILED, null , null, Locale.US, null, null,ProductConstants.PRODUCT_REGISTRATION);

		ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		try {
			logger.info("Neo Register Response ::" + response.toString());

			String req = mapper.writeValueAsString(request);

			String url = neoCoreUrl + "registration-manager/v4/register";

			HttpHeaders headers = new HttpHeaders();
			headers.add("TENANT", TenantContextHolder.getNeoTenant());
			headers.setContentType(MediaType.APPLICATION_JSON);

			String res = postForEntityWithHeaders(req, url, HttpMethod.POST, headers);

			if (res != null) {
				NeoResponse resp = mapper.readValue(res, NeoResponse.class);
				if (resp != null && resp.getResult() != null) {
					String result = mapper.writeValueAsString(resp.getResult());
					RegistrationResponseDto coreRegisterResponse = mapper.readValue(result,
							RegistrationResponseDto.class);
					kitNo = coreRegisterResponse.getKitNo();
					response.setKitNo(kitNo);
				} else {
					logger.info("Exception occured on core");
					throw new NeoException(resp.getException().getShortMessage(),
							resp.getException().getDetailMessage(), null, null, "Exception occured please try later");
				}
			} else {
				throw new NeoException("Something went wrong contact support", "Something went wrong contact support",
						null, null, "Exception occured please try later");
			}

		} catch (NeoException e) {
			throw e;
		} catch (Exception ex) {
			logger.info("Exception occured ::", ex);
			throw new NeoException("Something went wrong contact support", "Something went wrong contact support", null,
					null, "Exception occured please try later");
		}

		return response;
	}

	private String postForEntityWithHeaders(String request, String url, HttpMethod method, HttpHeaders headers) {

		try {

			logger.info("Request to Server :: " + request);

			HttpEntity<String> entity = new HttpEntity<>(request, headers);
			ResponseEntity<String> response = restTemplate.exchange(url, method, entity, String.class);

			logger.info("Response from Server :: " + response.getBody());

			return response.getBody();
		} catch (Exception e) {
			logger.info("Exception Occured :: " + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
		}
		return null;
	}

}
