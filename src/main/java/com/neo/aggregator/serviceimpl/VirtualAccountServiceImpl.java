package com.neo.aggregator.serviceimpl;

import java.util.List;
import java.util.Locale;

import com.fasterxml.jackson.core.type.TypeReference;
import com.neo.aggregator.model.EcollectRequest;
import com.neo.aggregator.service.RemoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.constant.ProductConstants;
import com.neo.aggregator.dao.EcollectCoreUrlConfigDao;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.NeoResponse;
import com.neo.aggregator.dto.ecollect.EcollectRequestDto;
import com.neo.aggregator.model.EcollectCoreUrlConfig;
import com.neo.aggregator.service.VirtualAccountService;
import com.neo.aggregator.utility.NeoExceptionBuilder;
import com.neo.aggregator.utility.NeoExceptionConstant;

@Service
public class VirtualAccountServiceImpl implements VirtualAccountService {

	private static Logger logger = LoggerFactory.getLogger(VirtualAccountServiceImpl.class);

	@Autowired
	EcollectCoreUrlConfigDao coreUrlConfigDao;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private NeoExceptionBuilder exceptionBuilder;

	@Autowired
	private ObjectMapper mapper;

	@Autowired @Lazy
	private RemoteService rmService;
	
    @Value("${core.ecollecturl}")
    private String coreNotifyUrl;

    @Value("${core.ecollectValidateurl}")
    private String coreValidateUrl;

	@Value("${neo.agg.local.url}")
	private String localUrl;

	@Override
	public NeoResponse eCollectValidate(EcollectRequestDto request) throws NeoException {

		String url = coreValidateUrl;

		try {

			EcollectCoreUrlConfig coreUrlConfig = fetchEcollectConfigByCode(request);

			if (coreUrlConfig != null)
				url = coreUrlConfig.getCoreServiceUrl() + "/ecollectValidate";

			return postToCore(request, url);

		} catch (NeoException e) {
			throw e;
		} catch (Exception ex) {
			logger.info("Exception occured :: " + ex.getMessage());
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SOMETHING_WENT_WRONG, ex.getMessage(), null,
					Locale.US, null, null, ProductConstants.ECOLLECT);
		}
	}

	@Override
	public NeoResponse eCollectNotify(EcollectRequestDto request) throws NeoException {

		String url = coreNotifyUrl;

		try {

			EcollectCoreUrlConfig coreUrlConfig = fetchEcollectConfigByCode(request);

			if (coreUrlConfig != null)
				url = coreUrlConfig.getCoreServiceUrl() + "/ecollectNotify";

			return postToCore(request, url);

		} catch (NeoException e) {
			throw e;
		} catch (Exception ex) {
			logger.info("Exception occured :: " + ex.getMessage());
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SOMETHING_WENT_WRONG, ex.getMessage(), null,
					Locale.US, null, null, ProductConstants.ECOLLECT);
		}
	}

	private NeoResponse postToCore(EcollectRequestDto request, String coreUrl) throws NeoException {

		NeoResponse coreResp = new NeoResponse();

		String requestPayload = null;
		String responsePayload = null;

		try {
			requestPayload = mapper.writeValueAsString(request);
			logger.info("Request Payload ::" + requestPayload);

			ResponseEntity<String> resp = restTemplate.postForEntity(coreUrl, request, String.class,
					new Object[] { coreUrl });

			responsePayload = resp.getBody();

			logger.info("Response Received ::" + responsePayload);

			coreResp = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
					.readValue(resp.getBody(), NeoResponse.class);

		} catch (HttpStatusCodeException e) {

			logger.info("Exception Response Received ::" + e.getResponseBodyAsString() + " Status Code ::"
					+ e.getRawStatusCode());

			responsePayload = e.getResponseBodyAsString();

			ResponseEntity<String> respException = ResponseEntity.status(e.getRawStatusCode())
					.headers(e.getResponseHeaders()).body(responsePayload);

			try {
				coreResp = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
						.readValue(respException.getBody(), NeoResponse.class);
			} catch (Exception ex) {
				logger.info("Core Response Mapping failed", ex.getMessage());
				exceptionBuilder.buildAndNotify(NeoExceptionConstant.SOMETHING_WENT_WRONG, null, null, Locale.US,
						requestPayload, responsePayload, ProductConstants.ECOLLECT);
			}
		} catch (Exception ex) {
			logger.info("Core Response Mapping failed", ex.getMessage());
			exceptionBuilder.buildAndNotify(NeoExceptionConstant.SOMETHING_WENT_WRONG, null, null, Locale.US,
					requestPayload, responsePayload, ProductConstants.ECOLLECT);
		}

		return coreResp;
	}

	private EcollectCoreUrlConfig fetchEcollectConfigByCode(EcollectRequestDto request) {

		Integer vaCodeLength = request.getVaCodeLength() != null ? request.getVaCodeLength() : 9;
		String code = request.getEcollectCode();

		String ecollectCode = null;

		if (code != null && code.length() > vaCodeLength)
			ecollectCode = code.substring(0, vaCodeLength);
		else
			return null;

		return coreUrlConfigDao.findByEcollectCode(ecollectCode);

	}

	@Override
	public List<EcollectRequest> fetchEcollectData(String tenant, EcollectRequestDto request) {

		NeoResponse response = rmService.fetchData(request,
				localUrl + "/neo-lego/common/fetchEcollectRequestData", tenant, HttpMethod.POST);

		return new ObjectMapper().configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true)
				.convertValue(response.getResult(), new TypeReference<List<EcollectRequest>>() {
				});
	}
}
