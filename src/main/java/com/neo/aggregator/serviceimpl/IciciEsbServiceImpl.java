package com.neo.aggregator.serviceimpl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.bank.icici.core.IciciCoreDto;
import com.neo.aggregator.bank.icici.core.IciciCoreExceptionDto;
import com.neo.aggregator.bank.icici.enums.InternalFTTypes;
import com.neo.aggregator.bank.icici.request.BeneficiaryAdditionRequest;
import com.neo.aggregator.bank.icici.request.BeneficiaryVpaAdditionRequest;
import com.neo.aggregator.bank.icici.request.CIBRegistrationRequest;
import com.neo.aggregator.bank.icici.request.CPCSRequest;
import com.neo.aggregator.bank.icici.request.CompositeFundTransferRequest;
import com.neo.aggregator.bank.icici.request.IMPSStatusCheckRequest;
import com.neo.aggregator.bank.icici.request.NeftRtgsFTStatusCheckRequest;
import com.neo.aggregator.bank.icici.request.UPIStatusCheckRequest;
import com.neo.aggregator.bank.icici.request.UpiCollectRefundRequest;
import com.neo.aggregator.bank.icici.request.paylater.AccountBalanceInquiryRequest;
import com.neo.aggregator.bank.icici.request.paylater.AccountDiscoveryRequest;
import com.neo.aggregator.bank.icici.request.paylater.DebitOrchestrationRequest;
import com.neo.aggregator.bank.icici.request.paylater.OtpRequest;
import com.neo.aggregator.bank.icici.response.BeneficiaryAdditionResponse;
import com.neo.aggregator.bank.icici.response.CIBRegistrationResponse;
import com.neo.aggregator.bank.icici.response.CPCSResponse;
import com.neo.aggregator.bank.icici.response.CompositeFundTransferResponse;
import com.neo.aggregator.bank.icici.response.IMPSStatusCheckResponse;
import com.neo.aggregator.bank.icici.response.NeftRtgsFTStatusCheckResponse;
import com.neo.aggregator.bank.icici.response.UPIStatusCheckResponse;
import com.neo.aggregator.bank.icici.response.UpiCollectRefundResponse;
import com.neo.aggregator.bank.icici.response.paylater.AccountBalanceInquiryResponse;
import com.neo.aggregator.bank.icici.response.paylater.AccountDiscoveryResponse;
import com.neo.aggregator.bank.icici.response.paylater.DebitOrchestrationResponse;
import com.neo.aggregator.bank.icici.response.paylater.OtpResponse;
import com.neo.aggregator.bank.icici.utils.EncryptionDecryption;
import com.neo.aggregator.bank.icici.utils.UtilityFunctions;
import com.neo.aggregator.commonservice.IciciCommonService;
import com.neo.aggregator.constant.IciciPropertyConstants;
import com.neo.aggregator.constant.NeoCustomFieldConstants;
import com.neo.aggregator.constant.ProductConstants;
import com.neo.aggregator.dao.NeoFundTransferDao;
import com.neo.aggregator.dao.UpiCollectRequestDao;
import com.neo.aggregator.dto.AadhaarOtpRequest;
import com.neo.aggregator.dto.AadhaarOtpResponse;
import com.neo.aggregator.dto.AccountClosureRequestDto;
import com.neo.aggregator.dto.AccountClosureResponseDto;
import com.neo.aggregator.dto.AccountCreationRequestDto;
import com.neo.aggregator.dto.AccountCreationResponseDto;
import com.neo.aggregator.dto.AccountInquiryRequestDto;
import com.neo.aggregator.dto.AccountInquiryResponseDto;
import com.neo.aggregator.dto.AccountModificationRequestDto;
import com.neo.aggregator.dto.AccountModificationResponseDto;
import com.neo.aggregator.dto.BalanceCheckRequest;
import com.neo.aggregator.dto.BalanceCheckResponseDto;
import com.neo.aggregator.dto.BeneficiaryManagementReqDto;
import com.neo.aggregator.dto.BeneficiaryManagementResDto;
import com.neo.aggregator.dto.CustomerDedupCheckResponseDto;
import com.neo.aggregator.dto.FetchAccountStatementRequest;
import com.neo.aggregator.dto.FetchAccountStatemetResponse;
import com.neo.aggregator.dto.FundTransferRequestDto;
import com.neo.aggregator.dto.FundTransferResponseDto;
import com.neo.aggregator.dto.ImpsTransactionRequestDto;
import com.neo.aggregator.dto.ImpsTransactionResponseDto;
import com.neo.aggregator.dto.KYCBioRequestDto;
import com.neo.aggregator.dto.KYCBioResponseDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.PanValidationRequest;
import com.neo.aggregator.dto.PanValidationResponse;
import com.neo.aggregator.dto.PayLaterRequestDto;
import com.neo.aggregator.dto.PayLaterResponseDto;
import com.neo.aggregator.dto.RegistrationRequestDto;
import com.neo.aggregator.dto.RegistrationRequestV2Dto;
import com.neo.aggregator.dto.RegistrationResponseDto;
import com.neo.aggregator.dto.TDAcctCloseTrailResponseDto;
import com.neo.aggregator.dto.TdAccountTrailCloseRequestDto;
import com.neo.aggregator.dto.yesbank.MerchantOBReq;
import com.neo.aggregator.dto.yesbank.MerchantOBRes;
import com.neo.aggregator.dto.yesbank.NeoPayRequest;
import com.neo.aggregator.dto.yesbank.NeoPayResponse;
import com.neo.aggregator.dto.yesbank.ValidateVpaReq;
import com.neo.aggregator.dto.yesbank.ValidateVpaRes;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.MasterConfiguration;
import com.neo.aggregator.model.NeoBusinessCustomField;
import com.neo.aggregator.model.NeoFundTransfer;
import com.neo.aggregator.model.PartnerServiceConfiguration;
import com.neo.aggregator.model.UpiCollectRequest;
import com.neo.aggregator.service.AggregatorService;
import com.neo.aggregator.service.IciciEbsService;
import com.neo.aggregator.service.NeoBusinessCustomFieldService;
import com.neo.aggregator.service.PartnerConfigService;
import com.neo.aggregator.utility.NeoExceptionBuilder;
import com.neo.aggregator.utility.NeoExceptionConstant;
import com.neo.aggregator.utility.TenantContextHolder;
import com.neo.aggregator.utility.TxnIdGenerator;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;

import org.dozer.DozerBeanMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Service("iciciEsbServiceImpl")
public class IciciEsbServiceImpl implements AggregatorService, IciciEbsService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${neo.keystore.password}")
    private String keyStorePassword;

    @Value("${neo.keystore.path}")
    private Resource keyStoreFile;

    @Value("${neo.agg.ssl.alias}")
    private String sslAlias;

    @Autowired
    PartnerConfigService partnerConfigService;

    @Autowired
    private EncryptionDecryption encryptionDecryption;

    @Autowired
    private NeoFundTransferDao fundTransferDao;

    @Autowired
    private NeoBusinessCustomFieldService customFieldService;

    @Autowired
    private IciciCommonService commonService;

    @Autowired
    private UpiCollectRequestDao upiCollectRequestDao;

    @Autowired
    private NeoExceptionBuilder exceptionBuilder;

    @Autowired
    DozerBeanMapper mapper;

    private static final Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(IciciEsbServiceImpl.class);

    private String postForEntityWithHeaders(String request, String url, HttpMethod method,
                                            HttpHeaders headers, Boolean logRequired) {
        try {
            if (logRequired.equals(true)) {
                logger.info("Request for Bank Server :: " + request);
            }

            HttpEntity<String> entity = new HttpEntity<>(request, headers);

            ResponseEntity<String> response;
            try {
                response = restTemplate.exchange(url, method, entity, String.class,
                        url);
            } catch (HttpStatusCodeException e) {
                logger.info("Http Status Error code :: " + e.getStatusCode());
                response = ResponseEntity.status(e.getRawStatusCode()).headers(e.getResponseHeaders())
                        .body(e.getResponseBodyAsString());
            }

            if (logRequired.equals(true)) {
                logger.info("Response from Bank Server :: " + response.getBody());
            }

            return response.getBody();
        } catch (Exception e) {
            logger.info("Exception Occured ::" + e.getMessage());
            logger.m2pdebug("Exception Occured :: ", e);
        }
        return null;
    }

    private RestTemplate sslConfig(String alias, String tlsVersion) throws KeyStoreException, NoSuchAlgorithmException, CertificateException,
            IOException, KeyManagementException, UnrecoverableKeyException {

        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(keyStoreFile.getInputStream(), keyStorePassword.toCharArray());

        try {
            keyStoreFile.getInputStream().close();
        } catch (IOException e) {
            logger.info("IOException KeyStore File :: " + e.getMessage());
        }

        SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
                new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy())
                        .loadKeyMaterial(keyStore, keyStorePassword.toCharArray(),
                                (aliases, socket) -> alias
                        ).build(), new String[]{tlsVersion}, null,
                NoopHostnameVerifier.INSTANCE);

        HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();

        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
        return new RestTemplate(requestFactory);
    }

    private String callIciciCore(HttpHeaders headers,byte[]publicKey, String... varArgs) {

        try {
            String sessionKey = UtilityFunctions.generateSessionKey();
            logger.info("Session Key :: " + sessionKey);

            SecureRandom randomSecureRandom = SecureRandom.getInstance("SHA1PRNG");
            byte[] iv = new byte[16];
            randomSecureRandom.nextBytes(iv);

            String encryptedSessionKey = Base64.encodeBase64String(encryptionDecryption.rsaEncrypt(sessionKey,publicKey));
            String encryptedRequest = encryptionDecryption.aesEncrypt(varArgs[1], sessionKey, iv);
            String encryptedIV = Base64.encodeBase64String(iv);

            IciciCoreDto requestDto = IciciCoreDto.builder()
                    .requestId(varArgs[0])
                    .service("")
                    .initializationVector(encryptedIV)
                    .encryptedData(encryptedRequest)
                    .encryptedKey(encryptedSessionKey)
                    .clientInfo("")
                    .oaepHashAlgorithm("")
                    .optionalParam("")
                    .build();

            String body = new ObjectMapper().writeValueAsString(requestDto);

            if (headers == null) {
                headers = new HttpHeaders();
            }

            headers.setContentType(MediaType.APPLICATION_JSON);
            if (!varArgs[3].equalsIgnoreCase("NA")) {
                headers.set("apikey", varArgs[3]);
                logger.info("ICICI API Key :: " + varArgs[3]);
            }
            logger.info("ICICI API Url :: " + varArgs[2]);
            String responseBody = postForEntityWithHeaders(body, varArgs[2], HttpMethod.POST, headers, true);
            try {
                IciciCoreDto encryptedResponseDto = new ObjectMapper()
                        .readValue(responseBody, IciciCoreDto.class);

                return encryptionDecryption.aesDecrypt(encryptedResponseDto.getEncryptedData(),
                        encryptedResponseDto.getEncryptedKey(), varArgs[4]);
            } catch (Exception e) {
                IciciCoreExceptionDto exceptionDto = new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(responseBody, IciciCoreExceptionDto.class);

                return new ObjectMapper().writeValueAsString(exceptionDto);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }

    }

    private String callIciciCoreAsymc(HttpHeaders headers,byte[]publicKey, String... varArgs) {
        String certBitLen = "";
        String encPayload = "";
        try {

            encPayload = Base64.encodeBase64String(encryptionDecryption.rsaEncrypt(varArgs[1],publicKey));

            if (headers == null) {
                headers = new HttpHeaders();
            }

            headers.setContentType(MediaType.APPLICATION_JSON);
            if (!varArgs[3].equalsIgnoreCase("NA")) {
                headers.set("apikey", varArgs[3]);
                logger.info("ICICI API Key :: " + varArgs[3]);
            }

            logger.info("ICICI API Url :: " + varArgs[2]);
            String responseBody = postForEntityWithHeaders(encPayload, varArgs[2], HttpMethod.POST, headers, true);

            try {
                if (varArgs.length == 5) {
                    certBitLen = varArgs[4];
                }
                return encryptionDecryption.rsaDecrypt(Base64.decodeBase64(responseBody), certBitLen);

            } catch (Exception e) {

                IciciCoreExceptionDto exceptionDto = new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(responseBody, IciciCoreExceptionDto.class);

                return new ObjectMapper().writeValueAsString(exceptionDto);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }

    }

    @Override
    public PayLaterResponseDto payLaterAccountDiscovery(PayLaterRequestDto request) throws NeoException {
        PayLaterResponseDto responseDto = new PayLaterResponseDto();
        try {
            MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

            PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
                    ProductConstants.PL_ACCOUNT_DISCOVERY);

            String requestId = TxnIdGenerator.getReferenceNumber();
            String url = serviceModel.getServiceEndpoint();

            AccountDiscoveryRequest payLaterAcntDiscoveryRequest = AccountDiscoveryRequest.builder()
                    .mobileNumber(request.getMobileNumber())
                    .build();
            String payLaterAcntDiscovertReqStr = new ObjectMapper().writeValueAsString(payLaterAcntDiscoveryRequest);

            logger.info("Actual Payload for Account Discovery :: " + payLaterAcntDiscovertReqStr);

            NeoBusinessCustomField payLaterApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_PAYLATER_APIKEY,
                    TenantContextHolder.getNeoTenant());
            String apiKey = (payLaterApiKeyField != null) ? payLaterApiKeyField.getFieldValue() : IciciPropertyConstants.PAYLATER_API_KEY;

            String actualResponseStr = callIciciCore(null,serviceModel.getBankPublicKey(),requestId, payLaterAcntDiscovertReqStr, url, apiKey,
                    IciciPropertyConstants.ICICI_M2P_4096KEY);
            logger.info("Actual Response for Account Discovery :: " + actualResponseStr);

            if (actualResponseStr != null) {

                AccountDiscoveryResponse actualResponse = new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(actualResponseStr, AccountDiscoveryResponse.class);

                if (actualResponse.getSuccess() != null
                        && actualResponse.getSuccess().equalsIgnoreCase("false")) {
                    throw new NeoException("Failure", "Account Discovery Failed", null, null, "Failure");

                } else {
                    if ((actualResponse.getStatus() != null &&
                            actualResponse.getStatus().equalsIgnoreCase("FAILED"))) {
                        responseDto.setStatus("Failure");
                    } else
                        responseDto.setStatus(actualResponse.getStatus());

                    responseDto.setAccountNumber(actualResponse.getAccountNumber());
                    responseDto.setMessage(actualResponse.getMessage());

                    //if success and for pay@ease call balance fetch and respond
                    if (actualResponse.getAccountNumber() != null &&
                            TenantContextHolder.getNeoTenant().equalsIgnoreCase("pay@ease")) {
                        PayLaterRequestDto balanceFetchDto = new PayLaterRequestDto();
                        balanceFetchDto.setAccountNumber(actualResponse.getAccountNumber());
                        responseDto = payLaterAcctBalInquiry(balanceFetchDto);
                    }
                }
            } else {
                throw new NeoException("Failure", "Account Discovery Failed", null, null, "Failure");
            }

        } catch (NeoException nex) {
            logger.error(nex.getMessage());
            throw nex;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new NeoException("Service Unavailable", "Service Unavailable", null, null, null);
        }

        return responseDto;
    }

    @Override
    public PayLaterResponseDto payLaterAcctBalInquiry(PayLaterRequestDto request) throws NeoException {
        PayLaterResponseDto responseOutDto = new PayLaterResponseDto();
        try {
            MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

            PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
                    ProductConstants.PL_ACCOUNT_BALANCEINQUIRY);

            String requestId = TxnIdGenerator.getReferenceNumber();
            String url = serviceModel.getServiceEndpoint();

            AccountBalanceInquiryRequest payLaterAcntBalRequest = AccountBalanceInquiryRequest.builder()
                    .accountNumber(request.getAccountNumber())
                    .build();

            String payLaterAcntBalReqStr = new ObjectMapper().writeValueAsString(payLaterAcntBalRequest);

            logger.info("Actual Payload for PayLater Balance Inquiry:: " + payLaterAcntBalReqStr);

            NeoBusinessCustomField payLaterApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_PAYLATER_APIKEY,
                    TenantContextHolder.getNeoTenant());
            String apiKey = (payLaterApiKeyField != null) ? payLaterApiKeyField.getFieldValue() : IciciPropertyConstants.PAYLATER_API_KEY;

            String actualResponseStr = callIciciCore(null,serviceModel.getBankPublicKey(),requestId, payLaterAcntBalReqStr, url, apiKey,
                    IciciPropertyConstants.ICICI_M2P_4096KEY);

            logger.info("Actual Response for PayLater Balance Inquiry:: " + actualResponseStr);

            if (actualResponseStr != null) {
                AccountBalanceInquiryResponse actualResponse = new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(actualResponseStr,
                                AccountBalanceInquiryResponse.class);


                if (actualResponse.getSuccess() != null
                        && actualResponse.getSuccess().equalsIgnoreCase("false")) {
                    throw new NeoException("Failure", "Balance Fetch Failed", null, null, "Failure");
                } else {
                    responseOutDto.setAccountNumber(request.getAccountNumber());
                    List<PayLaterResponseDto.AccountBalance> acbalList = new ArrayList<>();

                    for (AccountBalanceInquiryResponse.AccountBalance acntbal : actualResponse.getAcctBal()) {
                        PayLaterResponseDto.AccountBalance accountBalance = new PayLaterResponseDto.AccountBalance();
                        accountBalance.setBalance(acntbal.getAmount());
                        accountBalance.setProductId(acntbal.getBalType());
                        acbalList.add(accountBalance);
                    }

                    responseOutDto.setAccountBalance(acbalList);
                }
            } else {
                throw new NeoException("Failure", "Balance Fetch Failed", null, null, "Failure");
            }
        } catch (NeoException nex) {
            logger.error(nex.getMessage());
            throw nex;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new NeoException("Service Unavailable", "Service Unavailable", null, null, null);
        }
        return responseOutDto;
    }

    @Override
    public PayLaterResponseDto payLaterOtpCreation(PayLaterRequestDto request) throws NeoException {
        PayLaterResponseDto responseDto = new PayLaterResponseDto();
        try {
            MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

            PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
                    ProductConstants.PL_CREATE_OTP);

            String requestId = TxnIdGenerator.getReferenceNumber();
            String url = serviceModel.getServiceEndpoint();

            OtpRequest payLaterOtpCreationRequest = OtpRequest.builder()
                    .accountNumber(request.getAccountNumber())
                    .amount(request.getAmount())
                    .mobileNumber(request.getMobileNumber())
                    .transactionIdentifier(request.getTransactionIdentifier())
                    .build();

            String payLaterOtpCreationReqStr = new ObjectMapper().writeValueAsString(payLaterOtpCreationRequest);

            logger.info("Actual Payload for PayLater OTP Creation :: " + payLaterOtpCreationReqStr);

            NeoBusinessCustomField payLaterApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_PAYLATER_APIKEY,
                    TenantContextHolder.getNeoTenant());
            String apiKey = (payLaterApiKeyField != null) ? payLaterApiKeyField.getFieldValue() : IciciPropertyConstants.PAYLATER_API_KEY;

            String actualResponseStr = callIciciCore(null,serviceModel.getBankPublicKey(),requestId, payLaterOtpCreationReqStr, url, apiKey,
                    IciciPropertyConstants.ICICI_M2P_4096KEY);

            logger.info("Actual Response for PayLater OTP Creation :: " + actualResponseStr);

            if (actualResponseStr != null) {
                OtpResponse actualResponse = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(actualResponseStr, OtpResponse.class);
                if (actualResponse.getSuccess() != null
                        && actualResponse.getSuccess().equalsIgnoreCase("failed")) {
                    throw new NeoException("Failure", "OTP Creation Failed", null, null, "Failure");
                } else {
                    if (actualResponse.getResponseCode() != null &&
                            actualResponse.getResponseCode().equalsIgnoreCase("000")) {
                        responseDto.setResponseCode(actualResponse.getResponseCode());
                        responseDto.setAppName(actualResponse.getAppName());
                        responseDto.setTransactionIdentifier(actualResponse.getTransactionIdentifier());
                        responseDto.setMobileNumber(actualResponse.getMobileNumber());
                        responseDto.setStatus("Success");
                    } else {
                        responseDto.setErrCode(actualResponse.getErrorCode());
                        responseDto.setStatus(actualResponse.getStatus().equalsIgnoreCase("FAILED") ?
                                "Failure" : actualResponse.getStatus());
                        responseDto.setMessage(
                                (actualResponse.getResponseCode() != null &&
                                        actualResponse.getResponseCode().equalsIgnoreCase("555")) ? "Invalid OTP" : "Invalid Data");

                    }
                }
            } else {
                throw new NeoException("Failure", "OTP Creation Failed", null, null, "Failure");
            }
        } catch (NeoException nex) {
            logger.error(nex.getMessage());
            throw nex;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new NeoException("Service Unavailable", "Service Unavailable", null, null, null);
        }
        return responseDto;
    }

    @Override
    public PayLaterResponseDto payLaterOtpVerification(PayLaterRequestDto request) throws NeoException {
        PayLaterResponseDto responseDto = new PayLaterResponseDto();
        try {

            if (request.getOtp() == null) {
                throw new NeoException("Failure", "OTP not found for verification", null, null, "Failure");
            }

            MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

            PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
                    ProductConstants.PL_VERIFY_OTP);

            String requestId = TxnIdGenerator.getReferenceNumber();
            String url = serviceModel.getServiceEndpoint();

            OtpRequest payLaterOtpVerifyRequest = OtpRequest.builder()
                    .otp(request.getOtp())
                    .mobileNumber(request.getMobileNumber())
                    .transactionIdentifier(request.getTransactionIdentifier())
                    .build();

            String payLaterOtpVerifyReqStr = new ObjectMapper().writeValueAsString(payLaterOtpVerifyRequest);

            logger.info("Actual Payload for PayLater OTP Verification :: " + payLaterOtpVerifyReqStr);

            NeoBusinessCustomField payLaterApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_PAYLATER_APIKEY,
                    TenantContextHolder.getNeoTenant());
            String apiKey = (payLaterApiKeyField != null) ? payLaterApiKeyField.getFieldValue() : IciciPropertyConstants.PAYLATER_API_KEY;

            String actualResponseStr = callIciciCore(null,serviceModel.getBankPublicKey(),requestId, payLaterOtpVerifyReqStr, url, apiKey,
                    IciciPropertyConstants.ICICI_M2P_4096KEY);

            logger.info("Actual Response for PayLater OTP Verification :: " + actualResponseStr);

            if (actualResponseStr != null) {
                OtpResponse actualResponse = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(actualResponseStr, OtpResponse.class);

                if (actualResponse.getSuccess() != null
                        && actualResponse.getSuccess().equalsIgnoreCase("Failed")) {
                    throw new NeoException("Failure", "OTP Verification Failed", null, null, "Failure");

                } else {
                    if (actualResponse.getResponseCode() != null &&
                            actualResponse.getResponseCode().equalsIgnoreCase("000")) {

                        responseDto.setResponseCode(actualResponse.getResponseCode());
                        responseDto.setAppName(actualResponse.getAppName());
                        responseDto.setTransactionIdentifier(actualResponse.getTransactionIdentifier());
                        responseDto.setMobileNumber(actualResponse.getMobileNumber());
                        responseDto.setStatus("Success");


                    } else {
                        responseDto.setErrCode(actualResponse.getErrorCode());
                        responseDto.setStatus(actualResponse.getStatus().equalsIgnoreCase("FAILED") ?
                                "Failure" : actualResponse.getStatus());
                        responseDto.setMessage(
                                ((actualResponse.getResponseCode() != null &&
                                        actualResponse.getResponseCode().equalsIgnoreCase("555"))
                                        || actualResponse.getErrorCode().equals("555")) ? "Invalid OTP" : "Invalid Data");
                    }
                }
            } else {
                throw new NeoException("Failure", "OTP Verification Failed", null, null, "Failure");
            }
        } catch (NeoException nex) {
            logger.error(nex.getMessage());
            throw nex;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new NeoException("Service Unavailable", "Service Unavailable", null, null, null);
        }
        return responseDto;
    }

    @Override
    public PayLaterResponseDto payLaterDebitOrchestration(PayLaterRequestDto request) throws NeoException {
        PayLaterResponseDto responseOutDto = new PayLaterResponseDto();
        try {

            if (TenantContextHolder.getNeoTenant().equalsIgnoreCase("pay@ease")) {
                PayLaterResponseDto otpVerifyResponse = payLaterOtpVerification(request);
                if (otpVerifyResponse.getStatus().equalsIgnoreCase("Failure")) {
                    throw new NeoException("Failure", "OTP Verification Failed", null, null, "Failure");
                }
            }

            MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

            PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
                    ProductConstants.PL_DEBIT_ORCHESTRATION);

            String requestId = TxnIdGenerator.getReferenceNumber();
            String url = serviceModel.getServiceEndpoint();

            double txnAmount;
            txnAmount = Double.parseDouble(request.getAmount()) * 100;

            DebitOrchestrationRequest payLaterDebitRequest = DebitOrchestrationRequest.builder()
                    .accountNumber(request.getAccountNumber())
                    .additionalField(request.getTxnParticulars())
                    .billRefInfo(request.getTransactionIdentifier())
                    .consumerCode(request.getTransactionIdentifier())
                    .consumerNumber(request.getMobileNumber())
                    .transactionAmount(String.valueOf((int) txnAmount))
                    .build();

            String payLaterDebitReqStr = new ObjectMapper().writeValueAsString(payLaterDebitRequest);

            logger.info("Actual Payload for PayLater Debit Orchestration :: " + payLaterDebitReqStr);

            NeoBusinessCustomField payLaterApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_PAYLATER_APIKEY,
                    TenantContextHolder.getNeoTenant());
            String apiKey = (payLaterApiKeyField != null) ? payLaterApiKeyField.getFieldValue() : IciciPropertyConstants.PAYLATER_API_KEY;

            String actualResponseStr = callIciciCore(null,serviceModel.getBankPublicKey(),requestId, payLaterDebitReqStr, url, apiKey,
                    IciciPropertyConstants.ICICI_M2P_4096KEY);

            logger.info("Actual Response for PayLater Debit Orchestration :: " + actualResponseStr);

            if (actualResponseStr != null) {
                DebitOrchestrationResponse actualResponse = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(actualResponseStr, DebitOrchestrationResponse.class);
                if (actualResponse.getSuccess() != null
                        && actualResponse.getSuccess().equalsIgnoreCase("FAILED")) {
                    throw new NeoException("Failure", "Debit Transaction Failed", null, null, "Failure");

                } else {
                    if (actualResponse.getStatus() != null) {
                        responseOutDto.setPaymentId(actualResponse.getPaymentId());
                        responseOutDto.setStatus(
                                actualResponse.getStatus().equalsIgnoreCase("FAILED") ?
                                        "Failure" : actualResponse.getStatus()
                        );
                        txnAmount = Double.parseDouble(actualResponse.getTxnAmount()) / 100;
                        responseOutDto.setTxnAmount(String.valueOf(txnAmount));
                        responseOutDto.setErrCode(actualResponse.getErrCode());
                    } else {
                        throw new NeoException("Failure", "Debit Transaction Failed", null, null, "Failure");
                    }
                }
            } else {
                throw new NeoException("Failure", "Debit Transaction Failed", null, null, "Failure");
            }
        } catch (NeoException nex) {
            logger.error(nex.getMessage());
            throw nex;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new NeoException("Service Unavailable", "Service Unavailable", null, null, null);
        }
        return responseOutDto;
    }

    @Override
    public CustomerDedupCheckResponseDto dedupCheck(RegistrationRequestDto request) throws NeoException {
        CustomerDedupCheckResponseDto dedupCheckResponseDto = new CustomerDedupCheckResponseDto();
        String mobile = "";

        try {

            MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());
            PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
                    ProductConstants.DEDUP_CHECK);
            String requestId = TxnIdGenerator.getReferenceNumber();
            String url = serviceModel.getServiceEndpoint();

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
            sdf.applyPattern("dd/MM/yyyy");
            String dateOfBirth = sdf.format(request.getSpecialDate());

            if (request.getContactNo().length() > 10 &&
                    request.getContactNo().contains("+")) {
                mobile = request.getContactNo().substring(3);
            } else {
                mobile = request.getContactNo();
            }
            CPCSRequest.Demographics demographics = CPCSRequest.Demographics.builder()
                    .DateOfBirth(dateOfBirth)
                    .ApplctnNO("")
                    .CustType("")
                    .FirstName(request.getFirstName())
                    .ProductID("")
                    .MiddleName(request.getMiddleName())
                    .CompanyName("")
                    .UserID(request.getEntityId())
                    .LastName(request.getLastName())
                    .LocCode("")
                    .RefID(TxnIdGenerator.getReferenceNumber())
                    .Pan("")
                    .build();

            CPCSRequest.Address address = CPCSRequest.Address.builder()
                    .AddrLn1(request.getAddress())
                    .AddrLn2(request.getAddress2())
                    .AddrLn3("")
                    .City(request.getCity())
                    .CompanyAddr1("")
                    .CompanyAddr2("")
                    .CompanyAddr3("")
                    .CompanyPincd("")
                    .Pincode(request.getPincode())
                    .build();

            CPCSRequest.Contact contact = CPCSRequest.Contact.builder()
                    .CompanyPhone1("")
                    .CompanyPhone2("")
                    .Phone1(mobile)
                    .Phone2("")
                    .build();

            CPCSRequest cpcsRequest = CPCSRequest.builder().Demographics(demographics)
                    .Address(address).Contact(contact).build();
            String cpcsReqStr = new ObjectMapper().writeValueAsString(cpcsRequest);

            logger.info("Actual Payload for CPCS Check :: " + cpcsReqStr);

            String cpcsResStr = callIciciCore(null,serviceModel.getBankPublicKey(),requestId, cpcsReqStr, url, IciciPropertyConstants.CPCS_API_KEY,
                    IciciPropertyConstants.ICICI_M2P_4096KEY);

            logger.info("Actual Response for CPCS Check :: " + cpcsResStr);

            if (cpcsResStr != null) {
                CPCSResponse cpcsResponse = new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(cpcsResStr, CPCSResponse.class);
                dedupCheckResponseDto = CustomerDedupCheckResponseDto.builder().rematchcount(cpcsResponse.getRematchcount())
                        .requestId(cpcsResponse.getRequestId()).message(cpcsResponse.getMessage())
                        .customermatchcount(cpcsResponse.getCustomermatchcount())
                        .status(cpcsResponse.getStatus()).build();
            } else {
                throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.BANK_EXCEPTION_OCCURED,
                        "Dedup Check Failed" , null, Locale.US, null, null,ProductConstants.DEDUP_CHECK);

            }
        } catch (NeoException nex) {
            logger.error(nex.getMessage());
            throw nex;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
                    "System Error" , null, Locale.US, null, null,ProductConstants.DEDUP_CHECK);

        }
        return dedupCheckResponseDto;
    }

    @Override
    public FundTransferResponseDto paymentStatusCheck(FundTransferRequestDto request) throws NeoException {

        FundTransferResponseDto responseDto = new FundTransferResponseDto();
        String urlServiceId = "";
        String apiKey = "";
        try {

            NeoFundTransfer neoFundTransfer = fundTransferDao.findByExternalTransactionIdAndTenant(
                    request.getExternalTransactionId(),TenantContextHolder.getNeoTenant());

            if (neoFundTransfer == null) {
                logger.info("No such transaction exists!!");
                throw new NeoException("No such transaction exists", "No such transaction exists", null, null,
                        "Fund Transfer Status check Failed");
            }
            String requestId = TxnIdGenerator.getReferenceNumber();
            String statusRequestString = "";
            HttpHeaders headers = new HttpHeaders();

            MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

            NeoBusinessCustomField corpIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_ID,
                    TenantContextHolder.getNeoTenant());
            NeoBusinessCustomField corpUserField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_USER,
                    TenantContextHolder.getNeoTenant());
            NeoBusinessCustomField aggrIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_AGGR_ID,
                    TenantContextHolder.getNeoTenant());
            NeoBusinessCustomField bcIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_BC_ID,
                    TenantContextHolder.getNeoTenant());
            NeoBusinessCustomField passCodeField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_IMPS_PASSCODE,
                    TenantContextHolder.getNeoTenant());
            NeoBusinessCustomField urnField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_URN,
                    TenantContextHolder.getNeoTenant());

            NeoBusinessCustomField fundTrfApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_FUNDTRF_APIKEY,
                    TenantContextHolder.getNeoTenant());
            apiKey = (fundTrfApiKeyField != null) ? fundTrfApiKeyField.getFieldValue() : IciciPropertyConstants.FUNDTRF_API_KEY;

            String urn = (urnField != null) ? urnField.getFieldValue() : "";
            String bcID = (bcIdField != null) ? bcIdField.getFieldValue() : "";
            String passCode = (passCodeField != null) ? passCodeField.getFieldValue() : "";
            String corpId = (corpIdField != null) ? corpIdField.getFieldValue() : "";
            String corpUser = (corpUserField != null) ? corpUserField.getFieldValue() : "";
            String aggrID = (aggrIdField != null) ? aggrIdField.getFieldValue() : "";


            if (neoFundTransfer.getTransactionType().equalsIgnoreCase("UPI")) {
                urlServiceId = ProductConstants.UPI_STATUS_CHECK;
                UPIStatusCheckRequest upiStatusCheckRequest = UPIStatusCheckRequest
                        .builder()
                        .channelCode(IciciPropertyConstants.CHANNEL_CODE)
                        .profileId(IciciPropertyConstants.PROFILE_ID)
                        .deviceId(IciciPropertyConstants.DEVICE_ID)
                        .mobile(neoFundTransfer.getBeneficiaryMobile())
                        .seqNo(TxnIdGenerator.getReferenceNumber())
                        .txnRefno(neoFundTransfer.getExternalTransactionId())
                        .build();

                statusRequestString =new ObjectMapper().writeValueAsString(upiStatusCheckRequest);
            } else if (neoFundTransfer.getTransactionType().equalsIgnoreCase("IMPS")) {
                urlServiceId = ProductConstants.IMPS_STATUS_CHECK;

                IMPSStatusCheckRequest impsStatusCheckRequest = IMPSStatusCheckRequest.builder()
                        .bcId(bcID)
                        .passCode(passCode)
                        .txnRefno(neoFundTransfer.getExternalTransactionId())
                        .build();

                statusRequestString = new ObjectMapper().writeValueAsString(impsStatusCheckRequest);
            } else {
                if (neoFundTransfer.getTransactionType().equalsIgnoreCase("NEFT") ||
                        neoFundTransfer.getTransactionType().equalsIgnoreCase("IFT"))
                    headers.add("x-priority", IciciPropertyConstants.X_PRIORITY_NEFT);
                else
                    headers.add("x-priority", IciciPropertyConstants.X_PRIORITY_RTGS);
                urlServiceId = ProductConstants.FT_STATUS_CHECK;

                NeftRtgsFTStatusCheckRequest ftStatusCheckRequest = NeftRtgsFTStatusCheckRequest.builder()
                        .aggrId(aggrID)
                        .corpId(corpId)
                        .userId(corpUser)
                        .txnRefno(neoFundTransfer.getExternalTransactionId())
                        .urn(urn)
                        .build();
                statusRequestString =new ObjectMapper().writeValueAsString(ftStatusCheckRequest);
            }

            logger.m2pdebug("Actual Payload :: " + statusRequestString);

            PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
                    urlServiceId);

            String responseBody = callIciciCore(headers,serviceModel.getBankPublicKey(),requestId, statusRequestString, serviceModel.getServiceEndpoint(), apiKey,
                    IciciPropertyConstants.ICICI_M2P_4096KEY);

            logger.info("Response Payload :: " + responseBody);

            if (neoFundTransfer.getTransactionType().equalsIgnoreCase("UPI")) {

                UPIStatusCheckResponse paymentStatusResponse = new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(responseBody, UPIStatusCheckResponse.class);

				if (paymentStatusResponse != null) {
					if (paymentStatusResponse.getSuccess().equalsIgnoreCase("true")
							&& paymentStatusResponse.getResponse().equalsIgnoreCase("0")) {
						responseDto.setBankReferenceNo(paymentStatusResponse.getBankRrn());
						responseDto.setStatus(IciciPropertyConstants.SUCCESS_MSG);
						responseDto.setDescription(paymentStatusResponse.getMessage());
					} else {
						responseDto.setBankReferenceNo(paymentStatusResponse.getBankRrn());
						responseDto.setStatus(IciciPropertyConstants.FAILURE_MSG);
						responseDto.setDescription(
								paymentStatusResponse.getMessage() != null ? paymentStatusResponse.getMessage()
										: "Transaction Failure");
					}
				} else {
					responseDto.setStatus(IciciPropertyConstants.FAILURE_MSG);
					responseDto.setDescription("Transaction Failure");
				}

            } else if (neoFundTransfer.getTransactionType().equalsIgnoreCase("IMPS")) {

                IMPSStatusCheckResponse paymentStatusResponse = new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true)
                        .readValue(responseBody, IMPSStatusCheckResponse.class);

				if (paymentStatusResponse != null) {
					if (paymentStatusResponse.getActCode().equalsIgnoreCase("0")) {
						responseDto.setBankReferenceNo(paymentStatusResponse.getBankRrn());
						responseDto.setStatus(IciciPropertyConstants.SUCCESS_MSG);
						responseDto.setDescription(paymentStatusResponse.getTxnResponse());
					} else {
						responseDto.setBankReferenceNo(paymentStatusResponse.getBankRrn());
						responseDto.setStatus(IciciPropertyConstants.FAILURE_MSG);
						responseDto.setDescription(
								paymentStatusResponse.getTxnResponse() != null ? paymentStatusResponse.getTxnResponse()
										: "Transaction Failure");
					}
				} else {
                    responseDto.setStatus(IciciPropertyConstants.FAILURE_MSG);
                    responseDto.setDescription(
                            (paymentStatusResponse != null) ? paymentStatusResponse.getTxnResponse() : "Transaction Failure");
                }

            } else if ((neoFundTransfer.getTransactionType().equalsIgnoreCase("IFT"))||
                    (neoFundTransfer.getTransactionType().equalsIgnoreCase("RTGS"))){

                NeftRtgsFTStatusCheckResponse paymentStatusResponse = new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(responseBody, NeftRtgsFTStatusCheckResponse.class);

                if (paymentStatusResponse != null) {
                    if (paymentStatusResponse.getUtr() != null &&
                            paymentStatusResponse.getStatus().equalsIgnoreCase("success")) {
                        responseDto.setBankReferenceNo(paymentStatusResponse.getUtr());
                        responseDto.setStatus(IciciPropertyConstants.SUCCESS_MSG);
                        responseDto.setDescription(paymentStatusResponse.getTxnResponse());
                    } else if (paymentStatusResponse.getStatus().equalsIgnoreCase("PENDING FOR PROCESSING")) {
                        responseDto.setBankReferenceNo(paymentStatusResponse.getUtr());
                        responseDto.setStatus(IciciPropertyConstants.INPROGRESS_MSG);
                        responseDto.setDescription(paymentStatusResponse.getTxnResponse());
                    } else {
                    	responseDto.setBankReferenceNo(paymentStatusResponse.getUtr());
                        responseDto.setStatus(IciciPropertyConstants.FAILURE_MSG);
                        responseDto.setDescription(
                                (paymentStatusResponse.getTxnResponse() != null) ? paymentStatusResponse.getTxnResponse() : "Transaction Failure");
                    }
                } else {
                    responseDto.setStatus(IciciPropertyConstants.FAILURE_MSG);
                    responseDto.setDescription("Transaction Failure");
                }

            } else {

                NeftRtgsFTStatusCheckResponse paymentStatusResponse = new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(responseBody, NeftRtgsFTStatusCheckResponse.class);

                if (paymentStatusResponse != null && paymentStatusResponse.getUtr() != null) {

                    FundTransferResponseDto transferResponseDto = neftIncrementalStatusCheck(paymentStatusResponse.getUtr(),
                            TenantContextHolder.getNeoTenant());

                    if (transferResponseDto != null) {
                        if (transferResponseDto.getStatus().equalsIgnoreCase(IciciPropertyConstants.SUCCESS_MSG) ||
                                (transferResponseDto.getStatus().equalsIgnoreCase(IciciPropertyConstants.INPROGRESS_MSG))) {
                            responseDto.setBankReferenceNo(transferResponseDto.getBankReferenceNo());
                            responseDto.setStatus(transferResponseDto.getStatus());
                            responseDto.setDescription(transferResponseDto.getDescription());
                        } else {
                        	responseDto.setBankReferenceNo(transferResponseDto.getBankReferenceNo());
                            responseDto.setStatus(IciciPropertyConstants.FAILURE_MSG);
                            responseDto.setDescription(
                                    (paymentStatusResponse.getTxnResponse() != null) ? paymentStatusResponse.getTxnResponse() : "Transaction Failure");
                        }
                    } else {
                        responseDto.setStatus(IciciPropertyConstants.FAILURE_MSG);
                        responseDto.setDescription("Transaction Failure");
                    }
                } else {
                    responseDto.setStatus(IciciPropertyConstants.FAILURE_MSG);
                    responseDto.setDescription(
                            (paymentStatusResponse != null) ? paymentStatusResponse.getTxnResponse() : "Transaction Failure");
                }

            }

            logger.info("Response Received for Fund Transfer Status Check :: " + responseBody);

            return responseDto;

        } catch (NeoException nEx) {
            throw nEx;
        } catch (Exception e) {
            logger.m2pdebug("Exception Occured :: ", e);
            throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
                    "Payment Status Check Failed" , null, Locale.US, null, null,urlServiceId);
        }
    }

    @Override
    public FundTransferResponseDto fundTransfer(FundTransferRequestDto request) throws NeoException {
        FundTransferResponseDto responseOutDto = new FundTransferResponseDto();
        try {

            NeoFundTransfer neoFundTransfer;
            CompositeFundTransferRequest fundTransferRequest = new CompositeFundTransferRequest();
            HttpHeaders headers = new HttpHeaders();

            neoFundTransfer = fundTransferDao.findByExternalTransactionIdAndTenant(request.getExternalTransactionId(),
                    TenantContextHolder.getNeoTenant());

            if (neoFundTransfer == null) {
                neoFundTransfer = mapper.map(request, NeoFundTransfer.class);
                neoFundTransfer.setBeneficiaryAccountNo(request.getToAccountNo());
                neoFundTransfer.setRemitterAccountNo(request.getFromAccountNo());
                neoFundTransfer.setTenant(TenantContextHolder.getNeoTenant());
                neoFundTransfer.setTransactionAmount(request.getAmount());
                neoFundTransfer.setTransactionStatus(IciciPropertyConstants.INPROGRESS_MSG);
                fundTransferDao.save(neoFundTransfer);
            }

            MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

            PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
                    ProductConstants.FUND_TRANSFER);

            String requestId = TxnIdGenerator.getReferenceNumber();
            String url = serviceModel.getServiceEndpoint();

            if (request.getTransactionType().equalsIgnoreCase("UPI")) {
                headers.add("x-priority", IciciPropertyConstants.X_PRIORITY_UPI);
                fundTransferRequest = commonService.buildUPIFundTrfRequest(request);
            } else if (request.getTransactionType().equalsIgnoreCase("IMPS")) {
                headers.add("x-priority", IciciPropertyConstants.X_PRIORITY_IMPS);
                fundTransferRequest = commonService.buildIMPSFundTrfRequest(request);
            } else if (request.getTransactionType().equalsIgnoreCase("NEFT") ||
                    request.getTransactionType().equalsIgnoreCase("IFT")) {
                headers.add("x-priority", IciciPropertyConstants.X_PRIORITY_NEFT);
                fundTransferRequest = commonService.buildNEFTFundTrfRequest(request);
            } else if (request.getTransactionType().equalsIgnoreCase("RTGS")) {
                headers.add("x-priority", IciciPropertyConstants.X_PRIORITY_RTGS);
                fundTransferRequest = commonService.buildRTGSFundTrfRequest(request);
            }

            NeoBusinessCustomField ftRealtimeSettlementfield = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_FT_REALTIME_SETTLEMENT,
                    TenantContextHolder.getNeoTenant());

            if (!("N".equalsIgnoreCase(request.getIsWalletDebit()))) {
                if (ftRealtimeSettlementfield != null
                        && "Y".equalsIgnoreCase(ftRealtimeSettlementfield.getFieldValue())) {
                    logger.info("Initiating Fund movement from Pool Account..");
                    String poolAcTrfStatus = poolAcFundTransfer(TxnIdGenerator.getReferenceNumber(),
                            request.getAmount(), request.getDescription(),
                            TenantContextHolder.getNeoTenant(), InternalFTTypes.POOL_DR_NODAL_CR);
                    if (!("success".equalsIgnoreCase(poolAcTrfStatus))) {
                        throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.BANK_EXCEPTION_OCCURED,
                                "Internal fund transfer Failed at Bank" , null, Locale.US, null, null,null);
                    }
                    logger.info("Fund movement from Pool Account Successful!!..");
                }
            }
            String fundTransferReqStr = new ObjectMapper().writeValueAsString(fundTransferRequest);

            logger.info("Actual Payload for ICICI Fund Transfer :: " + fundTransferReqStr);

            NeoBusinessCustomField fundTrfApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_FUNDTRF_APIKEY,
                    TenantContextHolder.getNeoTenant());
            String fundTrfApiKey = (fundTrfApiKeyField != null) ? fundTrfApiKeyField.getFieldValue() : IciciPropertyConstants.FUNDTRF_API_KEY;

            String actualResponseStr = callIciciCore(headers,serviceModel.getBankPublicKey(),requestId, fundTransferReqStr, url, fundTrfApiKey,
                    IciciPropertyConstants.ICICI_M2P_4096KEY);

            logger.info("Actual Response for ICICI Fund Transfer :: " + actualResponseStr);

            if (actualResponseStr != null) {
                CompositeFundTransferResponse actualResponse = new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(actualResponseStr, CompositeFundTransferResponse.class);

                responseOutDto = commonService.buildFundTransferResponse(actualResponse, request.getTransactionType());

                neoFundTransfer.setTransactionStatus(responseOutDto.getStatus().toUpperCase());
                neoFundTransfer.setBankTransactionId(responseOutDto.getBankReferenceNo());

                if(responseOutDto.getStatus().equalsIgnoreCase(IciciPropertyConstants.FAILURE_MSG)) {
                    neoFundTransfer.setTransactionFailureReason(responseOutDto.getDescription());
                }

            } else {
                responseOutDto.setStatus(IciciPropertyConstants.FAILURE_MSG);
                responseOutDto.setDescription("Transaction Failed.");

                neoFundTransfer.setTransactionStatus(responseOutDto.getStatus().toUpperCase());
                neoFundTransfer.setBankTransactionId("");
            }
            fundTransferDao.save(neoFundTransfer);
            responseOutDto.setExternalTransactionId(request.getExternalTransactionId());

        } catch (Exception e) {
            logger.info("Exception Occured :: "+e.getMessage());
            throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
                    "Fund Transfer failed" , null, Locale.US, null, null,ProductConstants.FUND_TRANSFER);
        }
        return responseOutDto;
    }

    @Override
    public CIBRegistrationResponse cibRegistration(CIBRegistrationRequest request) throws NeoException {
        CIBRegistrationResponse responseOutDto = new CIBRegistrationResponse();
        try {

            MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

            PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
                    ProductConstants.CIB_CREATION);

            NeoBusinessCustomField corpIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_ID,
                    TenantContextHolder.getNeoTenant());
            NeoBusinessCustomField corpUserField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_USER,
                    TenantContextHolder.getNeoTenant());
            NeoBusinessCustomField aggrIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_AGGR_ID,
                    TenantContextHolder.getNeoTenant());
            NeoBusinessCustomField aggrNameField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_AGGR_NAME,
                    TenantContextHolder.getNeoTenant());
            NeoBusinessCustomField urnField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_URN,
                    TenantContextHolder.getNeoTenant());

            if (corpUserField == null || corpIdField == null || aggrIdField == null || aggrNameField == null || urnField == null)
                throw new NeoException("Failure", "Either one of CorpUser or Corp ID or Aggr ID or Aggr Name is null or Urn is null", null, null, "Failure");

            String requestId = TxnIdGenerator.getReferenceNumber();
            String url = serviceModel.getServiceEndpoint();

            request.setUrn(urnField.getFieldValue());
            request.setAggrId(aggrIdField.getFieldValue());
            request.setAggrName(aggrNameField.getFieldValue());
            request.setCorpId(corpIdField.getFieldValue());
            request.setUserId(request.getUserId()!=null?request.getUserId(): corpUserField.getFieldValue());
            request.setAliasId("");

            String cibRegisterReqStr = new ObjectMapper().writeValueAsString(request);

            logger.info("Actual Payload for CIB Registration :: " + cibRegisterReqStr);

            NeoBusinessCustomField cibApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CIB_APIKEY,
                    TenantContextHolder.getNeoTenant());
            String cibApiKey = (cibApiKeyField != null) ? cibApiKeyField.getFieldValue() : IciciPropertyConstants.CIB_API_KEY;

            String actualResponseStr = callIciciCore(null,serviceModel.getBankPublicKey(),requestId, cibRegisterReqStr, url, cibApiKey,
                    IciciPropertyConstants.ICICI_M2P_4096KEY);

            logger.info("Actual Response for CIB Registration :: " + actualResponseStr);

            if (actualResponseStr != null) {
                CIBRegistrationResponse actualResponse = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(actualResponseStr, CIBRegistrationResponse.class);
                if (actualResponse.getResponse() != null
                        && actualResponse.getResponse().toUpperCase().contains("FAIL")) {
                    throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.BANK_EXCEPTION_OCCURED,
                            "CIB Registration Failed" , null, Locale.US, null, null,ProductConstants.CIB_CREATION);

                } else {
                    if (actualResponse.getResponse() != null
                            && actualResponse.getResponse().toUpperCase().contains("SUCCESS")) {
                        responseOutDto = actualResponse;
                        responseOutDto.setMessage("Please request Partner - " + TenantContextHolder.getNeoTenant()
                                + " to approve the CIB Registration through their CIB Portal");

                    } else {
                        throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.BANK_EXCEPTION_OCCURED,
                                "CIB Registration Failed" , null, Locale.US, null, null,ProductConstants.CIB_CREATION);
                    }
                }
            } else {
                throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.BANK_EXCEPTION_OCCURED,
                        "CIB Registration Failed" , null, Locale.US, null, null,ProductConstants.CIB_CREATION);
            }
        } catch (NeoException nex) {
            logger.error(nex.getMessage());
            throw nex;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
                    "System Error" , null, Locale.US, null, null,ProductConstants.CIB_CREATION);
        }
        return responseOutDto;
    }

    @Override
    public ImpsTransactionResponseDto internalFundTransfer(ImpsTransactionRequestDto request) throws NeoException {
        FundTransferRequestDto fundTransferRequestDto = new FundTransferRequestDto();
        ImpsTransactionResponseDto responseOutDto = new ImpsTransactionResponseDto();
        FundTransferResponseDto responseDto = null;
        ImpsTransactionResponseDto.TransactionStatus txnStatusdto = new ImpsTransactionResponseDto.TransactionStatus();
        InternalFTTypes txnType = null;

        try {
            txnType = InternalFTTypes.valueOf(request.getTransferType());
        } catch (IllegalArgumentException illExp) {
            txnType = null;
        }
        if (txnType != null) {

            String response = poolAcFundTransfer(request.getUniqueRequestNo(), String.valueOf(request.getAmount()),
                    TenantContextHolder.getNeoTenant() + " " + request.getDescription(),
                    TenantContextHolder.getNeoTenant(), txnType);

            if (response.equalsIgnoreCase(IciciPropertyConstants.SUCCESS_MSG)) {
                txnStatusdto.setBankReferenceNo(request.getUniqueRequestNo());
            }
            txnStatusdto.setStatusCode(response);
            txnStatusdto.setBeneficiaryReferenceNo(request.getUniqueRequestNo());
            responseOutDto.setTransactionStatus(txnStatusdto);

            return responseOutDto;
        }
        fundTransferRequestDto.setFromAccountNo(request.getDebitAccountNo());
        fundTransferRequestDto.setToAccountNo(request.getBeneficiaryAccountNumber());
        fundTransferRequestDto.setDescription(TenantContextHolder.getNeoTenant() + " " + request.getDescription());
        fundTransferRequestDto.setAmount(request.getAmount().toString());
        fundTransferRequestDto.setTransactionCurrency("INR");
        fundTransferRequestDto.setTransactionOrigin("LOAD");
        fundTransferRequestDto.setExternalTransactionId(request.getUniqueRequestNo());
        fundTransferRequestDto.setBeneficiaryName(request.getBeneficiaryName());
        fundTransferRequestDto.setRemitterName(request.getBeneficiaryName());
        fundTransferRequestDto.setBeneficiaryIfsc(request.getBeneficiaryIfscCode());
        fundTransferRequestDto.setTransactionType(request.getTransferType());
        fundTransferRequestDto.setBeneficiaryMobile(request.getBeneficiaryMobileNo());
        fundTransferRequestDto.setTransactionMode("TRANSFER");
        fundTransferRequestDto.setIsWalletDebit("N");

        responseDto = fundTransfer(fundTransferRequestDto);


        if (responseDto.getStatus() != null && responseDto.getStatus().equals("SUCCESS")) {
            txnStatusdto.setStatusCode("COMPLETED");
            txnStatusdto.setBankReferenceNo(responseDto.getBankReferenceNo());
            txnStatusdto.setBeneficiaryReferenceNo(responseDto.getExternalTransactionId());
        } else {
            txnStatusdto.setStatusCode("FAILED");
            txnStatusdto.setReason(responseDto.getDescription());
        }
        responseOutDto.setTransactionStatus(txnStatusdto);

        return responseOutDto;
    }

    @Override
    public BeneficiaryManagementResDto beneficiaryAddition(BeneficiaryManagementReqDto request) throws NeoException {
        BeneficiaryManagementResDto responseOutDto = new BeneficiaryManagementResDto();
        String beneRegisterReqStr;
        try {

            MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

            String isVpaReg = !request.getBeneficiaryAccountNo().contains("@") ? "0" : "1";

            String serviceId = !isVpaReg.equals("1") ? ProductConstants.BENEFICIARY_ADDITION : ProductConstants.BENEFICIARY_VPA_ADDITION;

            PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
                    serviceId);

            Calendar cal = Calendar.getInstance();
            String requestId = TxnIdGenerator.getReferenceNumber();
            String url = serviceModel.getServiceEndpoint();

            NeoBusinessCustomField corpIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_ID,
                    TenantContextHolder.getNeoTenant());
            NeoBusinessCustomField corpUserField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_USER,
                    TenantContextHolder.getNeoTenant());
            NeoBusinessCustomField aggrIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_AGGR_ID,
                    TenantContextHolder.getNeoTenant());
            NeoBusinessCustomField urnField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_URN,
                    TenantContextHolder.getNeoTenant());

            if (corpUserField == null || corpIdField == null || aggrIdField == null || urnField == null)
                throw new NeoException("Failure", "Either one of CorpUser or Corp ID or Aggr ID is null or URN is null", null, null, "Failure");

            if (isVpaReg.equals("0")) {

                int nickNameMaxlen;
                String beneNickName=request.getNickName().concat(String.valueOf(cal.getTimeInMillis()));

                nickNameMaxlen = Math.min(beneNickName.length(), 40);
                beneNickName=beneNickName.substring(0,nickNameMaxlen);

                String payeeType = !request.getIfsc().toUpperCase().startsWith("ICIC") ? "O" : "W";
                BeneficiaryAdditionRequest beneficiaryAdditionRequest = BeneficiaryAdditionRequest.builder()
                        .beneAccNo(request.getBeneficiaryAccountNo())
                        .beneName(request.getBeneficiaryName())
                        .beneNickName(beneNickName)
                        .ifsc(request.getIfsc().toUpperCase())
                        .payeeType(payeeType)
                        .urn(urnField.getFieldValue())
                        .corpUser(corpUserField.getFieldValue())
                        .corpId(corpIdField.getFieldValue())
                        .aggrId(aggrIdField.getFieldValue())
                        .build();
                beneRegisterReqStr = new ObjectMapper().writeValueAsString(beneficiaryAdditionRequest);
            } else {
                BeneficiaryVpaAdditionRequest beneficiaryAdditionRequest = BeneficiaryVpaAdditionRequest.builder()
                        .urn(urnField.getFieldValue())
                        .vpa(request.getBeneficiaryAccountNo())
                        .userId(corpUserField.getFieldValue())
                        .corpId(corpIdField.getFieldValue())
                        .aggrId(aggrIdField.getFieldValue())
                        .build();
                beneRegisterReqStr =new ObjectMapper().writeValueAsString(beneficiaryAdditionRequest);
            }
            logger.info("Actual Payload for Bene Addition :: " + beneRegisterReqStr);

            String actualResponseStr = "";

            NeoBusinessCustomField cibApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CIB_APIKEY,
                    TenantContextHolder.getNeoTenant());
            String cibApiKey = (cibApiKeyField != null) ? cibApiKeyField.getFieldValue() : IciciPropertyConstants.CIB_API_KEY;

            actualResponseStr = callIciciCore(null,serviceModel.getBankPublicKey(),requestId, beneRegisterReqStr, url, cibApiKey,
                    IciciPropertyConstants.ICICI_M2P_4096KEY);

            logger.info("Actual Response for Bene Addition :: " + actualResponseStr);

            if (actualResponseStr != null) {
                BeneficiaryAdditionResponse actualResponse = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(actualResponseStr, BeneficiaryAdditionResponse.class);
                if (actualResponse.getResponse() != null
                        && actualResponse.getResponse().toUpperCase().contains("FAIL")) {

                    if ((actualResponse.getMessage().toLowerCase().contains("counterparty already exists")
                            && actualResponse.getErrorCode().equals("100340"))
                            || (actualResponse.getMessage().toLowerCase().contains("bene user already registered")
                            && actualResponse.getErrorCode().equals("999590"))) {
                        logger.info(actualResponse.getMessage());
                        logger.info("Bene already present in bank system and hence returning true to add Bene in YAP system!!");
                        responseOutDto.setStatus(true);
                        responseOutDto.setDescription("Beneficiary Added!!");
                    } else {
                        throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
                                "Beneficiary Addition Failed" , null, Locale.US, null, null,ProductConstants.BENEFICIARY_ADDITION);
                    }

                } else {
                    if (actualResponse.getResponse() != null
                            && actualResponse.getResponse().toUpperCase().contains("SUCCESS")) {
                        responseOutDto.setBeneficiaryId(actualResponse.getBeneId());
                        responseOutDto.setStatus(true);
                        responseOutDto.setDescription(actualResponse.getMessage());
                    } else {
                        throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
                                "Beneficiary Addition Failed" , null, Locale.US, null, null,ProductConstants.BENEFICIARY_ADDITION);
                    }
                }
            } else {
                throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
                        "Beneficiary Addition Failed" , null, Locale.US, null, null,ProductConstants.BENEFICIARY_ADDITION);
            }
        } catch (NeoException nex) {
            logger.error(nex.getMessage());
            throw nex;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
                    "Beneficiary Addition Failed" , null, Locale.US, null, null,ProductConstants.BENEFICIARY_ADDITION);
        }
        return responseOutDto;
    }

    public FundTransferResponseDto neftIncrementalStatusCheck(String utrNo, String tenant) throws NeoException {

        FundTransferResponseDto responseDto = new FundTransferResponseDto();
        StringBuilder sw = new StringBuilder();
        try {

            String requestId = TxnIdGenerator.getReferenceNumber();
            HttpHeaders headers = new HttpHeaders();

            NeoBusinessCustomField corpIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_ID,
                    tenant);
            NeoBusinessCustomField corpUserField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_USER,
                    tenant);
            NeoBusinessCustomField aggrIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_AGGR_ID,
                    tenant);
            NeoBusinessCustomField urnField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_URN,
                    tenant);
            NeoBusinessCustomField cibApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CIB_APIKEY,
                    tenant);
            String cibApiKey = (cibApiKeyField != null) ? cibApiKeyField.getFieldValue() : IciciPropertyConstants.CIB_API_KEY;
            String corpId = (corpIdField != null) ? corpIdField.getFieldValue() : "";
            String corpUser = (corpUserField != null) ? corpUserField.getFieldValue() : "";
            String aggrID = (aggrIdField != null) ? aggrIdField.getFieldValue() : "";
            String urn = (urnField != null) ? urnField.getFieldValue() : "";


            NeftRtgsFTStatusCheckRequest statusCheckRequest = NeftRtgsFTStatusCheckRequest.builder()
                    .aggrId(aggrID)
                    .corpId(corpId)
                    .urn(urn)
                    .userId(corpUser)
                    .utr(utrNo)
                    .build();

            sw.append(new ObjectMapper().writeValueAsString(statusCheckRequest));

            logger.info("Actual Payload for NEFT Incremental status check :: " + sw.toString());

            MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(tenant);

            PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
                    ProductConstants.FT_INCREMENTAL_STATUS_CHECK);

            String responseBody = callIciciCore(headers,serviceModel.getBankPublicKey(),requestId, sw.toString(), serviceModel.getServiceEndpoint(), cibApiKey,
                    IciciPropertyConstants.ICICI_M2P_4096KEY);

            logger.info("Actual Resposne for NEFT Incremental status check :: " + responseBody);

            NeftRtgsFTStatusCheckResponse paymentStatusResponse = new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .readValue(responseBody, NeftRtgsFTStatusCheckResponse.class);

            if (paymentStatusResponse != null) {
                if (paymentStatusResponse.getResponse().equalsIgnoreCase("success")) {
                    responseDto.setBankReferenceNo(paymentStatusResponse.getUtr());
                    responseDto.setStatus(IciciPropertyConstants.SUCCESS_MSG);
                    responseDto.setDescription(paymentStatusResponse.getStatus());
                } else if (paymentStatusResponse.getStatus().equalsIgnoreCase("PENDING FOR PROCESSING")) {
                    responseDto.setBankReferenceNo(paymentStatusResponse.getUtr());
                    responseDto.setStatus(IciciPropertyConstants.INPROGRESS_MSG);
                    responseDto.setDescription(paymentStatusResponse.getTxnResponse());
                } else {
                    responseDto.setStatus(IciciPropertyConstants.FAILURE_MSG);
                    responseDto.setDescription(
                            (paymentStatusResponse.getTxnResponse() != null) ? paymentStatusResponse.getTxnResponse() : "Transaction Failure");
                }
            } else {
                responseDto.setStatus(IciciPropertyConstants.FAILURE_MSG);
                responseDto.setDescription("Transaction Failure");
            }

            logger.info("Response Received for Fund Transfer Status Check :: " + responseBody);

            return responseDto;

        } catch (Exception e) {
            logger.m2pdebug("Exception Occured :: ", e);
            throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
                    "NEFT Incremental Status Check Failed" , null, Locale.US, null, null,ProductConstants.FT_STATUS_CHECK);

        }
    }

    private String poolAcFundTransfer(String utrNo, String amount, String narration, String tenant, InternalFTTypes transferType) throws NeoException {

        StringBuilder sw = new StringBuilder();
        String bankTxnId="";
        String txnStatus="";
        String debitApiKey="";
        try {

            NeoFundTransfer neoFundTransfer;
            NeoBusinessCustomField debitApiKeyField = null;
            String requestId = TxnIdGenerator.getReferenceNumber();
            HttpHeaders headers = new HttpHeaders();

            neoFundTransfer = fundTransferDao.findByExternalTransactionIdAndTenant(utrNo,TenantContextHolder.getNeoTenant());

            if (neoFundTransfer == null) {
                neoFundTransfer=new NeoFundTransfer();
                neoFundTransfer.setExternalTransactionId(utrNo);
                neoFundTransfer.setBeneficiaryAccountNo(transferType.name());
                neoFundTransfer.setTenant(TenantContextHolder.getNeoTenant());
                neoFundTransfer.setTransactionOrigin("INTERNAL");
                neoFundTransfer.setTransactionAmount(amount);
                neoFundTransfer.setDescription(narration);
                fundTransferDao.save(neoFundTransfer);
            }
            logger.info("Initiating internal fund Transfer..." + transferType);
            if (InternalFTTypes.POOL_DR_NODAL_CR.equals(transferType)) {
                debitApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_POOLDR_NODALCR_APIKEY,
                        tenant);
            } else if (InternalFTTypes.POOL_DR_CURR_CR.equals(transferType)) {
                debitApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_POOLDR_CURRCR_APIKEY,
                        tenant);
            } else if (InternalFTTypes.POOL_DR_ESCROW_CR.equals(transferType)) {
                debitApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_POOLDR_ESCROWCR_APIKEY,
                        tenant);
            } else if (InternalFTTypes.NODAL_DR_POOL_CR.equals(transferType)) {
                debitApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_NODALDR_POOLCR_APIKEY,
                        tenant);
            } else if (InternalFTTypes.ESCROW_DR_POOL_CR.equals(transferType)) {
                debitApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_ESCROWDR_POOLCR_APIKEY,
                        tenant);
            } else if (InternalFTTypes.CURR_DR_POOL_CR.equals(transferType)) {
                debitApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CURRDR_POOLCR_APIKEY,
                        tenant);
            }

            debitApiKey= (debitApiKeyField!=null)?debitApiKeyField.getFieldValue():"";

            double txnAmount;
            txnAmount = Double.parseDouble(amount) * 100;

            Date date = new Date();
            String localDateTime = new SimpleDateFormat("yyyyMMddHHmmss").format(date);

            bankTxnId= TxnIdGenerator.getReferenceNumber(12);

            Map<String, String> requestMap = new HashMap<>();
            requestMap.put("UTR", bankTxnId);
            requestMap.put("TransactionAmount", String.valueOf((int) txnAmount));
            requestMap.put("Narration", narration);
            requestMap.put("TransactionDateTime", localDateTime);

            sw.append(new ObjectMapper().writeValueAsString(requestMap));

            logger.info("Actual Payload :: " + sw.toString());

            MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(tenant);

            PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
                    ProductConstants.FT_POOLAC_DEBITCREDIT);

            String responseBody = callIciciCore(headers,serviceModel.getBankPublicKey(),requestId, sw.toString(), serviceModel.getServiceEndpoint(), debitApiKey,
                    IciciPropertyConstants.ICICI_M2P_4096KEY);

            logger.info("Response Received for Internal Fund Transfer :: " + responseBody);

            Map<String, String> paymentResponse = new ObjectMapper().readValue(responseBody, Map.class);

            if (paymentResponse != null && paymentResponse.get("Status").equalsIgnoreCase("success")) {
                txnStatus= IciciPropertyConstants.SUCCESS_MSG;
            } else {
                txnStatus= IciciPropertyConstants.FAILURE_MSG;
            }

            neoFundTransfer.setBankTransactionId(bankTxnId);
            neoFundTransfer.setTransactionStatus(txnStatus);
            neoFundTransfer.setTransactionType(transferType.name());
            fundTransferDao.save(neoFundTransfer);
            
            return txnStatus;
        } catch (Exception e) {
            logger.info("Exception Occurred :: ", e);
            throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
                    "Internal Fund Transfer Failed" , null, Locale.US, null, null,transferType.name());

        }
    }

    @Override
    public Boolean upiCollectRefund(String orgTxnId) throws NeoException {
        Boolean response = false;
        String txnRefno = "";
        String refundStatus = "";
        double txnAmount = 0D;
        try {
            UpiCollectRequest collectRequest = upiCollectRequestDao.findByTransferUniqueNumber(orgTxnId);

            PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId("ICICI",
                    ProductConstants.UPI_COLLECT_REFUND);

            NeoBusinessCustomField refundApiKeyField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_UPI_REFUND_APIKEY,
                    "ICICI");

            String requestId = TxnIdGenerator.getReferenceNumber();
            String url = serviceModel.getServiceEndpoint() + collectRequest.getMerchantId();
            txnAmount = Double.parseDouble(collectRequest.getAmount());
            txnRefno = TxnIdGenerator.getReferenceNumber();

            UpiCollectRefundRequest refundReq = UpiCollectRefundRequest.builder()
                    .merchantId(collectRequest.getMerchantId())
                    .merchantTranId(txnRefno)
                    .note(collectRequest.getDescription())
                    .onlineRefund("Y")
                    .originalBankRRN(collectRequest.getTransferUniqueNumber())
                    .refundAmount(String.format("%.2f", txnAmount))
                    .originalmerchantTranId(collectRequest.getMerchantTranId())
                    .terminalId("5411")
                    .subMerchantId(collectRequest.getSubMerchantId())
                    .payeeVA(collectRequest.getPayerVpa())
                    .build();
            String refundReqStr = new ObjectMapper().writeValueAsString(refundReq);

            logger.info("Actual Payload for Refund :: " + refundReqStr);

            String refundResStr = callIciciCoreAsymc(null,serviceModel.getBankPublicKey(),requestId, refundReqStr, url, refundApiKeyField.getFieldValue(),
                    IciciPropertyConstants.ICICI_M2P_4096KEY);

            logger.info("Actual Response for Refund :: " + refundResStr);

            if (refundResStr != null) {
                UpiCollectRefundResponse refundRes = new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(refundResStr, UpiCollectRefundResponse.class);
                if (refundRes != null && "SUCCESS".equalsIgnoreCase(refundRes.getStatus())) {
                    refundStatus = refundRes.getStatus();
                    collectRequest.setRefund_bankTid(refundRes.getOriginalBankRRN());
                    response = true;
                    logger.info("Refund Success");
                } else {
                    refundStatus = "Failure";
                }
            } else {
                refundStatus = "Failure";
            }

            collectRequest.setRefund_txn_id(txnRefno);
            collectRequest.setRefund_status(refundStatus);
            upiCollectRequestDao.save(collectRequest);

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
                    "UPICOLLECT Refund Failed" , null, Locale.US, null, null,ProductConstants.UPI_COLLECT_REFUND);

        }
        return response;
    }

    @Override
    public AccountModificationResponseDto savingsAccountModification(AccountModificationRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public AccountClosureResponseDto tdAccountClosure(AccountClosureRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public AccountInquiryResponseDto tdAccountInquiry(AccountInquiryRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public KYCBioResponseDto eKycBioValidate(KYCBioRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public KYCBioResponseDto getDemographicData(KYCBioRequestDto request, String kycType) throws NeoException {
        return null;
    }

    @Override
    public TDAcctCloseTrailResponseDto tdTrailAccountClosure(TdAccountTrailCloseRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public AccountInquiryResponseDto savingsAccountInquiry(AccountInquiryRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public FundTransferResponseDto fundTransferWithOtp(FundTransferRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public FundTransferResponseDto fundTransferValidateOtp(FundTransferRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public KYCBioResponseDto aadharXmlInvoke(RegistrationRequestV2Dto request) throws NeoException {
        return null;
    }

    @Override
    public KYCBioResponseDto validateAadhaarXml(AadhaarOtpRequest request) throws NeoException {
        return null;
    }

    @Override
    public AccountInquiryResponseDto fetchAccountDetails(AccountInquiryRequestDto request) throws NeoException {
        return null;
    }


    @Override
    public RegistrationResponseDto retailCifCreation(RegistrationRequestV2Dto request) throws NeoException {
        return null;
    }

    @Override
    public AccountCreationResponseDto savingsAccountCreation(AccountCreationRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public BalanceCheckResponseDto fetchAccountBalance(BalanceCheckRequest request) throws NeoException {

        return null;
    }

    @Override
    public FetchAccountStatemetResponse fetchAccountStatement(FetchAccountStatementRequest request)
            throws NeoException {

        return null;
    }

    @Override
    public AccountCreationResponseDto termDepositCreation(AccountCreationRequestDto request) throws NeoException {

        return null;
    }

    @Override
    public AadhaarOtpResponse generateOtpAadhaar(AadhaarOtpRequest request) throws NeoException {

        return null;
    }

    @Override
    public AadhaarOtpResponse validateOtpAadhaar(AadhaarOtpRequest request) throws NeoException {

        return null;
    }

    @Override
    public PanValidationResponse validatePan(PanValidationRequest request) throws NeoException {

        return null;
    }

    @Override
    public RegistrationResponseDto register(RegistrationRequestV2Dto request) throws NeoException {

        return null;
    }

    @Override
    public RegistrationResponseDto addProduct(RegistrationRequestV2Dto request) throws NeoException {

        return null;
    }

    @Override
    public KYCBioResponseDto bioEKycOneTimeToken(RegistrationRequestV2Dto request, String eKycRefnum) throws NeoException {
        return null;
    }

	@Override
	public NeoPayResponse upiPay(NeoPayRequest request) throws NeoException {
		return null;
	}

	@Override
	public ValidateVpaRes validateVpa(ValidateVpaReq request) throws NeoException {
		return null;
	}

	@Override
	public MerchantOBRes onboardSubMerchant(MerchantOBReq request) throws NeoException {
		return null;
	}

}
