package com.neo.aggregator.serviceimpl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.dto.FundTransferRequestDto;
import com.neo.aggregator.dto.FundTransferResponseDto;
import com.neo.aggregator.dto.NeoResponse;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.service.AggregatorInterfaceService;
import com.neo.aggregator.service.AggregatorService;
import com.neo.aggregator.service.NeoQueueReceiverService;
import com.neo.aggregator.service.RemoteService;
import com.neo.aggregator.utility.CommonService;
import com.neo.aggregator.utility.TenantContextHolder;
import com.neo.aggregator.utility.Utils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Component
public class NeoQueueReceiverServiceImpl implements NeoQueueReceiverService {

    @Value("${neo.agg.core.url}")
    private String coreUrl;

    @Value("${neo.agg.local.url}")
    private String localUrl;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    AggregatorInterfaceService aggregatorInterfaceService;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    RemoteService remoteService;

    @Autowired
    CommonService commonService;

    private static Log4j2Wrapper LOGGER = (Log4j2Wrapper) Log4j2LogManager.getLog(NeoQueueReceiverServiceImpl.class);

    @Override
    @JmsListener(destination = "neo.process.fundtransfer")
    public void processFundTransferRequest(String message) {
        try {

            FundTransferRequestDto request = objectMapper.readValue(message, FundTransferRequestDto.class);

            LOGGER.info("Request Received fundTransfer ::" + message);

            TenantContextHolder.setNeoTenant(request.getFromTenant());

            LOGGER.info("Neo-Tenant :: " + TenantContextHolder.getNeoTenant());

            AggregatorService service = aggregatorInterfaceService.findServiceBasedonTenant(request.getFromTenant());

            try {
                FundTransferResponseDto response = service.fundTransfer(request);

                LOGGER.info("Response for fundTransfer :: " + Utils.convertModelToString(response));

                if (response!=null && response.getStatus() != null) {
                    if (StringUtils.isNotEmpty(response.getBankReferenceNo())) {
                        updateRRN(request.getExternalTransactionId(), response.getBankReferenceNo());
                    }

                    LOGGER.info("Txn for Tenant :: " + TenantContextHolder.getNeoTenant() + " :: Extn Txn ID ::" + request.getExternalTransactionId());
                    LOGGER.info("Txn Status :: " + response.getStatus());

                    if (!(response.getStatus().equalsIgnoreCase("SUCCESS") ||
                            response.getStatus().equalsIgnoreCase("INITIATED") || response.getStatus().equalsIgnoreCase("IN PROGRESS")
                            || response.getStatus().equalsIgnoreCase("ON HOLD"))) {
                        handleFailedTxn(request);
                    }
                    commonService.sendNotificationInFundTransfer(request, response);
                } else {
                    handleFailedTxn(request);
                }
            } catch (Exception ftExcep) {
                LOGGER.error("Fund Transfer Error ::" + ftExcep);
                handleFailedTxn(request);
            }

        } catch (Exception e) {
            LOGGER.error("Error : ", e);
            LOGGER.error("Exception when calling Notification :: " + e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    private void handleFailedTxn(FundTransferRequestDto request) throws JsonProcessingException {
        NeoResponse neoResponse = null;
        String txnType = "";
        String amount;

        txnType = request.getTransactionType().equalsIgnoreCase("FT") ? "IFT" : request.getTransactionType();

        String fetchTxnUrl = coreUrl + "txn-manager/fetch/" + request.getExternalTransactionId();

        NeoResponse txn = postToCore(fetchTxnUrl, null, TenantContextHolder.getNeoTenant(), HttpMethod.GET);

        txnType += "_DEBIT";

        if (txn != null && txn.getResult() != null) {
            Map<String, String> txnMap = objectMapper
                    .readValue(objectMapper.writeValueAsString(txn.getResult()), Map.class);
            if (txnMap.containsKey("transaction")) {
                Map<String, String> transaction = objectMapper
                        .readValue(objectMapper.writeValueAsString(txnMap.get("transaction")), Map.class);
                amount = transaction.get("amount");

                String url = coreUrl + "txn-manager/chargeback";
                StringBuilder desc = new StringBuilder();
                desc.append("REVERSAL for ");
                desc.append(txnType);

                Map<String, String> requestMap = new HashMap<>();
                requestMap.put("externalTransactionId", request.getExternalTransactionId());
                requestMap.put("transactionType", txnType);
                requestMap.put("toEntityId", request.getEntityId());
                requestMap.put("amount", amount);
                requestMap.put("productId", "GENERAL");
                requestMap.put("description", desc.toString());

                neoResponse = postToCore(url, requestMap, TenantContextHolder.getNeoTenant(), HttpMethod.POST);
                if (neoResponse != null && neoResponse.getResult() != null) {
                    Map<String, String> coreResp = new ObjectMapper()
                            .configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
                            .readValue(objectMapper.writeValueAsString(neoResponse.getResult()), Map.class);

                    if (coreResp != null && coreResp.containsKey("txId")) {
                        LOGGER.info("Transaction Failed and refund credited to customer wallet.");
                    }

                }
            }
        }
    }

    private void updateRRN(String extnTxnId, String rrn) {

        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("extnTxnId", extnTxnId);
        requestMap.put("rrn", rrn);

        remoteService.fetchData(requestMap, localUrl + "/neo-lego/common/updateRrn",
                TenantContextHolder.getNeoTenant(), HttpMethod.POST);

    }

    private NeoResponse postToCore(String coreURL, Map<String, String> map, String tenant, HttpMethod method) {

        NeoResponse coreResp;
        ResponseEntity<String> resp;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("TENANT", tenant);

            ObjectMapper obj = new ObjectMapper();
            HttpEntity<String> entity = null;
            try {

                String payload = "";
                if (map != null)
                    payload = obj.writeValueAsString(map);

                LOGGER.info("Core call :: " + coreURL + " Payload :: " + payload);
                entity = new HttpEntity<>(payload, headers);
            } catch (JsonProcessingException e) {
                LOGGER.info("Error : ", e);
            }

            resp = restTemplate.exchange(coreURL, method, entity, String.class);

        } catch (HttpStatusCodeException e) {
            resp = ResponseEntity.status(e.getRawStatusCode()).headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
        }

        LOGGER.info("Core Response :: " + resp.getBody());

        try {
            coreResp = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .readValue(resp.getBody(), NeoResponse.class);
            LOGGER.info("Core Response Mapping Success");
        } catch (Exception e) {
            LOGGER.info("Core Response Mapping failed");
            LOGGER.info(e.getMessage());
            return new NeoResponse("false", null, null);
        }

        return coreResp;
    }
}

