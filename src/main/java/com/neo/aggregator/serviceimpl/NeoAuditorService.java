package com.neo.aggregator.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.neo.aggregator.dao.NeoAuditDao;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.NeoAudit;

@Service
public class NeoAuditorService {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(NeoAuditorService.class);

	@Autowired
	private NeoAuditDao auditRepo;

	@Async
	public void audit(String entityId, String bankId, String partnerId, String description, String productName,
			Boolean status) {

		NeoAudit model = new NeoAudit();

		try {

			model.setApi(productName);
			model.setBankId(bankId);
			model.setPartnerName(partnerId);
			model.setDescription(description);
			model.setEntityId(entityId);
			model.setStatus(status);

			auditRepo.save(model);

		} catch (Exception e) {
			logger.info("Exception ex : " + e.getLocalizedMessage());
			logger.info("Exception occured when saving model");
		}
	}

}
