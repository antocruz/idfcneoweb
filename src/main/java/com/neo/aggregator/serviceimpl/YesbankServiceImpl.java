package com.neo.aggregator.serviceimpl;

import java.io.IOException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.KeyManagerFactory;

import com.upi.merchanttoolkit.security.UPISecurity;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.transport.WebServiceMessageSender;
import org.springframework.ws.transport.http.HttpsUrlConnectionMessageSender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.config.SoapRequestHeaderModifier;
import com.neo.aggregator.constant.FundTransferTypes;
import com.neo.aggregator.constant.NeoCustomFieldConstants;
import com.neo.aggregator.dao.MasterConfigDao;
import com.neo.aggregator.dao.MerchantOnBoardingDao;
import com.neo.aggregator.dao.NeoFundTransferDao;
import com.neo.aggregator.dao.UpiFundTransferDao;
import com.neo.aggregator.dto.AadhaarOtpRequest;
import com.neo.aggregator.dto.AadhaarOtpResponse;
import com.neo.aggregator.dto.AccountClosureRequestDto;
import com.neo.aggregator.dto.AccountClosureResponseDto;
import com.neo.aggregator.dto.AccountCreationRequestDto;
import com.neo.aggregator.dto.AccountCreationResponseDto;
import com.neo.aggregator.dto.AccountInquiryRequestDto;
import com.neo.aggregator.dto.AccountInquiryResponseDto;
import com.neo.aggregator.dto.AccountModificationRequestDto;
import com.neo.aggregator.dto.AccountModificationResponseDto;
import com.neo.aggregator.dto.BalanceCheckRequest;
import com.neo.aggregator.dto.BalanceCheckResponseDto;
import com.neo.aggregator.dto.BeneficiaryManagementReqDto;
import com.neo.aggregator.dto.BeneficiaryManagementResDto;
import com.neo.aggregator.dto.CustomerDedupCheckResponseDto;
import com.neo.aggregator.dto.FetchAccountStatementRequest;
import com.neo.aggregator.dto.FetchAccountStatemetResponse;
import com.neo.aggregator.dto.FundTransferRequestDto;
import com.neo.aggregator.dto.FundTransferResponseDto;
import com.neo.aggregator.dto.KYCBioRequestDto;
import com.neo.aggregator.dto.KYCBioResponseDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.PanValidationRequest;
import com.neo.aggregator.dto.PanValidationResponse;
import com.neo.aggregator.dto.PayLaterRequestDto;
import com.neo.aggregator.dto.PayLaterResponseDto;
import com.neo.aggregator.dto.RegistrationRequestDto;
import com.neo.aggregator.dto.RegistrationRequestV2Dto;
import com.neo.aggregator.dto.RegistrationResponseDto;
import com.neo.aggregator.dto.TDAcctCloseTrailResponseDto;
import com.neo.aggregator.dto.TdAccountTrailCloseRequestDto;
import com.neo.aggregator.dto.BalanceCheckResponseDto.AccountBalance;
import com.neo.aggregator.dto.v2.AddressDto;
import com.neo.aggregator.dto.yesbank.MerchantOBReq;
import com.neo.aggregator.dto.yesbank.MerchantOBRes;
import com.neo.aggregator.dto.yesbank.NeoPayRequest;
import com.neo.aggregator.dto.yesbank.NeoPayResponse;
import com.neo.aggregator.dto.yesbank.PayRequest;
import com.neo.aggregator.dto.yesbank.PayResponse;
import com.neo.aggregator.dto.yesbank.ValidateVpaReq;
import com.neo.aggregator.dto.yesbank.ValidateVpaRes;
import com.neo.aggregator.model.MasterConfiguration;
import com.neo.aggregator.model.MerchantOBReqRes;
import com.neo.aggregator.model.NeoBusinessCustomField;
import com.neo.aggregator.model.NeoFundTransfer;
import com.neo.aggregator.model.UpiFundTransfer;
import com.neo.aggregator.service.AggregatorService;
import com.neo.aggregator.service.NeoBusinessCustomFieldService;
import com.neo.aggregator.utility.CommonService;
import com.neo.aggregator.utility.TenantContextHolder;
import com.neo.aggregator.utility.UpiUtility;
import com.quantiguous.services.AddressType;
import com.quantiguous.services.BeneficiaryDetailType;
import com.quantiguous.services.BeneficiaryType;
import com.quantiguous.services.ContactType;
import com.quantiguous.services.CurrencyCodeType;
import com.quantiguous.services.GetBalance;
import com.quantiguous.services.GetBalanceResponse;
import com.quantiguous.services.NameType;
import com.quantiguous.services.ObjectFactory;
import com.quantiguous.services.Transfer;
import com.quantiguous.services.TransferResponse;
import com.quantiguous.services.TransferTypeType;


@Service
@Qualifier("YesbankServiceImpl")
public class YesbankServiceImpl implements AggregatorService,InitializingBean {

    static final Logger log = LoggerFactory.getLogger(YesbankServiceImpl.class);

    @Value("${upi.merchant.id}")
    String upiMerchantId;

    @Value("${upi.merchant.enc.key}")
    String upiMerchantEncKey;

    @Value("${upi.pay.req.url}")
    String mePayServerReq;
    
    @Value("${upi.pay.merchant.url}")
    String onBoardSMUrl; //submerchant on-boarding url
    
    @Value("${upi.pay.validatevpa.url}")
    String validateVpaUrl;

    @Autowired
    CommonService commonService;
    
    @Autowired
    UpiFundTransferDao upiFtDao;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    UpiUtility upiUtils;
    
    @Autowired
    MasterConfigDao masterConfig;
    
    @Autowired
    MerchantOnBoardingDao merchantDao;
    
    @Autowired
	private ApplicationContext context;
    
	@Value("${yes.endpoint.url}")
    private String url;
	
	@Value("${neo.yes.keystore.path}")
	private Resource keyStoreFile;

	@Value("${neo.yes.keystore.password}")
	private String keyStorePassword;
    
	@Autowired
	private NeoFundTransferDao fundTransferDao;
	   
    @Autowired
	private NeoBusinessCustomFieldService neoBusinessCustomerField;
    
    @Override
    public RegistrationResponseDto retailCifCreation(RegistrationRequestV2Dto request) throws NeoException {
        return null;
    }

    @Override
    public AccountCreationResponseDto savingsAccountCreation(AccountCreationRequestDto request) throws NeoException {
        return null;
    }

    @Override
	public BalanceCheckResponseDto fetchAccountBalance(BalanceCheckRequest request) throws NeoException {
		WebServiceTemplate sc = context.getBean(WebServiceTemplate.class);
		try {
			sc.setMessageSender(sslConfig());
		} catch (Exception e) {
			e.printStackTrace();
		}

		NeoBusinessCustomField yblUserName = neoBusinessCustomerField.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_USERNAME, TenantContextHolder.getNeoTenant());
		NeoBusinessCustomField yblPassword = neoBusinessCustomerField.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_PASSWORD, TenantContextHolder.getNeoTenant());
		NeoBusinessCustomField yblappId = neoBusinessCustomerField.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_APPID ,TenantContextHolder.getNeoTenant());
		NeoBusinessCustomField yblVersion = neoBusinessCustomerField.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_VERSION, TenantContextHolder.getNeoTenant());
		NeoBusinessCustomField yblCustomerId = neoBusinessCustomerField.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_CUSTOMERID, TenantContextHolder.getNeoTenant());
		NeoBusinessCustomField debitAccountNumber = neoBusinessCustomerField.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_ACCOUNT_NO, TenantContextHolder.getNeoTenant());
		NeoBusinessCustomField yblClientId = neoBusinessCustomerField.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_CLIENT_ID, TenantContextHolder.getNeoTenant());
		NeoBusinessCustomField yblClientSecret = neoBusinessCustomerField.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_CLIENT_SECRET, TenantContextHolder.getNeoTenant());
		
		ObjectFactory objFactory = new ObjectFactory();
		GetBalance bal = objFactory.createGetBalance();
		bal.setAppID(yblappId.getFieldValue());
		bal.setVersion(yblVersion.getFieldValue());
		bal.setAccountNumber(debitAccountNumber.getFieldValue());
		bal.setCustomerID(yblCustomerId.getFieldValue());
		ObjectMapper m = new ObjectMapper();
		GetBalanceResponse response = objFactory.createGetBalanceResponse();
		response = (GetBalanceResponse) sc.marshalSendAndReceive(this.url, bal, new SoapRequestHeaderModifier(
				this.getBasicAuthorization(yblUserName.getFieldValue(), yblPassword.getFieldValue()), yblClientId.getFieldValue(),yblClientSecret.getFieldValue()));
		try {
			log.info("Response :: {}", m.writeValueAsString(response));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		BalanceCheckResponseDto balance = new BalanceCheckResponseDto();

		List<AccountBalance> accountBalance = new ArrayList<>();
		AccountBalance accountBal = new AccountBalance();
		accountBal.setBalance(String.valueOf(response.getAccountBalanceAmount()));
		accountBalance.add(accountBal);
		balance.setCurrency(response.getAccountCurrencyCode().name());

		return balance;
	}

    @Override
    public FetchAccountStatemetResponse fetchAccountStatement(FetchAccountStatementRequest request) throws NeoException {
        return null;
    }

    @Override
    public AccountCreationResponseDto termDepositCreation(AccountCreationRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public AadhaarOtpResponse generateOtpAadhaar(AadhaarOtpRequest request) throws NeoException {
        return null;
    }

    @Override
    public AadhaarOtpResponse validateOtpAadhaar(AadhaarOtpRequest request) throws NeoException {
        return null;
    }

    @Override
    public PanValidationResponse validatePan(PanValidationRequest request) throws NeoException {
        return null;
    }

    @Override
    public RegistrationResponseDto register(RegistrationRequestV2Dto request) throws NeoException {
        return null;
    }

    @Override
    public RegistrationResponseDto addProduct(RegistrationRequestV2Dto request) throws NeoException {
        return null;
    }

    @Override
    public FundTransferResponseDto paymentStatusCheck(FundTransferRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public AccountModificationResponseDto savingsAccountModification(AccountModificationRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public AccountClosureResponseDto tdAccountClosure(AccountClosureRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public AccountInquiryResponseDto tdAccountInquiry(AccountInquiryRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public KYCBioResponseDto bioEKycOneTimeToken(RegistrationRequestV2Dto request, String eKycRefnum) throws NeoException {
        return null;
    }

    @Override
    public KYCBioResponseDto eKycBioValidate(KYCBioRequestDto request) throws NeoException {
        return null;
    }

    public KYCBioResponseDto getDemographicData(KYCBioRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public TDAcctCloseTrailResponseDto tdTrailAccountClosure(TdAccountTrailCloseRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public AccountInquiryResponseDto savingsAccountInquiry(AccountInquiryRequestDto request) throws NeoException {
        return null;
    }

    @Override
    public KYCBioResponseDto aadharXmlInvoke(RegistrationRequestV2Dto request) throws NeoException {
        return null;
    }

    @Override
    public KYCBioResponseDto validateAadhaarXml(AadhaarOtpRequest request) throws NeoException {
        return null;
    }

    @Override
    public AccountInquiryResponseDto fetchAccountDetails(AccountInquiryRequestDto request) throws NeoException {
        return null;
    }
    
	@Override
	public FundTransferResponseDto fundTransferWithOtp(FundTransferRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FundTransferResponseDto fundTransferValidateOtp(FundTransferRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ValidateVpaRes validateVpa(ValidateVpaReq request) throws NeoException {
		log.info("Validate vpa request :: Yesbank:: {}", request);
		try {
			MasterConfiguration config = masterConfig.findByPartnerId(TenantContextHolder.getNeoTenant());
			request.setMerchantId(upiMerchantId);
			Map<String, String> reqMap = mapper.convertValue(request, new TypeReference<Map<String, String>>() {
			});
			String reqStdMsg = commonService.pipeSptdMsg(reqMap);
			UPISecurity upiSecurity = new UPISecurity();
			String encryptedMsg = upiSecurity.encrypt(reqStdMsg, upiMerchantEncKey);
			PayRequest payRequest = new PayRequest(encryptedMsg, upiMerchantId);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("X-IBM-Client-ID", "365fcaae-c8c1-4bc4-8eb2-1c0d76b74c05");
			headers.add("X-IBM-Client-Secret", "I0jU2gU0rW1kR6iS7yV6oY4jO2wF8hM0rD8rY5pR7jG2dQ0mJ0");
			String respStr = commonService.exchangeForEntityWithHeaders(mapper.writeValueAsString(payRequest), validateVpaUrl, HttpMethod.POST
					, headers, true, config.getSslAlias(), config.getSslEnabled());
			if(respStr != null) {
				String respDecStr = upiSecurity.decrypt(respStr, upiMerchantEncKey);
				ValidateVpaRes vRes = mapper.readValue(respDecStr, ValidateVpaRes.class);
				return vRes;
			}else {
				ValidateVpaRes vRes = new ValidateVpaRes();
				vRes.setStatus("01");
				vRes.setStatusDesc("Validate Vpa request failed");
			}
			return null;
		} catch (Exception e) {
			log.error("Exception in validate vpa", e);
			throw new NeoException("System Error - ".concat(e.getMessage()));
		}
	}
    
	public NeoPayResponse upiPay(NeoPayRequest request) throws NeoException {
		
		log.info("UPI Fund transfer request :: Yesbank :: {}", request);
		//global values for entity model
		UpiFundTransfer ft = null;
		try {
			
			MasterConfiguration config = masterConfig.findByPartnerId(TenantContextHolder.getNeoTenant());
			
			request.setMerchantId(upiMerchantId); //Yesbank merchant id unique for M2P
			
			/*
			 * Setting txn type relevant to YESBANK UPI Server spec 1.6_2
			 * */
			FundTransferTypes type = FundTransferTypes.valueOf(request.getTxnType());
			request.setTxnType(type.getTransactionType());
			request.setTransferType(type.getTransferType());
			request.setPaymentType("P2P");
			request.setPayeeVpaType(type.getVpaType());
			
			
			TypeReference<LinkedHashMap<String, String>> ref = new TypeReference<LinkedHashMap<String, String>>() {
			};
			LinkedHashMap<String, String> reqMap = mapper.convertValue(request, ref);
			String pipeStdMsg = commonService.pipeSptdMsg(reqMap);
			UPISecurity sec = new UPISecurity();
			String encMsg = sec.encrypt(pipeStdMsg, upiMerchantEncKey);
			PayRequest payReq = new PayRequest(encMsg, upiMerchantId);
			ft = mapper.convertValue(request, UpiFundTransfer.class);
			
			upiFtDao.save(ft); //saving ft req before sending it to bank

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("X-IBM-Client-ID", "365fcaae-c8c1-4bc4-8eb2-1c0d76b74c05");
			headers.add("X-IBM-Client-Secret", "I0jU2gU0rW1kR6iS7yV6oY4jO2wF8hM0rD8rY5pR7jG2dQ0mJ0");
			
			String PayReqString = mapper.writeValueAsString(payReq);
			String postRes = commonService.exchangeForEntityWithHeaders(PayReqString, mePayServerReq, 
					HttpMethod.POST, headers, true, config.getSslAlias(), config.getSslEnabled());
			if(postRes != null) {
				String resPipeStdMsg = sec.decrypt(postRes, upiMerchantEncKey);
				Map<String, String> respMap = mapper.convertValue(new NeoPayResponse(), ref);
				respMap = commonService.decodePipeStdMsg(resPipeStdMsg, respMap);
				NeoPayResponse nResp = mapper.convertValue(respMap, NeoPayResponse.class);
				ft.setYblTxnId(nResp.getYblTxnId());
				ft.setResponseCode(nResp.getResponseCode());
				ft.setStatusCode(nResp.getStatusCode());
				ft.setNpciTxnId(nResp.getNpciTxnId());
				ft.setErrorCode(nResp.getErrorCode());
				ft.setResponseErrorCode(nResp.getResponseErrorCode());
				ft.setTxnStatus(nResp.getStatusDesc());
				return nResp;
			}else {
				NeoPayResponse nResp = new NeoPayResponse();
				nResp.setStatusCode("01");
				nResp.setStatusDesc("FundTransfer failed");
				return nResp;
			}
		} catch (Exception e) {
			if(ft != null) {
				ft.setErrorMsg(e.getMessage());
				ft.setTxnStatus("FAILED");
			}
			log.error("Exception in Upi Pay", e);
			throw new NeoException("System Error - ".concat(e.getMessage()));
		} finally {
			if(ft != null)
				upiFtDao.save(ft); //saving finalResps
		}
	}
	
	public MerchantOBRes onboardSubMerchant(MerchantOBReq request) throws NeoException{
		
		log.info("Upi submerchant onboard request {}",request);
		MerchantOBReqRes ob = null;
		try {
			MasterConfiguration config = masterConfig.findByPartnerId(TenantContextHolder.getNeoTenant());
			request.setPgMerchantId(upiMerchantId); // m2p merchant id(master)
			ob = mapper.convertValue(request, MerchantOBReqRes.class);
			String req = mapper.writeValueAsString(request);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("X-IBM-Client-ID", "365fcaae-c8c1-4bc4-8eb2-1c0d76b74c05");
			headers.add("X-IBM-Client-Secret", "I0jU2gU0rW1kR6iS7yV6oY4jO2wF8hM0rD8rY5pR7jG2dQ0mJ0");
			UPISecurity sec = new UPISecurity();
			String reqStr = sec.decrypt(req, upiMerchantEncKey);
			PayRequest pReq = new PayRequest(reqStr, upiMerchantId);
			String pReqStr = mapper.writeValueAsString(pReq);
			String respStr = commonService.exchangeForEntityWithHeaders(pReqStr,onBoardSMUrl , 
					HttpMethod.POST, headers, true, config.getSslAlias(), config.getSslEnabled());
			if(respStr != null) {
				String merRespStr = sec.decrypt(respStr, upiMerchantEncKey);
				PayResponse merResp = mapper.readValue(merRespStr,PayResponse.class);
				MerchantOBRes mResp = mapper.readValue(merResp.getResp(), MerchantOBRes.class);
				return mResp;
			}else {
				MerchantOBRes mRes = new MerchantOBRes();
				mRes.setStatus("01");
				mRes.setStatusDesc("Merchant onboarding failed");
				return mRes;
			}
		} catch (Exception e) {
			log.error("Exception in onboarding",e);
			throw new NeoException("System Error - ".concat(e.getMessage()));
		} finally {
			if(ob != null)
				merchantDao.save(ob);
		}
	}
	
	

	@Override
	public void afterPropertiesSet() throws Exception {
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);	
	}
	
	private WebServiceMessageSender sslConfig() throws Exception {
		
		log.info("Fetching JKS file for yes bank fund transfer Request");
        KeyStore ks = KeyStore.getInstance("JKS");
        ks.load(keyStoreFile.getInputStream(), keyStorePassword.toCharArray());
        
        try {
        	keyStoreFile.getInputStream().close();
        } catch (IOException e) {
        	log.info("Exception while fetching Key Store File {}", e.getMessage());
        }
        
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(ks, keyStorePassword.toCharArray());

        HttpsUrlConnectionMessageSender messageSender = new HttpsUrlConnectionMessageSender();
        messageSender.setKeyManagers(keyManagerFactory.getKeyManagers());
        messageSender.setHostnameVerifier((hostname, sslSession) -> {
            if (hostname.equals("localhost")) {
                return true;
            }
            return false;
        });
        log.info("SSL Handshake done successfully");
		return messageSender;
	}

	@Override
	public FundTransferResponseDto fundTransfer(FundTransferRequestDto req) throws NeoException {
		NeoFundTransfer neoFundTransfer;
		FundTransferResponseDto ftRes = new FundTransferResponseDto();
		ObjectMapper m = new ObjectMapper();
		log.info("Yes bank Fund Transfer Request {}", req);
		WebServiceTemplate sc = context.getBean(WebServiceTemplate.class);
		try {
			sc.setMessageSender(sslConfig());
		} catch (Exception e) {
			e.printStackTrace();
		}

		log.info("JKS File loaded successfull. processing the reqeuest");
		ObjectFactory objFactory = new ObjectFactory();
		NeoBusinessCustomField yblUserName = neoBusinessCustomerField
				.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_USERNAME, TenantContextHolder.getNeoTenant());
		NeoBusinessCustomField yblPassword = neoBusinessCustomerField
				.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_PASSWORD, TenantContextHolder.getNeoTenant());
		NeoBusinessCustomField yblappId = neoBusinessCustomerField
				.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_APPID, TenantContextHolder.getNeoTenant());
		NeoBusinessCustomField yblVersion = neoBusinessCustomerField
				.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_VERSION, TenantContextHolder.getNeoTenant());
		NeoBusinessCustomField yblCustomerId = neoBusinessCustomerField
				.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_CUSTOMERID, TenantContextHolder.getNeoTenant());
		NeoBusinessCustomField debitAccountNumber = neoBusinessCustomerField
				.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_ACCOUNT_NO, TenantContextHolder.getNeoTenant());
		NeoBusinessCustomField yblClientId = neoBusinessCustomerField
				.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_CLIENT_ID, TenantContextHolder.getNeoTenant());
		NeoBusinessCustomField yblSecret = neoBusinessCustomerField
				.findByFieldNameAndTenant(NeoCustomFieldConstants.YBL_CLIENT_SECRET, TenantContextHolder.getNeoTenant());

		Transfer transfer = objFactory.createTransfer();
		transfer.setVersion(yblVersion.getFieldValue());
		transfer.setUniqueRequestNo(req.getExternalTransactionId());

		BeneficiaryType bft = new BeneficiaryType();

		BeneficiaryDetailType bdtf = new BeneficiaryDetailType();
		bdtf.setBeneficiaryAccountNo(req.getToAccountNo());
		bdtf.setBeneficiaryIFSC(req.getBeneficiaryIfsc());
		bdtf.setBeneficiaryMobileNo(req.getBeneficiaryMobile());
		bdtf.setBeneficiaryMMID(req.getBeneficiaryMMID());

		NameType name = new NameType();
		name.setFullName(req.getBeneficiaryName());

		ContactType contact = new ContactType();
		contact.setEmailID(req.getBeneficiaryEmail());
		contact.setMobileNo(req.getBeneficiaryMobile());
		AddressDto addressDto = new AddressDto();
		addressDto = req.getAddress().get(0);
		AddressType address = new AddressType();
		address.setAddress1(addressDto.getAddress1());
		address.setAddress2(addressDto.getAddress2());
		address.setAddress3(addressDto.getAddress3());
		address.setCity(addressDto.getCity());
		address.setCountry(addressDto.getCountry());
		address.setPostalCode(addressDto.getPincode());
		address.setStateOrProvince(addressDto.getState());

		bdtf.setBeneficiaryName(name);
		bdtf.setBeneficiaryContact(contact);
		bdtf.setBeneficiaryAddress(address);

		bft.setBeneficiaryDetail(bdtf);

		transfer.setBeneficiary(bft);
		transfer.setAppID(yblappId.getFieldValue());
		transfer.setCustomerID(yblCustomerId.getFieldValue());
		transfer.setDebitAccountNo(debitAccountNumber.getFieldValue());

		switch (req.getTransactionType()) {

		case "IMPS":
			transfer.setTransferType(TransferTypeType.IMPS);
			break;
		case "RTGS":
			transfer.setTransferType(TransferTypeType.RTGS);
			break;
		case "FT":
			transfer.setTransferType(TransferTypeType.FT);
			break;
		case "NEFT":
			transfer.setTransferType(TransferTypeType.NEFT);
			break;
		case "ANY":
			transfer.setTransferType(TransferTypeType.ANY);
			break;

		default:
			log.info("Transfer Type did not match, Hence treating it as FT");
			transfer.setTransferType(TransferTypeType.FT);
		}

		transfer.setTransferCurrencyCode(CurrencyCodeType.INR);
		transfer.setRemitterToBeneficiaryInfo(req.getDescription());
		transfer.setTransferAmount(Float.parseFloat(req.getAmount()));

		TransferResponse response = objFactory.createTransferResponse();
		
		log.info("Sending Request ::");

		response = (TransferResponse) sc.marshalSendAndReceive(this.url, transfer, new SoapRequestHeaderModifier(
				this.getBasicAuthorization(yblUserName.getFieldValue(), yblPassword.getFieldValue()), yblClientId.getFieldValue(),yblSecret.getFieldValue()) );
		
		if(response == null) {
			log.info("Transfer Response is Null ::");
			ftRes.setStatus("FAILURE");
			return ftRes;
		}
		
		log.info("Transfer Respones Unique Response Number {}", response.getUniqueResponseNo());
		
		try {
			log.info("Response :: {}", m.writeValueAsString(response));
		} catch (JsonProcessingException e) {
			log.info("JSON Mapping Exception");
		}
		
		neoFundTransfer = new DozerBeanMapper().map(req, NeoFundTransfer.class);
		neoFundTransfer.setBeneficiaryAccountNo(req.getToAccountNo());
		neoFundTransfer.setTransactionAmount(req.getAmount());
		neoFundTransfer.setTenant(TenantContextHolder.getNeoTenant());
		neoFundTransfer.setRemitterAccountNo(debitAccountNumber.getFieldValue());
		
		if (response != null && response.getTransactionStatus().getStatusCode().equals("SENT_TO_BENEFICIARY")) {
			ftRes.setStatus("SUCCESS");
		} else {
			ftRes.setStatus("FAILURE");
		}
		ftRes.setBankReferenceNo(response.getUniqueResponseNo());
		ftRes.setDescription(response.getTransactionStatus().getReason());
		ftRes.setExternalTransactionId(req.getExternalTransactionId());

		neoFundTransfer.setTransactionStatus(response.getTransactionStatus().getStatusCode());
		neoFundTransfer.setBankTransactionId(response.getUniqueResponseNo());
		fundTransferDao.save(neoFundTransfer);

		return ftRes;
	}
	
	private String getBasicAuthorization(String user, String pass) {
		String source = user + ":" + pass;
		String retunVal = "Basic " + Base64.getUrlEncoder().encodeToString(source.getBytes());
		return retunVal;
	}

	@Override
	public KYCBioResponseDto getDemographicData(KYCBioRequestDto request, String kycType) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CustomerDedupCheckResponseDto dedupCheck(RegistrationRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterAccountDiscovery(PayLaterRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterAcctBalInquiry(PayLaterRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterOtpCreation(PayLaterRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterOtpVerification(PayLaterRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterDebitOrchestration(PayLaterRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BeneficiaryManagementResDto beneficiaryAddition(BeneficiaryManagementReqDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}
}
