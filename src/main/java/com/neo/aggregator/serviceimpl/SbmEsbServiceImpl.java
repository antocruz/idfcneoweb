package com.neo.aggregator.serviceimpl;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.SSLContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.bank.sbm.AddrDtls;
import com.neo.aggregator.bank.sbm.CustDtls;
import com.neo.aggregator.bank.sbm.DemographicData;
import com.neo.aggregator.bank.sbm.EkycResponseDto;
import com.neo.aggregator.bank.sbm.EntityDoctData;
import com.neo.aggregator.bank.sbm.Header;
import com.neo.aggregator.bank.sbm.PhoneEmailDtls;
import com.neo.aggregator.bank.sbm.PsychographMiscData;
import com.neo.aggregator.bank.sbm.PsychographicData;
import com.neo.aggregator.bank.sbm.RelatedDtls;
import com.neo.aggregator.bank.sbm.RetCustAddRequest;
import com.neo.aggregator.bank.sbm.RetCustAddRequestBody;
import com.neo.aggregator.bank.sbm.RetCustAddRq;
import com.neo.aggregator.bank.sbm.core.request.AcctLienAddRequestDto;
import com.neo.aggregator.bank.sbm.core.request.AcctLienAddRequestDto.Body.AcctLienAddRequest;
import com.neo.aggregator.bank.sbm.core.request.AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq;
import com.neo.aggregator.bank.sbm.core.request.AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.LienDtls;
import com.neo.aggregator.bank.sbm.core.request.AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.LienDtls.LienDt;
import com.neo.aggregator.bank.sbm.core.request.AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.LienDtls.NewLienAmt;
import com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto;
import com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest;
import com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq;
import com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorBankDtls;
import com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorBankDtls.NetworkIdentification;
import com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorDtls;
import com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.DebtorDtls;
import com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.DebtorDtls.AddressDetails;
import com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.RemittanceInfo;
import com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.RequestedAmount;
import com.neo.aggregator.bank.sbm.core.request.BalinqRequestDto;
import com.neo.aggregator.bank.sbm.core.request.BalinqRequestDto.Body.BalInqRequest;
import com.neo.aggregator.bank.sbm.core.request.BalinqRequestDto.Body.BalInqRequest.BalInqRq;
import com.neo.aggregator.bank.sbm.core.request.BalinqRequestDto.Body.BalInqRequest.BalInqRq.AcctId;
import com.neo.aggregator.bank.sbm.core.request.CreditAddRequestDto;
import com.neo.aggregator.bank.sbm.core.request.CreditAddRequestDto.Body.CreditAddRequest;
import com.neo.aggregator.bank.sbm.core.request.CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq;
import com.neo.aggregator.bank.sbm.core.request.CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq.CreditPartTrnRec;
import com.neo.aggregator.bank.sbm.core.request.DebitAddRequestDto;
import com.neo.aggregator.bank.sbm.core.request.DebitAddRequestDto.Body.DebitAddRequest;
import com.neo.aggregator.bank.sbm.core.request.DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq;
import com.neo.aggregator.bank.sbm.core.request.DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq.DebitPartTrnRec;
import com.neo.aggregator.bank.sbm.core.request.FetchAccountStatementRequestDto;
import com.neo.aggregator.bank.sbm.core.request.FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest;
import com.neo.aggregator.bank.sbm.core.request.FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest.PaginatedAccountTransactionCriteria;
import com.neo.aggregator.bank.sbm.core.request.FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest.PaginatedAccountTransactionCriteria.PaginationDetails;
import com.neo.aggregator.bank.sbm.core.request.FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest.PaginatedAccountTransactionCriteria.PaginationDetails.LastBalance;
import com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto;
import com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.XferTrnAddRequest;
import com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq;
import com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail;
import com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec;
import com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec.TrnAmt;
import com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnHdr;
import com.neo.aggregator.bank.sbm.core.request.MiniStatementRequestDto;
import com.neo.aggregator.bank.sbm.core.request.MiniStatementRequestDto.Body.GetMiniAccountStatementRequest;
import com.neo.aggregator.bank.sbm.core.request.MiniStatementRequestDto.Body.GetMiniAccountStatementRequest.AccountListElement;
import com.neo.aggregator.bank.sbm.core.request.RetCustAddRequestDto;
import com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto;
import com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto.Body;
import com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto.Body.SBAcctAddRequest;
import com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddCustomData;
import com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq;
import com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.CustId;
import com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.SBAcctGenInfo;
import com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.SBAcctId;
import com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.SBAcctId.AcctType;
import com.neo.aggregator.bank.sbm.core.request.SBAcctInquiryRequestDto;
import com.neo.aggregator.bank.sbm.core.request.SBAcctInquiryRequestDto.Body.SBAcctInqRequest;
import com.neo.aggregator.bank.sbm.core.request.SBAcctInquiryRequestDto.Body.SBAcctInqRequest.SBAcctInqRq;
import com.neo.aggregator.bank.sbm.core.request.SBAcctModRequestDto;
import com.neo.aggregator.bank.sbm.core.request.SBAcctModRequestDto.Body.SBAcctModRequest;
import com.neo.aggregator.bank.sbm.core.request.SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq;
import com.neo.aggregator.bank.sbm.core.request.SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec;
import com.neo.aggregator.bank.sbm.core.request.SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineeContactInfo;
import com.neo.aggregator.bank.sbm.core.request.SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineeContactInfo.NomineeContactInfoBuilder;
import com.neo.aggregator.bank.sbm.core.request.SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineeContactInfo.PostAddr;
import com.neo.aggregator.bank.sbm.core.request.SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineeContactInfo.PostAddr.PostAddrBuilder;
import com.neo.aggregator.bank.sbm.core.request.SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineeInfoRecBuilder;
import com.neo.aggregator.bank.sbm.core.request.SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineePercent;
import com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto;
import com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest;
import com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddCustomData;
import com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq;
import com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.DepositTerm;
import com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.InitialDeposit;
import com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RenewalDtls;
import com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RenewalDtls.RenewalTerm;
import com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RepayAcctId;
import com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TDAcctGenInfo;
import com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TDAcctId;
import com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TDAcctId.TDAcctIdBuilder;
import com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TrnDtls;
import com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TrnDtls.DebitAcctId;
import com.neo.aggregator.bank.sbm.core.request.TdAcctCloseRequestDto;
import com.neo.aggregator.bank.sbm.core.request.TdAcctCloseRequestDto.Body.DepAcctCloseCustomData;
import com.neo.aggregator.bank.sbm.core.request.TdAcctCloseRequestDto.Body.DepAcctCloseRequest;
import com.neo.aggregator.bank.sbm.core.request.TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq;
import com.neo.aggregator.bank.sbm.core.request.TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.CloseAmt;
import com.neo.aggregator.bank.sbm.core.request.TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.DepAcctId;
import com.neo.aggregator.bank.sbm.core.request.TdAcctInquiryRequestDto;
import com.neo.aggregator.bank.sbm.core.request.TdAcctInquiryRequestDto.Body.TDAcctInqRequest;
import com.neo.aggregator.bank.sbm.core.request.TdAcctInquiryRequestDto.Body.TDAcctInqRequest.TDAcctInqRq;
import com.neo.aggregator.bank.sbm.core.request.TdAcctTrailCloseRequestDto;
import com.neo.aggregator.bank.sbm.core.response.AcctLienAddResponseDto;
import com.neo.aggregator.bank.sbm.core.response.AddOutBoundPymtResponseDto;
import com.neo.aggregator.bank.sbm.core.response.BalinqResponseDto;
import com.neo.aggregator.bank.sbm.core.response.BalinqResponseDto.Body.BalInqResponse.BalInqRs.AcctBal;
import com.neo.aggregator.bank.sbm.core.response.CreditAddResponseDto;
import com.neo.aggregator.bank.sbm.core.response.DebitAddResponseDto;
import com.neo.aggregator.bank.sbm.core.response.FetchAccountStatementResponseDto;
import com.neo.aggregator.bank.sbm.core.response.FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement;
import com.neo.aggregator.bank.sbm.core.response.FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.TransactionDetails;
import com.neo.aggregator.bank.sbm.core.response.FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary;
import com.neo.aggregator.bank.sbm.core.response.FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TxnBalance;
import com.neo.aggregator.bank.sbm.core.response.IFTTransferResponseDto;
import com.neo.aggregator.bank.sbm.core.response.MiniStatementResponseDto;
import com.neo.aggregator.bank.sbm.core.response.MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary;
import com.neo.aggregator.bank.sbm.core.response.MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountTransactionSummary;
import com.neo.aggregator.bank.sbm.core.response.RetCustAddResponseDto;
import com.neo.aggregator.bank.sbm.core.response.SBAcctAddResponseDto;
import com.neo.aggregator.bank.sbm.core.response.SBAcctInquiryResponseDto;
import com.neo.aggregator.bank.sbm.core.response.SBModResponseDto;
import com.neo.aggregator.bank.sbm.core.response.TDAccountCloseTrailResponseDto;
import com.neo.aggregator.bank.sbm.core.response.TdAcctAddResponseDto;
import com.neo.aggregator.bank.sbm.core.response.TdAcctCloseResponseDto;
import com.neo.aggregator.bank.sbm.core.response.TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs;
import com.neo.aggregator.bank.sbm.core.response.TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.TrnDetailsRec;
import com.neo.aggregator.bank.sbm.core.response.TdAcctInquiryResponseDto;
import com.neo.aggregator.bank.sbm.request.AadhaarOtpRequestDto;
import com.neo.aggregator.bank.sbm.request.ESBRequestDto;
import com.neo.aggregator.bank.sbm.request.IMPSRequestDto;
import com.neo.aggregator.bank.sbm.request.LoginRequestDto;
import com.neo.aggregator.bank.sbm.request.PanRequestDto;
import com.neo.aggregator.bank.sbm.request.RequestInDto;
import com.neo.aggregator.bank.sbm.request.ValidateOtpRequestDto;
import com.neo.aggregator.bank.sbm.response.AadhaarOtpResponseDto;
import com.neo.aggregator.bank.sbm.response.IMPSResponseDto;
import com.neo.aggregator.bank.sbm.response.LoginResponseDto;
import com.neo.aggregator.bank.sbm.response.PanResponseDto;
import com.neo.aggregator.bank.sbm.response.PanResponseDto.PanData;
import com.neo.aggregator.bank.sbm.response.ResponseOutDto;
import com.neo.aggregator.commonservice.SbmCommonService;
import com.neo.aggregator.constant.NeoCustomFieldConstants;
import com.neo.aggregator.constant.ProductConstants;
import com.neo.aggregator.constant.SbmPropertyConstants;
import com.neo.aggregator.dao.MasterConfigDao;
import com.neo.aggregator.dao.NeoAccountCreationReqResDao;
import com.neo.aggregator.dao.NeoAccountTransferReqResDao;
import com.neo.aggregator.dao.PartnerAggregatorConfig;
import com.neo.aggregator.dao.PartnerServiceConfigRepo;
import com.neo.aggregator.dao.ProductConfigDao;
import com.neo.aggregator.dao.SessionManagementRepo;
import com.neo.aggregator.dto.AadhaarOtpRequest;
import com.neo.aggregator.dto.AadhaarOtpResponse;
import com.neo.aggregator.dto.AadhaarOtpResponse.AadhaarOtpInnerValidationResponse;
import com.neo.aggregator.dto.AadhaarOtpResponse.EkycData;
import com.neo.aggregator.dto.AccountClosureRequestDto;
import com.neo.aggregator.dto.AccountClosureResponseDto;
import com.neo.aggregator.dto.AccountCreationRequestDto;
import com.neo.aggregator.dto.AccountCreationResponseDto;
import com.neo.aggregator.dto.AccountInquiryRequestDto;
import com.neo.aggregator.dto.AccountInquiryResponseDto;
import com.neo.aggregator.dto.AccountModificationRequestDto;
import com.neo.aggregator.dto.AccountModificationResponseDto;
import com.neo.aggregator.dto.BalanceCheckRequest;
import com.neo.aggregator.dto.BalanceCheckResponseDto;
import com.neo.aggregator.dto.BalanceCheckResponseDto.AccountBalance;
import com.neo.aggregator.dto.BeneficiaryManagementReqDto;
import com.neo.aggregator.dto.BeneficiaryManagementResDto;
import com.neo.aggregator.dto.CustomerDedupCheckResponseDto;
import com.neo.aggregator.dto.FetchAccountStatementRequest;
import com.neo.aggregator.dto.FetchAccountStatemetResponse;
import com.neo.aggregator.dto.FetchAccountStatemetResponse.Transaction;
import com.neo.aggregator.dto.FundTransferRequestDto;
import com.neo.aggregator.dto.FundTransferResponseDto;
import com.neo.aggregator.dto.KYCBioRequestDto;
import com.neo.aggregator.dto.KYCBioResponseDto;
import com.neo.aggregator.dto.LienMarkRequestDto;
import com.neo.aggregator.dto.LienMarkResponseDto;
import com.neo.aggregator.dto.NeoCustomerDataDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.NeoResponse;
import com.neo.aggregator.dto.PanValidationRequest;
import com.neo.aggregator.dto.PanValidationResponse;
import com.neo.aggregator.dto.PanValidationResponse.PanValidationInnerResponse;
import com.neo.aggregator.dto.PayLaterRequestDto;
import com.neo.aggregator.dto.PayLaterResponseDto;
import com.neo.aggregator.dto.RegistrationRequestDto;
import com.neo.aggregator.dto.RegistrationRequestV2Dto;
import com.neo.aggregator.dto.RegistrationResponseDto;
import com.neo.aggregator.dto.SessionLoginDto;
import com.neo.aggregator.dto.TDAcctCloseTrailResponseDto;
import com.neo.aggregator.dto.TdAccountTrailCloseRequestDto;
import com.neo.aggregator.dto.TestAPIRequestDto;
import com.neo.aggregator.dto.v2.AccountsDto;
import com.neo.aggregator.dto.v2.AddressDto;
import com.neo.aggregator.dto.v2.NomineeDetailsDto;
import com.neo.aggregator.dto.yesbank.MerchantOBReq;
import com.neo.aggregator.dto.yesbank.MerchantOBRes;
import com.neo.aggregator.dto.yesbank.NeoPayRequest;
import com.neo.aggregator.dto.yesbank.NeoPayResponse;
import com.neo.aggregator.dto.yesbank.ValidateVpaReq;
import com.neo.aggregator.dto.yesbank.ValidateVpaRes;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.GeoMasterSBM;
import com.neo.aggregator.model.MasterConfiguration;
import com.neo.aggregator.model.NeoAccountCreationReqResModel;
import com.neo.aggregator.model.NeoAccountDetails;
import com.neo.aggregator.model.NeoAccountTransferReqRes;
import com.neo.aggregator.model.NeoBusinessCustomField;
import com.neo.aggregator.model.NeoCustomerCreationReqResModel;
import com.neo.aggregator.model.NeoCustomerData;
import com.neo.aggregator.model.NeoLienMarkReqResModel;
import com.neo.aggregator.model.PartnerMasterConfiguration;
import com.neo.aggregator.model.PartnerServiceConfiguration;
import com.neo.aggregator.model.ProductConfig;
import com.neo.aggregator.model.SessionManagement;
import com.neo.aggregator.service.AggregatorService;
import com.neo.aggregator.service.NeoBusinessCustomFieldService;
import com.neo.aggregator.service.RemoteService;
import com.neo.aggregator.service.SbmEbsService;
import com.neo.aggregator.utility.DateUtilityFunction;
import com.neo.aggregator.utility.EncryptionUtil;
import com.neo.aggregator.utility.IdGeneratorUtility;
import com.neo.aggregator.utility.NeoExceptionBuilder;
import com.neo.aggregator.utility.NeoExceptionConstant;
import com.neo.aggregator.utility.NeoJAXBContextFactory;
import com.neo.aggregator.utility.StringUtilsNeo;
import com.neo.aggregator.utility.TenantContextHolder;

@Service("sbmServiceImpl")
public class SbmEsbServiceImpl implements SbmEbsService, AggregatorService {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(SbmEsbServiceImpl.class);

	@Autowired
	private SessionManagementRepo sessionRepo;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private MasterConfigDao masterConfig;

	@Autowired
	private EncryptionUtil encryptionUtil;

	@Autowired
	private PartnerAggregatorConfig partnerConfig;

	@Autowired
	@Lazy
	private RemoteService rmService;

	@Autowired
	private SbmCommonService commonSerivce;

	@Autowired
	private ProductConfigDao productConfigDao;

	@Value("${neo.agg.local.url}")
	private String localUrl;

	@Value("${sbm.esb.base.url}")
	private String esbBaseUrl;

	@Value("${neo.keystore.path}")
	private Resource keyStoreFile;

	@Value("${neo.keystore.password}")
	private String keyStorePassword;

	@Autowired
	PartnerServiceConfigRepo apiConfig;

	@Autowired
	private AuditServiceImpl auditService;

	@Autowired
	private NeoAccountTransferReqResDao neoAccountReqRes;

	@Autowired
	private DozerBeanMapper dzMapper;

	@Autowired
	private NeoBusinessCustomFieldService customFieldService;

	@Autowired
	private NeoExceptionBuilder exceptionBuilder;

	@Autowired
	private NeoAccountCreationReqResDao neoAccountCreationDao;

	public static final String BANKID = "SBM";

	public static final String TLS_V1_2 = "TLSv1.2";
	
	@Autowired
	private IdGeneratorUtility idGeneratorUtility;

	private String getBankSessionId() {

		SessionManagement sessionModel = sessionRepo.findTopByIsExpiredAndBankIdOrderByCreatedDesc(Boolean.FALSE, BANKID);

		return sessionModel.getSessionId();
	}
	
	public RestTemplate disableSSLRestTemplate()
			throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		
		TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
	    SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
	    SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

	    Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
	            .register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();

	    BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
	            socketFactoryRegistry);
	    CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
	            .setConnectionManager(connectionManager).build();

	    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);

	    RestTemplate restTemplate = new RestTemplate(requestFactory);

	    return restTemplate;
	}

	public String postForEntity(String request, String url, HttpMethod method, String partnerName, String sslAlias,
			Boolean sslEnabled) {

		try {
			logger.info("URL ::" + url);
			logger.info("Request from Server ::" + request);
			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", partnerName);
			headers.setContentType(MediaType.APPLICATION_XML);

			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.APPLICATION_XML);
			mediaTypeList.add(MediaType.TEXT_PLAIN);
			headers.setAccept(mediaTypeList);

			if (sslEnabled != null && sslEnabled) {
				logger.info("SSL config loaded");
				restTemplate = sslConfig(sslAlias, TLS_V1_2);
			} else {
				logger.info("SSL config disabled");
				
				logger.info("HTTP Headers ::" + headers);

				HttpEntity<String> entity = new HttpEntity<String>(request, headers);
				RestTemplate template = disableSSLRestTemplate();
				ResponseEntity<String> response = template.exchange(url, method, entity, String.class,
						new Object[] { url });

				logger.info("Response from Server ::" + response.getBody());

				return response.getBody();
			}

			HttpEntity<String> entity = new HttpEntity<>(request, headers);
			ResponseEntity<String> response = restTemplate.exchange(url, method, entity, String.class,
					new Object[] { url });
			logger.info("Response from Server ::" + response.getBody());

			return response.getBody();
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
		}
		return null;
	}

	public String postForEntityWithHeaders(String request, String url, HttpMethod method, HttpHeaders headers,
			Boolean logRequired, String alias, Boolean sslEnabled, String product) throws NeoException {

		try {

			logger.info("URL ::" + url);
			if (logRequired) {

				if (request != null && product != null && product.equals(ProductConstants.EKYC_GENERATEAOTP)) {
					String maskedRequest = request;
					logger.info("Request from Server ::" + maskedRequest.replaceAll("Request\\>([^>]*?)<\\/Request", "*"));		

				} else {
					logger.info("Request from Server ::" + request);
				}
			} else {
				logger.info("Request log disabled");
			}

			if (sslEnabled != null && sslEnabled) {
				logger.info("SSL config loaded :: alias " + alias + " tls :: TLSv1.2");
				restTemplate = sslConfig(alias, "TLSv1.2");
			} else {
				logger.info("SSL config disabled :: using disableSSLRestTemplate");

				logger.info("HTTP Headers ::" + headers);
				
				RestTemplate template = disableSSLRestTemplate();

				HttpEntity<String> entity = new HttpEntity<String>(request, headers);
				ResponseEntity<String> response = template.exchange(url, method, entity, String.class,
						new Object[] { url });

				logger.info("Response from Server ::" + response.getBody());

				return response.getBody();
			}

			logger.info("HTTP Headers ::" + headers);

			HttpEntity<String> entity = new HttpEntity<String>(request, headers);
			ResponseEntity<String> response = restTemplate.exchange(url, method, entity, String.class,
					new Object[] { url });

			logger.info("Response from Server ::" + response.getBody());

			return response.getBody();
		} catch (HttpClientErrorException ce) {
			logger.info("HttpClientErrorException Occured ::" + ce.getMessage());
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
					"Bank Client Error Occured " + ce.getRawStatusCode(), null, Locale.US, request,
					ce.getResponseBodyAsString(), product);
		} catch (HttpServerErrorException se) {
			logger.info("HttpServerErrorException Occured ::" + se.getMessage());
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
					"Bank Server Error Occured " + se.getRawStatusCode(), null, Locale.US, request,
					se.getResponseBodyAsString(), product);
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					request, e.getMessage(), product);
		}
	}

	private RestTemplate sslConfig(String alias, String tlsVersion) throws KeyStoreException, NoSuchAlgorithmException,
			CertificateException, IOException, KeyManagementException, UnrecoverableKeyException {

		KeyStore keyStore = KeyStore.getInstance("JKS");
		keyStore.load(keyStoreFile.getInputStream(), keyStorePassword.toCharArray());

		try {
			keyStoreFile.getInputStream().close();
		} catch (IOException e) {
			logger.info("IOException KeyStore File :: " + e.getMessage());
		}

		SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
				new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy())
						.loadKeyMaterial(keyStore, keyStorePassword.toCharArray(), (aliases, socket) -> alias).build(),
				new String[] { tlsVersion }, null, NoopHostnameVerifier.INSTANCE);

		HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();

		ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);

		return new RestTemplate(requestFactory);
	}

	@Override
	public RegistrationResponseDto retailCifCreation(RegistrationRequestV2Dto request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		RetCustAddRequestDto requestDto = null;
		String retailCifCreationRequest = null;

		String retailCifCreationResponse = null;
		StringWriter sw = new StringWriter();
		RegistrationResponseDto response = new RegistrationResponseDto();
		NeoCustomerCreationReqResModel customerCreationModel = new NeoCustomerCreationReqResModel();

		try {

			MasterConfiguration configModel = masterConfig.findByPartnerId(TenantContextHolder.getNeoTenant());

			ProductConfig productConfigModel = productConfigDao
					.findByPartnerAndProduct(TenantContextHolder.getNeoTenant(), ProductConstants.CIF_CREATION);

			if (productConfigModel == null)
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.PRODUCT_CONFIG_NOT_FOUND, null, null,
						Locale.US, null, null, ProductConstants.CIF_CREATION);

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			Header header = commonSerivce.buildHeader(SbmPropertyConstants.RETAIL_CIF_CREATION);

			List<PhoneEmailDtls> phoneDetails = commonSerivce.buildPhoneDetails(request.getCommunicationInfo());
			List<AddrDtls> addrDtls = commonSerivce.buildAddressDtls(request.getAddressInfo(), request);

			CustDtls custDtls = commonSerivce.buildCustDtls(request, addrDtls, request.getCommunicationInfo(),
					phoneDetails, productConfigModel.getBranchId());

			DemographicData demoData = commonSerivce.buildDemographic(request);

			List<EntityDoctData> entityDocData = commonSerivce.buildEntityDocData(request, addrDtls);

			PsychographMiscData psychoMisc = commonSerivce.buildMiscData(request,
					productConfigModel.getDefaultAccountCurrency(), productConfigModel.getDefaultNationality());

			PsychographicData psychoData = commonSerivce.buildPsychoData(request, psychoMisc,
					productConfigModel.getDefaultAccountCurrency(), productConfigModel.getDefaultNationality());

			RelatedDtls relatedDtls = commonSerivce.buildRelatedDtls(psychoData, demoData, entityDocData);

			RetCustAddRq retCustAddRq = RetCustAddRq.builder().custDtls(custDtls).relatedDtls(relatedDtls).build();

			RetCustAddRequest retCustAddRequest = RetCustAddRequest.builder().retCustAddRq(retCustAddRq).build();

			RetCustAddRequestBody body = RetCustAddRequestBody.builder().retCustAddRequest(retCustAddRequest).build();

			requestDto = RetCustAddRequestDto.builder().body(body).header(header).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getRetCustAddRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.RETAIL_CIF_CREATION_XSD);
			m.marshal(requestDto, sw);

			retailCifCreationRequest = sw.toString();

			logger.m2pdebug("Plain Request Send ::" + retailCifCreationRequest);

			String encryptedPayload = encryptionUtil.encrypt(retailCifCreationRequest);

			logger.info("Encrypted Actual Payload ::" + encryptedPayload);

			RequestInDto retailCustAddRequestDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1001").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(retailCustAddRequestDto, stringWriter);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1001";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.CIF_CREATION);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			customerCreationModel.setRequestId(requestId);
			customerCreationModel.setEntityId(request.getEntityId());
			customerCreationModel.setBankId(BANKID);
			customerCreationModel.setTenant(TenantContextHolder.getNeoTenant());
			customerCreationModel.setFirstName(request.getFirstName());
			customerCreationModel.setMiddleName(request.getMiddleName());
			customerCreationModel.setLastName(request.getLastName());
			customerCreationModel.setSolId(custDtls.getCustData().getPrimarySolid());
			customerCreationModel.setTaxDeductionTable(custDtls.getCustData().getTaxDeductionTable());
			customerCreationModel.setIsNewCustomer(Boolean.FALSE);
			customerCreationModel.setIsNRECustomer(request.getIsNRECustomer());
			customerCreationModel.setIsMinor(request.getIsMinor());

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.CIF_CREATION);

			retailCifCreationResponse = decryptESBResponse(responseBody, ProductConstants.CIF_CREATION, sessionId);

			logger.m2pdebug("Decrypted Actual Response ::" + retailCifCreationResponse);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getRetCustAddResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(retailCifCreationResponse);
			RetCustAddResponseDto ress = (RetCustAddResponseDto) um.unmarshal(decryptedResponsereader);

			if (ress.getHeader().getResponseHeader().getHostTransaction().getStatus().equals("SUCCESS")) {

				logger.info("SUCCESS RESPONSE");

				String custId = ress.getBody().getRetCustAddResponse().getRetCustAddRs().getCustId();
				String status = ress.getBody().getRetCustAddResponse().getRetCustAddRs().getStatus();
				String description = ress.getBody().getRetCustAddResponse().getRetCustAddRs().getDesc();
				response.setStatus(status);
				response.setDescription(description);
				response.setEntityId(request.getEntityId());

				response.setCustomerId(custId);

				customerCreationModel.setStatus("SUCCESS");
				customerCreationModel.setIsNewCustomer(Boolean.TRUE);
				customerCreationModel.setResponseDescription(description);
				customerCreationModel.setCifNumber(custId);

			} else {
				logger.info("FAILURE RESPONSE");

				if (ress.getBody().getError() != null) {

					if (ress.getBody().getError().getFiSystemException() != null && ress.getBody().getError().getFiSystemException().getErrorDetail().getErrorDesc() != null) {

						String errDesp = ress.getBody().getError().getFiSystemException().getErrorDetail()
								.getErrorDesc();

						if (!errDesp.contains("Record is already present with CIF")) {

							throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
									ress.getBody().getError().getFiSystemException().getErrorDetail().getErrorDesc(),
									null, Locale.US, retailCifCreationRequest, retailCifCreationResponse,
									ProductConstants.CIF_CREATION);
						}
						
						logger.info("Looking for duplicate CIF");
						
						errDesp = errDesp.replace("Record is already present with CIF ", "");

						Pattern pattern = Pattern.compile("^([\\w\\-]+)");
						Matcher matcher = pattern.matcher(errDesp);
						
						String custId = null;
						
						if (matcher.find()) {
							logger.info("Duplicate cif pattern match found ::" + matcher.group(0));
							custId = matcher.group(0);
						}
												
						if(custId != null) {
							String status = "SUCCESS";
							String description = "Duplicate cif number found";
							response.setStatus(status);
							response.setDescription(description);
							response.setEntityId(request.getEntityId());

							response.setCustomerId(custId);

							customerCreationModel.setStatus("SUCCESS");
							customerCreationModel.setResponseDescription(description);
							customerCreationModel.setCifNumber(custId);

						} else {
							throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
									ress.getBody().getError().getFiSystemException().getErrorDetail().getErrorDesc(),
									null, Locale.US, retailCifCreationRequest, retailCifCreationResponse,
									ProductConstants.CIF_CREATION);
						}

					} else if (ress.getBody().getError().getFIBusinessException() != null) {

						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFIBusinessException().getErrorDetail().getErrorDesc(),
								null, Locale.US, retailCifCreationRequest, retailCifCreationResponse,
								ProductConstants.CIF_CREATION);
					}

				} else {
					throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
							"Failure Response Error Details Not Available", null, Locale.US, retailCifCreationRequest,
							retailCifCreationResponse, ProductConstants.CIF_CREATION);
				}

			}

			return response;

		} catch (NeoException e) {
			customerCreationModel.setStatus("FAILURE");
			customerCreationModel.setResponseDescription(e.getDetailMessage());
			throw e;
		} catch (Exception e) {
			customerCreationModel.setStatus("FAILURE");
			customerCreationModel.setResponseDescription("Exception occured ::" + e.getMessage());
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					retailCifCreationRequest, retailCifCreationResponse, ProductConstants.CIF_CREATION);
		} finally {
			auditService.saveCustomerCreation(customerCreationModel);
		}
	}

	@Override
	public AadhaarOtpResponse generateOtpAadhaar(AadhaarOtpRequest request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		AadhaarOtpResponse responseDto = new AadhaarOtpResponse();
		String bankId = BANKID;
		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			String channelId = configModel.getChannelName();
			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);
			String stan = RandomStringUtils.random(6, false, true);
			String requestUidaiId = RandomStringUtils.random(12, false, true);
			String localTimeDate = DateUtilityFunction.dateInFormat(date,
					SbmPropertyConstants.ESB_LOCAL_DATE_TIME_FORMAT);

			AadhaarOtpRequestDto aadharReqDto = AadhaarOtpRequestDto.builder()
					.aadhaarValidation(request.getIdTypeValidation()).aadharNo(request.getIdNumber())
					.bank(configModel.getBankName()).channel("MobileApp").localTransactionDateTime(localTimeDate)
					.requestId(requestUidaiId).stan(stan).build();

			String aadhaarJsonPayload = new ObjectMapper().writeValueAsString(aadharReqDto);

			String encryptedPayload = encryptionUtil.encrypt(aadhaarJsonPayload);

			logger.info("Encrypted Request ::" + encryptedPayload);

			RequestInDto aadhaarRequestInDto = RequestInDto.builder().authType("GenerateOTP").channelId(channelId)
					.moduleID("").partnerReqID(requestUidaiId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("EKYC").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(aadhaarRequestInDto, stringWriter);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-AeKYCAPI%23e_KYCAPI_OTPGenerate_Validate";

			String sessionId = getBankSessionId();

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.EKYC_GENERATEAOTP);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.EKYC_GENERATEAOTP);

			String decryptedResponse = decryptESBResponse(responseBody, ProductConstants.EKYC_GENERATEAOTP, sessionId);

			logger.m2pdebug("Actual Response ::" + decryptedResponse);

			AadhaarOtpResponseDto aadhaarOtpResponseDto = new ObjectMapper().readValue(decryptedResponse,
					AadhaarOtpResponseDto.class);

			AadhaarOtpInnerValidationResponse innerValidationResponse = AadhaarOtpInnerValidationResponse.builder()
					.description(aadhaarOtpResponseDto.getResponseMsg()).otpReferenceNo(aadhaarOtpResponseDto.getStan())
					.responseCode(aadhaarOtpResponseDto.getResponseCode()).build();

			responseDto.setValidationResponse(innerValidationResponse);

		} catch (NeoException ex) {
			logger.info("Exception Occured ::" + ex.getDetailMessage());
			throw ex;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US, null,
					null, ProductConstants.EKYC_GENERATEAOTP);
		}

		return responseDto;
	}

	@Override
	public AadhaarOtpResponse validateOtpAadhaar(AadhaarOtpRequest request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		AadhaarOtpResponse responseDto = new AadhaarOtpResponse();
		String bankId = BANKID;

		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			String channelId = configModel.getChannelName();
			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);
			String requestUidaiId = RandomStringUtils.random(12, false, true);
			String localTimeDate = DateUtilityFunction.dateInFormat(date,
					SbmPropertyConstants.ESB_LOCAL_DATE_TIME_FORMAT);

			AadhaarOtpRequestDto aadhaarOtpReqDto = AadhaarOtpRequestDto.builder()
					.aadhaarValidation(request.getIdTypeValidation()).aadharNo(request.getIdNumber())
					.bank(configModel.getBankName()).channel("MobileApp").localTransactionDateTime(localTimeDate)
					.requestId(requestUidaiId).stan(request.getOtpReferenceNo()).otp(request.getOtp())
					.tpa(configModel.getBankName()).build();

			String aadhaarOtpReqPayload = new ObjectMapper().writeValueAsString(aadhaarOtpReqDto);

			String encryptedRequestPayload = encryptionUtil.encrypt(aadhaarOtpReqPayload);

			logger.info("Encrypted Request ::" + encryptedRequestPayload);

			RequestInDto req = RequestInDto.builder().authType("OTP").channelId(channelId).moduleID("")
					.partnerReqID(requestUidaiId).partnerUserName(configModel.getUserName()).timestamp(timeStamp)
					.requestType("EKYC").request(encryptedRequestPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(req, stringWriter);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-AeKYCAPI%23e_KYCAPI_OTPGenerate_Validate";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.EKYC_VALIDATEAOTP);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.FALSE, sslAlias, sslEnabled, ProductConstants.EKYC_VALIDATEAOTP);

			String decryptedResponse = decryptESBResponse(responseBody, ProductConstants.EKYC_VALIDATEAOTP ,sessionId);

			logger.m2pdebug("Decrypted Actual Response ::" + decryptedResponse);

			AadhaarOtpResponseDto aotpResponseDto = new ObjectMapper().readValue(decryptedResponse,
					AadhaarOtpResponseDto.class);

			if (aotpResponseDto == null)
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
						null, null, ProductConstants.EKYC_VALIDATEAOTP);

			if (aotpResponseDto.getResponseCode() != null && !aotpResponseDto.getResponseCode().equals("00"))
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
						aotpResponseDto.getResponseMsg(), null, Locale.US, null, null,
						ProductConstants.EKYC_VALIDATEAOTP);

			String ekycRefNo = aotpResponseDto.getReferenceKey();
			String aadhaarEkycData = aotpResponseDto.getEkycData();

			aadhaarEkycData = aadhaarEkycData.replace("\u0000", "");
			aadhaarEkycData = aadhaarEkycData.replace("\\u0000", "");
			EkycResponseDto ekycResponseDto = null;
			EkycData ekycData = null;

			try {
				Document ekycDocument = StringUtilsNeo.convertStringToDocument(aadhaarEkycData);
				String ekycDocumentXml = StringUtilsNeo.convertDocumentToString(ekycDocument);

				JAXBContext responseOutJAXB = NeoJAXBContextFactory.getEkycResponseContext();
				Unmarshaller responseOutUnmarshaller = responseOutJAXB.createUnmarshaller();

				StringReader stringReader = new StringReader(ekycDocumentXml);
				ekycResponseDto = (EkycResponseDto) responseOutUnmarshaller.unmarshal(stringReader);

				String firstName = ekycResponseDto.getUidData().getPoi().getName();
				String dob = ekycResponseDto.getUidData().getPoi().getDob();
				String country = ekycResponseDto.getUidData().getPoa().getCountry();
				String district = ekycResponseDto.getUidData().getPoa().getDist();
				String house = ekycResponseDto.getUidData().getPoa().getHouse();
				String postalCode = ekycResponseDto.getUidData().getPoa().getPc();
				String state = ekycResponseDto.getUidData().getPoa().getState();
				String street = ekycResponseDto.getUidData().getPoa().getStreet();
				String area = ekycResponseDto.getUidData().getPoa().getVtc();
				String kycImage = ekycResponseDto.getUidData().getPht();
				String careOf = ekycResponseDto.getUidData().getPoa().getCo();
				String gender = ekycResponseDto.getUidData().getPoi().getGender();

				StringBuffer stringBuffer = new StringBuffer();
				stringBuffer.append(StringUtilsNeo.nullCheckToEmpty(house));
				stringBuffer.append(" ");
				stringBuffer.append(StringUtilsNeo.nullCheckToEmpty(street));
				stringBuffer.append(" ");
				stringBuffer.append(StringUtilsNeo.nullCheckToEmpty(area));

				String address = stringBuffer.toString();

				ekycData = EkycData.builder().firstName(firstName).address(address).area(area).city(district)
						.country(country).pinCode(postalCode).house(house).dob(dob).state(state).kycImage(kycImage)
						.street(street).district(district).careOf(careOf).gender(gender).build();

			} catch (Exception e) {
				logger.info("Exception Occured ::" + e.getMessage());
				logger.m2pdebug("Exception Occured :: ", e);
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
						"KYC Data format is invalid", null, Locale.US, null, null, ProductConstants.EKYC_VALIDATEAOTP);

			}

			AadhaarOtpInnerValidationResponse aadhaarInnerResponse = AadhaarOtpInnerValidationResponse.builder()
					.description(aotpResponseDto.getResponseMsg()).otpReferenceNo(aotpResponseDto.getStan())
					.responseCode(aotpResponseDto.getResponseCode()).ekycData(ekycData).ekycRefNo(ekycRefNo).build();

			responseDto.setValidationResponse(aadhaarInnerResponse);

		} catch (NeoException e) {
			logger.info("Exception occured ::" + e.getDetailMessage());
			throw e;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US, null,
					null, ProductConstants.EKYC_VALIDATEAOTP);
		}

		return responseDto;
	}

	@Override
	public PanValidationResponse validatePan(PanValidationRequest request) throws NeoException {

		StringWriter sw = new StringWriter();
		PanValidationResponse responseDto = new PanValidationResponse();
		String bankId = BANKID;

		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);
			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();
			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			List<PanRequestDto> requestList = new ArrayList<>();

			PanRequestDto panRequestDto = new PanRequestDto();
			panRequestDto.setPanNumber(request.getIdNumber());

			requestList.add(panRequestDto);

			String requestPayload = new ObjectMapper().writeValueAsString(requestList);

			String encryptedPayload = encryptionUtil.encrypt(requestPayload);

			RequestInDto req = RequestInDto.builder().authType("").channelId(channelId).moduleID("")
					.partnerReqID(requestId).partnerUserName(configModel.getUserName()).timestamp(timeStamp)
					.requestType("PAN").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(req, sw);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration&prgname=HTTP&arguments=-AHTTP_PAN%23PANVerification";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.EKYC_VALIDATEPAN);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(sw.toString(), url, HttpMethod.POST, headers, Boolean.TRUE,
					sslAlias, sslEnabled, ProductConstants.EKYC_VALIDATEPAN);

			String decryptedResponse = decryptESBResponse(responseBody, ProductConstants.EKYC_VALIDATEPAN, sessionId);

			PanResponseDto panRespDto = new ObjectMapper().readValue(decryptedResponse, PanResponseDto.class);

			List<PanData> panResonseListData = panRespDto.getData();

			PanData panDataRespo = panResonseListData.get(0);

			PanValidationInnerResponse panInnerValidationResponse = PanValidationInnerResponse.builder()
					.firstName(panDataRespo.getFirstName()).lastName(panDataRespo.getLastName())
					.panNo(panDataRespo.getPanNo()).middleName(panDataRespo.getMiddleName()).build();

			responseDto.setValidationResponse(panInnerValidationResponse);

		} catch (NeoException ex) {
			logger.info("Exception occured ::" + ex.getDetailMessage());
			throw ex;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US, null,
					null, ProductConstants.EKYC_VALIDATEPAN);
		}

		return responseDto;
	}

	@Override
	public SessionLoginDto loginCreation(SessionLoginDto request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		SessionLoginDto responseDto = new SessionLoginDto();
		String bankId = BANKID;
		String esbLoginRequest = null;
		String esbLoginResponse = null;

		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			String userName = configModel.getUserName();
			String password = configModel.getPassword();

			LoginRequestDto loginReqDto = LoginRequestDto.builder().username(userName).password(password).build();

			JAXBContext loginRequestJAXB = NeoJAXBContextFactory.getLoginRequestContext();
			Marshaller loginRequestMarshaller = loginRequestJAXB.createMarshaller();
			loginRequestMarshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
			loginRequestMarshaller.marshal(loginReqDto, stringWriter);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ALogin%23LoginSession";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.ESB_LOGIN);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			esbLoginRequest = stringWriter.toString();

			String encryptedReqPayload = encryptionUtil.encrypt(esbLoginRequest);
			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();
			String timeStamp = DateUtilityFunction.dateInFormat(new Date(), SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			ESBRequestDto esbRequestDto = ESBRequestDto.builder().partnerReqID(requestId).timestamp(timeStamp)
					.channelId(channelId).request(encryptedReqPayload).build();

			JAXBContext esjaxbContext = NeoJAXBContextFactory.getESBRequestContext();
			Marshaller mes = esjaxbContext.createMarshaller();
			stringWriter = new StringWriter();
			mes.marshal(esbRequestDto, stringWriter);

			esbLoginResponse = postForEntity(stringWriter.toString(), url, HttpMethod.POST,
					configModel.getPartnerName(), sslAlias, sslEnabled);

			JAXBContext loginResponseJAXB = NeoJAXBContextFactory.getLoginResponseContext();
			Unmarshaller loginResponseUnmarshaller = loginResponseJAXB.createUnmarshaller();

			StringReader reader = new StringReader(esbLoginResponse);
			LoginResponseDto loginResponse = (LoginResponseDto) loginResponseUnmarshaller.unmarshal(reader);

			if (loginResponse.getStatus() != null && loginResponse.getStatus().toUpperCase().equals("SUCCESS")) {
				responseDto.setSessionId(loginResponse.getSessionid());
				responseDto.setResponseCode("00");
				responseDto.setDescription("Login Successful");
				responseDto.setResponseMessage("SUCCESS");
				
				Integer sessionExpiryInMin = configModel.getSessionInterval() != null ? configModel.getSessionInterval()
						: 220;
				Calendar cal = Calendar.getInstance();
				Long time = cal.getTimeInMillis();
				Date expiryTime = new Date(time + (sessionExpiryInMin * 60000));

				SessionManagement sessionModel = new SessionManagement();
				sessionModel.setBankId(bankId);
				sessionModel.setIsExpired(Boolean.FALSE);
				sessionModel.setSessionId(responseDto.getSessionId());
				sessionModel.setExpiryDate(expiryTime);

				sessionRepo.save(sessionModel);
				
			} else {
				
				responseDto.setResponseCode("05");
				responseDto.setDescription("Login Failed");
				responseDto.setResponseMessage("Failure");
			}

		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, "Session Login Failed",
					null, Locale.US, esbLoginRequest, esbLoginResponse, ProductConstants.ESB_LOGIN);
		}

		return responseDto;
	}

	@Override
	public AccountCreationResponseDto savingsAccountCreation(AccountCreationRequestDto request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		SBAcctAddRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		AccountCreationResponseDto response = new AccountCreationResponseDto();
		AccountsDto accountInfo = null;
		NeoAccountCreationReqResModel accountReqResModel = new NeoAccountCreationReqResModel();
		NomineeDetailsDto nomineeDetails = null;
		AddressDto addressDto = null;
		String savingsAccountCreationRequest = null;
		String savingsAccountCreationResponse = null;
		try {

			SBAcctAddRq sbAccountAddReq = null;

			if (request.getAccountInfo() != null && request.getAccountInfo().size() > 0) {
				accountInfo = request.getAccountInfo().get(0);
			} else
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.ACCOUNTINFO_INCORRECT, null, null, Locale.US,
						null, null, ProductConstants.SB_CREATION);

			MasterConfiguration configModel = masterConfig.findByPartnerId(TenantContextHolder.getNeoTenant());

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			Header header = commonSerivce.buildHeader(SbmPropertyConstants.SAVINGS_ACCOUNT_OPENING);

			String channelIdCBS = commonSerivce.getCBSChannelId(TenantContextHolder.getNeoTenant());

			String modeOfOperation = "SELF";

			SBAcctAddCustomData customData = null;

			customData = SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddCustomData.builder()
					.solid(accountInfo.getBranchId()).calflg("N").intcodecr("S").chnlid(channelIdCBS)
					.modeofoperation(modeOfOperation).sbmiscentrd("1").freetext1("TEST").freetext2("").freetext3("")
					.freetext4("").freetext5("").freetext6("").freetext7("").freetext8("").freetext9("FI")
					.debitcardflg("Y").build();

			NeoBusinessCustomField sbDebitFreezeCustomField = customFieldService.findByFieldNameAndTenant(
					NeoCustomFieldConstants.SAVINGS_ACCOUNT_DEBIT_FREEZE_ENABLED, TenantContextHolder.getNeoTenant());

			if (sbDebitFreezeCustomField != null && sbDebitFreezeCustomField.getFieldValue() != null
					&& sbDebitFreezeCustomField.getFieldValue().equals("Y")) {
				customData = SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddCustomData.builder()
						.solid(accountInfo.getBranchId()).calflg("N").intcodecr("S").chnlid(channelIdCBS)
						.modeofoperation(modeOfOperation).sbmiscentrd("1").freetext1("TEST").freetext2("").freetext3("")
						.freetext4("").freetext5("").freetext6("").freetext7("").freetext8("").freetext9("FI")
						.debitcardflg("Y").freezeCode("C").freezeReasonCode("999").build();
			}

			CustId custId = SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.CustId.builder()
					.custId(request.getCifNumber()).build();

			AcctType accType = SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.SBAcctId.AcctType.builder()
					.schmCode(accountInfo.getSchemeCode()).build();

			SBAcctId sbAccountId = SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.SBAcctId.builder()
					.acctCurr(accountInfo.getAccountsCurrency()).acctType(accType).build();

			SBAcctGenInfo dispatchMode = SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.SBAcctGenInfo.builder()
					.despatchMode("N").build();

			if ((request.getNomineeInfo() != null && request.getNomineeInfo().size() > 0)
					&& (request.getNomineeContactInfo() != null && request.getNomineeContactInfo().size() > 0)) {

				nomineeDetails = request.getNomineeInfo().get(0);

				com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineeInfoRecBuilder nomineeInfoBuilder = SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec
						.builder();

				com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineeContactInfo.NomineeContactInfoBuilder nomineeContactInfoBuilder = SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineeContactInfo
						.builder();

				com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineePercent nomineePercentage = SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineePercent
						.builder().value("100").build();

				com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineeContactInfo.PostAddr.PostAddrBuilder postalAddressBuilder = SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineeContactInfo.PostAddr
						.builder();

				addressDto = request.getNomineeContactInfo().get(0);

				GeoMasterSBM geoMaster = commonSerivce.getGeoMasterFromPinCode(addressDto.getPincode());

				com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineeContactInfo.PostAddr postalAddress = postalAddressBuilder
						.addr1(addressDto.getAddress1()).addr2(addressDto.getAddress2()).addr3(addressDto.getAddress3())
						.city(geoMaster.getDistrict()).stateProv(geoMaster.getState()).country(geoMaster.getCountry())
						.postalCode(addressDto.getPincode()).build();

				com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineeContactInfo nomineeContactInfo = nomineeContactInfoBuilder
						.postAddr(postalAddress).build();

				String isMinor = "N";

				if (nomineeDetails.getIsMinor()) {
					isMinor = "Y";
				}

				String dobFormatted = DateUtilityFunction.dateInFormat(nomineeDetails.getNomineeDOB(),
						SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

				String relPartyId = commonSerivce
						.getRelPartyTypeByRelationShip(nomineeDetails.getNomineeRelationship());

				com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec nomineeInfoRec = nomineeInfoBuilder
						.nomineeContactInfo(nomineeContactInfo).regNum("0001")
						.nomineeName(nomineeDetails.getNomineeName()).nomineePercent(nomineePercentage)
						.nomineeBirthDt(dobFormatted).relType(relPartyId).nomineeMinorFlg(isMinor).build();

				sbAccountAddReq = SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.builder().custId(custId)
						.sbAcctId(sbAccountId).sbAcctGenInfo(dispatchMode).nomineeInfoRec(nomineeInfoRec).build();

			} else {
				sbAccountAddReq = SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.builder().custId(custId)
						.sbAcctId(sbAccountId).sbAcctGenInfo(dispatchMode).build();
			}

			SBAcctAddRequest sbAcctAddRequest = SBAcctAddRequestDto.Body.SBAcctAddRequest.builder()
					.sbAcctAddCustomData(customData).sbAcctAddRq(sbAccountAddReq).build();

			Body requestBody = SBAcctAddRequestDto.Body.builder().sbAcctAddRequest(sbAcctAddRequest).build();

			requestDto = SBAcctAddRequestDto.builder().header(header).body(requestBody).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getSBAcctAddRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.SAVINGS_ACCOUNT_OPENING_XSD);
			m.marshal(requestDto, sw);

			savingsAccountCreationRequest = sw.toString();

			logger.info("Actual Payload ::" + savingsAccountCreationRequest);

			String encryptedPayload = encryptionUtil.encrypt(savingsAccountCreationRequest);

			RequestInDto retailCustAddRequestDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1007").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(retailCustAddRequestDto, stringWriter);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1007";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.SB_CREATION);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.SB_CREATION);

			savingsAccountCreationResponse = decryptESBResponse(responseBody, ProductConstants.SB_CREATION, sessionId);

			logger.info("Decrypted Response ::" + savingsAccountCreationResponse);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getSBAcctAddResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(savingsAccountCreationResponse);
			SBAcctAddResponseDto ress = (SBAcctAddResponseDto) um.unmarshal(decryptedResponsereader);

			accountReqResModel.setRequestId(requestId);
			accountReqResModel.setAccountCurrency(accountInfo.getAccountsCurrency());

			accountReqResModel.setTenant(TenantContextHolder.getNeoTenant());
			accountReqResModel.setBankId(BANKID);
			accountReqResModel.setEntityId(request.getEntityId());
			accountReqResModel.setModeOfOperation(modeOfOperation);
			accountReqResModel.setSchemeCode(accountInfo.getSchemeCode());
			accountReqResModel.setSolId(accountInfo.getBranchId());
			accountReqResModel.setProductId(ProductConstants.SAVINGS_ACCOUNT_OPENING);

			if (ress.getHeader().getResponseHeader().getHostTransaction().getStatus().equals("SUCCESS")) {
				logger.info("SUCCESS RESPONSE");
				String accountId = ress.getBody().getSBAcctAddResponse().getSBAcctAddRs().getSBAcctId().getAcctId();

				response.setAccountId(accountId);
				accountReqResModel.setAccountId(accountId);
				accountReqResModel.setStatus("SUCCESS");
				accountReqResModel.setResponseDescription("Account Creation Successful");
			} else {
				logger.info("FAILURE RESPONSE");

				if (ress.getBody().getError() != null) {

					accountReqResModel.setStatus("FAILURE");

					if (ress.getBody().getError().getFiSystemException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFiSystemException().getErrorDetail().getErrorDesc(), null,
								Locale.US, savingsAccountCreationRequest, savingsAccountCreationResponse,
								ProductConstants.SB_CREATION);
					} else if (ress.getBody().getError().getFIBusinessException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFIBusinessException().getErrorDetail().getErrorDesc(),
								null, Locale.US, savingsAccountCreationRequest, savingsAccountCreationResponse,
								ProductConstants.SB_CREATION);
					}

				}
			}

			return response;

		} catch (NeoException e) {
			accountReqResModel.setStatus("FAILURE");
			accountReqResModel.setResponseDescription(e.getDetailMessage());
			throw e;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			accountReqResModel.setStatus("FAILURE");
			accountReqResModel.setResponseDescription("Exception occured ::" + e.getMessage());
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					savingsAccountCreationRequest, savingsAccountCreationResponse, ProductConstants.SB_CREATION);
		} finally {
			auditService.saveAccountCreation(accountReqResModel);
		}
	}

	@Override
	public BalanceCheckResponseDto fetchAccountBalance(BalanceCheckRequest request) throws NeoException {
		StringWriter stringWriter = new StringWriter();
		String bankId = BANKID;
		BalinqRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		BalanceCheckResponseDto response = new BalanceCheckResponseDto();
		String balInqRequest = null;
		String balInqResponse = null;

		try {

			if (StringUtils.isEmpty(request.getAccountNo()))
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.ACCOUNTINFO_INCORRECT, null, null, Locale.US,
						null, null, ProductConstants.BALANCE_INQUIRY);

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			Header header = commonSerivce.buildHeader(SbmPropertyConstants.BALANCE_INQUIRY);

			AcctId accountId = BalinqRequestDto.Body.BalInqRequest.BalInqRq.AcctId.builder()
					.acctId(request.getAccountNo()).build();

			BalInqRq balInqRq = BalinqRequestDto.Body.BalInqRequest.BalInqRq.builder().acctId(accountId).build();

			BalInqRequest balInqRequestDto = BalinqRequestDto.Body.BalInqRequest.builder().balInqRq(balInqRq).build();

			com.neo.aggregator.bank.sbm.core.request.BalinqRequestDto.Body balinqBody = BalinqRequestDto.Body.builder()
					.balInqRequest(balInqRequestDto).build();

			requestDto = BalinqRequestDto.builder().header(header).body(balinqBody).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getBalInqRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.BALANCE_INQUIRY_XSD);
			m.marshal(requestDto, sw);

			String balanceInqRequest = sw.toString();

			logger.info("Actual payload ::" + balanceInqRequest);

			String encryptedPayload = encryptionUtil.encrypt(balanceInqRequest);

			RequestInDto retailCustAddRequestDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1013").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(retailCustAddRequestDto, stringWriter);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1013";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.BALANCE_INQUIRY);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.BALANCE_INQUIRY);

			balInqResponse = decryptESBResponse(responseBody, ProductConstants.BALANCE_INQUIRY, sessionId);

			logger.info("Decrypted Response ::" + balInqResponse);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getBalInqResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(balInqResponse);
			BalinqResponseDto ress = (BalinqResponseDto) um.unmarshal(decryptedResponsereader);

			if (ress.getHeader().getResponseHeader().getHostTransaction().getStatus().equals("SUCCESS")) {

				logger.info("SUCCESS RESPONSE");

				List<AcctBal> accBal = ress.getBody().getBalInqResponse().getBalInqRs().getAcctBal();

				String accountNo = ress.getBody().getBalInqResponse().getBalInqRs().getAcctId().getAcctId();

				String currency = ress.getBody().getBalInqResponse().getBalInqRs().getAcctId().getAcctCurr();

				String branchId = ress.getBody().getBalInqResponse().getBalInqRs().getAcctId().getBankInfo()
						.getBranchId();

				List<AccountBalance> accountBalance = new ArrayList<>();

				for (AcctBal account : accBal) {

					AccountBalance accountBal = new AccountBalance();

					accountBal.setBalance(account.getBalAmt().getAmountValue());
					accountBal.setProductId(account.getBalType());
					accountBal.setCurrency(account.getBalAmt().getCurrencyCode());

					accountBalance.add(accountBal);
				}

				response.setAccountBalance(accountBalance);
				response.setAccountNo(accountNo);
				response.setBranchId(branchId);
				response.setCurrency(currency);

			} else {
				logger.info("FAILURE RESPONSE");

				if (ress.getBody().getError() != null) {

					if (ress.getBody().getError().getFiSystemException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFiSystemException().getErrorDetail().getErrorDesc(), null,
								Locale.US, balInqRequest, balInqResponse, ProductConstants.BALANCE_INQUIRY);
					} else if (ress.getBody().getError().getFIBusinessException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFIBusinessException().getErrorDetail().getErrorDesc(),
								null, Locale.US, balInqRequest, balInqResponse, ProductConstants.BALANCE_INQUIRY);
					}

				}

			}

			return response;

		} catch (NeoException e) {
			logger.info("Exception Occured ::" + e.getDetailMessage());
			throw e;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					balInqRequest, balInqResponse, ProductConstants.BALANCE_INQUIRY);
		}
	}

	@Override
	public FetchAccountStatemetResponse fetchAccountStatement(FetchAccountStatementRequest request)
			throws NeoException {
		StringWriter stringWriter = new StringWriter();
		String bankId = BANKID;
		FetchAccountStatementRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		FetchAccountStatemetResponse response = new FetchAccountStatemetResponse();
		String fetchAccountStatementRequest = null;
		String fetchAccountStatementResponse = null;

		try {

			if (StringUtils.isEmpty(request.getAccountNo()))
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.ACCOUNTINFO_INCORRECT, null, null, Locale.US,
						null, null, ProductConstants.FETCH_TRANSACTION);

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			Header header = commonSerivce.buildHeader(SbmPropertyConstants.FETCH_TRANSACTION);

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date fromdate = format.parse(request.getFromDate());
			Date toDate = format.parse(request.getToDate());

			String bankfromDate = DateUtilityFunction.dateInFormat(fromdate,
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);
			String banktoDate = DateUtilityFunction.dateInFormat(toDate,
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			String postedDate = null;
			String txnDate = null;
			PaginationDetails paginationDetail = null;
			PaginatedAccountTransactionCriteria accountTransactionCriteria = null;

			if (request.getLastTxnDate() != null && request.getLastBankTxnId() != null) {

				postedDate = DateUtilityFunction.dateInFormat(request.getLastTxnDate(),
						SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);
				txnDate = DateUtilityFunction.dateInFormat(request.getLastValueDate(),
						SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);
				FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest.PaginatedAccountTransactionCriteria.PaginationDetails
						.builder().lastPstdDate(postedDate).lastTxnId(request.getLastBankTxnId()).lastTxnDate(txnDate);

				LastBalance lastBalance = FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest.PaginatedAccountTransactionCriteria.PaginationDetails.LastBalance
						.builder().amountValue(request.getLastBalance()).currencyCode(request.getLastBalanceCurrency())
						.build();

				paginationDetail = FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest.PaginatedAccountTransactionCriteria.PaginationDetails
						.builder().lastPstdDate(postedDate).lastTxnDate(txnDate).lastTxnId(request.getLastBankTxnId())
						.lastBalance(lastBalance).lastTxnSrlNo(request.getLastSerialNo()).build();
			}

			if (paginationDetail != null) {
				accountTransactionCriteria = FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest.PaginatedAccountTransactionCriteria
						.builder().acid(request.getAccountNo()).branchId(request.getBranchId()).fromDate(bankfromDate)
						.toDate(banktoDate).paginationDetails(paginationDetail).build();
			} else {
				accountTransactionCriteria = FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest.PaginatedAccountTransactionCriteria
						.builder().acid(request.getAccountNo()).branchId(request.getBranchId()).fromDate(bankfromDate)
						.toDate(banktoDate).build();
			}

			GetFullAccountStatementWithPaginationRequest paginatedRequest = FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest
					.builder().paginatedAccountTransactionCriteria(accountTransactionCriteria).build();

			com.neo.aggregator.bank.sbm.core.request.FetchAccountStatementRequestDto.Body body = FetchAccountStatementRequestDto.Body
					.builder().getFullAccountStatementWithPaginationRequest(paginatedRequest).build();

			requestDto = FetchAccountStatementRequestDto.builder().header(header).body(body).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getfetchAcctStatementRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.FETCH_TRANSACTION_XSD);
			m.marshal(requestDto, sw);

			fetchAccountStatementRequest = sw.toString();

			logger.info("Actual Request ::" + fetchAccountStatementRequest);

			String encryptedPayload = encryptionUtil.encrypt(fetchAccountStatementRequest);

			RequestInDto retailCustAddRequestDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1014").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(retailCustAddRequestDto, stringWriter);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1014";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.FETCH_TRANSACTION);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.FETCH_TRANSACTION);

			fetchAccountStatementResponse = decryptESBResponse(responseBody, ProductConstants.FETCH_TRANSACTION, sessionId);

			logger.info("Decrypted Response ::" + fetchAccountStatementResponse);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getfetchAcctStatementResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(fetchAccountStatementResponse);
			FetchAccountStatementResponseDto ress = (FetchAccountStatementResponseDto) um
					.unmarshal(decryptedResponsereader);

			if (ress.getHeader().getResponseHeader().getHostTransaction().getStatus().equals("SUCCESS")) {

				logger.info("SUCCESS RESPONSE");
				PaginatedAccountStatement paginatedResp = ress.getBody()
						.getGetFullAccountStatementWithPaginationResponse().getPaginatedAccountStatement();

				String availableBalance = paginatedResp.getAccountBalances().getAvailableBalance().getAmountValue();
				String currencyCode = paginatedResp.getAccountBalances().getAvailableBalance().getCurrencyCode();
				String accountId = paginatedResp.getAccountBalances().getAcid();
				String branchId = paginatedResp.getAccountBalances().getBranchId();
				String ledgerBalance = paginatedResp.getAccountBalances().getLedgerBalance().getAmountValue();

				String hasMoreData = StringUtils.isEmpty(paginatedResp.getHasMoreData()) ? "N"
						: paginatedResp.getHasMoreData();

				List<TransactionDetails> txnDetails = paginatedResp.getTransactionDetails();

				List<Transaction> txnList = new ArrayList<>();
				SimpleDateFormat formattxnDate = new SimpleDateFormat(SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

				for (TransactionDetails txDetails : txnDetails) {

					Date fTxnDate = formattxnDate.parse(txDetails.getPstdDate());
					Date fValueDate = formattxnDate.parse(txDetails.getValueDate());

					Transaction txn = new Transaction();
					TransactionSummary txnSummary = txDetails.getTransactionSummary();
					TxnBalance transactionBalance = txDetails.getTxnBalance();
					txn.setAmount(txnSummary.getTxnAmt().getAmountValue());
					txn.setBalance(transactionBalance.getAmountValue());
					txn.setDescription(txnSummary.getTxnDesc());

					txn.setTime(fTxnDate.getTime());
					txn.setTxnCategory(txDetails.getTxnCat());

					txn.setTxnDate(fTxnDate);
					txn.setTxnId(txDetails.getTxnId());
					txn.setSerialNo(txDetails.getTxnSrlNo());
					txn.setValueDate(fValueDate);

					if (txnSummary.getTxnType().equals("C"))
						txn.setType("CREDIT");
					else if (txnSummary.getTxnType().equals("D"))
						txn.setType("DEBIT");

					txn.setCurrency(txnSummary.getTxnAmt().getCurrencyCode());

					txnList.add(txn);
				}

				response.setHasMoreData(hasMoreData);
				response.setAccountCurrency(currencyCode);
				response.setAccountId(accountId);
				response.setAvailableBalance(availableBalance);
				response.setLedgerBalance(ledgerBalance);
				response.setBranchId(branchId);
				response.setTransaction(txnList);

			} else {
				logger.info("FAILURE RESPONSE");

				if (ress.getBody().getError() != null) {

					if (ress.getBody().getError().getFiSystemException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFiSystemException().getErrorDetail().getErrorDesc(), null,
								Locale.US, fetchAccountStatementRequest, fetchAccountStatementResponse,
								ProductConstants.FETCH_TRANSACTION);
					} else if (ress.getBody().getError().getFIBusinessException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFIBusinessException().getErrorDetail().getErrorDesc(),
								null, Locale.US, fetchAccountStatementRequest, fetchAccountStatementResponse,
								ProductConstants.FETCH_TRANSACTION);
					}
				}
			}

			return response;

		} catch (NeoException e) {

			throw e;

		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					fetchAccountStatementRequest, fetchAccountStatementResponse, ProductConstants.FETCH_TRANSACTION);
		}
	}

	@Override
	public AccountCreationResponseDto termDepositCreation(AccountCreationRequestDto request) throws NeoException {
		StringWriter stringWriter = new StringWriter();
		String bankId = BANKID;
		TdAcctAddRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		AccountCreationResponseDto response = new AccountCreationResponseDto();
		AccountsDto accountInfo = null;
		NeoAccountCreationReqResModel accountReqResModel = new NeoAccountCreationReqResModel();

		NomineeDetailsDto nomineeDetails = null;
		AddressDto addressDto = null;
		String tdAccountCreationRequest = null;
		String tdAccountCreationResponse = null;

		try {

			if (request.getAccountInfo() != null && request.getAccountInfo().size() > 0) {
				accountInfo = request.getAccountInfo().get(0);
			} else
			{
				logger.info("Account Details Idenified with Provided paymentref Number : ");
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.ACCOUNTINFO_INCORRECT, null, null, Locale.US,
						null, null, ProductConstants.TD_CREATION);
			}
			List<NeoAccountCreationReqResModel> neoCreationList = neoAccountCreationDao
					.findByEntityIdAndStatusAndTenantAndPaymentRef(request.getEntityId(), TenantContextHolder.getNeoTenant(), ProductConstants.NEO_SUCCESS, accountInfo.getPaymentRef());

			if (neoCreationList != null && !neoCreationList.isEmpty()) {
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.REGISTRATION_PRODUCT_ALREADY_EXISTS,
						null, null, Locale.US, null, null, ProductConstants.PRODUCT_REGISTRATION);
			}

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			String channelIdCBS = commonSerivce.getCBSChannelId(TenantContextHolder.getNeoTenant());

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			Header header = commonSerivce.buildHeader(SbmPropertyConstants.TD_FIXED_ACCOUNT_OPENING);

			com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.CustId cifNumber = TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.CustId
					.builder().custId(request.getCifNumber()).build();

			TDAcctIdBuilder accountIdBuilder = TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TDAcctId.builder();

			TDAcctId tdAccId = accountIdBuilder
					.acctType(TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TDAcctId.AcctType.builder()
							.schmCode(accountInfo.getSchemeCode()).build())
					.acctCurr(accountInfo.getAccountsCurrency()).build();

			TDAcctGenInfo tdGenInfo = TDAcctGenInfo.builder().acctStmtMode("N").despatchMode("N").build();

			InitialDeposit initialDeposit = InitialDeposit.builder().amountValue(request.getAmount())
					.currencyCode(accountInfo.getAccountsCurrency()).build();

			DepositTerm depositTerm = DepositTerm.builder().days("").months(request.getDepositMonths()).build();

			RepayAcctId repayAccId = RepayAcctId.builder().acctId(request.getDebitAccount()).acctType("").bankInfo("")
					.build();

			String accountClosureOnMaturity = "N";

			if (request.getAutoClosureOnMaturity() != null && !request.getAutoClosureOnMaturity())
				accountClosureOnMaturity = "N";

			TDAcctAddRq tdAcctAddReq = null;

			RenewalTerm renewalTerm = null;

			NeoBusinessCustomField tdCreationRenewalDays = customFieldService.findByFieldNameAndTenant(
					NeoCustomFieldConstants.TD_CREATION_RENEWAL_TENURE_DAYS, TenantContextHolder.getNeoTenant());

			String depositMonths = accountInfo.getDepositMonths()!=null ? Integer.toString(accountInfo.getDepositMonths()) : "12";
			
			if (tdCreationRenewalDays != null && tdCreationRenewalDays.getFieldValue() != null) {

				logger.info(
						"Custom Tenure Days Configuration is Identified : " + tdCreationRenewalDays.getFieldValue());

				renewalTerm = RenewalTerm.builder().days(tdCreationRenewalDays.getFieldValue()).months(depositMonths).build();

			} else {

				logger.info("Default Tenure Days Configuration is Identified : " + request.getDepositMonths());
				renewalTerm = RenewalTerm.builder().days("0").months(depositMonths).build();
			}

			RenewalDtls renewalDtls = RenewalDtls.builder().autoCloseOnMaturityFlg(accountClosureOnMaturity)
					.autoRenewalflg("U").renewalTerm(renewalTerm).renewalSchm("").genLedgerSubHead("")
					.renewalOption("M").renewalAmt("").renewalAddnlAmt("").build();

			DebitAcctId debitAccountId = DebitAcctId.builder().acctId(request.getDebitAccount()).acctType("")
					.bankInfo("").build();

			TrnDtls txnDtls = TrnDtls.builder().trnType("T").debitAcctId(debitAccountId).build();

			String modeOfOperation = "SELF";

			if ((request.getNomineeInfo() != null && request.getNomineeInfo().size() > 0)
					&& (request.getNomineeContactInfo() != null && request.getNomineeContactInfo().size() > 0)) {

				nomineeDetails = request.getNomineeInfo().get(0);

				com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineeInfoRecBuilder nomineeInfoBuilder = TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec
						.builder();

				com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineeContactInfo.NomineeContactInfoBuilder nomineeContactInfoBuilder = TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineeContactInfo
						.builder();

				com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineePercent nomineePercentage = TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineePercent
						.builder().value("100").build();

				com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineeContactInfo.PostAddr.PostAddrBuilder postalAddressBuilder = TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineeContactInfo.PostAddr
						.builder();

				addressDto = request.getNomineeContactInfo().get(0);

				GeoMasterSBM geoMaster = commonSerivce.getGeoMasterFromPinCode(addressDto.getPincode());
				
				NeoBusinessCustomField addressTrimRequired = customFieldService.findByFieldNameAndTenant(
						NeoCustomFieldConstants.ADDRESS_TRIM_REQUIRED, TenantContextHolder.getNeoTenant());

				NeoBusinessCustomField addressTrimLimit = customFieldService.findByFieldNameAndTenant(
						NeoCustomFieldConstants.ADDRESS_TRIM_LIMIT, TenantContextHolder.getNeoTenant());

				String addressLine1 = addressDto.getAddress1();
				String addressLine2 = addressDto.getAddress2();
				String addressLine3 = addressDto.getAddress3();

				if (addressTrimRequired != null && addressTrimRequired.getFieldValue() != null
						&& addressTrimRequired.getFieldValue().equals("Y")) {
					
					logger.info("Address Trim is Enabled");

					Integer limit = 45;

					if (addressTrimLimit != null) {

						try {
							limit = Integer.parseInt(addressTrimLimit.getFieldValue());
						} catch (NumberFormatException e) {
							logger.info("Limit is not a valid integer");
						}
					}

					addressLine1 = StringUtilsNeo.splitStringByLength(addressLine1, limit).size() > 0
							? StringUtilsNeo.splitStringByLength(addressLine1, limit).get(0)
							: addressLine1;
					addressLine2 = StringUtilsNeo.splitStringByLength(addressLine2, limit).size() > 0
							? StringUtilsNeo.splitStringByLength(addressLine2, limit).get(0)
							: addressLine2;
					addressLine3 = StringUtilsNeo.splitStringByLength(addressLine3, limit).size() > 0
							? StringUtilsNeo.splitStringByLength(addressLine3, limit).get(0)
							: addressLine3;
				}

				com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineeContactInfo.PostAddr postalAddress = postalAddressBuilder
						.addr1(addressLine1).addr2(addressLine2).addr3(addressLine3)
						.city(geoMaster.getDistrict()).stateProv(geoMaster.getState()).country(geoMaster.getCountry())
						.postalCode(addressDto.getPincode()).build();

				com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineeContactInfo nomineeContactInfo = nomineeContactInfoBuilder
						.postAddr(postalAddress).build();

				String isMinor = "N";

				if (nomineeDetails.getIsMinor()) {
					isMinor = "Y";
				}

				String dobFormatted = DateUtilityFunction.dateInFormat(nomineeDetails.getNomineeDOB(),
						SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

				String relPartyId = commonSerivce
						.getRelPartyTypeByRelationShip(nomineeDetails.getNomineeRelationship());

				com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec nomineeInfoRec = nomineeInfoBuilder
						.nomineeContactInfo(nomineeContactInfo).regNum("0001")
						.nomineeName(nomineeDetails.getNomineeName()).nomineePercent(nomineePercentage)
						.nomineeBirthDt(dobFormatted).relType(relPartyId).nomineeMinorFlg(isMinor).build();

				tdAcctAddReq = TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.builder().custId(cifNumber)
						.tdAcctId(tdAccId).tdAcctGenInfo(tdGenInfo).initialDeposit(initialDeposit)
						.depositTerm(depositTerm).repayAcctId(repayAccId).renewalDtls(renewalDtls).trnDtls(txnDtls)
						.nomineeInfoRec(nomineeInfoRec).build();

			} else {
				tdAcctAddReq = TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.builder().custId(cifNumber)
						.tdAcctId(tdAccId).tdAcctGenInfo(tdGenInfo).initialDeposit(initialDeposit)
						.depositTerm(depositTerm).repayAcctId(repayAccId).renewalDtls(renewalDtls).trnDtls(txnDtls)
						.build();
			}

			TDAcctAddCustomData addCustomData = TDAcctAddCustomData.builder().xferind("O")
					.critsolid(accountInfo.getBranchId()).trancremode("O").intcracct(request.getDebitAccount())
					.modeofoperation(modeOfOperation).chnlid(channelIdCBS).build();

			TDAcctAddRequest tdAddRequest = TDAcctAddRequest.builder().tdAcctAddRq(tdAcctAddReq)
					.tdAcctAddCustomData(addCustomData).build();

			com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto.Body requestBody = TdAcctAddRequestDto.Body
					.builder().tdAcctAddRequest(tdAddRequest).build();

			requestDto = TdAcctAddRequestDto.builder().header(header).body(requestBody).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getTdAcctAddRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.TD_FIXED_ACCOUNT_OPENING_XSD);
			m.marshal(requestDto, sw);

			tdAccountCreationRequest = sw.toString();

			logger.info("Actual payload ::" + tdAccountCreationRequest);

			String encryptedPayload = encryptionUtil.encrypt(tdAccountCreationRequest);

			RequestInDto retailCustAddRequestDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1024").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(retailCustAddRequestDto, stringWriter);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1024";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.TD_CREATION);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.TD_CREATION);

			tdAccountCreationResponse = decryptESBResponse(responseBody, ProductConstants.TD_CREATION, sessionId);

			logger.info("Decrypted Response ::" + tdAccountCreationResponse);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getTdAcctAddResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(tdAccountCreationResponse);
			TdAcctAddResponseDto ress = (TdAcctAddResponseDto) um.unmarshal(decryptedResponsereader);

			accountReqResModel.setRequestId(requestId);
			accountReqResModel.setAccountCurrency(accountInfo.getAccountsCurrency());

			accountReqResModel.setTenant(TenantContextHolder.getNeoTenant());
			accountReqResModel.setBankId(BANKID);
			accountReqResModel.setEntityId(request.getEntityId());
			accountReqResModel.setModeOfOperation(modeOfOperation);
			accountReqResModel.setSchemeCode(accountInfo.getSchemeCode());
			accountReqResModel.setSolId(accountInfo.getBranchId());
			accountReqResModel.setProductId(ProductConstants.TD_FIXED_ACCOUNT_OPENING);
			accountReqResModel.setAmount(accountInfo.getAmount());
			accountReqResModel.setPaymentRef(accountInfo.getPaymentRef());

			if (ress.getHeader().getResponseHeader().getHostTransaction().getStatus().equals("SUCCESS")) {

				logger.info("SUCCESS RESPONSE");
				String accountId = ress.getBody().getTDAcctAddResponse().getTDAcctAddRs().getTDAcctId().getAcctId();
				response.setDescription("Account Creation Successful");
				response.setStatus("SUCCESS");
				response.setAccountId(accountId);

				accountReqResModel.setStatus("SUCCESS");
				accountReqResModel.setResponseDescription("Account Creation Successful");
				accountReqResModel.setAccountId(accountId);

			} else {
				logger.info("FAILURE RESPONSE");

				if (ress.getBody().getError() != null) {

					accountReqResModel.setStatus("FAILURE");

					if (ress.getBody().getError().getFiSystemException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFiSystemException().getErrorDetail().getErrorDesc(), null,
								Locale.US, tdAccountCreationRequest, tdAccountCreationResponse,
								ProductConstants.TD_CREATION);
					} else if (ress.getBody().getError().getFIBusinessException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFIBusinessException().getErrorDetail().getErrorDesc(),
								null, Locale.US, tdAccountCreationRequest, tdAccountCreationResponse,
								ProductConstants.TD_CREATION);
					}

				}

			}
			return response;

		} catch (NeoException e) {
			accountReqResModel.setStatus("FAILURE");
			accountReqResModel.setResponseDescription(e.getDetailMessage());
			throw e;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			accountReqResModel.setStatus("FAILURE");
			accountReqResModel.setResponseDescription("Exception occured ::" + e.getMessage());
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					tdAccountCreationRequest, tdAccountCreationResponse, ProductConstants.TD_CREATION);
		} finally {
			auditService.saveAccountCreation(accountReqResModel);
		}
	}

	@Override
	public RegistrationResponseDto register(RegistrationRequestV2Dto request) throws NeoException {

		String tenant = TenantContextHolder.getNeoTenant();

		PartnerMasterConfiguration masterConfig = partnerConfig.findByTenant(tenant);
		RegistrationResponseDto registrationResponse = new RegistrationResponseDto();
		try {

			String cifNumber = null;
			String accountNo = null;

			NeoCustomerData customerData = fetchCustomerData(request);

			if (customerData != null && customerData.getCifNumber() != null) {
				logger.info("Customer Id Already Present");
				cifNumber = customerData.getCifNumber();
			} else {
				logger.info("Registring new customer id");
				RegistrationResponseDto cifResponse = this.retailCifCreation(request);
				cifNumber = cifResponse.getCustomerId();

				if (StringUtils.isEmpty(cifNumber)) {
					throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.INVALID_CIF_RESPONSE, null, null,
							Locale.US, null, null, ProductConstants.PRODUCT_REGISTRATION);
				}

				request.setCifNumber(cifNumber);
				updateCustomerData(request);
			}

			request.setCifNumber(cifNumber);

			String regProduct = masterConfig.getOnRegistrationProduct();

			if (StringUtils.isNotBlank(regProduct)) {

				Boolean isRegProductExists = Boolean.FALSE;

				List<NeoAccountCreationReqResModel> neoCreationList = neoAccountCreationDao
						.findByEntityIdAndStatusAndTenant(request.getEntityId(), ProductConstants.NEO_SUCCESS,tenant);

				if (neoCreationList != null && !neoCreationList.isEmpty()) {
					throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.REGISTRATION_PRODUCT_ALREADY_EXISTS,
							null, null, Locale.US, null, null, ProductConstants.PRODUCT_REGISTRATION);
				}

				List<NeoAccountDetails> accountDetails = fetchCustomerAccountData(request);

				if (accountDetails != null && !accountDetails.isEmpty()) {

					for (NeoAccountDetails accDetails : accountDetails) {
						if (accDetails.getModeOfCreation() != null && accDetails.getModeOfCreation().equals("REG")) {
							logger.info("Product Already created with mode REG");
							isRegProductExists = Boolean.TRUE;
						}
					}

				}

				if (isRegProductExists)
					throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.REGISTRATION_PRODUCT_ALREADY_EXISTS,
							null, null, Locale.US, null, null, ProductConstants.PRODUCT_REGISTRATION);

				request.setProductId(regProduct);
				RegistrationResponseDto addProdResponse = this.addProduct(request);

				if (addProdResponse != null && addProdResponse.getStatus() != null) {

					if (addProdResponse.getStatus().equals("SUCCESS")) {
						accountNo = addProdResponse.getAccountId();
						registrationResponse.setAccountId(accountNo);
						registrationResponse.setCustomerId(cifNumber);
						registrationResponse.setStatus("SUCCESS");
						registrationResponse.setDescription("Registration Success");
						registrationResponse.setEntityId(request.getEntityId());
					} else {
						registrationResponse.setCustomerId(cifNumber);
						registrationResponse.setStatus("FAILURE");
						registrationResponse.setDescription(addProdResponse.getDescription());
						registrationResponse.setEntityId(request.getEntityId());
					}
				} else {
					logger.info("Invalid Add product Configuration");
				}

				return registrationResponse;
			}

			registrationResponse.setCustomerId(cifNumber);
			registrationResponse.setStatus("SUCCESS");
			registrationResponse.setDescription("Registration Created");
			registrationResponse.setEntityId(request.getEntityId());

		} catch (NeoException ex) {
			throw ex;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			registrationResponse.setDescription("Registration Failed");
			registrationResponse.setStatus("FAILURE");
		}

		return registrationResponse;
	}

	@Override
	public RegistrationResponseDto addProduct(RegistrationRequestV2Dto request) throws NeoException {

		String tenant = TenantContextHolder.getNeoTenant();

		RegistrationResponseDto addProductResponse = new RegistrationResponseDto();

		try {

			ProductConfig productConfigModel = productConfigDao.findByPartnerAndProduct(tenant, request.getProductId());

			if (productConfigModel.getProduct().equals(ProductConstants.SB_CREATION)) {

				AccountCreationRequestDto savingAccountCreateRequest = new AccountCreationRequestDto();

				String branchId = productConfigModel.getBranchId();
				String schemeCode = productConfigModel.getDefaultSchemeCode();
				String accountCurr = productConfigModel.getDefaultAccountCurrency();

				for (AccountsDto accounts : request.getAccountInfo()) {

					if (accounts.getBranchId() == null)
						accounts.setBranchId(branchId);

					if (accounts.getAccountsCurrency() == null)
						accounts.setAccountsCurrency(accountCurr);

					if (accounts.getSchemeCode() == null)
						accounts.setSchemeCode(schemeCode);
				}

				savingAccountCreateRequest.setCifNumber(request.getCifNumber());
				savingAccountCreateRequest.setAccountInfo(request.getAccountInfo());
				savingAccountCreateRequest.setEntityId(request.getEntityId());
				savingAccountCreateRequest.setNomineeContactInfo(request.getAddressInfo());
				savingAccountCreateRequest.setNomineeInfo(request.getNomineeInfo());

				AccountCreationResponseDto response = this.savingsAccountCreation(savingAccountCreateRequest);
				addProductResponse.setAccountId(response.getAccountId());
				addProductResponse.setStatus("SUCCESS");
				addProductResponse.setDescription("Add Product Successful");
			}

			else if (productConfigModel.getProduct().equals(ProductConstants.TD_CREATION)) {

				AccountCreationRequestDto tdAccountCreation = new AccountCreationRequestDto();

				String branchId = productConfigModel.getBranchId();
				String schemeCode = productConfigModel.getDefaultSchemeCode();
				String accountCurr = productConfigModel.getDefaultAccountCurrency();

				Double depositAmount = null;
				Integer depositMonths = null;

				for (AccountsDto accounts : request.getAccountInfo()) {

					if (accounts.getBranchId() == null)
						accounts.setBranchId(branchId);

					if (accounts.getAccountsCurrency() == null)
						accounts.setAccountsCurrency(accountCurr);

					if (accounts.getSchemeCode() == null)
						accounts.setSchemeCode(schemeCode);

					depositAmount = accounts.getAmount();
					depositMonths = accounts.getDepositMonths();
				}
				
				if(depositMonths == null)
					depositMonths = 12;

				tdAccountCreation.setNomineeContactInfo(request.getAddressInfo());
				tdAccountCreation.setDebitAccount(productConfigModel.getDefaultAccountType());
				tdAccountCreation.setCifNumber(request.getCifNumber());
				tdAccountCreation.setAccountInfo(request.getAccountInfo());
				tdAccountCreation.setEntityId(request.getEntityId());
				tdAccountCreation.setDepositMonths(String.valueOf(depositMonths));
				tdAccountCreation.setAmount(depositAmount != null ? String.valueOf(depositAmount) : "0");
				tdAccountCreation.setNomineeContactInfo(request.getAddressInfo());
				tdAccountCreation.setNomineeInfo(request.getNomineeInfo());

				AccountCreationResponseDto response = this.termDepositCreation(tdAccountCreation);
				addProductResponse.setAccountId(response.getAccountId());
				addProductResponse.setStatus("SUCCESS");
				addProductResponse.setDescription("Add Product Successful");

				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.MONTH, depositMonths);
				Date endDate = cal.getTime();

				LienMarkRequestDto markLien = new LienMarkRequestDto();
				markLien.setEntityId(request.getEntityId());
				markLien.setAccountCurrency("INR");
				markLien.setAccountId(response.getAccountId());
				markLien.setStartDate(new Date());
				markLien.setEndDate(endDate);

				NeoBusinessCustomField tdLienModuleType = customFieldService.findByFieldNameAndTenant(
						NeoCustomFieldConstants.TD_LIEN_MODULE_TYPE, TenantContextHolder.getNeoTenant());

				NeoBusinessCustomField tdLienReasonCode = customFieldService.findByFieldNameAndTenant(
						NeoCustomFieldConstants.TD_LIEN_REASON_CODE, TenantContextHolder.getNeoTenant());

				String moduleType = tdLienModuleType != null ? tdLienModuleType.getFieldValue() : "ULIEN";

				String reasonCode = tdLienReasonCode != null ? tdLienReasonCode.getFieldValue() : "001";

				markLien.setReasonCode(reasonCode);
				markLien.setModuleType(moduleType);
				markLien.setRemarks("Lien mark add");
				markLien.setAmount(tdAccountCreation.getAmount());

				rmService.asyncCallLienMark(markLien, TenantContextHolder.getNeoTenant());
			}

			addProductResponse.setEntityId(request.getEntityId());

		} catch (NeoException e) {
			logger.info("Neo Exception Occured ::" + e.getDetailMessage());
			addProductResponse.setStatus("FAILURE");
			addProductResponse.setDescription(e.getDetailMessage());
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			addProductResponse.setStatus("FAILURE");
			addProductResponse.setDescription("Add Product Failed");
		}

		return addProductResponse;
	}

	private NeoCustomerData fetchCustomerData(RegistrationRequestV2Dto request) {

		NeoCustomerDataDto dto = new NeoCustomerDataDto();

		NeoCustomerData resp = null;

		dto.setEntityId(request.getEntityId());

		NeoResponse response = rmService.fetchData(dto, localUrl + "/neo-lego/common/fetchNeoCustomerData",
				TenantContextHolder.getNeoTenant(), HttpMethod.POST);

		ObjectMapper mapper = new ObjectMapper();

		resp = mapper.convertValue(response.getResult(), NeoCustomerData.class);

		return resp;
	}

	private List<NeoAccountDetails> fetchCustomerAccountData(RegistrationRequestV2Dto request) {

		NeoCustomerDataDto dto = new NeoCustomerDataDto();

		List<NeoAccountDetails> resp = new ArrayList<>();

		dto.setEntityId(request.getEntityId());

		NeoResponse response = rmService.fetchData(dto, localUrl + "/neo-lego/common/fetchNeoAccountData",
				TenantContextHolder.getNeoTenant(), HttpMethod.POST);

		ObjectMapper mapper = new ObjectMapper();

		resp = mapper.convertValue(response.getResult(), new TypeReference<List<NeoAccountDetails>>() {
		});

		return resp;
	}

	private NeoCustomerData updateCustomerData(RegistrationRequestV2Dto request) {

		NeoCustomerDataDto dto = new NeoCustomerDataDto();

		NeoCustomerData resp = null;

		dto.setEntityId(request.getEntityId());
		dto.setCifNumber(request.getCifNumber());
		dto.setCorporate(request.getBusinessType());

		NeoResponse response = rmService.fetchData(dto, localUrl + "/neo-lego/common/updateNeoCustomerData",
				TenantContextHolder.getNeoTenant(), HttpMethod.POST);

		ObjectMapper mapper = new ObjectMapper();

		resp = mapper.convertValue(response.getResult(), NeoCustomerData.class);
		return resp;
	}

	@Override
	public FundTransferResponseDto fundTransfer(FundTransferRequestDto request) throws NeoException {

		if (request.getTransactionType().equals("IMPS")) {
			return impsTransfer(request);
		} else if (request.getTransactionType().equals("IFT")) {
			return iftTransfer(request);
		} else {
			return addOutboundPymt(request);
		}
	}

	@Override
	public FundTransferResponseDto paymentStatusCheck(FundTransferRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public FundTransferResponseDto addOutboundPymt(FundTransferRequestDto request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		String bankId = BANKID;
		AddOutBoundPymtRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		FundTransferResponseDto response = new FundTransferResponseDto();
		String transactionRequest = null;
		String transactionResponse = null;
		String service = null;

		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			Header header = commonSerivce.buildOutboundPaymentHeader(SbmPropertyConstants.OUTBOUND_TRANSACTION);

			String bankTxnType = "";

			String url = null;
			PartnerServiceConfiguration serviceConfig = null;

			if (request.getTransactionType() != null && request.getTransactionType().equals("NEFT")) {
				bankTxnType = "NEFT";
				url = esbBaseUrl
						+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1034";
				service = ProductConstants.OUTBOUND_TRANSACTION_NEFT;
				serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(), service);
			} else if (request.getTransactionType() != null && request.getTransactionType().equals("RTGS")) {
				bankTxnType = "NRTGS";
				url = esbBaseUrl
						+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1035";
				service = ProductConstants.OUTBOUND_TRANSACTION_RTGS;
				serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(), service);
			}

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String requestDate = DateUtilityFunction.dateInFormat(new Date(),
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			RequestedAmount requestedAmt = RequestedAmount.builder().amountValue(request.getAmount())
					.currencyCode("INR").build();

			AddressDetails senderAddress = AddressDetails.builder().name(request.getRemitterName()).build();

			com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorDtls.AddressDetails beneficiaryAddress = com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorDtls.AddressDetails
					.builder().name(request.getBeneficiaryName()).build();

			DebtorDtls debtorDtls = DebtorDtls.builder().accountId(request.getFromAccountNo())
					.addressDetails(senderAddress).build();

			CreditorDtls creditorDtls = CreditorDtls.builder().accountId(request.getToAccountNo())
					.addressDetails(beneficiaryAddress).build();

			NetworkIdentification nwId = NetworkIdentification.builder().networkDirectory("IFSC")
					.networkIdentifier(request.getBeneficiaryIfsc()).build();

			CreditorBankDtls creditorBankId = CreditorBankDtls.builder().networkIdentification(nwId).build();

			RemittanceInfo remiterInfo = RemittanceInfo.builder().remitInfo1(request.getDescription()).build();

			AddOutboundPymtEntryDtlsRq outBountDtlsRq = AddOutboundPymtEntryDtlsRq.builder().paysysId(bankTxnType)
					.requestedExecutionDate(requestDate).requestedAmount(requestedAmt).purposeCode("CASH")
					.requestedAmountCrncy("INR").waiveChargeFlag("Y").serviceId("NCP").serviceType("O")
					.debtorDtls(debtorDtls).creditorBankDtls(creditorBankId).creditorDtls(creditorDtls)
					.tranIdentification(request.getExternalTransactionId()).remittanceInfo(remiterInfo).build();

			AddOutboundPymtEntryDtlsRequest outboundReq = AddOutboundPymtEntryDtlsRequest.builder()
					.addOutboundPymtEntryDtlsRq(outBountDtlsRq).build();

			com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto.Body body = AddOutBoundPymtRequestDto.Body
					.builder().addOutboundPymtEntryDtlsRequest(outboundReq).build();

			requestDto = AddOutBoundPymtRequestDto.builder().header(header).body(body).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getAddOutBoundPymtRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.OUTBOUND_TRANSACTION_XSD);
			m.marshal(requestDto, sw);

			transactionRequest = sw.toString();

			logger.info("Actual payload ::" + transactionRequest);

			String encryptedPayload = encryptionUtil.encrypt(transactionRequest);

			RequestInDto retailCustAddRequestDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1034").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(retailCustAddRequestDto, stringWriter);

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, service);

			transactionResponse = decryptESBResponse(responseBody, service, sessionId);

			logger.info("Decrypted Response ::" + transactionResponse);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getAddOutBoundPymtResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(transactionResponse);
			AddOutBoundPymtResponseDto ress = (AddOutBoundPymtResponseDto) um.unmarshal(decryptedResponsereader);

			if (ress.getHeader().getResponseHeader().getHostTransaction().getStatus().equals("SUCCESS")) {

				logger.info("SUCCESS RESPONSE");
				String tranIdentification = ress.getBody().getAddOutboundPymtEntryDtlsResponse()
						.getAddOutboundPymtEntryDtlsRs().getTranIdentification();
				response.setBankReferenceNo(tranIdentification);
				response.setStatus("SUCCESS");
				response.setExternalTransactionId(request.getExternalTransactionId());
			} else {
				logger.info("FAILURE RESPONSE");

				if (ress.getBody().getError() != null) {

					if (ress.getBody().getError().getFiSystemException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFiSystemException().getErrorDetail().getErrorDesc(), null,
								Locale.US, transactionRequest, transactionResponse, service);
					} else if (ress.getBody().getError().getFIBusinessException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFIBusinessException().getErrorDetail().getErrorDesc(),
								null, Locale.US, transactionRequest, transactionResponse, service);
					}
				}

			}

			return response;

		} catch (NeoException e) {
			logger.info("Exception Occured ::" + e.getDetailMessage());
			throw e;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					transactionRequest, transactionResponse, service);
		}
	}

	@Override
	public FundTransferResponseDto impsTransfer(FundTransferRequestDto request) throws NeoException {

		StringWriter sw = new StringWriter();
		FundTransferResponseDto responseDto = new FundTransferResponseDto();
		String bankId = BANKID;
		String impsTransactionRequest = null;
		String impsTransactionResponse = null;

		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);
			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();
			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			IMPSRequestDto impsReqDto = new IMPSRequestDto();
			impsReqDto.setREMITTERACC(request.getFromAccountNo());
			impsReqDto.setREMITTERNAME(request.getRemitterName());

			if (request.getRemitterNumber() != null && request.getRemitterNumber().contains("+91"))
				request.setRemitterNumber(request.getRemitterNumber().replace("+91", ""));

			impsReqDto.setMobileNumber(request.getRemitterNumber());
			impsReqDto.setTXNAMT(request.getAmount());
			impsReqDto.setBENIFICIARYACC(request.getToAccountNo());
			impsReqDto.setIsMobileFT("false");
			impsReqDto.setIsAccountFT("false");
			impsReqDto.setRemark(request.getDescription());
			impsReqDto.setMSGID(requestId);
			impsReqDto.setTransType("OutWardFundTransfer");
			impsReqDto.setIsBLQ("false");
			impsReqDto.setIsMini("false");
			impsReqDto.setIsCC("false");
			impsReqDto.setIsPayment("false");
			impsReqDto.setIsMerchant("false");
			impsReqDto.setRDTDAmount("0");
			impsReqDto.setPenaltiAmount("0");
			impsReqDto.setIsMisc("false");
			impsReqDto.setIFSC(request.getBeneficiaryIfsc());
			impsReqDto.setIsBranch("true");

			JAXBContext jaxbContext = NeoJAXBContextFactory.getImpsRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.marshal(impsReqDto, sw);

			impsTransactionRequest = sw.toString();

			logger.info("Plain text ::" + impsTransactionRequest);

			String encryptedPayload = encryptionUtil.encrypt(impsTransactionRequest);
			
			NeoBusinessCustomField  impsModeCustomField =   customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.IMPS_MODE, TenantContextHolder.getNeoTenant());

			String impsMode = impsModeCustomField !=null ? impsModeCustomField.getFieldValue() : "INTERNET";
			
			String authType = "3";
			String impsProductConstant = ProductConstants.OUTBOUND_TRANSACTION_IMPS;

			switch (impsMode) {
			
			case "INTERNET":
				break;
			
			case "MOBILE":
				authType = "1";
				impsProductConstant = ProductConstants.OUTBOUND_TRANSACTION_IMPS_MOBILE;
				break;
			
			case "BRANCH":
				authType = "2";
				impsProductConstant = ProductConstants.OUTBOUND_TRANSACTION_IMPS_BRANCH;
				break;
				
			case "IMPS_FIR":
				authType = "IMPS_FIR";
				impsProductConstant = ProductConstants.OUTBOUND_TRANSACTION_IMPS_FIR;
				break;

			default:
				break;
			}
			
			
			RequestInDto req = RequestInDto.builder().authType(authType).channelId(channelId).moduleID("")
					.partnerReqID(requestId).partnerUserName(configModel.getUserName()).timestamp(timeStamp)
					.requestType("IMPS").request(encryptedPayload).build();

			sw = new StringWriter();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(req, sw);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-AHTTP_IMPS%23IMPS_MobileTransfer";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					impsProductConstant);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(sw.toString(), url, HttpMethod.POST, headers, Boolean.TRUE,
					sslAlias, sslEnabled, ProductConstants.OUTBOUND_TRANSACTION_IMPS);

			impsTransactionResponse = decryptESBResponse(responseBody, ProductConstants.OUTBOUND_TRANSACTION_IMPS, sessionId);

			logger.info("Decrypted Response ::" + impsTransactionResponse);

			IMPSResponseDto impsResponse = new ObjectMapper().readValue(impsTransactionResponse, IMPSResponseDto.class);

			if (impsResponse != null && impsResponse.getResponsecode() != null
					&& impsResponse.getResponsecode().equals("00")) {
				responseDto.setExternalTransactionId(request.getExternalTransactionId());
				responseDto.setDescription(impsResponse.getResponsedesc());
				responseDto.setBankReferenceNo(impsResponse.getReferencenumber());
				responseDto.setStatus("SUCCESS");
			} else {
				responseDto.setStatus("FAILURE");
			}

		} catch (NeoException e) {
			logger.info("Exception Occured ::" + e.getDetailMessage());
			throw e;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					impsTransactionRequest, impsTransactionResponse, ProductConstants.OUTBOUND_TRANSACTION_IMPS);
		}

		return responseDto;
	}

	@Override
	public FundTransferResponseDto iftTransfer(FundTransferRequestDto request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		String bankId = BANKID;
		IFTTransferRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		FundTransferResponseDto response = new FundTransferResponseDto();
		String internalTransferRequest = null;
		String internalTransferResponse = null;

		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			Header header = commonSerivce.buildHeader(SbmPropertyConstants.INTERNAL_TRANSFER);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1016";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.INTERNAL_TRANSFER);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			XferTrnHdr xferTrnHdr = XferTrnHdr.builder().trnType("T").trnSubType("CI").build();

			com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec.AcctId fromAccountId = com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec.AcctId
					.builder().acctId(request.getFromAccountNo()).build();

			TrnAmt txnAmount = TrnAmt.builder().amountValue(request.getAmount())
					.currencyCode(request.getTransactionCurrency()).build();

			String txnDate = DateUtilityFunction.dateInFormat(new Date(),
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			String debitTrnParticulars = this.checkLength(request.getBeneficiaryName(), null,
					request.getExternalTransactionId(), request.getDescription());

			PartTrnRec debitReq = PartTrnRec.builder().acctId(fromAccountId).creditDebitFlg("D").trnAmt(txnAmount)
					.trnParticulars(debitTrnParticulars != null ? debitTrnParticulars
							: "IFT" + "/" + request.getExternalTransactionId() + "/" + request.getBeneficiaryName()
									+ "/" + request.getDescription())
					.partTrnRmks(request.getDescription()).valueDt(txnDate).build();

			com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec.AcctId toAccountId = com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec.AcctId
					.builder().acctId(request.getToAccountNo()).build();

			String creditTrnParticulars = this.checkLength(null, request.getRemitterName(),
					request.getExternalTransactionId(), request.getDescription());

			PartTrnRec creditReq = PartTrnRec.builder().acctId(toAccountId).creditDebitFlg("C").trnAmt(txnAmount)
					.trnParticulars(creditTrnParticulars != null ? creditTrnParticulars
							: "IFT" + "/" + request.getExternalTransactionId() + "/" + request.getRemitterName() + "/"
									+ request.getDescription())
					.partTrnRmks(request.getDescription()).valueDt(txnDate).build();

			List<PartTrnRec> partTrnRecList = new ArrayList<>();
			partTrnRecList.add(creditReq);
			partTrnRecList.add(debitReq);

			XferTrnDetail xTrndetails = XferTrnDetail.builder().partTrnRec(partTrnRecList).build();

			XferTrnAddRq xTrnAddRq = XferTrnAddRq.builder().xferTrnDetail(xTrndetails).xferTrnHdr(xferTrnHdr).build();

			XferTrnAddRequest xTrnAddRequest = XferTrnAddRequest.builder().xferTrnAddRq(xTrnAddRq).build();

			requestDto = IFTTransferRequestDto.builder()
					.body(com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.builder()
							.xferTrnAddRequest(xTrnAddRequest).build())
					.header(header).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getIftTransferRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.INTERNAL_TRANSFER_XSD);
			m.marshal(requestDto, sw);

			internalTransferRequest = sw.toString();

			logger.info("Actual payload ::" + internalTransferRequest);

			String encryptedPayload = encryptionUtil.encrypt(internalTransferRequest);

			RequestInDto internalTransferDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1016").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(internalTransferDto, stringWriter);

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.INTERNAL_TRANSFER);

			internalTransferResponse = decryptESBResponse(responseBody, ProductConstants.INTERNAL_TRANSFER, sessionId);

			logger.info("Decrypted Response ::" + internalTransferResponse);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getIftTransferResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(internalTransferResponse);
			IFTTransferResponseDto iftTransactionResponseDto = (IFTTransferResponseDto) um
					.unmarshal(decryptedResponsereader);

			if (iftTransactionResponseDto.getHeader().getResponseHeader().getHostTransaction().getStatus()
					.equals("SUCCESS")) {

				logger.info("SUCCESS RESPONSE");
				String tranIdentification = iftTransactionResponseDto.getBody().getXferTrnAddResponse()
						.getXferTrnAddRs().getTrnIdentifier().getTrnId();
				response.setBankReferenceNo(tranIdentification);
				response.setStatus("SUCCESS");
				response.setExternalTransactionId(request.getExternalTransactionId());
			} else {
				logger.info("FAILURE RESPONSE");

				if (iftTransactionResponseDto.getBody().getError() != null) {

					if (iftTransactionResponseDto.getBody().getError().getFiSystemException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								iftTransactionResponseDto.getBody().getError().getFiSystemException().getErrorDetail()
										.getErrorDesc(),
								null, Locale.US, internalTransferRequest, internalTransferResponse,
								ProductConstants.INTERNAL_TRANSFER);
					} else if (iftTransactionResponseDto.getBody().getError().getFIBusinessException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								iftTransactionResponseDto.getBody().getError().getFIBusinessException().getErrorDetail()
										.getErrorDesc(),
								null, Locale.US, null, null, ProductConstants.INTERNAL_TRANSFER);
					}

				}

			}

			return response;

		} catch (NeoException e) {
			logger.info("Exception Occured ::" + e.getDetailMessage());
			throw e;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					internalTransferRequest, internalTransferResponse, ProductConstants.INTERNAL_TRANSFER);
		}
	}

	@Override
	public AccountModificationResponseDto savingsAccountModification(AccountModificationRequestDto request)
			throws NeoException {

		StringWriter stringWriter = new StringWriter();
		SBAcctModRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		AccountModificationResponseDto response = new AccountModificationResponseDto();
		AccountsDto accountInfo = new AccountsDto();
		AddressDto addressDto = new AddressDto();
		NomineeDetailsDto nomineeDetails = new NomineeDetailsDto();
		String savingsAccountModRequest = null;
		String savingsAccountModResponse = null;

		try {

			if (request.getAccountInfo() != null && request.getAccountInfo().size() > 0) {
				accountInfo = request.getAccountInfo().get(0);
			} else
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.ACCOUNTINFO_INCORRECT, null, null, Locale.US,
						null, null, ProductConstants.SAVINGS_ACCOUNT_MODIFICATION);

			if (request.getNomineeContactInfo() != null && request.getNomineeContactInfo().size() > 0) {
				addressDto = request.getNomineeContactInfo().get(0);
			} else
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.NOMINEE_CONTACTINFO_INCORRECT, null, null,
						Locale.US, null, null, ProductConstants.SAVINGS_ACCOUNT_MODIFICATION);

			if (request.getNomineeInfo() != null && request.getNomineeInfo().size() > 0) {
				nomineeDetails = request.getNomineeInfo().get(0);
			} else
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.NOMINEE_INFO_INCORRECT, null, null,
						Locale.US, null, null, ProductConstants.SAVINGS_ACCOUNT_MODIFICATION);

			MasterConfiguration configModel = masterConfig.findByPartnerId(TenantContextHolder.getNeoTenant());

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			Header header = commonSerivce.buildHeader(SbmPropertyConstants.SAVINGS_ACCOUNT_MODIFICATION);

			com.neo.aggregator.bank.sbm.core.request.SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.SBAcctId sbAccountId = SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.SBAcctId
					.builder().acctId(accountInfo.getAccountNo()).build();

			NomineeInfoRecBuilder nomineeInfoBuilder = SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec
					.builder();

			NomineeContactInfoBuilder nomineeContactInfoBuilder = SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineeContactInfo
					.builder();

			NomineePercent nomineePercentage = SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineePercent
					.builder().value("100").build();

			PostAddrBuilder postalAddressBuilder = NomineeContactInfo.PostAddr.builder();

			GeoMasterSBM geoMaster = commonSerivce.getGeoMasterFromPinCode(addressDto.getPincode());

			PostAddr postalAddress = postalAddressBuilder.addr1(addressDto.getAddress1())
					.addr2(addressDto.getAddress2()).addr3(addressDto.getAddress3()).city(geoMaster.getDistrict())
					.stateProv(geoMaster.getState()).country(geoMaster.getCountry()).postalCode(addressDto.getPincode())
					.build();

			NomineeContactInfo nomineeContactInfo = nomineeContactInfoBuilder.postAddr(postalAddress).build();

			String isMinor = "N";

			if (nomineeDetails.getIsMinor()) {
				isMinor = "Y";
			}

			List<NomineeInfoRec> nomineeInfoRecList = new ArrayList<>();

			String dobFormatted = DateUtilityFunction.dateInFormat(nomineeDetails.getNomineeDOB(),
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			String relPartyId = commonSerivce.getRelPartyTypeByRelationShip(nomineeDetails.getNomineeRelationship());

			NomineeInfoRec nomineeInfoRec = nomineeInfoBuilder.nomineeContactInfo(nomineeContactInfo).regNum("0001")
					.nomineeName(nomineeDetails.getNomineeName()).nomineePercent(nomineePercentage)
					.nomineeBirthDt(dobFormatted).relType(relPartyId).nomineeMinorFlg(isMinor).build();

			nomineeInfoRecList.add(nomineeInfoRec);

			SBAcctModRq sbAccountModRq = SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.builder()
					.nomineeInfoRec(nomineeInfoRecList).sbAcctId(sbAccountId).build();

			SBAcctModRequest sbAcctModRequest = SBAcctModRequestDto.Body.SBAcctModRequest.builder()
					.sbAcctModRq(sbAccountModRq).build();

			com.neo.aggregator.bank.sbm.core.request.SBAcctModRequestDto.Body requestBody = SBAcctModRequestDto.Body
					.builder().sbAcctModRequest(sbAcctModRequest).build();

			requestDto = SBAcctModRequestDto.builder().header(header).body(requestBody).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getSbAcctModRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.SAVINGS_ACCOUNT_MODIFICATION_XSD);
			m.marshal(requestDto, sw);

			savingsAccountModRequest = sw.toString();

			logger.info("Actual payload ::" + savingsAccountModRequest);

			String encryptedPayload = encryptionUtil.encrypt(savingsAccountModRequest);

			RequestInDto retailCustAddRequestDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1018").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(retailCustAddRequestDto, stringWriter);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1018";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.SAVINGS_ACCOUNT_MODIFICATION);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.SAVINGS_ACCOUNT_MODIFICATION);

			savingsAccountModResponse = decryptESBResponse(responseBody, ProductConstants.SAVINGS_ACCOUNT_MODIFICATION, sessionId);

			logger.info("Decrypted Response ::" + savingsAccountModResponse);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getSbAcctModResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(savingsAccountModResponse);
			SBModResponseDto ress = (SBModResponseDto) um.unmarshal(decryptedResponsereader);

			if (ress.getHeader().getResponseHeader().getHostTransaction().getStatus().equals("SUCCESS")) {
				logger.info("SUCCESS RESPONSE");
				String accountId = ress.getBody().getSBAcctModResponse().getSBAcctModRs().getSBAcctId().getAcctId();
				response.setDescription("Account Modification Successful");
				response.setStatus(ress.getHeader().getResponseHeader().getHostTransaction().getStatus());
				response.setAccountId(accountId);
			} else {
				logger.info("FAILURE RESPONSE");

				if (ress.getBody().getError() != null) {

					if (ress.getBody().getError().getFiSystemException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFiSystemException().getErrorDetail().getErrorDesc(), null,
								Locale.US, savingsAccountModRequest, savingsAccountModResponse,
								ProductConstants.SAVINGS_ACCOUNT_MODIFICATION);
					} else if (ress.getBody().getError().getFIBusinessException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFIBusinessException().getErrorDetail().getErrorDesc(),
								null, Locale.US, savingsAccountModRequest, savingsAccountModResponse,
								ProductConstants.SAVINGS_ACCOUNT_MODIFICATION);
					}

				}
			}

			return response;

		} catch (NeoException e) {
			throw e;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					savingsAccountModRequest, savingsAccountModResponse, ProductConstants.SAVINGS_ACCOUNT_MODIFICATION);
		}

	}

	@Override
	public String testAPI(TestAPIRequestDto req) throws NeoException {

		String bankId = BANKID;
		StringWriter stringWriter = new StringWriter();
		String decryptedResponse = null;
		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			String inputData = req.getPlainRequest();

			logger.info("testAPI Request  ::" + inputData);

			String encryptedPayload = encryptionUtil.encrypt(inputData);

			logger.info("Encrypted data ::" + encryptedPayload);

			RequestInDto internalTransferDto = RequestInDto.builder().authType(req.getAuthType())
					.channelId(req.getChannelId()).moduleID(req.getModuleId()).partnerReqID(requestId)
					.partnerUserName(req.getPartnerUserName()).timestamp(timeStamp).requestType(req.getRequestType())
					.request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(internalTransferDto, stringWriter);

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", req.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String url = req.getUrl();

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, null, null, null);

			decryptedResponse = decryptESBResponse(responseBody, null, sessionId);

			logger.info("Decrypted Response ::" + decryptedResponse);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return decryptedResponse;
	}

	@Override
	public FundTransferResponseDto directCredit(FundTransferRequestDto request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		String bankId = BANKID;
		CreditAddRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		FundTransferResponseDto response = new FundTransferResponseDto();
		String creditAddRequest = null;
		String creditAddResponse = null;

		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			Header header = commonSerivce.buildHeader(SbmPropertyConstants.CREDIT_ADD);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1040";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.CREDIT_ADD);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String txnDate = DateUtilityFunction.dateInFormat(new Date(),
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			com.neo.aggregator.bank.sbm.core.request.CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq.CreditPartTrnRec.TrnAmt txnAmount = com.neo.aggregator.bank.sbm.core.request.CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq.CreditPartTrnRec.TrnAmt
					.builder().amountValue(request.getAmount()).currencyCode(request.getTransactionCurrency()).build();

			com.neo.aggregator.bank.sbm.core.request.CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq.CreditPartTrnRec.AcctId toAccountNo = com.neo.aggregator.bank.sbm.core.request.CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq.CreditPartTrnRec.AcctId
					.builder().acctId(request.getToAccountNo()).build();

			CreditPartTrnRec creditPartRec = CreditPartTrnRec.builder().trnAmt(txnAmount).valueDt(txnDate)
					.acctId(toAccountNo).build();

			CreditAddRq creditAddRq = CreditAddRq.builder().creditPartTrnRec(creditPartRec)
					.totAmtCurCode(request.getTransactionCurrency()).trnSubType("NR").build();

			CreditAddRequest creditAddRequestInnerDto = CreditAddRequest.builder().creditAddRq(creditAddRq).build();

			requestDto = CreditAddRequestDto.builder().header(header)
					.body(com.neo.aggregator.bank.sbm.core.request.CreditAddRequestDto.Body.builder()
							.creditAddRequest(creditAddRequestInnerDto).build())
					.build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getCreditAddRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.CREDIT_ADD_XSD);
			m.marshal(requestDto, sw);

			creditAddRequest = sw.toString();

			logger.info("Actual payload ::" + creditAddRequest);

			String encryptedPayload = encryptionUtil.encrypt(creditAddRequest);

			RequestInDto creditAddRequestDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1040").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(creditAddRequestDto, stringWriter);

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.CREDIT_ADD);

			creditAddResponse = decryptESBResponse(responseBody, ProductConstants.CREDIT_ADD, sessionId);

			logger.info("Decrypted Response ::" + creditAddResponse);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getCreditAddResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(creditAddResponse);
			CreditAddResponseDto creditAddResponseDto = (CreditAddResponseDto) um.unmarshal(decryptedResponsereader);

			if (creditAddResponseDto.getHeader().getResponseHeader().getHostTransaction().getStatus()
					.equals("SUCCESS")) {

				logger.info("SUCCESS RESPONSE");
				String tranIdentification = creditAddResponseDto.getBody().getCreditAddResponse().getCreditAddRs()
						.getTrnId();
				response.setBankReferenceNo(tranIdentification);
				response.setStatus("SUCCESS");
				response.setExternalTransactionId(request.getExternalTransactionId());
			} else {
				logger.info("FAILURE RESPONSE");

				if (creditAddResponseDto.getBody().getError() != null) {

					if (creditAddResponseDto.getBody().getError().getFiSystemException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								creditAddResponseDto.getBody().getError().getFiSystemException().getErrorDetail()
										.getErrorDesc(),
								null, Locale.US, creditAddRequest, creditAddResponse, ProductConstants.CREDIT_ADD);
					} else if (creditAddResponseDto.getBody().getError().getFIBusinessException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								creditAddResponseDto.getBody().getError().getFIBusinessException().getErrorDetail()
										.getErrorDesc(),
								null, Locale.US, creditAddRequest, creditAddResponse, ProductConstants.CREDIT_ADD);
					}
				}

			}

			return response;

		} catch (NeoException e) {

			logger.info("Exception Occured ::" + e.getDetailMessage());
			throw e;

		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
					NeoExceptionConstant.INTERNAL_SERVER_ERROR, null, Locale.US, creditAddRequest, creditAddResponse,
					ProductConstants.CREDIT_ADD);
		}
	}

	@Override
	public FundTransferResponseDto directDebit(FundTransferRequestDto request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		String bankId = BANKID;
		DebitAddRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		FundTransferResponseDto response = new FundTransferResponseDto();
		String debitAddrequest = null;
		String debitAddResponse = null;

		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			Header header = commonSerivce.buildHeader(SbmPropertyConstants.DEBIT_ADD);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1041";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.DEBIT_ADD);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String txnDate = DateUtilityFunction.dateInFormat(new Date(),
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			com.neo.aggregator.bank.sbm.core.request.DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq.DebitPartTrnRec.TrnAmt txnAmount = com.neo.aggregator.bank.sbm.core.request.DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq.DebitPartTrnRec.TrnAmt
					.builder().amountValue(request.getAmount()).currencyCode(request.getTransactionCurrency()).build();

			com.neo.aggregator.bank.sbm.core.request.DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq.DebitPartTrnRec.AcctId fromAccount = com.neo.aggregator.bank.sbm.core.request.DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq.DebitPartTrnRec.AcctId
					.builder().acctId(request.getFromAccountNo()).build();

			DebitPartTrnRec debitPartReq = DebitPartTrnRec.builder().trnAmt(txnAmount).valueDt(txnDate)
					.acctId(fromAccount).build();

			DebitAddRq debitRq = DebitAddRq.builder().debitPartTrnRec(debitPartReq)
					.totAmtCurCode(request.getTransactionCurrency()).trnSubType("NP").build();

			DebitAddRequest debitAddRequest = DebitAddRequest.builder().debitAddRq(debitRq).build();

			requestDto = DebitAddRequestDto.builder()
					.body(com.neo.aggregator.bank.sbm.core.request.DebitAddRequestDto.Body.builder()
							.debitAddRequest(debitAddRequest).build())
					.header(header).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getDebitAddRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.DEBIT_ADD_XSD);
			m.marshal(requestDto, sw);

			debitAddrequest = sw.toString();

			logger.info("Actual payload ::" + debitAddrequest);

			String encryptedPayload = encryptionUtil.encrypt(debitAddrequest);

			RequestInDto debitAddRequestDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1041").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(debitAddRequestDto, stringWriter);

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.DEBIT_ADD);

			debitAddResponse = decryptESBResponse(responseBody, ProductConstants.DEBIT_ADD, sessionId);

			logger.info("Decrypted Response ::" + debitAddResponse);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getDebitAddResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(debitAddResponse);
			DebitAddResponseDto debitAddResponseDto = (DebitAddResponseDto) um.unmarshal(decryptedResponsereader);

			if (debitAddResponseDto.getHeader().getResponseHeader().getHostTransaction().getStatus()
					.equals("SUCCESS")) {

				logger.info("SUCCESS RESPONSE");
				String tranIdentification = debitAddResponseDto.getBody().getDebitAddResponse().getDebitAddRs()
						.getTrnId();
				response.setBankReferenceNo(tranIdentification);
				response.setStatus("SUCCESS");
				response.setExternalTransactionId(request.getExternalTransactionId());
			} else {
				logger.info("FAILURE RESPONSE");

				if (debitAddResponseDto.getBody().getError() != null) {

					if (debitAddResponseDto.getBody().getError().getFiSystemException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								debitAddResponseDto.getBody().getError().getFiSystemException().getErrorDetail()
										.getErrorDesc(),
								null, Locale.US, debitAddrequest, debitAddResponse, ProductConstants.DEBIT_ADD);
					} else if (debitAddResponseDto.getBody().getError().getFIBusinessException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								debitAddResponseDto.getBody().getError().getFIBusinessException().getErrorDetail()
										.getErrorDesc(),
								null, Locale.US, debitAddrequest, debitAddResponse, ProductConstants.DEBIT_ADD);
					}
				}

			}

			return response;

		} catch (NeoException e) {

			logger.info("Exception Occured ::" + e.getDetailMessage());
			throw e;

		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					debitAddrequest, debitAddResponse, ProductConstants.DEBIT_ADD);
		}
	}

	@Override
	public AccountClosureResponseDto tdAccountClosure(AccountClosureRequestDto request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		String bankId = BANKID;
		TdAcctCloseRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		String rpaymentAccount = null;
		AccountClosureResponseDto response = new AccountClosureResponseDto();
		TdAccountTrailCloseRequestDto tdAccountTrailCloseRequest = new TdAccountTrailCloseRequestDto();

		String tdAccountCloseRequest = null;
		String tdAccountCloseResponse = null;

		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			ProductConfig productConfigModel = productConfigDao
					.findByPartnerAndProduct(TenantContextHolder.getNeoTenant(), ProductConstants.TD_CREATION);

			if (productConfigModel != null && productConfigModel.getDefaultRePaymentAccountType() != null) {
				rpaymentAccount = productConfigModel.getDefaultRePaymentAccountType();
			} else {
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.REPAY_ACCOUNT_INVALID, null, null, Locale.US,
						null, null, ProductConstants.TD_FIXED_ACCOUNT_CLOSE);
			}

			String accountId = request.getAccountInfo().get(0).getAccountNo();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			Header header = commonSerivce.buildHeader(SbmPropertyConstants.TD_FIXED_ACCOUNT_CLOSE);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1039";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.TD_FIXED_ACCOUNT_CLOSE);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			com.neo.aggregator.bank.sbm.core.request.TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.DepAcctId.AcctType accType = com.neo.aggregator.bank.sbm.core.request.TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.DepAcctId.AcctType
					.builder().schmType("TDA").build();

			tdAccountTrailCloseRequest.setAccountId(accountId);

			tdAccountTrailCloseRequest.setRePayAccount(rpaymentAccount);

			TDAcctCloseTrailResponseDto tDAcctCloseTrailResponse = this
					.tdTrailAccountClosure(tdAccountTrailCloseRequest);

			if (tDAcctCloseTrailResponse == null) {
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.TD_TRAIL_CLOSURE_FAILED, null, null,
						Locale.US, null, null, ProductConstants.TD_FIXED_ACCOUNT_CLOSE);
			}

			DepAcctId depAcctId = DepAcctId.builder().acctId(accountId).acctType(accType).build();

			CloseAmt closeAmt = CloseAmt.builder().amountValue(tDAcctCloseTrailResponse.getWithdrwlAmt())
					.currencyCode("INR").build();

			com.neo.aggregator.bank.sbm.core.request.TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.RepayAcctId repayAcctId = com.neo.aggregator.bank.sbm.core.request.TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.RepayAcctId
					.builder().acctId(rpaymentAccount).build();

			DepAcctCloseCustomData depAcctCloseCustomData = DepAcctCloseCustomData.builder()
					.withdrwlamt(tDAcctCloseTrailResponse.getWithdrwlAmt()).wcrncy("INR").build();

			DepAcctCloseRq depAcctCloseRq = DepAcctCloseRq.builder().depAcctId(depAcctId).closeModeFlg("Y")
					.closeAmt(closeAmt).repayAcctId(repayAcctId).build();

			DepAcctCloseRequest depAcctCloseRequest = DepAcctCloseRequest.builder().depAcctCloseRq(depAcctCloseRq)
					.build();

			com.neo.aggregator.bank.sbm.core.request.TdAcctCloseRequestDto.Body body = com.neo.aggregator.bank.sbm.core.request.TdAcctCloseRequestDto.Body
					.builder().depAcctCloseRequest(depAcctCloseRequest).depAcctCloseCustomData(depAcctCloseCustomData)
					.build();

			requestDto = TdAcctCloseRequestDto.builder().header(header).body(body).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getTdAcctAddRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.TD_FIXED_ACCOUNT_CLOSE_XSD);
			m.marshal(requestDto, sw);

			tdAccountCloseRequest = sw.toString();

			logger.info("Actual payload ::" + tdAccountCloseRequest);

			String encryptedPayload = encryptionUtil.encrypt(tdAccountCloseRequest);

			RequestInDto debitAddRequestDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1039").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(debitAddRequestDto, stringWriter);

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.TD_FIXED_ACCOUNT_CLOSE);

			tdAccountCloseResponse = decryptESBResponse(responseBody, ProductConstants.TD_FIXED_ACCOUNT_CLOSE, sessionId);

			logger.info("Decrypted Response ::" + tdAccountCloseResponse);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getTdAcctCloseResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(tdAccountCloseResponse);
			TdAcctCloseResponseDto tdAcctCloseResponseDto = (TdAcctCloseResponseDto) um
					.unmarshal(decryptedResponsereader);

			SimpleDateFormat formattxnDate = new SimpleDateFormat(SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			if (tdAcctCloseResponseDto.getHeader().getResponseHeader().getHostTransaction().getStatus()
					.equals("SUCCESS")) {

				logger.info("SUCCESS RESPONSE");

				response.setStatus("SUCCESS");
				DepAcctCloseRs depositCloseRs = tdAcctCloseResponseDto.getBody().getDepAcctCloseResponse()
						.getDepAcctCloseRs();

				Date closureDate = formattxnDate.parse(depositCloseRs.getCloseValueDt());
				String closureDateString = DateUtilityFunction.dateInFormat(closureDate, "yyyy-MM-dd");

				response.setAccountId(depositCloseRs.getDepAcctId().getAcctId());
				response.setAccountCurrency(depositCloseRs.getDepAcctId().getAcctCurr());
				response.setCustomerId(depositCloseRs.getCustId().getCustId());
				response.setCustomerName(depositCloseRs.getCustId().getPersonName().getName());
				response.setRepaymentAccountId(depositCloseRs.getRepayAcctId().getAcctId());
				response.setClosureDate(closureDateString);
				response.setWithdrawAmount(depositCloseRs.getWithdrawnAmt().getAmountValue());
				response.setWithdrawCurrency(depositCloseRs.getWithdrawnAmt().getCurrencyCode());
				response.setBalanceAmount(depositCloseRs.getBalAmt().getAmountValue());
				response.setBalanceCurrency(depositCloseRs.getBalAmt().getCurrencyCode());
				response.setLiftedAmount(depositCloseRs.getLiftedLienAmt().getAmountValue());
				response.setClosureTransactionId(depositCloseRs.getCloseTrnId());
				response.setProfitPercentage(depositCloseRs.getProfitPercent().getValue());
				response.setPayoutAmount(depositCloseRs.getPayoutAmt().getAmountValue());
				response.setProfitAmount(depositCloseRs.getProfitAmt().getAmountValue());
				response.setCurrentDepositAmount(depositCloseRs.getCurrDepAmt().getAmountValue());
				response.setCurrentDepositCurrency(depositCloseRs.getCurrDepAmt().getCurrencyCode());

				List<TrnDetailsRec> txnDetails = depositCloseRs.getTrnDetailsRec();

				List<com.neo.aggregator.dto.AccountClosureResponseDto.Transaction> txnList = new ArrayList<>();

				for (TrnDetailsRec txDetails : txnDetails) {

					com.neo.aggregator.dto.AccountClosureResponseDto.Transaction txn = new com.neo.aggregator.dto.AccountClosureResponseDto.Transaction();
					txn.setAmount(txDetails.getTrnAmt().getAmountValue());
					txn.setDescription(txDetails.getTrnParticulars());
					txn.setTxnId(txDetails.getTrnId());

					if (txDetails.getCreditDebitFlg().equals("C"))
						txn.setType("CREDIT");
					else if (txDetails.getCreditDebitFlg().equals("D"))
						txn.setType("DEBIT");

					txn.setCurrency(txDetails.getCurCode());

					txnList.add(txn);
				}

				response.setTxnList(txnList);

			} else {
				logger.info("FAILURE RESPONSE");

				if (tdAcctCloseResponseDto.getBody().getError() != null) {

					if (tdAcctCloseResponseDto.getBody().getError().getFiSystemException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								tdAcctCloseResponseDto.getBody().getError().getFiSystemException().getErrorDetail()
										.getErrorDesc(),
								null, Locale.US, tdAccountCloseRequest, tdAccountCloseResponse,
								ProductConstants.TD_FIXED_ACCOUNT_CLOSE);
					} else if (tdAcctCloseResponseDto.getBody().getError().getFIBusinessException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								tdAcctCloseResponseDto.getBody().getError().getFIBusinessException().getErrorDetail()
										.getErrorDesc(),
								null, Locale.US, tdAccountCloseRequest, tdAccountCloseResponse,
								ProductConstants.TD_FIXED_ACCOUNT_CLOSE);
					}
				}

			}

			return response;

		} catch (NeoException e) {

			logger.info("Exception Occured ::" + e.getDetailMessage());
			throw e;

		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					tdAccountCloseRequest, tdAccountCloseResponse, ProductConstants.TD_FIXED_ACCOUNT_CLOSE);
		}

	}

	@Override
	public TDAcctCloseTrailResponseDto tdTrailAccountClosure(TdAccountTrailCloseRequestDto request)
			throws NeoException {

		StringWriter stringWriter = new StringWriter();
		String bankId = BANKID;
		TdAcctTrailCloseRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		TDAcctCloseTrailResponseDto response = new TDAcctCloseTrailResponseDto();
		String tdAccountTrailCloseRequest = null;
		String tdAccountTrailCloseResponse = null;

		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			String accountId = request.getAccountId();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			String currentDate = DateUtilityFunction.dateInFormat(date, "dd-MM-yyyy");

			Header header = commonSerivce.buildHeader(SbmPropertyConstants.TD_FIXED_ACCOUNT_TRAIL_CLOSE);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1053";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.TD_FIXED_ACCOUNT_TRAIL_CLOSE);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			com.neo.aggregator.bank.sbm.core.request.TdAcctTrailCloseRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptCustomData executeFinacleScriptCustomData = com.neo.aggregator.bank.sbm.core.request.TdAcctTrailCloseRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptCustomData
					.builder().clrValueDate(currentDate).clsrReason("003").rePayAccount(request.getRePayAccount())
					.tdAccount(accountId).build();

			com.neo.aggregator.bank.sbm.core.request.TdAcctTrailCloseRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptInputVO executeFinacleScriptInputVO = com.neo.aggregator.bank.sbm.core.request.TdAcctTrailCloseRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptInputVO
					.builder().requestId("FI_TDAcctTrialClose.scr").build();

			com.neo.aggregator.bank.sbm.core.request.TdAcctTrailCloseRequestDto.Body.ExecuteFinacleScriptRequest executeFinacleScriptRequest = com.neo.aggregator.bank.sbm.core.request.TdAcctTrailCloseRequestDto.Body.ExecuteFinacleScriptRequest
					.builder().executeFinacleScriptCustomData(executeFinacleScriptCustomData)
					.executeFinacleScriptInputVO(executeFinacleScriptInputVO).build();

			com.neo.aggregator.bank.sbm.core.request.TdAcctTrailCloseRequestDto.Body body = com.neo.aggregator.bank.sbm.core.request.TdAcctTrailCloseRequestDto.Body
					.builder().executeFinacleScriptRequest(executeFinacleScriptRequest).build();

			requestDto = TdAcctTrailCloseRequestDto.builder().header(header).body(body).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getTdTrailCloseRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.TD_FIXED_ACCOUNT_TRAIL_CLOSE_XSD);
			m.marshal(requestDto, sw);

			tdAccountTrailCloseRequest = sw.toString();

			logger.info("Actual payload ::" + tdAccountTrailCloseRequest);

			String encryptedPayload = encryptionUtil.encrypt(tdAccountTrailCloseRequest);

			RequestInDto trailCloseRequestDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1053").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(trailCloseRequestDto, stringWriter);

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.TD_FIXED_ACCOUNT_TRAIL_CLOSE);

			tdAccountTrailCloseResponse = decryptESBResponse(responseBody,
					ProductConstants.TD_FIXED_ACCOUNT_TRAIL_CLOSE, sessionId);

			logger.info("Decrypted Response ::" + tdAccountTrailCloseResponse);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getTdTrailCloseResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(tdAccountTrailCloseResponse);
			TDAccountCloseTrailResponseDto tdAcctCloseCloseResponseDto = (TDAccountCloseTrailResponseDto) um
					.unmarshal(decryptedResponsereader);

			if (tdAcctCloseCloseResponseDto.getHeader().getResponseHeader().getHostTransaction().getStatus()
					.equals("SUCCESS")) {

				logger.info("SUCCESS RESPONSE");

				response.setStatus("SUCCESS");
				TDAccountCloseTrailResponseDto.Body.ExecuteFinacleScriptResponse.ExecuteFinacleScriptCustomData executeFinacleScriptCustomDataResp = tdAcctCloseCloseResponseDto
						.getBody().getExecuteFinacleScriptResponse().getExecuteFinacleScriptCustomData();

				response.setActIntPcnt(executeFinacleScriptCustomDataResp.getActIntPcnt());
				response.setActualIntAmt(executeFinacleScriptCustomDataResp.getActualIntAmt());
				response.setContIntPcnt(executeFinacleScriptCustomDataResp.getContIntPcnt());
				response.setDepositAmt(executeFinacleScriptCustomDataResp.getDepositAmt());
				response.setEffIntPcnt(executeFinacleScriptCustomDataResp.getEffIntPcnt());
				response.setIntPayable(executeFinacleScriptCustomDataResp.getIntPayable());
				response.setIntRecoverable(executeFinacleScriptCustomDataResp.getIntRecoverable());
				response.setIntTaken(executeFinacleScriptCustomDataResp.getIntTaken());
				response.setLienAmt(executeFinacleScriptCustomDataResp.getLienAmt());
				response.setMaturityAmt(executeFinacleScriptCustomDataResp.getMaturityAmt());
				response.setMaturityDate(executeFinacleScriptCustomDataResp.getMaturityDate());
				response.setPenalIntAmt(executeFinacleScriptCustomDataResp.getPenalIntAmt());
				response.setPenalIntPcnt(executeFinacleScriptCustomDataResp.getPenalIntPcnt());
				response.setRepayAccount(executeFinacleScriptCustomDataResp.getRepayAccount());
				response.setRePayAmount(executeFinacleScriptCustomDataResp.getRePayAmount());
				response.setTaxPayable(executeFinacleScriptCustomDataResp.getTaxPayable());
				response.setTaxTillDt(executeFinacleScriptCustomDataResp.getTaxTillDt());
				response.setTdAccount(executeFinacleScriptCustomDataResp.getTDAccount());
				response.setWithdrwlAmt(executeFinacleScriptCustomDataResp.getWithdrwlAmt());

			} else {
				logger.info("FAILURE RESPONSE");

				if (tdAcctCloseCloseResponseDto.getBody().getError() != null) {

					if (tdAcctCloseCloseResponseDto.getBody().getError().getFiSystemException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								tdAcctCloseCloseResponseDto.getBody().getError().getFiSystemException().getErrorDetail()
										.getErrorDesc(),
								null, Locale.US, tdAccountTrailCloseRequest, tdAccountTrailCloseResponse,
								ProductConstants.TD_FIXED_ACCOUNT_TRAIL_CLOSE);
					} else if (tdAcctCloseCloseResponseDto.getBody().getError().getFIBusinessException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								tdAcctCloseCloseResponseDto.getBody().getError().getFIBusinessException()
										.getErrorDetail().getErrorDesc(),
								null, Locale.US, tdAccountTrailCloseRequest, tdAccountTrailCloseResponse,
								ProductConstants.TD_FIXED_ACCOUNT_TRAIL_CLOSE);
					}
				}

			}

			return response;

		} catch (NeoException e) {

			logger.info("Exception Occured ::" + e.getDetailMessage());
			throw e;

		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					tdAccountTrailCloseRequest, tdAccountTrailCloseResponse,
					ProductConstants.TD_FIXED_ACCOUNT_TRAIL_CLOSE);
		}

	}

	@Override
	public AccountInquiryResponseDto tdAccountInquiry(AccountInquiryRequestDto request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		TdAcctInquiryRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		AccountInquiryResponseDto response = new AccountInquiryResponseDto();
		String tdAccountInquiryRequest = null;
		String tdAccountInquiryResponse = null;

		try {

			MasterConfiguration configModel = masterConfig.findByPartnerId(TenantContextHolder.getNeoTenant());

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			Header header = commonSerivce.buildHeader(SbmPropertyConstants.TD_ACCOUNT_INQUIRY);

			com.neo.aggregator.bank.sbm.core.request.TdAcctInquiryRequestDto.Body.TDAcctInqRequest.TDAcctInqRq.TDAcctId accountId = com.neo.aggregator.bank.sbm.core.request.TdAcctInquiryRequestDto.Body.TDAcctInqRequest.TDAcctInqRq.TDAcctId
					.builder().acctId(request.getAccountId()).build();

			TDAcctInqRq tdAcctInqReq = TDAcctInqRq.builder().tdAcctId(accountId).build();

			TDAcctInqRequest tdAcctInqRequest = TdAcctInquiryRequestDto.Body.TDAcctInqRequest.builder()
					.tdAcctInqRq(tdAcctInqReq).build();

			com.neo.aggregator.bank.sbm.core.request.TdAcctInquiryRequestDto.Body requestBody = TdAcctInquiryRequestDto.Body
					.builder().tdAcctInqRequest(tdAcctInqRequest).build();

			requestDto = TdAcctInquiryRequestDto.builder().header(header).body(requestBody).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getTdAcctInquiryRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.TD_ACCOUNT_INQUIRY_XSD);
			m.marshal(requestDto, sw);

			tdAccountInquiryRequest = sw.toString();

			logger.info("Actual payload ::" + tdAccountInquiryRequest);

			String encryptedPayload = encryptionUtil.encrypt(tdAccountInquiryRequest);

			RequestInDto retailCustAddRequestDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1026").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(retailCustAddRequestDto, stringWriter);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1026";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.TD_ACCOUNT_INQUIRY);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.TD_ACCOUNT_INQUIRY);

			tdAccountInquiryResponse = decryptESBResponse(responseBody, ProductConstants.TD_ACCOUNT_INQUIRY, sessionId);

			logger.info("Decrypted Response ::" + tdAccountInquiryResponse);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getTdAcctInquiryResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(tdAccountInquiryResponse);
			TdAcctInquiryResponseDto tdAccointInqResponse = (TdAcctInquiryResponseDto) um
					.unmarshal(decryptedResponsereader);

			if (tdAccointInqResponse.getHeader().getResponseHeader().getHostTransaction().getStatus()
					.equals("SUCCESS")) {
				logger.info("SUCCESS RESPONSE");

				response.setStatus("SUCCESS");

				response.setAccountNo(tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs()
						.getTDAcctId().getAcctId());
				response.setSchemeCode(tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs()
						.getTDAcctId().getAcctType().getSchmCode());
				response.setSchemeType(tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs()
						.getTDAcctId().getAcctType().getSchmType());
				response.setAccountCurrency(tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs()
						.getTDAcctId().getAcctCurr());
				response.setAccountOpenDate(
						tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs().getAcctOpnDt());
				response.setModeOfOpertion(
						tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs().getModeOfOper());
				response.setBankAccountStatus(
						tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs().getBankAcctStatusCode());
				response.setCustomerId(
						tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs().getCustId().getCustId());
				response.setCustomerName(tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs()
						.getCustId().getPersonName().getName());
				response.setNetInterestRate(tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs()
						.getNetIntRate().getValue());
				response.setTotalInterestAmount(tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs()
						.getTotalIntAmt().getAmountValue());
				response.setInitialDepositAmount(tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs()
						.getInitialDeposit().getAmountValue());
				response.setMaturityAmount(tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs()
						.getMaturityAmt().getAmountValue());
				response.setDepositTerm(tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs()
						.getDepositTerm().getMonths());
				response.setMaturityDate(
						tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs().getMaturityDt());
				response.setRepaymentAccountId(tdAccointInqResponse.getBody().getTDAcctInqResponse().getTDAcctInqRs()
						.getRepayAcctId().getAcctId());
				
				logger.info("TD Inquiry Success Response : "+ response.toString());				
			} else {
				logger.info("FAILURE RESPONSE");

				if (tdAccointInqResponse.getBody().getError() != null) {

					if (tdAccointInqResponse.getBody().getError().getFiSystemException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								tdAccointInqResponse.getBody().getError().getFiSystemException().getErrorDetail()
										.getErrorDesc(),
								null, Locale.US, tdAccountInquiryRequest, tdAccountInquiryResponse,
								ProductConstants.TD_ACCOUNT_INQUIRY);
					} else if (tdAccointInqResponse.getBody().getError().getFIBusinessException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								tdAccointInqResponse.getBody().getError().getFIBusinessException().getErrorDetail()
										.getErrorDesc(),
								null, Locale.US, tdAccountInquiryRequest, tdAccountInquiryResponse,
								ProductConstants.TD_ACCOUNT_INQUIRY);
					}

				}
			}

			return response;

		} catch (NeoException e) {
			logger.info("Exception Occured ::" + e.getDetailMessage());
			throw e;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					tdAccountInquiryRequest, tdAccountInquiryResponse, ProductConstants.TD_ACCOUNT_INQUIRY);
		}
	}

	public String checkLength(String benificiaryName, String remmitername, String eTrxnId, String desc) {
		String constructedTrxnParticulars = null;
		String name = null;
		try {
			if (benificiaryName != null)
				name = benificiaryName;
			else
				name = remmitername;

			if (eTrxnId.length() > 50) {
				logger.info(
						"Ext Txn Id is Greater than 50 Characters so will be missed out for the following transaction : "
								+ eTrxnId);
				constructedTrxnParticulars = eTrxnId;
				return constructedTrxnParticulars;
			}

			if (name != null && !name.isEmpty() && desc != null && !desc.isEmpty()) {
				String overallCount = "IFT" + "/" + eTrxnId + "/" + name + "/" + desc;
				if (overallCount.length() <= 50) {
					constructedTrxnParticulars = overallCount;
				} else {
					String descTrim = null;

					String nameTrim = null;

					if (desc.length() > 10) {
						descTrim = desc.substring(0, 10);
					} else if (desc.length() > 5) {
						descTrim = desc.substring(0, 5);

					} else if (desc.length() > 1) {
						descTrim = desc.substring(0, 2);
					} else {
						descTrim = desc.substring(0, 1);
					}

					if (name.length() > 10) {
						nameTrim = name.substring(0, 10);
					} else if (name.length() > 5) {
						nameTrim = name.substring(0, 5);

					} else if (name.length() > 1) {
						nameTrim = name.substring(0, 2);
					} else {
						nameTrim = name.substring(0, 1);
					}

					String overalltrimCount = "IFT" + "/" + eTrxnId + "/" + nameTrim + "/" + descTrim;

					if (overalltrimCount.length() <= 50) {
						constructedTrxnParticulars = overalltrimCount;
					} else {
						constructedTrxnParticulars = "IFT" + "/" + eTrxnId;

						if (constructedTrxnParticulars.length() > 50) {
							constructedTrxnParticulars = "IFT" + "/" + eTrxnId;
						} else {
							logger.info(
									"Ext Txn Id is higher in length so benificiary / Desc will be missed out for the following transaction : "
											+ eTrxnId);
							constructedTrxnParticulars = eTrxnId;
						}
					}

				}
			} else if (name != null && !name.isEmpty() && eTrxnId != null && !eTrxnId.isEmpty()) {
				String overallCount = "IFT" + "/" + eTrxnId + "/" + name;
				if (overallCount.length() <= 50) {
					constructedTrxnParticulars = overallCount;
				} else {
					String benfiTrim = null;

					if (name.length() > 10) {
						benfiTrim = name.substring(0, 10);
					} else if (name.length() > 5) {
						benfiTrim = name.substring(0, 5);

					} else if (name.length() > 1) {
						benfiTrim = name.substring(0, 2);
					} else {
						benfiTrim = name.substring(0, 1);
					}

					overallCount = "IFT" + "/" + eTrxnId + "/" + benfiTrim;
					if (overallCount.length() <= 50) {
						constructedTrxnParticulars = overallCount;
					} else {
						overallCount = "IFT" + "/" + eTrxnId;
						if (overallCount.length() > 50) {
							constructedTrxnParticulars = overallCount;
						} else {
							logger.info(
									"Ext Txn Id is higher in length so benificiary name will be missed out for the following transaction : "
											+ eTrxnId);
							constructedTrxnParticulars = eTrxnId;
						}
					}
				}
			} else if (name == null && desc == null && eTrxnId != null && !eTrxnId.isEmpty()) {
				logger.info(
						"Benificiary name / Description not provided so benificiary name / Description will be missed out for the following transaction : "
								+ eTrxnId);
				constructedTrxnParticulars = eTrxnId;
			}

		} catch (Exception e) {
			logger.info("Exception in length validation", e);
		}

		return constructedTrxnParticulars;
	}

	@Override
	public KYCBioResponseDto bioEKycOneTimeToken(RegistrationRequestV2Dto request, String eKycRefnum)
			throws NeoException {
		return null;
	}

	@Override
	public KYCBioResponseDto eKycBioValidate(KYCBioRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public KYCBioResponseDto getDemographicData(KYCBioRequestDto request, String kycType) throws NeoException {
		return null;
	}

	@Override
	public LienMarkResponseDto addLienMark(LienMarkRequestDto request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		String bankId = BANKID;
		AcctLienAddRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		LienMarkResponseDto response = new LienMarkResponseDto();
		NeoLienMarkReqResModel lienMarkModel = new NeoLienMarkReqResModel();
		String lienMarkAddRequest = null;
		String lienMarkAddResponse = null;

		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			NeoBusinessCustomField tdLienChannelId = customFieldService.findByFieldNameAndTenant(
					NeoCustomFieldConstants.TD_LIEN_CHANNEL_ID, TenantContextHolder.getNeoTenant());

			String lienChannelId = tdLienChannelId != null ? tdLienChannelId.getFieldValue() : "MPBLN";

			Header header = commonSerivce.buildHeader(SbmPropertyConstants.ACCOUNT_LIEN_ADD, lienChannelId);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1044";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.ACCOUNT_LIEN_ADD);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String startDate = DateUtilityFunction.dateInFormat(request.getStartDate(),
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);
			String endDate = DateUtilityFunction.dateInFormat(request.getEndDate(),
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			com.neo.aggregator.bank.sbm.core.request.AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.AcctId acctId = com.neo.aggregator.bank.sbm.core.request.AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.AcctId
					.builder().acctId(request.getAccountId()).build();

			NewLienAmt newLienAmt = NewLienAmt.builder().amountValue(request.getAmount())
					.currencyCode(request.getAccountCurrency()).build();

			LienDt lienDt = LienDt.builder().startDt(startDate).endDt(endDate).build();

			LienDtls lienDtls = LienDtls.builder().lienDt(lienDt).newLienAmt(newLienAmt)
					.reasonCode(request.getReasonCode()).rmks(request.getRemarks()).build();

			AcctLienAddRq acctLienAddRq = AcctLienAddRq.builder().acctId(acctId).lienDtls(lienDtls)
					.moduleType(request.getModuleType()).build();

			AcctLienAddRequest acctLienAddRequest = AcctLienAddRequest.builder().acctLienAddRq(acctLienAddRq).build();

			com.neo.aggregator.bank.sbm.core.request.AcctLienAddRequestDto.Body body = com.neo.aggregator.bank.sbm.core.request.AcctLienAddRequestDto.Body
					.builder().acctLienAddRequest(acctLienAddRequest).build();

			requestDto = AcctLienAddRequestDto.builder().body(body).header(header).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getAcctLienAddRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.ACCOUNT_LIEN_ADD_XSD);
			m.marshal(requestDto, sw);

			lienMarkAddRequest = sw.toString();

			logger.info("Actual payload ::" + lienMarkAddRequest);

			String encryptedPayload = encryptionUtil.encrypt(lienMarkAddRequest);

			RequestInDto creditAddRequestDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1044").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(creditAddRequestDto, stringWriter);

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.ACCOUNT_LIEN_ADD);

			lienMarkAddResponse = decryptESBResponse(responseBody, ProductConstants.ACCOUNT_LIEN_ADD, sessionId);

			logger.info("Decrypted Response ::" + lienMarkAddResponse);

			lienMarkModel.setRequestId(requestId);
			lienMarkModel.setAccountId(request.getAccountId());
			lienMarkModel.setTenant(TenantContextHolder.getNeoTenant());
			lienMarkModel.setBankId(BANKID);
			lienMarkModel.setEntityId(request.getEntityId());
			lienMarkModel.setLienStart(request.getStartDate());
			lienMarkModel.setLienEnd(request.getEndDate());
			lienMarkModel.setLienReasonCode(request.getReasonCode());
			lienMarkModel.setLienModuleType(request.getModuleType());
			lienMarkModel.setProductId(ProductConstants.ACCOUNT_LIEN_ADD);
			lienMarkModel.setAmount(request.getAmount());
			lienMarkModel.setRetryCount(0);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getAcctLienAddResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(lienMarkAddResponse);
			AcctLienAddResponseDto acctLienAddResponse = (AcctLienAddResponseDto) um.unmarshal(decryptedResponsereader);

			if (acctLienAddResponse.getHeader().getResponseHeader().getHostTransaction().getStatus()
					.equals("SUCCESS")) {

				logger.info("SUCCESS RESPONSE");

				String lienId = acctLienAddResponse.getBody().getAcctLienAddResponse().getAcctLienAddRs().getLienIdRec()
						.getLienId();

				lienMarkModel.setLienId(lienId);
				lienMarkModel.setStatus("SUCCESS");
				lienMarkModel.setResponseDescription("Lien Mark Successful");

				response.setStatus("SUCCESS");
				response.setAccountId(acctLienAddResponse.getBody().getAcctLienAddResponse().getAcctLienAddRs()
						.getAcctId().getAcctId());
				response.setLienId(lienId);

			} else {
				logger.info("FAILURE RESPONSE");

				if (acctLienAddResponse.getBody().getError() != null) {

					if (acctLienAddResponse.getBody().getError().getFiSystemException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								acctLienAddResponse.getBody().getError().getFiSystemException().getErrorDetail()
										.getErrorDesc(),
								null, Locale.US, lienMarkAddRequest, lienMarkAddResponse,
								ProductConstants.ACCOUNT_LIEN_ADD);
					} else if (acctLienAddResponse.getBody().getError().getFIBusinessException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								acctLienAddResponse.getBody().getError().getFIBusinessException().getErrorDetail()
										.getErrorDesc(),
								null, Locale.US, lienMarkAddRequest, lienMarkAddResponse,
								ProductConstants.ACCOUNT_LIEN_ADD);
					}
				}

			}

			return response;

		} catch (NeoException e) {
			lienMarkModel.setStatus("FAILURE");
			lienMarkModel.setResponseDescription(e.getDetailMessage());
			throw e;

		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			lienMarkModel.setStatus("FAILURE");
			lienMarkModel.setResponseDescription("Exception occured ::" + e.getMessage());
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					lienMarkAddRequest, lienMarkAddResponse, ProductConstants.ACCOUNT_LIEN_ADD);
		} finally {
			auditService.saveTdLienMark(lienMarkModel);
		}
	}

	@Override
	public AccountInquiryResponseDto savingsAccountInquiry(AccountInquiryRequestDto request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		SBAcctInquiryRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		AccountInquiryResponseDto response = new AccountInquiryResponseDto();
		String savingsAccountInquiryResponse = null;
		String savingsAccountInquiry = null;

		try {

			MasterConfiguration configModel = masterConfig.findByPartnerId(TenantContextHolder.getNeoTenant());

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			Header header = commonSerivce.buildHeader(SbmPropertyConstants.SAVINGS_ACCOUNT_INQUIRY);

			com.neo.aggregator.bank.sbm.core.request.SBAcctInquiryRequestDto.Body.SBAcctInqRequest.SBAcctInqRq.SBAcctId sbAcctId = com.neo.aggregator.bank.sbm.core.request.SBAcctInquiryRequestDto.Body.SBAcctInqRequest.SBAcctInqRq.SBAcctId
					.builder().acctId(request.getAccountId()).build();

			SBAcctInqRq sbAcctInqRq = SBAcctInqRq.builder().sbAcctId(sbAcctId).build();

			SBAcctInqRequest inqRequest = SBAcctInqRequest.builder().sbAcctInqRq(sbAcctInqRq).build();

			com.neo.aggregator.bank.sbm.core.request.SBAcctInquiryRequestDto.Body body = SBAcctInquiryRequestDto.Body
					.builder().sbAcctInqRequest(inqRequest).build();

			requestDto = SBAcctInquiryRequestDto.builder().body(body).header(header).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getSBAcctInquiryRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.SAVINGS_ACCOUNT_INQUIRY_XSD);
			m.marshal(requestDto, sw);

			savingsAccountInquiry = sw.toString();

			logger.m2pdebug("Plain Request Send ::" + savingsAccountInquiry);

			String encryptedPayload = encryptionUtil.encrypt(savingsAccountInquiry);

			logger.info("Encrypted Actual Payload ::" + encryptedPayload);

			RequestInDto inquiryRequestDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1021").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(inquiryRequestDto, stringWriter);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1021";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.SAVINGS_ACCOUNT_INQUIRY);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.SAVINGS_ACCOUNT_INQUIRY);

			savingsAccountInquiryResponse = decryptESBResponse(responseBody, ProductConstants.SAVINGS_ACCOUNT_INQUIRY, sessionId);

			logger.m2pdebug("Decrypted Actual Response ::" + savingsAccountInquiryResponse);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getSBAcctInquiryResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(savingsAccountInquiryResponse);
			SBAcctInquiryResponseDto ress = (SBAcctInquiryResponseDto) um.unmarshal(decryptedResponsereader);

			if (ress.getHeader().getResponseHeader().getHostTransaction().getStatus().equals("SUCCESS")) {

				logger.info("SUCCESS RESPONSE");

				com.neo.aggregator.bank.sbm.core.response.SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctId sbAcctIdRes = ress
						.getBody().getSBAcctInqResponse().getSBAcctInqRs().getSBAcctId();

				String modeOfOperation = ress.getBody().getSBAcctInqResponse().getSBAcctInqRs() != null
						? ress.getBody().getSBAcctInqResponse().getSBAcctInqRs().getModeOfOper()
						: null;
				String schemeType = null;
				String schemeCode = null;
				String accountCurrency = null;

				if (sbAcctIdRes != null) {
					schemeType = sbAcctIdRes.getAcctType().getSchmType();
					schemeCode = sbAcctIdRes.getAcctType().getSchmCode();
					accountCurrency = sbAcctIdRes.getAcctCurr();
				}

				String customerId = ress.getBody().getSBAcctInqResponse().getSBAcctInqRs().getCustId().getCustId();
				String interestRate = ress.getBody().getSBAcctInqResponse().getSBAcctInqRs().getNetIntRate().getValue();

				com.neo.aggregator.bank.sbm.core.response.SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec nomineeInfoRec = ress
						.getBody().getSBAcctInqResponse().getSBAcctInqRs().getNomineeInfoRec();

				String nomineeName = nomineeInfoRec != null ? nomineeInfoRec.getNomineeName() : null;
				String nomineeRelationship = nomineeInfoRec != null ? nomineeInfoRec.getRelType() : null;

				String customerName = ress.getBody().getSBAcctInqResponse().getSBAcctInqRs().getRelPartyRec()
						.getCustId().getPersonName().getName();

				String relationShip = commonSerivce.getRelationShipByRelPartyType(nomineeRelationship);

				String accountNo = ress.getBody().getSBAcctInqResponse().getSBAcctInqRs().getSBAcctId().getAcctId();

				response.setAccountNo(accountNo);
				response.setAccountCurrency(accountCurrency);
				response.setSchemeCode(schemeCode);
				response.setSchemeType(schemeType);
				response.setCustomerId(customerId);
				response.setNetInterestRate(interestRate);
				response.setNomineeName(nomineeName);
				response.setNomineeRelationship(relationShip);
				response.setAccountCurrency(accountCurrency);
				response.setCustomerName(customerName);
				response.setModeOfOpertion(modeOfOperation);
				response.setStatus("SUCCESS");

			} else {
				logger.info("FAILURE RESPONSE");

				if (ress.getBody().getError() != null) {

					if (ress.getBody().getError().getFiSystemException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFiSystemException().getErrorDetail().getErrorDesc(), null,
								Locale.US, savingsAccountInquiry, savingsAccountInquiryResponse,
								ProductConstants.SAVINGS_ACCOUNT_INQUIRY);
					} else if (ress.getBody().getError().getFIBusinessException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFIBusinessException().getErrorDetail().getErrorDesc(),
								null, Locale.US, savingsAccountInquiry, savingsAccountInquiryResponse,
								ProductConstants.SAVINGS_ACCOUNT_INQUIRY);
					}

				}

			}

			return response;

		} catch (NeoException e) {
			logger.info("Exception Occured ::" + e.getDetailMessage());
			throw e;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					savingsAccountInquiry, savingsAccountInquiryResponse, ProductConstants.SAVINGS_ACCOUNT_INQUIRY);
		}
	}

	@Override
	public KYCBioResponseDto aadharXmlInvoke(RegistrationRequestV2Dto request) throws NeoException {
		return null;
	}

	@Override
	public KYCBioResponseDto validateAadhaarXml(AadhaarOtpRequest request) throws NeoException {
		return null;
	}

	private String decryptESBResponse(String responseBody, String productId,String sessionId) throws JAXBException, NeoException {

		logger.info("Decrypting ESB Response");

		JAXBContext responseOutJAXB = NeoJAXBContextFactory.getResponseOutContext();
		Unmarshaller responseOutUnmarshaller = responseOutJAXB.createUnmarshaller();

		StringReader stringReader = new StringReader(responseBody);
		ResponseOutDto responseOutDto = (ResponseOutDto) responseOutUnmarshaller.unmarshal(stringReader);

		if (responseOutDto.getMessage() != null && responseOutDto.getMessage().equalsIgnoreCase("FAILED")) {
			logger.info("ESB Exception :: " + responseOutDto.getResponse());

			if (responseOutDto.getStatusCode() != null && responseOutDto.getStatusCode().equals("ESB111")) {
				logger.info("Session Expired Exception Refreshing the session::");
				SessionLoginDto dto = new SessionLoginDto();
				dto.setRefreshSession(true);
				dto.setBankId("SBM");
				dto.setOldSessionId(sessionId);
				this.loginCreation(dto);
			}
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.BANK_EXCEPTION_OCCURED,
					responseOutDto.getResponse(), null, Locale.US, null, responseBody, productId);
		}

		String decryptedResponse = encryptionUtil.decrypt(responseOutDto.getResponse());

		return decryptedResponse;
	}

	@Override
	public FundTransferResponseDto fundTransferWithOtp(FundTransferRequestDto request) throws NeoException {
		if (request.getTransactionType().equals("IMPS")) {
			return impsTransferWithOtp(request);
		} else if (request.getTransactionType().equals("IFT")) {
			return iftTransferWithOtp(request);
		} else {
			return addOutboundPymtWithOtp(request);
		}
	}

	public FundTransferResponseDto impsTransferWithOtp(FundTransferRequestDto request) throws NeoException {

		StringWriter sw = new StringWriter();
		FundTransferResponseDto responseDto = new FundTransferResponseDto();
		String bankId = BANKID;
		String requestId = null;
		String impsWithOtpRequest = null;
		String impsWithOtpResponse = null;

		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);
			requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();
			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, "YYYY-MM-dd HH:mm:ss");

			IMPSRequestDto impsReqDto = new IMPSRequestDto();
			impsReqDto.setREMITTERACC(request.getFromAccountNo());
			impsReqDto.setREMITTERNAME(request.getRemitterName());

			if (request.getRemitterNumber() != null && request.getRemitterNumber().contains("+91"))
				request.setRemitterNumber(request.getRemitterNumber().replace("+91", ""));

			impsReqDto.setMobileNumber(request.getRemitterNumber());
			impsReqDto.setTXNAMT(request.getAmount());
			impsReqDto.setBENIFICIARYACC(request.getToAccountNo());
			impsReqDto.setIsMobileFT("false");
			impsReqDto.setIsAccountFT("false");
			impsReqDto.setRemark(request.getDescription());
			impsReqDto.setMSGID(requestId);
			impsReqDto.setTransType("OutWardFundTransfer");
			impsReqDto.setIsBLQ("false");
			impsReqDto.setIsMini("false");
			impsReqDto.setIsCC("false");
			impsReqDto.setIsPayment("false");
			impsReqDto.setIsMerchant("false");
			impsReqDto.setRDTDAmount("0");
			impsReqDto.setPenaltiAmount("0");
			impsReqDto.setIsMisc("false");
			impsReqDto.setIFSC(request.getBeneficiaryIfsc());
			impsReqDto.setIsBranch("true");

			JAXBContext jaxbContext = NeoJAXBContextFactory.getImpsRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.marshal(impsReqDto, sw);

			impsWithOtpRequest = sw.toString();

			logger.info("Actual payload ::" + impsWithOtpRequest);

			String encryptedPayload = encryptionUtil.encrypt(impsWithOtpRequest);

			RequestInDto req = RequestInDto.builder().authType("IMPS_OTP").channelId(channelId).moduleID("")
					.partnerReqID(requestId).partnerUserName(configModel.getUserName()).timestamp(timeStamp)
					.requestType("IMPS").request(encryptedPayload).build();

			sw = new StringWriter();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(req, sw);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-AHTTP_IMPS%23IMPS_OTP";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.FUNDTRANSFER_OTP);

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			String responseBody = postForEntityWithHeaders(sw.toString(), url, HttpMethod.POST, headers, Boolean.TRUE,
					sslAlias, sslEnabled, ProductConstants.FUNDTRANSFER_OTP);

			impsWithOtpResponse = decryptESBResponse(responseBody, ProductConstants.FUNDTRANSFER_OTP, sessionId);

			logger.info("Decrypted Response ::" + impsWithOtpResponse);

			responseDto = getIMPSResponse(request, responseDto, impsWithOtpResponse, Boolean.TRUE, requestId);

		} catch (NeoException ne) {
			logger.info("Exception Occured ::" + ne.getDetailMessage());
			throw ne;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
					NeoExceptionConstant.INTERNAL_SERVER_ERROR, null, Locale.US, impsWithOtpRequest,
					impsWithOtpResponse, ProductConstants.FUNDTRANSFER_OTP);
		} finally {
			saveFundTransferReq(request, responseDto, requestId, "SUCCESS", impsWithOtpResponse,
					ProductConstants.FUNDTRANSFER_OTP);
		}

		return responseDto;
	}

	public FundTransferResponseDto iftTransferWithOtp(FundTransferRequestDto request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		String bankId = BANKID;
		IFTTransferRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		FundTransferResponseDto response = new FundTransferResponseDto();
		String internalTransferResponse = null;
		String internalTransferRequest = null;
		String requestId = null;

		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, "YYYY-MM-dd HH:mm:ss");

			if (StringUtils.isBlank(request.getRemitterNumber()))
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.INVALID_REMITTER_NUMBER, null, null,
						Locale.US, null, null, ProductConstants.INTERNAL_TRANSFER_OTP);

			String remitterNumber = request.getRemitterNumber();

			if (remitterNumber != null && remitterNumber.length() > 10) {
				remitterNumber = request.getRemitterNumber().substring(request.getRemitterNumber().length() - 10);
			}

			Header header = commonSerivce.buildHeaderWithMobileNo(SbmPropertyConstants.INTERNAL_TRANSFER,
					remitterNumber);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1060";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.INTERNAL_TRANSFER_OTP);

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			XferTrnHdr xferTrnHdr = XferTrnHdr.builder().trnType("T").trnSubType("CI").build();

			com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec.AcctId fromAccountId = com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec.AcctId
					.builder().acctId(request.getFromAccountNo()).build();

			TrnAmt txnAmount = TrnAmt.builder().amountValue(request.getAmount())
					.currencyCode(request.getTransactionCurrency()).build();

			String txnDate = DateUtilityFunction.dateInFormat(new Date(),
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			String debitTrnParticulars = this.checkLength(request.getBeneficiaryName(), null,
					request.getExternalTransactionId(), request.getDescription());

			PartTrnRec debitReq = PartTrnRec.builder().acctId(fromAccountId).creditDebitFlg("D").trnAmt(txnAmount)
					.trnParticulars(debitTrnParticulars != null ? debitTrnParticulars
							: "IFT" + "/" + request.getExternalTransactionId() + "/" + request.getBeneficiaryName()
									+ "/" + request.getDescription())
					.partTrnRmks(request.getDescription()).valueDt(txnDate).build();

			com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec.AcctId toAccountId = com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec.AcctId
					.builder().acctId(request.getToAccountNo()).build();

			String creditTrnParticulars = this.checkLength(null, request.getRemitterName(),
					request.getExternalTransactionId(), request.getDescription());

			PartTrnRec creditReq = PartTrnRec.builder().acctId(toAccountId).creditDebitFlg("C").trnAmt(txnAmount)
					.trnParticulars(creditTrnParticulars != null ? creditTrnParticulars
							: "IFT" + "/" + request.getExternalTransactionId() + "/" + request.getRemitterName() + "/"
									+ request.getDescription())
					.partTrnRmks(request.getDescription()).valueDt(txnDate).build();

			List<PartTrnRec> partTrnRecList = new ArrayList<>();
			partTrnRecList.add(creditReq);
			partTrnRecList.add(debitReq);

			XferTrnDetail xTrndetails = XferTrnDetail.builder().partTrnRec(partTrnRecList).build();

			XferTrnAddRq xTrnAddRq = XferTrnAddRq.builder().xferTrnDetail(xTrndetails).xferTrnHdr(xferTrnHdr).build();

			XferTrnAddRequest xTrnAddRequest = XferTrnAddRequest.builder().xferTrnAddRq(xTrnAddRq).build();

			requestDto = IFTTransferRequestDto.builder()
					.body(com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto.Body.builder()
							.xferTrnAddRequest(xTrnAddRequest).build())
					.header(header).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getIftTransferRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.INTERNAL_TRANSFER_XSD);
			m.marshal(requestDto, sw);

			internalTransferRequest = sw.toString();

			logger.info("Actual payload ::" + internalTransferRequest);

			String encryptedPayload = encryptionUtil.encrypt(internalTransferRequest);

			RequestInDto internalTransferDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1060").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(internalTransferDto, stringWriter);

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.INTERNAL_TRANSFER_OTP);

			internalTransferResponse = decryptESBResponse(responseBody, ProductConstants.INTERNAL_TRANSFER_OTP, sessionId);

			logger.info("Decrypted Response ::" + internalTransferResponse);

			response = getIFTResponse(request, response, internalTransferResponse, Boolean.TRUE, requestId,
					internalTransferRequest, internalTransferResponse);

			return response;

		} catch (NeoException e) {
			logger.info("Exception Occured ::" + e.getDetailMessage());
			throw e;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					internalTransferRequest, internalTransferResponse, ProductConstants.INTERNAL_TRANSFER_OTP);
		} finally {
			saveFundTransferReq(request, response, requestId, "SUCCESS", internalTransferResponse,
					ProductConstants.FUNDTRANSFER_OTP);
		}
	}

	public FundTransferResponseDto addOutboundPymtWithOtp(FundTransferRequestDto request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		String bankId = BANKID;
		AddOutBoundPymtRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		FundTransferResponseDto response = new FundTransferResponseDto();
		String requestId = null;
		String fundTransferOtpResponse = null;
		String service = null;
		String fundTransferOtpRequest = null;

		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, "YYYY-MM-dd HH:mm:ss");

			if (StringUtils.isBlank(request.getRemitterNumber()))
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.INVALID_REMITTER_NUMBER, null, null,
						Locale.US, null, null, ProductConstants.FUNDTRANSFER_OTP);

			String remitterNumber = request.getRemitterNumber();

			if (remitterNumber != null && remitterNumber.length() > 10) {
				remitterNumber = request.getRemitterNumber().substring(request.getRemitterNumber().length() - 10);
			}

			Header header = commonSerivce
					.buildOutboundPaymentHeaderWithMobileNo(SbmPropertyConstants.OUTBOUND_TRANSACTION, remitterNumber);

			String bankTxnType = "";

			String url = null;

			String moduleId = "";

			if (request.getTransactionType() != null && request.getTransactionType().equals("NEFT")) {
				bankTxnType = "NEFT";
				url = esbBaseUrl
						+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1058";

				service = ProductConstants.FUNDTRANSFER_OTP_NEFT;

				PartnerServiceConfiguration serviceConfig = apiConfig
						.findByBankIdAndBankServiceId(configModel.getBankId(), service);

				if (serviceConfig != null) {
					url = serviceConfig.getServiceEndpoint();
				}

				moduleId = "Fin1058";
			} else if (request.getTransactionType() != null && request.getTransactionType().equals("RTGS")) {
				bankTxnType = "NRTGS";
				url = esbBaseUrl
						+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1059";
				PartnerServiceConfiguration serviceConfig = apiConfig
						.findByBankIdAndBankServiceId(configModel.getBankId(), ProductConstants.FUNDTRANSFER_OTP_RTGS);

				if (serviceConfig != null) {
					url = serviceConfig.getServiceEndpoint();
				}

				moduleId = "Fin1059";
			}

			String requestDate = DateUtilityFunction.dateInFormat(new Date(),
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			RequestedAmount requestedAmt = RequestedAmount.builder().amountValue(request.getAmount())
					.currencyCode("INR").build();

			AddressDetails senderAddress = AddressDetails.builder().name(request.getRemitterName()).build();

			com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorDtls.AddressDetails beneficiaryAddress = com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorDtls.AddressDetails
					.builder().name(request.getBeneficiaryName()).build();

			DebtorDtls debtorDtls = DebtorDtls.builder().accountId(request.getFromAccountNo())
					.addressDetails(senderAddress).build();

			CreditorDtls creditorDtls = CreditorDtls.builder().accountId(request.getToAccountNo())
					.addressDetails(beneficiaryAddress).build();

			NetworkIdentification nwId = NetworkIdentification.builder().networkDirectory("IFSC")
					.networkIdentifier(request.getBeneficiaryIfsc()).build();

			CreditorBankDtls creditorBankId = CreditorBankDtls.builder().networkIdentification(nwId).build();

			RemittanceInfo remiterInfo = RemittanceInfo.builder().remitInfo1(request.getDescription()).build();

			AddOutboundPymtEntryDtlsRq outBountDtlsRq = AddOutboundPymtEntryDtlsRq.builder().paysysId(bankTxnType)
					.requestedExecutionDate(requestDate).requestedAmount(requestedAmt).purposeCode("CASH")
					.requestedAmountCrncy("INR").waiveChargeFlag("Y").serviceId("NCP").serviceType("O")
					.debtorDtls(debtorDtls).creditorBankDtls(creditorBankId).creditorDtls(creditorDtls)
					.tranIdentification(request.getExternalTransactionId()).remittanceInfo(remiterInfo).build();

			AddOutboundPymtEntryDtlsRequest outboundReq = AddOutboundPymtEntryDtlsRequest.builder()
					.addOutboundPymtEntryDtlsRq(outBountDtlsRq).build();

			com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto.Body body = AddOutBoundPymtRequestDto.Body
					.builder().addOutboundPymtEntryDtlsRequest(outboundReq).build();

			requestDto = AddOutBoundPymtRequestDto.builder().header(header).body(body).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getAddOutBoundPymtRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.OUTBOUND_TRANSACTION_XSD);
			m.marshal(requestDto, sw);

			fundTransferOtpRequest = sw.toString();

			logger.info("Actual payload ::" + fundTransferOtpRequest);

			String encryptedPayload = encryptionUtil.encrypt(fundTransferOtpRequest);

			RequestInDto retailCustAddRequestDto = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID(moduleId).partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(retailCustAddRequestDto, stringWriter);

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, service);

			fundTransferOtpResponse = decryptESBResponse(responseBody, service, sessionId);

			logger.info("Decrypted Response ::" + fundTransferOtpResponse);

			response = getOutBoundPaymentResponse(request, response, fundTransferOtpResponse, Boolean.TRUE, requestId,
					fundTransferOtpRequest, fundTransferOtpResponse);

			return response;

		} catch (NeoException e) {
			logger.info("Exception Occured ::" + e.getDetailMessage());
			throw e;

		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					fundTransferOtpRequest, fundTransferOtpResponse, ProductConstants.FUNDTRANSFER_OTP);
		} finally {
			saveFundTransferReq(request, response, requestId, "SUCCESS", fundTransferOtpResponse,
					ProductConstants.FUNDTRANSFER_OTP);
		}
	}

	@Override
	public FundTransferResponseDto fundTransferValidateOtp(FundTransferRequestDto request) throws NeoException {

		StringWriter sw = new StringWriter();
		FundTransferResponseDto responseDto = new FundTransferResponseDto();
		String bankId = BANKID;
		String fundTransferValidateOtpRequest = null;
		String fundTransferValidationOtpResponse = null;

		try {

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);
			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();
			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, "YYYY-MM-dd HH:mm:ss");

			ValidateOtpRequestDto validateOtpRequest = new ValidateOtpRequestDto();

			validateOtpRequest.setOtp(request.getOtp());

			if (request.getResendOtp() != null && request.getResendOtp())
				validateOtpRequest.setResendOtp("Y");
			else
				validateOtpRequest.setResendOtp("N");

			validateOtpRequest.setTrfType("Core");
			validateOtpRequest.setStan(IdGeneratorUtility.getStan());

			String moduleId = request.getTransactionType();

			switch (moduleId) {

			case "IMPS":
				moduleId = "";
				break;
			case "NEFT":
				moduleId = "Fin1058";
				break;
			case "RTGS":
				moduleId = "Fin1059";
				break;
			case "IFT":
				moduleId = "Fin1060";
				break;

			default:
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.INVALID_TRANSACTION_MODULE_ID, null, null,
						Locale.US, null, null, ProductConstants.FUNDTRANSFER_VALIDATE_OTP);
			}

			NeoAccountTransferReqRes model = neoAccountReqRes
					.findByExternalTransactionId(request.getExternalTransactionId());

			validateOtpRequest.setRequestUUID(model.getRequestId());

			JAXBContext jaxbContext = NeoJAXBContextFactory.getValidateOtpRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.marshal(validateOtpRequest, sw);

			fundTransferValidateOtpRequest = sw.toString();

			logger.info("Plain text ::" + fundTransferValidateOtpRequest);

			String encryptedPayload = encryptionUtil.encrypt(fundTransferValidateOtpRequest);

			RequestInDto req = RequestInDto.builder().authType("").channelId(channelId).moduleID(moduleId)
					.partnerReqID(requestId).partnerUserName(configModel.getUserName()).timestamp(timeStamp)
					.requestType("Core").request(encryptedPayload).build();

			sw = new StringWriter();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(req, sw);

			String url = esbBaseUrl
					+ "/Magicxpi/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-AHTTP_otp_based_transaction%23Validate_OTP";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.FUNDTRANSFER_VALIDATE_OTP);

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			String responseBody = postForEntityWithHeaders(sw.toString(), url, HttpMethod.POST, headers, Boolean.TRUE,
					sslAlias, sslEnabled, ProductConstants.FUNDTRANSFER_VALIDATE_OTP);

			fundTransferValidationOtpResponse = decryptESBResponse(responseBody,
					ProductConstants.FUNDTRANSFER_VALIDATE_OTP, sessionId);

			logger.info("Decrypted Response ::" + fundTransferValidationOtpResponse);

			if (request.getTransactionType().equals("IMPS")) {
				responseDto = getIMPSResponse(request, responseDto, fundTransferValidationOtpResponse, Boolean.FALSE,
						requestId);

			} else if (request.getTransactionType().equals("IFT")) {
				responseDto = getIFTResponse(request, responseDto, fundTransferValidationOtpResponse, Boolean.FALSE,
						requestId, fundTransferValidateOtpRequest, fundTransferValidationOtpResponse);

			} else {
				responseDto = getOutBoundPaymentResponse(request, responseDto, fundTransferValidationOtpResponse,
						Boolean.FALSE, requestId, fundTransferValidateOtpRequest, fundTransferValidationOtpResponse);
			}

		} catch (NeoException e) {
			logger.info("Exception Occured ::" + e.getDetailMessage());
			throw e;

		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US,
					fundTransferValidateOtpRequest, fundTransferValidationOtpResponse,
					ProductConstants.FUNDTRANSFER_VALIDATE_OTP);
		}

		return responseDto;
	}

	public FundTransferResponseDto getIMPSResponse(FundTransferRequestDto request, FundTransferResponseDto responseDto,
			String decryptedResponse, Boolean isInitiateOTP, String reqId)
			throws JsonProcessingException, JsonMappingException {

		if (isInitiateOTP) {
			responseDto.setStatus("SUCCESS");
			responseDto.setDescription(decryptedResponse);
			return responseDto;
		}

		IMPSResponseDto impsResponse = new ObjectMapper().readValue(decryptedResponse, IMPSResponseDto.class);

		if (impsResponse != null && impsResponse.getResponsecode() != null
				&& impsResponse.getResponsecode().equals("00")) {
			responseDto.setExternalTransactionId(request.getExternalTransactionId());
			responseDto.setDescription(impsResponse.getResponsedesc());
			responseDto.setBankReferenceNo(impsResponse.getReferencenumber());
			responseDto.setStatus("SUCCESS");
		} else {
			responseDto.setStatus("FAILURE");
		}

		return responseDto;
	}

	public FundTransferResponseDto getIFTResponse(FundTransferRequestDto request, FundTransferResponseDto response,
			String decryptedResponse, Boolean isInitiateOTP, String reqId, String requestPayload,
			String responsePayload) throws JAXBException, NeoException {

		if (isInitiateOTP) {
			response.setStatus("SUCCESS");
			response.setDescription(decryptedResponse);
			return response;
		}

		JAXBContext jaxbResContext = NeoJAXBContextFactory.getIftTransferResponseContext();
		Unmarshaller um = jaxbResContext.createUnmarshaller();

		StringReader decryptedResponsereader = new StringReader(decryptedResponse);
		IFTTransferResponseDto iftTransactionResponseDto = (IFTTransferResponseDto) um
				.unmarshal(decryptedResponsereader);

		if (iftTransactionResponseDto.getHeader().getResponseHeader().getHostTransaction().getStatus()
				.equals("SUCCESS")) {

			logger.info("SUCCESS RESPONSE");
			String tranIdentification = iftTransactionResponseDto.getBody().getXferTrnAddResponse().getXferTrnAddRs()
					.getTrnIdentifier().getTrnId();
			response.setBankReferenceNo(tranIdentification);
			response.setStatus("SUCCESS");
			response.setExternalTransactionId(request.getExternalTransactionId());
		} else {
			logger.info("FAILURE RESPONSE");

			if (iftTransactionResponseDto.getBody().getError() != null) {

				if (iftTransactionResponseDto.getBody().getError().getFiSystemException() != null) {
					throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
							iftTransactionResponseDto.getBody().getError().getFiSystemException().getErrorDetail()
									.getErrorDesc(),
							null, Locale.US, requestPayload, responsePayload, ProductConstants.INTERNAL_TRANSFER);
				} else if (iftTransactionResponseDto.getBody().getError().getFIBusinessException() != null) {
					throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
							iftTransactionResponseDto.getBody().getError().getFIBusinessException().getErrorDetail()
									.getErrorDesc(),
							null, Locale.US, requestPayload, responsePayload, ProductConstants.INTERNAL_TRANSFER);
				}

			}

		}

		return response;
	}

	public FundTransferResponseDto getOutBoundPaymentResponse(FundTransferRequestDto request,
			FundTransferResponseDto response, String decryptedResponse, Boolean isInitiateOTP, String reqId,
			String requestPayload, String responsePayload)

			throws JAXBException, NeoException {

		if (isInitiateOTP) {
			response.setStatus("SUCCESS");
			response.setDescription(decryptedResponse);
			return response;
		}

		JAXBContext jaxbResContext = NeoJAXBContextFactory.getAddOutBoundPymtResponseContext();
		Unmarshaller um = jaxbResContext.createUnmarshaller();

		StringReader decryptedResponsereader = new StringReader(decryptedResponse);
		AddOutBoundPymtResponseDto ress = (AddOutBoundPymtResponseDto) um.unmarshal(decryptedResponsereader);

		if (ress.getHeader().getResponseHeader().getHostTransaction().getStatus().equals("SUCCESS")) {

			logger.info("SUCCESS RESPONSE");
			String tranIdentification = ress.getBody().getAddOutboundPymtEntryDtlsResponse()
					.getAddOutboundPymtEntryDtlsRs().getTranIdentification();
			response.setBankReferenceNo(tranIdentification);
			response.setStatus("SUCCESS");
			response.setExternalTransactionId(request.getExternalTransactionId());
		} else {
			logger.info("FAILURE RESPONSE");

			if (ress.getBody().getError() != null) {

				if (ress.getBody().getError().getFiSystemException() != null) {
					throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
							ress.getBody().getError().getFiSystemException().getErrorDetail().getErrorDesc(), null,
							Locale.US, requestPayload, responsePayload, ProductConstants.OUTBOUND_TRANSACTION_NEFT);
				} else if (ress.getBody().getError().getFIBusinessException() != null) {
					throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
							ress.getBody().getError().getFIBusinessException().getErrorDetail().getErrorDesc(), null,
							Locale.US, requestPayload, responsePayload, ProductConstants.OUTBOUND_TRANSACTION_NEFT);
				}

			}

		}

		return response;
	}

	public void saveFundTransferReq(FundTransferRequestDto request, FundTransferResponseDto response, String requestId,
			String status, String description, String productId) {

		NeoAccountTransferReqRes model = dzMapper.map(request, NeoAccountTransferReqRes.class);

		model.setRequestId(requestId);
		model.setTenant(TenantContextHolder.getNeoTenant());
		model.setStatus(status);
		model.setResponseDescription(description);
		model.setProductId(productId);

		try {
			neoAccountReqRes.save(model);
		} catch (Exception e) {
			logger.info("Exception ", e);
		}

	}

	@Override
	public AccountInquiryResponseDto fetchAccountDetails(AccountInquiryRequestDto request) throws NeoException {
		AccountInquiryResponseDto response = null;

		try {
			response = auditService.fetchAccountDetailByAccountNo(request.getAccountId());
		} catch (NeoException e) {
			logger.info("Exception Occured ::" + e.getDetailMessage());
			throw e;
		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.UNABLE_FETCH_ACCOUNT, null, null, Locale.US,
					null, null, ProductConstants.SAVINGS_ACCOUNT_INQUIRY);
		}
		return response;
	}

	@Override
	public CustomerDedupCheckResponseDto dedupCheck(RegistrationRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterAccountDiscovery(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterAcctBalInquiry(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterOtpCreation(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterOtpVerification(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterDebitOrchestration(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public BeneficiaryManagementResDto beneficiaryAddition(BeneficiaryManagementReqDto request) throws NeoException {
		return null;
	}

	@Override
	public FetchAccountStatemetResponse fetchMiniStatement(FetchAccountStatementRequest request) throws NeoException {

		StringWriter stringWriter = new StringWriter();
		String bankId = BANKID;
		MiniStatementRequestDto requestDto = null;
		StringWriter sw = new StringWriter();
		FetchAccountStatemetResponse response = new FetchAccountStatemetResponse();

		try {

			if (StringUtils.isEmpty(request.getAccountNo()))
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.ACCOUNTINFO_INCORRECT, null, null, Locale.US,
						null, null, ProductConstants.FETCH_MINISTATEMENT);

			MasterConfiguration configModel = masterConfig.findByBankId(bankId);

			String requestId = idGeneratorUtility.getSBMPartnerRequestId(configModel.getPartnerName(),
					configModel.getUserName());
			String channelId = configModel.getChannelName();

			Date date = new Date();
			String timeStamp = DateUtilityFunction.dateInFormat(date, SbmPropertyConstants.ESB_TIMESTAMP_FORMAT);

			Header header = commonSerivce.buildHeader(SbmPropertyConstants.FETCH_MINISTATEMENT);

			AccountListElement accountListElement = MiniStatementRequestDto.Body.GetMiniAccountStatementRequest.AccountListElement
					.builder().acid(request.getAccountNo()).branchId(request.getBranchId()).build();

			GetMiniAccountStatementRequest miniStatementRequestBody = MiniStatementRequestDto.Body.GetMiniAccountStatementRequest
					.builder().accountListElement(accountListElement).build();

			requestDto = MiniStatementRequestDto.builder().body(MiniStatementRequestDto.Body.builder()
					.getMiniAccountStatementRequest(miniStatementRequestBody).build()).header(header).build();

			JAXBContext jaxbContext = NeoJAXBContextFactory.getMiniStatementRequestContext();
			Marshaller m = jaxbContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, SbmPropertyConstants.FETCH_MINISTATEMENT_XSD);
			m.marshal(requestDto, sw);

			String miniStatementRequest = sw.toString();

			logger.info("Actual Request ::" + miniStatementRequest);

			String encryptedPayload = encryptionUtil.encrypt(miniStatementRequest);

			RequestInDto miniStatementEsbRequest = RequestInDto.builder().authType("").channelId(channelId)
					.moduleID("Fin1015").partnerReqID(requestId).partnerUserName(configModel.getUserName())
					.timestamp(timeStamp).requestType("CORE").request(encryptedPayload).build();

			JAXBContext requestInJAXB = NeoJAXBContextFactory.getRequestInContext();
			Marshaller requestInMarshaller = requestInJAXB.createMarshaller();
			requestInMarshaller.marshal(miniStatementEsbRequest, stringWriter);

			String url = esbBaseUrl
					+ "/Magicxpi4.9/MgWebRequester.dll?appname=IFSSBM_Core_Integration_temp&prgname=HTTP&arguments=-ACoreAPI%23SBMCoreAPI_Fin1015";

			PartnerServiceConfiguration serviceConfig = apiConfig.findByBankIdAndBankServiceId(configModel.getBankId(),
					ProductConstants.FETCH_MINISTATEMENT);

			String sslAlias = configModel.getSslAlias();
			Boolean sslEnabled = configModel.getSslEnabled();

			if (serviceConfig != null) {
				url = serviceConfig.getServiceEndpoint();
			}

			String sessionId = getBankSessionId();

			HttpHeaders headers = new HttpHeaders();
			headers.add("PARTNERNAME", configModel.getPartnerName());
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.set("SESSIONID", sessionId);

			String responseBody = postForEntityWithHeaders(stringWriter.toString(), url, HttpMethod.POST, headers,
					Boolean.TRUE, sslAlias, sslEnabled, ProductConstants.FETCH_MINISTATEMENT);

			String decryptedResponse = decryptESBResponse(responseBody, ProductConstants.FETCH_MINISTATEMENT, sessionId);

			logger.info("Decrypted Response ::" + decryptedResponse);

			JAXBContext jaxbResContext = NeoJAXBContextFactory.getMiniStatementResponseContext();
			Unmarshaller um = jaxbResContext.createUnmarshaller();

			StringReader decryptedResponsereader = new StringReader(decryptedResponse);
			MiniStatementResponseDto ress = (MiniStatementResponseDto) um.unmarshal(decryptedResponsereader);

			if (ress.getHeader().getResponseHeader().getHostTransaction().getStatus().equals("SUCCESS")) {

				logger.info("SUCCESS RESPONSE");
				MiniStatementSummary miniStatementSummary = ress.getBody().getGetMiniAccountStatementResponse()
						.getMiniStatementSummary();

				String availableBalance = miniStatementSummary.getAccountSummary().getAvailableBalance()
						.getAmountValue();
				String currencyCode = miniStatementSummary.getAccountSummary().getAvailableBalance().getCurrencyCode();
				String accountId = miniStatementSummary.getAccountSummary().getAcid();
				String branchId = miniStatementSummary.getAccountSummary().getBranchId();
				String ledgerBalance = miniStatementSummary.getAccountSummary().getLedgerBalance().getAmountValue();

				List<AccountTransactionSummary> txnDetails = miniStatementSummary.getAccountTransactionSummary();

				List<Transaction> txnList = new ArrayList<>();
				SimpleDateFormat formattxnDate = new SimpleDateFormat(SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

				for (AccountTransactionSummary txDetails : txnDetails) {

					Date fTxnDate = formattxnDate.parse(txDetails.getTxnDate());

					Transaction txn = new Transaction();
					txn.setAmount(txDetails.getTxnAmt().getAmountValue());
					txn.setDescription(txDetails.getTxnDesc());
					txn.setTime(fTxnDate.getTime());

					if (txDetails.getTxnType().equals("C"))
						txn.setType("CREDIT");
					else if (txDetails.getTxnType().equals("D"))
						txn.setType("DEBIT");

					txn.setCurrency(txDetails.getTxnAmt().getCurrencyCode());

					txnList.add(txn);
				}

				response.setAccountCurrency(currencyCode);
				response.setAccountId(accountId);
				response.setAvailableBalance(availableBalance);
				response.setLedgerBalance(ledgerBalance);
				response.setBranchId(branchId);
				response.setTransaction(txnList);

			} else {
				logger.info("FAILURE RESPONSE");

				if (ress.getBody().getError() != null) {

					if (ress.getBody().getError().getFiSystemException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFiSystemException().getErrorDetail().getErrorDesc(), null,
								Locale.US, null, null, ProductConstants.FETCH_MINISTATEMENT);
					} else if (ress.getBody().getError().getFIBusinessException() != null) {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE,
								ress.getBody().getError().getFIBusinessException().getErrorDetail().getErrorDesc(),
								null, Locale.US, null, null, ProductConstants.FETCH_MINISTATEMENT);
					}
				}
			}

			return response;

		} catch (NeoException e) {
			logger.info("Exception Occured ::" + e.getDetailMessage());
			throw e;

		} catch (Exception e) {
			logger.info("Exception Occured ::" + e.getMessage());
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_UNAVAILABLE, null, null, Locale.US, null,
					null, ProductConstants.FETCH_MINISTATEMENT);
		}
	}

	@Override
	public NeoPayResponse upiPay(NeoPayRequest request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ValidateVpaRes validateVpa(ValidateVpaReq request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MerchantOBRes onboardSubMerchant(MerchantOBReq request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}
	
}