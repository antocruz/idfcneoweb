package com.neo.aggregator.serviceimpl;

import java.util.concurrent.Future;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.neo.aggregator.constant.PosConstants;
import com.neo.aggregator.dao.IssuerAuthorizationConfigRepo;
import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.IsoAuthorizationConfig;
import com.neo.aggregator.service.POSServiceProcess;
import com.neo.aggregator.utility.Iso8583;
import com.neo.aggregator.utility.IsoMessageToIso8583Converter;
import com.neo.aggregator.validator.MandatoryFieldCheckValidator;

@Service
public class POSServiceProcessImpl implements POSServiceProcess {

	@Autowired
	@Qualifier("posPurchaseValidator")
	private MandatoryFieldCheckValidator posValidator;

	@Autowired
	private IsoMessageToIso8583Converter isoMessageToIso8583Converter;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private IssuerAuthorizationConfigRepo configRepo;

	@Autowired
	@Lazy
	private EncryptDecryptStringWithAES encryptDecryptStringWithAES;

	@Value("${validate.checksum}")
	private Boolean validateCheckSum;
	
	@Value("${switch.network}")
    private String switchNetwork;
	
	private static String[] csArray= { "02", "04", "07", "22", "32", "37", "41", "42", "43", "49"  };
	private static String[] encArray= { "11", "18", "25", "03", "32", "02", "04", "07", "22", "32"  };

	
	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(POSServiceProcessImpl.class);

	@Override
	public Future<Iso8583> processPosRequest(IsoMessage requestIso8583,
			IsoMessage responseIso8583) {
		logger.info("Process POS Request Start :::");

		Iso8583 requet = isoMessageToIso8583Converter.createIso8583Object(requestIso8583);
		Iso8583 response = isoMessageToIso8583Converter
				.createIso8583Object(responseIso8583);

		if (posValidator.validate(requet)) {

		}
		else {
			response.setResponseCode(PosConstants.COMPLAINCE_ISSUE_RESPONSE_CODE);
		}

		logger.info("Process POS Request End :::");
		return new AsyncResult<Iso8583>(response);

	}

	@Override
	public Future<Iso8583> authorizeTransaction(IsoMessage requestIso8583,
			IsoMessage responseIso8583) {
		Iso8583 request = isoMessageToIso8583Converter
				.createIso8583Object(requestIso8583);

		Iso8583 response = isoMessageToIso8583Converter
				.createIso8583Object(responseIso8583);
		response.setChipData(null);
	
		logger.info("valid iso8583 so sending request to host for authorization");
		String bin = request.getPan().substring(0, 6);
		String productCode = request.getPan().substring(6, 8);
		IsoAuthorizationConfig config = configRepo
				.findByBinAndProductCodeAndFieldName(bin, productCode,
						PosConstants.POS_AUTHORIZATION_URL);
		if(config == null){
			config = configRepo
					.findByBinAndFieldName(bin, PosConstants.POS_AUTHORIZATION_URL);
		}
		logger.info("POS Auth Url :: " + config.getFieldValue());
/*		if(request.getProcessingCode().substring(0, 2).equals(POSConstants.WD_PROC_ODE) && !request.getMcc().equals(POSConstants.ATM_MCC)){
			request.setAccountIdentification1(request.getMcc());
			request.setMcc(POSConstants.CASHATPOSMCC);
		}*/
		String checkSum = null;
		String encValue = null;
		if(validateCheckSum) {
			logger.info("Check sum generation and validation enabled.");
			checkSum = getCSValue(request, request.getDateTimeTransaction());
			encValue = getENCValue(request, request.getDateTimeTransaction());
			encValue = StringUtils.leftPad(encValue, 16, "F");
			encValue = encValue.substring(0, 16);
			
			String encryptedData = encryptDecryptStringWithAES.encrypt(checkSum, encValue);
			request.setCheckSum(encryptedData);
			logger.info("Request checksum : "+encryptedData);
		}	
		ResponseEntity<Iso8583> res = restTemplate.postForEntity(
				config.getFieldValue(), request, Iso8583.class,
				new Object[] { config.getFieldValue() });
		logger.info("after host authorization response :::");
		Iso8583 response8583 = res.getBody();
		if(validateCheckSum) {
			logger.info("Validating response checksum");
			if(response8583.getResponseCode()!=null && response8583.getResponseCode().equals("00")) {
				checkSum=checkSum+"00";
				if(response8583.getAuthorizationIdResponse()!=null) {
					checkSum=checkSum+response8583.getAuthorizationIdResponse();
				}
				String responseCheckSum = encryptDecryptStringWithAES.encrypt(checkSum, encValue);
				logger.info("Response checksum generated :"+responseCheckSum+" Response checksum in iso8583 :"+response8583.getCheckSum());
				if(!responseCheckSum.equals(response8583.getCheckSum())) {
					response8583.setAuthorizationIdResponse(null);
					response8583.setResponseCode("34");
					logger.error("Check sum mismatch critical error need to be monitored" );
				}
			}
		}
		
		return new AsyncResult<Iso8583>(response8583);
	}
	
	@Override
	public Future<Iso8583> authorizeTransaction(Iso8583 request,
			Iso8583 response) {
		
		response.setChipData(null);
		if (posValidator.validate(request)) {

			logger.info("valid iso8583 so sending request to host for authorization");
			String bin = request.getPan().substring(0, 6);
			String productCode = request.getPan().substring(6, 8);
			IsoAuthorizationConfig config = configRepo
					.findByBinAndProductCodeAndFieldName(bin, productCode,
							PosConstants.POS_AUTHORIZATION_URL);
			if(config == null){
				config = configRepo
						.findByBinAndFieldName(bin, PosConstants.POS_AUTHORIZATION_URL);
			}
			if(config!=null) {
				logger.info("POS Auth Url :: " + config.getFieldValue());
				
/*				if(request.getProcessingCode().substring(0, 2).equals(POSConstants.WD_PROC_ODE) && !request.getMcc().equals(POSConstants.ATM_MCC)){
					request.setAccountIdentification1(request.getMcc());
					request.setMcc(POSConstants.CASHATPOSMCC);
				}*/
				String checkSum = null;
				String encValue = null;
				if(validateCheckSum) {
					logger.info("Check sum generation and validation enabled.");
					checkSum = getCSValue(request, request.getDateTimeTransaction());
					encValue = getENCValue(request, request.getDateTimeTransaction());
					encValue = StringUtils.leftPad(encValue, 16, "F");
					encValue = encValue.substring(0, 16);
					
					String encryptedData = encryptDecryptStringWithAES.encrypt(checkSum, encValue);
					request.setCheckSum(encryptedData);
					logger.info("Request checksum : "+encryptedData);
				}	
				Iso8583 response8583 = new Iso8583();
				try {
					ResponseEntity<Iso8583> res = restTemplate.postForEntity(
							config.getFieldValue(), request, Iso8583.class,
							new Object[] { config.getFieldValue() });
					logger.info("after host authorization response :::" + res.getBody().toString());
					response8583 = res.getBody();
					if(validateCheckSum) {
						logger.info("Validating response checksum");
						if(response8583.getResponseCode()!=null && response8583.getResponseCode().equals("00")) {
							checkSum=checkSum+"00";
							if(response8583.getAuthorizationIdResponse()!=null) {
								checkSum=checkSum+response8583.getAuthorizationIdResponse();
							}
							String responseCheckSum = encryptDecryptStringWithAES.encrypt(checkSum, encValue);
							logger.info("Response checksum generated :"+responseCheckSum+" Response checksum in iso8583 :"+response8583.getCheckSum());
							if(!responseCheckSum.equals(response8583.getCheckSum())) {
								response8583.setAuthorizationIdResponse(null);
								response8583.setResponseCode("34");
								logger.error("Check sum mismatch critical error need to be monitored" );
							}
						}
					}
				}catch(Exception e) {
					logger.info("Error ", e);
					if(switchNetwork.toUpperCase().equals("VISA"))
						response.setVisaAdditionalResponseField(new Iso8583().new VisaAdditionalResponseField());
					response8583.setResponseCode("05");
				}
				return new AsyncResult<Iso8583>(response8583);
			}
			else {
				if(switchNetwork.toUpperCase().equals("VISA"))
					response.setVisaAdditionalResponseField(new Iso8583().new VisaAdditionalResponseField());
				response.setResponseCode("05");
			}
			
		}else {
			if(switchNetwork.toUpperCase().equals("VISA"))
				response.setVisaAdditionalResponseField(new Iso8583().new VisaAdditionalResponseField());
			response.setResponseCode("05");
		}
		return new AsyncResult<Iso8583>(response);
	}

	@Override
	public Future<Iso8583> reverseTransaction(IsoMessage requestIso8583,
			IsoMessage responseIso8583) {
		
		Iso8583 request = isoMessageToIso8583Converter
				.createIso8583Object(requestIso8583);
		logger.info("valid iso8583 so sending request to host for reversal");

		String bin = request.getPan().substring(0, 6);
		String productCode = request.getPan().substring(6, 8);
		IsoAuthorizationConfig config = configRepo.findByBinAndProductCodeAndFieldName(
				bin, productCode, PosConstants.POS_REVERSAL_URL);
		if(config==null){
			config = configRepo.findByBinAndFieldName(
					bin, PosConstants.POS_REVERSAL_URL);
		}
		if(config!=null) {
			logger.info("POS Reversal Url :: " + config.getFieldValue());
			try{
				ResponseEntity<Iso8583> res = restTemplate.postForEntity(config.getFieldValue(),
						request, Iso8583.class, new Object[] { config.getFieldValue() });
				logger.info("after host reversal response :::");
		
				return new AsyncResult<Iso8583>(res.getBody());
			}catch(Exception e) {
				logger.info("Exception for reversal response ::: "+e.getMessage());
				request.setResponseCode("05");
				return new AsyncResult<Iso8583>(request);
			}
		}else {
			request.setResponseCode("05");
			return new AsyncResult<Iso8583>(request);
		}
	}
	
	public Future<Iso8583> adviseTransaction(IsoMessage request8583, IsoMessage response8583){
		
		Iso8583 request = isoMessageToIso8583Converter
				.createIso8583Object(request8583);
		Iso8583 response = isoMessageToIso8583Converter
				.createIso8583Object(response8583);
		logger.info("valid iso8583 so sending request to host for advise");

		String bin = request.getPan().substring(0, 6);
		String productCode = request.getPan().substring(6, 8);
		IsoAuthorizationConfig config = configRepo.findByBinAndProductCodeAndFieldName(
				bin, productCode, PosConstants.POS_ADVISE_URL);
		if(config==null){
			config = configRepo.findByBinAndFieldName(
					bin, PosConstants.POS_ADVISE_URL);
		}
		if(config!=null) {

			try{
				logger.info("POS Advise Url :: " + config.getFieldValue());
				ResponseEntity<Iso8583> res = restTemplate.postForEntity(config.getFieldValue(),
						request, Iso8583.class, new Object[] { config.getFieldValue() });
				logger.info("after host advise response :::");
		
				return new AsyncResult<Iso8583>(res.getBody());
			}catch(Exception e){
				logger.info("Exception for reversal response ::: "+e.getMessage());
				response.setResponseCode("05");
				return new AsyncResult<Iso8583>(response);
			}
		}else {
			response.setResponseCode("05");
			return new AsyncResult<Iso8583>(response);
		}
	}

	private String getCSValue(Iso8583 iso8583, String value) {
		int first = Integer.parseInt(value.substring(2,3));
		int second = Integer.parseInt(value.substring(3,4));
		int third = Integer.parseInt(value.substring(4,5));
		
		String chkSum = returnCSValue(iso8583, csArray[first]);
		chkSum = chkSum.concat(returnCSValue(iso8583, csArray[second]));
		chkSum = chkSum.concat(returnCSValue(iso8583, csArray[third]));
		
		return chkSum;
	}
	
	private String getENCValue(Iso8583 iso8583, String value) {
		int startingOffset = Integer.parseInt(value.substring(4,5));
		int first = Integer.parseInt(value.substring(startingOffset,startingOffset+1));
		if((startingOffset+1)==10) {
			startingOffset=-1;
		}
		int second = Integer.parseInt(value.substring(startingOffset+1,startingOffset+2));
		if((startingOffset+2)==10) {
			startingOffset=-1;
		}
		int third = Integer.parseInt(value.substring(startingOffset+2,startingOffset+3));
		
		String chkSum = returnCSValue(iso8583, encArray[first]);
		chkSum = chkSum.concat(returnCSValue(iso8583, encArray[second]));
		chkSum = chkSum.concat(returnCSValue(iso8583, encArray[third]));
		
		return chkSum;
	}
	
	private String returnCSValue(Iso8583 iso8583, String csOffset) {
		switch(csOffset) {
			case "02":
				return iso8583.getPan();
			case "03":
				return iso8583.getProcessingCode();
			case "04":
				return iso8583.getTransactionAmount();
			case "07":
				return iso8583.getDateTimeTransaction();
			case "11":
				return iso8583.getStan();
			case "18":
				return iso8583.getMcc();
			case "22":
				return iso8583.getPosEntryMode();
			case "25":
				return iso8583.getPointOfServiceConditionCode();
			case "32":
				return iso8583.getAcquiringInstitutionCode();
			case "37":
				return iso8583.getRrn();
			case "41":
				return iso8583.getCardAcceptorId();
			case "42":
				return iso8583.getCardAcceptorTerminalId();
			case "43":
				return iso8583.getCardAcceptorNameLocation();
			case "49":
				return iso8583.getTransactionCurrencyCode();
			case "52":
				if(iso8583.getPinData()==null) {
					return "";
				}
				return iso8583.getPinData();
				
		}
		return "";
	}

	@Override
	public Future<Iso8583> serverInitTransaction(Iso8583 request, Iso8583 response) {
		
		logger.info("valid iso8583 so sending request to host for advise");

		String bin = request.getPan().substring(0, 6);
		String productCode = request.getPan().substring(6, 8);
		IsoAuthorizationConfig config = configRepo.findByBinAndProductCodeAndFieldName(
				bin, productCode, PosConstants.POS_SERVER_AUTH_URL);
		if(config==null){
			config = configRepo.findByBinAndFieldName(
					bin, PosConstants.POS_SERVER_AUTH_URL);
		}
		if(config!=null) {

			try{
				logger.info("POS Server Auth Url :: " + config.getFieldValue());
				ResponseEntity<Iso8583> res = restTemplate.postForEntity(config.getFieldValue(),
						request, Iso8583.class, new Object[] { config.getFieldValue() });
				logger.info("after host advise response :::");
		
				return new AsyncResult<Iso8583>(res.getBody());
			}catch(Exception e){
				logger.info("Exception for reversal response ::: "+e.getMessage());
				response.setResponseCode("05");
				return new AsyncResult<Iso8583>(response);
			}
		}else {
			response.setResponseCode("05");
			return new AsyncResult<Iso8583>(response);
		}
	}
	
}
