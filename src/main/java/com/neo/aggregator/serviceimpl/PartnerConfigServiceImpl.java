package com.neo.aggregator.serviceimpl;

import com.neo.aggregator.dao.MasterConfigDao;
import com.neo.aggregator.dao.PartnerAggregatorConfig;
import com.neo.aggregator.dao.PartnerServiceConfigRepo;
import com.neo.aggregator.model.MasterConfiguration;
import com.neo.aggregator.model.PartnerMasterConfiguration;
import com.neo.aggregator.model.PartnerServiceConfiguration;
import com.neo.aggregator.service.PartnerConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class PartnerConfigServiceImpl implements PartnerConfigService {

	private Logger logger = LoggerFactory.getLogger(PartnerConfigServiceImpl.class);

	@Autowired
	private PartnerAggregatorConfig partnerConfigDao;

	@Autowired
	PartnerServiceConfigRepo apiConfig;

	@Autowired
	private MasterConfigDao masterConfig;

	@Override
	@Cacheable(value = "findPartnerMasterConfigByTenant", key = "T(java.util.Objects).hash(#neoTenant)")
	public PartnerMasterConfiguration findPartnerMasterConfigByTenant(String neoTenant) {
		PartnerMasterConfiguration config = partnerConfigDao.findByTenant(neoTenant);
		if (config != null) {
			logger.info("Fetching Partner Service based on bank :: {}", config.getBankId());
			return config;
		} else {
			logger.info("Product config not found for {} '", neoTenant);
		}
		return null;
	}

	@Override
	@Cacheable(value = "findMasterConfigByPartnerId", key = "T(java.util.Objects).hash(#partnerId)")
	public MasterConfiguration findMasterConfigByPartnerId(String partnerId) {
		MasterConfiguration config = masterConfig.findByPartnerId(partnerId);
		if (config != null) {
			logger.info("Fetching Master config for  :: {}", partnerId);
			return config;
		} else {
			logger.info("Master config not found for {} '", partnerId);
		}
		return null;
	}

	@Override
	@Cacheable(value = "findPartnerServiceByBankIdAndBankServiceId", key = "T(java.util.Objects).hash(#bankId, #serviceId)")
	public PartnerServiceConfiguration findPartnerServiceByBankIdAndBankServiceId(String bankId,String serviceId) {
		PartnerServiceConfiguration config = apiConfig.findByBankIdAndBankServiceId(bankId,serviceId);
		if (config != null) {
			logger.info("Fetching Partner Service config for Bank: {} :: Service: {}", bankId,serviceId);
			return config;
		} else {
			logger.info("Partner config not foundfor Bank: {} :: Service: {}", bankId,serviceId);
		}
		return null;
	}

}