package com.neo.aggregator.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.dto.LienMarkRequestDto;
import com.neo.aggregator.dto.LienMarkResponseDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.NeoResponse;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.service.RemoteService;
import com.neo.aggregator.service.SbmEbsService;
import com.neo.aggregator.utility.TenantContextHolder;

@Service
public class RemoteServiceImpl implements RemoteService {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(RemoteServiceImpl.class);

	private SbmEbsService sbmEsbSerive;

	public SbmEbsService getSbmEsbSerive() {
		return sbmEsbSerive;
	}

	@Autowired
	public void setSbmEsbSerive(SbmEbsService sbmEsbSerive) {
		this.sbmEsbSerive = sbmEsbSerive;
	}

	@Override
	public NeoResponse fetchData(Object request, String url, String tenant, HttpMethod method) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.add("TENANT", tenant);
		headers.setContentType(MediaType.APPLICATION_JSON);
		NeoResponse response = new NeoResponse();
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			String data = mapper.writeValueAsString(request);
			
			logger.info("Request to Server ::" + data);
			
			HttpEntity<String> entity = new HttpEntity<String>(data, headers);
			ResponseEntity<NeoResponse> res = restTemplate.exchange(url, method, entity, NeoResponse.class,
					new Object[] { url });
			logger.info("Response from Server ::" + res.getBody());
			return res.getBody();
		} catch (Exception e) {
			logger.info("Exception Occured :: "+e.getLocalizedMessage());
		}
		return response;
	}

	@Async
	@Override
	public void asyncCallLienMark(LienMarkRequestDto request,String neoTenant) {
		
		TenantContextHolder.setNeoTenant(neoTenant);
		
		LienMarkResponseDto response = new LienMarkResponseDto();
		try {
			response = sbmEsbSerive.addLienMark(request);
			logger.info(response.toString());
		} catch(NeoException e) {
			logger.info("Neo Exception ::"+e.getDisplayMessage());
		}
	}
}
