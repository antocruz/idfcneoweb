package com.neo.aggregator.serviceimpl;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;


import com.ecs.okyc.webapi.client.ECSOkycWebApiClient;
import com.neo.aggregator.bank.rbl.AadhaarXmlDto;
import com.neo.aggregator.bank.rbl.request.*;
import com.neo.aggregator.bank.rbl.response.*;
import com.neo.aggregator.commonservice.RblCommonService;
import com.neo.aggregator.dao.*;
import com.neo.aggregator.dto.*;
import com.neo.aggregator.enums.GenderType;
import com.neo.aggregator.model.*;
import com.neo.aggregator.service.NeoBusinessCustomFieldService;
import com.neo.aggregator.service.PartnerConfigService;
import com.neo.aggregator.service.RblEbsService;
import com.neo.aggregator.utility.NeoExceptionBuilder;
import com.neo.aggregator.utility.NeoExceptionConstant;
import com.neo.aggregator.utility.Utils;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.neo.aggregator.bank.rbl.BusinessEntityListDto;
import com.neo.aggregator.bank.rbl.RblBioEkycRequestLog;
import com.neo.aggregator.bank.rbl.dao.RblBioEkycRequestLogDao;
import com.neo.aggregator.dao.EcollectRequestDao;
import com.neo.aggregator.constant.NeoCustomFieldConstants;
import com.neo.aggregator.constant.ProductConstants;
import com.neo.aggregator.constant.RblPropertyConstants;
import com.neo.aggregator.controller.VirtualAccountController;
import com.neo.aggregator.dto.ecollect.AddVARequestDto;
import com.neo.aggregator.dto.ecollect.AddVAResponseDto;
import com.neo.aggregator.dto.ecollect.rbl.RblEcollectResponseDto;
import com.neo.aggregator.dto.ecollect.rbl.RblVACreateRequestDto;
import com.neo.aggregator.dto.ecollect.rbl.RblVACreateResponseDto;
import com.neo.aggregator.dto.ecollect.rbl.VARequestBody;
import com.neo.aggregator.dto.ecollect.rbl.VARequestHeader;
import com.neo.aggregator.dto.v2.AddressDto;
import com.neo.aggregator.dto.v2.SpecialDatesDto;
import com.neo.aggregator.dto.yesbank.MerchantOBReq;
import com.neo.aggregator.dto.yesbank.MerchantOBRes;
import com.neo.aggregator.dto.yesbank.NeoPayRequest;
import com.neo.aggregator.dto.yesbank.NeoPayResponse;
import com.neo.aggregator.dto.yesbank.ValidateVpaReq;
import com.neo.aggregator.dto.yesbank.ValidateVpaRes;
import com.neo.aggregator.enums.AddressCategory;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.service.AggregatorService;
import com.neo.aggregator.service.RemoteService;
import com.neo.aggregator.utility.CommonService;
import com.neo.aggregator.utility.DateUtilityFunction;
import com.neo.aggregator.utility.TenantContextHolder;
import com.neo.aggregator.utility.TxnIdGenerator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

@Service("rblServiceImpl")
public class RblEsbServiceImpl implements AggregatorService, RblEbsService {

	@Autowired
	VirtualAccountCreationRegisterDao vaRepo;

	@Autowired
	BusinessEntityDao entityRepo;

	@Autowired @Lazy
	private RemoteService rmService;

	@Autowired
	private RestTemplate restTemplate;

	@Value("${neo.agg.local.url}")
	private String localUrl;

	@Value("${neo.agg.va.rbl.createurl}")
	private String rblVACreateUrl;

	@Value("${neo.agg.rbl.LDAP.User}")
	private String rblLDAPUser;

	@Value("${neo.agg.rbl.LDAP.Password}")
	private String rblLDAPPwd;

	@Value("${neo.agg.rbl.url.ClientID}")
	private String rblUrlClientId;

	@Value("${neo.agg.rbl.url.ClientSecret}")
	private String rblUrlClientSecret;

	@Value("${neo.keystore.password}")
	private String keyStorePassword;

	@Value("${neo.agg.va.rbl.corpid}")
	private String rblCorporateId;

	@Value("${neo.keystore.path}")
	private Resource keyStoreFile;

	@Value("${neo.agg.ssl.alias}")
	private String sslAlias;

	@Value("${neo.agg.rbl.ssl.enable}")
	private Boolean rblSSLEnable;

	@Value("${neo.rbl.ecs.pvt.key.pwd}")
	private String pvtKeyPassword;

	@Value("${neo.rbl.ecs.pvt.key}")
	private Resource pvtKeyFile;

	@Value("${neo.rbl.ecs.alias}")
	private String ecsAlias;

	@Value("${neo.rbl.ecs.clientid}")
	private String ecsClientId;

	@Value("${neo.agg.rbl.userid}")
	private String panUserId;

	private String tlsVersion = "TLSv1.2";

	@Autowired
	private PartnerConfigService partnerConfigService;

	@Autowired
	private RblBioEkycRequestLogDao ekycRequestLogDao;

	@Autowired
	private NeoFundTransferDao fundTransferDao;
	
	@Autowired
    private DozerBeanMapper dozerBeanMapper;
	
	@Autowired
    private RblCommonService rblCommonService;
    
    @Autowired
	private ProductRegistrationDao productRegistrationDao;

	@Autowired
	private NeoExceptionBuilder exceptionBuilder;

	@Autowired
	private NeoBusinessCustomFieldService customFieldService;

	@Autowired
	private VirtualAccountController virtualAccountController;

	@Autowired
	private EcollectRequestDao ecollectRequestDao;

	@Autowired
	private CommonService commonService;

	private static final Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(RblEsbServiceImpl.class);

	private String postForEntityWithHeaders(String request, String url, HttpMethod method, HttpHeaders headers,
			Boolean logRequired) {

		StringBuilder urlParams = new StringBuilder();

		try {

			if (logRequired.equals(true)) {
				logger.info("Request for Server :: " + request);
			}

			urlParams.append("client_id=").append(rblUrlClientId).append("&client_secret=").append(rblUrlClientSecret);

			String auth = rblLDAPUser + ":" + rblLDAPPwd;
			byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.ISO_8859_1));

			StringBuilder urlBuilder = new StringBuilder(url + "?" + urlParams);

			headers.set("Authorization", "Basic" + " " + new String(encodedAuth));

			if (rblSSLEnable.equals(true))
				restTemplate = sslConfig(sslAlias, tlsVersion);

			HttpEntity<String> entity = new HttpEntity<>(request, headers);
			ResponseEntity<String> response = restTemplate.exchange(urlBuilder.toString(), method, entity, String.class,
					urlBuilder.toString());

			if (logRequired.equals(true)) {
				logger.info("Response from Server :: " + response.getBody());
			}

			return response.getBody();
		} catch (Exception e) {
			logger.info("Exception Occured :: " + e.getMessage());
		}
		return null;
	}

	private RestTemplate sslConfig(String alias, String tlsVersion) throws KeyStoreException, NoSuchAlgorithmException,
			CertificateException, IOException, KeyManagementException, UnrecoverableKeyException {
		Integer timeOut=0;
		KeyStore keyStore = KeyStore.getInstance("JKS");
		keyStore.load(keyStoreFile.getInputStream(), keyStorePassword.toCharArray());

		try {
			keyStoreFile.getInputStream().close();
		} catch (IOException e) {
			logger.info("IOException KeyStore File :: " + e.getMessage());
		}

		SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
				new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy())
						.loadKeyMaterial(keyStore, keyStorePassword.toCharArray(), (aliases, socket) -> alias).build(),
				new String[] { tlsVersion }, null, NoopHostnameVerifier.INSTANCE);

		NeoBusinessCustomField timeoutField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.API_TIMEOUT,
				"RBL");
		HttpClient httpClient;
		if (timeoutField != null && !timeoutField.getFieldValue().equals("0")) {
			timeOut = Integer.parseInt(timeoutField.getFieldValue());
			RequestConfig requestConfig = RequestConfig.custom()
					.setConnectionRequestTimeout(timeOut)
					.setConnectTimeout(timeOut)
					.setSocketTimeout(timeOut).build();

			httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory)
					.setDefaultRequestConfig(requestConfig)
					.build();
		} else {
			httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();
		}
		ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);

		return new RestTemplate(requestFactory);
	}

	private String OkycRequestPayload(RegistrationRequestV2Dto request) {
		String reqData = "";
		try {

			KeyStore keyStore = KeyStore.getInstance("JKS");
			keyStore.load(keyStoreFile.getInputStream(), keyStorePassword.toCharArray());

			try {
				keyStoreFile.getInputStream().close();
			} catch (IOException e) {
				logger.info("IOException KeyStore File :: " + e.getMessage());
			}

			java.security.cert.Certificate pbCert = keyStore.getCertificate(ecsAlias);

			byte[] certBytes = pbCert.getEncoded();

			String clientId = ecsClientId;
			String clientRequestId = request.getEKycRefNum();
			String requestType = request.getBioeKycReqType();
			String responseURL = request.getBioeKycUrlId();
			String serverToServerURL = null;
			String proxyServer = null;
			String proxyPort = null;
			String dataVaultUsername = request.getBioeKycUserId();
			String dataVaultPassword = request.getBioeKycPassword();

			InputStream gatewayCertificate = new ByteArrayInputStream(certBytes);
			InputStream signPfx = pvtKeyFile.getInputStream();

			reqData = ECSOkycWebApiClient.prepareCustomRequestData(clientId, clientRequestId, requestType,
					responseURL, serverToServerURL, proxyServer, proxyPort, gatewayCertificate,
					signPfx, pvtKeyPassword, null, null, null, null,
					dataVaultUsername, dataVaultPassword);

			logger.info("Request Id :: " + clientRequestId);
			logger.info("ECS Payload :: " + reqData);


		} catch (Exception ex) {
			logger.error("Exception :: ",ex);

		}
		return reqData;
	}

	public NeoResponse createVirtualAccountNumber(AddVARequestDto request) {

		List<AddVAResponseDto> coreResponse = new ArrayList<>();
		List<String> fundLoadMode = new ArrayList<>();
		String entityId;
		String fullName;
		Integer vaNameMaxLen;
		NeoNotificationDto notifyDto = new NeoNotificationDto();

		logger.info("Entering RBL VA Creation Service...");
		try {

			VirtualAccountCreationRegister vaData;

			// remote call
			/*
			 * if (request.getEntityId() == null || request.getEntityId().isEmpty()) {
			 * entityData = fetchEntities(""); } else { entityData =
			 * fetchEntities(request.getEntityId()); }
			 */

			if (!(request.getEntityId() == null || request.getEntityId().isEmpty())) {

				switch (request.getFundLoadMode()) {
				case "CORPORATE":
					fundLoadMode.add("7");
					break;
				case "USER":
					fundLoadMode.add("8");
					break;
				case "BOTH":
					fundLoadMode.add("7");
					fundLoadMode.add("8");
					break;
				default:
					fundLoadMode.add("0");
					break;
				}

				for (String fundLoadFlag : fundLoadMode) {

					vaNameMaxLen = 100;

					AddVAResponseDto entityStatus = new AddVAResponseDto();

					String referenceNum = TxnIdGenerator.getReferenceNumber();
					Boolean corporateCredit = fundLoadFlag.equals("7") ? Boolean.TRUE : Boolean.FALSE;

					String vaSuffix = "";
					if (request.getKitNo() == null || request.getKitNo().isEmpty())
						vaSuffix = request.getEntityId();
					else
						vaSuffix = request.getKitNo();

					entityId = (fundLoadFlag.equals("0") ? "8" : fundLoadFlag) + vaSuffix;

					if (request.getLastName() != null && !request.getLastName().isEmpty()) {
						fullName = request.getFirstName().trim() + " " + request.getLastName().trim();
					} else {
						fullName = request.getFirstName().trim();
					}
					fullName = fullName.replaceAll("[-,.!'~#@&*+%|_^]", "");
					fullName = fullName.replaceAll("\\s+", " ");
					fullName = fullName.trim();

					if (fullName.length() - fullName.replaceAll(" ", "").length() > 5) {
						String[] newArray = Arrays.copyOfRange(fullName.split(" "), 0, 6);
						fullName = String.join(" ", newArray);
					}

					vaNameMaxLen = vaNameMaxLen < fullName.length() ? vaNameMaxLen : fullName.length();
					fullName = fullName.substring(0, vaNameMaxLen);

					vaData = fetchVADetails(request.getEntityId(), corporateCredit, "000" + request.getCorpClientId() + entityId);

					if (vaData == null) {
						vaData = new VirtualAccountCreationRegister();
						vaData.setCorpId(request.getCorpClientId());
						vaData.setEntityId(request.getEntityId());
						vaData.setCorporateCredit(corporateCredit);
						vaData.setRetryCount(0);
						vaData.setCustomerName(fullName);
						vaData.setKitNo((request.getKitNo() == null || request.getKitNo().isEmpty()) ? null
								: request.getKitNo());
						vaData = updateVirtualAccount(vaData);
					} else if (vaData != null && vaData.getResult() != null
							&& vaData.getResult().equalsIgnoreCase("Success")) {
						logger.info(entityId + " :: Virtual Account already exists...");
						entityStatus.setEntityId(request.getEntityId());
						entityStatus.setFullName(vaData.getCustomerName());
						entityStatus.setVaNum(vaData.getVirtualAccNum());
						entityStatus.setFundLoadMode(fundLoadFlag.equals("7") ? "CORPORATE" : "USER");
						entityStatus.setStatus(vaData.getResult());

						coreResponse.add(entityStatus);
						continue;
					}

					VARequestHeader vaCreateHdr = VARequestHeader.builder().TranID(referenceNum).CorpID(rblCorporateId)
							.ApproverID("").CheckerID("").MakerID("").build();

					VARequestBody vaCreateBody = VARequestBody.builder().AccountNo(request.getCorpAccNum())
							.ClientId(request.getCorpClientId()).VaSerialNo(entityId).VaBeneficiary(fullName).build();

					RblVACreateRequestDto vaCreateData = RblVACreateRequestDto.builder().header(vaCreateHdr)
							.body(vaCreateBody).build();

					String vaCreateReq = new ObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, true)
							.writeValueAsString(vaCreateData);

					// CALL RBL Link
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					String resp = postForEntityWithHeaders(vaCreateReq, rblVACreateUrl, HttpMethod.POST, headers, true);

					RblVACreateResponseDto responseDto;
					try {
						responseDto = new ObjectMapper()
								.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
								.enable(DeserializationFeature.UNWRAP_ROOT_VALUE)
								.readValue(resp, RblVACreateResponseDto.class);
					} catch (Exception e) {
						return new NeoResponse(null,
								new NeoException(null, e.getMessage(), null, null, e.getLocalizedMessage()), null);
					}

					if (responseDto.getHeader().getStatus().equalsIgnoreCase("Success")) {
						vaData.setCifNum(responseDto.getDetails().getCIF());
						vaData.setVirtualAccNum(responseDto.getDetails().getFullVAnumber());
						vaData.setKitNo(request.getKitNo());
						vaData.setCustomerName(fullName);
						vaData.setRetryCount(0);
					}

					vaData.setResult(responseDto.getHeader().getStatus()); // Failure,Success,FAILED
					updateVirtualAccount(vaData);

					notifyDto.setBusiness(TenantContextHolder.getNeoTenant());
					notifyDto.setEntityId(request.getEntityId());
					notifyDto.setTxnStatus(responseDto.getHeader().getStatus());
					if (responseDto.getHeader().getStatus().equalsIgnoreCase("Success")) {
						notifyDto.setDescription("Created Virtual Account");
						notifyDto.setTransactionType("va_creation");
						notifyDto.setAccountNum(responseDto.getDetails().getFullVAnumber());
					} else {
						notifyDto.setDescription("VA Creation Failure :: " + responseDto.getHeader().getStatusReason()
								+ " :: Will be Retried");
						notifyDto.setTransactionType("va_creation");
					}
					commonService.notifyRBLVAStatus(notifyDto);

					entityStatus.setEntityId(request.getEntityId());
					entityStatus.setFullName(fullName);
					entityStatus.setVaNum(vaData.getVirtualAccNum());
					entityStatus.setFundLoadMode(fundLoadFlag.equals("7") ? "CORPORATE" : "USER");
					entityStatus.setStatus(responseDto.getHeader().getStatus());
					entityStatus.setStatusReason(responseDto.getHeader().getStatusReason());

					coreResponse.add(entityStatus);

					logger.info("Response to Calling Func ::: " + coreResponse.toString());
				}
				return new NeoResponse(coreResponse, null, null);
			} else {
				return new NeoResponse("No Entities given for Virtual A/c Creation!!...EntityId is Mandatory!!", null,
						null);
			}
		} catch (Exception e) {
			return new NeoResponse(null, new NeoException(null, e.getMessage(), null, null, e.getLocalizedMessage()),
					null);
		}
	}

	@SuppressWarnings("unused")
	private BusinessEntityListDto fetchEntities(String entityId) throws JsonProcessingException {

		BusinessEntity dto = new BusinessEntity();
		dto.setEntityId(entityId);

		NeoResponse response = rmService.fetchData(dto, localUrl + "/neo-lego/common/virtual-account/fetchEntities",
				TenantContextHolder.getNeoTenant(), HttpMethod.POST);

		String responseStr = new ObjectMapper().writeValueAsString(response.getResult());
		return new ObjectMapper().readValue(responseStr, BusinessEntityListDto.class);
	}

	private VirtualAccountCreationRegister fetchVADetails(String entityId, Boolean fundLoadFlag, String virtualAccNum) {

		VirtualAccountCreationRegister dto = new VirtualAccountCreationRegister();
		VirtualAccountCreationRegister resp;

		dto.setEntityId(entityId);
		dto.setCorporateCredit(fundLoadFlag);
		dto.setVirtualAccNum(virtualAccNum);

		NeoResponse response = rmService.fetchData(dto, localUrl + "/neo-lego/common/virtual-account/fetchVADetails",
				TenantContextHolder.getNeoTenant(), HttpMethod.POST);

		resp = new ObjectMapper().convertValue(response.getResult(), VirtualAccountCreationRegister.class);

		return resp;
	}

	private VirtualAccountCreationRegister updateVirtualAccount(VirtualAccountCreationRegister request) {

		NeoResponse response = rmService.fetchData(request, localUrl + "/neo-lego/common/virtual-account/updateVAData",
				TenantContextHolder.getNeoTenant(), HttpMethod.POST);

		VirtualAccountCreationRegister resp = new ObjectMapper().convertValue(response.getResult(),
				VirtualAccountCreationRegister.class);
		return resp;
	}

	public String vaFailureRetry() throws NeoException {

		logger.info("In RBL vaFailureRetry...");

		NeoNotificationDto notifyDto = new NeoNotificationDto();
		String responseString = "";
		try {
			List<VirtualAccountCreationRegister> resp;

			NeoBusinessCustomField vaRetryTenantsCusField = customFieldService
					.findByFieldNameAndTenant(NeoCustomFieldConstants.RBL_VA_RETRY_TENANTS, "RBL");

			logger.info("Tenants on VA Retry :: " + vaRetryTenantsCusField.getFieldValue());

			String[] vaRetryFields = vaRetryTenantsCusField.getFieldValue().split("\\|");

			for (String vaRetryField : vaRetryFields) {
				int success = 0;

				String[] vaFields = vaRetryField.split(":");

				String tenant = vaFields[0];
				String corporateAccNum = vaFields[1];

				TenantContextHolder.setNeoTenant(tenant);

				logger.info("Currently Running Tenant :: " + tenant);

				VirtualAccountCreationRegister request = new VirtualAccountCreationRegister();

				request.setRetryCount(Integer.parseInt(vaFields[2]));

				NeoResponse response = rmService.fetchData(request,
						localUrl + "/neo-lego/common/virtual-account/fetchFailedVADetails", tenant, HttpMethod.POST);

				if (response.getResult() == null) {
					responseString = responseString.isEmpty() ? responseString : responseString + " ::: ";
					responseString = responseString + "Tenant - " + tenant + " :: NULL";
					continue;
				}

				resp = new ObjectMapper().configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true)
						.convertValue(response.getResult(), new TypeReference<List<VirtualAccountCreationRegister>>() {
						});

				for (VirtualAccountCreationRegister vaData : resp) {

					logger.info(vaData.getEntityId() + " ::: " + vaData.getKitNo() + " ::: "
							+ (vaData.getCorporateCredit().equals(Boolean.TRUE) ? "CORPORATE" : "USER"));

					String referenceNum = TxnIdGenerator.getReferenceNumber();

					String entityId = (vaData.getCorporateCredit().equals(Boolean.TRUE) ? "7" : "8")
							+ (vaData.getKitNo() != null ? vaData.getKitNo() : vaData.getEntityId());

					VARequestHeader vaCreateHdr = VARequestHeader.builder().TranID(referenceNum).CorpID(rblCorporateId)
							.ApproverID("").CheckerID("").MakerID("").build();

					VARequestBody vaCreateBody = VARequestBody.builder().AccountNo(corporateAccNum)
							.ClientId(vaData.getCorpId()).VaSerialNo(entityId).VaBeneficiary(vaData.getCustomerName())
							.build();

					RblVACreateRequestDto vaCreateData = RblVACreateRequestDto.builder().header(vaCreateHdr)
							.body(vaCreateBody).build();

					String vaCreateReq = new ObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, true)
							.writeValueAsString(vaCreateData);

					// CALL RBL Link
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					String responseStr = postForEntityWithHeaders(vaCreateReq, rblVACreateUrl, HttpMethod.POST, headers,
							true);

					RblVACreateResponseDto responseDto = null;
					if (responseStr != null) {
						try {
							responseDto = new ObjectMapper()
									.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
									.enable(DeserializationFeature.UNWRAP_ROOT_VALUE)
									.readValue(responseStr, RblVACreateResponseDto.class);
						} catch (Exception e) {
							logger.error("Exception Occured :: " + e.getMessage());
						}
					} else {
						throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR, "VA Retry Failed",
								null, Locale.US, null, null, "VA Retry");
					}

					if (responseDto.getHeader() != null
							&& responseDto.getHeader().getStatus().equalsIgnoreCase("Success")) {
						vaData.setCifNum(responseDto.getDetails().getCIF());
						vaData.setVirtualAccNum(responseDto.getDetails().getFullVAnumber());
						success++;
					}

					vaData.setRetryCount(vaData.getRetryCount() + 1);
					vaData.setResult(responseDto.getHeader().getStatus()); // Failure,Success,FAILED
					updateVirtualAccount(vaData);

					notifyDto.setBusiness(tenant);
					notifyDto.setEntityId(vaData.getEntityId());
					notifyDto.setTxnStatus(responseDto.getHeader().getStatus());
					if (responseDto.getHeader() != null
							&& responseDto.getHeader().getStatus().equalsIgnoreCase("Success")) {
						notifyDto.setDescription("Created Virtual Account");
						notifyDto.setTransactionType("va_creation");
						notifyDto.setAccountNum(responseDto.getDetails().getFullVAnumber());
						commonService.notifyRBLVAStatus(notifyDto);
					} else if (vaData.getRetryCount() == Integer.parseInt(vaFields[2])) {
						notifyDto.setDescription("VA Creation Failure :: " + responseDto.getHeader().getStatusReason());
						notifyDto.setTransactionType("va_creation");
						commonService.notifyRBLVAStatus(notifyDto);
					}
				}
				logger.info("Tenant - " + tenant + " ::: Total - " + resp.size() + " :: Success - " + success
						+ " :: Failure - " + (resp.size() - success));
				responseString = responseString.isEmpty() ? responseString : responseString + " ::: ";
				responseString = responseString + "Tenant - " + tenant + " :: Total - " + resp.size() + " :: Success - "
						+ success + " :: Failure - " + (resp.size() - success);
			}
		} catch (Exception e) {
			logger.error("Exception Occured :: " + e.getMessage());
			logger.error("Exception :: " + e);
		}
		return responseString.isEmpty() ? "vaFailureRetry Failed" : responseString;
	}

	private List<String> fetchYapEcollectRequestData(String tenant, EcollectRequest request) {

		NeoResponse response = rmService.fetchData(request,
				localUrl + "/neo-lego/common/virtual-account/fetchYapEcollectRequestData", tenant, HttpMethod.POST);

		List<String> resp = new ObjectMapper().configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true)
				.convertValue(response.getResult(), new TypeReference<List<String>>() {
				});

		return resp;
	}

	@Override
	public KYCBioResponseDto bioEKycOneTimeToken(RegistrationRequestV2Dto request, String eKycRefnum)
			throws NeoException {

		logger.info("RBL OTT API call...");
		KYCBioResponseDto response = new KYCBioResponseDto();
		StringWriter sw = new StringWriter();

		String address = request.getAddressInfo().get(0).getAddress1() + " "
				+ request.getAddressInfo().get(0).getAddress2() + " " + request.getAddressInfo().get(0).getAddress3();
		address = address.substring(0, (Math.min(address.length(), 100)));
		String name = request.getFirstName() + " " + request.getMiddleName() + " " + request.getLastName();
		Date dob = request.getDateInfo().get(0).getDate();
		Date date = new Date();
		String dateTime = DateUtilityFunction.dateInFormat(date, "MM-dd-yyyy HH:MM:SS:MSS");
		String dobFormatted = DateUtilityFunction.dateInFormat(dob, "yyyy-MM-dd");

		try {
			OneTimeTokenRequest ottrequest = OneTimeTokenRequest.builder().address(address)
					.bcagentid(request.getBioeKycBCAgentId()).bseg("BAAS")
					.city(request.getAddressInfo().get(0).getCity()).cpuniquerefno(eKycRefnum).datetime(dateTime)
					.dob(dobFormatted).email(request.getCommunicationInfo().get(0).getEmailId())
					.mobile(request.getCommunicationInfo().get(0).getContactNo()).name(name).pan("")
					.password(request.getBioeKycPassword()).pincode(request.getAddressInfo().get(0).getPincode())
					.req("1").state(request.getAddressInfo().get(0).getState()).type("I")
					.userid(request.getBioeKycUserId()).field13(request.getBioeKycUrlId()).field14("").field15("")
					.field16("").build();

			sw.append(new ObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, true)
					.writeValueAsString(ottrequest));
			String ottRequestString = sw.toString();

			logger.info("Actual Payload ::" + ottRequestString);

			MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

			PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
					ProductConstants.EKYC_ONETIMETOKEN);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			String responseBody = postForEntityWithHeaders(sw.toString(), serviceModel.getServiceEndpoint(),
					HttpMethod.POST, headers, Boolean.TRUE);

			try {
				OneTimeTokenResponse responseOutDto = new ObjectMapper()
						.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true)
						.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
						.readValue(responseBody, OneTimeTokenResponse.class);
				String responseStr = responseOutDto.toString();

				logger.info("Response Received ::" + responseStr);

				String redirectUrl = responseOutDto.getUrl() + "&cpuniquerefno=" + eKycRefnum;

				response.setEntityId(request.getEntityId());
				response.setStatus(responseOutDto.getStatus());
				response.setKycRedirectUrl(redirectUrl);

				RblBioEkycRequestLog ekycRequestLog = new RblBioEkycRequestLog();
				ekycRequestLog.setCpuniquerefno(eKycRefnum);
				ekycRequestLogDao.save(ekycRequestLog);

			} catch (Exception ex) {

				ErrorResponse responseOutDto = new ObjectMapper()
						.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true)
						.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
						.readValue(responseBody, ErrorResponse.class);

				String responseStr = responseOutDto.toString();

				logger.info("Response Received ::" + responseStr);
				response.setEntityId(request.getEntityId());
				response.setStatus(responseOutDto.getStatus());
				response.setDescription(responseOutDto.getDescription());
			}

			return response;

		} catch (Exception e) {
			logger.info("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
					"System Error" , null, Locale.US, null, null,ProductConstants.EKYC_ONETIMETOKEN);
		}

	}

	@Override
	public KYCBioResponseDto eKycBioValidate(KYCBioRequestDto request) throws NeoException {

		logger.info("Rbl Biometric Data Validation...");
		KYCBioResponseDto response = new KYCBioResponseDto();
		StringWriter sw = new StringWriter();
		String successFlag = "1";
		String rblUniqueRefno = "";
		String rurnStatus = "";
		String responseBody = "";
		RblBioEkycRequestLog ekycRequestLog = ekycRequestLogDao.findByCpuniquerefno(request.getEkycRefnum());
		try {
			MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());
			PartnerServiceConfiguration serviceModel;
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			response.setEntityId(request.getEntityId());

			if (ekycRequestLog.getEkycstatus().equals("1")) {
				// call demographic api
				KYCBioRequestDto ekycRequryRequest = new KYCBioRequestDto();
				ekycRequryRequest.setBioeKycUserId(request.getBioeKycUserId());
				ekycRequryRequest.setBioeKycPassword(request.getBioeKycPassword());
				ekycRequryRequest.setEkycRefnum(ekycRequestLog.getBioekycrefno());
				ekycRequryRequest.setEntityId(request.getEntityId());
				
				request.getRegistrationRequestDtoV2().setEKycRefNum(request.getEkycRefnum());
				ekycRequryRequest.setRegistrationRequestDtoV2(request.getRegistrationRequestDtoV2());

				try {
					response = getDemographicData(ekycRequryRequest, RblPropertyConstants.KYC_TYPE_BIO);
					
					if(response == null) {
						throw new NeoException("Fail", "Fail", null, null, "Error fetching Demographic Data!!");
					}
				} catch (Exception e) {
					logger.info("Exception :: " + e.getMessage());
					if(response==null){
						response= new KYCBioResponseDto();
					}
					response.setStatus("0");
					response.setDescription("Error fetching Demographic Data!!");
				}

				return response;
			}

			if (ekycRequestLog != null && ekycRequestLog.getBioekycattempts() < 3) {
				if (ekycRequestLog.getBioekycattempts() > 0) {

					rblUniqueRefno = ekycRequestLog.getRbluniquerefno();
					rurnStatus = "1";
					ekycRequestLog.setBioekycattempts(ekycRequestLog.getBioekycattempts() + 1);
					ekycRequestLogDao.save(ekycRequestLog);

				} else {

					// ekycRequestLog = new RblBioEkycRequestLog();

					RurnQueryRequest rurnrequest = RurnQueryRequest.builder().bcagentid(request.getBioeKycBCAgentId())
							.cpuniquerefno(request.getEkycRefnum()).password(request.getBioeKycPassword())
							.userid(request.getBioeKycUserId()).build();

					sw.append(new ObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, true)
							.writeValueAsString(rurnrequest));
					String rurnRequestString = sw.toString();

					logger.info("Payload for RURN Query::" + rurnRequestString);

					serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
							ProductConstants.EKYC_RURN);

					responseBody = postForEntityWithHeaders(sw.toString(), serviceModel.getServiceEndpoint(),
							HttpMethod.POST, headers, Boolean.TRUE);

					RurnQueryResponse rurnResponseOutDto;
					try {

						rurnResponseOutDto = new ObjectMapper()
								.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true)
								.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
								.readValue(responseBody, RurnQueryResponse.class);

						rurnStatus = rurnResponseOutDto.getStatus();
						rblUniqueRefno = rurnResponseOutDto.getRbluniquerefno();
						ekycRequestLog.setBioekycattempts(1);
						ekycRequestLog.setRbluniquerefno(rblUniqueRefno);
						ekycRequestLogDao.save(ekycRequestLog);

					} catch (Exception e) {
						ErrorResponse responseOutDto = new ObjectMapper()
								.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true)
								.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
								.readValue(responseBody, ErrorResponse.class);
						rurnStatus = "0";
						response.setStatus("0");
						response.setDescription(responseOutDto.getDescription());
					}

					logger.info("Response Received for RURN Query::" + responseBody);
				}
			} else {

				response.setStatus("0");
				response.setDescription("Wrong Request or Number of attempts for Biometric Validation exceeded!!");
				return response;
			}

			if (rurnStatus.equals(successFlag) && rblUniqueRefno != null) {
				int maxVal = 19;
				String bioRefNumber = request.getBioeKycRefPrefix() + TxnIdGenerator.getReferenceNumber();
				maxVal = Math.min(bioRefNumber.length(), maxVal);
				bioRefNumber = bioRefNumber.substring(0, maxVal);

				ekycRequestLog.setBioekycrefno(bioRefNumber);

				BioDataValidationRequest bioValidateRequest = BioDataValidationRequest.builder()
						.userid(request.getBioeKycUserId()).password(request.getBioeKycPassword())
						.bcagentid(request.getBioeKycBCAgentId()).rbluniquerefno(rblUniqueRefno)
						.uniquerefno(bioRefNumber).ekyctype(1).encryptedpid(request.getDataValue())
						.encryptedhmac(request.getHmac()).sessionkeyvalue(request.getSkey())
						.certificateidentifier(request.getCi()).registereddeviceserviceid(request.getRdsId())
						.registereddeviceserviceversion(request.getRdsVer())
						.registereddeviceproviderid(request.getDpId()).registereddevicecode(request.getDc())
						.registereddevicemodelid(request.getMi()).registereddevicepublickey(request.getMc()).build();

				sw = new StringWriter();

				sw.append(new ObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, true)
						.writeValueAsString(bioValidateRequest));
				String bioRequestString = sw.toString();

				logger.info("Payload for Biometric Validation :: " + bioRequestString);

				serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
						ProductConstants.EKYC_VALIDATEBIO);

				responseBody = postForEntityWithHeaders(sw.toString(), serviceModel.getServiceEndpoint(),
						HttpMethod.POST, headers, Boolean.TRUE);

				BioDataValidationResponse bioValidateResponseDto = new ObjectMapper()
						.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true)
						.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
						.readValue(responseBody, BioDataValidationResponse.class);

				logger.info("Response Received for Biometric Validation :: " + responseBody);

				if (bioValidateResponseDto.getStatus().equals(successFlag)) {

					ekycRequestLog.setEkycstatus("1");
					ekycRequestLogDao.save(ekycRequestLog); // Bio validation completed with success

					KYCBioRequestDto ekycRequryRequest = new KYCBioRequestDto();
					ekycRequryRequest.setBioeKycUserId(request.getBioeKycUserId());
					ekycRequryRequest.setBioeKycPassword(request.getBioeKycPassword());
					ekycRequryRequest.setEkycRefnum(bioRefNumber);
					ekycRequryRequest.setEntityId(request.getEntityId());
					
					request.getRegistrationRequestDtoV2().setEKycRefNum(request.getEkycRefnum());
					ekycRequryRequest.setRegistrationRequestDtoV2(request.getRegistrationRequestDtoV2());
					
					response = getDemographicData(ekycRequryRequest, RblPropertyConstants.KYC_TYPE_BIO);
					
					if(!(response != null)) {
						throw new NeoException("Fail", "Fail", null, null, "Error fetching Demographic Data!!");
					}
				} else {
					response.setStatus("0");
					response.setResponsecode(bioValidateResponseDto.getResponsecode());
					response.setResponsemessage(bioValidateResponseDto.getResponsemessage());
					response.setDescription("UIDAI Response :: " + bioValidateResponseDto.getResponsemessage()
							+ " :: Error Msg :::" + bioValidateResponseDto.getAuthxmlerrormessage());

					ekycRequestLog.setEkycstatus("0");
					ekycRequestLogDao.save(ekycRequestLog);

					return response;
				}
			}
		} catch (Exception e) {
			ekycRequestLog.setEkycstatus("0");
			ekycRequestLogDao.save(ekycRequestLog);
			logger.info("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
					"Ekyc Biometric Validation Failed" , null, Locale.US, null, null,ProductConstants.EKYC_VALIDATEBIO);
		}

		return response;
	}

	@Override
	public KYCBioResponseDto getDemographicData(KYCBioRequestDto request, String kycType) throws NeoException {

		logger.info("RBL Demographic Data Fetch API Call...");

		KYCBioResponseDto response;
		StringWriter sw;
		GenderType gender;

		try {

			MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

			EkycRequeryRequest.RequestHeader requestHeader = EkycRequeryRequest.RequestHeader.builder()
					.username(request.getBioeKycUserId())
					.password(request.getBioeKycPassword())
					.build();

			EkycRequeryRequest ekycRequryRequest = EkycRequeryRequest.builder()
					.header(requestHeader)
					.uniquerequestid(request.getEkycRefnum())
					.build();

			sw = new StringWriter();

			sw.append(new ObjectMapper()
					.configure(SerializationFeature.WRAP_ROOT_VALUE, true)
					.writeValueAsString(ekycRequryRequest));

			String reQueryRequestString = sw.toString();

			logger.info("Payload for EKYC Requery :: " + reQueryRequestString);

			PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
					ProductConstants.EKYC_DEMOGRAPHICDATA);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			EkycRequeryResponse ekycRequeryResponse = null;
			String responseBody = postForEntityWithHeaders(sw.toString(), serviceModel.getServiceEndpoint(), HttpMethod.POST,
					headers, Boolean.TRUE);
			ObjectMapper mapper = new ObjectMapper();
			try {
				ekycRequeryResponse = new ObjectMapper()
						.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true)
						.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
						.readValue(responseBody, EkycRequeryResponse.class);
			} catch (Exception ex) {
				Map<String, String> excpRespMap = mapper.readValue(responseBody, Map.class);

				Map<String, String> respBodyMap = mapper.readValue(
						mapper.writeValueAsString(excpRespMap.get("errorres")), Map.class);

				if (respBodyMap.get("description").contains("OKYC Request id not exist")) {
					return null;
				} else {
					logger.info("Exception Occured :: ", ex);
					throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
							"Fetching Demographic Data Failed" , null, Locale.US, null, null,ProductConstants.EKYC_DEMOGRAPHICDATA);
				}
			}
			response = dozerBeanMapper.map(ekycRequeryResponse, KYCBioResponseDto.class);

			AddressDto addressDto = new AddressDto();
			addressDto.setAddressCategory(AddressCategory.PERMANENT);
			addressDto.setAddress1(ekycRequeryResponse.getBuilding());
			addressDto.setAddress2(ekycRequeryResponse.getStreet());
			addressDto.setAddress3(ekycRequeryResponse.getArea());
			addressDto.setCity(ekycRequeryResponse.getCity());
			addressDto.setCountry(ekycRequeryResponse.getCountry());
			addressDto.setPincode(ekycRequeryResponse.getPin());
			addressDto.setDistrict(ekycRequeryResponse.getDistrict());
			addressDto.setState(ekycRequeryResponse.getState());

			response.setAddressDto(addressDto);
			response.setAuthCode(ekycRequeryResponse.getAuthenticationcode());
			response.setKycRefNo(ekycRequeryResponse.getUniquerequestid());
			response.setEntityId(request.getEntityId());
			response.setCareOf(ekycRequeryResponse.getCareOf());
			response.setTitle(ekycRequeryResponse.getGender().equals("M") ? "Mr" : "Ms");
			
			// call product api
			NeoBusinessCustomField neoCustomField = customFieldService.findByFieldNameAndTenant(
					NeoCustomFieldConstants.RBL_PRODUCT_REGISTER, TenantContextHolder.getNeoTenant());
			
			if(neoCustomField != null && neoCustomField.getFieldValue().equalsIgnoreCase("Y")) {

	            RegistrationRequestV2Dto productRegRequest=dozerBeanMapper.map(request.getRegistrationRequestDtoV2(),RegistrationRequestV2Dto.class);
	            
	            productRegRequest.setBioeKycUserId(request.getBioeKycUserId());
	            productRegRequest.setBioeKycPassword(request.getBioeKycPassword());
	            
	            productRegRequest.setFirstName(ekycRequeryResponse.getName());
	            productRegRequest.setTitle(ekycRequeryResponse.getGender().equals("M") ? "Mr" : "Ms");
				productRegRequest.setEKycRefNum(request.getEkycRefnum());

	            List<SpecialDatesDto> dateInfo = new ArrayList<SpecialDatesDto>();
	            SpecialDatesDto dateDto = new SpecialDatesDto();
	            dateDto.setDate(new SimpleDateFormat("dd-MM-yyyy").parse(ekycRequeryResponse.getDob()));
	            dateInfo.add(dateDto);
	            productRegRequest.setDateInfo(dateInfo);
	            
	            productRegRequest.setGender(GenderType.valueOf(ekycRequeryResponse.getGender()));
	            
	            List<AddressDto> addressInfo = new ArrayList<AddressDto>();
	            addressInfo.add(addressDto);
	            productRegRequest.setAddressInfo(addressInfo);
	            
	            RegistrationResponseDto productResponeDto= productRegistration(productRegRequest, kycType);
	            
	            if(productResponeDto != null) {
	            	if(productResponeDto.getStatus().equals("0")) {
	            		logger.info("Product Registration Failed :: " + productResponeDto.getDescription());
	            		throw new NeoException("Failed", productResponeDto.getDescription(), null, null, 
	            				"Product Registration Failed");
	            	}
	            	response.setDescription(productResponeDto.getDescription());
	            	response.setStatus(productResponeDto.getStatus());
	            	response.setCustId(productResponeDto.getCustomerId());
	            } else {
	            	logger.info("Product Registration Failed");
	    			throw new NeoException("Service Unavailable", "Service Unavailable", null, null,
	    					"Product Registration Failed");
	            }
	             //
			}
		} catch(NeoException neo) {
			throw neo;
		} catch (Exception e) {
			logger.info("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
					"Fetching Demographic Data Failed" , null, Locale.US, null, null,ProductConstants.EKYC_DEMOGRAPHICDATA);

		}
		return response;
	}

	@Override
	public KYCBioResponseDto aadharXmlInvoke(RegistrationRequestV2Dto request) throws NeoException {

		logger.info("RBL OKYC ECS API Call...");

		KYCBioResponseDto response = new KYCBioResponseDto();
		String ecsPayload = "";

		try {
			MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

			PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
					ProductConstants.EKYC_AADHARXML);

			byte[] encodedbytes = OkycRequestPayload(request).getBytes();
			ecsPayload = Base64.encodeBase64String(encodedbytes);
			if (!StringUtils.isEmpty(ecsPayload)) {
				response.setEntityId(request.getEntityId());
				response.setStatus("1");
				response.setEncryptedData(ecsPayload);
				response.setKycRedirectUrl(serviceModel.getServiceEndpoint());
			} else {
				response.setEntityId(request.getEntityId());
				response.setStatus("0");
				response.setDescription("Error Creating Encrypted Payload !!");
			}
			return response;

		} catch (Exception e) {
			logger.info("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
					"Generation of Aadhar XML Payload failed" , null, Locale.US, null, null,ProductConstants.EKYC_AADHARXML);
		}

	}

	@Override
	public KYCBioResponseDto validateAadhaarXml(AadhaarOtpRequest request) throws NeoException {
		KYCBioResponseDto reQueryResponse;
		try {
			KYCBioRequestDto ekycRequryRequest = new KYCBioRequestDto();
			ekycRequryRequest.setBioeKycUserId(request.getBioeKycUserId());
			ekycRequryRequest.setBioeKycPassword(request.getBioeKycPassword());
			ekycRequryRequest.setEkycRefnum(request.getTxnRef());
			ekycRequryRequest.setEntityId(request.getEntityId());
			
			request.getRegistrationRequestDtoV2().setEKycRefNum(request.getTxnRef());
			ekycRequryRequest.setRegistrationRequestDtoV2(request.getRegistrationRequestDtoV2());
			
			reQueryResponse = getDemographicData(ekycRequryRequest, RblPropertyConstants.KYC_TYPE_XML);
			
			if(!(reQueryResponse != null)) {
				throw new NeoException("Fail", "Fail", null, null, "Fetching Demographic Data Failed");
			}
		} catch (Exception e) {
			logger.info("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
					"Fetching Demographic Data Failed" , null, Locale.US, null, null,ProductConstants.EKYC_DEMOGRAPHICDATA);

		}
		return reQueryResponse;
	}
	
	public RegistrationResponseDto productRegistration(RegistrationRequestV2Dto request, String kycType) throws NeoException {

		logger.info("RBL :: Product API Call for :: "+request.getEntityId());

        RegistrationResponseDto response = new RegistrationResponseDto();
        StringWriter sw = new StringWriter();
        String responseBody = "";
        String requestString = "";
        Boolean isUpdate = Boolean.FALSE;
        Boolean isLog = Boolean.FALSE;
        String tenant= TenantContextHolder.getNeoTenant();

		ProductRegistration register = productRegistrationDao.findByEntityIdAndMobilenumberAndIsLogAndTenant(request.getEntityId(),
				rblCommonService.getMobile(request.getCommunicationInfo().get(0).getContactNo()), isLog, tenant);

		if (register != null && register.getStatus().equals("1")
				&& register.getDescription().equalsIgnoreCase("SUCCESS")) {
			logger.info("Customer already registered.");
			response.setDescription(register.getDescription());
			response.setCustomerId(register.getCustomerId());
			response.setEntityId(register.getEntityId());
			response.setStatus(register.getStatus());
			return response;
		} else if (register != null) {
			isUpdate = true;
		}
		ProductRegistration registerCheck = productRegistrationDao.findByMobilenumberAndStatusAndTenant(
				rblCommonService.getMobile(request.getCommunicationInfo().get(0).getContactNo()), "1", tenant);
        
		if(registerCheck != null && registerCheck.getDescription().equalsIgnoreCase("SUCCESS")
				&& !request.getEntityId().equals(registerCheck.getEntityId())) {
			logger.info("Mobile already registered with bank.");
			rblCommonService.buildCustomerKYCOnboardingRequest(request, kycType, registerCheck, Boolean.FALSE, Boolean.TRUE);
			response.setDescription("Mobile Number Already Registered");
			response.setStatus("0");
			return response;
		}

        try {
        	MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());
            PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
            		ProductConstants.CIF_CREATION);
        	
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_XML);
            
            CustomerKYCOnboardingRequest cifRequest = rblCommonService.buildCustomerKYCOnboardingRequest(request, kycType, 
            		register, isUpdate, isLog);
            
            JAXBContext jaxbContext = JAXBContext.newInstance(CustomerKYCOnboardingRequest.class);
            Marshaller m = jaxbContext.createMarshaller();
            m.marshal(cifRequest, sw);
            
            requestString = sw.toString();

            logger.info("Payload for Customer On-boarding :: " + requestString);

            responseBody = postForEntityWithHeaders(requestString, serviceModel.getServiceEndpoint(),
					HttpMethod.POST, headers, Boolean.TRUE);
            
            logger.info("Response Received for Customer On-boarding ::" + responseBody);
            
            CustomerKYCOnboardingResponse responseOutDto = null;
            ErrorResponseXML errorResponseOutDto = null;
            ProductRegistration registration = productRegistrationDao.findByEntityIdAndMobilenumberAndTenant(request.getEntityId(),
            		cifRequest.getMobilenumber(),tenant);
            if(responseBody != null) {
	            try {
		            JAXBContext responseOutJAXB = JAXBContext.newInstance(CustomerKYCOnboardingResponse.class);
		            Unmarshaller responseOutUnmarshaller = responseOutJAXB.createUnmarshaller();
		            
		            StringReader stringReader = new StringReader(responseBody);
		            responseOutDto = (CustomerKYCOnboardingResponse) 
		            		responseOutUnmarshaller.unmarshal(stringReader);
	            } catch(Exception ex) {
	            	JAXBContext responseOutJAXB = JAXBContext.newInstance(ErrorResponseXML.class);
	                Unmarshaller responseOutUnmarshaller = responseOutJAXB.createUnmarshaller();
	
	                StringReader stringReader = new StringReader(responseBody);
	                errorResponseOutDto = (ErrorResponseXML) responseOutUnmarshaller.unmarshal(stringReader);
	                if(!registration.getStatus().equals("1") && errorResponseOutDto != null) {
		                registration.setDescription(errorResponseOutDto.getDescription());
		                registration.setStatus(errorResponseOutDto.getStatus());
		                productRegistrationDao.save(registration);
	                }
	            }
            
	            if (responseOutDto != null && responseOutDto.getStatus().equals("1") && 
	            		responseOutDto.getDescription().equalsIgnoreCase("SUCCESS")) {
	                logger.info("Customer Creation Successful at Bank");
	                response.setDescription(responseOutDto.getDescription());
	                response.setCustomerId(responseOutDto.getCustomerid());
	                response.setEntityId(request.getEntityId());
	                response.setStatus(responseOutDto.getStatus());
	                
	                registration.setDescription(responseOutDto.getDescription());
	                registration.setCustomerId(responseOutDto.getCustomerid());
	                registration.setStatus(responseOutDto.getStatus());
	                productRegistrationDao.save(registration);
	            } else if(errorResponseOutDto != null) {
	            	response.setDescription(errorResponseOutDto.getDescription());
	            	response.setStatus(errorResponseOutDto.getStatus());
	            } else {
	            	throw new NeoException("Failed", "Fail", null, null, "Product Registration Failed");
	            }
            } else {
            	throw new NeoException("Failed", "Fail", null, null, "Product Registration Failed");
            }
        } catch(NeoException neo) {
        	throw neo;
        } catch (Exception e) {
			logger.error("Exception:: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
					"Product Registration Failed" , null, Locale.US, null, null,"Product Registration");

		}
        return response;
    }

	@Override
	public KycAadhaarXmlResponseDto okycDataPush(RegistrationRequestV2Dto request) throws NeoException {

		logger.info("RBL :: OKYC Data Push API call...");
		KycAadhaarXmlResponseDto xmlResponseDto = new KycAadhaarXmlResponseDto();
		StringBuilder sw = new StringBuilder();
		KYCBioResponseDto reQueryResponse;
		OKYCDataPushResponse okycDataPushResponse = null;
		KycAadhaarXmlResponseDto.Data data = new KycAadhaarXmlResponseDto.Data();
		try {
			JAXBContext aadhaarXmlJAXB = JAXBContext.newInstance(AadhaarXmlDto.class);
			Unmarshaller aadhaarXmlUnmarshaller = aadhaarXmlJAXB.createUnmarshaller();

			StringReader stringReader = new StringReader(request.getAadhaarXml());
			AadhaarXmlDto aadhaarXmlDto = (AadhaarXmlDto) aadhaarXmlUnmarshaller.unmarshal(stringReader);

			KYCBioRequestDto ekycRequryRequest = new KYCBioRequestDto();
			ekycRequryRequest.setBioeKycUserId(request.getBioeKycUserId());
			ekycRequryRequest.setBioeKycPassword(request.getBioeKycPassword());
			ekycRequryRequest.setEkycRefnum("OKYC" + request.getEKycRefNum());
			ekycRequryRequest.setEntityId(request.getEntityId());

			RegistrationRequestDtoV2 regRequestDtoV2=dozerBeanMapper.map(request,RegistrationRequestDtoV2.class);
			regRequestDtoV2.setEKycRefNum("OKYC" + request.getEKycRefNum());
			ekycRequryRequest.setRegistrationRequestDtoV2(regRequestDtoV2);
			
			reQueryResponse = getDemographicData(ekycRequryRequest, RblPropertyConstants.KYC_TYPE_XML);

			if (reQueryResponse == null) {

				StringBuffer stringBuffer = new StringBuffer();

				stringBuffer.append(Utils.nullCheckToEmpty(aadhaarXmlDto.getUidData().getPoa().getCo()));
				stringBuffer.append(" ");
				stringBuffer.append(Utils.nullCheckToEmpty(aadhaarXmlDto.getUidData().getPoa().getHouse()));
				stringBuffer.append(" ");
				stringBuffer.append(Utils.nullCheckToEmpty(aadhaarXmlDto.getUidData().getPoa().getStreet()));
				stringBuffer.append(" ");
				stringBuffer.append(Utils.nullCheckToEmpty(aadhaarXmlDto.getUidData().getPoa().getLandmark()));
				stringBuffer.append(" ");
				stringBuffer.append(Utils.nullCheckToEmpty(aadhaarXmlDto.getUidData().getPoa().getLoc()));
				stringBuffer.append(" ");
				stringBuffer.append(Utils.nullCheckToEmpty(aadhaarXmlDto.getUidData().getPoa().getPo()));
				stringBuffer.append(" ");
				stringBuffer.append(Utils.nullCheckToEmpty(aadhaarXmlDto.getUidData().getPoa().getDist()));
				stringBuffer.append(" ");
				stringBuffer.append(Utils.nullCheckToEmpty(aadhaarXmlDto.getUidData().getPoa().getState()));
				stringBuffer.append(" ");
				stringBuffer.append(Utils.nullCheckToEmpty(aadhaarXmlDto.getUidData().getPoa().getPc()));

				String kycAddress = stringBuffer.toString();

				OKYCDataPushRequest.RequestHeader header = OKYCDataPushRequest.RequestHeader.builder()
						.username(request.getBioeKycUserId())
						.password(request.getBioeKycPassword())
						.build();

				byte[] rawXmlBytes = request.getAadhaarXml().getBytes(StandardCharsets.UTF_8);

				OKYCDataPushRequest okycDataPushRequest = OKYCDataPushRequest.builder()
						.header(header)
						.kycType("OKYC")
						.address(kycAddress)
						.photo(aadhaarXmlDto.getUidData().getPht())
						.gender(aadhaarXmlDto.getUidData().getPoi().getGender())
						.dateOfBirth(aadhaarXmlDto.getUidData().getPoi().getDob())
						.name(aadhaarXmlDto.getUidData().getPoi().getName())
						.transactionTimestamp("")
						.kycTimestamp("")
						.rawXml(Base64.encodeBase64String(rawXmlBytes))
						.version("1.0")
						.hashedEmail("").hashedMobileNumber("").faceAuthImage("")
						.faceAuthScore("").faceAuthStatus("").verifiedEmailId("")
						.verifiedMobileNumber("")
						.nameMatchScore("").addressMatchScore("").dobMatchScore("")
						.latitude("").longitude("")
						.transactionReferenceNumber("OKYC" + aadhaarXmlDto.getReferenceId())
						.clientId(request.getEntityId())
						.okycRequestId("OKYC" + request.getEKycRefNum())
						.build();

				sw.append(new ObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, true)
						.writeValueAsString(okycDataPushRequest));
				String okycDataPushRequestStr = sw.toString();

				logger.info("Actual Payload ::" + okycDataPushRequestStr);

				MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

				PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
						ProductConstants.OKYC_DATA_PUSH);

				String responseBody = "";

				if ("mock".equalsIgnoreCase(serviceModel.getServiceEndpoint())) {
					okycDataPushResponse = new OKYCDataPushResponse();
					okycDataPushResponse.setStatus("1");
				} else {
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					responseBody = postForEntityWithHeaders(sw.toString(), serviceModel.getServiceEndpoint(), HttpMethod.POST, headers,
							Boolean.TRUE);

					okycDataPushResponse = new ObjectMapper()
							.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true)
							.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
							.readValue(responseBody, OKYCDataPushResponse.class);
				}
			} else {
				okycDataPushResponse = new OKYCDataPushResponse();
				okycDataPushResponse.setStatus("1");
			}
			if (okycDataPushResponse != null) {
				if ("1".equals(okycDataPushResponse.getStatus())) {

					if (reQueryResponse == null) {
						KYCBioRequestDto ekycRequryRequest1 = new KYCBioRequestDto();
						ekycRequryRequest1.setBioeKycUserId(request.getBioeKycUserId());
						ekycRequryRequest1.setBioeKycPassword(request.getBioeKycPassword());
						ekycRequryRequest1.setEkycRefnum("OKYC" + request.getEKycRefNum());
						ekycRequryRequest1.setEntityId(request.getEntityId());
						
						ekycRequryRequest1.setRegistrationRequestDtoV2(regRequestDtoV2);

						reQueryResponse = getDemographicData(ekycRequryRequest1, RblPropertyConstants.KYC_TYPE_XML);
					}

					if (reQueryResponse != null) {
						//build kyc response
						KycAadhaarXmlResponseDto.Address address = new KycAadhaarXmlResponseDto.Address();
						address.setLandmark(aadhaarXmlDto.getUidData().getPoa().getLandmark());
						address.setCareof(aadhaarXmlDto.getUidData().getPoa().getCo());
						address.setCountry(aadhaarXmlDto.getUidData().getPoa().getCountry());
						address.setDist(aadhaarXmlDto.getUidData().getPoa().getDist());
						address.setLoc(aadhaarXmlDto.getUidData().getPoa().getLoc());
						address.setHouse(aadhaarXmlDto.getUidData().getPoa().getHouse());
						address.setPc(aadhaarXmlDto.getUidData().getPoa().getPc());
						address.setState(aadhaarXmlDto.getUidData().getPoa().getState());
						address.setStreet(aadhaarXmlDto.getUidData().getPoa().getStreet());
						address.setSubdist(aadhaarXmlDto.getUidData().getPoa().getSubdist());
						address.setPo(aadhaarXmlDto.getUidData().getPoa().getPo());
						address.setVtc(aadhaarXmlDto.getUidData().getPoa().getVtc());

						KycAadhaarXmlResponseDto.BasicInfo basicInfo = new KycAadhaarXmlResponseDto.BasicInfo();
						basicInfo.setDob(aadhaarXmlDto.getUidData().getPoi().getDob());
						basicInfo.setGender(aadhaarXmlDto.getUidData().getPoi().getGender());
						basicInfo.setName(aadhaarXmlDto.getUidData().getPoi().getName());

						KycAadhaarXmlResponseDto.Signaturedata signaturedata = new KycAadhaarXmlResponseDto.Signaturedata();
						signaturedata.setSigned(true);
						signaturedata.setIssuer("UIDAI");

						KycAadhaarXmlResponseDto.Result result = new KycAadhaarXmlResponseDto.Result();
						result.setSignatureData(signaturedata);
						result.setOfflineAadhaarDownloadedAt(TenantContextHolder.getNeoTenant());

						data.setAddress(address);
						data.setBasicInfo(basicInfo);
						data.setImage(aadhaarXmlDto.getUidData().getPht());
						data.setResult(result);
						data.setStatus(1);
					} else {
						data.setStatus(0);
					}
				} else
					data.setStatus(0);
			}

			xmlResponseDto.setData(data);

		} catch (Exception e) {
			logger.info("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
					"Saving Demographic Data Failed" , null, Locale.US, null, null,ProductConstants.OKYC_DATA_PUSH);
		}
		return xmlResponseDto;
	}

	@Override
	public KYCBioResponseDto fetchDemographicData(KYCBioRequestDto request) throws NeoException {

		logger.info("RBL Demographic Data Fetch API Call...");

		KYCBioResponseDto response;
		StringWriter sw;
		GenderType gender;

		try {

			MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

			EkycRequeryRequest.RequestHeader requestHeader = EkycRequeryRequest.RequestHeader.builder()
					.username(request.getBioeKycUserId())
					.password(request.getBioeKycPassword())
					.build();

			EkycRequeryRequest ekycRequryRequest = EkycRequeryRequest.builder()
					.header(requestHeader)
					.uniquerequestid(request.getEkycRefnum())
					.build();

			sw = new StringWriter();

			sw.append(new ObjectMapper()
					.configure(SerializationFeature.WRAP_ROOT_VALUE, true)
					.writeValueAsString(ekycRequryRequest));

			String reQueryRequestString = sw.toString();

			logger.info("Payload for EKYC Requery :: " + reQueryRequestString);

			PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
					ProductConstants.EKYC_DEMOGRAPHICDATA);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			EkycRequeryResponse ekycRequeryResponse = null;
			String responseBody = postForEntityWithHeaders(sw.toString(), serviceModel.getServiceEndpoint(), HttpMethod.POST,
					headers, Boolean.TRUE);
			ObjectMapper mapper = new ObjectMapper();
			try {
				ekycRequeryResponse = new ObjectMapper()
						.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true)
						.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
						.readValue(responseBody, EkycRequeryResponse.class);
			} catch (Exception ex) {
				Map<String, String> excpRespMap = mapper.readValue(responseBody, Map.class);

				Map<String, String> respBodyMap = mapper.readValue(
						mapper.writeValueAsString(excpRespMap.get("errorres")), Map.class);

				if (respBodyMap.get("description").contains("OKYC Request id not exist")) {
					return null;
				} else {
					logger.info("Exception Occured :: ", ex);
					throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
							"Fetching Demographic Data Failed" , null, Locale.US, null, null,ProductConstants.EKYC_DEMOGRAPHICDATA);
				}
			}
			response = dozerBeanMapper.map(ekycRequeryResponse, KYCBioResponseDto.class);

			AddressDto addressDto = new AddressDto();
			addressDto.setAddressCategory(AddressCategory.PERMANENT);
			addressDto.setAddress1(ekycRequeryResponse.getBuilding());
			addressDto.setAddress2(ekycRequeryResponse.getStreet());
			addressDto.setAddress3(ekycRequeryResponse.getArea());
			addressDto.setCity(ekycRequeryResponse.getCity());
			addressDto.setCountry(ekycRequeryResponse.getCountry());
			addressDto.setPincode(ekycRequeryResponse.getPin());
			addressDto.setDistrict(ekycRequeryResponse.getDistrict());
			addressDto.setState(ekycRequeryResponse.getState());

			response.setAddressDto(addressDto);
			response.setAuthCode(ekycRequeryResponse.getAuthenticationcode());
			response.setKycRefNo(ekycRequeryResponse.getUniquerequestid());
			response.setEntityId(request.getEntityId());
			response.setCareOf(ekycRequeryResponse.getCareOf());
			response.setTitle(ekycRequeryResponse.getGender().equals("M") ? "Mr" : "Ms");

		} catch(NeoException neo) {
			throw neo;
		} catch (Exception e) {
			logger.info("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
					"Fetching Demographic Data Failed" , null, Locale.US, null, null,ProductConstants.EKYC_DEMOGRAPHICDATA);

		}
		return response;
	}


	@Override
	public RegistrationResponseDto retailCifCreation(RegistrationRequestV2Dto request) throws NeoException {
		return null;
	}

	@Override
	public AccountCreationResponseDto savingsAccountCreation(AccountCreationRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public BalanceCheckResponseDto fetchAccountBalance(BalanceCheckRequest request) throws NeoException {

		return null;
	}

	@Override
	public FetchAccountStatemetResponse fetchAccountStatement(FetchAccountStatementRequest request)
			throws NeoException {

		return null;
	}

	@Override
	public AccountCreationResponseDto termDepositCreation(AccountCreationRequestDto request) throws NeoException {

		return null;
	}

	@Override
	public AadhaarOtpResponse generateOtpAadhaar(AadhaarOtpRequest request) throws NeoException {

		return null;
	}

	@Override
	public AadhaarOtpResponse validateOtpAadhaar(AadhaarOtpRequest request) throws NeoException {

		return null;
	}

	@Override
	public PanValidationResponse validatePan(PanValidationRequest request) throws NeoException {
		PanValidationResponse response = new PanValidationResponse();
    	PanValidationResponse.PanValidationInnerResponse innerResponse = new PanValidationResponse
    			.PanValidationInnerResponse();

    	logger.info("RBL :: PAN Inquiry API call..");
		try {
			PANInquiryRequest.PANInquiryRequestHeader panInqReqHeader = PANInquiryRequest.PANInquiryRequestHeader
					.builder().TranID(TxnIdGenerator.getReferenceNumber(16))
					.CorpID(rblCorporateId)
					.ApproverID("").CheckerID("").MakerID("").NSDLUserID(panUserId).build();
			
			Map<String,String> panMap = new HashMap<String, String>();
			panMap.put("pan1", request.getIdNumber());
			
			PANInquiryRequest.PANInquiryRequestBody panInqReqBody = PANInquiryRequest.PANInquiryRequestBody.builder()
					.panNumbers(Arrays.asList(panMap)).build();
			PANInquiryRequest.PANInquiryRequestSignature panInqSignature = PANInquiryRequest.PANInquiryRequestSignature
					.builder().Signature("").build();

			PANInquiryRequest panInqReq = PANInquiryRequest.builder().Header(panInqReqHeader).Body(panInqReqBody)
					.Signature(panInqSignature).build();

			String panRequest = new ObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, true)
					.writeValueAsString(panInqReq);

			MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

			PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
					ProductConstants.PAN_VALIDATION);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			logger.info("Request Payload for PAN Inquiry ::" + panRequest);

			String responseBody = postForEntityWithHeaders(panRequest, serviceModel.getServiceEndpoint(),
					HttpMethod.POST, headers, Boolean.TRUE);
			
			logger.info("Response Received for PAN Inquiry :: " + responseBody);
			
			if(responseBody != null) {
				PANInquiryResponse panInqResponse = new ObjectMapper()
						.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true)
						.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
						.readValue(responseBody, PANInquiryResponse.class);
				
				if(panInqResponse.getBody().getPanDetails() != null) {
					PANInquiryResponse.PANInquiryDetails details = panInqResponse.getBody().getPanDetails().get(0);
					boolean valid = true;
					if(details.getPanStatus().equalsIgnoreCase("D") || details.getPanStatus().equalsIgnoreCase("X") ||
							details.getPanStatus().equalsIgnoreCase("F") || details.getPanStatus().equalsIgnoreCase("N")) {
						valid = false;
					}
					innerResponse = PanValidationResponse.PanValidationInnerResponse.builder()
							.panNo(details.getPanNumber()).exists(details.getPanStatus())
							.lastName(details.getLastName()).middleName(details.getMiddleName())
							.firstName(details.getFirstName()).title(details.getPanTitle())
							.issueDate("").nameOnCard("").lastUpdated(details.getLastUpdateDate())
							.valid(valid).build();
				} else {
					throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
							panInqResponse.getBody().getEsbReturnDesc() , null, Locale.US, null, null,ProductConstants.PAN_VALIDATION);
				}
			} else {
				throw new NeoException("Failed", "Failed", null, null, "PANInquiry Service Failed");
			}
		} catch (NeoException neo) {
            throw neo;
        } catch (Exception e) {
			logger.m2pdebug("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
					"Pan Validation Failed" , null, Locale.US, null, null,ProductConstants.PAN_VALIDATION);

		}
		response.setValidationResponse(innerResponse);
		return response;
	}

	@Override
	public RegistrationResponseDto register(RegistrationRequestV2Dto request) throws NeoException {

		return null;
	}

	@Override
	public RegistrationResponseDto addProduct(RegistrationRequestV2Dto request) throws NeoException {

		return null;
	}

	@Override
	public FundTransferResponseDto fundTransfer(FundTransferRequestDto request) throws NeoException {

		FundTransferResponseDto responseDto = new FundTransferResponseDto();
		StringBuilder sw = new StringBuilder();
		String beneBank = "";
		NeoFundTransfer neoFundTransfer;
		try {

			neoFundTransfer = fundTransferDao.findByExternalTransactionIdAndTenant(request.getExternalTransactionId(),TenantContextHolder.getNeoTenant());

			if (neoFundTransfer == null) {
				neoFundTransfer = dozerBeanMapper.map(request, NeoFundTransfer.class);
				neoFundTransfer.setBeneficiaryAccountNo(request.getToAccountNo());
				neoFundTransfer.setRemitterAccountNo(request.getFromAccountNo());
				neoFundTransfer.setTenant(TenantContextHolder.getNeoTenant());
				neoFundTransfer.setTransactionAmount(request.getAmount());
				neoFundTransfer.setTransactionStatus("IN PROGRESS");
				fundTransferDao.save(neoFundTransfer);
			}
			if (!request.getTransactionType().equalsIgnoreCase("IFT")) {
				beneBank = request.getBeneficiaryIfsc().substring(0, 4).concat(" Bank");
			}
			VARequestHeader reqHeader = VARequestHeader.builder()
					.TranID(request.getExternalTransactionId())
					.CorpID(rblCorporateId)
					.ApproverID("").CheckerID("").MakerID("").build();

			SinglePaymentRequest.PaymentReqBody paymentReqBody = SinglePaymentRequest.PaymentReqBody.builder()
					.amount(request.getAmount())
					.benAcctNo(request.getToAccountNo())
					.benAddress("")
					.benBankcd("")
					.benBankname(beneBank)
					.benBranchcd("")
					.benEmail(request.getBeneficiaryEmail() != null ? request.getBeneficiaryEmail() : "")
					.benIfsc(request.getBeneficiaryIfsc())
					.benMobile(request.getBeneficiaryMobile() != null ? request.getBeneficiaryMobile() : "")
					.benName(request.getBeneficiaryName())
					.benTrnparticulars(request.getDescription())
					.benParttrnrmks("")
					.debitAcctName(request.getRemitterName())
					.debitAcctNo(request.getFromAccountNo())
					.debitIfsc(request.getRemitterIfsc())
					.debitMobile("0000000000")
					.debitParttrnrmks("")
					.debitTrnparticulars(request.getDescription())
					.issueBranchcd("")
					.modeOfPay(request.getTransactionType().equalsIgnoreCase("IFT") ? "FT" : request.getTransactionType())
					.remarks(request.getDescription())
					.build();

			SinglePaymentRequest.PaymentReqSignature paymentReqSign = SinglePaymentRequest.PaymentReqSignature
					.builder().signature("").build();

			SinglePaymentRequest paymentRequest = SinglePaymentRequest.builder()
					.header(reqHeader)
					.body(paymentReqBody)
					.sign(paymentReqSign)
					.build();

			sw.append(new ObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, true)
					.writeValueAsString(paymentRequest));

			String statusRequestString = sw.toString();

			logger.info("Actual Payload ::" + statusRequestString);

			MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

			PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
					ProductConstants.FUND_TRANSFER);

			logger.info("RBL URL :: " + serviceModel.getServiceEndpoint());

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			String responseBody = postForEntityWithHeaders(sw.toString(), serviceModel.getServiceEndpoint(), HttpMethod.POST, headers,
					Boolean.TRUE);

			SinglePaymentResponse paymentResponse = new ObjectMapper()
					.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true)
					.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
					.readValue(responseBody, SinglePaymentResponse.class);

			logger.info("Response Received for Fund Transfer :: " + responseBody);

			String txnStatus = paymentResponse.getHeader().getStatus();
			String txnRefNo = "";
			String desc = "Request Initiated";

			if (txnStatus.equalsIgnoreCase("success") ||
					txnStatus.equalsIgnoreCase("initiated") ||
					txnStatus.equalsIgnoreCase("In Progress")) {
				if (request.getTransactionType().equalsIgnoreCase("IFT") ||
						request.getTransactionType().equalsIgnoreCase("DD")) {
					txnRefNo = paymentResponse.getBody().getRefno();
					desc = "Request processed Successfully";
				} else if (request.getTransactionType().equalsIgnoreCase("IMPS")) {
					if ("00".equals(paymentResponse.getHeader().getResponseCode())) {
						txnRefNo = paymentResponse.getBody().getRrn();
						desc = "IMPS Request Processed";
					} else {
						txnStatus = "In Progress";
						txnRefNo = (paymentResponse.getBody().getRrn()!=null?
								(paymentResponse.getBody().getRrn())
								: (paymentResponse.getBody().getChannelPartnerRefno()));
						desc = paymentResponse.getHeader().getErrorDescription().asText();
					}
				} else {
					txnRefNo = paymentResponse.getBody().getUtrno();
				}
			} else if (txnStatus.equalsIgnoreCase("on hold")) {
				txnRefNo = paymentResponse.getBody().getRefno();
				desc = paymentResponse.getHeader().getErrorDescription().asText();
			} else {
				//Failed
				txnStatus = "FAILED";
				if (request.getTransactionType().equalsIgnoreCase("IMPS")) {
					if(paymentResponse.getBody()!=null){
						txnRefNo = paymentResponse.getBody().getChannelPartnerRefno()!=null?
								paymentResponse.getBody().getChannelPartnerRefno():"";
						txnStatus = "IN PROGRESS";
					}else{
						txnRefNo="";
					}
				}
				desc = paymentResponse.getHeader().getErrorDescription().asText();
				neoFundTransfer.setTransactionFailureReason(desc);
			}

			responseDto.setDescription(desc);
			responseDto.setBankReferenceNo(txnRefNo);
			responseDto.setExternalTransactionId(request.getExternalTransactionId());
			responseDto.setStatus(txnStatus.toUpperCase());

			neoFundTransfer.setTransactionStatus(txnStatus.toUpperCase());
			neoFundTransfer.setBankTransactionId(txnRefNo);
			fundTransferDao.save(neoFundTransfer);

			return responseDto;

		} catch (Exception e) {
			logger.info("Exception Occured :: ", e);
			responseDto.setDescription("Request Initiated.");
			responseDto.setBankReferenceNo("");
			responseDto.setExternalTransactionId(request.getExternalTransactionId());
			responseDto.setStatus("IN PROGRESS");
			//throw new NeoException("Service Unavailable", "Service Unavailable", null, null, "Fund Transfer Failed");
			return responseDto;
		}
	}

	@Override
	public FundTransferResponseDto paymentStatusCheck(FundTransferRequestDto request) throws NeoException {

		FundTransferResponseDto responseDto = new FundTransferResponseDto();
		StringBuilder sw = new StringBuilder();
		String bankRefno = "";
		String txnStatus="";
		try {

			NeoFundTransfer neoFundTransfer = fundTransferDao.findByExternalTransactionIdAndTenant(
					request.getExternalTransactionId(),TenantContextHolder.getNeoTenant());

			if (neoFundTransfer == null) {
				logger.info("No such transaction exists!!");
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.INTERNAL_SERVER_ERROR,
						"No Such transaction exists" , null, Locale.US, null, null,ProductConstants.FT_STATUS_CHECK);
			}
			String referenceNum = TxnIdGenerator.getReferenceNumber(10);

			VARequestHeader reqHeader = VARequestHeader.builder().TranID(referenceNum).CorpID(rblCorporateId)
					.ApproverID("").CheckerID("").MakerID("").build();

			PaymentStatusRequest.StatusReqBody statusReqBody = new PaymentStatusRequest.StatusReqBody();
			statusReqBody.setOrgTxnId(request.getExternalTransactionId());

			PaymentStatusRequest.StatusReqSignature statusReqSign = new PaymentStatusRequest.StatusReqSignature();
			statusReqSign.setSignature("");

			PaymentStatusRequest statusRequest = PaymentStatusRequest.builder()
					.header(reqHeader)
					.body(statusReqBody)
					.sign(statusReqSign)
					.build();

			sw.append(new ObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, true)
					.writeValueAsString(statusRequest));

			String statusRequestString = sw.toString();

			logger.info("Actual Payload :: " + statusRequestString);

			MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());

			PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
					ProductConstants.FT_STATUS_CHECK);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			String responseBody = postForEntityWithHeaders(sw.toString(), serviceModel.getServiceEndpoint(), HttpMethod.POST, headers,
					Boolean.TRUE);

			PaymentStatusResponse paymentStatusResponse = new ObjectMapper()
					.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true)
					.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
					.readValue(responseBody, PaymentStatusResponse.class);

			logger.info("Response Received for Fund Transfer Status Check :: " + responseBody);
			if (paymentStatusResponse.getBody() != null) {
				if ("FAILED".equalsIgnoreCase(paymentStatusResponse.getBody().getTxnStatus()) ||
						"Failure".equalsIgnoreCase(paymentStatusResponse.getBody().getTxnStatus())) {
					if (neoFundTransfer.getTransactionType().equalsIgnoreCase("neft")
							|| (neoFundTransfer.getTransactionType().equalsIgnoreCase("rtgs"))) {
						bankRefno = paymentStatusResponse.getBody().getUtr();
					} else if (neoFundTransfer.getTransactionType().equalsIgnoreCase("imps")) {
						bankRefno = paymentStatusResponse.getBody().getRrn();
					}
					responseDto.setStatus("FAILED");
					responseDto.setBankReferenceNo(bankRefno != null ? bankRefno : "");
					responseDto.setDescription(paymentStatusResponse.getHeader().getErrorDescription().asText());
				} else {

					if (neoFundTransfer.getTransactionType().equalsIgnoreCase("neft")
							|| (neoFundTransfer.getTransactionType().equalsIgnoreCase("rtgs")))
					{
						bankRefno = paymentStatusResponse.getBody().getUtr();
						txnStatus="SUCCESS";
					}
					else if (neoFundTransfer.getTransactionType().equalsIgnoreCase("imps")) {
						if ("7".equals(paymentStatusResponse.getBody().getPaymentStatus())) {
							bankRefno = paymentStatusResponse.getBody().getRrn();
							txnStatus="SUCCESS";
						}else if ("8".equals(paymentStatusResponse.getBody().getPaymentStatus())){
							txnStatus="Failed";
						}else if ("9".equals(paymentStatusResponse.getBody().getPaymentStatus())){
							txnStatus="In Progress";
						}
					}
					responseDto.setDescription(paymentStatusResponse.getBody().getStatusDesc());
					responseDto.setExternalTransactionId(paymentStatusResponse.getBody().getOrgTxnId());
					responseDto.setBankReferenceNo(bankRefno);
					responseDto.setStatus(txnStatus.toUpperCase());
				}
			} else {
				responseDto.setExternalTransactionId(request.getExternalTransactionId());
				responseDto.setStatus(paymentStatusResponse.getHeader().getStatus());
				responseDto.setDescription(paymentStatusResponse.getHeader().getErrorDescription().asText());
			}
			return responseDto;

		} catch (NeoException nEx) {
			throw nEx;
		} catch (Exception e) {
			logger.info("Exception Occured :: ", e);
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR,
					"Fetching Payment Status Check Failed" , null, Locale.US, null, null,ProductConstants.FT_STATUS_CHECK);
		}
	}

	@Override
	public AccountModificationResponseDto savingsAccountModification(AccountModificationRequestDto request)
			throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AccountClosureResponseDto tdAccountClosure(AccountClosureRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AccountInquiryResponseDto tdAccountInquiry(AccountInquiryRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TDAcctCloseTrailResponseDto tdTrailAccountClosure(TdAccountTrailCloseRequestDto request)
			throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FundTransferResponseDto fundTransferWithOtp(FundTransferRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FundTransferResponseDto fundTransferValidateOtp(FundTransferRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public AccountInquiryResponseDto savingsAccountInquiry(AccountInquiryRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AccountInquiryResponseDto fetchAccountDetails(AccountInquiryRequestDto request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CustomerDedupCheckResponseDto dedupCheck(RegistrationRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterAccountDiscovery(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterAcctBalInquiry(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterOtpCreation(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterOtpVerification(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public PayLaterResponseDto payLaterDebitOrchestration(PayLaterRequestDto request) throws NeoException {
		return null;
	}

	@Override
	public BeneficiaryManagementResDto beneficiaryAddition(BeneficiaryManagementReqDto request) throws NeoException {
		return null;
	}
	
	@Override
	public CustomerGetDetailsResponseDto getDetails(CustomerGetDetailsRequestDto request) throws NeoException {
		CustomerGetDetailsResponseDto response = new CustomerGetDetailsResponseDto();
		StringWriter sw = new StringWriter();
		String responseBody = "";
        String requestString = "";
		try {
			MasterConfiguration config = partnerConfigService.findMasterConfigByPartnerId(TenantContextHolder.getNeoTenant());
            PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(config.getBankId(),
                    ProductConstants.GET_DETAILS);
            
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_XML);
            
            CustomerGetDetailsRequest.RequestHeader header = CustomerGetDetailsRequest.RequestHeader
            		.builder().username(request.getUsername()).password(request.getPassword()).build();
            
            CustomerGetDetailsRequest cifRequest = CustomerGetDetailsRequest.builder()
            		.header(header)
            		.refno(request.getEKycRefNum())
            		.mobilenumber(request.getMobilenumber())
            		.build();
            
            JAXBContext jaxbContext = JAXBContext.newInstance(CustomerGetDetailsRequest.class);
            Marshaller m = jaxbContext.createMarshaller();
            m.marshal(cifRequest, sw);
            
            requestString = sw.toString();

            logger.info("Payload for Customer Get Details :: " + requestString);
            
            responseBody = postForEntityWithHeaders(requestString, serviceModel.getServiceEndpoint(),
					HttpMethod.POST, headers, Boolean.TRUE);
            
            logger.info("Response Received for Customer Get Details ::" + responseBody);
            
            CustomerGetDetailsResponse responseOutDto = null;
            ErrorResponseXML errorResponseOutDto = null;
            if(responseBody != null) {
            	try {
	            JAXBContext responseOutJAXB = JAXBContext.newInstance(CustomerGetDetailsResponse.class);
	            Unmarshaller responseOutUnmarshaller = responseOutJAXB.createUnmarshaller();
	            StringReader stringReader = new StringReader(responseBody);
	             responseOutDto = (CustomerGetDetailsResponse) 
	            		responseOutUnmarshaller.unmarshal(stringReader);
            	} catch(Exception ex) {
                	JAXBContext responseOutJAXB = JAXBContext.newInstance(ErrorResponseXML.class);
                    Unmarshaller responseOutUnmarshaller = responseOutJAXB.createUnmarshaller();

                    StringReader stringReader = new StringReader(responseBody);
                    errorResponseOutDto = (ErrorResponseXML) responseOutUnmarshaller.unmarshal(stringReader);
                }
            
	            if(responseOutDto != null && responseOutDto.getStatus().equals("1")) {
	            	response = rblCommonService.buildGetDetailsResponse(responseOutDto);
	            } else {
					throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.INTERNAL_SERVER_ERROR,
							(errorResponseOutDto!=null?errorResponseOutDto.getDescription():NeoExceptionConstant.INTERNAL_SERVER_ERROR) , null, Locale.US, null, null,ProductConstants.GET_DETAILS);
	            }
            } else {
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.INTERNAL_SERVER_ERROR,
						"Fetching Product Details Failed" , null, Locale.US, null, null,ProductConstants.GET_DETAILS);
            }
		} catch(NeoException neo) {
        	throw neo;
        } catch (Exception e) {
			logger.error("Exception :: ",e);
        }
        return response;
	}

	@Override
	public NeoPayResponse upiPay(NeoPayRequest request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ValidateVpaRes validateVpa(ValidateVpaReq request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MerchantOBRes onboardSubMerchant(MerchantOBReq request) throws NeoException {
		// TODO Auto-generated method stub
		return null;
	}

	public String rblRecon() throws NeoException {
		ReconResponse response;
		ReconStatusResponse statusResponse;
		String bankId = "RBL";
		String reconReqStr = "";
		String responseString = "";
		try {
			logger.info("In RBL Ecollect - Recon...");

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			Calendar cal = Calendar.getInstance();
			if (cal.get(Calendar.HOUR_OF_DAY) == 0) {
				cal.add(Calendar.DATE, -1);
			}
			String requestDate = sdf.format(cal.getTime());
			logger.info("Request Date :: " + requestDate);

			PartnerServiceConfiguration serviceModel = partnerConfigService.findPartnerServiceByBankIdAndBankServiceId(bankId,
					ProductConstants.RBL_RECON);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			NeoBusinessCustomField reconTenantsCusField = customFieldService
					.findByFieldNameAndTenant(NeoCustomFieldConstants.RBL_RECON_TENANTS, bankId);

			logger.info("Tenants on Recon :: " + reconTenantsCusField.getFieldValue());

			String[] reconTenantFields = reconTenantsCusField.getFieldValue().split("\\|");

			for (String reconTenant : reconTenantFields) {

				String[] reconFields = reconTenant.split(":");

				String tenant = reconFields[0];
				String corpClientId = reconFields[1];

				logger.info("Currently Running Tenant :: " + tenant);

				TenantContextHolder.setNeoTenant(tenant);

				sdf = new SimpleDateFormat("yyyy-MM-dd");
				EcollectRequest request = new EcollectRequest();
				request.setCreditedAt(sdf.format(cal.getTime()));
				request.setEcollectCode(corpClientId);

				ReconRequest.ReconHeader header = ReconRequest.ReconHeader.builder()
						.TranID(TxnIdGenerator.getReferenceNumber(15)).CorpID(rblCorporateId).build();

				ReconRequest.ReconBody body = ReconRequest.ReconBody.builder().ClientID(corpClientId).date(requestDate)
						.build();

				ReconRequest reconReq = ReconRequest.builder().Header(header).Body(body).build();

				reconReqStr = new ObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, true)
						.writeValueAsString(reconReq);

				logger.info("Payload for Recon - " + tenant + " :: " + reconReqStr);

				String responseBody = postForEntityWithHeaders(reconReqStr, serviceModel.getServiceEndpoint(),
						HttpMethod.POST, headers, Boolean.TRUE);

//				logger.info("Response Received for Recon - " + tenant + " :: " + responseBody);

				if (responseBody != null) {
					if (responseBody.contains("No records found")) {
						responseString = responseString.isEmpty() ? responseString : responseString + " ::: ";
						responseString = responseString + "Tenant - " + tenant + " :: No records found";
						continue;
					}
					try {
						response = new ObjectMapper()
								.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true)
								.readValue(responseBody, ReconResponse.class);
					} catch (com.fasterxml.jackson.databind.exc.MismatchedInputException exc) {
						try {
							statusResponse = new ObjectMapper().readValue(responseBody, ReconStatusResponse.class);
							responseString = responseString.isEmpty() ? responseString : responseString + " ::: ";
							responseString = responseString + "Tenant - " + tenant + " :: "
									+ statusResponse.getGetRecord().getHeader().getStatusDesc();
							continue;
						} catch (com.fasterxml.jackson.databind.exc.MismatchedInputException ex) {
							Map<String, List<Map<String, String>>> result = new ObjectMapper().readValue(responseBody,
									new TypeReference<Map<String, List<Map<String, String>>>>() {
									});
							String resultStr = result.get("getRecordRes").get(0).get("Payload");
							responseString = responseString.isEmpty() ? responseString : responseString + " ::: ";
							responseString = responseString + "Tenant - " + tenant + " :: " + resultStr;
							continue;
						}
					}
				} else {
					throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SYSTEM_ERROR, "Recon Failed", null,
							Locale.US, null, null, ProductConstants.RBL_RECON);
				}

				int totalFromRecon = response.getGetRecordRes().size();
				logger.info("Total Ecollect Requests from Recon :: " + totalFromRecon);

				List<String> yapData = fetchYapEcollectRequestData("YAP", request);

				logger.info("Total Ecollect Requests from YAP :: " + yapData.size());

				if (response != null && response.getGetRecordRes() != null) {

					response.getGetRecordRes()
							.removeIf(e -> yapData.contains(e.getPayload().getData().getItem().getUTRNumber()));

					logger.info("Remaining Ecollect Requests to push :: " + response.getGetRecordRes().size());

					for (ReconResponse.GetRecordRes r : response.getGetRecordRes()) {

						ReconResponse.Item item = r.getPayload().getData().getItem();

						String req = new ObjectMapper().writeValueAsString(item);

						req = "{\"Data\":[" + req + "]}";

						ResponseEntity<RblEcollectResponseDto> resp = virtualAccountController.rblnotify(null, req,
								Optional.of(8));

						String referenceNum = TxnIdGenerator.getReferenceNumber();

						EcollectRequest ecollectRequest = new EcollectRequest();

						String transferType = "";
						if (item.getUTRNumber().contains("UPI")) {
							transferType = "UPI";
						} else {
							transferType = item.getMessageType();
						}
						ecollectRequest.setEcollectCode(item.getBeneficiaryAccountNumber());
						ecollectRequest.setAmount(item.getAmount());
						ecollectRequest.setTransferUniqueNumber(item.getUTRNumber());
						ecollectRequest.setCustomerCode(item.getClientCodeMaster());
						ecollectRequest.setVirtualAccountNo(item.getBeneficiaryAccountNumber());
						ecollectRequest.setTransferType(transferType);
						ecollectRequest.setRemitterAccountNumber(item.getSenderAccountNumber());
						ecollectRequest.setRemitterAccountType(item.getSenderAccountType());
						ecollectRequest.setRemitterIFSCCode(item.getSenderIFSC());
						ecollectRequest.setRemitterFullName(item.getSenderName());
						ecollectRequest.setCreditedAt(item.getCreditDate());
						ecollectRequest.setCreditAccountNo(item.getSenderAccountNumber());
						ecollectRequest.setCorporate(tenant);
						ecollectRequest.setApplication("RBL");
						ecollectRequest.setM2pReferenceNumber(referenceNum);
						ecollectRequest.setResult(resp.getBody().getResult().equals("0") ? "true" : "false");
						ecollectRequest.setDescription(resp.getBody().getErrorMsg());

						ecollectRequestDao.save(ecollectRequest);
					}
				}
				responseString = responseString.isEmpty() ? responseString : responseString + " ::: ";
				responseString = responseString + "Tenant - " + tenant + " :: fromRecon - " + totalFromRecon
						+ " :: fromYAP - " + yapData.size() + " :: toYAP - " + response.getGetRecordRes().size();
			}
		} catch (NeoException neo) {
			logger.error("Exception Occured :: " + neo.getDetailMessage());
			throw neo;
		} catch (Exception e) {
			logger.error("Exception Occured :: " + e.getMessage());
			logger.error("Exception :: ", e);
		}
		return responseString;
	}

}