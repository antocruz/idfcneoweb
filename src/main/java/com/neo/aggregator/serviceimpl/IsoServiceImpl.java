package com.neo.aggregator.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.neo.aggregator.config.ClientInit;
import com.neo.aggregator.constant.NeoCustomFieldConstants;
import com.neo.aggregator.constant.TransactionTypeConstants;
import com.neo.aggregator.dao.IsoMessageRepo;
import com.neo.aggregator.dto.AccountIdentification;
import com.neo.aggregator.dto.AuthorizationRequestDto;
import com.neo.aggregator.dto.AuthorizationRequestDto.Request;
import com.neo.aggregator.dto.AuthorizationResponseDto;
import com.neo.aggregator.dto.AuthorizationResponseDto.Response;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.ResponseDetails;
import com.neo.aggregator.dto.ReversalRequestDto;
import com.neo.aggregator.dto.ReversalResponseDto;
import com.neo.aggregator.enums.VisaFieldType;
import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.iso8583.IsoType;
import com.neo.aggregator.iso8583.MessageFactory;
import com.neo.aggregator.jreactive8583.client.Iso8583Client;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.IsoAuthorizationMessage;
import com.neo.aggregator.model.NeoBusinessCustomField;
import com.neo.aggregator.service.IsoService;
import com.neo.aggregator.service.NeoBusinessCustomFieldService;
import com.neo.aggregator.utility.CommonService;
import com.neo.aggregator.utility.DateUtilityFunction;
import com.neo.aggregator.utility.ISOUtil;
import com.neo.aggregator.utility.Iso8583;
import com.neo.aggregator.utility.IsoMessageToIso8583Converter;

@Service
public class IsoServiceImpl implements IsoService {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(IsoServiceImpl.class);

	@Autowired
	private ClientInit clientInit;

	@Autowired
	private IsoMessageToIso8583Converter isoMessageToIso8583Converter;

	@Autowired
	@Qualifier("isoConcurrentMap")
	private ConcurrentHashMap<String, Iso8583> isoHashMap;

	@Value("${switch.network}")
	private String switchNetwork;

	@Autowired
	private IsoMessageRepo isoMessageRepo;

	@Autowired
	private CommonService commonService;
	
	@Autowired
	private NeoBusinessCustomFieldService customFieldService;

	@Override
	public Iso8583 authorizeTransaction(Iso8583 request, String tenant) throws NeoException {

		Iso8583 response = null;

		try {

			Iso8583Client<IsoMessage> isoClient = clientInit.getPosIssuerClientMap().get(tenant);
			MessageFactory<IsoMessage> isoMessageFactory = isoClient.getIsoMessageFactory();

			IsoMessage isoMessage = isoMessageFactory.getMessageTemplate(Integer.parseInt(request.getMti(), 16));

			String dateTimeLocal = DateUtilityFunction.dateInFormat(new Date(), "yyyyMMddHHmmss");

			String dateTime = dateTimeLocal.substring(0, 8);

			isoMessage.setValue(3, request.getProcessingCode(), IsoType.NUMERIC, 6);
			isoMessage.setValue(4, StringUtils.leftPad(request.getTransactionAmount(), 16, '0'), IsoType.NUMERIC, 16);
			isoMessage.setValue(11, request.getRrn(), IsoType.NUMERIC, 12);
			isoMessage.setValue(12, dateTimeLocal, IsoType.NUMERIC, 14);
			isoMessage.setValue(17, dateTime, IsoType.NUMERIC, 8);
			isoMessage.setValue(24, request.getFileUpdateCode(), IsoType.NUMERIC, 3);// Function code
			isoMessage.setValue(32, request.getAcquiringInstitutionCode(), IsoType.LLVAR, 0);
			isoMessage.setValue(34, request.getPrimaryAccountNoExtended(), IsoType.LLVAR, 0);
			isoMessage.setValue(37, request.getRrn(), IsoType.NUMERIC, 12);
			isoMessage.setValue(41, request.getCardAcceptorTerminalId(), IsoType.ALPHA, 16);
			isoMessage.setValue(42, request.getCardAcceptorId(), IsoType.ALPHA, 15);
			isoMessage.setValue(43, request.getCardAcceptorNameLocation(), IsoType.LLVAR, 0);
			isoMessage.setValue(49, request.getTransactionCurrencyCode(), IsoType.ALPHA, 3);
			isoMessage.setValue(56, request.getAdditionalResponseData(), IsoType.LLVAR, 0);
			isoMessage.setValue(102, request.getAccountIdentification1(), IsoType.LLVAR, 0);
			isoMessage.setValue(123, request.getHostName_DE123(), IsoType.LLLVAR, 0);
			isoMessage.setValue(124, request.getTransactionOrigin_DE124(), IsoType.LLLVAR, 0);

			isoMessage.setBinaryBitmap(true);

			response = publishIsoSend(request, isoClient, isoMessage, tenant);

		} catch (Exception e) {
			e.printStackTrace();
			throw new NeoException();
		}
		return response;

	}

	@Override
	public Iso8583 reversalTransaction(Iso8583 request, String tenant) throws NeoException {

		Iso8583 response = null;

		try {

			request.setMti("1420");
			
			String dateTimeLocal = DateUtilityFunction.dateInFormat(new Date(), "yyyyMMddHHmmss");

			String dateTime = dateTimeLocal.substring(0, 8);

			Iso8583Client<IsoMessage> isoClient = clientInit.getPosIssuerClientMap().get(tenant);
			MessageFactory<IsoMessage> isoMessageFactory = isoClient.getIsoMessageFactory();

			IsoMessage isoMessage = isoMessageFactory.getMessageTemplate(Integer.parseInt(request.getMti(), 16));

			isoMessage.setValue(3, request.getProcessingCode(), IsoType.NUMERIC, 6);
			isoMessage.setValue(4, StringUtils.leftPad(request.getTransactionAmount(), 16, '0'), IsoType.NUMERIC, 16);
			isoMessage.setValue(11, request.getRrn(), IsoType.NUMERIC, 12);
			isoMessage.setValue(12, dateTimeLocal, IsoType.NUMERIC, 14);
			isoMessage.setValue(17, dateTime, IsoType.NUMERIC, 8);
			isoMessage.setValue(24, request.getFileUpdateCode(), IsoType.NUMERIC, 3);// Function code
			isoMessage.setValue(32, request.getAcquiringInstitutionCode(), IsoType.LLVAR, 0);
			isoMessage.setValue(34, request.getPrimaryAccountNoExtended(), IsoType.LLVAR, 0);
			isoMessage.setValue(37, request.getRrn(), IsoType.NUMERIC, 12);
			isoMessage.setValue(41, request.getCardAcceptorTerminalId(), IsoType.ALPHA, 16);
			isoMessage.setValue(42, request.getCardAcceptorId(), IsoType.ALPHA, 15);
			isoMessage.setValue(43, request.getCardAcceptorNameLocation(), IsoType.LLVAR, 0);
			isoMessage.setValue(49, request.getTransactionCurrencyCode(), IsoType.ALPHA, 3);
			isoMessage.setValue(56, request.getAdditionalResponseData(), IsoType.ALPHA, 40);
			isoMessage.setValue(102, request.getAccountIdentification1(), IsoType.LLVAR, 0);
			isoMessage.setValue(123, request.getHostName_DE123(), IsoType.LLLVAR, 0);
			isoMessage.setValue(124, request.getTransactionOrigin_DE124(), IsoType.LLLVAR, 0);

			isoMessage.setBinaryBitmap(true);

			response = publishIsoSend(request, isoClient, isoMessage,tenant);

		} catch (Exception e) {
			e.printStackTrace();
			throw new NeoException();
		}
		return response;
	}

	@Override
	public AuthorizationResponseDto authorizeTransaction(AuthorizationRequestDto authRequest) throws NeoException {

		AuthorizationResponseDto authorizationResponse = new AuthorizationResponseDto();
		Response res = authorizationResponse.getOffIssuerPurchaseResult();
		Iso8583 response = null;
		IsoMessage isoMessage = null;
		
		String kitNo = authRequest.getOffUsIssuerPurchaseRequest().getProxyCardNo();
		String acqInstitutionCode = authRequest.getIsoRequest().getAcquiringInstitutionCode();

		try {

			String tenant = authRequest.getTenant();

			logger.info("Authorize Transaction for Tenant ::" + tenant);

			Iso8583 request = authRequest.getIsoRequest();
			Request authReq  = authRequest.getOffUsIssuerPurchaseRequest();

			List<AccountIdentification> accountDetails = authRequest.getAccountIdentification();

			String accountIdentification1 = null;
			String accountCurrency = null;

			for (AccountIdentification id : accountDetails) {

				if (id.getDefaultAccount()) {

					StringBuffer bf = new StringBuffer();
					bf.append(StringUtils.rightPad(id.getBankId(), 11, "")); // Bank id
					bf.append(StringUtils.rightPad(id.getSolId(), 8, "")); // Sol id
					bf.append(StringUtils.rightPad(id.getAccountNo(), 19, "")); // Account No
					accountIdentification1 = bf.toString();
					accountCurrency = id.getTransactionCurrency();
				}
			}

			request.setMti("1200");
			request.setFileUpdateCode("200");
			request.setTransactionCurrencyCode(authRequest.getTransactionCurrencyName());
			request.setAccountIdentification1(accountIdentification1);
			String cardAcceptorId = returnCardAcceptorId(authReq.getChannel(), authReq.getNetwork());
			
			String txnType = authReq.getTransactionType();
			
			String txnOrg = fetchTransactionOrigin(txnType);
			String processingCode = fetchProcessingCode(txnType);

			String dateTimeLocal = DateUtilityFunction.getISOdateToNewFormat(request.getDateTimeTransaction(),
					"MMddHHmmss",
					"yyyyMMddHHmmss");

			String dateTime = dateTimeLocal.substring(0, 8);

			Iso8583Client<IsoMessage> isoClient = clientInit.getPosIssuerClientMap().get(tenant);
			MessageFactory<IsoMessage> isoMessageFactory = isoClient.getIsoMessageFactory();

			isoMessage = isoMessageFactory.getMessageTemplate(Integer.parseInt(request.getMti(), 16));

			isoMessage.setValue(3, processingCode, IsoType.NUMERIC, 6);
			if(authReq.getDccAmount()!=null) {
				isoMessage.setValue(4, ISOUtil.formatDouble(Double.parseDouble(authReq.getDccAmount()), 16, '0'), IsoType.NUMERIC,
						16);
			}else if(request.getCardHolderBillingAmount()!=null){
				isoMessage.setValue(4, StringUtils.leftPad(request.getCardHolderBillingAmount(), 16, '0'), IsoType.NUMERIC,
						16);
			}else {
				isoMessage.setValue(4, StringUtils.leftPad(request.getTransactionAmount(), 16, '0'), IsoType.NUMERIC,
						16);
			}
			
			isoMessage.setValue(11, request.getRrn(), IsoType.NUMERIC,
					12);
			isoMessage.setValue(12, dateTimeLocal, IsoType.NUMERIC, 14);
			isoMessage.setValue(17, dateTime, IsoType.NUMERIC, 8);
			isoMessage.setValue(24, request.getFileUpdateCode(), IsoType.NUMERIC, 3);// Function code
			isoMessage.setValue(32, acqInstitutionCode, IsoType.LLVAR, 0);
			isoMessage.setValue(34, request.getPan(), IsoType.LLVAR, 0);
			isoMessage.setValue(37, request.getRrn(), IsoType.NUMERIC, 12);
			isoMessage.setValue(41, StringUtils.leftPad(authReq.getTerminalID(), 16, '0'), IsoType.ALPHA, 16);
			isoMessage.setValue(42, cardAcceptorId, IsoType.ALPHA, 15);
			isoMessage.setValue(43, authReq.getMerchantName(), IsoType.LLVAR, 0);
			isoMessage.setValue(49, accountCurrency, IsoType.ALPHA, 3);
			isoMessage.setValue(102, request.getAccountIdentification1(), IsoType.LLVAR, 0);
			isoMessage.setValue(123, "M2P", IsoType.LLLVAR, 0);
			isoMessage.setValue(124, txnOrg, IsoType.LLLVAR, 0);

			isoMessage.setBinaryBitmap(true);

			response = publishIsoSend(request, isoClient, isoMessage, tenant);

			ResponseDetails resCode = returnAuthResponseCode(response.getResponseCode());
			res.setStatus(resCode.getStatus());
			res.setDescription(resCode.getDescription());
			res.setMcc(authRequest.getOffUsIssuerPurchaseRequest().getMCC());
			res.setTxnRefNo(authRequest.getOffUsIssuerPurchaseRequest().getTxnRefNo());

			if (txnType.equals(TransactionTypeConstants.ATM_MINI_STATEMENT)) {
				String bal = fetchBalance(response);
				res.setBalance(getISOMiniStatementResponse(response.getStatement_DE125(), bal));
			}
			else
				res.setBalance(fetchBalance(response));

			authorizationResponse.setOffIssuerPurchaseResult(res);

			logger.info(response.toString());

		} catch (Exception e) {
			e.printStackTrace();
			res.setStatus("91");
			res.setDescription("Exception Occured");
			res.setMcc(authRequest.getOffUsIssuerPurchaseRequest().getMCC());
			res.setTxnRefNo(authRequest.getOffUsIssuerPurchaseRequest().getTxnRefNo());
			authorizationResponse.setOffIssuerPurchaseResult(res);
		}

		commonService.saveIsoAuthResponse(isoMessage, response, kitNo, acqInstitutionCode);

		return authorizationResponse;

	}

	@Override
	public ReversalResponseDto reversalTransaction(ReversalRequestDto reversalRequest) throws NeoException {

		ReversalResponseDto reversalResponse = new ReversalResponseDto();
		ReversalResponseDto.Response res = reversalResponse.getResponse();
		Iso8583 response = null;
		IsoMessage isoMessage = null;

		String kitNo = reversalRequest.getRequest().getProxyCardNo();
		String acqInstitutionCode = reversalRequest.getIsoRequest().getAcquiringInstitutionCode();

		try {

			String tenant = reversalRequest.getTenant();

			com.neo.aggregator.dto.ReversalRequestDto.Request reversalReq = reversalRequest.getRequest();

			logger.info("Reversal Transaction for Tenant ::" + tenant);

			Iso8583 request = reversalRequest.getIsoRequest();

			List<AccountIdentification> accountDetails = reversalRequest.getAccountIdentification();

			String accountIdentification1 = null;
			String accountCurrency = null;

			for (AccountIdentification id : accountDetails) {

				if (id.getDefaultAccount()) {

					StringBuffer bf = new StringBuffer();
					bf.append(StringUtils.rightPad(id.getBankId(), 11, "")); // Bank id
					bf.append(StringUtils.rightPad(id.getSolId(), 8, "")); // Sol id
					bf.append(StringUtils.rightPad(id.getAccountNo(), 19, "")); // Account No
					accountIdentification1 = bf.toString();
					accountCurrency = id.getTransactionCurrency();

				}
			}

			request.setMti("1420");
			request.setFileUpdateCode("400");
			request.setTransactionCurrencyCode(reversalRequest.getTransactionCurrencyName());
			request.setAccountIdentification1(accountIdentification1);

			String txnType = reversalReq.getTransactionType();

			String txnOrg = fetchTransactionOriginReversal(txnType);
			String processingCode = fetchProcessingCodeReversal(txnType);
			String cardAccptorId = returnCardAcceptorId(reversalReq.getChannel(), reversalReq.getNetwork());
			String dateTimeLocal = DateUtilityFunction.getISOdateToNewFormat(request.getDateTimeTransaction(),
					"MMddHHmmss", "yyyyMMddHHmmss");

			String dateTime = dateTimeLocal.substring(0, 8);

			String orginalTxnId = null;

			String rrn = StringUtils.leftPad(request.getRrn(), 12, '0');
			String caTerminalId = StringUtils.leftPad(reversalReq.getTerminalID(), 16, '0');

			logger.info("Fetching Original Txn for :: MTI :" + "1200" + " Processing Code :" + processingCode
					+ " Pan:"
					+ kitNo + " RRN :" + rrn + " CardAcceptorTerminalId :"
					+ caTerminalId);

			IsoAuthorizationMessage model = isoMessageRepo.fetchOriginalTxn("1200", kitNo, processingCode, rrn,
					caTerminalId);

			if (model != null) {
				
				orginalTxnId = model.getMti() + model.getStan() + model.getTimeLocalTransaction()
						+ String.format("%02d", model.getAcquiringInstitutionCode().length())
						+ model.getAcquiringInstitutionCode();
			}


			Iso8583Client<IsoMessage> isoClient = clientInit.getPosIssuerClientMap().get(tenant);
			MessageFactory<IsoMessage> isoMessageFactory = isoClient.getIsoMessageFactory();

			isoMessage = isoMessageFactory.getMessageTemplate(Integer.parseInt(request.getMti(), 16));

			isoMessage.setValue(3, processingCode, IsoType.NUMERIC, 6);
			if(reversalReq.getDccAmount()!=null) {
				isoMessage.setValue(4, StringUtils.leftPad(ISOUtil.formatDouble(Double.parseDouble(reversalReq.getDccAmount()),16, '0'), 16, '0'), IsoType.NUMERIC,
						16);
			}else if(request.getCardHolderBillingAmount()!=null){
				isoMessage.setValue(4, StringUtils.leftPad(request.getCardHolderBillingAmount(), 16, '0'), IsoType.NUMERIC,
						16);
			}else {
				isoMessage.setValue(4, StringUtils.leftPad(request.getTransactionAmount(), 16, '0'), IsoType.NUMERIC,
						16);
			}
			isoMessage.setValue(11, request.getRrn(), IsoType.NUMERIC, 12);
			isoMessage.setValue(12, dateTimeLocal, IsoType.NUMERIC, 14);
			isoMessage.setValue(17, dateTime, IsoType.NUMERIC, 8);
			isoMessage.setValue(24, request.getFileUpdateCode(), IsoType.NUMERIC, 3);// Function code
			isoMessage.setValue(32, acqInstitutionCode, IsoType.LLVAR, 0);
			isoMessage.setValue(34, request.getPan(), IsoType.LLVAR, 0);
			isoMessage.setValue(37, request.getRrn(), IsoType.NUMERIC, 12);
			isoMessage.setValue(41, StringUtils.leftPad(reversalReq.getTerminalID(), 16, '0'), IsoType.ALPHA, 16);
			isoMessage.setValue(42, cardAccptorId, IsoType.ALPHA, 15);
			isoMessage.setValue(43, reversalReq.getMerchantName(), IsoType.LLVAR, 0);
			isoMessage.setValue(49, accountCurrency, IsoType.ALPHA, 3);
			isoMessage.setValue(56, orginalTxnId, IsoType.LLVAR, 0);
			isoMessage.setValue(102, request.getAccountIdentification1(), IsoType.LLVAR, 0);
			isoMessage.setValue(123, "M2P", IsoType.LLLVAR, 0);
			isoMessage.setValue(124, txnOrg, IsoType.LLLVAR, 0);

			isoMessage.setBinaryBitmap(true);

			response = publishIsoSend(request, isoClient, isoMessage, tenant);

			ResponseDetails resCode = returnAuthResponseCode(response.getResponseCode());
			res.setStatus(resCode.getStatus());
			res.setDescription(resCode.getDescription());
			res.setTxnRefNo(reversalRequest.getRequest().getTxnRefNo());
			res.setBalance(fetchBalance(response));
			reversalResponse.setResponse(res);

			logger.info(response.toString());

		} catch (Exception e) {
			e.printStackTrace();
			res.setStatus("91");
			res.setDescription("Exception Occured");
			res.setTxnRefNo(reversalRequest.getRequest().getTxnRefNo());
			reversalResponse.setResponse(res);
		}

		commonService.saveIsoAuthResponse(isoMessage, response, kitNo, acqInstitutionCode);

		return reversalResponse;
	}

	private Iso8583 publishIsoSend(Iso8583 request, Iso8583Client<IsoMessage> isoClient, IsoMessage isoMessage,String tenant)
			throws InterruptedException, NeoException {

		isoClient.send(isoMessage);

		Long time = 0L;
		
		Long timeOutConfigured = 5000L;
		
		NeoBusinessCustomField timeoutField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ISO_TIMEOUT_MILLIS, "MASTER");

		if(timeoutField!=null && timeoutField.getFieldValue()!=null) {
			timeOutConfigured = Long.parseLong(timeoutField.getFieldValue());
		}
		
		logger.info("Timeout Configured :: "+timeOutConfigured);

		int respMti = Integer.parseInt(request.getMti()) + 10;

		String uniqueId = StringUtils.leftPad(request.getRrn(), 12, '0')
				+ StringUtils.leftPad(request.getRrn(), 12, '0');

		String requestId = Integer.parseInt(request.getMti()) + uniqueId;

		logger.info("Request id ::" + requestId);

		String responseId = respMti + uniqueId;

		logger.info("Response id ::" + responseId);

		while (true) {

			try {

				Thread.sleep(1000L);
				time += 1000L;

				if (time < timeOutConfigured) {

					if (isoHashMap.containsKey(responseId)) {
						Iso8583 response = isoHashMap.get(responseId);
						isoHashMap.remove(responseId);
						isoHashMap.remove(requestId);

						logger.info("Response Received ::" + response.toString());

						return response;
					}
				} else {
					logger.info("Request Timeout");
					isoHashMap.remove(responseId);
					isoHashMap.remove(requestId);
					Iso8583 response8583 = new Iso8583();
					IsoMessage createdResponse = isoClient.getIsoMessageFactory().createResponse(isoMessage);
					createdResponse.setValue(VisaFieldType.RESPONSE_CODE.getPosition(), "91",
							VisaFieldType.RESPONSE_CODE.getFieldType(), VisaFieldType.RESPONSE_CODE.getLength(),
							"Cp1047");

					try {
						response8583 = isoMessageToIso8583Converter.createIso8583Object(createdResponse);
					} catch (Exception e) {
						logger.info("Exception when converting response");
					}
					logger.info("Response Sending for Timeout ::" + response8583.toString());
					return response8583;
				}

			} catch (Exception e) {
				e.printStackTrace();
				isoHashMap.remove(responseId);
				isoHashMap.remove(requestId);
				throw new NeoException();
			}
		}
	}

	private String fetchTransactionOrigin(String txnType) {

		switch (txnType) {
		case TransactionTypeConstants.ATM:
			return "ATM";
		case TransactionTypeConstants.POS:
			return "POS";
		case TransactionTypeConstants.ECOM:
			return "ECM";
		case TransactionTypeConstants.CASH_AT_POS:
			return "POS";
		case TransactionTypeConstants.MICRO_ATM:
			return "ATM";
		case TransactionTypeConstants.QUASI_CASH:
			return "POS";
		case TransactionTypeConstants.LOAD_ONLINE:
			return "POS";
		case TransactionTypeConstants.SERVICE_CREATION:
			return "POS";
		case TransactionTypeConstants.ATM_BALANCE_ENQUIRY:
			return "ATM";
		case TransactionTypeConstants.PURCHASE_CASH_BACK:
			return "POS";

		default:
			return null;
		
		}
	}

	private String fetchTransactionOriginReversal(String txnType) {

		switch (txnType) {
		case TransactionTypeConstants.ATM_REVERSAL:
			return "ATM";
		case TransactionTypeConstants.POS_REVERSAL:
			return "POS";
		case TransactionTypeConstants.ECOM_REVERSAL:
			return "ECM";
		case TransactionTypeConstants.CASH_AT_POS_REVERSAL:
			return "POS";
		case TransactionTypeConstants.MICRO_ATM_REVERSAL:
			return "ATM";
		case TransactionTypeConstants.QUASI_CASH:
			return "POS";
		case TransactionTypeConstants.LOAD_ONLINE_REVERSAL:
			return "POS";
		case TransactionTypeConstants.SERVICE_CREATION:
			return "POS";
		case TransactionTypeConstants.ATM_BALANCE_ENQUIRY:
			return "ATM";
		case TransactionTypeConstants.PURCHASE_CASH_BACK:
			return "POS";
		case TransactionTypeConstants.ATM_MINI_STATEMENT:
			return "ATM";

		default:
			return null;

		}
	}

	private String fetchProcessingCodeReversal(String txnType) {

		switch (txnType) {
		case TransactionTypeConstants.ATM_REVERSAL:
			return "010000";
		case TransactionTypeConstants.POS_REVERSAL:
			return "000000";
		case TransactionTypeConstants.ECOM_REVERSAL:
			return "000000";
		case TransactionTypeConstants.CASH_AT_POS_REVERSAL:
			return "001000";
		case TransactionTypeConstants.MICRO_ATM_REVERSAL:
			return "010000";
		case TransactionTypeConstants.QUASI_CASH:
			return "000000";
		case TransactionTypeConstants.LOAD_ONLINE_REVERSAL:
			return "000000";
		case TransactionTypeConstants.SERVICE_CREATION:
			return "000000";
		case TransactionTypeConstants.ATM_BALANCE_ENQUIRY:
			return "310000";
		case TransactionTypeConstants.PURCHASE_CASH_BACK:
			return "090000";
		case TransactionTypeConstants.ATM_MINI_STATEMENT:
			return "380000";

		default:
			return null;

		}
	}

	private String fetchProcessingCode(String txnType) {

		switch (txnType) {
		case TransactionTypeConstants.ATM:
			return "010000";
		case TransactionTypeConstants.POS:
			return "000000";
		case TransactionTypeConstants.ECOM:
			return "000000";
		case TransactionTypeConstants.CASH_AT_POS:
			return "001000";
		case TransactionTypeConstants.MICRO_ATM:
			return "010000";
		case TransactionTypeConstants.QUASI_CASH:
			return "000000";
		case TransactionTypeConstants.LOAD_ONLINE:
			return "000000";
		case TransactionTypeConstants.SERVICE_CREATION:
			return "000000";
		case TransactionTypeConstants.ATM_BALANCE_ENQUIRY:
			return "310000";
		case TransactionTypeConstants.PURCHASE_CASH_BACK:
			return "090000";
		case TransactionTypeConstants.ATM_MINI_STATEMENT:
			return "380000";

		default:
			return null;

		}
	}

	private ResponseDetails returnAuthResponseCode(String isoResponseCode) {
		
		ResponseDetails response = new ResponseDetails();
		
		if (StringUtils.isBlank(isoResponseCode)) {
			response.setStatus("06");
			response.setDescription("Transaction Failed");
			return response;
		}
		
		switch (isoResponseCode) {

		case "000":
			response.setStatus("00");
			response.setDescription("Transaction Successful");
			break;
		case "913":
			response.setStatus("06");
			response.setDescription("Duplicate Transaction Request");
			break;
		case "116":
			response.setStatus("51");
			response.setDescription("Insufficient Balance");
			break;
		case "91":
			response.setStatus("91");
			response.setDescription("Timeout");
			break;
		case "902":
			response.setStatus("06");
			response.setDescription("Invalid Transaction");
			break;
		default:
			response.setStatus("06");
			response.setDescription("Unknown Transaction Error code ::" + isoResponseCode);
			break;
		}
		
		return response;

	}

	private String returnCardAcceptorId(String channel, String network) {

		String netName = null;

		switch (network) {

		case "VISA":
			netName = "VISA";
			break;
		case "MASTER":
			netName = "MAST";
			break;
		case "RUPAY":
			netName = "OURS";
			break;
		}

		StringBuffer sb = new StringBuffer();
		sb.append("M2P");

		if (channel.equalsIgnoreCase("ATM")) {
			sb.append("ATM");
			sb.append(netName);
			sb.append("ISS");
		} else if (channel.equalsIgnoreCase("POS")) {
			sb.append("POS");
			sb.append(netName);
			sb.append("ISS");
		} else if (channel.equalsIgnoreCase("ECOM")) {
			sb.append("ECM");
			sb.append(netName);
			sb.append("ISS");
		}

		return sb.toString();

	}

	private String fetchBalance(Iso8583 response) {

		try {
			if (response.getAdditionalData48() != null)

			{
				String[] arr = response.getAdditionalData48().split("\\+");

				if (arr.length >= 1)
					return String.valueOf(Double.parseDouble(arr[1]) / 100);
			}
		} catch (Exception e) {
			logger.info("Exception when fetching balance");
		}

		return null;
	}

	public static String getISOMiniStatementResponse(String ministatement, String bal) {

		logger.info(String.format("bal >>>> %s", bal));
		logger.info(String.format("ministatement >>>> %s", ministatement));


		String result = "";
		String de125 = ministatement;

		List<String> str = Lists.newArrayList(Splitter.fixedLength(87).split(de125));

		List<String> data = new ArrayList<>();

		for (String s : str) {
			s = s.trim().replaceAll(" +", " ");

			if (StringUtils.isNotBlank(s)) {
				data.add(s);
			}

		}

		Double d = Double.parseDouble(bal);
		int balan = d.intValue();
		String balance = StringUtils.leftPad(String.valueOf(balan) + "00", 12, "0");
		logger.info(String.format("Balance >>>> %s", balance));
		data.add(StringUtils.rightPad("Balance " + balance + "+", 35, " "));

		StringBuffer org = new StringBuffer();

		for (String x : data) {
			org.append(x);
			org.append(System.lineSeparator());
		}

		String msg = org.toString();

		StringBuffer sb = new StringBuffer();

		sb.append("00100207002003GDN005002");

		String length = String.valueOf(data.size());

		sb.append(StringUtils.leftPad(length, 2, '0'));
		sb.append("006");
		sb.append(msg.length());
		sb.append(msg);

		result = sb.toString();

		logger.info(String.format("result >>>> %s", result));
		return result;
	}

}

