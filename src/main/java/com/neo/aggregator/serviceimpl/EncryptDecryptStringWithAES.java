package com.neo.aggregator.serviceimpl;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;

@Service
public class EncryptDecryptStringWithAES {

	@Autowired
	@Qualifier("createEncryptCypher")
	private Cipher ecipher;

	@Autowired
	@Qualifier("createDecryptCypher")
	private Cipher dcipher;

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(EncryptDecryptStringWithAES.class);

	public String encrypt(String data, String keyString) {

		try {
			return generateHash(Base64.encodeBase64String(ecipher.doFinal(data.getBytes())), keyString);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String testEncrypt(String data, String keyString) {
		Cipher enccipher = null;
		try {
			Key key = new SecretKeySpec("7C0B8344E0742D854BFCF57DE9F24E93".getBytes(), "AES");
			enccipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			enccipher.init(Cipher.ENCRYPT_MODE, key);
			return generateHash(Base64.encodeBase64String(enccipher.doFinal(data.getBytes())), keyString);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
			logger.error("Error while creating decypher instance ", e);
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String generateHash(String passwordToHash, String salt) {

		try {
			SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			PBEKeySpec spec = new PBEKeySpec(passwordToHash.toCharArray(), salt.getBytes(), 5, 32);
			SecretKey key = f.generateSecret(spec);
			return Base64.encodeBase64String(key.getEncoded());
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			logger.error("Error during hashing ", e);
		}
		return null;
	}

}
