package com.neo.aggregator.serviceimpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.neo.aggregator.dao.NeoBusinessCustomFieldDao;
import com.neo.aggregator.model.NeoBusinessCustomField;
import com.neo.aggregator.service.NeoBusinessCustomFieldService;

@Service
public class NeoBusinessCustomFieldServiceImpl implements NeoBusinessCustomFieldService {

	private Logger logger = LoggerFactory.getLogger(NeoBusinessCustomFieldServiceImpl.class);

	@Autowired
	private NeoBusinessCustomFieldDao customFieldDao;

	@Override
	@Cacheable(value = "findNeoBusinessCustomFieldByFieldName", key = "T(java.util.Objects).hash(#fieldName, #tenant)")
	public NeoBusinessCustomField findByFieldNameAndTenant(String fieldName, String tenant) {
		NeoBusinessCustomField config = customFieldDao.findByFiledNameAndTenant(fieldName, tenant);
		if (config != null) {
			return config;
		} else {
			logger.info("Field '" + fieldName + "' not found in tenant : " + tenant);
		}
		return null;
	}

}