package com.neo.aggregator.enums;

public enum SpecialDateType {
	DOB, MARRIAGE, NRE_BECOMING
}