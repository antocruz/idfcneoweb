package com.neo.aggregator.enums;

public enum IncomeCategory {
	SALARIED, BUSINESS, AGRICULTURE, OTHERS
}