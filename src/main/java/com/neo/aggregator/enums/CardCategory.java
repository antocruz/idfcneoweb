package com.neo.aggregator.enums;

public enum CardCategory {
	PREPAID, FOREX, CREDIT, DEBIT, MEAL, FUEL, GIFT,WALLET
}