package com.neo.aggregator.enums;

public enum CardUseType {
	PHYSICAL, VIRTUAL, WALLET
}
