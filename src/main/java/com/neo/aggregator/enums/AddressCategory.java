package com.neo.aggregator.enums;

public enum AddressCategory {
	PERMANENT, DELIVERY, COMMUNICATION, NRERELATIVE
}