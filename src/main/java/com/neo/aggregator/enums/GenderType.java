package com.neo.aggregator.enums;

public enum GenderType {

	MALE, FEMALE, OTHER, M, F, T
}
