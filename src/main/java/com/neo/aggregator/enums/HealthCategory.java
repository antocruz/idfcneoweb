package com.neo.aggregator.enums;

public enum HealthCategory {
	GOOD, AVERAGE, BAD
}
