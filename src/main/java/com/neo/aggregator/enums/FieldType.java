package com.neo.aggregator.enums;

import com.neo.aggregator.iso8583.IsoType;

public enum FieldType {

	PAN("PAN", 2, IsoType.LLVAR, true, 0), PROCESSING_CODE("PROCESSING_CODE", 3, IsoType.NUMERIC, true, 6),
	TRANSACTION_AMOUNT("TRANSACTION_AMOUNT", 4, IsoType.NUMERIC, true, 12),
	SETTLEMENT_AMOUNT("SETTLEMENT_AMOUNT", 5, IsoType.NUMERIC, false, 12),
	CARDHOLDER_BILLING_AMOUNT("CARDHOLDER_BILLING_AMOUNT", 6, IsoType.NUMERIC, false, 12),
	TRANSMISSION_DATE_TIME("TRANSMISSION_DATE_TIME", 7, IsoType.NUMERIC, true, 10),
	SETTLMENT_CONVERSION_RATE("SETTLMENT_CONVERSION_RATE", 9, IsoType.NUMERIC, false, 8),
	CARDHOLDER_SETTLMENT_CONVERSION_RATE("CARDHOLDER_SETTLMENT_CONVERSION_RATE", 10, IsoType.NUMERIC, false, 8),
	STAN("SYSTEM_TRACE_AUDIT_NUMBER", 11, IsoType.NUMERIC, true, 6),
	TIME_LOCAL_TXN("TIME_LOCAL_TXN", 12, IsoType.NUMERIC, true, 6),
	DATE_LOCAL_TXN("DATE_LOCAL_TXN", 13, IsoType.NUMERIC, true, 4),
	EXPIRY_DATE("EXPIRY_DATE", 14, IsoType.NUMERIC, false, 4),
	SETTLEMENT_DATE("SETTLEMENT_DATE", 15, IsoType.NUMERIC, false, 4),
	CONVERSION_DATE("CONVERSION_DATE", 16, IsoType.NUMERIC, false, 4), MCC("MCC", 18, IsoType.NUMERIC, true, 4),
	ACQUIRING_COUNRY_CODE("ACQUIRING_COUNRY_CODE", 19, IsoType.NUMERIC, true, 3),
	POS_ENTRY_MODE("POS_ENTRY_MODE", 22, IsoType.BINARY, true, 2),
	CARD_SEQ_NO("CARD_SEQ_NO", 23, IsoType.NUMERIC, false, 3),
	POS_CONDITION_CODE("POS_CONDITION_CODE", 25, IsoType.NUMERIC, true, 2),
	FEES_AMOUNT("FEES_AMOUNT", 28, IsoType.ALPHA, false, 0),
	ACQUIRING_INSTITUTION_CODE("ACQUIRING_INSTITUTION_CODE", 32, IsoType.LLVAR, true, 0),
	FORWARDING_INSTITUTION_CODE("FORWARDING_INSTITUTION_CODE", 33, IsoType.LLVAR, false, 0),
	TRACK_DATA2("TRACK_DATA2", 35, IsoType.LLVAR, false, 0), RRN("RRN", 37, IsoType.ALPHA, true, 12),
	AUTHORIZATION_ID_RESPONSE("AUTHORIZATION_ID_RESPONSE", 38, IsoType.ALPHA, false, 6),
	RESPONSE_CODE("RESPONSE_CODE", 39, IsoType.ALPHA, true, 2),
	SERVICE_CONDITION_CODE("SERVICE_CONDITION_CODE", 40, IsoType.ALPHA, true, 3),
	CARD_ACCEPTOR_TERMINAL_ID("CARD_ACCEPTOR_TERMINAL_ID", 41, IsoType.ALPHA, true, 8),
	CARD_ACCEPTOR_ID("CARD_ACCEPTOR_ID", 42, IsoType.ALPHA, true, 15),
	CARD_ACCEPTOR_NAME("CARD_ACCEPTOR_NAME", 43, IsoType.ALPHA, true, 40),
	ADDITIONAL_RESPONSE_DATA("ADDITIONAL_RESPONSE_DATA", 44, IsoType.LLVAR, false, 0),
	TRACK_DATA1("TRACK_DATA1", 45, IsoType.LLVAR, false, 0),
	ADDITIONAL_DATA_48("ADDITIONAL_DATA_48", 48, IsoType.LLLVAR, false, 0),
	TXN_CURRENCY_CODE("TXN_CURRENCY_CODE", 49, IsoType.NUMERIC, true, 3),
	SETTLEMEMNT_CURRENCY_CODE("SETTLEMEMNT_CURRENCY_CODE", 50, IsoType.NUMERIC, false, 3),
	CARDHOLDER_BILLING_CURRENCY_CODE("CARDHOLDER_BILLING_CURRENCY_CODE", 51, IsoType.NUMERIC, false, 3),

	PIN_DATA("PIN_DATA", 52, IsoType.BINARY, false, 16), CHIP_DATA("CHIP_DATA", 55, IsoType.LLLBIN, false, 0),
	ADDITIONAL_AMOUNT("ADDITIONAL_AMOUNT", 54, IsoType.LLLVAR, false, 0),
	ADVICE_REASON_CODE("ADVICE_REASON_CODE", 60, IsoType.LLLVAR, false, 0),
	VISA_CUSTOM_DATA("VISA_CUSTOM_DATA", 62, IsoType.LLBIN, false, 0),
	BIOMETRIC_DATA("BIOMETRIC_DATA", 62, IsoType.LLLVAR, false, 0),
	VISA_NETWORK_DATA("VISA_NETWORK_DATA", 63, IsoType.LLBIN, false, 0),
	ENCRYPTED_BIOMETRIC_DATA("ENCRYPTED_BIOMETRIC_DATA", 63, IsoType.LLLVAR, false, 0),

	POS_DATA_CODE("POS_DATA_CODE", 61, IsoType.LLLVAR, true, 0),
	NETWORK_MANAGEMENT_INFO_CODE("NETWORK_MANAGEMENT_INFO_CODE", 70, IsoType.NUMERIC, false, 4),
	AUTHENTICATION_FIELD("AUTHENTICATION_FIELD", 124, IsoType.LLBIN, false, 0);

	String description;

	Integer length;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public IsoType getFieldType() {
		return fieldType;
	}

	public void setFieldType(IsoType fieldType) {
		this.fieldType = fieldType;
	}

	public Boolean getIsMandatory() {
		return isMandatory;
	}

	public void setIsMandatory(Boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	Integer position;

	IsoType fieldType;

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	Boolean isMandatory;

	FieldType(String description, Integer position, IsoType fieldType, Boolean isMandatory, Integer length) {
		this.description = description;
		this.position = position;
		this.fieldType = fieldType;
		this.isMandatory = isMandatory;
		this.length = length;
	}
}
