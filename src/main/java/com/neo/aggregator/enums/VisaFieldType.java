package com.neo.aggregator.enums;

import com.neo.aggregator.iso8583.IsoType;

public enum VisaFieldType {

	PAN("PAN", 2, IsoType.LLBIN, true, 0), PROCESSING_CODE("PROCESSING_CODE", 3, IsoType.BINARY, true, 3),
	TRANSACTION_AMOUNT("TRANSACTION_AMOUNT", 4, IsoType.BINARY, true, 6),
	SETTLEMENT_AMOUNT("SETTLEMENT_AMOUNT", 5, IsoType.BINARY, false, 6),
	CARDHOLDER_BILLING_AMOUNT("CARDHOLDER_BILLING_AMOUNT", 6, IsoType.BINARY, false, 6),
	TRANSMISSION_DATE_TIME("TRANSMISSION_DATE_TIME", 7, IsoType.BINARY, true, 5),
	SETTLMENT_CONVERSION_RATE("SETTLMENT_CONVERSION_RATE", 9, IsoType.BINARY, false, 4),
	CARDHOLDER_SETTLMENT_CONVERSION_RATE("CARDHOLDER_SETTLMENT_CONVERSION_RATE", 10, IsoType.BINARY, false, 4),
	STAN("SYSTEM_TRACE_AUDIT_NUMBER", 11, IsoType.BINARY, true, 3),
	TIME_LOCAL_TXN("TIME_LOCAL_TXN", 12, IsoType.BINARY, true, 3),
	DATE_LOCAL_TXN("DATE_LOCAL_TXN", 13, IsoType.BINARY, true, 2),
	EXPIRY_DATE("EXPIRY_DATE", 14, IsoType.BINARY, false, 2),
	SETTLEMENT_DATE("SETTLEMENT_DATE", 15, IsoType.BINARY, false, 2),
	CONVERSION_DATE("CONVERSION_DATE", 16, IsoType.BINARY, false, 2), MCC("MCC", 18, IsoType.BINARY, true, 2),
	ACQUIRING_COUNRY_CODE("ACQUIRING_COUNRY_CODE", 19, IsoType.BINARY, true, 2),
	PAN_EXTENDED_COUNTRY_CODE("PAN_EXTENDED_COUNTRY_CODE", 20, IsoType.BINARY, false, 2),
	POS_ENTRY_MODE("POS_ENTRY_MODE", 22, IsoType.BINARY, true, 2),
	CARD_SEQ_NO("CARD_SEQ_NO", 23, IsoType.BINARY, false, 2),
	POS_CONDITION_CODE("POS_CONDITION_CODE", 25, IsoType.BINARY, true, 1),
	FEES_AMOUNT("FEES_AMOUNT", 28, IsoType.ALPHA, false, 9),
	ACQUIRING_INSTITUTION_CODE("ACQUIRING_INSTITUTION_CODE", 32, IsoType.LLBIN, false, 0),
	FORWARDING_INSTITUTION_CODE("FORWARDING_INSTITUTION_CODE", 33, IsoType.LLBIN, false, 0),
	TRACK_DATA2("TRACK_DATA2", 35, IsoType.LLBIN, false, 0), RRN("RRN", 37, IsoType.ALPHA, true, 12),
	AUTHORIZATION_ID_RESPONSE("AUTHORIZATION_ID_RESPONSE", 38, IsoType.ALPHA, false, 6),
	RESPONSE_CODE("RESPONSE_CODE", 39, IsoType.ALPHA, true, 2),
	SERVICE_CONDITION_CODE("SERVICE_CONDITION_CODE", 40, IsoType.ALPHA, true, 0),
	CARD_ACCEPTOR_TERMINAL_ID("CARD_ACCEPTOR_TERMINAL_ID", 41, IsoType.ALPHA, true, 8),
	CARD_ACCEPTOR_ID("CARD_ACCEPTOR_ID", 42, IsoType.ALPHA, true, 15),
	CARD_ACCEPTOR_NAME("CARD_ACCEPTOR_NAME", 43, IsoType.ALPHA, true, 40),
	ADDITIONAL_RESPONSE_DATA("ADDITIONAL_RESPONSE_DATA", 44, IsoType.LLBIN, false, 0),
	TRACK_DATA1("TRACK_DATA1", 45, IsoType.LLVAR, false, 0),
	ADDITIONAL_DATA_48("ADDITIONAL_DATA_48", 48, IsoType.LLBIN, false, 0),
	TXN_CURRENCY_CODE("TXN_CURRENCY_CODE", 49, IsoType.BINARY, true, 2),
	SETTLEMEMNT_CURRENCY_CODE("SETTLEMEMNT_CURRENCY_CODE", 50, IsoType.BINARY, false, 2),
	CARDHOLDER_BILLING_CURRENCY_CODE("CARDHOLDER_BILLING_CURRENCY_CODE", 51, IsoType.BINARY, false, 2),
	PIN_DATA("PIN_DATA", 52, IsoType.BINARY, false, 8), CHIP_DATA("CHIP_DATA", 55, IsoType.LLBIN, false, 0),
	ADDITIONAL_AMOUNT("ADDITIONAL_AMOUNT", 54, IsoType.LLBIN, false, 0),
	CUSTOMER_RELATED_DATA("CUSTOMER_RELATED_DATA", 56, IsoType.LLBIN, false, 0),
	POS_GEO_DATA("POS_GEO_DATA", 59, IsoType.LLBIN, false, 0),
	ADVICE_REASON_CODE("ADVICE_REASON_CODE", 60, IsoType.LLBIN, false, 0),
	ADDITIONAL_POS_INFO("ADDITIONAL_POS_INFO", 60, IsoType.LLBIN, false, 0),
	VISA_CUSTOM_DATA("VISA_CUSTOM_DATA", 62, IsoType.LLBIN, false, 0),
	BIOMETRIC_DATA("BIOMETRIC_DATA", 62, IsoType.LLBIN, false, 0),
	VISA_NETWORK_DATA("VISA_NETWORK_DATA", 63, IsoType.LLBIN, false, 0),
	ENCRYPTED_BIOMETRIC_DATA("ENCRYPTED_BIOMETRIC_DATA", 63, IsoType.LLBIN, false, 0),
	POS_DATA_CODE("POS_DATA_CODE", 61, IsoType.LLLVAR, true, 0),
	NETWORK_MANAGEMENT_INFO_CODE("NETWORK_MANAGEMENT_INFO_CODE", 70, IsoType.BINARY, false, 2),
	DATE_ACTION("DATE_ACTION", 73, IsoType.BINARY, false, 3),
	ORIGINAL_DATA_ELEMENT_FIELD("ORIGINAL_DATA_ELEMENT_FIELD", 90, IsoType.BINARY, false, 21),
	REPLACEMENT_AMOUNT_FIELD("REPLACEMENT_AMOUNT_FIELD", 95, IsoType.ALPHA, false, 42),
	VISA_FILE_NAME("VISA_FILE_NAME", 101, IsoType.LLBIN, false, 0),
	VISA_FILE_UPDATE_CODE("VISA_FILE_UPDATE_CODE", 91, IsoType.ALPHA, false, 1),
	VISA_TRANSACTION_DESCRIPTION_FIELD("VISA_TRANSACTION_DESCRIPTION_FIELD", 104, IsoType.LLBIN, false, 0),
	ISSUING_INSTITUTION_CODE("ISSUING_INSTITUTION_CODE", 121, IsoType.LLBIN, false, 0),
	VISA_VERIFICATION_DATA_FIELD("VISA_VERIFICATION_DATA_FIELD", 123, IsoType.LLBIN, false, 0),
	AUTHENTICATION_FIELD("AUTHENTICATION_FIELD", 124, IsoType.LLBIN, false, 0),
	VISA_SUPPORTING_INFO_FIELD("VISA_SUPPORTING_INFO_FIELD", 125, IsoType.LLBIN, false, 0),
	VISA_PRIVATE_MERCHANT_FIELD("VISA_PRIVATE_MERCHANT_FIELD", 126, IsoType.LLBIN, false, 0),
	VISA_FILE_RECORDS_ACTION("VISA_FILE_RECORDS_ACTION", 127, IsoType.LLBIN, false, 0);

	String description;

	Integer length;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public IsoType getFieldType() {
		return fieldType;
	}

	public void setFieldType(IsoType fieldType) {
		this.fieldType = fieldType;
	}

	public Boolean getIsMandatory() {
		return isMandatory;
	}

	public void setIsMandatory(Boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	Integer position;

	IsoType fieldType;

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	Boolean isMandatory;

	VisaFieldType(String description, Integer position, IsoType fieldType, Boolean isMandatory, Integer length) {
		this.description = description;
		this.position = position;
		this.fieldType = fieldType;
		this.isMandatory = isMandatory;
		this.length = length;
	}
}
