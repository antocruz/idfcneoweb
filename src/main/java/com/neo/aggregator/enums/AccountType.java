package com.neo.aggregator.enums;

public enum AccountType {
	SAVING, CURRENT, CREDIT, PREPAID, GL, NODAL, VOSTRO, NOSTRO, LOAN, WALLET, TERM_DEPOSIT, RECURRING_DEPOSIT
}
