package com.neo.aggregator.enums;

public enum NpciNetworkMsgType {

	LOGIN("LOGIN", "001"), LOGOFF("LOGOFF", "002"), CUTOFF("CUTOFF", "201"), ECHOTEST("ECHOTEST", "301");

	NpciNetworkMsgType(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private String name;

	private String value;
}
