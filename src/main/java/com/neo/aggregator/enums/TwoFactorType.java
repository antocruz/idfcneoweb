package com.neo.aggregator.enums;

public enum TwoFactorType {
	OTP, PUSH, TOTP, KEY, RECOVERY_CODES
}
