package com.neo.aggregator.enums;

public enum CardRegStatus {
	ACTIVE, UNACTIVATED, LOCKED
}
