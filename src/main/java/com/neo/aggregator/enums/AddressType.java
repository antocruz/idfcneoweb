package com.neo.aggregator.enums;

public enum AddressType {

	PERMANENT, COMMUNICATION, DELIVERY

}
