package com.neo.aggregator.enums;

public enum CardType {

	VIRTUAL, PHYSICAL, ICC_CONTACT, ICC_CONTACTLESS, WALLET, VC4, VC5, VC7, VC8, VC11, VC12, VC15, VC16, VC6,VOUCHER
}

