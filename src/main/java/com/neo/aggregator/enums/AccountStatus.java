package com.neo.aggregator.enums;

public enum AccountStatus {
	ACTIVE, INACTIVE, HOTLISTED, FRAUD, CLOSED
}