package com.neo.aggregator.enums;

public enum MaritalStatusType {

	SINGLE, MARRIED, OTHER

}
