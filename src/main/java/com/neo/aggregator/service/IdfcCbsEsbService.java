package com.neo.aggregator.service;

import com.neo.aggregator.bank.idfc.request.IdfcFundTransferRequestDto;
import com.neo.aggregator.bank.idfc.request.MDMSearchByFieldsRequestDto;
import com.neo.aggregator.bank.idfc.request.MDMSearchGstIndvCorpRequestDto;
import com.neo.aggregator.bank.idfc.request.PosidexGenericDedupeRequestDto;
import com.neo.aggregator.bank.idfc.response.IdfcFundTransferResponseDto;
import com.neo.aggregator.bank.idfc.response.MDMSearchByFieldsSPResponseDto;
import com.neo.aggregator.bank.idfc.response.MDMSearchByGSTCropResponseDto;
import com.neo.aggregator.bank.idfc.response.PosidexGenericDedupeResponseDto;
import com.neo.aggregator.dto.NeoException;

public interface IdfcCbsEsbService {
    IdfcFundTransferResponseDto idfcFundTransferReq(IdfcFundTransferRequestDto idfcFundTransferRequestDto) throws NeoException;
    MDMSearchByFieldsSPResponseDto mdmSearchByFieldsRes(MDMSearchByFieldsRequestDto mdmSearchByFieldsRequestDto) throws NeoException;
    MDMSearchByGSTCropResponseDto mdmSearchGstIndvCorpRes(MDMSearchGstIndvCorpRequestDto mdmSearchGstIndvCorpRequestDto) throws NeoException;
    PosidexGenericDedupeResponseDto posidexGenericDedupeRes(PosidexGenericDedupeRequestDto posidexGenericDedupeRequestDto) throws NeoException;
}
