package com.neo.aggregator.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.SimpleTriggerContext;
import org.springframework.stereotype.Component;

import com.neo.aggregator.constant.NeoCustomFieldConstants;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.NeoBusinessCustomField;
import com.neo.aggregator.serviceimpl.RblEsbServiceImpl;

@Component
@ConditionalOnExpression("${neo.scheduler.enabled:false}")
public class RblSchedulerService {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(RblSchedulerService.class);

	@Autowired
	private NeoBusinessCustomFieldService customFieldService;

	@Autowired
	private RblEsbServiceImpl rblEsbService;

	private static String rblEcollectCron;

	private static String rblVARetryCron;

	@Bean("getRblEcollectCron")
	public String getRblEcollectCron() {

		if (rblEcollectCron == null) {
			NeoBusinessCustomField customField = customFieldService
					.findByFieldNameAndTenant(NeoCustomFieldConstants.CRON_EXPRESSION, "RBLRECON");
			rblEcollectCron = customField != null ? customField.getFieldValue() : "0 0 0 * * *";
		}
		return rblEcollectCron;
	}

	@Scheduled(cron = "#{@getRblEcollectCron}")
	public void runRblEcollectNotify() {
		try {
			logger.info("Running Cron - " + rblEcollectCron + " - runRblEcollectNotify()");
			Date currentDateTime = new Date();

			rblEsbService.rblRecon();

			CronTrigger t = new CronTrigger(rblEcollectCron);
			logger.info("Current Executed Time :: " + currentDateTime);
			logger.info("Next Execution Time :: " + t.nextExecutionTime(new SimpleTriggerContext()));
		} catch (Exception exc) {
			logger.error("Exception Occured :: " + exc.getMessage());
			logger.error("Exception :: ", exc);
		}
	}

	@Bean("getRblVARetryCron")
	public String getRblVARetryCron() {

		if (rblVARetryCron == null) {
			NeoBusinessCustomField customField = customFieldService
					.findByFieldNameAndTenant(NeoCustomFieldConstants.CRON_EXPRESSION, "RBLVARETRY");
			rblVARetryCron = customField != null ? customField.getFieldValue() : "0 0 0 * * *";
		}
		return rblVARetryCron;
	}

	@Scheduled(cron = "#{@getRblVARetryCron}")
	public void runRblVARetry() {
		try {
			logger.info("Running cron - " + rblVARetryCron + " - runRblVARetry()");
			Date currentDateTime = new Date();

			rblEsbService.vaFailureRetry();

			CronTrigger t = new CronTrigger(rblVARetryCron);
			logger.info("Current Executed Time :: " + currentDateTime);
			logger.info("Next Execution Time :: " + t.nextExecutionTime(new SimpleTriggerContext()));

		} catch (Exception exc) {
			logger.error("Exception Occured :: " + exc.getMessage());
			logger.error("Exception :: ", exc);
		}
	}

}
