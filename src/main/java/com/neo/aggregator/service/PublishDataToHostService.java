package com.neo.aggregator.service;

import com.neo.aggregator.utility.Iso8583;

public interface PublishDataToHostService {

	Iso8583 sendPosTransactionAuthorizationData(Iso8583 request);
}
