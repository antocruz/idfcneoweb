package com.neo.aggregator.service;

import org.springframework.http.HttpMethod;

import com.neo.aggregator.dto.LienMarkRequestDto;
import com.neo.aggregator.dto.NeoResponse;

public interface RemoteService {

	public NeoResponse fetchData(Object request, String url, String tenant, HttpMethod method);

	public void asyncCallLienMark(LienMarkRequestDto request,String neoTenant);
}
