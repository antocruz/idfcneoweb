package com.neo.aggregator.service;

import com.neo.aggregator.dto.AuthorizationRequestDto;
import com.neo.aggregator.dto.AuthorizationResponseDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.ReversalRequestDto;
import com.neo.aggregator.dto.ReversalResponseDto;
import com.neo.aggregator.utility.Iso8583;

public interface IsoService {

	Iso8583 authorizeTransaction(Iso8583 request, String tenant) throws NeoException;

	Iso8583 reversalTransaction(Iso8583 request, String tenant) throws NeoException;

	AuthorizationResponseDto authorizeTransaction(AuthorizationRequestDto request) throws NeoException;

	ReversalResponseDto reversalTransaction(ReversalRequestDto request) throws NeoException;

}
