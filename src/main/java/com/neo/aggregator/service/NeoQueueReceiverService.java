package com.neo.aggregator.service;


public interface NeoQueueReceiverService {
	
	void processFundTransferRequest(String message);

}