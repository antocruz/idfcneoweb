package com.neo.aggregator.service;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.neo.aggregator.constant.ProductConstants;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.model.PartnerMasterConfiguration;
import com.neo.aggregator.utility.NeoExceptionBuilder;
import com.neo.aggregator.utility.NeoExceptionConstant;

@Component
public class AggregatorInterfaceService {

	@Autowired
	private PartnerConfigService partnerConfig;

	@Autowired
	@Qualifier("sbmServiceImpl")
	private AggregatorService sbmService;

	@Autowired
	@Qualifier("rblServiceImpl")
	private AggregatorService rblService;
	
	@Autowired
    @Qualifier("equitasServiceImpl")
    private AggregatorService equitasService;

	@Autowired
	@Qualifier("iciciEsbServiceImpl")
	private AggregatorService iciciService;

	@Autowired
	private NeoExceptionBuilder exceptionBuilder;
	
	@Autowired
	@Qualifier("YesbankServiceImpl")
	private AggregatorService yesbankService;

	@Cacheable(value = "findServiceBasedonTenant", key = "T(java.util.Objects).hash(#tenant)")
	public AggregatorService findServiceBasedonTenant(String tenant) throws NeoException {

		if (tenant == null || tenant.isEmpty()) {
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.TENANT_NOT_FOUND, tenant , null, Locale.US, null, null,ProductConstants.GENERIC);
		}

		PartnerMasterConfiguration configModel = partnerConfig.findPartnerMasterConfigByTenant(tenant);

		if (configModel != null && configModel.getBankServiceId() != null) {

			String bankId = configModel.getBankId();

			switch (bankId) {
			case "SBM":
				return sbmService;
			case "RBL":
				return rblService;
			case "EQUITAS":
                return equitasService;
            case "ICICI":
				return iciciService;
            case "YESBANK":
            	return yesbankService;
			}

		} else {
			throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.PRODUCT_CONFIG_NOT_FOUND, tenant , null, Locale.US, null, null,ProductConstants.GENERIC);
		}

		throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.SERVICE_NOT_FOUND, tenant , null, Locale.US, null, null,ProductConstants.GENERIC);
	}

}
