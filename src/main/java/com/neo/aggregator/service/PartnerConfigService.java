package com.neo.aggregator.service;

import com.neo.aggregator.model.MasterConfiguration;
import com.neo.aggregator.model.PartnerMasterConfiguration;
import com.neo.aggregator.model.PartnerServiceConfiguration;

public interface PartnerConfigService {

	PartnerMasterConfiguration findPartnerMasterConfigByTenant(String tenant);
	MasterConfiguration findMasterConfigByPartnerId(String tenant);
	PartnerServiceConfiguration findPartnerServiceByBankIdAndBankServiceId(String bankId, String serviceId);

}