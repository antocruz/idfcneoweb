package com.neo.aggregator.service;

import java.util.concurrent.Future;

import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.utility.Iso8583;

public interface POSServiceProcess {

	Future<Iso8583> processPosRequest(IsoMessage request, IsoMessage response);

	Future<Iso8583> authorizeTransaction(IsoMessage request, IsoMessage response);

	Future<Iso8583> reverseTransaction(IsoMessage request, IsoMessage response);

	Future<Iso8583> adviseTransaction(IsoMessage request, IsoMessage response);

	Future<Iso8583> authorizeTransaction(Iso8583 request, Iso8583 respons);

	Future<Iso8583> serverInitTransaction(Iso8583 request, Iso8583 response);

}
