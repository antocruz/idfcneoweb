package com.neo.aggregator.service;

import com.neo.aggregator.model.NeoBusinessCustomField;

public interface NeoBusinessCustomFieldService {
	
	NeoBusinessCustomField findByFieldNameAndTenant(String fieldName, String tenant);

}