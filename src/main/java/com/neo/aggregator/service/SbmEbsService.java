package com.neo.aggregator.service;

import com.neo.aggregator.dto.AadhaarOtpRequest;
import com.neo.aggregator.dto.AadhaarOtpResponse;
import com.neo.aggregator.dto.AccountCreationRequestDto;
import com.neo.aggregator.dto.AccountCreationResponseDto;
import com.neo.aggregator.dto.BalanceCheckRequest;
import com.neo.aggregator.dto.BalanceCheckResponseDto;
import com.neo.aggregator.dto.FetchAccountStatementRequest;
import com.neo.aggregator.dto.FetchAccountStatemetResponse;
import com.neo.aggregator.dto.FundTransferRequestDto;
import com.neo.aggregator.dto.FundTransferResponseDto;
import com.neo.aggregator.dto.LienMarkRequestDto;
import com.neo.aggregator.dto.LienMarkResponseDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.PanValidationRequest;
import com.neo.aggregator.dto.PanValidationResponse;
import com.neo.aggregator.dto.RegistrationRequestV2Dto;
import com.neo.aggregator.dto.RegistrationResponseDto;
import com.neo.aggregator.dto.SessionLoginDto;
import com.neo.aggregator.dto.TestAPIRequestDto;

public interface SbmEbsService {

	RegistrationResponseDto retailCifCreation(RegistrationRequestV2Dto request) throws NeoException;

	AccountCreationResponseDto savingsAccountCreation(AccountCreationRequestDto request) throws NeoException;

	BalanceCheckResponseDto fetchAccountBalance(BalanceCheckRequest request) throws NeoException;

	FetchAccountStatemetResponse fetchAccountStatement(FetchAccountStatementRequest request) throws NeoException;

	AccountCreationResponseDto termDepositCreation(AccountCreationRequestDto request) throws NeoException;

	AadhaarOtpResponse generateOtpAadhaar(AadhaarOtpRequest request) throws NeoException;

	AadhaarOtpResponse validateOtpAadhaar(AadhaarOtpRequest request) throws NeoException;

	PanValidationResponse validatePan(PanValidationRequest request) throws NeoException;

	SessionLoginDto loginCreation(SessionLoginDto request) throws NeoException;

	FundTransferResponseDto addOutboundPymt(FundTransferRequestDto request) throws NeoException;

	FundTransferResponseDto impsTransfer(FundTransferRequestDto request) throws NeoException;

	FundTransferResponseDto iftTransfer(FundTransferRequestDto request) throws NeoException;

	String testAPI(TestAPIRequestDto res) throws NeoException;

	FundTransferResponseDto directCredit(FundTransferRequestDto request) throws NeoException;

	FundTransferResponseDto directDebit(FundTransferRequestDto request) throws NeoException;

	LienMarkResponseDto addLienMark(LienMarkRequestDto request) throws NeoException;
	
	FetchAccountStatemetResponse fetchMiniStatement(FetchAccountStatementRequest request) throws NeoException;
	
	

}
