package com.neo.aggregator.service;

import com.neo.aggregator.dto.*;
import com.neo.aggregator.dto.yesbank.MerchantOBReq;
import com.neo.aggregator.dto.yesbank.MerchantOBRes;
import com.neo.aggregator.dto.yesbank.NeoPayRequest;
import com.neo.aggregator.dto.yesbank.NeoPayResponse;
import com.neo.aggregator.dto.yesbank.ValidateVpaReq;
import com.neo.aggregator.dto.yesbank.ValidateVpaRes;
import com.quantiguous.services.TransferResponse;

public interface AggregatorService {

	RegistrationResponseDto retailCifCreation(RegistrationRequestV2Dto request) throws NeoException;

	AccountCreationResponseDto savingsAccountCreation(AccountCreationRequestDto request) throws NeoException;

	BalanceCheckResponseDto fetchAccountBalance(BalanceCheckRequest request) throws NeoException;

	FetchAccountStatemetResponse fetchAccountStatement(FetchAccountStatementRequest request) throws NeoException;

	AccountCreationResponseDto termDepositCreation(AccountCreationRequestDto request) throws NeoException;

	AadhaarOtpResponse generateOtpAadhaar(AadhaarOtpRequest request) throws NeoException;

	AadhaarOtpResponse validateOtpAadhaar(AadhaarOtpRequest request) throws NeoException;

	PanValidationResponse validatePan(PanValidationRequest request) throws NeoException;

	RegistrationResponseDto register(RegistrationRequestV2Dto request) throws NeoException;

	RegistrationResponseDto addProduct(RegistrationRequestV2Dto request) throws NeoException;

	FundTransferResponseDto fundTransfer(FundTransferRequestDto request) throws NeoException;

    FundTransferResponseDto paymentStatusCheck(FundTransferRequestDto request) throws NeoException;

    AccountModificationResponseDto savingsAccountModification(AccountModificationRequestDto request)
			throws NeoException;

	AccountClosureResponseDto tdAccountClosure(AccountClosureRequestDto request) throws NeoException;

	AccountInquiryResponseDto tdAccountInquiry(AccountInquiryRequestDto request) throws NeoException;

	KYCBioResponseDto bioEKycOneTimeToken(RegistrationRequestV2Dto request, String eKycRefnum) throws NeoException;

	KYCBioResponseDto eKycBioValidate(KYCBioRequestDto request) throws NeoException;

	KYCBioResponseDto getDemographicData(KYCBioRequestDto request, String kycType) throws NeoException;

	TDAcctCloseTrailResponseDto tdTrailAccountClosure(TdAccountTrailCloseRequestDto request) throws NeoException;
	
	AccountInquiryResponseDto savingsAccountInquiry(AccountInquiryRequestDto request)
			throws NeoException;

	FundTransferResponseDto fundTransferWithOtp(FundTransferRequestDto request) throws NeoException;

	FundTransferResponseDto fundTransferValidateOtp(FundTransferRequestDto request) throws NeoException;

	KYCBioResponseDto aadharXmlInvoke(RegistrationRequestV2Dto request) throws NeoException;

	KYCBioResponseDto validateAadhaarXml(AadhaarOtpRequest request) throws NeoException;
	
	AccountInquiryResponseDto fetchAccountDetails(AccountInquiryRequestDto request)
			throws NeoException;

	CustomerDedupCheckResponseDto dedupCheck(RegistrationRequestDto request) throws NeoException;

	PayLaterResponseDto payLaterAccountDiscovery(PayLaterRequestDto request) throws NeoException;

	PayLaterResponseDto payLaterAcctBalInquiry(PayLaterRequestDto request) throws NeoException;

	PayLaterResponseDto payLaterOtpCreation(PayLaterRequestDto request) throws NeoException;

	PayLaterResponseDto payLaterOtpVerification(PayLaterRequestDto request) throws NeoException;

	PayLaterResponseDto payLaterDebitOrchestration(PayLaterRequestDto request) throws NeoException;

	BeneficiaryManagementResDto beneficiaryAddition(BeneficiaryManagementReqDto request) throws NeoException;
	
	NeoPayResponse upiPay(NeoPayRequest request) throws NeoException;
	
	ValidateVpaRes validateVpa(ValidateVpaReq request) throws NeoException;
	
	MerchantOBRes onboardSubMerchant(MerchantOBReq request) throws NeoException;

}
