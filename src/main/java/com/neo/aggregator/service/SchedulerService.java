package com.neo.aggregator.service;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.SimpleTriggerContext;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.constant.NeoCustomFieldConstants;
import com.neo.aggregator.dao.NeoFundTransferDao;
import com.neo.aggregator.dao.NeoLienMarkReqResDao;
import com.neo.aggregator.dao.SessionManagementRepo;
import com.neo.aggregator.dao.UpiCollectRequestDao;
import com.neo.aggregator.dto.FundTransferRequestDto;
import com.neo.aggregator.dto.FundTransferResponseDto;
import com.neo.aggregator.dto.LienMarkRequestDto;
import com.neo.aggregator.dto.LienMarkResponseDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.NeoNotificationDto;
import com.neo.aggregator.dto.NeoResponse;
import com.neo.aggregator.dto.SessionLoginDto;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.NeoBusinessCustomField;
import com.neo.aggregator.model.NeoFundTransfer;
import com.neo.aggregator.model.NeoLienMarkReqResModel;
import com.neo.aggregator.model.SessionManagement;
import com.neo.aggregator.model.UpiCollectRequest;
import com.neo.aggregator.serviceimpl.IciciEsbServiceImpl;
import com.neo.aggregator.serviceimpl.RblEsbServiceImpl;
import com.neo.aggregator.serviceimpl.SbmEsbServiceImpl;
import com.neo.aggregator.utility.CommonService;
import com.neo.aggregator.utility.TenantContextHolder;

@Component
@ConditionalOnExpression("${neo.scheduler.enabled:false}")
public class SchedulerService {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(SchedulerService.class);

	@Autowired
	private NeoBusinessCustomFieldService customFieldService;

	@Autowired
	private SessionManagementRepo sessionRepo;

	@Autowired
	private RblEsbServiceImpl rblEsbService;

	@Autowired
	private NeoFundTransferDao neoFundTransferDao;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	private CommonService commonService;

	@Autowired
	private ObjectMapper mapper;

	@Value("${neo.agg.core.url}")
	private String coreUrl;

	@Autowired
	private NeoLienMarkReqResDao neoLienMarkReqResDao;

	@Autowired
	private SbmEsbServiceImpl sbmEsbService;

	@Autowired
	private IciciEsbServiceImpl iciciEsbService;

	@Autowired
	private UpiCollectRequestDao upiCollectRequestDao;

	private void triggerSBMLogin() {

		try {
			SessionManagement sessionModel = sessionRepo.findTopByIsExpiredAndBankIdOrderByCreatedDesc(Boolean.FALSE,
					SbmEsbServiceImpl.BANKID);

			if (!sessionRepo.checkSessionValid(sessionModel)) {

				SessionLoginDto dto = new SessionLoginDto();
				dto.setRefreshSession(true);
				dto.setBankId("SBM");
				SessionLoginDto responseDto = sbmEsbService.loginCreation(dto);

				if (sessionModel != null && responseDto != null && "00".equals(responseDto.getResponseCode())) {
					sessionModel.setIsExpired(Boolean.TRUE);
					sessionRepo.save(sessionModel);
				}
			}
		} catch (NeoException e) {
			logger.info("Session Trigger login Exception ::" + e.getDisplayMessage());
		}
	}

	@Scheduled(fixedDelay = 3600000, initialDelay = 1000)
	public void runSessionManagement() {
		logger.info("Scheduler Service Running for ::runSessionManagement()");
		triggerSBMLogin();
	}

	@Scheduled(cron = "0 0 0 * * * ")
	public void runSessionManagementMidNight() {

		logger.info("Scheduler Service Running for ::runSessionManagementMidNight()");

		CronTrigger t = new CronTrigger("0 0 0 * * * ");
		TriggerContext tc = new SimpleTriggerContext();
		logger.info("Current Time ::" + new Date() + " :: Next Execution " + t.nextExecutionTime(tc));
		triggerSBMLogin();
	}

	@Scheduled(cron = "0 0/15 * * * *")
	public void runSessionManagementWithConfig() {

		logger.info("Running cron = 0 0/15 * * * * runSessionManagementWithConfig()");
		triggerSBMLogin();
	}

	@SuppressWarnings("unchecked")
	@Scheduled(cron = "0 0 */2 * * * ")
	public void runRblFundTrfStatusCheck() {

		logger.info("Scheduler Service Running for :: RBL runFundTrfStatusCheck() ");

		List<NeoFundTransfer> neoFundTransfer = neoFundTransferDao.fetchRblPendingTxns();

		if (neoFundTransfer.size() > 0) {
			neoFundTransfer.forEach(f -> {
				FundTransferRequestDto requestDto = new FundTransferRequestDto();
				FundTransferResponseDto responseDto;

				requestDto.setExternalTransactionId(f.getExternalTransactionId());

				try {

					NeoResponse neoResponse = null;
					TenantContextHolder.setNeoTenant(f.getTenant());
					responseDto = rblEsbService.paymentStatusCheck(requestDto);
					String txnType = "";
					String description = "";
					String amount = "0";

					txnType = f.getTransactionType().equalsIgnoreCase("FT") ? "IFT" : f.getTransactionType();

					if (responseDto != null) {
						if (!(responseDto.getStatus().equalsIgnoreCase("initated")
								|| responseDto.getStatus().equalsIgnoreCase("on hold")
								|| responseDto.getStatus().equalsIgnoreCase("in progress"))) {

							if (responseDto.getStatus().equalsIgnoreCase("failed")
									|| responseDto.getStatus().equalsIgnoreCase("failure")) {
								f.setTransactionStatus("FAILED");
								txnType += "_DEBIT";
								String fetchTxnUrl = coreUrl + "txn-manager/fetch/" + f.getExternalTransactionId();
								NeoResponse txn = postToCore(fetchTxnUrl, null, f.getTenant(), HttpMethod.GET);

								if (txn != null && txn.getResult() != null) {
									Map<String, String> txnMap = mapper
											.readValue(mapper.writeValueAsString(txn.getResult()), Map.class);
									if (txnMap.containsKey("transaction")) {
										Map<String, String> transaction = mapper.readValue(
												mapper.writeValueAsString(txnMap.get("transaction")), Map.class);
										amount = transaction.get("amount");

										String url = coreUrl + "txn-manager/chargeback";
										Map<String, String> requestMap = new HashMap<>();
										requestMap.put("externalTransactionId", f.getExternalTransactionId());
										requestMap.put("transactionType", txnType);
										requestMap.put("toEntityId", f.getEntityId());
										requestMap.put("amount", amount);
										requestMap.put("productId", "GENERAL");

										neoResponse = postToCore(url, requestMap, f.getTenant(), HttpMethod.POST);

									}
								}
								if (neoResponse != null && neoResponse.getResult() != null) {
									Map<String, String> coreResp = mapper
											.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
											.readValue(mapper.writeValueAsString(neoResponse.getResult()), Map.class);

									if (coreResp != null && coreResp.containsKey("txId")) {
										f.setTransactionStatus("REVERSED");
										f.setBankTransactionId(responseDto.getBankReferenceNo());
										description = "Transaction Failed and refund credited to customer wallet.";
									}

								}
							} else if (responseDto.getStatus().equalsIgnoreCase("success")) {
								f.setTransactionStatus("SUCCESS");
								f.setBankTransactionId(responseDto.getBankReferenceNo());
								description = "Transaction Successful!";
							}

							NeoBusinessCustomField businessCustomField = customFieldService
									.findByFieldNameAndTenant(NeoCustomFieldConstants.FT_STATUS_NOTIFY, f.getTenant());
							if (businessCustomField != null && businessCustomField.getFieldValue() != null
									&& "Y".equalsIgnoreCase(businessCustomField.getFieldValue())) {
								NeoNotificationDto notificationDto = new NeoNotificationDto();
								notificationDto.setBusiness(f.getTenant());
								notificationDto.setTxnStatus(f.getTransactionStatus());
								notificationDto.setAmount(f.getTransactionAmount());
								notificationDto.setUtr(f.getBankTransactionId());
								notificationDto.setTransactionType(f.getTransactionType());
								notificationDto.setEntityId(f.getEntityId());
								notificationDto.setDescription(description);
								notificationDto.setExtTxnId(f.getExternalTransactionId());

								commonService.notifyFundTrfStatus(notificationDto);
							}
						}

						neoFundTransferDao.save(f);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		}
	}

	private NeoResponse postToCore(String coreURL, Map<String, String> map, String tenant, HttpMethod method) {

		NeoResponse coreResp;
		ResponseEntity<String> resp;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("TENANT", tenant);

			ObjectMapper obj = new ObjectMapper();
			HttpEntity<String> entity = null;
			try {

				String payload = "";
				if (map != null)
					payload = obj.writeValueAsString(map);

				logger.info("Core call :: " + coreURL + " Payload :: " + payload);
				entity = new HttpEntity<>(payload, headers);
			} catch (JsonProcessingException e) {
				logger.info("Error : ", e);
			}

			resp = restTemplate.exchange(coreURL, method, entity, String.class);

		} catch (HttpStatusCodeException e) {
			resp = ResponseEntity.status(e.getRawStatusCode()).headers(e.getResponseHeaders())
					.body(e.getResponseBodyAsString());
		}

		logger.info("Core Response :: " + resp.getBody());

		try {
			coreResp = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
					.readValue(resp.getBody(), NeoResponse.class);
			logger.info("Core Response Mapping Success");
		} catch (Exception e) {
			logger.info("Core Response Mapping failed");
			e.printStackTrace();
			return new NeoResponse("false", null, null);
		}

		return coreResp;
	}

	@Scheduled(cron = "0 0 6-21 * * *")
	public void runAutoLienMarkRequest() {

		logger.info("Scheduler Service Running for ::runAutoLienMarkRequest()");

		LocalDate nowdate = LocalDate.now();

		String previousDateStr = findPrevDay(nowdate).toString();

		List<NeoLienMarkReqResModel> failedLienMarkList = neoLienMarkReqResDao.findByStatusAndDateRange("FAILURE",
				previousDateStr, nowdate.toString());

		if (failedLienMarkList == null || failedLienMarkList.isEmpty()) {
			return;
		}

		failedLienMarkList = failedLienMarkList.stream().filter(trail -> (trail.getRetryCount() < 4))
				.collect(Collectors.toList());

		for (NeoLienMarkReqResModel failedLienMark : failedLienMarkList) {

			logger.info("Lien Mark Trigger for account id ::" + failedLienMark.getAccountId());

			LienMarkRequestDto markLien = new LienMarkRequestDto();
			markLien.setEntityId(failedLienMark.getEntityId());
			markLien.setAccountCurrency("INR");

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, 12);
			Date endDate = cal.getTime();

			markLien.setAccountId(failedLienMark.getAccountId());
			markLien.setStartDate(new Date());
			markLien.setEndDate(endDate);
			markLien.setReasonCode(failedLienMark.getLienReasonCode());
			markLien.setModuleType(failedLienMark.getLienModuleType());
			markLien.setRemarks("Lien mark add");
			markLien.setAmount(failedLienMark.getAmount());

			try {
				LienMarkResponseDto response = sbmEsbService.addLienMark(markLien);

				if (response.getStatus() != null && response.getStatus().equals("SUCCESS")) {
					failedLienMark.setStatus("SUCCESS");
					failedLienMark.setRetryCount(failedLienMark.getRetryCount() + 1);
					neoLienMarkReqResDao.save(failedLienMark);
				}
			} catch (NeoException e) {
				failedLienMark.setStatus("FAILURE");
				failedLienMark.setRetryCount(failedLienMark.getRetryCount() + 1);
				neoLienMarkReqResDao.save(failedLienMark);
			}

		}
	}

	@SuppressWarnings("unchecked")
	@Scheduled(cron = "0 0 */1 * * * ")
	public void runIciciFundTrfStatusCheck() {

		logger.info("Scheduler Service Running for :: ICICI runIciciFundTrfStatusCheck() ");

		List<NeoFundTransfer> neoFundTransfer = neoFundTransferDao.fetchIciciPendingTxns();

		neoFundTransfer.forEach(f -> {
			FundTransferRequestDto requestDto = new FundTransferRequestDto();
			FundTransferResponseDto responseDto;

			requestDto.setExternalTransactionId(f.getExternalTransactionId());

			try {

				NeoResponse neoResponse = null;
				TenantContextHolder.setNeoTenant(f.getTenant());
				responseDto = iciciEsbService.paymentStatusCheck(requestDto);
				String txnType = "";
				String description = "";
				String amount = "0";
				String txnStatus = "";

				txnType = f.getTransactionType().equalsIgnoreCase("FT") ? "IFT" : f.getTransactionType();

				if (responseDto != null) {
					if (!(responseDto.getStatus().equalsIgnoreCase("initated")
							|| responseDto.getStatus().equalsIgnoreCase("on hold")
							|| responseDto.getStatus().equalsIgnoreCase("in progress"))) {

						if (responseDto.getStatus().equalsIgnoreCase("failed")
								|| responseDto.getStatus().equalsIgnoreCase("failure")) {
							f.setTransactionStatus("FAILED");
							txnStatus = "PAYMENT_FAILURE";
							txnType += "_DEBIT";
							String fetchTxnUrl = coreUrl + "txn-manager/fetch/" + f.getExternalTransactionId();
							NeoResponse txn = postToCore(fetchTxnUrl, null, f.getTenant(), HttpMethod.GET);

							if (txn != null && txn.getResult() != null) {
								Map<String, String> txnMap = mapper
										.readValue(mapper.writeValueAsString(txn.getResult()), Map.class);
								if (txnMap.containsKey("transaction")) {
									Map<String, String> transaction = mapper
											.readValue(mapper.writeValueAsString(txnMap.get("transaction")), Map.class);
									amount = transaction.get("amount");

									String url = coreUrl + "txn-manager/chargeback";
									Map<String, String> requestMap = new HashMap<>();
									requestMap.put("externalTransactionId", f.getExternalTransactionId());
									requestMap.put("transactionType", txnType);
									requestMap.put("toEntityId", f.getEntityId());
									requestMap.put("amount", amount);
									requestMap.put("productId", "GENERAL");

									neoResponse = postToCore(url, requestMap, f.getTenant(), HttpMethod.POST);

								}
							}
							if (neoResponse != null && neoResponse.getResult() != null) {
								Map<String, String> coreResp = mapper
										.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
										.readValue(mapper.writeValueAsString(neoResponse.getResult()), Map.class);

								if (coreResp != null && coreResp.containsKey("txId")) {
									f.setTransactionStatus("REVERSED");
									f.setBankTransactionId(responseDto.getBankReferenceNo());
									description = "Transaction Failed and refund credited to customer wallet.";
								}

							}
						} else if (responseDto.getStatus().equalsIgnoreCase("success")) {
							f.setTransactionStatus("SUCCESS");
							f.setBankTransactionId(responseDto.getBankReferenceNo());
							txnStatus = "PAYMENT_SUCCESS";
							description = "Transaction Successful!";
						}
						NeoBusinessCustomField businessCustomField = customFieldService
								.findByFieldNameAndTenant(NeoCustomFieldConstants.FT_STATUS_NOTIFY, f.getTenant());
						if (businessCustomField != null && businessCustomField.getFieldValue() != null
								&& "Y".equalsIgnoreCase(businessCustomField.getFieldValue())) {
							NeoNotificationDto notificationDto = new NeoNotificationDto();
							notificationDto.setBusiness(f.getTenant());
							notificationDto.setTxnStatus(txnStatus);
							notificationDto.setAmount(f.getTransactionAmount());
							notificationDto.setUtr(f.getBankTransactionId());
							notificationDto.setTransactionType(f.getTransactionType());
							notificationDto.setEntityId(f.getEntityId());
							notificationDto.setDescription(description);
							notificationDto.setExtTxnId(f.getExternalTransactionId());

							commonService.notifyFundTrfStatus(notificationDto);
						}
					}

					neoFundTransferDao.save(f);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	@Scheduled(cron = "0 0 */1 * * * ")
	public void runIciciUpiRefund() {

		logger.info("Scheduler Service Running for :: Icici Upi Refund ");

		List<UpiCollectRequest> collectRequests = upiCollectRequestDao.fetchTransactionsForRefund();

		if (!collectRequests.isEmpty()) {
			collectRequests.forEach(f -> {
				try {
					Boolean response = iciciEsbService.upiCollectRefund(f.getTransferUniqueNumber());
					if (response != null) {
						if (Boolean.TRUE.equals(response)) {
							logger.info("Refunded Successfully!!");
						} else {
							logger.info("Refunded Failure!!");
						}
					}
				} catch (Exception e) {
					logger.info(("Exception Occurred :: " + e.getMessage()));
					e.printStackTrace();
				}
			});
		}
	}

	private LocalDate findPrevDay(LocalDate localdate) {
		return localdate.minusDays(1);
	}

}
