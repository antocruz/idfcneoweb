package com.neo.aggregator.service;

import com.neo.aggregator.enums.NpciNetworkMsgType;
import com.neo.aggregator.utility.Iso8583;

public interface PosIssuerService {

	String sendOctIssuerSignMsg(NpciNetworkMsgType networkMsgType, String tenant);

	String sendClientIssuerSignMsg(NpciNetworkMsgType networkMsgType, String tenant);

	Iso8583 externalOutAuthorize(Iso8583 request, String tenant);

	Iso8583 externalInAuthorize(Iso8583 request, String tenant);
}
