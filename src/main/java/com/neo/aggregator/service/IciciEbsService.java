package com.neo.aggregator.service;

import com.neo.aggregator.bank.icici.request.CIBRegistrationRequest;
import com.neo.aggregator.bank.icici.response.CIBRegistrationResponse;
import com.neo.aggregator.dto.ImpsTransactionRequestDto;
import com.neo.aggregator.dto.ImpsTransactionResponseDto;
import com.neo.aggregator.dto.NeoException;

public interface IciciEbsService {
	CIBRegistrationResponse cibRegistration(CIBRegistrationRequest request) throws NeoException;
	ImpsTransactionResponseDto internalFundTransfer(ImpsTransactionRequestDto request) throws NeoException;
	Boolean upiCollectRefund(String request) throws NeoException;
}
