package com.neo.aggregator.service;

import com.neo.aggregator.bank.rbl.response.KycAadhaarXmlResponseDto;
import com.neo.aggregator.dto.CustomerGetDetailsRequestDto;
import com.neo.aggregator.dto.CustomerGetDetailsResponseDto;
import com.neo.aggregator.dto.KYCBioRequestDto;
import com.neo.aggregator.dto.KYCBioResponseDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.RegistrationRequestV2Dto;
import com.neo.aggregator.dto.RegistrationResponseDto;

public interface RblEbsService {
	KycAadhaarXmlResponseDto okycDataPush(RegistrationRequestV2Dto request) throws NeoException;

	RegistrationResponseDto productRegistration(RegistrationRequestV2Dto request, String kycType) throws NeoException;

	CustomerGetDetailsResponseDto getDetails(CustomerGetDetailsRequestDto request) throws NeoException;

	KYCBioResponseDto fetchDemographicData(KYCBioRequestDto request) throws NeoException;

}
