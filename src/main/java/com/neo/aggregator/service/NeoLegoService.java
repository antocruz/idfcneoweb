package com.neo.aggregator.service;

import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.RegistrationRequestV2Dto;
import com.neo.aggregator.dto.RegistrationResponseDto;
import com.neo.aggregator.dto.neolego.PanDataDto;

public interface NeoLegoService {

	PanDataDto validatePan(String panNumber) throws NeoException;

	RegistrationResponseDto register(RegistrationRequestV2Dto request) throws NeoException;

	RegistrationResponseDto addProduct(RegistrationRequestV2Dto request) throws NeoException;
	
	RegistrationResponseDto registerWithCore(RegistrationRequestV2Dto request) throws NeoException;

}
