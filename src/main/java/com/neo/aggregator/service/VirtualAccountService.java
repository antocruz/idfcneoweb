package com.neo.aggregator.service;

import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.NeoResponse;
import com.neo.aggregator.dto.ecollect.EcollectRequestDto;
import com.neo.aggregator.model.EcollectRequest;

import java.util.List;

public interface VirtualAccountService {
	
	NeoResponse eCollectValidate(EcollectRequestDto request) throws NeoException;
	
	NeoResponse eCollectNotify(EcollectRequestDto request) throws NeoException;

	List<EcollectRequest> fetchEcollectData(String tenant, EcollectRequestDto request);

}
