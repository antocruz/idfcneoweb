package com.neo.aggregator.commonservice;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neo.aggregator.bank.rbl.request.CustomerKYCOnboardingRequest;
import com.neo.aggregator.bank.rbl.response.CustomerGetDetailsResponse;
import com.neo.aggregator.constant.RblPropertyConstants;
import com.neo.aggregator.dao.ProductRegistrationDao;
import com.neo.aggregator.dto.CustomerGetDetailsResponseDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.RegistrationRequestV2Dto;
import com.neo.aggregator.enums.EmploymentType;
import com.neo.aggregator.enums.GenderType;
import com.neo.aggregator.enums.Industry;
import com.neo.aggregator.model.ProductRegistration;
import com.neo.aggregator.utility.TenantContextHolder;
import com.neo.aggregator.utility.TxnIdGenerator;

@Service
public class RblCommonService {
	
	@Autowired
	private ProductRegistrationDao productRegistrationDao;
	
    public CustomerKYCOnboardingRequest buildCustomerKYCOnboardingRequest(RegistrationRequestV2Dto request,String kycType, 
    		ProductRegistration registration, Boolean isUpdate, Boolean isLog) throws NeoException {
    	
    	String tenant = TenantContextHolder.getNeoTenant();
    	
    	ProductRegistration register = null;
    	
		String mobileNumber = getMobile(request.getCommunicationInfo().get(0).getContactNo());
		
		request.getCommunicationInfo().get(0).setContactNo(mobileNumber);
		
        String customerStatus= getCustomerStatus(request.getCustomerStatus());

        String nationality = request.getNationality() != null ? request.getNationality() : "Indian";

        String gender = getGender(request.getGender());

        String customerType = getCustomerType(request.getEmploymentType());

        String sourceIncome = getSourceIncomeType(request.getEmploymentIndustry());

        String annualIncome = getAnnualIncome(request.getGrossAnnualIncome());
        
        String politicallyExposedPerson = getPoliticallyExposedPerson(request.getPoliticallyExposed());

		SimpleDateFormat sdf = new SimpleDateFormat("MMddyyHHmmss");
		String requestDate = sdf.format(Calendar.getInstance().getTime());

        CustomerKYCOnboardingRequest.RequestHeader header = CustomerKYCOnboardingRequest.RequestHeader.builder()
                .username(request.getBioeKycUserId())
                .password(request.getBioeKycPassword())
                .build();

        if (isUpdate.equals(Boolean.TRUE)) {
            register = registration;
        } else {
            register = new ProductRegistration();
        }
        register.setTenant(tenant);
        register.setMobilenumber(mobileNumber);
        register.setEntityId(request.getEntityId());
        register.setIsLog(isLog);
        register.setTitle(request.getTitle());
        register.setName(request.getFirstName());
        register.setEKycRefNum(request.getEKycRefNum());
        register.setEmailaddress(request.getCommunicationInfo().get(0).getEmailId().trim());
        register.setCardAlias(request.getKitInfo().get(0).getKitNo());
        register.setReqtype(kycType);
        register.setStatus("0");
        register.setGender(gender);
        register.setDob(new SimpleDateFormat("yyyy-MM-dd").format(request.getDateInfo().get(0).getDate()));
        register.setSourceIncomeType(sourceIncome);
        register.setAnnualIncome(annualIncome);
        register.setCustomerStatus(customerStatus);
        register.setCustomerType(customerType);
        register.setEntityType(request.getEntityType().name());
        register.setPoliticallyExposed(politicallyExposedPerson);
        register.setFatcaDecl(request.getFatcaDecl());
        register.setNationality(nationality);
        register.setOccupation(request.getOccupation().replaceAll("[ _@-]*", ""));
        register.setAddressCategory(request.getAddressInfo().get(0).getAddressCategory());
        register.setAddress1(request.getAddressInfo().get(0).getAddress1());
        register.setAddress2(request.getAddressInfo().get(0).getAddress2());
        register.setAddress3(request.getAddressInfo().get(0).getAddress3());
        register.setCity(request.getAddressInfo().get(0).getCity());
        register.setDistrict(request.getAddressInfo().get(0).getDistrict());
        register.setState(request.getAddressInfo().get(0).getState());
        register.setCountry(request.getAddressInfo().get(0).getCountry());
        register.setPincode(request.getAddressInfo().get(0).getPincode());
        
        productRegistrationDao.save(register);

        return CustomerKYCOnboardingRequest.builder()
                .header(header)
                .refno(request.getEKycRefNum())
                .requestdate(requestDate)
                .partnerrefno("")
                .gender(gender)
                .laddress1(request.getAddressInfo().get(0).getAddress1() + "," + request.getAddressInfo().get(0).getAddress2())
                .laddress2(request.getAddressInfo().get(0).getAddress3())
                .lcity(request.getAddressInfo().get(0).getCity())
                .lstate(request.getAddressInfo().get(0).getState())
                .lcountry(request.getAddressInfo().get(0).getCountry())
                .lpincode(request.getAddressInfo().get(0).getPincode())
                .emailaddress(request.getCommunicationInfo().get(0).getEmailId().trim())
                .mobilenumber(request.getCommunicationInfo().get(0).getContactNo())
                .AnnualIncome(annualIncome)
                .CardAlias(request.getKitInfo().get(0).getKitNo())
                .CustomerStatus(customerStatus)
                .CustomerType(customerType)
                .fatcadecl(request.getFatcaDecl())
                .mothermaidenname("")
                .nationality(nationality)
                .occupation(request.getOccupation().replaceAll("[ _@-]*", ""))
                .PoliticallyExposedPerson(politicallyExposedPerson)
                .product(tenant + "PPI")
                .SourceIncomeType(sourceIncome)
                .Reqtype(kycType)
                .Kyctype("")
                .Documenttype("")
                .Documentnumber("")
                .Reserved1("")
                .Reserved2("")
                .Reserved3("")
                .Reserved4("")
                .build();
    }
    
    public CustomerGetDetailsResponseDto buildGetDetailsResponse(CustomerGetDetailsResponse responseOutDto) {
		
		String approvalStatus = getApprovalStatus(responseOutDto.getApprovalstatus());
		
		return CustomerGetDetailsResponseDto.builder()
    			.status(responseOutDto.getStatus())
    			.firstname(responseOutDto.getFirstname())
    			.middlename(responseOutDto.getMiddlename())
    			.lastname(responseOutDto.getLastname())
    			.dateofbirth(responseOutDto.getDateofbirth())
    			.gender(responseOutDto.getGender())
    			.mothermaidenname(responseOutDto.getMothermaidenname())
    			.nationality(responseOutDto.getNationality())
    			.resaddress1(responseOutDto.getResaddress1())
    			.resaddress2(responseOutDto.getResaddress2())
    			.city(responseOutDto.getCity())
    			.state(responseOutDto.getState())
    			.pincode(responseOutDto.getPincode())
    			.rescountry(responseOutDto.getRescountry())
    			.mobilenumber(responseOutDto.getMobilenumber())
    			.emailaddress(responseOutDto.getEmailaddress())
    			.laddress1(responseOutDto.getLaddress1())
    			.laddress2(responseOutDto.getLaddress2())
    			.lcity(responseOutDto.getLcity())
    			.lstate(responseOutDto.getLstate())
    			.lpincode(responseOutDto.getLpincode())
    			.lcountry(responseOutDto.getLcountry())
    			.CustomerStatus(responseOutDto.getCustomerStatus())
    			.CustomerType(responseOutDto.getCustomerType())
    			.SourceIncomeType(responseOutDto.getSourceIncomeType())
    			.AnnualIncome(responseOutDto.getAnnualIncome())
    			.PoliticallyExposedPerson(responseOutDto.getPoliticallyExposedPerson())
    			.customerid(responseOutDto.getCustomerid())
    			.Approvalstatus(approvalStatus)
    			.cardAlias(responseOutDto.getCardAlias())
    			.AccountNumber(responseOutDto.getAccountNumber())
    			.idproofname(responseOutDto.getIdproofname())
    			.idproofnumber(responseOutDto.getIdproofnumber())
    			.Reqtype(responseOutDto.getReqtype())
    			.Reserved1(responseOutDto.getReserved1())
    			.Reserved2(responseOutDto.getReserved2())
    			.Reserved3(responseOutDto.getReserved3())
    			.Reserved4(responseOutDto.getReserved4())
    			.build();
	}
    
    public String getMobile(String mobile) throws NeoException {
    	
    	mobile = mobile.replaceAll(" ", "");
    	
		if (mobile.length() < 10) {
			throw new NeoException("Failed", "Enter correct Mobile Number", null, null, "Product Registration Failed");
		}
		if (mobile.startsWith("+") || mobile.startsWith("+91") || mobile.startsWith("0")
				|| mobile.startsWith("91")) {
			mobile = mobile.substring(mobile.length() - 10);
			if (isValid(mobile)) {
				return mobile;
			} else {
				throw new NeoException("Failed", "Enter correct Mobile Number", null, null,
						"Product Registration Failed");
			}
		}
		return mobile;
	}

	private boolean isValid(String mobileNumber) {
		String regex = "(0/91/+91/+)?[6-9][0-9]{9}";
		if(mobileNumber.matches(regex)) {
			return true;
		}
		return false;
	}

	public String getCustomerStatus(String customerStatus){
        String status="";
        if(customerStatus.equalsIgnoreCase("individual")){
            status=RblPropertyConstants.CUSTOMER_STATUS_INDIVIDUAL;
        }else
        {
            status=RblPropertyConstants.CUSTOMER_STATUS_NONINDIVIDUAL;
        }

        return status;
    }

    public String getGender(GenderType gender) {
        String custGender = "";

        if (gender.equals(GenderType.MALE) || gender.equals(GenderType.M)) {
            custGender = "Male";
        } else {
            custGender = "Female";
        }
        return custGender;
    }

    public String getAnnualIncome(String income) {

        String annualIncome = "6";

        if (income == null) {
            return annualIncome;
        }

        double incomeDbl = Double.parseDouble(income);

        if (incomeDbl < 200000) {
            annualIncome = RblPropertyConstants.ANNUALINCOME_0L_2L;
        } else if (incomeDbl >= 200000 && incomeDbl < 500000) {
            annualIncome = RblPropertyConstants.ANNUALINCOME_2L_5L;
        } else if (incomeDbl >= 500000 && incomeDbl < 1000000) {
            annualIncome = RblPropertyConstants.ANNUALINCOME_5L_10L;
        } else {
            annualIncome = RblPropertyConstants.ANNUALINCOME_ABOVE_10L;
        }

        return annualIncome;
    }

    public String getCustomerType(EmploymentType type) {

        //CUSTOMERTYPE
        String customerType = "";

        if (type == null) {
            return customerType;
        }

        switch (type) {
            case SALARIED:
            case EMPLOYED:
            case PUBLIC_SECTOR_EMPLOYEE:
            case HOUSEWORK:
            case APPRENTICE:
            case RETIRED:
                customerType = RblPropertyConstants.CUSTOMER_TYPE_SALARIED;
                break;
            case SELF_EMPLOYED:
            case ENTREPRENEUR:
            case FREELANCER:
                customerType = RblPropertyConstants.CUSTOMER_TYPE_SELFEMPLOYED;
                break;
            case HOUSEWIFE:
                customerType = RblPropertyConstants.CUSTOMER_TYPE_HOUSEWIFE;
                break;
            case FARMER:
                customerType = RblPropertyConstants.CUSTOMER_TYPE_FARMER;
                break;
            case MINOR:
                customerType = RblPropertyConstants.CUSTOMER_TYPE_MINOR;
                break;
            default:
                customerType=RblPropertyConstants.CUSTOMER_TYPE_SELFEMPLOYED;
                break;
        }

        return customerType;

    }

    public String getSourceIncomeType(Industry type) {

        String sourceIncome = "";

        if (type == null) {
            return sourceIncome;
        }

        switch (type) {
            case DEPENDANT:
                sourceIncome = RblPropertyConstants.SOURCEINCOME_DEPENDANT;
                break;
            case BUSINESS_MANAGEMENT_AND_ADMINISTRATION:
                sourceIncome = RblPropertyConstants.SOURCEINCOME_BUSINESS;
                break;
            case AGRICULTURE_FOOD_NATURAL_RESOURCES:
                sourceIncome = RblPropertyConstants.SOURCEINCOME_AGRICULTURE;
                break;
            case GOVERNMENT_AND_PUBLIC_ADMINISTRATION:
                sourceIncome = RblPropertyConstants.SOURCEINCOME_PUBLIC;
                break;
            default:
                sourceIncome = RblPropertyConstants.SOURCEINCOME_PRIVATE;
                break;
        }

        return sourceIncome;

    }
    
    public String getApprovalStatus(String approvalStatus) {
		
		switch(approvalStatus) {
		case "0":
			return "KYC not done";
		case "1":
			return "KYC uploaded not accepted (In Process)";
		case "3":
			return "KYC rejected";
		case "7":
			return "KYC Accepted";
		case "10":
			return "KYC reuploaded- in process";
		}
		return "Failed";
	}
    
    private String getPoliticallyExposedPerson(String politicallyExposed) {
    	if(politicallyExposed.equals("Y")) {
    		return "1";
    	}
		return "0";
	}
}
