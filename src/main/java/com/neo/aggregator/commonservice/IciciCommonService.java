package com.neo.aggregator.commonservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.bank.icici.core.IciciCoreDto;
import com.neo.aggregator.bank.icici.request.CompositeFundTransferRequest;
import com.neo.aggregator.bank.icici.response.CompositeFundTransferResponse;
import com.neo.aggregator.bank.icici.utils.EncryptionDecryption;
import com.neo.aggregator.bank.icici.utils.UtilityFunctions;
import com.neo.aggregator.constant.IciciPropertyConstants;
import com.neo.aggregator.constant.NeoCustomFieldConstants;
import com.neo.aggregator.dto.FundTransferRequestDto;
import com.neo.aggregator.dto.FundTransferResponseDto;
import com.neo.aggregator.model.NeoBusinessCustomField;
import com.neo.aggregator.service.NeoBusinessCustomFieldService;
import com.neo.aggregator.utility.TenantContextHolder;
import com.neo.aggregator.utility.TxnIdGenerator;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.*;
import java.security.SecureRandom;
import java.text.*;
import java.util.Date;
import java.util.Objects;

@Service
public class IciciCommonService {

    @Autowired
    private NeoBusinessCustomFieldService customFieldService;

    @Autowired
    EncryptionDecryption encryptionDecryption;

    public static final String BAD_REQUEST="Bad request";

    public CompositeFundTransferRequest buildUPIFundTrfRequest(FundTransferRequestDto request) {
        String txnAmount;
        txnAmount = String.format("%.2f", Double.parseDouble(request.getAmount()));

        NeoBusinessCustomField corpIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_ID,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField corpUserField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_USER,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField aggrIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_AGGR_ID,
                TenantContextHolder.getNeoTenant());

        NeoBusinessCustomField channelField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_FT_CHANNEL_CODE, TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField deviceIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_FT_DEVICE_ID, TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField profileIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_FT_PROFILE_ID, TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField preApprovedField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_FT_PREAPPROVED, TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField accProviderField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_FT_ACCOUNT_PROVIDER, TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField upiTxnTypeField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_FT_UPI_TXNTYPE, TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField upiMerchantTypeField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_FT_UPI_MERCHANTTYPE, TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField upiMccField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_FT_UPI_MCC, TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField upiMobileField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_FT_UPI_MOBILE, TenantContextHolder.getNeoTenant());

        String corpId = (corpIdField != null) ? corpIdField.getFieldValue() : "";
        String corpUser = (corpUserField != null) ? corpUserField.getFieldValue() : "";
        String aggrID = (aggrIdField != null) ? aggrIdField.getFieldValue() : "";
        String channelCode = (channelField != null) ? channelField.getFieldValue():IciciPropertyConstants.CHANNEL_CODE;
        String deviceId = (deviceIdField != null) ? deviceIdField.getFieldValue():IciciPropertyConstants.DEVICE_ID;
        String profileId = (profileIdField != null) ? profileIdField.getFieldValue() : IciciPropertyConstants.PROFILE_ID;
        String preApproved = (preApprovedField != null) ? preApprovedField.getFieldValue():IciciPropertyConstants.PREAPPROVED;
        String accProvider = (accProviderField != null) ? accProviderField.getFieldValue():IciciPropertyConstants.ACCOUNT_PROVIDER;
        String txnType = (upiTxnTypeField != null) ? upiTxnTypeField.getFieldValue():"merchantToPersonPay";
        String merchantType = (upiMerchantTypeField != null) ? upiMerchantTypeField.getFieldValue() : "ENTITY";
        String mcc = (upiMccField != null) ? upiMccField.getFieldValue() : "6011";
        String remMobile = (upiMobileField != null) ? upiMobileField.getFieldValue() : request.getRemitterNumber();

        if (remMobile.length() > 10 &&
                remMobile.contains("+")) {
            remMobile = remMobile.substring(3);
        }

        return CompositeFundTransferRequest.builder()
                .deviceId(deviceId).mobile(remMobile)
                .channelCode(channelCode).profileId(profileId)
                .seqNo(request.getExternalTransactionId()).useDefaultAcc("D")
                .payeeVA(request.getToAccountNo()).payerVA(request.getFromAccountNo())
                .amount(txnAmount).preApproved(preApproved)
                .defaultDebit("N").defaultCredit("N").txnTypeUpi(txnType)
                .remarks(request.getDescription()).mcc(mcc).payeeNameUpi(request.getBeneficiaryName())
                .merchantType(merchantType).accountProvider(accProvider)
                .crpID(corpId).aggrID(aggrID).userID(corpUser).vpa(request.getToAccountNo())
                .build();
    }

    public CompositeFundTransferRequest buildIMPSFundTrfRequest(FundTransferRequestDto request) {

        String txnAmount;
        txnAmount = String.format("%.2f", Double.parseDouble(request.getAmount()));

        String paymentRef = request.getDescription().substring(0, Math.min(50, request.getDescription().length()));

        Date date = new Date();
        String localDateTime = new SimpleDateFormat("yyyyMMddHHmmss").format(date);

        NeoBusinessCustomField corpIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_ID,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField corpUserField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_USER,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField bcIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_BC_ID,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField passCodeField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_IMPS_PASSCODE,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField retailerCodeField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_FT_RETAILER_CODE,
                TenantContextHolder.getNeoTenant());

        String bcID = (bcIdField != null) ? bcIdField.getFieldValue() : "";
        String passCode = (passCodeField != null) ? passCodeField.getFieldValue() : "";
        String corpId = (corpIdField != null) ? corpIdField.getFieldValue() : "";
        String corpUser = (corpUserField != null) ? corpUserField.getFieldValue() : "";
        String retailerCode = (retailerCodeField != null) ? retailerCodeField.getFieldValue() : IciciPropertyConstants.RETAILER_CODE;

        String remMobile = Objects.toString(request.getBeneficiaryMobile(), request.getRemitterNumber());

        if (remMobile.length() > 10 &&
                remMobile.contains("+")) {
            remMobile = remMobile.substring(3);
        }

        return CompositeFundTransferRequest.builder()
                .localTxnDtTime(localDateTime).beneAccNo(request.getToAccountNo())
                .beneIFSC(request.getBeneficiaryIfsc().toUpperCase()).amount(txnAmount)
                .tranRefNo(request.getExternalTransactionId()).paymentRef(paymentRef)
                .senderName(request.getRemitterName()).mobile(remMobile)
                .retailerCode(retailerCode).passCode(passCode)
                .bcID(bcID).crpId(corpId).crpUsr(corpUser)
                .build();
    }

    public CompositeFundTransferRequest buildNEFTFundTrfRequest(FundTransferRequestDto request) {

        String txnAmount;
        txnAmount = String.format("%.2f", Double.parseDouble(request.getAmount()));

        String transactionType = "";

        if ("IFT".equals(request.getTransactionType())) {
            transactionType = "TPA";
        } else {
            transactionType = "RGS";
        }

        String narration = request.getDescription().substring(0, Math.min(35, request.getDescription().length()));

        NeoBusinessCustomField corpIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_ID,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField corpUserField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_USER,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField aggrIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_AGGR_ID,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField aggrNameField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_AGGR_NAME,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField urnField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_URN,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField workFlowReqField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_FT_WORKFLOW_REQD,
                TenantContextHolder.getNeoTenant());

        String urn = (urnField != null) ? urnField.getFieldValue() : "";
        String corpId = (corpIdField != null) ? corpIdField.getFieldValue() : "";
        String corpUser = (corpUserField != null) ? corpUserField.getFieldValue() : "";
        String aggrName = (aggrNameField != null) ? aggrNameField.getFieldValue() : "";
        String aggrID = (aggrIdField != null) ? aggrIdField.getFieldValue() : "";
        String workFlowReqd = (workFlowReqField != null) ? workFlowReqField.getFieldValue() : "";

        return CompositeFundTransferRequest.builder()
                .tranRefNo(request.getExternalTransactionId()).amount(txnAmount)
                .senderAcctNo(request.getFromAccountNo()).beneAccNo(request.getToAccountNo())
                .beneName(request.getBeneficiaryName()).beneIFSC(request.getBeneficiaryIfsc().toUpperCase())
                .narration1(narration).crpId(corpId)
                .crpUsr(corpUser).aggrIdNeft(aggrID)
                .aggrName(aggrName).urn(urn)
                .txnTypeNeft(transactionType)
                .workflowReqd(workFlowReqd)
                //.benlei("")
                .build();
    }

    public CompositeFundTransferRequest buildRTGSFundTrfRequest(FundTransferRequestDto request) {

        String txnAmount;
        txnAmount = String.format("%.2f", Double.parseDouble(request.getAmount()));

        String transactionType = "";

        if (request.getBeneficiaryIfsc().toUpperCase().contains("ICI")) {
            transactionType = "TPA";
        } else {
            transactionType = "RTG";
        }

        NeoBusinessCustomField corpIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_ID,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField corpUserField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_USER,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField aggrIdField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_AGGR_ID,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField aggrNameField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_AGGR_NAME,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField urnField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_CORP_URN,
                TenantContextHolder.getNeoTenant());
        NeoBusinessCustomField workFlowReqField = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.ICICI_FT_WORKFLOW_REQD,
                TenantContextHolder.getNeoTenant());

        String urn = (urnField != null) ? urnField.getFieldValue() : "";
        String corpId = (corpIdField != null) ? corpIdField.getFieldValue() : "";
        String corpUser = (corpUserField != null) ? corpUserField.getFieldValue() : "";
        String aggrName = (aggrNameField != null) ? aggrNameField.getFieldValue() : "";
        String aggrID = (aggrIdField != null) ? aggrIdField.getFieldValue() : "";
        String workFlowReqd = (workFlowReqField != null) ? workFlowReqField.getFieldValue() : "";
        String txnCurrency = request.getTransactionCurrency()!=null?request.getTransactionCurrency():"INR";

        return CompositeFundTransferRequest.builder()
                .aggrIdRtgs(aggrID).corpIdRtgs(corpId)
                .userIdRtgs(corpUser).urnRtgs(urn)
                .aggrNameRtgs(aggrName).uniqueId(request.getExternalTransactionId())
                .debitAcc(request.getFromAccountNo()).creditAcc(request.getToAccountNo())
                .ifsc(request.getBeneficiaryIfsc().toUpperCase()).amountRtgs(txnAmount)
                .currencyRtgs(txnCurrency).txnTypeRtgs(transactionType)
                .payeeName(request.getBeneficiaryName()).remarksRtgs(request.getDescription())
                .workflowReqd(workFlowReqd)
                //.benlei("")
                .build();
    }

    public FundTransferResponseDto buildFundTransferResponse(CompositeFundTransferResponse response, String txnType) {

        FundTransferResponseDto fundTransferResponse = null;

        if (txnType.equalsIgnoreCase("upi")) {
            fundTransferResponse = buildUPIFundTrfResponse(response);

        } else if (txnType.equalsIgnoreCase("imps")) {
            fundTransferResponse = buildIMPSFundTrfResponse(response);

        } else {
            fundTransferResponse = buildNEFTRTGSFundTrfResponse(response);

        }

        return fundTransferResponse;
    }

    private FundTransferResponseDto buildUPIFundTrfResponse(CompositeFundTransferResponse response) {
        FundTransferResponseDto fundTrfResponse = new FundTransferResponseDto();

        if ("997".equals(response.getErrorCde())) {
            if (response.getDescription().equalsIgnoreCase(BAD_REQUEST)) {
                fundTrfResponse.setStatus(IciciPropertyConstants.FAILURE_MSG);
            } else {
                fundTrfResponse.setStatus(IciciPropertyConstants.INPROGRESS_MSG);
            }
            fundTrfResponse.setDescription(response.getDescription());
            fundTrfResponse.setBankReferenceNo("");
        } else {
            if (response.getSuccess().equals(Boolean.TRUE) &&
                    response.getResponseUpi().equalsIgnoreCase("0")) {
                fundTrfResponse.setStatus(response.getMobileAppData());
            } else if (response.getSuccess().equals(Boolean.TRUE) &&
                    (response.getResponseUpi().equalsIgnoreCase("5") ||
                            response.getResponseUpi().equalsIgnoreCase("9999") ||
                            response.getResponseUpi().equalsIgnoreCase("00XB") ||
                            response.getResponseUpi().equalsIgnoreCase("00XC") ||
                            response.getResponseUpi().equalsIgnoreCase("0U28") ||
                            response.getResponseUpi().equalsIgnoreCase("0L05") ||
                            response.getResponseUpi().equalsIgnoreCase("0L16") ||
                            response.getResponseUpi().equalsIgnoreCase("72") ||
                            response.getResponseUpi().equalsIgnoreCase("73") ||
                            response.getResponseUpi().equalsIgnoreCase("74") ||
                            response.getResponseUpi().equalsIgnoreCase("0U27") ||
                            response.getResponseUpi().equalsIgnoreCase("00XY") ||
                            response.getResponseUpi().equalsIgnoreCase("00BT") ||
                            response.getResponseUpi().equalsIgnoreCase("U70") ||
                            response.getResponseUpi().equalsIgnoreCase("18") ||
                            response.getResponseUpi().equalsIgnoreCase("6") ||
                            response.getResponseUpi().equalsIgnoreCase("U88") ||
                            response.getResponseUpi().equalsIgnoreCase("101") ||
                            response.getResponseUpi().equalsIgnoreCase("94") ||
                            response.getResponseUpi().equalsIgnoreCase("92") ||
                            response.getResponseUpi().equalsIgnoreCase("91"))) {
                fundTrfResponse.setStatus(IciciPropertyConstants.INPROGRESS_MSG);
            } else {
                fundTrfResponse.setStatus(IciciPropertyConstants.FAILURE_MSG);
            }
            fundTrfResponse.setDescription(response.getMessageUpi());
            fundTrfResponse.setBankReferenceNo(response.getBankRRN());
            fundTrfResponse.setExternalTransactionId(response.getSeqNo());
        }
        return fundTrfResponse;
    }

    private FundTransferResponseDto buildIMPSFundTrfResponse(CompositeFundTransferResponse response) {
        FundTransferResponseDto fundTrfResponse = new FundTransferResponseDto();

        if ("997".equals(response.getErrorCde())) {
            if (response.getDescription().equalsIgnoreCase(BAD_REQUEST)) {
                fundTrfResponse.setStatus(IciciPropertyConstants.FAILURE_MSG);
            } else {
                fundTrfResponse.setStatus(IciciPropertyConstants.INPROGRESS_MSG);
            }
            fundTrfResponse.setDescription(response.getDescription());
            fundTrfResponse.setBankReferenceNo("");
        } else {
            if (response.getSuccess().equals(Boolean.TRUE) &&
                    response.getActCode().equalsIgnoreCase("0")) {
                fundTrfResponse.setStatus(IciciPropertyConstants.SUCCESS_MSG);
                fundTrfResponse.setDescription(response.getResponseImps());
                fundTrfResponse.setBankReferenceNo(response.getBankRRN());
            } else if (response.getSuccess().equals(Boolean.TRUE) && (
                    response.getActCode().equalsIgnoreCase("30") ||
                            response.getActCode().equalsIgnoreCase("31") ||
                            response.getActCode().equalsIgnoreCase("11")
                            || response.getActCode().equalsIgnoreCase("32")
                            || response.getActCode().equalsIgnoreCase("33")
                            || response.getActCode().equalsIgnoreCase("63"))) {
                fundTrfResponse.setStatus(IciciPropertyConstants.INPROGRESS_MSG);
                fundTrfResponse.setDescription("Transaction in Progress");
                fundTrfResponse.setBankReferenceNo(response.getBankRRN());
            } else {
                fundTrfResponse.setStatus(IciciPropertyConstants.FAILURE_MSG);
                fundTrfResponse.setDescription(Objects.toString(response.getActCodeDesc(), response.getResponseImps()));
                fundTrfResponse.setBankReferenceNo(response.getBankRRN());
            }
            fundTrfResponse.setExternalTransactionId(response.getTransRefNo());
        }
        return fundTrfResponse;
    }

    private FundTransferResponseDto buildNEFTRTGSFundTrfResponse(CompositeFundTransferResponse response) {
        FundTransferResponseDto fundTrfResponse = new FundTransferResponseDto();
        if ("997".equals(response.getErrorCde())) {
            if (response.getDescription().equalsIgnoreCase(BAD_REQUEST)) {
                fundTrfResponse.setStatus(IciciPropertyConstants.FAILURE_MSG);
            } else {
                fundTrfResponse.setStatus(IciciPropertyConstants.INPROGRESS_MSG);
            }
            fundTrfResponse.setDescription(response.getDescription());
            fundTrfResponse.setBankReferenceNo("");
        } else {
            if ("SUCCESS".equalsIgnoreCase(response.getStatus())) {
                fundTrfResponse.setStatus(IciciPropertyConstants.SUCCESS_MSG);
                fundTrfResponse.setDescription(response.getResponseNeftRtgs());
                fundTrfResponse.setBankReferenceNo(response.getUtr());
            } else if ((response.getStatus().equalsIgnoreCase("PENDING FOR PROCESSING"))) {
                fundTrfResponse.setStatus(IciciPropertyConstants.INPROGRESS_MSG);
                fundTrfResponse.setDescription(response.getResponseNeftRtgs());
            } else {
                fundTrfResponse.setStatus(IciciPropertyConstants.FAILURE_MSG);
                fundTrfResponse.setDescription(response.getMessageNeftRtgs());
            }

            fundTrfResponse.setExternalTransactionId(response.getUniqueId());
        }
        return fundTrfResponse;
    }

    public String buildIciciCoreRequest(byte[] publicKey, String... request) {

        String encRequest = "";
        try {
            String sessionKey = UtilityFunctions.generateSessionKey();

            SecureRandom randomSecureRandom = SecureRandom.getInstance("SHA1PRNG");
            byte[] iv = new byte[16];
            randomSecureRandom.nextBytes(iv);

            String encryptedSessionKey = Base64.encodeBase64String(encryptionDecryption.rsaEncrypt(sessionKey, publicKey));
            String encryptedRequest = encryptionDecryption.aesEncrypt(request[0], sessionKey, iv);
            String encryptedIV = Base64.encodeBase64String(iv);

            IciciCoreDto requestDto = IciciCoreDto.builder()
                    .requestId(TxnIdGenerator.getReferenceNumber())
                    .service("")
                    .initializationVector(encryptedIV)
                    .encryptedData(encryptedRequest)
                    .encryptedKey(encryptedSessionKey)
                    .clientInfo("")
                    .oaepHashAlgorithm("")
                    .optionalParam("")
                    .build();

            encRequest = new ObjectMapper().writeValueAsString(requestDto);
        } catch (Exception e) {
            return null;
        }
        return encRequest;
    }

}
