package com.neo.aggregator.commonservice;

import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neo.aggregator.bank.sbm.AddrDtls;
import com.neo.aggregator.bank.sbm.CustData;
import com.neo.aggregator.bank.sbm.CustDtls;
import com.neo.aggregator.bank.sbm.DemographicData;
import com.neo.aggregator.bank.sbm.EntityDoctData;
import com.neo.aggregator.bank.sbm.Header;
import com.neo.aggregator.bank.sbm.MessageKey;
import com.neo.aggregator.bank.sbm.PasswordToken;
import com.neo.aggregator.bank.sbm.PhoneEmailDtls;
import com.neo.aggregator.bank.sbm.PsychographMiscData;
import com.neo.aggregator.bank.sbm.PsychographicData;
import com.neo.aggregator.bank.sbm.RelatedDtls;
import com.neo.aggregator.bank.sbm.RequestHeader;
import com.neo.aggregator.bank.sbm.RequestMessageInfo;
import com.neo.aggregator.bank.sbm.Security;
import com.neo.aggregator.bank.sbm.Token;
import com.neo.aggregator.constant.NeoCustomFieldConstants;
import com.neo.aggregator.constant.ProductConstants;
import com.neo.aggregator.constant.SbmPropertyConstants;
import com.neo.aggregator.dao.GeoMasterSbmDao;
import com.neo.aggregator.dao.MasterConfigDao;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.RegistrationRequestV2Dto;
import com.neo.aggregator.dto.v2.AddressDto;
import com.neo.aggregator.dto.v2.CommunicationDto;
import com.neo.aggregator.dto.v2.KYCDto;
import com.neo.aggregator.dto.v2.SpecialDatesDto;
import com.neo.aggregator.enums.AddressCategory;
import com.neo.aggregator.enums.EmploymentType;
import com.neo.aggregator.enums.Industry;
import com.neo.aggregator.enums.SpecialDateType;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.GeoMasterSBM;
import com.neo.aggregator.model.MasterConfiguration;
import com.neo.aggregator.model.NeoBusinessCustomField;
import com.neo.aggregator.service.NeoBusinessCustomFieldService;
import com.neo.aggregator.utility.DateUtilityFunction;
import com.neo.aggregator.utility.IdGeneratorUtility;
import com.neo.aggregator.utility.NeoExceptionBuilder;
import com.neo.aggregator.utility.NeoExceptionConstant;
import com.neo.aggregator.utility.StringUtilsNeo;
import com.neo.aggregator.utility.TenantContextHolder;

@Service
public class SbmCommonService {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(SbmCommonService.class);

	@Autowired
	private GeoMasterSbmDao geoMasterDao;

	@Autowired
	private MasterConfigDao masterConfig;

	@Autowired
	private NeoBusinessCustomFieldService customFieldService;

	@Autowired
	private NeoExceptionBuilder exceptionBuilder;

	public Header buildHeader(String requestId) throws NeoException {

		Header header = null;

		String channelId = null;

		try {
			channelId = this.getCBSChannelId(TenantContextHolder.getNeoTenant());

			MessageKey messageKey = MessageKey.builder().requestUUID(IdGeneratorUtility.getRequestId())
					.serviceRequestId(requestId).serviceRequestVersion(SbmPropertyConstants.SERVICE_REQUEST_VERSION)
					.channelId(channelId).languageId("").build();

			String messageDateTime = DateUtilityFunction.dateInFormat(new Date(),
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			RequestMessageInfo requestMessageInfo = RequestMessageInfo.builder()
					.bankId(SbmPropertyConstants.SERVICE_REQUEST_BANKID).timeZone("").entityId("").entityType("")
					.armCorrelationId("").messageDateTime(messageDateTime).build();

			PasswordToken passwordToken = PasswordToken.builder().password("").userId("").build();

			Token token = Token.builder().passwordToken(passwordToken).build();

			Security security = Security.builder().token(token).ficertToken("").realUserLoginSessionId("").realUser("")
					.realUserPwd("").ssoTransferToken("").build();

			RequestHeader requestHeader = RequestHeader.builder().messageKey(messageKey)
					.requestMsgInfo(requestMessageInfo).security(security).build();

			header = Header.builder().requestHeader(requestHeader).build();

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.info("Exception during buildHeader :: ", ex);
			if (ex instanceof NeoException)
				throw ex;

		}

		return header;

	}

	public Header buildHeaderWithMobileNo(String requestId, String mobileNo) throws NeoException {

		Header header = null;

		String channelId = null;

		try {
			channelId = this.getCBSChannelId(TenantContextHolder.getNeoTenant());

			MessageKey messageKey = MessageKey.builder().requestUUID(IdGeneratorUtility.getRequestId())
					.serviceRequestId(requestId).serviceRequestVersion(SbmPropertyConstants.SERVICE_REQUEST_VERSION)
					.channelId(channelId).languageId("").mobileNoOtp(mobileNo).build();

			String messageDateTime = DateUtilityFunction.dateInFormat(new Date(),
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			RequestMessageInfo requestMessageInfo = RequestMessageInfo.builder()
					.bankId(SbmPropertyConstants.SERVICE_REQUEST_BANKID).timeZone("").entityId("").entityType("")
					.armCorrelationId("").messageDateTime(messageDateTime).build();

			PasswordToken passwordToken = PasswordToken.builder().password("").userId("").build();

			Token token = Token.builder().passwordToken(passwordToken).build();

			Security security = Security.builder().token(token).ficertToken("").realUserLoginSessionId("").realUser("")
					.realUserPwd("").ssoTransferToken("").build();

			RequestHeader requestHeader = RequestHeader.builder().messageKey(messageKey)
					.requestMsgInfo(requestMessageInfo).security(security).build();

			header = Header.builder().requestHeader(requestHeader).build();

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.info("Exception during buildHeader :: ", ex);
			if (ex instanceof NeoException)
				throw ex;

		}

		return header;

	}

	public Header buildHeader(String requestId, String channelId) throws NeoException {

		Header header = null;

		try {

			MessageKey messageKey = MessageKey.builder().requestUUID(IdGeneratorUtility.getRequestId())
					.serviceRequestId(requestId).serviceRequestVersion(SbmPropertyConstants.SERVICE_REQUEST_VERSION)
					.channelId(channelId).languageId("").build();

			String messageDateTime = DateUtilityFunction.dateInFormat(new Date(),
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			RequestMessageInfo requestMessageInfo = RequestMessageInfo.builder()
					.bankId(SbmPropertyConstants.SERVICE_REQUEST_BANKID).timeZone("").entityId("").entityType("")
					.armCorrelationId("").messageDateTime(messageDateTime).build();

			PasswordToken passwordToken = PasswordToken.builder().password("").userId("").build();

			Token token = Token.builder().passwordToken(passwordToken).build();

			Security security = Security.builder().token(token).ficertToken("").realUserLoginSessionId("").realUser("")
					.realUserPwd("").ssoTransferToken("").build();

			RequestHeader requestHeader = RequestHeader.builder().messageKey(messageKey)
					.requestMsgInfo(requestMessageInfo).security(security).build();

			header = Header.builder().requestHeader(requestHeader).build();

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.info("Exception during buildHeader :: ", ex);
			if (ex instanceof NeoException)
				throw ex;

		}

		return header;

	}

	public Header buildOutboundPaymentHeader(String requestId) throws NeoException {

		Header header = null;

		String channelId = null;

		try {
			channelId = this.getCBSChannelId(TenantContextHolder.getNeoTenant());
			String uuid = IdGeneratorUtility.getRandomUUID();

			String globalUUID = IdGeneratorUtility.getRandomUUID();

			MessageKey messageKey = MessageKey.builder().requestUUID(uuid).globalUUID(globalUUID)
					.serviceRequestId(requestId).serviceRequestVersion("11.0").originatorVersion("11.0")
					.channelId(channelId).languageId("").build();

			String messageDateTime = DateUtilityFunction.dateInFormat(new Date(),
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			RequestMessageInfo requestMessageInfo = RequestMessageInfo.builder()
					.bankId(SbmPropertyConstants.SERVICE_REQUEST_BANKID).timeZone("").entityId("").entityType("")
					.messageDateTime(messageDateTime).build();

			PasswordToken passwordToken = PasswordToken.builder().password("").userId("").build();

			Token token = Token.builder().passwordToken(passwordToken).build();

			Security security = Security.builder().token(token).ficertToken("").realUserLoginSessionId("").realUser("")
					.realUserPwd("").ssoTransferToken("").build();

			RequestHeader requestHeader = RequestHeader.builder().messageKey(messageKey)
					.requestMsgInfo(requestMessageInfo).security(security).build();

			header = Header.builder().requestHeader(requestHeader).build();

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.info("Exception during buildOutboundHeader :: ", ex);
			if (ex instanceof NeoException)
				throw ex;

		}

		return header;

	}

	public Header buildOutboundPaymentHeaderWithMobileNo(String requestId, String mobileNo) throws NeoException {

		Header header = null;

		String channelId = null;

		try {
			channelId = this.getCBSChannelId(TenantContextHolder.getNeoTenant());
			String uuid = IdGeneratorUtility.getRandomUUID();

			String globalUUID = IdGeneratorUtility.getRandomUUID();

			MessageKey messageKey = MessageKey.builder().requestUUID(uuid).globalUUID(globalUUID)
					.serviceRequestId(requestId).serviceRequestVersion("11.0").originatorVersion("11.0")
					.channelId(channelId).languageId("").mobileNoOtp(mobileNo).build();

			String messageDateTime = DateUtilityFunction.dateInFormat(new Date(),
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			RequestMessageInfo requestMessageInfo = RequestMessageInfo.builder()
					.bankId(SbmPropertyConstants.SERVICE_REQUEST_BANKID).timeZone("").entityId("").entityType("")
					.messageDateTime(messageDateTime).build();

			PasswordToken passwordToken = PasswordToken.builder().password("").userId("").build();

			Token token = Token.builder().passwordToken(passwordToken).build();

			Security security = Security.builder().token(token).ficertToken("").realUserLoginSessionId("").realUser("")
					.realUserPwd("").ssoTransferToken("").build();

			RequestHeader requestHeader = RequestHeader.builder().messageKey(messageKey)
					.requestMsgInfo(requestMessageInfo).security(security).build();

			header = Header.builder().requestHeader(requestHeader).build();

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.info("Exception during buildOutboundHeader :: ", ex);
			if (ex instanceof NeoException)
				throw ex;

		}

		return header;

	}

	public String getCBSChannelId(String partnerId) {

		String channelId = null;

		MasterConfiguration configModel = masterConfig.findByPartnerId(partnerId);

		if (configModel != null) {
			if (configModel.getCbsChannelId() != null) {
				channelId = configModel.getCbsChannelId();
			} else {
				channelId = SbmPropertyConstants.SERVICE_REQUEST_CHANNELID;
			}
		}
		return channelId;

	}

	public List<PhoneEmailDtls> buildPhoneDetails(List<CommunicationDto> commDetailsList) {

		List<PhoneEmailDtls> phoneDetails = null;

		try {
			CommunicationDto commDetails = null;

			for (CommunicationDto details : commDetailsList) {
				commDetails = details;
			}

			if (commDetails != null) {

				phoneDetails = new ArrayList<>();
				// Adding Communication Details

				String countryCode = commDetails.getCountryCode() != null ? commDetails.getCountryCode() : "91";

				String contactNo = commDetails.getContactNo();
				String phoneNumberlocalCode = commDetails.getContactNo();

				if (commDetails.getContactNo() != null && commDetails.getContactNo().length() > 10) {
					phoneNumberlocalCode = commDetails.getContactNo()
							.substring(commDetails.getContactNo().length() - 10);
					contactNo = "+".concat(countryCode).concat("()").concat(phoneNumberlocalCode);
				}

				PhoneEmailDtls phoneEmailDtls1 = PhoneEmailDtls.builder()
						.phoneEmailType(SbmPropertyConstants.PHONE_TYPE_COMMUNICATION).phoneNum(contactNo)
						.phoneNumCountryCode(countryCode).phoneNumLocalCode(phoneNumberlocalCode).prefFlag("Y")
						.phoneOrEmail(SbmPropertyConstants.COMMUNICATION_TYPE_PHONE).build();

				PhoneEmailDtls phoneEmailDtls2 = PhoneEmailDtls.builder().email(commDetails.getEmailId())
						.phoneEmailType(SbmPropertyConstants.EMAIL_TYPE_COMMUNICATION).prefFlag("Y")
						.phoneOrEmail(SbmPropertyConstants.COMMUNICATION_TYPE_EMAIL).build();

				phoneDetails.add(phoneEmailDtls1);
				phoneDetails.add(phoneEmailDtls2);
			}
		} catch (Exception ex) {

			logger.info("Exception during buildPhoneDetails :: ", ex);

			if (ex instanceof NeoException)
				throw ex;

		}

		return phoneDetails;

	}

	public List<AddrDtls> buildAddressDtls(List<AddressDto> addressDetails, RegistrationRequestV2Dto request) {

		List<AddrDtls> addressDetailsList = new ArrayList<>();

		AddrDtls addrDtls = null;
		Boolean isPrefferedAddressSet = Boolean.FALSE;
		Boolean isMailingAddressSet = Boolean.FALSE;

		try {

			for (AddressDto addDetails : addressDetails) {

				NeoBusinessCustomField addressCountryField = customFieldService.findByFieldNameAndTenant(
						NeoCustomFieldConstants.DEFAULT_ADDRESS_COUNTRY, TenantContextHolder.getNeoTenant());

				String country = "IN";

				if (addressCountryField != null) {
					country = addressCountryField.getFieldValue();
				}

				String state = "."; // Others
				String city = "."; // Others

				if (addDetails != null) {

					String pincode = addDetails.getPincode();

					List<GeoMasterSBM> geoList = geoMasterDao.fetchGaoMasterDetailsByPinCode(pincode);

					if (geoList != null && !geoList.isEmpty()) {
						GeoMasterSBM geoModel = geoList.get(0);
						state = geoModel.getState_code();
						city = geoModel.getDistrict_code();
						country = geoModel.getIso2();
					}

					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.YEAR, -10);
					Date startDate = cal.getTime();

					String date = DateUtilityFunction.dateInFormat(startDate,
							SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

					String addressType = "Mailing";
					String preferedAddress = "N";

					if (addDetails.getAddressCategory() != null) {

						if (AddressCategory.PERMANENT.equals(addDetails.getAddressCategory())) {
							addressType = "Home";
						} else if (AddressCategory.COMMUNICATION.equals(addDetails.getAddressCategory())) {
							addressType = "Mailing";
							preferedAddress = "Y";
							isMailingAddressSet = Boolean.TRUE;
							isPrefferedAddressSet = Boolean.TRUE;
						} else if (AddressCategory.NRERELATIVE.equals(addDetails.getAddressCategory())) {
							addressType = "NRERelative";
						} else
							continue;

					}

					NeoBusinessCustomField addressTrimRequired = customFieldService.findByFieldNameAndTenant(
							NeoCustomFieldConstants.ADDRESS_TRIM_REQUIRED, TenantContextHolder.getNeoTenant());

					NeoBusinessCustomField addressTrimLimit = customFieldService.findByFieldNameAndTenant(
							NeoCustomFieldConstants.ADDRESS_TRIM_LIMIT, TenantContextHolder.getNeoTenant());

					String addressLine1 = addDetails.getAddress1();
					String addressLine2 = addDetails.getAddress2();
					String addressLine3 = addDetails.getAddress3();

					if (addressTrimRequired != null && addressTrimRequired.getFieldValue() != null
							&& addressTrimRequired.getFieldValue().equals("Y")) {

						Integer limit = 45;

						if (addressTrimLimit != null) {

							try {
								limit = Integer.parseInt(addressTrimLimit.getFieldValue());
							} catch (NumberFormatException e) {
								logger.info("Limit is not a valid integer");
							}
						}

						addressLine1 = StringUtilsNeo.splitStringByLength(addressLine1, limit).size() > 0
								? StringUtilsNeo.splitStringByLength(addressLine1, limit).get(0)
								: addressLine1;
						addressLine2 = StringUtilsNeo.splitStringByLength(addressLine2, limit).size() > 0
								? StringUtilsNeo.splitStringByLength(addressLine2, limit).get(0)
								: addressLine2;
						addressLine3 = StringUtilsNeo.splitStringByLength(addressLine3, limit).size() > 0
								? StringUtilsNeo.splitStringByLength(addressLine3, limit).get(0)
								: addressLine3;
					}

					addrDtls = AddrDtls.builder().addressLine1(addressLine1).addressLine2(addressLine2)
							.addressLine3(addressLine3).addressCategory(addressType).city(city).country(country)
							.freeTextLabel("RETAIL").holdMailFlag("N").prefAddress(preferedAddress)
							.prefFormat("FREE_TEXT_FORMAT").state(state).postalCode(addDetails.getPincode())
							.startDt(date).build();

					addressDetailsList.add(addrDtls);
				}
			}

			for (AddrDtls addDtls : addressDetailsList) {

				if (addDtls.getAddressCategory().equals("Home")) {
					if (!isPrefferedAddressSet) {
						addDtls.setPrefAddress("Y");
					}

					if (!isMailingAddressSet) {
						addDtls.setAddressCategory("Mailing");
					}

					break;
				}
			}

			if (request.getIsNRECustomer() != null && request.getIsNRECustomer()) {

				AddrDtls nreAddress = addressDetailsList.stream()
						.filter(address -> "NRERelative".equals(address.getAddressCategory())).findAny().orElse(null);

				if (nreAddress == null) {
					AddrDtls communicationAddress = addressDetailsList.stream()
							.filter(address -> "Mailing".equals(address.getAddressCategory())).findAny().orElse(null);
					nreAddress = SerializationUtils.clone(communicationAddress);
					nreAddress.setAddressCategory("NRERelative");
					addressDetailsList.add(nreAddress);
				}

			}

		} catch (Exception ex) {

			ex.printStackTrace();
			logger.info("Exception during buildAddressDtls :: ", ex);

			if (ex instanceof NeoException)
				throw ex;

		}

		return addressDetailsList;
	}

	public CustDtls buildCustDtls(RegistrationRequestV2Dto request, List<AddrDtls> addrDtls,
			List<CommunicationDto> commDetailsList, List<PhoneEmailDtls> phoneDetails, String solId) {

		CustDtls custDtls = null;

		String prefName = null;

		try {

			CommunicationDto commDetails = null;

			for (CommunicationDto details : commDetailsList) {
				commDetails = details;
			}

			String contactNo = commDetails.getContactNo();

			SpecialDatesDto dobSpecialDate = request.getDateInfo().stream()
					.filter(date -> SpecialDateType.DOB.equals(date.getDateType())).findAny().orElse(null);

			SpecialDatesDto nreBecomingSpecialDate = request.getDateInfo().stream()
					.filter(date -> SpecialDateType.NRE_BECOMING.equals(date.getDateType())).findAny().orElse(null);

			Date dob = dobSpecialDate.getDate();
			String gender = "M";
			String salutation = "MR.";

			String customerType = SbmPropertyConstants.CUSTOMER_TYPE_INDIVIDUAL_MALE;

			if (request.getGender().name().equals("MALE") || request.getGender().name().equals("M")) {
				gender = "M";
				salutation = SbmPropertyConstants.SALUTATION_MR;
			} else {
				gender = "F";
				salutation = SbmPropertyConstants.SALUTATION_MS;
				customerType = SbmPropertyConstants.CUSTOMER_TYPE_INDIVIDUAL_FEMALE;
			}

			String dobYear = DateUtilityFunction.getCurrentYearMonthDay(dob, 'Y');
			String dobMonth = StringUtils.leftPad(DateUtilityFunction.getCurrentYearMonthDay(dob, 'M'), 2, '0');
			String dobDay = StringUtils.leftPad(DateUtilityFunction.getCurrentYearMonthDay(dob, 'D'), 2, '0');

			String dobFormatted = DateUtilityFunction.dateInFormat(dob,
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);
			String currentDateFormatted = DateUtilityFunction.dateInFormat(new Date(),
					SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);

			String region = SbmPropertyConstants.REGION_URBAN;
			String primarySolid = solId;
			String occupation = getOccupationByemploymentIndustry(request.getEmploymentIndustry());

			String middleName = request.getMiddleName() != null ? request.getMiddleName() : "";
			String firstName = request.getFirstName() != null ? request.getFirstName() : "";
			String lastName = request.getLastName() != null ? request.getLastName() : "";

			String combinedName = firstName + " " + lastName + " " + middleName;

			String shortName = StringUtils.trim(combinedName);

			if (StringUtils.isNotBlank(request.getFirstName())) {
				prefName = request.getFirstName();
			} else if (StringUtils.isNotBlank(request.getLastName())) {
				prefName = request.getLastName();
			} else if (StringUtils.isNotBlank(request.getMiddleName())) {
				prefName = request.getMiddleName();

			}

			Boolean isSeniorCitizen = Boolean.FALSE;
			Boolean isMinorCitizen = Boolean.FALSE;

			Period period = DateUtilityFunction.calculatePeriodBetweenDate(dob);

			if (period.getYears() >= 60) {
				logger.info("Customer is Senior Citizen age :: " + period.getYears());
				isSeniorCitizen = Boolean.TRUE;
			} else if (period.getYears() < 18) {
				logger.info("Customer is Minor age :: " + period.getYears());
				isMinorCitizen = Boolean.TRUE;
			}

			String taxSlab = SbmPropertyConstants.TAX_SLAB_TDTAX;

			String isMinor = "N";
			if (request.getIsMinor() != null && request.getIsMinor() && isMinorCitizen) {
				isMinor = "Y";
			}

			if (isSeniorCitizen) {
				taxSlab = SbmPropertyConstants.TAX_SLAB_SRTAX;
			}

			CustData custData = CustData.builder().addDtls(addrDtls).birthDt(dobDay).birthMonth(dobMonth)
					.birthYear(dobYear).createdBySystemid("FIVUSR").dateOfBirth(dobFormatted)
					.firstName(request.getFirstName()).language(SbmPropertyConstants.LANGUAGE_INDIA_ENGLISH)
					.lastName(lastName).middleName(middleName).isMinor(isMinor)
					.defaultAddrType(SbmPropertyConstants.ADDRESS_TYPE_MAILING).gender(gender)
					.manager(SbmPropertyConstants.MANAGER_ID).nativeLanguageCode("INFENG").phoneEmailDtls(phoneDetails)
					.prefName(prefName).region(region).primarySolid(primarySolid)
					.relationshipOpeningDt(currentDateFormatted).salutation(salutation)
					.segmentationClass(SbmPropertyConstants.SEGMENTATION_CLASS_A).shortName(shortName.substring(0, 3))
					.staffFlag("N").subSegment(SbmPropertyConstants.SEGMENTATION_SUBCLASS_CLASS_AA)
					.taxDeductionTable(taxSlab).tradeFinFlag("N").smsBankingMobileNumber(contactNo)
					.isSmsBankingEnabled("Y").isEbankingEnabled("Y").occupation(occupation).custType(customerType)
					.constitutionCode(customerType).autoApprovalCif("Y").enableAlerts("Y").build();

			if (request.getIsNRECustomer() != null && request.getIsNRECustomer()) {

				String nreBecomingDate = DateUtilityFunction.dateInFormat(nreBecomingSpecialDate.getDate(),
						SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);
				custData.setNreBecomingDate(nreBecomingDate);
				custData.setIsCustnre("Y");
			} else {
				custData.setIsCustnre("N");
			}

			custDtls = CustDtls.builder().custData(custData).build();

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.info("Exception during buildCustDtls :: ", ex);
			if (ex instanceof NeoException)
				throw ex;
		}

		return custDtls;
	}

	public DemographicData buildDemographic(RegistrationRequestV2Dto request) {

		String nationality = request.getNationality() != null ? request.getNationality() : "IN";

		String employmentStatus = getEmploymentByType(request.getEmploymentType());

		String maritalStatus = getMaritalStatus(request.getMaritalStatus());

		return DemographicData.builder().employmentStatus(employmentStatus).maritalStatus(maritalStatus)
				.nationality(nationality).build();

	}

	public List<EntityDoctData> buildEntityDocData(RegistrationRequestV2Dto request, List<AddrDtls> address) {

		EntityDoctData entityDocData = null;

		List<EntityDoctData> entityDocDataList = new ArrayList<>();

		NeoBusinessCustomField defaultPreferredDocumentTypeCustomValue = customFieldService.findByFieldNameAndTenant(
				NeoCustomFieldConstants.DEFAULT_PREFERRED_DOCUMENT_TYPE, TenantContextHolder.getNeoTenant());

		try {

			List<KYCDto> documentsList = request.getKycInfo();

			KYCDto documentDto = null;
			String documentNo = "";
			String documentType = "";
			String documentCode = "";

			Boolean isPrefered = Boolean.FALSE;
			String preferredUniqueId = "N";

			for (KYCDto document : documentsList) {
				documentDto = document;

				String countryOfIssue = documentDto.getCountryOfIssue() != null ? documentDto.getCountryOfIssue()
						: "IN";
				String issuedBy = documentDto.getDocumentIssuedBy() != null ? documentDto.getDocumentIssuedBy()
						: SbmPropertyConstants.ISSUED_ORG_INCOMETAX_DEPARTMENT;

				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.YEAR, -10);
				Date docIssued = cal.getTime();

				String documentIssueDate = DateUtilityFunction.dateInFormat(docIssued,
						SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);
				String documentExpiryDate = null;

				if (documentDto.getDocumentExpiry() != null) {
					documentExpiryDate = DateUtilityFunction.dateInFormat(documentDto.getDocumentExpiry(),
							SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);
				} else {
					documentExpiryDate = DateUtilityFunction.dateInFormat(
							DateUtilityFunction.addNoOfMonthsToDate(new Date(), 1200),
							SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);
				}

				if (documentDto.getDocumentType() != null) {
					if (documentDto.getDocumentType().equals("PAN")) {
						documentType = SbmPropertyConstants.IDTYPE_CODE_PAN;
						documentCode = SbmPropertyConstants.DOCTYPE_CODE_PAN;
						documentNo = documentDto.getDocumentNo();
						documentExpiryDate = null;
					} else if (documentDto.getDocumentType().equals("PASSPORT")) {
						documentType = SbmPropertyConstants.IDTYPE_CODE_RETAIL;
						documentCode = SbmPropertyConstants.DOCTYPE_CODE_PASSP;
						documentNo = documentDto.getDocumentNo();
					} else if (documentDto.getDocumentType().equals("CKYC")) {
						documentType = SbmPropertyConstants.IDTYPE_CODE_CKYC;
						documentCode = SbmPropertyConstants.DOCTYPE_CODE_CKYC;
						documentNo = documentDto.getDocumentNo();
						if (StringUtils.isNotBlank(document.getDocumentIssuanceDate() != null
								? document.getDocumentIssuanceDate().toString()
								: null)) {
							logger.info("CKYC Issuance Date Provided : ");
							documentIssueDate = DateUtilityFunction.dateInFormat(document.getDocumentIssuanceDate(),
									SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);
						}
						documentExpiryDate = null;
					} else if (documentDto.getDocumentType().equals("FORM60")) {
						documentType = SbmPropertyConstants.IDTYPE_CODE_RETAIL;
						documentCode = SbmPropertyConstants.DOCTYPE_CODE_FORM60;
						documentNo = documentDto.getDocumentNo();

						if (documentDto.getDocumentExpiry() == null) {
							documentExpiryDate = DateUtilityFunction.dateInFormat(
									DateUtilityFunction.addNoOfMonthsToDate(new Date(), 12),
									SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);
						} else {
							documentExpiryDate = DateUtilityFunction.dateInFormat(documentDto.getDocumentExpiry(),
									SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);
						}

					} else {
						// Default Type as FORM60
						documentType = SbmPropertyConstants.IDTYPE_CODE_RETAIL;
						documentCode = SbmPropertyConstants.DOCTYPE_CODE_FORM60;
						documentNo = documentDto.getDocumentNo();

						if (documentDto.getDocumentExpiry() == null) {
							documentExpiryDate = DateUtilityFunction.dateInFormat(
									DateUtilityFunction.addNoOfMonthsToDate(new Date(), 12),
									SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);
						} else {
							documentExpiryDate = DateUtilityFunction.dateInFormat(documentDto.getDocumentExpiry(),
									SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);
						}
					}
				}

				isPrefered = Boolean.TRUE;

				String prefUniqId = "N";


				if (isPrefered && preferredUniqueId.equals("N") && defaultPreferredDocumentTypeCustomValue == null) {

					preferredUniqueId = "Y";
					prefUniqId = "Y";
				} else if (preferredUniqueId.equals("N") && defaultPreferredDocumentTypeCustomValue != null) {

					if (documentDto.getDocumentType().equals(defaultPreferredDocumentTypeCustomValue.getFieldValue())) {
						logger.info("Custom Prefered Document Type is Configured : Success Match");
						preferredUniqueId = "Y";
						prefUniqId = "Y";

					}

				}

				AddrDtls custAddress = new AddrDtls();

				if (address != null && address.size() > 0) {
					custAddress = address.get(0);
				}

				entityDocData = EntityDoctData.builder().countryOfIssue(countryOfIssue).docCode(documentCode)
						.issueDt(documentIssueDate).expDt(documentExpiryDate).typeCode(documentType)
						.placeOfIssue(custAddress.getCity()).referenceNum(documentNo).preferredUniqueId(prefUniqId)
						.idIssuedOrganisation(issuedBy).build();

				entityDocDataList.add(entityDocData);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.info("Exception during buildEntityDocData :: ", ex);
			if (ex instanceof NeoException)
				throw ex;
		}

		return entityDocDataList;
	}

	public PsychographMiscData buildMiscData(RegistrationRequestV2Dto request, String currency, String country) {
		/*
		 * String dtd1 =
		 * DateUtilityFunction.dateInFormat(DateUtilityFunction.addNoOfMonthsToDate(new
		 * Date(), 1200), SbmPropertyConstants.CBS_LOCAL_DATE_TIME_FORMAT);
		 */

		String dtd1 = "2099-12-31T00:00:00.000";

		return PsychographMiscData.builder().countryOfIssue("IN").dtdt1(dtd1).strTxt10("INR").type("CURRENCY").build();

	}

	public PsychographicData buildPsychoData(RegistrationRequestV2Dto request, PsychographMiscData psychoMisc,
			String currency, String country) {
		return PsychographicData.builder().custCurrCode("INR").miscData(psychoMisc)
				.preferredLocale(SbmPropertyConstants.LANGUAGE_LOCALE_ENGLISH_US).build();
	}

	public RelatedDtls buildRelatedDtls(PsychographicData psychoData, DemographicData demoData,
			List<EntityDoctData> entityDocData) {
		return RelatedDtls.builder().demographicData(demoData).entityDoctData(entityDocData)
				.pyschographicData(psychoData).build();
	}

	public String getOccupationByemploymentIndustry(Industry industry) {

		String occupation = "O";

		if (industry == null) {
			return occupation;
		}

		switch (industry) {

		case AGRICULTURE_FOOD_NATURAL_RESOURCES:
			occupation = SbmPropertyConstants.AGRICULTURE_FOOD_NATURAL_RESOURCES;
			break;
		case ARCHITECTURE_AND_CONSTRUCTION:
			occupation = SbmPropertyConstants.ARCHITECTURE_AND_CONSTRUCTION;
			break;
		case ARTS_AUDIO_OR_VIDEO_TECHNOLOGY_AND_COMMUNICATIONS:
			occupation = SbmPropertyConstants.ARTS_AUDIO_OR_VIDEO_TECHNOLOGY_AND_COMMUNICATIONS;
			break;
		case BUSINESS_MANAGEMENT_AND_ADMINISTRATION:
			occupation = SbmPropertyConstants.BUSINESS_MANAGEMENT_AND_ADMINISTRATION;
			break;
		case EDUCATION_AND_TRAINING:
			occupation = SbmPropertyConstants.EDUCATION_AND_TRAINING;
			break;
		case FINANCE:
			occupation = SbmPropertyConstants.FINANCE;
			break;
		case GOVERNMENT_AND_PUBLIC_ADMINISTRATION:
			occupation = SbmPropertyConstants.GOVERNMENT_AND_PUBLIC_ADMINISTRATION;
			break;
		case HEALTH_SCIENCE:
			occupation = SbmPropertyConstants.HEALTH_SCIENCE;
			break;
		case HOSPITALITY_AND_TOURISM:
			occupation = SbmPropertyConstants.HOSPITALITY_AND_TOURISM;
			break;
		case HUMAN_SERVICES:
			occupation = SbmPropertyConstants.HUMAN_SERVICES;
			break;
		case INFORMATION_TECHNOLOGY:
			occupation = SbmPropertyConstants.INFORMATION_TECHNOLOGY;
			break;
		case LAW_PUBLIC_SAFETY_CORRECTIONS_AND_SECURITY:
			occupation = SbmPropertyConstants.LAW_PUBLIC_SAFETY_CORRECTIONS_AND_SECURITY;
			break;
		case MANUFACTURING:
			occupation = SbmPropertyConstants.MANUFACTURING;
			break;
		case MARKETING_SALES_AND_SERVICE:
			occupation = SbmPropertyConstants.MARKETING_SALES_AND_SERVICE;
			break;
		case SCIENCE_TECHNOLOGY_ENGINEERING_AND_MATHEMATICS:
			occupation = SbmPropertyConstants.SCIENCE_TECHNOLOGY_ENGINEERING_AND_MATHEMATICS;
			break;
		case TRANSPORTATION_DISTRIBUTION_AND_LOGISTICS:
			occupation = SbmPropertyConstants.TRANSPORTATION_DISTRIBUTION_AND_LOGISTICS;
			break;
		}

		return occupation;
	}

	public String getEmploymentByType(EmploymentType type) {

		String employmentStatus = "Other";

		if (type == null) {
			return employmentStatus;
		}

		switch (type) {

		case EMPLOYED:
			employmentStatus = SbmPropertyConstants.EMPLOYED;
			break;
		case UNEMPLOYED:
			employmentStatus = SbmPropertyConstants.UNEMPLOYED;
			break;
		case ENTREPRENEUR:
			employmentStatus = SbmPropertyConstants.ENTREPRENEUR;
			break;
		case PUBLIC_SECTOR_EMPLOYEE:
			employmentStatus = SbmPropertyConstants.PUBLIC_SECTOR_EMPLOYEE;
			break;
		case FREELANCER:
			employmentStatus = SbmPropertyConstants.FREELANCER;
			break;
		case HOUSEWORK:
			employmentStatus = SbmPropertyConstants.HOUSEWORK;
			break;
		case APPRENTICE:
			employmentStatus = SbmPropertyConstants.APPRENTICE;
			break;
		case RETIRED:
			employmentStatus = SbmPropertyConstants.RETIRED;
			break;
		case STUDENT:
			employmentStatus = SbmPropertyConstants.STUDENT;
			break;
		case SELF_EMPLOYED:
			employmentStatus = SbmPropertyConstants.SELF_EMPLOYED;
			break;
		case MILITARY_OR_COMMUNITY_SERVICE:
			employmentStatus = SbmPropertyConstants.MILITARY_OR_COMMUNITY_SERVICE;
			break;
		default:
			break;

		}

		return employmentStatus;

	}

	public String getMaritalStatus(String status) {

		String maritalStatus = "UNMAR";

		if (status == null) {
			return maritalStatus;
		}

		switch (status) {

		case "SINGLE":
			maritalStatus = "UNMAR";
			break;

		case "MARRIED":
			maritalStatus = "MARR";
			break;
		}

		return maritalStatus;

	}

	public GeoMasterSBM getGeoMasterFromPinCode(String pincode) throws NeoException {

		GeoMasterSBM response = new GeoMasterSBM();

		try {

			List<GeoMasterSBM> geoList = geoMasterDao.fetchGaoMasterDetailsByPinCode(pincode);

			if (geoList != null && !geoList.isEmpty()) {
				GeoMasterSBM geoModel = geoList.get(0);
				response.setState(geoModel.getState_code());
				response.setDistrict(geoModel.getDistrict_code());
				response.setCountry(geoModel.getIso2());
			} else {
				/*
				 * Removed default to others response.setCountry("IN"); response.setState(".");
				 * response.setDistrict(".");
				 */
				throw exceptionBuilder.buildAndNotify(NeoExceptionConstant.GEO_MASTER_NOT_FOUND, pincode, null,
						Locale.US, null, null, ProductConstants.GENERIC);

			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.info("Exception during buildAddressDtls :: ", ex);

			if (ex instanceof NeoException)
				throw ex;
		}

		return response;
	}

	public String getRelPartyTypeByRelationShip(String relationShip) {

		String relParty = "999";

		if (relationShip == null) {
			return relParty;
		}

		switch (relationShip) {

		case "FATHER":
			relParty = SbmPropertyConstants.RELPARTY_FATHER;
			break;
		case "GRAND_FATHER":
			relParty = SbmPropertyConstants.RELPARTY_GRAND_FATHER;
			break;
		case "GRAND_MOTHER":
			relParty = SbmPropertyConstants.RELPARTY_GRAND_MOTHER;
			break;
		case "MOTHER":
			relParty = SbmPropertyConstants.RELPARTY_MOTHER;
			break;
		case "SON":
			relParty = SbmPropertyConstants.RELPARTY_SON;
			break;
		case "DAUGHTER":
			relParty = SbmPropertyConstants.RELPARTY_DAUGHTER;
			break;
		case "BROTHER":
			relParty = SbmPropertyConstants.RELPARTY_BROTHER;
			break;
		case "SISTER":
			relParty = SbmPropertyConstants.RELPARTY_SISTER;
			break;
		case "PARTNERS":
			relParty = SbmPropertyConstants.RELPARTY_PARTNERS;
			break;
		case "DIRECTOR":
			relParty = SbmPropertyConstants.RELPARTY_DIRECTOR;
			break;
		case "GUARANTOR":
			relParty = SbmPropertyConstants.RELPARTY_GUARANTOR;
			break;
		case "KARTA":
			relParty = SbmPropertyConstants.RELPARTY_KARTA;
			break;
		case "CO_PARCENERS":
			relParty = SbmPropertyConstants.RELPARTY_CO_PARCENERS;
			break;
		case "NO_RELATION":
			relParty = SbmPropertyConstants.RELPARTY_NO_RELATION;
			break;
		case "NATURAL_GUARDIAN":
			relParty = SbmPropertyConstants.RELPARTY_NATURAL_GUARDIAN;
			break;
		case "LEGAL_GUARDIAN":
			relParty = SbmPropertyConstants.RELPARTY_LEGAL_GUARDIAN;
			break;
		case "WIFE":
			relParty = SbmPropertyConstants.RELPARTY_WIFE;
			break;
		case "HUSBAND":
			relParty = SbmPropertyConstants.RELPARTY_HUSBAND;
			break;
		case "POWER_OF_ATTORNEY_HOLDER":
			relParty = SbmPropertyConstants.RELPARTY_POWER_OF_ATTORNEY_HOLDER;
			break;
		case "EXECUTOR":
			relParty = SbmPropertyConstants.RELPARTY_EXECUTOR;
			break;
		case "ADMINISTRATOR":
			relParty = SbmPropertyConstants.RELPARTY_ADMINISTRATOR;
			break;
		case "MANAGER":
			relParty = SbmPropertyConstants.RELPARTY_MANAGER;
			break;
		case "MANDATE_HOLDER":
			relParty = SbmPropertyConstants.RELPARTY_MANDATE_HOLDER;
			break;
		case "OTHERS":
			relParty = SbmPropertyConstants.RELPARTY_OTHERS;
			break;
		}

		return relParty;
	}

	public String getRelationShipByRelPartyType(String relParty) {

		String relationShip = null;

		if (relParty == null) {
			return relParty;
		}

		switch (relParty) {

		case SbmPropertyConstants.RELPARTY_FATHER:
			relationShip = "FATHER";
			break;
		case SbmPropertyConstants.RELPARTY_GRAND_FATHER:
			relationShip = "GRAND_FATHER";
			break;
		case SbmPropertyConstants.RELPARTY_GRAND_MOTHER:
			relationShip = "GRAND_MOTHER";
			break;
		case SbmPropertyConstants.RELPARTY_MOTHER:
			relationShip = "MOTHER";
			break;
		case SbmPropertyConstants.RELPARTY_SON:
			relationShip = "SON";
			break;
		case SbmPropertyConstants.RELPARTY_DAUGHTER:
			relationShip = "DAUGHTER";
			break;
		case SbmPropertyConstants.RELPARTY_BROTHER:
			relationShip = "BROTHER";
			break;
		case SbmPropertyConstants.RELPARTY_SISTER:
			relationShip = "SISTER";
			break;
		case SbmPropertyConstants.RELPARTY_PARTNERS:
			relationShip = "PARTNERS";
			break;
		case SbmPropertyConstants.RELPARTY_DIRECTOR:
			relationShip = "DIRECTOR";
			break;
		case SbmPropertyConstants.RELPARTY_GUARANTOR:
			relationShip = "GUARANTOR";
			break;
		case SbmPropertyConstants.RELPARTY_KARTA:
			relationShip = "KARTA";
			break;
		case SbmPropertyConstants.RELPARTY_CO_PARCENERS:
			relationShip = "CO_PARCENERS";
			break;
		case SbmPropertyConstants.RELPARTY_NO_RELATION:
			relationShip = "NO_RELATION";
			break;
		case SbmPropertyConstants.RELPARTY_NATURAL_GUARDIAN:
			relationShip = "NATURAL_GUARDIAN";
			break;
		case SbmPropertyConstants.RELPARTY_LEGAL_GUARDIAN:
			relationShip = "LEGAL_GUARDIAN";
			break;
		case SbmPropertyConstants.RELPARTY_WIFE:
			relationShip = "WIFE";
			break;
		case SbmPropertyConstants.RELPARTY_HUSBAND:
			relationShip = "HUSBAND";
			break;
		case SbmPropertyConstants.RELPARTY_POWER_OF_ATTORNEY_HOLDER:
			relationShip = "POWER_OF_ATTORNEY_HOLDER";
			break;
		case SbmPropertyConstants.RELPARTY_EXECUTOR:
			relationShip = "EXECUTOR";
			break;
		case SbmPropertyConstants.RELPARTY_ADMINISTRATOR:
			relationShip = "ADMINISTRATOR";
			break;
		case SbmPropertyConstants.RELPARTY_MANAGER:
			relationShip = "MANAGER";
			break;
		case SbmPropertyConstants.RELPARTY_MANDATE_HOLDER:
			relationShip = "MANDATE_HOLDER";
			break;
		case SbmPropertyConstants.RELPARTY_OTHERS:
			relationShip = "OTHERS";
			break;
		}

		return relationShip;
	}
}
