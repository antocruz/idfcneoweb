package com.neo.aggregator.commonservice;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.annotation.CheckForNull;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.neo.aggregator.bank.equitas.AboutLeadDetails;
import com.neo.aggregator.bank.equitas.AboutProspectDetails;
import com.neo.aggregator.bank.equitas.AccountLead;
import com.neo.aggregator.bank.equitas.AccountLeadMessageBody;
import com.neo.aggregator.bank.equitas.AddDocRq;
import com.neo.aggregator.bank.equitas.AddDocumentMessageBody;
import com.neo.aggregator.bank.equitas.AddressDetails;
import com.neo.aggregator.bank.equitas.AuthInfo;
import com.neo.aggregator.bank.equitas.CreateCaseReq;
import com.neo.aggregator.bank.equitas.CreateDigiCaseMessageBody;
import com.neo.aggregator.bank.equitas.CstmFld;
import com.neo.aggregator.bank.equitas.CurrentTDMaturityDetails;
import com.neo.aggregator.bank.equitas.CustomFields;
import com.neo.aggregator.bank.equitas.CustomerAccountLeadRelation;
import com.neo.aggregator.bank.equitas.CustomerDeliverables;
import com.neo.aggregator.bank.equitas.CustomerPreferences;
import com.neo.aggregator.bank.equitas.DigiAccountDetailMessageBody;
import com.neo.aggregator.bank.equitas.DigiAccountStatementMessageBody;
import com.neo.aggregator.bank.equitas.DigiCustLeadwitheKYCMessageBody;
import com.neo.aggregator.bank.equitas.DigiLeadStatusMessageBody;
import com.neo.aggregator.bank.equitas.DigiTDMaturityDetailsMessageBody;
import com.neo.aggregator.bank.equitas.DocRefId;
import com.neo.aggregator.bank.equitas.Documents;
import com.neo.aggregator.bank.equitas.EKycDetails;
import com.neo.aggregator.bank.equitas.FatcaTaxDetailsIndividual;
import com.neo.aggregator.bank.equitas.FetchAccountLeadMessageBody;
import com.neo.aggregator.bank.equitas.GenerateDigiOTPMessageBody;
import com.neo.aggregator.bank.equitas.Guardian;
import com.neo.aggregator.bank.equitas.GuardianAddress;
import com.neo.aggregator.bank.equitas.IdentityInfoDetails;
import com.neo.aggregator.bank.equitas.IndividualEntry;
import com.neo.aggregator.bank.equitas.LeadDetails;
import com.neo.aggregator.bank.equitas.LeadMessageBody;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.LeadUpdateMessageBody;
import com.neo.aggregator.bank.equitas.MessageBody;
import com.neo.aggregator.bank.equitas.MessageHeader;
import com.neo.aggregator.bank.equitas.Nominee;
import com.neo.aggregator.bank.equitas.NomineeAddress;
import com.neo.aggregator.bank.equitas.RedeemDigiDepositReq;
import com.neo.aggregator.bank.equitas.RedeemDigiTDMaturityDetailsMessageBody;
import com.neo.aggregator.bank.equitas.SsnAuth;
import com.neo.aggregator.bank.equitas.SubmitAccountLeadMessageBody;
import com.neo.aggregator.bank.equitas.UpdateAccountLeadMessageBody;
import com.neo.aggregator.bank.equitas.UpdateDigiDocumentMessageBody;
import com.neo.aggregator.bank.equitas.UpdateDigiTDMaturityDetailsMessageBody;
import com.neo.aggregator.bank.equitas.dto.AccountLeadDto;
import com.neo.aggregator.bank.equitas.dto.CustomerAccountLeadRelationDto;
import com.neo.aggregator.bank.equitas.dto.CustomerDeliverablesDto;
import com.neo.aggregator.bank.equitas.dto.CustomerPreferencesDto;
import com.neo.aggregator.bank.equitas.dto.DigiAccountRequestDto;
import com.neo.aggregator.bank.equitas.dto.GuardianAddresssDto;
import com.neo.aggregator.bank.equitas.dto.GuardianDto;
import com.neo.aggregator.bank.equitas.dto.KycRequestDto;
import com.neo.aggregator.bank.equitas.dto.NomineeAddressDto;
import com.neo.aggregator.bank.equitas.dto.NomineeDto;
import com.neo.aggregator.bank.equitas.kyc.Auth;
import com.neo.aggregator.bank.equitas.kyc.KycReqInfo;
import com.neo.aggregator.bank.equitas.kyc.KycRequestData;
import com.neo.aggregator.bank.equitas.kyc.TransactionInfo;
import com.neo.aggregator.bank.equitas.kyc.UID;
import com.neo.aggregator.dao.GeoMasterDao;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.v2.AddressDto;
import com.neo.aggregator.dto.v2.SpecialDatesDto;
import com.neo.aggregator.enums.AddressCategory;
import com.neo.aggregator.enums.GenderType;
import com.neo.aggregator.enums.SpecialDateType;
import com.neo.aggregator.model.GeoMaster;
import com.neo.aggregator.utility.JaxbCharacterEscapeHandler;
import com.neo.aggregator.utility.NeoExceptionBuilder;
import com.neo.aggregator.utility.Utils;
import com.sun.xml.bind.marshaller.DataWriter;

@Service
public class EquitasCommonService {
	
	private static final Logger logger = LoggerFactory.getLogger(EquitasCommonService.class);

	@Autowired
	private GeoMasterDao gmDao;
	
	@Autowired
	private NeoExceptionBuilder exceptionBuilder;
	
	public MessageHeader buildMessageHeader(KycRequestDto request) {

		MessageHeader requestHeader = null;
		String id = Utils.uniqueIdentifier();
		try {
			AuthInfo authInf = buildAuth(request);
			requestHeader = MessageHeader.builder().msgId(id).cnvId(Utils.uniqueIdentifier()).extRefId(id).bizObjId(id)
					.appId(request.getAppId()).timestamp(Utils.formatTimestamp()).authInfo(authInf).build();

		} catch (Exception ex) {

			if (ex instanceof NeoException)
				throw ex;

		}
		return requestHeader;
	}

	public MessageBody buildMessageBody(KycRequestDto request) {

		MessageBody messageBody = null;
		try {

			messageBody = MessageBody.builder().associatedWith(request.getAssociatedWith())
					.authenticationToken(request.getAuthenticationToken()).custLeadId(request.getCustLeadId()).build();
		} catch (Exception ex) {

			if (ex instanceof NeoException)
				throw ex;

		}
		return messageBody;
	}

	public LeadMessageHeader buildLeadCreationMessageHeader(KycRequestDto request) {

		LeadMessageHeader leadMessageHeader = null;
		String id = Utils.uniqueIdentifier();
		try {
			AuthInfo authInf = buildAuth(request);

			leadMessageHeader = LeadMessageHeader.builder().cnvId(Utils.uniqueIdentifier()).bizObjId(id)
					.appId(request.getAppId()).msgId(id).extRefId(id).timestamp(Utils.formatTimestamp())
					.authInfo(authInf).build();
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}

		return leadMessageHeader;
	}
	
	public LeadMessageHeader buildLeadCreationMessageHeader(DigiAccountRequestDto request) {

		LeadMessageHeader leadMessageHeader = null;
		String id = Utils.uniqueIdentifier();
		try {
			AuthInfo authInf = buildAuth(request);

			leadMessageHeader = LeadMessageHeader.builder().cnvId(Utils.uniqueIdentifier()).bizObjId(id)
					.appId(request.getAppId()).msgId(id).extRefId(id).timestamp(Utils.formatTimestamp())
					.authInfo(authInf).build();
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}

		return leadMessageHeader;
	}

	public LeadMessageBody buildLeadCreationMessageBody(KycRequestDto request) {
		LeadMessageBody leadMessageBody = null;
		try {

			IndividualEntry indiEntry = IndividualEntry.builder().title(request.getTitle())
					.firstName(request.getFirstName()).lastName(request.getLastName())
					.middleName(request.getMiddleName()).motherMaidenName(request.getMotherMaidenName())
					.mobilePhone(request.getRegistrationRequestDtoV2().getCommunicationInfo().get(0).getContactNo())
					.identityType(request.getIdentityType()).dob(request.getDob()).aadhar(request.getAadhar())
					.PAN(request.getPAN()).build();

			leadMessageBody = LeadMessageBody.builder().individualEntry(indiEntry)
					.ignoreProbableMatch(request.getIgnoreProbableMatch()).entityType(request.getEntityType())
					.fromMADP(request.getFromMADP()).deDupChkReqByCustCount(request.getDeDupChkReqByCustCount())
					.isAadhar(request.getIsAadhar()).mappedToAccountLead(request.getMappedToAccountLead())
					.purposeOfCreation(request.getPurposeOfCreation()).usrId(request.getUsrId())
					.authenticationToken(request.getAuthenticationToken()).entityFlagType(request.getEntityFlagType())
					.build();
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return leadMessageBody;
	}

	public LeadUpdateMessageBody buildUpdateLeadMessageBody(KycRequestDto request) throws Exception {
		LeadUpdateMessageBody leadUpdateMessageBody = null;

		try {

			List<AddressDetails> addressDetails = buildAddressDtls(request);
			AboutLeadDetails aboutLeadDetails = buildAboutLeadDetails(request);
			AboutProspectDetails aboutProspectDetails = buildAboutProspectDetails(request);
			IdentityInfoDetails identityInfoDetails = buildIdentityInfoDetails(request);
			FatcaTaxDetailsIndividual fatcaTaxDetailsIndividual = buildFatcaTaxDetailsIndividual(request);

			leadUpdateMessageBody = LeadUpdateMessageBody.builder().associatedWith(request.getAssociatedWith())
					.isCurrentAddressSameAsPermanent(request.getIsCurrentAddressSameAsPermanent())
					.entityIdentityDetails(null).addressDetails(addressDetails).aboutLeadDetails(aboutLeadDetails)
					.aboutProspectDetails(aboutProspectDetails).ID(request.getCustLeadId())
					.identityInfoDetails(identityInfoDetails).fatcaTaxDetailsIndividual(fatcaTaxDetailsIndividual)
					.custLeadId(request.getCustLeadId()).authenticationToken(request.getAuthenticationToken()).build();
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return leadUpdateMessageBody;
	}

	public UpdateDigiDocumentMessageBody buildDigiDocumentMessageBody(KycRequestDto request) {
		UpdateDigiDocumentMessageBody updateDigiDocumentMessageBody = null;

		try {

			List<Documents> documents = buildDocuments(request);

			updateDigiDocumentMessageBody = UpdateDigiDocumentMessageBody.builder().documents(documents)
					.authToken(request.getAuthToken()).build();
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return updateDigiDocumentMessageBody;
	}

	public AddDocumentMessageBody buildAddDocumentMessageBody(KycRequestDto request) throws Exception {
		AddDocumentMessageBody addDocumentMessageBody = null;

		try {

			SsnAuth ssnAuth = buildSsnAuth(request);

			List<AddDocRq> addDocRq = buildAddDocRq(request);

			addDocumentMessageBody = AddDocumentMessageBody.builder().ssnAuth(ssnAuth).addDocRq(addDocRq).build();
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return addDocumentMessageBody;
	}

	public GenerateDigiOTPMessageBody buildGenerateDigiOTPMessageBody(KycRequestDto request) {
		GenerateDigiOTPMessageBody generateDigiOTPMessageBody = null;

		try {
			String stan = Utils.generateStan();

			String requestXML = "<OtpRequest><TransactionInfo><UID type=\"" + request.getIdTypeValidation() + "\">"
					+ request.getIdNumber() + "</UID>" + "<Transm_Date_time>" + Utils.currentDateTimeMMDDHHmmSS()
					+ "</Transm_Date_time><Stan>" + stan + "</Stan><Local_Trans_Time>" + Utils.currentTimeHHmmss()
					+ "</Local_Trans_Time>" + "<Local_date>" + Utils.currentDateMMDD() + "</Local_date><Mcc>"
					+ request.getMcc() + "</Mcc><Pos_entry_mode>" + request.getPosEntryMode()
					+ "</Pos_entry_mode><Pos_code>" + request.getPosCode() + "</Pos_code>" + "<CA_ID>"
					+ request.getCaId() + "</CA_ID><CA_TID>" + request.getCaTid() + "</CA_TID><CA_TA>"
					+ request.getCaTa() + "</CA_TA>" + "</TransactionInfo><Opts ch='01'/></OtpRequest>";

			generateDigiOTPMessageBody = GenerateDigiOTPMessageBody.builder().eKYCXMLStringReqPayload(requestXML)
					.build();
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return generateDigiOTPMessageBody;
	}

	public DigiCustLeadwitheKYCMessageBody buildDigiCustLeadwitheKYCMessageBody(KycRequestDto request) {

		DigiCustLeadwitheKYCMessageBody digiCustLeadwitheKYCMessageBody = null;
		try {
			String data = null;
			String pidData = null;
			if (request.getRdsVer() != null) {
				data = createPid(request.getSkey(), request.getCi(), request.getMc(), request.getDataType(),
						request.getDataValue(), request.getHmac(), request.getMi(), request.getRdsId(),
						request.getRdsVer(), request.getDpId(), request.getDc());
				pidData = jaxbObjectToXML(request, data);
			} else {
				data = createOtpPid(request.getSkey(), request.getCi(), request.getDataType(), request.getDataValue(), request.getHmac());
				pidData = jaxbObjectToXML(request, data);
			}
			IndividualEntry indiEntry = IndividualEntry.builder().title(request.getTitle())
					.firstName(request.getFirstName()).lastName(request.getLastName())
					.middleName(request.getMiddleName()).motherMaidenName(request.getMotherMaidenName())
					.mobilePhone(request.getRegistrationRequestDtoV2().getCommunicationInfo().get(0).getContactNo())
					.identityType(request.getIdentityType()).dob(request.getDob()).aadhar(request.getAadhar())
					.PAN(request.getRegistrationRequestDtoV2().getKycInfo().get(0).getDocumentNo())
					.shortName(request.getShortName()).iFullName(request.getIfullName()).NLFound(request.getNlFound())
					.reasonNotApplicable(request.getReasonNotApplicable()).reason(request.getReason())
					.voterid(request.getVoterid()).purposeOfCreation(request.getPurposeOfCreation()).build();

			EKycDetails eKYCDetails = EKycDetails.builder().eKYCXMLStringReqPayload(pidData).build();

			LeadDetails leadDetails = LeadDetails.builder().individualEntry(indiEntry)
					.authenticationToken(request.getAuthenticationToken()).entityType(request.getEntityType())
					.entityFlagType(request.getEntityFlagType()).usrId(request.getUsrId())
					.deDupChkReqByCustCount(request.getDeDupChkReqByCustCount()).fromMADP(request.getFromMADP())
					.isAadhar(request.getIsAadhar()).ignoreProbableMatch(request.getIgnoreProbableMatch())
					.mappedToAccountLead(request.getMappedToAccountLead()).build();

			digiCustLeadwitheKYCMessageBody = DigiCustLeadwitheKYCMessageBody.builder().eKYCDetails(eKYCDetails)
					.leadDetails(leadDetails).build();
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return digiCustLeadwitheKYCMessageBody;

	}

	public AuthInfo buildAuth(KycRequestDto request) {
		return AuthInfo.builder().branchId(request.getBranchId()).authUserId(request.getAuthUserId()).build();
	}
	
	public AuthInfo buildAuth(DigiAccountRequestDto request) {
		return AuthInfo.builder().branchId(request.getBrnchId()).authUserId(request.getAuthUserId()).build();
	}

	public AboutLeadDetails buildAboutLeadDetails(KycRequestDto request) {
		return AboutLeadDetails.builder().sourceBranch(request.getSourceBranch()).homeBranch(request.getHomeBranch())
				.sourceOfLead("11").otherSourcesOfLead("M2P " + request.getCorporateName()).build();
	}

	public AboutProspectDetails buildAboutProspectDetails(KycRequestDto request) {
		return AboutProspectDetails.builder().lastName(request.getLastName()).fatherName(request.getFatherName())
				.preferredLanguage(request.getPreferredLanguage()).gender(request.getGender())
				.politicallyExposedPerson(request.getPoliticallyExposedPerson())
				.mobileNumber(request.getRegistrationRequestDtoV2().getCommunicationInfo().get(0).getContactNo())
				.emailID(request.getEmailID()).title(request.getTitle()).employeeNumber(request.getEmployeeNumber())
				.priorityLead(request.getPriorityLead()).firstName(request.getFirstName())
				.nationality(request.getNationality()).dob(request.getDob()).middleName(request.getMiddleName())
				.age(request.getAge()).maritalStatus(request.getMaritalStatus()).occupation(request.getOccupation())
				.grossAnnualIncome(request.getGrossAnnualIncome()).occuapation(request.getOccuapation()).build();
	}

	public IdentityInfoDetails buildIdentityInfoDetails(KycRequestDto request) {
		return IdentityInfoDetails.builder().estimatedNonAgriculturalIncome(request.getEstimatedNonAgriculturalIncome())
				.estimatedAgriculturalIncome(request.getEstimatedAgriculturalIncome()).build();
	}

	public FatcaTaxDetailsIndividual buildFatcaTaxDetailsIndividual(KycRequestDto request) {
		return FatcaTaxDetailsIndividual.builder().cityOfBirth(request.getCityOfBirth())
				.countryOfBirth(request.getCountryOfBirth()).taxResident(request.getTaxResident()).build();
	}

	public List<AddressDetails> buildAddressDtls(KycRequestDto request) throws Exception {

		AddressDetails addrDtls = null;

		List<AddressDetails> alladdress = new ArrayList<>();

		try {

			DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
			df.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, ""); 
			df.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
			df.setFeature("http://xml.org/sax/features/external-general-entities", false);
			df.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			df.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
			df.setValidating(true);
			DocumentBuilder builder = df.newDocumentBuilder();
			InputSource src = new InputSource();
			src.setCharacterStream(new StringReader(request.getEkycxmlreqPayload()));
			Document doc = builder.parse(src);

			NodeList poa = doc.getElementsByTagName("Poa");
			String pincode = null;
			String addLine1 = null;
			String addLine2 = null;
			String addLine3 = null;
			String city = null;
			String district = null;
			String state = null;
			String subDistrict = null;
			String postOffice = null;
			
			for (int j = 0; j < poa.getLength(); j++) {
				Node child = poa.item(j);
				if (child.getNodeType() == Node.ELEMENT_NODE) {
					Element check = (Element) child;
					String loc = check.getAttribute("loc");
					String street = check.getAttribute("street");
					String pc = check.getAttribute("pc");
					String co = check.getAttribute("co");
					String lm = check.getAttribute("lm");
					String vtc = check.getAttribute("vtc");
					String dist = check.getAttribute("dist");
					String st = check.getAttribute("state");
					String house = check.getAttribute("house");
					String subdist = check.getAttribute("subdist");
					String po = check.getAttribute("po");

					if (!pc.isEmpty()) {
						pincode = Objects.toString(poa.item(0).getAttributes().getNamedItem("pc").getNodeValue(), "");
					}
					if (!co.isEmpty()) {
						co = Objects.toString(poa.item(0).getAttributes().getNamedItem("co").getNodeValue(), "");
					}
					if (!lm.isEmpty()) {
						lm = Objects.toString(poa.item(0).getAttributes().getNamedItem("lm").getNodeValue(), "");
					}
					if (!vtc.isEmpty()) {
						city = Objects.toString(poa.item(0).getAttributes().getNamedItem("vtc").getNodeValue(), "");
					}
					if (!dist.isEmpty()) {
						district = Objects.toString(poa.item(0).getAttributes().getNamedItem("dist").getNodeValue(), "");
					}
					if (!st.isEmpty()) {
						state = Objects.toString(poa.item(0).getAttributes().getNamedItem("state").getNodeValue(), "");
					}
					if (!loc.isEmpty()) {
						addLine3 = Objects.toString(poa.item(0).getAttributes().getNamedItem("loc").getNodeValue(), "");
					} 
					if (!street.isEmpty()) {
						street = Objects.toString(poa.item(0).getAttributes().getNamedItem("street").getNodeValue(), "");
					}
					if (!house.isEmpty()) {
						house = Objects.toString(poa.item(0).getAttributes().getNamedItem("house").getNodeValue(), "");
					}
					if (!subdist.isEmpty()) {
						subDistrict = Objects.toString(poa.item(0).getAttributes().getNamedItem("subdist").getNodeValue(), "");
					}
					if (!po.isEmpty()) {
						postOffice = Objects.toString(poa.item(0).getAttributes().getNamedItem("po").getNodeValue(), "");
					}
					if (!co.isEmpty() && !house.isEmpty()) {
						addLine1 = co +" "+ house;
					} else if(!co.isEmpty()) {
						addLine1 = co;
					} else if(!house.isEmpty()) {
						addLine1 = house;
					} else {
						addLine1 = null;
					}
					
					if (!street.isEmpty() && !lm.isEmpty()) {
						addLine2 = street +" "+ lm;
					} else if(!street.isEmpty()) {
						addLine2 = street;
					} else if(!lm.isEmpty()) {
						addLine2 = lm;
					} else {
						addLine2 = null;
					}
					
				}
			}

			logger.info("pincode :: {}", pincode);
			logger.info("addLine1 :: {}", addLine1);
			logger.info("addLine2 :: {}", addLine2);
			logger.info("addLine3 :: {}", addLine3);
			logger.info("city :: {}", city);
			logger.info("district :: {}", district);
			logger.info("state :: {}", state);
			logger.info("subDistrict :: {}", subDistrict);
			logger.info("postOffice :: {}", postOffice);
			
			request.setCity(city);
			request.setState(state);
			request.setCountry("India");
			request.setCityOfBirth(city);
			
			if (request.getIsCurrentAddressSameAsPermanent()) {

				fetchGeoMasterData(pincode, city, request);

				for (int i = 0; i < 2; i++) {

					addrDtls = AddressDetails.builder().country(request.getCountry()).pincode(pincode)
							.city(request.getCity()).addressType(Integer.toString(i + 1))
							.mobileNumber(
									request.getRegistrationRequestDtoV2().getCommunicationInfo().get(0).getContactNo())
							.district(request.getDistrict()).state(request.getState())
							.landline(
									request.getRegistrationRequestDtoV2().getCommunicationInfo().get(0).getContactNo())
							.landmark(addLine1).addressLine1(addLine1).addressLine2(addLine2).addressLine3(addLine3)
							.build();

					alladdress.add(addrDtls);
				}
				buildRegV2Req(request, alladdress);
			} else {

				fetchGeoMasterData(pincode, city, request);

				AddressDetails adhaaraddrDtls = AddressDetails.builder().country(request.getCountry()).pincode(pincode)
						.city(request.getCity()).addressType(Integer.toString(1))
						.mobileNumber(
								request.getRegistrationRequestDtoV2().getCommunicationInfo().get(0).getContactNo())
						.district(request.getDistrict()).state(request.getState())
						.landline(request.getRegistrationRequestDtoV2().getCommunicationInfo().get(0).getContactNo())
						.landmark(addLine1).addressLine1(addLine1).addressLine2(addLine2).addressLine3(addLine3)
						.build();

				fetchGeoMasterData(request.getRegistrationRequestDtoV2().getAddressInfo().get(0).getPincode(),
						request.getRegistrationRequestDtoV2().getAddressInfo().get(0).getCity(), request);

				AddressDetails communicationaddrDtls = AddressDetails.builder()
						.country(request.getRegistrationRequestDtoV2().getAddressInfo().get(0).getCountry())
						.pincode(request.getRegistrationRequestDtoV2().getAddressInfo().get(0).getPincode())
						.city(request.getRegistrationRequestDtoV2().getAddressInfo().get(0).getCity())
						.addressType(Integer.toString(2))
						.mobileNumber(
								request.getRegistrationRequestDtoV2().getCommunicationInfo().get(0).getContactNo())
						.district(request.getRegistrationRequestDtoV2().getAddressInfo().get(0).getDistrict())
						.state(request.getRegistrationRequestDtoV2().getAddressInfo().get(0).getState())
						.landline(request.getRegistrationRequestDtoV2().getCommunicationInfo().get(0).getContactNo())
						.landmark(request.getRegistrationRequestDtoV2().getAddressInfo().get(0).getAddress1())
						.addressLine1(request.getRegistrationRequestDtoV2().getAddressInfo().get(0).getAddress1())
						.addressLine2(request.getRegistrationRequestDtoV2().getAddressInfo().get(0).getAddress2())
						.addressLine3(request.getRegistrationRequestDtoV2().getAddressInfo().get(0).getAddress3())
						.build();

				alladdress.add(adhaaraddrDtls);
				alladdress.add(communicationaddrDtls);

				buildRegV2Req(request, alladdress);

			}

		} catch (Exception ex) {

			ex.printStackTrace();

			if (ex instanceof NeoException)
				throw ex;

		}

		return alladdress;
	}

	public List<Documents> buildDocuments(KycRequestDto request) {

		Documents docs = null;
		List<Documents> allDocs = new ArrayList<>();

		try {
			if (request.getEkycxmlreqPayload() != null) {
				docs = Documents.builder().documentNo(request.getDocumentNo())
						.associatedWith(request.getAssociatedWith()).documentType(Integer.toString(14))
						.mappedCustomerLead(request.getCustLeadId()).categoryCode(Integer.toString(70))
						.dmsDocumentId(request.getDmsDocumentId()).subcategoryCode(Integer.toString(12)).build();

				allDocs.add(docs);
			} else {
				docs = Documents.builder().documentNo(request.getDocumentNo())
						.associatedWith(request.getAssociatedWith())
						.documentType(Integer.toString(request.getDocTypCd()))
						.mappedCustomerLead(request.getCustLeadId()).categoryCode(Integer.toString(70))
						.dmsDocumentId(request.getDmsDocumentId())
						.subcategoryCode(Integer.toString(request.getDocSbCtgryCd())).build();

				allDocs.add(docs);
			}

		} catch (Exception ex) {

			ex.printStackTrace();

			if (ex instanceof NeoException)
				throw ex;

		}

		return allDocs;
	}

	public SsnAuth buildSsnAuth(KycRequestDto request) {
		return SsnAuth.builder().usrTkn(request.getUsrTkn()).usrNm(request.getUsrNm()).usrPwd(request.getUsrPwd())
				.build();
	}

	public List<AddDocRq> buildAddDocRq(KycRequestDto request) throws Exception {

		AddDocRq docs = null;
		DocRefId docRefId = null;
		List<AddDocRq> allDocs = new ArrayList<>();
		List<DocRefId> allDocRef = new ArrayList<>();

		try {

			for (int i = 0; i < 2; i++) {
				if (i == 0) {
					docRefId = DocRefId.builder().idTp("LEDID").id(Integer.parseInt(request.getCustLeadId())).build();
					allDocRef.add(docRefId);
				}
				if (i == 1) {
					docRefId = DocRefId.builder().idTp("BRNCH").id(Integer.parseInt(request.getUsrId())).build();
					allDocRef.add(docRefId);
				}

			}
			if (request.getEkycxmlreqPayload() != null) {
				DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
				df.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, ""); 
				df.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
				df.setFeature("http://xml.org/sax/features/external-general-entities", false);
				df.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
				df.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
				df.setValidating(true);
				DocumentBuilder builder = df.newDocumentBuilder();

				InputSource src = new InputSource();
				src.setCharacterStream(new StringReader(request.getEkycxmlreqPayload()));

				Document doc = builder.parse(src);
				String prn = doc.getElementsByTagName("Prn").item(0).getTextContent();

				docs = AddDocRq.builder().docTp("LEAD").docSbCtgry("AADHAR_CARD")
						.docNm("AADHAR_CARD" + Utils.uniqueIdentifier() + ".pdf").docCtgryCd(70).docCatg("ACCOPENREQ")
						.docTypCd(14).docSbCtgryCd(12).flLoc("").docCmnts("Addition of document for Lead Creation")
						.bsPyld(prn).docRefId(allDocRef).build();

				allDocs.add(docs);

			} else {
				docs = AddDocRq.builder().docTp("LEAD").docSbCtgry(request.getDocSbCtgry())
						.docNm(request.getDocSbCtgry() + Utils.uniqueIdentifier() + ".pdf").docCtgryCd(70)
						.docCatg("ACCOPENREQ").docTypCd(request.getDocTypCd()).docSbCtgryCd(request.getDocSbCtgryCd())
						.flLoc("").docCmnts("Addition of document for Lead Creation").bsPyld(request.getBsPyld())
						.docRefId(allDocRef).build();

				allDocs.add(docs);
			}

		} catch (Exception ex) {

			ex.printStackTrace();

			if (ex instanceof NeoException)
				throw ex;

		}

		return allDocs;
	}

	public String jaxbObjectToXML(KycRequestDto requestDto, String data) {
		String xmlContent = null;
		KycReqInfo kycReqInfo = null;
		try {

			if (requestDto.getStan() == null) {
				requestDto.setStan(Utils.generateStan());
			}

			UID uid = UID.builder().type(requestDto.getIdTypeValidation()).uidData(requestDto.getIdNumber()).build();
			Auth auth = Auth.builder().txn("UKC:" + requestDto.getStan()).txnValue(data).build();
			
			if (requestDto.getWadh() != null) {
				kycReqInfo = KycReqInfo.builder().de(requestDto.getWadh().getDe()).lr(requestDto.getWadh().getLr())
						.pfr(requestDto.getWadh().getPfr()).ra(requestDto.getWadh().getRa()).rc(requestDto.getWadh().getRc())
						.ver(requestDto.getWadh().getVer()).auth(auth).build();
			} else {
				kycReqInfo = KycReqInfo.builder().de("N").lr("Y").pfr("Y").ra("I").rc("Y").ver("2.5").auth(auth)
						.build();
			}

			TransactionInfo transactionInfo = TransactionInfo.builder().uid(uid)
					.transmDateTime(Utils.currentDateTimeMMDDHHmmSS()).stan(requestDto.getStan())
					.localTransTime(Utils.currentTimeHHmmss()).localDate(Utils.currentDateMMDD())
					.caId(requestDto.getCaId()).caTid(requestDto.getCaTid()).caTa(requestDto.getCaTa()).build();
			KycRequestData kycReqdata = KycRequestData.builder().transactionInfo(transactionInfo).kycReqInfo(kycReqInfo)
					.build();

			JAXBContext jaxbContext = JAXBContext.newInstance(KycRequestData.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			StringWriter stringWriter = new StringWriter();
			DataWriter dataWriter = new DataWriter(stringWriter, "UTF-8", new JaxbCharacterEscapeHandler());
			jaxbMarshaller.marshal(kycReqdata, dataWriter);

			xmlContent = stringWriter.toString();

			xmlContent = xmlContent.replaceAll("\\n", "");
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return xmlContent;
	}

//	public String otpPid(String otp) {
//
//		String publicKeyFileName = "uidai_auth_encrypt_preprod.cer";
//		InputStream inputStream = null;
//		String requestXML = null;
//
//		try {
//			inputStream = new ClassPathResource(publicKeyFileName).getInputStream();
//		} catch (Exception e) {
//
//			throw new RuntimeException("Exception in creating input stream", e);
//		}
//		UIDFactory uidFactory = UIDBuilder.getNewUIDFactoryInstance(UIDConstants.AUTH_TRANSACTION, inputStream);
//		uidFactory.setOtp(otp);
//		uidFactory.generatePIDBlock();
//		UIDBlock uidBlock = uidFactory.getPIDBlock();
//
//		String skey = uidBlock.getBase64EncryptedSessionKey();
//		String data = uidBlock.getBase64EncryptedPID();
//		String hmac = uidBlock.getBase64EncryptedHMAC();
//
//		requestXML = "<Uses pi=\"n\" pa=\"n\" pfa=\"n\"  bio=\"n\" otp=\"y\"/>" + "<Meta/>" + "<Skey ci=\"20201030\">"
//				+ skey + "</Skey>" + "<Data type=\"P\">" + data + "</Data>" + "<Hmac>" + hmac + "</Hmac>";
//
//		return requestXML;
//	}
	
	public String createPid(String skey, String ci, String mc, String dataType, String dataValue, String hmac, String mi, String rdsId, String rdsVer, String dpId, String dc) {

		String requestXML = null;

		requestXML = "<Uses pi=\"n\" pa=\"n\" pfa=\"n\" pin=\"n\" bio=\"y\" bt=\"FMR\" otp=\"n\"/>"
				+ "<Meta udc=\"EQT000000000001\" rdsId=\"" + rdsId + "\" rdsVer=\"" + rdsVer + "\" dpId=\"" + dpId + "\" dc=\"" + dc + "\" mi=\"" + mi + "\" mc=\"" + mc + "\"/> "
				+ "<Skey ci=\"" + ci + "\">" + skey + "</Skey> "
				+ "<Data type=\"" + dataType +"\">" + dataValue + "</Data> "
				+ "<Hmac>" + hmac + "</Hmac>";

		return requestXML;
	}

	public String createOtpPid(String skey, String ci, String dataType, String dataValue, String hmac) {

		String requestXML = null;

		requestXML = "<Uses pi=\"n\" pa=\"n\" pfa=\"n\" bio=\"n\" otp=\"y\"/><Meta/><Skey ci=\""+ci+"\">"+skey+"</Skey><Data type=\""+dataType+"\">"+dataValue+"</Data><Hmac>"+hmac+"</Hmac>";

		return requestXML;
	}
	public void fetchFromAadhar(String eKYCXMLStringRepPayload, KycRequestDto request) {
		try {
			DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
			df.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, ""); 
			df.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
			df.setFeature("http://xml.org/sax/features/external-general-entities", false);
			df.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			df.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
			df.setValidating(true);
			DocumentBuilder builder = df.newDocumentBuilder();
			InputSource src = new InputSource();
			src.setCharacterStream(new StringReader(eKYCXMLStringRepPayload));
			Document doc = builder.parse(src);

			NodeList poi = doc.getElementsByTagName("Poi");
			String gender = poi.item(0).getAttributes().getNamedItem("gender").getNodeValue();
			String dob = poi.item(0).getAttributes().getNamedItem("dob").getNodeValue();
			String[] fullname = poi.item(0).getAttributes().getNamedItem("name").getNodeValue().split(" ", 3);
			String firstName = null;
			String lastName = null;
			String middleName = null;
			if (fullname.length != 0) {
				if (fullname.length == 1) {
					firstName = fullname[0];
				} else if (fullname.length == 2) {
					firstName = fullname[0];
					lastName = fullname[1];
				} else if (fullname.length == 3) {
					firstName = fullname[0];
					lastName = fullname[1];
					middleName = fullname[2];
				}

				request.setFirstName(firstName);
				request.setLastName(lastName);
				request.setMiddleName(middleName);
				request.getRegistrationRequestDtoV2().setFirstName(firstName);
				request.getRegistrationRequestDtoV2().setLastName(lastName);
			}

			String pht = doc.getElementsByTagName("Pht").item(0).getTextContent();
			NodeList resp = doc.getElementsByTagName("resp");
			String authCode = resp.item(0).getAttributes().getNamedItem("code").getNodeValue();
			String rrn = doc.getElementsByTagName("RRN").item(0).getTextContent();
			String name = poi.item(0).getAttributes().getNamedItem("name").getNodeValue();

			if (request.getIsPhtRequired()) {
				request.getRegistrationRequestDtoV2().setPhoto(pht);
			}
			request.getRegistrationRequestDtoV2().setAuthCode(authCode);
			request.getRegistrationRequestDtoV2().setRrn(rrn);
			request.getRegistrationRequestDtoV2().setName(name);

			request.setGender(Enum.valueOf(GenderType.class, gender));
			request.getRegistrationRequestDtoV2().setGender(Enum.valueOf(GenderType.class, gender));
			if (gender.equals("M")) {
				request.setTitle("1");
				request.getRegistrationRequestDtoV2().setTitle("Mr");
				request.getRegistrationRequestDtoV2().setGender(GenderType.M);

			} else if (gender.equals("F")) {
				request.setTitle("15");
				request.getRegistrationRequestDtoV2().setTitle("Miss");
				request.getRegistrationRequestDtoV2().setGender(GenderType.F);
			}
			
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
			DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");

			String aadharDob = LocalDate.parse(dob, formatter).format(formatter2);
			request.setDob(aadharDob);
			request.getRegistrationRequestDtoV2().setDob(aadharDob);
			formatSpecialDate(request, dob); 
			String[] dateOfBirth = dob.split("-");
			String day = dateOfBirth[0];
			String month = dateOfBirth[1];
			String year = dateOfBirth[2];

			LocalDate start = LocalDate.of(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day));
			int years = (int) ChronoUnit.YEARS.between(start, LocalDate.now());
			request.setAge(years);
			
			NodeList poa = doc.getElementsByTagName("Poa");
			String pincode = null;
			String addLine1 = null;
			String addLine2 = null;
			String addLine3 = null;
			String city = null;
			String district = null;
			String state = null;
			String subDistrict = null;
			String postOffice = null;
			
			for (int j = 0; j < poa.getLength(); j++) {
				Node child = poa.item(j);
				if (child.getNodeType() == Node.ELEMENT_NODE) {
					Element check = (Element) child;
					String loc = check.getAttribute("loc");
					String street = check.getAttribute("street");
					String pc = check.getAttribute("pc");
					String co = check.getAttribute("co");
					String lm = check.getAttribute("lm");
					String vtc = check.getAttribute("vtc");
					String dist = check.getAttribute("dist");
					String st = check.getAttribute("state");
					String house = check.getAttribute("house");
					String subdist = check.getAttribute("subdist");
					String po = check.getAttribute("po");

					if (!pc.isEmpty()) {
						pincode = Objects.toString(poa.item(0).getAttributes().getNamedItem("pc").getNodeValue(), "");
					}
					if (!co.isEmpty()) {
						co = Objects.toString(poa.item(0).getAttributes().getNamedItem("co").getNodeValue(), "");
					}
					if (!lm.isEmpty()) {
						lm = Objects.toString(poa.item(0).getAttributes().getNamedItem("lm").getNodeValue(), "");
					}
					if (!vtc.isEmpty()) {
						city = Objects.toString(poa.item(0).getAttributes().getNamedItem("vtc").getNodeValue(), "");
					}
					if (!dist.isEmpty()) {
						district = Objects.toString(poa.item(0).getAttributes().getNamedItem("dist").getNodeValue(), "");
					}
					if (!st.isEmpty()) {
						state = Objects.toString(poa.item(0).getAttributes().getNamedItem("state").getNodeValue(), "");
					}
					if (!loc.isEmpty()) {
						addLine3 = Objects.toString(poa.item(0).getAttributes().getNamedItem("loc").getNodeValue(), "");
					} 
					if (!street.isEmpty()) {
						street = Objects.toString(poa.item(0).getAttributes().getNamedItem("street").getNodeValue(), "");
					}
					if (!house.isEmpty()) {
						house = Objects.toString(poa.item(0).getAttributes().getNamedItem("house").getNodeValue(), "");
					}
					if (!subdist.isEmpty()) {
						subDistrict = Objects.toString(poa.item(0).getAttributes().getNamedItem("subdist").getNodeValue(), "");
					}
					if (!po.isEmpty()) {
						postOffice = Objects.toString(poa.item(0).getAttributes().getNamedItem("po").getNodeValue(), "");
					}
					if (!co.isEmpty() && !house.isEmpty()) {
						addLine1 = co +" "+ house;
					} else if(!co.isEmpty()) {
						addLine1 = co;
					} else if(!house.isEmpty()) {
						addLine1 = house;
					} else {
						addLine1 = null;
					}
					
					if (!street.isEmpty() && !lm.isEmpty()) {
						addLine2 = street +" "+ lm;
					} else if(!street.isEmpty()) {
						addLine2 = street;
					} else if(!lm.isEmpty()) {
						addLine2 = lm;
					} else {
						addLine2 = null;
					}
				}
			}

			if (subDistrict != null && !subDistrict.isEmpty() && postOffice != null && !postOffice.isEmpty()) {
				request.setAdd((Objects.toString(addLine1, "") + " " + Objects.toString(addLine2, "") + " "
						+ Objects.toString(addLine3, "") + " " + " City: " + city + " Sub District: " + subDistrict
						+ " District: " + district + " State: " + state + " Pincode: " + pincode + " Post Office: "
						+ postOffice).trim().replaceAll(" +", " "));
			} else if (subDistrict != null && !subDistrict.isEmpty()) {
				request.setAdd((Objects.toString(addLine1, "") + " " + Objects.toString(addLine2, "") + " "
						+ Objects.toString(addLine3, "") + " " + " City: " + city + " Sub District: " + subDistrict
						+ " District: " + district + " State: " + state + " Pincode: " + pincode).trim()
								.replaceAll(" +", " "));
			} else if (postOffice != null && !postOffice.isEmpty()) {
				request.setAdd((Objects.toString(addLine1, "") + " " + Objects.toString(addLine2, "") + " "
						+ Objects.toString(addLine3, "") + " " + " City: " + city + " District: " + district
						+ " State: " + state + " Pincode: " + pincode + " Post Office: " + postOffice).trim()
								.replaceAll(" +", " "));
			} else {
				request.setAdd((Objects.toString(addLine1, "") + " " + Objects.toString(addLine2, "") + " "
						+ Objects.toString(addLine3, "") + " " + " City: " + city + " District: " + district
						+ " State: " + state + " Pincode: " + pincode).trim().replaceAll(" +", " "));
			}
			
			request.setCity(city);
			request.setState(state);
			request.setCountry("India");
			request.setDistrict(district);
			request.getRegistrationRequestDtoV2().setCity(city);
			request.getRegistrationRequestDtoV2().setState(state);
			request.getRegistrationRequestDtoV2().setCountry("India");
			request.getRegistrationRequestDtoV2().setDistrict(district);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}

	}

	private void formatSpecialDate(KycRequestDto request, String dob) {
		try {
			SimpleDateFormat form = new SimpleDateFormat("dd-MM-yyyy");
			Date date = form.parse(dob);

			SpecialDatesDto sdd = new SpecialDatesDto();
			sdd.setDateType(SpecialDateType.DOB);
			sdd.setDate(date);
			List<SpecialDatesDto> sd = new ArrayList<>();
			sd.add(sdd);
			request.getRegistrationRequestDtoV2().setDateInfo(sd);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public String fetchStan(String eKYCXMLStringRepPayload, KycRequestDto request) {
		try {
			DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
			df.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, ""); 
			df.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
			df.setFeature("http://xml.org/sax/features/external-general-entities", false);
			df.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			df.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
			df.setValidating(true);
			DocumentBuilder builder = df.newDocumentBuilder();
			InputSource src = new InputSource();
			src.setCharacterStream(new StringReader(eKYCXMLStringRepPayload));
			Document doc = builder.parse(src);
			return doc.getElementsByTagName("Stan").item(0).getTextContent();
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	public KycRequestDto fetchGeoMasterData(String pincode, String cityName, KycRequestDto request)
			throws NeoException {
		GeoMaster gm = gmDao.findByPincodeAndCityName(pincode, cityName);
		if (gm == null) {
			gm = gmDao.findByPincodeAndTopCityName(pincode, cityName);
			if (gm == null) {
				String[] city = cityName.split(" ");
			      for(String loc : city) {
			    	  gm = gmDao.findByPincodeAndTopCityName(pincode, loc);
			    	  if(gm!=null) {
			    		  break;
			    	  }
			      }
			}
			if (gm == null) {
				gm = gmDao.findByPincode(pincode);
			}
			if (gm == null) {
				throw exceptionBuilder.build("Geo master data does not match.", null, Locale.US, null);
			}
		}
		if (request.getIsCurrentAddressSameAsPermanent()) {
			request.setCountry(gm.getCountryId());
			request.setCity(gm.getCityId());
			request.setDistrict(gm.getDistrictId());
			request.setState(gm.getStateId());
			request.setPincode(gm.getPincode());
		} else {
			List<AddressDto> addDto = new ArrayList<>();
			AddressDto per = new AddressDto();

			per.setCountry(gm.getCountryId());
			per.setCity(gm.getCityId());
			per.setState(gm.getStateId());
			per.setDistrict(gm.getDistrictId());
			per.setPincode(gm.getPincode());
			addDto.add(per);
			request.getRegistrationRequestDtoV2().setAddressInfo(addDto);
		}
		return request;

	}
	
	public KycRequestDto buildRegV2Req(KycRequestDto request,
			List<AddressDetails> alladdress) {

		request.getRegistrationRequestDtoV2().setCustLeadId(request.getCustLeadId());
		List<AddressDto> addDto = new ArrayList<>();
		AddressDto per = new AddressDto();
		per.setAddressCategory(AddressCategory.PERMANENT);

		AddressDto com = new AddressDto();
		com.setAddressCategory(AddressCategory.COMMUNICATION);

		if (request.getIsCurrentAddressSameAsPermanent()) {

			per.setAddress1(alladdress.get(0).getAddressLine1());
			per.setAddress2(alladdress.get(0).getAddressLine2());
			per.setAddress3(alladdress.get(0).getAddressLine3());
			per.setCity(alladdress.get(0).getCity());
			per.setCountry(alladdress.get(0).getCountry());
			per.setPincode(alladdress.get(0).getPincode());
			per.setState(alladdress.get(0).getState());

			com.setAddress1(alladdress.get(0).getAddressLine1());
			com.setAddress2(alladdress.get(0).getAddressLine2());
			com.setAddress3(alladdress.get(0).getAddressLine3());
			com.setCity(alladdress.get(0).getCity());
			com.setCountry(alladdress.get(0).getCountry());
			com.setPincode(alladdress.get(0).getPincode());
			com.setState(alladdress.get(0).getState());

			addDto.add(per);
			addDto.add(com);

			request.getRegistrationRequestDtoV2().setAddressInfo(addDto);
		} else {
			per.setAddress1(alladdress.get(0).getAddressLine1());
			per.setAddress2(alladdress.get(0).getAddressLine2());
			per.setAddress3(alladdress.get(0).getAddressLine3());
			com.setAddress1(alladdress.get(1).getAddressLine1());
			com.setAddress2(alladdress.get(1).getAddressLine2());
			com.setAddress3(alladdress.get(1).getAddressLine3());
			com.setCity(alladdress.get(1).getCity());
			com.setCountry(alladdress.get(1).getCountry());
			com.setPincode(alladdress.get(1).getPincode());
			com.setState(alladdress.get(1).getState());

			addDto.add(per);
			addDto.add(com);

			request.getRegistrationRequestDtoV2().setAddressInfo(addDto);
		}

		return request;

	}

	public AccountLeadMessageBody buildAccountLeadMessageBody(DigiAccountRequestDto request) {

		AccountLeadMessageBody accountLeadMessageBody = null;

		try {
			List<CustomerAccountLeadRelation> customerAccountLeadRelation = buildCustomerAccountLeadRelation(request.getCustomerAccountLeadRelationDto());
			AccountLead accountLead = buildAccountLead(request.getAccountLeadDto());

			accountLeadMessageBody = accountLeadMessageBody.builder()
					.customerAccountLeadRelation(customerAccountLeadRelation).accountLead(accountLead)
					.fromMADP(request.getFromMADP()).usrId(request.getUsrId()).accountLeadId(request.getAccountLeadID())
					.authenticationToken(request.getAuthenticationToken()).build();

		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return accountLeadMessageBody;
	}

	private AccountLead buildAccountLead(@CheckForNull AccountLeadDto accountLeadDto) {
		AccountLead accountLead = null;
		try {
			accountLead = accountLead.builder().sourceBranch(accountLeadDto.getSourceBranch())
					.isPriorityLead(accountLeadDto.getIsPriorityLead())
					.sourcingChannel(accountLeadDto.getSourcingChannel())
					.initialDepositType(accountLeadDto.getInitialDepositType())
					.accountTitle(accountLeadDto.getAccountTitle()).isDelete(accountLeadDto.getIsDelete())
					.fieldEmployeeCode(accountLeadDto.getFieldEmployeeCode())
					.accountType(accountLeadDto.getAccountType())
					.accountOpeningFlow(accountLeadDto.getAccountOpeningFlow())
					.currencyOfDeposit(accountLeadDto.getCurrencyOfDeposit())
					.productCategory(accountLeadDto.getProductCategory()).sourceOfFund(accountLeadDto.getSourceOfFund())
					.initialDepositAmount(accountLeadDto.getInitialDepositAmount())
					.initialDepositMode(accountLeadDto.getInitialDepositMode())
					.productCode(accountLeadDto.getProductCode()).modeOfOperation(accountLeadDto.getModeOfOperation())
					.isRestrictedAccount(accountLeadDto.getIsRestrictedAccount())
					.sourceofLead(accountLeadDto.getSourceofLead())
					.otherSourceOfLead(accountLeadDto.getOtherSourceOfLead())
					.fatcaDeclaration(accountLeadDto.getFatcaDeclaration())
					.accountOpeningBranch(accountLeadDto.getAccountOpeningBranch())
					.MICreditToESFBAccountNo(accountLeadDto.getMICreditToESFBAccountNo())
					.leadAssignedTo(accountLeadDto.getLeadAssignedTo())
					.cashTransactionRefNo(accountLeadDto.getCashTransactionRefNo())
					.transactionNarration(accountLeadDto.getTransactionNarration())
					.accountOpeningValueDate(accountLeadDto.getAccountOpeningValueDate())
					.sourceOfFund(accountLeadDto.getSourceOfFund())
					.purposeOfOpeningAccount(accountLeadDto.getPurposeOfOpeningAccount())
					.interestPayout(accountLeadDto.getInterestPayout())
					.interestCompoundFrequency(accountLeadDto.getInterestCompoundFrequency())
					.modeOfOperation(accountLeadDto.getModeOfOperation()).tenureDays(accountLeadDto.getTenureDays())
					.MICreditToOtherBankAccountNo(accountLeadDto.getMICreditToOtherBankAccountNo())
					.chequeIssuedBank(accountLeadDto.getChequeIssuedBank())
					.accountOpeningBranch(accountLeadDto.getAccountOpeningBranch())
					.iPayToOtherBankName(accountLeadDto.getIPayToOtherBankName())
					.depositAmount(accountLeadDto.getDepositAmount())
					.iPayToOtherBankIFSC(accountLeadDto.getIPayToOtherBankIFSC())
					.tenureMonths(accountLeadDto.getTenureMonths()).sourceBranch(accountLeadDto.getSourceBranch())
					.chequeNo(accountLeadDto.getChequeNo()).isDelete(accountLeadDto.getIsDelete())
					.maturityInstruction(accountLeadDto.getMaturityInstruction())
					.accountType(accountLeadDto.getAccountType())
					.accountOpeningFlow(accountLeadDto.getAccountOpeningFlow())
					.MICreditToOtherBankBranch(accountLeadDto.getMICreditToOtherBankBranch())
					.iPayToOtherBankBranch(accountLeadDto.getIPayToOtherBankBranch())
					.iPayToOtherBankBenificiaryName(accountLeadDto.getIPayToOtherBankBenificiaryName())
					.initialDepositAmount(accountLeadDto.getInitialDepositAmount())
					.MICreditToOtherBankIFSC(accountLeadDto.getMICreditToOtherBankIFSC())
					.initialDepositMode(accountLeadDto.getInitialDepositMode())
					.iPayToESFBAccountNo(accountLeadDto.getIPayToESFBAccountNo())
					.productVariant(accountLeadDto.getProductVariant()).sourceofLead(accountLeadDto.getSourceofLead())
					.fromESFBAccountNo(accountLeadDto.getFromESFBAccountNo())
					.iPayToOtherBankAccountNo(accountLeadDto.getIPayToESFBAccountNo())
					.otherSourceOfLead(accountLeadDto.getOtherSourceOfLead())
					.MICreditToOtherBankName(accountLeadDto.getMICreditToOtherBankName()).build();
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return accountLead;
	}

	private List<CustomerAccountLeadRelation> buildCustomerAccountLeadRelation(List<CustomerAccountLeadRelationDto> request) {
		CustomerAccountLeadRelation customerAccountLeadRelation =null;
		List<CustomerAccountLeadRelation> allCustomerAccountLeadRelation = new ArrayList<>();
		try {
		for (CustomerAccountLeadRelationDto custLeadRel : request) {
			customerAccountLeadRelation = customerAccountLeadRelation.builder().isPrimaryHolder(custLeadRel.getIsPrimaryHolder())
					.customerAccountRelation(custLeadRel.getCustomerAccountRelation()).id(custLeadRel.getId())
					.completedDataEntryD0(custLeadRel.getCompletedDataEntryD0()).isInsert(custLeadRel.getIsInsert())
					.ucic(custLeadRel.getUcic()).build();
			
			allCustomerAccountLeadRelation.add(customerAccountLeadRelation);
		}
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return null;
	}

	public FetchAccountLeadMessageBody buildFetchAccountLeadMessageBody(DigiAccountRequestDto request) {

		FetchAccountLeadMessageBody fetchAccountLeadMessageBody = null;

		try {

			fetchAccountLeadMessageBody = fetchAccountLeadMessageBody.builder()
					.accountLeadId(request.getAccountLeadID()).authenticationToken(request.getAuthenticationToken())
					.build();

		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return fetchAccountLeadMessageBody;
	}

	public UpdateAccountLeadMessageBody buildUpdateAccountLeadMessageBody(DigiAccountRequestDto request) {

		UpdateAccountLeadMessageBody updateAccountLeadMessageBody = null;

		try {

			List<CustomerPreferences> customerPreferences = buildCustomerPreferences(
					request.getCustomerPreferencesDto());
			List<CustomerAccountLeadRelation> customerAccountLeadRelation = buildCustomerAccountLeadRelation(
					request.getCustomerAccountLeadRelationDto());
			AccountLead accountLead = buildAccountLead(request.getAccountLeadDto());
			List<NomineeAddress> nomineeAddress = buildNomineeAddress(request.getNomineeAddressDto());
			List<GuardianAddress> guardianAddress = buildGuardianAddress(request.getGuardianAddressDto());
			List<Nominee> nominee = buildNominee(request.getNomineeDto());
			List<Guardian> guardian = buildGuardian(request.getGuardianDto());
			List<CustomerDeliverables> customerDeliverables = buildCustomerDeliverables(
					request.getCustomerDeliverablesDto());

			updateAccountLeadMessageBody = updateAccountLeadMessageBody.builder()
					.customerPreferences(customerPreferences).nomineeAddress(nomineeAddress)
					.customerAccountLeadRelation(customerAccountLeadRelation).accountLead(accountLead)
					.guardianAddress(guardianAddress).nominee(nominee).guardian(guardian)
					.customerDeliverables(customerDeliverables).fromMADP(request.getFromMADP())
					.usrId(request.getUsrId()).accountLeadID(request.getAccountLeadID())
					.authenticationToken(request.getAuthenticationToken()).build();

		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return updateAccountLeadMessageBody;
	}

	private List<Guardian> buildGuardian(List<GuardianDto> guardianDto) {
		return null;
	}

	private List<Nominee> buildNominee(List<NomineeDto> nomineeDto) {
		return null;
	}

	private List<GuardianAddress> buildGuardianAddress(List<GuardianAddresssDto> guardianAddressDto) {
		return null;
	}

	private List<NomineeAddress> buildNomineeAddress(List<NomineeAddressDto> nomineeAddressDto) {
		return null;
	}

	private List<CustomerDeliverables> buildCustomerDeliverables(List<CustomerDeliverablesDto> customerDeliverablesDto) {
		CustomerDeliverables customerDeliverables = null;
		List<CustomerDeliverables> allCustomerDeliverables = new ArrayList<>();
		try {
			for (CustomerDeliverablesDto custDeliver : customerDeliverablesDto) {
				customerDeliverables = customerDeliverables.builder().welcomeKit(custDeliver.getWelcomeKit())
						.debitCard(custDeliver.getDebitCard()).iKITChequeBook(custDeliver.getIKITChequeBook())
						.mappedCustomer(custDeliver.getMappedCustomer()).chequeBook(custDeliver.getChequeBook())
						.iKIT(custDeliver.getIKIT()).deliverableID(custDeliver.getDeliverableID()).build();

				allCustomerDeliverables.add(customerDeliverables);
			}
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return null;
	}

	

	private List<CustomerPreferences> buildCustomerPreferences(List<CustomerPreferencesDto> customerPreferencesDto) {
		CustomerPreferences customerPreferences =null;
		List<CustomerPreferences> allCustomerPreferences = new ArrayList<>();
		try {
			for (CustomerPreferencesDto custPref : customerPreferencesDto) {
				customerPreferences = customerPreferences.builder().isStaff(custPref.getIsStaff())
						.netBankingRights(custPref.getNetBankingRights()).allSMSAlerts(custPref.getAllSMSAlerts())
						.onlyTransactionAlerts(custPref.getOnlyTransactionAlerts())
						.mappedAccountLead(custPref.getMappedAccountLead()).netBanking(custPref.getNetBanking())
						.mobileBankingNumber(custPref.getMobileBankingNumber()).preferenceID(custPref.getPreferenceID())
						.sms(custPref.getSms()).mappedCustomer(custPref.getMappedCustomer())
						.passbook(custPref.getPassbook()).physicalStatement(custPref.getPhysicalStatement())
						.emailStatement(custPref.getEmailStatement()).build();

				allCustomerPreferences.add(customerPreferences);
			}
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return null;
	}

	public SubmitAccountLeadMessageBody buildSubmitAccountLeadMessageBody(DigiAccountRequestDto request) {

		SubmitAccountLeadMessageBody submitAccountLeadMessageBody = null;

		try {
			submitAccountLeadMessageBody = submitAccountLeadMessageBody.builder()
					.authToken(request.getAuthenticationToken()).emailId(request.getEmailId())
					.loginUserId(request.getLoginUserId()).accountLeadId(request.getAccountLeadID()).build();

		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return submitAccountLeadMessageBody;
	}


	public DigiAccountDetailMessageBody buildDigiAccountDetailMessageBody(DigiAccountRequestDto request) {

		DigiAccountDetailMessageBody digiAccountDetailMessageBody = null;

		try {
			digiAccountDetailMessageBody = digiAccountDetailMessageBody.builder().acctTp(request.getAcctTp())
					.accountNo(request.getAccountNo()).build();

		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return digiAccountDetailMessageBody;
	}

	public DigiTDMaturityDetailsMessageBody buildDigiTDMaturityDetailsMessageBody(DigiAccountRequestDto request) {

		DigiTDMaturityDetailsMessageBody digiTDMaturityDetailsMessageBody = null;

		try {
			digiTDMaturityDetailsMessageBody = digiTDMaturityDetailsMessageBody.builder()
					.tdAcctNb(request.getAccountNo()).dpstNb(request.getDpstNb()).build();

		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return digiTDMaturityDetailsMessageBody;
	}

	public UpdateDigiTDMaturityDetailsMessageBody buildUpdateDigiTDMaturityDetailsMessageBody(
			DigiAccountRequestDto request) {

		UpdateDigiTDMaturityDetailsMessageBody updateDigiTDMaturityDetailsMessageBody = null;

		try {
			CurrentTDMaturityDetails currentTDMaturityDetails = CurrentTDMaturityDetails.builder()
					.pytTp(request.getCurrentTDMaturityDetails().getPytTp())
					.prtryNb(request.getCurrentTDMaturityDetails().getPrtryNb()).build();

			updateDigiTDMaturityDetailsMessageBody = updateDigiTDMaturityDetailsMessageBody.builder()
					.tdAcctNb(request.getAccountNo()).crntTDMtrtyDtls(currentTDMaturityDetails)
					.pytTp(request.getPytTp()).pytOpt(request.getPytOpt()).dpstNb(request.getDpstNb())
					.pytFrq(request.getPytFrq()).pytAcctNb(request.getPytAcctNb()).build();

		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return updateDigiTDMaturityDetailsMessageBody;
	}

	public RedeemDigiTDMaturityDetailsMessageBody buildRedeemDigiTDMaturityDetailsMessageBody(
			DigiAccountRequestDto request) {

		RedeemDigiTDMaturityDetailsMessageBody redeemDigiTDMaturityDetailsMessageBody = null;
		RedeemDigiDepositReq redeemDigiDepositReq = null;
		try {

			redeemDigiDepositReq = redeemDigiDepositReq.builder().rdmptnToAcct(request.getRdmptnToAcct())
					.rdmptnAmt(request.getRdmptnAmt()).dpstNb(request.getDpstNb()).acctId(request.getAcctId())
					.txnTp(request.getTxnTp()).userId(request.getUserId()).build();
			redeemDigiTDMaturityDetailsMessageBody = redeemDigiTDMaturityDetailsMessageBody.builder()
					.redeemDigiDepositReq(redeemDigiDepositReq).build();

		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return redeemDigiTDMaturityDetailsMessageBody;
	}

	public DigiLeadStatusMessageBody buildDigiLeadStatusMessageBody(DigiAccountRequestDto request) {
		DigiLeadStatusMessageBody digiLeadStatusMessageBody = null;
		try {
			digiLeadStatusMessageBody = digiLeadStatusMessageBody.builder().authToken(request.getAuthenticationToken())
					.id(request.getId()).associatedWith(request.getAssociatedWith()).build();
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return digiLeadStatusMessageBody;
	}

	public CreateDigiCaseMessageBody buildCreateDigiCaseMessageBody(DigiAccountRequestDto request) {
		CreateDigiCaseMessageBody createDigiCaseMessageBody = null;
		List<CreateCaseReq> createcaseReq = null;
		try {
			createcaseReq = buildCreateCaseReq(request.getCreateCaseReq());
			createDigiCaseMessageBody = createDigiCaseMessageBody.builder().createcaseReq(createcaseReq).build();
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return createDigiCaseMessageBody;
	}
	
	private List<CreateCaseReq> buildCreateCaseReq(List<CreateCaseReq> createCaseReqDto) {
		CreateCaseReq createCaseReq = null;
		List<CreateCaseReq> allCreateCaseReq = new ArrayList<>();
		List<CstmFld> cstmFld = null;
		CustomFields customFields = null;
		try {
			cstmFld = buildCstmFld(createCaseReqDto.get(0).getCstmFlds().getCstmFld());
			customFields = customFields.builder().cstmFld(cstmFld).build();
			for (CreateCaseReq ccr : createCaseReqDto) {

				createCaseReq = createCaseReq.builder().sbCtgry(ccr.getSbCtgry()).ctgry(ccr.getCtgry())
						.sbSbCtgry(ccr.getSbSbCtgry()).brnch(ccr.getBrnch()).prrty(ccr.getPrrty())
						.cstmFlds(customFields).sbjct(ccr.getSbjct()).build();

				allCreateCaseReq.add(ccr);
			}
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return null;
	}
	
	
	private List<CstmFld> buildCstmFld(List<CstmFld> cstmFldDto) {
		CstmFld cstmFld = null;
		List<CstmFld> allCstmFld = new ArrayList<>();
		try {
			for (CstmFld cf : cstmFldDto) {

				cstmFld = cstmFld.builder().vlu(cf.getVlu()).nm(cf.getNm()).build();

				allCstmFld.add(cstmFld);
			}
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return null;
	}

	public DigiAccountStatementMessageBody buildDigiAccountStatementMessageBody(DigiAccountRequestDto request) {
		DigiAccountStatementMessageBody digiAccountStatementMessageBody = null;
		try {
			digiAccountStatementMessageBody = digiAccountStatementMessageBody.builder().pgNum(request.getPgNum())
					.lastBalanceAmount(request.getLastBalanceAmount()).acctId(request.getAcctId())
					.frDt(request.getFrDt()).nbOfTxns(request.getNbOfTxns()).toDt(request.getToDt()).build();
		} catch (Exception e) {
			if (e instanceof NeoException)
				throw e;
		}
		return digiAccountStatementMessageBody;
	}
	
}
