package com.neo.aggregator.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.NeoBusinessCustomField;

@Repository
public interface NeoBusinessCustomFieldDao extends CrudRepository<NeoBusinessCustomField, Long>{

	 @Query("select p from NeoBusinessCustomField p where p.fieldName=:fieldName and p.tenant=:tenant")
	 NeoBusinessCustomField findByFiledNameAndTenant(@Param("fieldName")String fieldName,
	                                                    @Param("tenant") String tenant);
}
