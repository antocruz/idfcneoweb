package com.neo.aggregator.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.MerchantOBReqRes;

@Repository
public interface MerchantOnBoardingDao extends JpaRepository<MerchantOBReqRes, Long>{

}
