package com.neo.aggregator.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.neo.aggregator.model.MasterConfiguration;

public interface MasterConfigDao extends CrudRepository<MasterConfiguration, Long> {

	@Query(nativeQuery = true, value = "SELECT m.* FROM master_configuration m WHERE m.bankId=:bankName LIMIT 1")
	MasterConfiguration findByBankId(@Param("bankName") String bankName);

	MasterConfiguration findByPartnerId(String partnerId);

}
