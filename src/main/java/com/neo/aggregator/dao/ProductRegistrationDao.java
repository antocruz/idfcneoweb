package com.neo.aggregator.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.ProductRegistration;

@Repository
public interface ProductRegistrationDao extends CrudRepository<ProductRegistration, Long> {

	ProductRegistration findByEntityIdAndMobilenumberAndTenant(String entityId, String mobilenumber,String tenant);
	
	ProductRegistration findByEntityIdAndMobilenumberAndIsLogAndTenant(String entityId, String mobilenumber, Boolean isLog,String tenant);
	
	ProductRegistration findByMobilenumberAndStatusAndTenant(String mobileNumber, String status,String tenant);
}
