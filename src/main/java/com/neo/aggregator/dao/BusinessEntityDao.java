package com.neo.aggregator.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;

import com.neo.aggregator.model.BusinessEntity;

@Component
public interface BusinessEntityDao extends PagingAndSortingRepository<BusinessEntity, Long> {

	@Query("Select b from BusinessEntity b where " +
			"b.entityId not in (select v.entityId from VirtualAccountCreationRegister v " +
			"where v.result ='Success') ")
	List<BusinessEntity> getAllEntityDetails();

	List<BusinessEntity> findByEntityId(String entityId);
}