package com.neo.aggregator.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.IsoAuthorizationConfig;

@Repository
public interface IssuerAuthorizationConfigRepo extends CrudRepository<IsoAuthorizationConfig, Long> {

	IsoAuthorizationConfig findByBinAndProductCodeAndFieldName(String bin, String productCode, String fieldName);

	IsoAuthorizationConfig findByBinAndFieldName(String bin, String fieldName);
}
