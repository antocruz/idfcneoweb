package com.neo.aggregator.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.NeoAccountCreationReqResModel;

@Repository
public interface NeoAccountCreationReqResDao extends JpaRepository<NeoAccountCreationReqResModel, Long> {

	@Async
	@Override
	public <S extends NeoAccountCreationReqResModel> S saveAndFlush(S neoAccountCreationReqResModel);
	
	List<NeoAccountCreationReqResModel> findByAccountIdAndStatus(String accountNo,String status);
	
	List<NeoAccountCreationReqResModel> findByEntityIdAndStatusAndTenant(String entityId,String status,String tenant);
	
	List<NeoAccountCreationReqResModel> findByEntityIdAndStatusAndTenantAndPaymentRef(String entityId,String status,String tenant, String paymentRef);
	
}
