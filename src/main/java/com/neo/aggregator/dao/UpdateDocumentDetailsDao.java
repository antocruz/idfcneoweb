package com.neo.aggregator.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;

import com.neo.aggregator.model.UpdateDocumentDetails;

@Component
public interface UpdateDocumentDetailsDao extends PagingAndSortingRepository<UpdateDocumentDetails, Long> {

}
