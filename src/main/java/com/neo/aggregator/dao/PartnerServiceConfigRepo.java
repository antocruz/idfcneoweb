package com.neo.aggregator.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.PartnerServiceConfiguration;

@Repository
public interface PartnerServiceConfigRepo extends CrudRepository<PartnerServiceConfiguration, Long> {

	PartnerServiceConfiguration findByBankIdAndBankServiceId(String bankId,String serviceId);

}
