package com.neo.aggregator.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.UpiFundTransfer;

@Repository
public interface UpiFundTransferDao extends JpaRepository<UpiFundTransfer, Long>{

}
