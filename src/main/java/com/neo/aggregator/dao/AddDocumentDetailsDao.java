package com.neo.aggregator.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;

import com.neo.aggregator.model.AddDocumentDetails;

@Component
public interface AddDocumentDetailsDao extends PagingAndSortingRepository<AddDocumentDetails, Long> {

}
