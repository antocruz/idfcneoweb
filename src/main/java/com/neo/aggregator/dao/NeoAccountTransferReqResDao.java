package com.neo.aggregator.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.NeoAccountTransferReqRes;

@Repository
public interface NeoAccountTransferReqResDao  extends CrudRepository<NeoAccountTransferReqRes , Long> {
	
	NeoAccountTransferReqRes findByExternalTransactionId(String externalTxnId);

}
