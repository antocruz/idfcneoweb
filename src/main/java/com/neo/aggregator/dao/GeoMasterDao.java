package com.neo.aggregator.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.neo.aggregator.model.GeoMaster;


@Component
public interface GeoMasterDao extends PagingAndSortingRepository<GeoMaster, Long>,JpaSpecificationExecutor<GeoMaster>{

	GeoMaster findByPincodeAndCityName(@Param("pincode") String pincode, @Param("cityName") String cityName);

	@Query(value="SELECT g.* FROM geo_master g WHERE g.pincode = :pincode AND g.cityName LIKE %:cityName% LIMIT 1",nativeQuery = true)
	GeoMaster findByPincodeAndTopCityName(@Param("pincode") String pincode, @Param("cityName") String cityName);
	
	@Query(value="SELECT g.* FROM geo_master g WHERE g.pincode = :pincode LIMIT 1",nativeQuery = true)
	GeoMaster findByPincode(@Param("pincode") String pincode);
	
}
