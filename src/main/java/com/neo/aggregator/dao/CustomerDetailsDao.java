package com.neo.aggregator.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;

import com.neo.aggregator.model.CustomerDetails;

@Component
public interface CustomerDetailsDao extends PagingAndSortingRepository<CustomerDetails, Long> {

}
