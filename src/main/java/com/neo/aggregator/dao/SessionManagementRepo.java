package com.neo.aggregator.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.SessionManagement;

@Repository
public interface SessionManagementRepo extends CrudRepository<SessionManagement, Long> {

	static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(SessionManagementRepo.class);

	@Query("select p from SessionManagement p where p.isExpired=:isExpired and p.bankId=:bankId order by p.created DESC")
	List<SessionManagement> fetchSessionBybankIdAndIsExpired(@Param("isExpired") Boolean isExpired,
			@Param("bankId") String bankId);

	SessionManagement findByBankId(String bankId);

	SessionManagement findTopByIsExpiredAndBankIdOrderByCreatedDesc(Boolean isExpired, String bankId);
	
	SessionManagement findBySessionIdAndIsExpired(String sessionId,Boolean isExpired);

	default Boolean checkSessionValid(SessionManagement model) {

		Boolean valid = Boolean.FALSE;

		if (model != null) {

			if (model.getExpiryDate() != null) {
				if (model.getExpiryDate().before(new Date())) {
					logger.info("Session Expired");
				} else {
					logger.info("Valid Session Found");
					valid = Boolean.TRUE;
				}
			} else {
				logger.info("Session Found without Expiry");
				valid = Boolean.TRUE;
			}
		} else {
			logger.info("Session Not Found");
		}

		return valid;
	}

}
