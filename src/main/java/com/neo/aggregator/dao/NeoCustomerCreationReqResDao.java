package com.neo.aggregator.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.NeoCustomerCreationReqResModel;

@Repository
public interface NeoCustomerCreationReqResDao extends JpaRepository<NeoCustomerCreationReqResModel, Long> {
	
    @Async
    @Override
    public <S extends NeoCustomerCreationReqResModel> S saveAndFlush(S neoCustomerCreationModel);

}
