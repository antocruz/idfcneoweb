package com.neo.aggregator.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.NeoCustomerData;

@Repository
public interface NeoCustomerDao extends CrudRepository<NeoCustomerData, Long> {

	NeoCustomerData findByEntityId(String entityId);

}
