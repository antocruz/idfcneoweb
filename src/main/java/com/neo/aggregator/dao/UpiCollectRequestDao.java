package com.neo.aggregator.dao;

import com.neo.aggregator.model.UpiCollectRequest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UpiCollectRequestDao extends CrudRepository<UpiCollectRequest, Long> {
    UpiCollectRequest findByTransferUniqueNumber(String txnUniqueRefNum);

    @Query("Select n from UpiCollectRequest n where " +
            " lower(n.result) ='false' and n.refund_status is null")
    List<UpiCollectRequest>   fetchTransactionsForRefund();
}
