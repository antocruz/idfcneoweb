package com.neo.aggregator.dao.core;

import com.neo.aggregator.model.core.Transaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionDao extends CrudRepository<Transaction, Long> {

    @Query(value = "select * from transaction a where a.ext_txn_id = :extId",nativeQuery = true)
    Transaction findByExtTxnId(String extId);

}
