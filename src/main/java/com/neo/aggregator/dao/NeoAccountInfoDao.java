package com.neo.aggregator.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.neo.aggregator.model.NeoAccountDetails;

public interface NeoAccountInfoDao  extends CrudRepository<NeoAccountDetails, Long> {

	List<NeoAccountDetails> findByEntityId(String entityId);
	
	List<NeoAccountDetails> findByEntityIdAndModeOfCreation(String entityId,String modeOfCreation);
	
}
