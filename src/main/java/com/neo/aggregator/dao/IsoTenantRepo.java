package com.neo.aggregator.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.IsoTenant;

@Repository("tenantRepository")
public interface IsoTenantRepo extends CrudRepository<IsoTenant, Long> {

	@Override
	Iterable<IsoTenant> findAll();
}
