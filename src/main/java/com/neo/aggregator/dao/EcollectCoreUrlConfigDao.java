package com.neo.aggregator.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.EcollectCoreUrlConfig;

@Repository
public interface EcollectCoreUrlConfigDao extends CrudRepository<EcollectCoreUrlConfig, Long> {

	EcollectCoreUrlConfig findByEcollectCode(String eCollectCode);
}
