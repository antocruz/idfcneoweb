package com.neo.aggregator.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.VirtualAccountCreationRegister;

@Repository
public interface VirtualAccountCreationRegisterDao extends CrudRepository<VirtualAccountCreationRegister, Long> {

	VirtualAccountCreationRegister findByEntityIdAndCorporateCredit(String entityId, Boolean corpLoad);

	@Query(value = "select * from virtualaccount_creation_register v where v.entityId=:entityId and v.corporateCredit=:corpLoad and (v.virtualAccNum=:virtualAccNum or v.virtualAccNum is NULL)", nativeQuery = true)
	VirtualAccountCreationRegister findByEntityIdAndCorporateCreditAndVirtualAccNum(String entityId, Boolean corpLoad, String virtualAccNum);

	@Query(value = "select * from virtualaccount_creation_register v where (v.result in ('FAILED', 'Failure', '') or v.result is NULL) and v.retryCount<:retryCount", nativeQuery = true)
	List<VirtualAccountCreationRegister> findByResultAndRegCount(int retryCount);

}
