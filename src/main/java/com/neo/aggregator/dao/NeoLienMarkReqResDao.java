package com.neo.aggregator.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.NeoLienMarkReqResModel;

@Repository
public interface NeoLienMarkReqResDao extends JpaRepository<NeoLienMarkReqResModel, Long> {

	@Async
	@Override
	public <S extends NeoLienMarkReqResModel> S saveAndFlush(S neoLienMarkModel);

	List<NeoLienMarkReqResModel> findByStatus(String status);

	@Query(value = "SELECT ns.* FROM neo_lien_mark_req_res ns WHERE ns.created BETWEEN :previousDate AND :nextDate AND ns.`status`=:status", nativeQuery = true)
	List<NeoLienMarkReqResModel> findByStatusAndDateRange(@Param("status") String status,
			@Param("previousDate") String previousDate, @Param("nextDate") String nextDate);

}
