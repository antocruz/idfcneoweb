package com.neo.aggregator.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.ProductConfig;

@Repository
public interface ProductConfigDao extends CrudRepository<ProductConfig, Long> {

	ProductConfig findByPartnerAndProduct(String partner, String product);

}
