package com.neo.aggregator.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.IsoAuthorizationMessage;

@Repository
public interface IsoMessageRepo extends CrudRepository<IsoAuthorizationMessage, Long> {

	@Query("select p from IsoAuthorizationMessage p where p.mti =:mti and p.pan =:pan and p.processingCode =:processingcode "
			+ "and p.rrn=:rrn and p.cardAcceptorTerminalId =:cardAcceptorTerminalId")
	IsoAuthorizationMessage fetchOriginalTxn(@Param("mti") String mti, @Param("pan") String pan, @Param("processingcode") String processingCode
			, @Param("rrn") String rrn,
			@Param("cardAcceptorTerminalId") String cardAcceptorTerminalId);

}
