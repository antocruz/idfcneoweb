package com.neo.aggregator.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;

import com.neo.aggregator.model.DefaultData;

@Component
public interface DefaultDataDao extends PagingAndSortingRepository<DefaultData, Long>,JpaSpecificationExecutor<DefaultData>{

		Optional<DefaultData> findById(Long id);
}