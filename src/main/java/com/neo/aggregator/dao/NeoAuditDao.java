package com.neo.aggregator.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.NeoAudit;

@Repository
public interface NeoAuditDao extends CrudRepository<NeoAudit, Long> {

}
