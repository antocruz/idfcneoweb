package com.neo.aggregator.dao;

import java.util.List;

import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.neo.aggregator.model.Tenant;

@Component
@Order(1)
public interface TenantDao extends PagingAndSortingRepository<Tenant, Long> {

	@Query("select p from Tenant p where p.business =:businessId")
	Tenant findByName(@Param("businessId") String businessId);

	@Query("select t from Tenant t where t.business =:businessId and t.deleted =:deleted")
	Tenant findByNameAndDeleted(@Param("businessId") String businessId, @Param("deleted") Boolean deleted);

	@Query("select p from Tenant p")
	List<Tenant> findAll();

}
