package com.neo.aggregator.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;

import com.neo.aggregator.model.AddressDetails;

@Component
public interface AddressDetailsDao extends PagingAndSortingRepository<AddressDetails, Long> {

}
