package com.neo.aggregator.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.PartnerMasterConfiguration;

@Repository
public interface PartnerAggregatorConfig extends CrudRepository<PartnerMasterConfiguration, Long> {

	PartnerMasterConfiguration findByTenant(String tenant);

}
