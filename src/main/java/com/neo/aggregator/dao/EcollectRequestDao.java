package com.neo.aggregator.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.EcollectRequest;

@Repository
public interface EcollectRequestDao extends CrudRepository<EcollectRequest, Long> {

	@Query(value = "select e.transferUniqueNumber from ecollect_request e where e.application='RBL' and date(e.created)=:created and e.ecollectCode LIKE :corpClientId%", nativeQuery = true)
	List<String> findByApplication(String created, String corpClientId);

	@Query(value = "select e.* from ecollect_request e where e.transferUniqueNumber=:utr and e.ecollectCode = :virtualAc", nativeQuery = true)
	List<EcollectRequest> findByTransferUniqueNumberAndEcollectCode(String utr, String virtualAc);

}
