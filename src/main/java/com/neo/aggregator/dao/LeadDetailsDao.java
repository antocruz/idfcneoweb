package com.neo.aggregator.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.neo.aggregator.model.LeadDetails;

@Component
public interface LeadDetailsDao extends PagingAndSortingRepository<LeadDetails, Long> {

	LeadDetails findByEntityId(String entityId);

	@Query(value = "SELECT l.* FROM lead_details l WHERE l.entityId = :entityId ORDER BY TIMESTAMP DESC LIMIT 1", nativeQuery = true)
	LeadDetails findByLatestEntityId(@Param("entityId") String entityId);
}
