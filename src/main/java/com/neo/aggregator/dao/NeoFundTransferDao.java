package com.neo.aggregator.dao;

import com.neo.aggregator.model.NeoFundTransfer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NeoFundTransferDao extends CrudRepository<NeoFundTransfer, Long> {

    NeoFundTransfer findByExternalTransactionIdAndTenant(String extTxnId,String tenant);

    @Query("Select n from NeoFundTransfer n,MasterConfiguration m where " +
            " m.partnerId=n.tenant and m.bankId='RBL' and lower(n.transactionStatus) in ('initiated','on hold','in progress')")
    List<NeoFundTransfer>   fetchRblPendingTxns();

    @Query("Select n from NeoFundTransfer n,MasterConfiguration m where " +
            " m.partnerId=n.tenant and m.bankId='ICICI'" +
            " and n.transactionType in('IFT','NEFT','RTGS','UPI','IMPS')" +
            " and n.transactionOrigin not in('LOAD','INTERNAL')" +
            " and lower(n.transactionStatus) in ('initiated','on hold','in progress')")
    List<NeoFundTransfer>   fetchIciciPendingTxns();
}
