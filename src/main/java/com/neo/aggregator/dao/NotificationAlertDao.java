package com.neo.aggregator.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.NotificationAlertModel;

@Repository
public interface NotificationAlertDao extends CrudRepository<NotificationAlertModel, Long> {
	
	NotificationAlertModel findByBusinessAndErrorCode(String business,String errorCode);

}
