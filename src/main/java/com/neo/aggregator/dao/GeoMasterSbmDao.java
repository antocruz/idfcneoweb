package com.neo.aggregator.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.model.GeoMasterSBM;

@Repository
public interface GeoMasterSbmDao extends CrudRepository<GeoMasterSBM, Long> {

	@Query("select b from GeoMasterSBM b where b.pincode=:pincode")
	List<GeoMasterSBM> fetchGaoMasterDetailsByPinCode(@Param("pincode") String pincode);

	@Query("select b from GeoMasterSBM b where b.pincode=:district")
	List<GeoMasterSBM> fetchGaoMasterDetailsByDistrict(@Param("district") String pincode);

}
