package com.neo.aggregator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class NeoAggregatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(NeoAggregatorApplication.class, args);
	}

}
