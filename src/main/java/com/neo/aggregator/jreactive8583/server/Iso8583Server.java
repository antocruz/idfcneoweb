package com.neo.aggregator.jreactive8583.server;

import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;

import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.iso8583.MessageFactory;
import com.neo.aggregator.jreactive8583.AbstractIso8583Connector;
import com.neo.aggregator.jreactive8583.netty.pipeline.Iso8583ChannelInitializer;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class Iso8583Server<T extends IsoMessage>
		extends AbstractIso8583Connector<ServerConfiguration, ServerBootstrap, T> {

	private int headerLength;

	public Iso8583Server(int port, ServerConfiguration config, MessageFactory<T> messageFactory, int headerLength) {
		super(config, messageFactory);
		setSocketAddress(new InetSocketAddress(port));
		this.headerLength = headerLength;
	}

	public Iso8583Server(int port, MessageFactory<T> messageFactory, int headerLength) {
		this(port, ServerConfiguration.newBuilder().build(), messageFactory, headerLength);
	}

	public void start() throws InterruptedException {

		getBootstrap().bind().addListener((ChannelFuture future) -> {
			final Channel channel = future.channel();
			setChannel(channel);
			logger.info("Server is started and listening at {}", channel.localAddress());
		}).sync().await();
	}

	@Override
	protected ServerBootstrap createBootstrap() {

		final ServerBootstrap bootstrap = new ServerBootstrap();

		bootstrap.group(getBossEventLoopGroup(), getWorkerEventLoopGroup()).channel(NioServerSocketChannel.class)
				.localAddress(getSocketAddress())
				.childHandler(new Iso8583ChannelInitializer<>(getConfiguration(), getConfigurer(),
						getWorkerEventLoopGroup(), getIsoMessageFactory(), headerLength, getMessageHandler()));

		configureBootstrap(bootstrap);

		bootstrap.validate();

		return bootstrap;
	}

	public void shutdown() {
		stop();
		super.shutdown();
	}

	/**
	 * @return True if server is ready to accept connections.
	 */
	public boolean isStarted() {
		final Channel channel = getChannel();
		return channel != null && channel.isOpen();
	}

	public void stop() {
		final Channel channel = getChannel();
		if (channel == null) {
			throw new IllegalStateException("Server is not started.");
		}
		logger.info("Stopping the Server...");
		try {
			channel.deregister();
			channel.close().sync().await(10, TimeUnit.SECONDS);
			logger.info("Server was Stopped.");
		} catch (InterruptedException e) {
			logger.error("Error while stopping the server", e);
		}

	}

	/**
	 * Sends asynchronously and returns a {@link ChannelFuture}
	 *
	 * @param isoMessage A message to send
	 * @return ChannelFuture which will be notified when message is sent
	 */
	public ChannelFuture sendAsync(IsoMessage isoMessage) {
		Channel channel = getChannel();
		if (channel == null) {
			throw new IllegalStateException("Channel is not opened");
		}
		if (!channel.isWritable()) {
			throw new IllegalStateException("Channel is not writable");
		}
		return channel.writeAndFlush(isoMessage);
	}

	public void send(IsoMessage isoMessage) throws InterruptedException {
		sendAsync(isoMessage).sync().await();
	}

	public void send(IsoMessage isoMessage, long timeout, TimeUnit timeUnit) throws InterruptedException {
		sendAsync(isoMessage).sync().await(timeout, timeUnit);
	}

	public boolean isConnected() {
		Channel channel = getChannel();
		return channel != null && channel.isActive();
	}

	public Channel getChannelActive() {
		return getChannelV2();
	}
}
