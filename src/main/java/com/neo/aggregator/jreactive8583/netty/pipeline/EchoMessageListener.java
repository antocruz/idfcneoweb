package com.neo.aggregator.jreactive8583.netty.pipeline;

import com.neo.aggregator.dao.IsoMessageRepo;
import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.iso8583.MessageFactory;
import com.neo.aggregator.jreactive8583.IsoMessageListener;
import com.neo.aggregator.utility.BeanUtil;
import com.neo.aggregator.utility.Iso8583;
import com.neo.aggregator.utility.IsoMessageToIso8583Converter;

import io.netty.channel.ChannelHandlerContext;

/**
 *
 */
public class EchoMessageListener<T extends IsoMessage> implements IsoMessageListener<T> {

	private final MessageFactory<T> isoMessageFactory;

	public EchoMessageListener(MessageFactory<T> isoMessageFactory) {
		this.isoMessageFactory = isoMessageFactory;
	}

	@Override
	public boolean applies(IsoMessage isoMessage) {
		return isoMessage != null && isoMessage.getType() == 0x800;
	}

	/**
	 * Sends EchoResponse message. Always returns <code>false</code>.
	 *
	 * @param isoMessage a message to handle
	 * @return <code>false</code> - message should not be handled by any other
	 *         handler.
	 */
	@Override
	public boolean onMessage(ChannelHandlerContext ctx, T isoMessage) {
		final IsoMessage echoResponse = isoMessageFactory.createResponse(isoMessage);
		Iso8583 iso8583 = BeanUtil.getBean(IsoMessageToIso8583Converter.class).createIso8583Object(isoMessage);
		BeanUtil.getBean(IsoMessageRepo.class).save(Iso8583.setAuthorizationMessage(iso8583));
		ctx.writeAndFlush(echoResponse);
		return false;
	}
}
