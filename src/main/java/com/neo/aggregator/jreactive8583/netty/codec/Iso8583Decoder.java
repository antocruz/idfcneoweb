package com.neo.aggregator.jreactive8583.netty.codec;

import java.text.ParseException;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.iso8583.MessageFactory;
import com.neo.aggregator.jreactive8583.netty.pipeline.IsoMessageLoggingHandler;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.logging.LogLevel;

@SuppressWarnings("rawtypes")
public class Iso8583Decoder extends ByteToMessageDecoder {

	private int headerLength = 0;
	private IsoMessageLoggingHandler handler = null;
	private final MessageFactory messageFactory;
	private static final Logger logger = LoggerFactory.getLogger(Iso8583Decoder.class);

	public Iso8583Decoder(MessageFactory messageFactory, int headerLength) {
		this.messageFactory = messageFactory;
		this.headerLength = headerLength;
	}

	/**
	 * @implNote Message body starts immediately, no length header, see
	 *           <code>"lengthFieldFameDecoder"</code> in
	 *           {@link com.neo.aggregator.jreactive8583.netty.pipeline.Iso8583ChannelInitializer#initChannel}
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf byteBuf, List out) throws Exception {
		if (!byteBuf.isReadable()) {
			return;
		}
		byte[] bytes = new byte[byteBuf.readableBytes()];
		byteBuf.readBytes(bytes);
		logger.info("Bytes ::: " + ReflectionToStringBuilder.toString(bytes));
		logger.info("Using HexDump ::: " + ByteBufUtil.prettyHexDump(byteBuf));
		String hexDumpStr = ByteBufUtil.hexDump(bytes);
		logger.info(hexDumpStr);

		final IsoMessage isoMessage = messageFactory.parseMessage(bytes, headerLength);

		logger.info("ISO Message to String ::" + isoMessage.toString());

		if (isoMessage != null) {
			logger.debug("IsoMessage Builder ::" + ReflectionToStringBuilder.toString(isoMessage));
			this.handler = new IsoMessageLoggingHandler(LogLevel.DEBUG, false, true, new int[0]);
			String result = this.handler.format(ctx, "Request-Event", isoMessage);
			logger.info(result);
			out.add(isoMessage);
		} else {
			throw new ParseException("Can't parse ISO8583 message", 0);
		}
	}
}
