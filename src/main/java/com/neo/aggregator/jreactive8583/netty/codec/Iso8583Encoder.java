package com.neo.aggregator.jreactive8583.netty.codec;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.jreactive8583.netty.pipeline.IsoMessageLoggingHandler;
import com.neo.aggregator.utility.ISOException;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.handler.logging.LogLevel;

@ChannelHandler.Sharable
public class Iso8583Encoder extends MessageToByteEncoder<IsoMessage> {

	private final int lengthHeaderLength;
	private static final Logger logger = LoggerFactory.getLogger(Iso8583Encoder.class);

	public Iso8583Encoder(int lengthHeaderLength) {
		this.lengthHeaderLength = lengthHeaderLength;
	}

	@Override
	protected void encode(ChannelHandlerContext ctx, IsoMessage isoMessage, ByteBuf out)
			throws IOException, ISOException {
		if (lengthHeaderLength == 0) {
			byte[] bytes = isoMessage.writeData();
			out.writeBytes(bytes);
		} else {
//        	System.out.println(" Response IsoMessage :::" + ReflectionToStringBuilder.toString(isoMessage));
			logger.info("IsoMessage Builder ::" + ReflectionToStringBuilder.toString(isoMessage));
			String result = new IsoMessageLoggingHandler(LogLevel.DEBUG, false, true, new int[0]).format(ctx,
					"Request-Event", isoMessage);
			logger.info("ISO Message Data ::" + result);
			ByteBuffer byteBuffer = isoMessage.writeToBuffer(lengthHeaderLength);
			logger.info("Response Byte Value :::" + ReflectionToStringBuilder.toString(byteBuffer));
			String hexDumpStr = ByteBufUtil.hexDump(byteBuffer.array());
			logger.info("Hex Dump :: " + hexDumpStr);
			out.writeBytes(byteBuffer);
		}
	}
}
