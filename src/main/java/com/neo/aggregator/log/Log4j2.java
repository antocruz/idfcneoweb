package com.neo.aggregator.log;

public interface Log4j2 {

	boolean isInfoEnabled();

	boolean isDebugEnabled();

	boolean isErrorEnabled();

	boolean isFatalEnabled();

	boolean isWarnEnabled();

	boolean isTraceEnabled();

	void info(String str);

	void info(String str, Throwable t);

	void debug(String str);

	void error(String str);

	void fatal(String str);

	void warn(String str);

	void trace(String str);

	boolean isM2pDebugEnabled();

	void m2pdebug(String str);

}