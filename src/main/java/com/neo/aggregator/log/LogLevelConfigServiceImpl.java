package com.neo.aggregator.log;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.filter.ThresholdFilter;
import org.springframework.stereotype.Service;

@Service
public class LogLevelConfigServiceImpl implements LogLevelConfigService {

	private static Log4j2Wrapper Logger = (Log4j2Wrapper) Log4j2LogManager.getLog(LogLevelConfigServiceImpl.class);

	@Override
	public LogLevelChangeResponseDto setLogLevel(LogLevelChangeRequestDto levelChangeRequestDto) {
		LogLevelChangeResponseDto logLevelChangeResponseDto = new LogLevelChangeResponseDto();
		try {
			String logLevel = levelChangeRequestDto.getConfiguredLevel();
			String packageName = levelChangeRequestDto.getPackageName();

			if (logLevel.equalsIgnoreCase("DEBUG")) {

				Configurator.setLevel(packageName, Level.DEBUG);
				logLevelChangeResponseDto.setDescription("LogLevel Set to DEBUG for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" DEBUG");

			} else if (logLevel.equalsIgnoreCase("INFO")) {

				Configurator.setLevel(packageName, Level.INFO);
				logLevelChangeResponseDto.setDescription("LogLevel Set to INFO for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" INFO");

			} else if (logLevel.equalsIgnoreCase("TRACE")) {

				Configurator.setLevel(packageName, Level.TRACE);
				logLevelChangeResponseDto.setDescription("LogLevel Set to TRACE for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" TRACE");

			} else if (logLevel.equalsIgnoreCase("WARN")) {

				Configurator.setLevel(packageName, Level.WARN);
				logLevelChangeResponseDto.setDescription("LogLevel Set to WARN for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" WARN");

			} else if (logLevel.equalsIgnoreCase("ERROR")) {

				Configurator.setLevel(packageName, Level.ERROR);
				logLevelChangeResponseDto.setDescription("LogLevel Set to ERROR for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" ERROR");

			} else if (logLevel.equalsIgnoreCase("FATAL")) {

				Configurator.setLevel(packageName, Level.FATAL);
				logLevelChangeResponseDto.setDescription("LogLevel Set to FATAL for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" FATAL");

			} else if (logLevel.equalsIgnoreCase("ALL")) {

				Configurator.setLevel(packageName, Level.ALL);
				logLevelChangeResponseDto.setDescription("LogLevel Set to ALL for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" ALL");

			} else if (logLevel.equalsIgnoreCase("M2PDEBUG")) {

				Configurator.setLevel(packageName, Log4j2Wrapper.M2PDEBUG);
				logLevelChangeResponseDto.setDescription("LogLevel Set to M2PDEBUG for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" M2PDEBUG");

			} else {
				Logger.error("Not a known loglevel: " + logLevel);
				logLevelChangeResponseDto.setDescription("Error, not a known loglevel: " + logLevel);
			}
			return logLevelChangeResponseDto;
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error("Exception is thrown");
		}

		return logLevelChangeResponseDto;
	}

	@Override
	public LogLevelChangeResponseDto filterLogLevel(LogLevelChangeRequestDto levelFilterRequestDto) {

		LogLevelChangeResponseDto logLevelChangeResponseDto = new LogLevelChangeResponseDto();
		try {

			String packageName = levelFilterRequestDto.getPackageName();
			String logLevel = levelFilterRequestDto.getConfiguredLevel();
			LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
			ThresholdFilter DefaultFilter = ThresholdFilter.createFilter(Level.TRACE, Filter.Result.ACCEPT,
					Filter.Result.ACCEPT);
			Configuration config = ctx.getConfiguration();
			LoggerConfig loggerConfig = config.getLoggerConfig(LogManager.getLogger(packageName).getName());
			Boolean isFilterEnabled = loggerConfig.hasFilter();
			if (isFilterEnabled == true) {
				loggerConfig.getFilter().stop();
			}
			if (logLevel.equalsIgnoreCase("DEBUG")) {

				Configurator.setLevel(packageName, Level.DEBUG);
				ThresholdFilter filter = ThresholdFilter.createFilter(Level.INFO, Filter.Result.DENY,
						Filter.Result.NEUTRAL);
				loggerConfig.addFilter(filter);
				logLevelChangeResponseDto.setDescription("LogLevel Filter to DEBUG for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" DEBUG");

			} else if (logLevel.equalsIgnoreCase("INFO")) {

				Configurator.setLevel(packageName, Level.INFO);
				ThresholdFilter filter = ThresholdFilter.createFilter(Level.WARN, Filter.Result.DENY,
						Filter.Result.NEUTRAL);
				loggerConfig.addFilter(filter);
				logLevelChangeResponseDto.setDescription("LogLevel Filter to INFO for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" INFO");

			} else if (logLevel.equalsIgnoreCase("TRACE")) {

				Configurator.setLevel(packageName, Level.TRACE);
				ThresholdFilter filter = ThresholdFilter.createFilter(Level.DEBUG, Filter.Result.DENY,
						Filter.Result.NEUTRAL);
				loggerConfig.addFilter(filter);
				logLevelChangeResponseDto.setDescription("LogLevel Filter to TRACE for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" TRACE");

			} else if (logLevel.equalsIgnoreCase("WARN")) {

				Configurator.setLevel(packageName, Level.WARN);
				ThresholdFilter filter = ThresholdFilter.createFilter(Level.ERROR, Filter.Result.DENY,
						Filter.Result.NEUTRAL);
				loggerConfig.addFilter(filter);
				logLevelChangeResponseDto.setDescription("LogLevel Filter to WARN for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" WARN");

			} else if (logLevel.equalsIgnoreCase("ERROR")) {

				Configurator.setLevel(packageName, Level.ERROR);
				ThresholdFilter filter = ThresholdFilter.createFilter(Level.FATAL, Filter.Result.DENY,
						Filter.Result.NEUTRAL);
				loggerConfig.addFilter(filter);
				logLevelChangeResponseDto.setDescription("LogLevel Filter to ERROR for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" ERROR");

			} else if (logLevel.equalsIgnoreCase("FATAL")) {

				Configurator.setLevel(packageName, Level.FATAL);
				ThresholdFilter filter = ThresholdFilter.createFilter(Level.FATAL, Filter.Result.ACCEPT,
						Filter.Result.ACCEPT);
				loggerConfig.addFilter(filter);
				logLevelChangeResponseDto.setDescription("LogLevel Filter to FATAL for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" FATAL");

			} else if (logLevel.equalsIgnoreCase("RESET")) {

				loggerConfig.addFilter(DefaultFilter);
				Configurator.setRootLevel(Level.INFO);
				Configurator.setLevel(packageName, Level.INFO);
				logLevelChangeResponseDto.setDescription("LogLevel Filter Reset to INFO for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" INFO");

			} else if (logLevel.equalsIgnoreCase("ALL")) {

				Configurator.setLevel(packageName, Level.ALL);
				loggerConfig.addFilter(DefaultFilter);
				logLevelChangeResponseDto.setDescription("LogLevel Filter to ALL for Package : " + packageName);
				logLevelChangeResponseDto.setEffectiveLevel(" ALL");

			} else {

				Logger.error("Not a known loglevel: " + logLevel);
				logLevelChangeResponseDto.setDescription("Error, not a known loglevel: " + logLevel);

			}
			return logLevelChangeResponseDto;
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error("Exception Thrown");
			logLevelChangeResponseDto.setDescription("Error, not a known loglevel or Exception Thrown");
		}

		return logLevelChangeResponseDto;
	}

	@Override
	public LogLevelChangeResponseDto rootLogLevelChange(LogLevelChangeRequestDto rootLevelRequestDto) {

		LogLevelChangeResponseDto logLevelChangeResponseDto = new LogLevelChangeResponseDto();
		try {
			String logLevel = rootLevelRequestDto.getConfiguredLevel();

			if (logLevel.equalsIgnoreCase("DEBUG")) {

				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.DEBUG);
				logLevelChangeResponseDto.setDescription("ROOT LogLevel Set to DEBUG ");
				logLevelChangeResponseDto.setEffectiveLevel(" DEBUG");

			} else if (logLevel.equalsIgnoreCase("INFO")) {

				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.INFO);
				logLevelChangeResponseDto.setDescription("ROOT LogLevel Set to INFO ");
				logLevelChangeResponseDto.setEffectiveLevel(" INFO");

			} else if (logLevel.equalsIgnoreCase("TRACE")) {

				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.TRACE);
				logLevelChangeResponseDto.setDescription("ROOT LogLevel Set to TRACE ");
				logLevelChangeResponseDto.setEffectiveLevel(" TRACE");

			} else if (logLevel.equalsIgnoreCase("WARN")) {

				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.WARN);
				logLevelChangeResponseDto.setDescription("ROOT LogLevel Set to WARN ");
				logLevelChangeResponseDto.setEffectiveLevel(" WARN");

			} else if (logLevel.equalsIgnoreCase("ERROR")) {

				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ERROR);
				logLevelChangeResponseDto.setDescription("ROOT LogLevel Set to ERROR ");
				logLevelChangeResponseDto.setEffectiveLevel(" ERROR");

			} else if (logLevel.equalsIgnoreCase("FATAL")) {

				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.FATAL);
				logLevelChangeResponseDto.setDescription("ROOT LogLevel Set to FATAL ");
				logLevelChangeResponseDto.setEffectiveLevel(" FATAL");

			} else if (logLevel.equalsIgnoreCase("ALL")) {

				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ALL);
				logLevelChangeResponseDto.setDescription("ROOT LogLevel Set to ALL ");
				logLevelChangeResponseDto.setEffectiveLevel(" ALL");

			} else if (logLevel.equalsIgnoreCase("M2PDEBUG")) {

				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Log4j2Wrapper.M2PDEBUG);
				logLevelChangeResponseDto.setDescription("LogLevel Set to M2PDEBUG");
				logLevelChangeResponseDto.setEffectiveLevel(" M2PDEBUG");

			} else {
				Logger.error("Not a known loglevel: " + logLevel);
				logLevelChangeResponseDto.setDescription("Error, not a known loglevel: " + logLevel);
			}
			return logLevelChangeResponseDto;
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error("Exception is thrown");
			logLevelChangeResponseDto.setDescription("Error, not a known loglevel or Exception Thrown ");
		}

		return logLevelChangeResponseDto;
	}

	@Override
	public LogLevelChangeResponseDto filterRootLogLevelChange(LogLevelChangeRequestDto rootLevelFilterRequestDto) {

		LogLevelChangeResponseDto rootLogLevelChangeResponseDto = new LogLevelChangeResponseDto();
		try {

			String logLevel = rootLevelFilterRequestDto.getConfiguredLevel();
			LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
			Configuration config = ctx.getConfiguration();
			LoggerConfig loggerConfig = config.getLoggerConfig(LogManager.getRootLogger().getName());
			Boolean isFilterEnabled = loggerConfig.hasFilter();
			if (isFilterEnabled == false) {
				loggerConfig.getFilter().stop();
			}
			if (logLevel.equalsIgnoreCase("DEBUG")) {

				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.DEBUG);
				ThresholdFilter filter = ThresholdFilter.createFilter(Level.INFO, Filter.Result.DENY,
						Filter.Result.NEUTRAL);
				loggerConfig.addFilter(filter);
				ctx.updateLoggers();
				rootLogLevelChangeResponseDto.setDescription("ROOT LogLevel Filter to DEBUG");
				rootLogLevelChangeResponseDto.setEffectiveLevel(" DEBUG");

			} else if (logLevel.equalsIgnoreCase("INFO")) {

				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.INFO);
				ThresholdFilter filter = ThresholdFilter.createFilter(Level.WARN, Filter.Result.DENY,
						Filter.Result.NEUTRAL);
				loggerConfig.addFilter(filter);
				ctx.updateLoggers();
				rootLogLevelChangeResponseDto.setDescription("ROOT LogLevel Filter to INFO");
				rootLogLevelChangeResponseDto.setEffectiveLevel(" INFO");

			} else if (logLevel.equalsIgnoreCase("TRACE")) {

				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.TRACE);
				ThresholdFilter filter = ThresholdFilter.createFilter(Level.DEBUG, Filter.Result.DENY,
						Filter.Result.NEUTRAL);
				loggerConfig.addFilter(filter);
				ctx.updateLoggers();
				rootLogLevelChangeResponseDto.setDescription("ROOT LogLevel Filter to TRACE");
				rootLogLevelChangeResponseDto.setEffectiveLevel(" TRACE");

			} else if (logLevel.equalsIgnoreCase("WARN")) {

				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.WARN);
				ThresholdFilter filter = ThresholdFilter.createFilter(Level.ERROR, Filter.Result.DENY,
						Filter.Result.NEUTRAL);
				loggerConfig.addFilter(filter);
				ctx.updateLoggers();
				rootLogLevelChangeResponseDto.setDescription("ROOT LogLevel Filter to WARN");
				rootLogLevelChangeResponseDto.setEffectiveLevel(" WARN");

			} else if (logLevel.equalsIgnoreCase("ERROR")) {

				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ERROR);
				ThresholdFilter filter = ThresholdFilter.createFilter(Level.FATAL, Filter.Result.DENY,
						Filter.Result.NEUTRAL);
				loggerConfig.addFilter(filter);
				ctx.updateLoggers();
				rootLogLevelChangeResponseDto.setDescription("ROOT LogLevel Filter to ERROR");
				rootLogLevelChangeResponseDto.setEffectiveLevel(" ERROR");

			} else if (logLevel.equalsIgnoreCase("FATAL")) {

				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.FATAL);
				ThresholdFilter filter = ThresholdFilter.createFilter(Level.FATAL, Filter.Result.ACCEPT,
						Filter.Result.ACCEPT);
				loggerConfig.addFilter(filter);
				ctx.updateLoggers();
				rootLogLevelChangeResponseDto.setDescription("ROOT LogLevel Filter to FATAL");
				rootLogLevelChangeResponseDto.setEffectiveLevel(" FATAL");

			} else if (logLevel.equalsIgnoreCase("RESET")) {

				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.INFO);
				ThresholdFilter DefaultFilter = ThresholdFilter.createFilter(Level.TRACE, Filter.Result.ACCEPT,
						Filter.Result.ACCEPT);
				loggerConfig.addFilter(DefaultFilter);
				ctx.updateLoggers();
				rootLogLevelChangeResponseDto.setDescription("ROOT LogLevel Filter RESET to INFO");
				rootLogLevelChangeResponseDto.setEffectiveLevel(" INFO");

			} else if (logLevel.equalsIgnoreCase("ALL")) {
				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ALL);
				ThresholdFilter DefaultFilter = ThresholdFilter.createFilter(Level.ALL, Filter.Result.ACCEPT,
						Filter.Result.ACCEPT);
				loggerConfig.addFilter(DefaultFilter);
				ctx.updateLoggers();
				rootLogLevelChangeResponseDto.setDescription("ROOT LogLevel Filter to ALL");
				rootLogLevelChangeResponseDto.setEffectiveLevel(" ALL");

			} else if (logLevel.equalsIgnoreCase("M2PDEBUG")) {
				ThresholdFilter DefaultFilter = ThresholdFilter.createFilter(Level.INFO, Filter.Result.DENY,
						Filter.Result.NEUTRAL);
				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Log4j2Wrapper.M2PDEBUG);
				loggerConfig.addFilter(DefaultFilter);
				ctx.updateLoggers();
				rootLogLevelChangeResponseDto.setDescription("ROOT LogLevel Filter to M2PDEBUG");
				rootLogLevelChangeResponseDto.setEffectiveLevel(" M2PDEBUG");

			} else {

				Logger.error("Not a known loglevel: " + logLevel);
				rootLogLevelChangeResponseDto.setDescription("Error, not a known loglevel: " + logLevel);

			}
			return rootLogLevelChangeResponseDto;
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error("Exception Thrown");
			rootLogLevelChangeResponseDto.setDescription("Error, not a known loglevel or Exception Thrown");
		}

		return rootLogLevelChangeResponseDto;
	}

}