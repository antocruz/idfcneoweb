package com.neo.aggregator.log;

import lombok.Data;

@Data
public class LogLevelChangeRequestDto {
	private String configuredLevel;
	private String packageName;
}
