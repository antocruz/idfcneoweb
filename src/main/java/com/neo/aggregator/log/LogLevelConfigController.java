package com.neo.aggregator.log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller("/logger")
public class LogLevelConfigController {

	@Autowired
	LogLevelConfigServiceImpl logLevelConfigServiceImpl;

	private static Log4j2Wrapper LOG = (Log4j2Wrapper) Log4j2LogManager.getLog(LogLevelConfigServiceImpl.class);

	@PostMapping("/loglevel/change")
	@ResponseBody
	public ResponseEntity<LogLevelChangeResponseDto> logLevelChangeRequest(
			@RequestBody LogLevelChangeRequestDto levelChangeRequestDtoList) throws Exception {
		LOG.info("Log level: " + levelChangeRequestDtoList.getConfiguredLevel());
		LOG.info("Package name: " + levelChangeRequestDtoList.getPackageName());
		LogLevelChangeResponseDto levelChangeResponseDtoList = logLevelConfigServiceImpl
				.setLogLevel(levelChangeRequestDtoList);
		return new ResponseEntity<LogLevelChangeResponseDto>(levelChangeResponseDtoList, HttpStatus.OK);
	}

	@PostMapping("/loglevel/filter")
	@ResponseBody
	public ResponseEntity<LogLevelChangeResponseDto> filterLogLevelRequest(
			@RequestBody LogLevelChangeRequestDto levelChangeRequestDtoList) throws Exception {
		LOG.info("Log level: " + levelChangeRequestDtoList.getConfiguredLevel());
		LOG.m2pdebug("This is M2P DEBUG Logger");
		LogLevelChangeResponseDto levelChangeResponseDtoList = logLevelConfigServiceImpl
				.filterLogLevel(levelChangeRequestDtoList);
		return new ResponseEntity<LogLevelChangeResponseDto>(levelChangeResponseDtoList, HttpStatus.OK);
	}

	@PostMapping("/loglevel/ROOT/change")
	@ResponseBody
	public ResponseEntity<LogLevelChangeResponseDto> changeRootLogLevelRequest(
			@RequestBody LogLevelChangeRequestDto levelChangeRequestDtoList) throws Exception {
		LOG.info("Log level: " + levelChangeRequestDtoList.getConfiguredLevel());
		LogLevelChangeResponseDto levelChangeResponseDtoList = logLevelConfigServiceImpl
				.rootLogLevelChange(levelChangeRequestDtoList);
		return new ResponseEntity<LogLevelChangeResponseDto>(levelChangeResponseDtoList, HttpStatus.OK);
	}

	@PostMapping("/loglevel/ROOT/filter")
	@ResponseBody
	public ResponseEntity<LogLevelChangeResponseDto> filterRootLogLevelRequest(
			@RequestBody LogLevelChangeRequestDto rootlevelChangeRequestDtoList) throws Exception {
		LOG.info("Log level: " + rootlevelChangeRequestDtoList.getConfiguredLevel());
		LogLevelChangeResponseDto levelChangeResponseDtoList = logLevelConfigServiceImpl
				.filterRootLogLevelChange(rootlevelChangeRequestDtoList);
		return new ResponseEntity<LogLevelChangeResponseDto>(levelChangeResponseDtoList, HttpStatus.OK);
	}

}

