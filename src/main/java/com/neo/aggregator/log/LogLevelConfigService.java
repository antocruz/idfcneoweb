package com.neo.aggregator.log;

public interface LogLevelConfigService {

	public LogLevelChangeResponseDto setLogLevel(LogLevelChangeRequestDto levelChangeRequestDto);

	public LogLevelChangeResponseDto filterLogLevel(LogLevelChangeRequestDto levelFilterRequestDto);

	public LogLevelChangeResponseDto rootLogLevelChange(LogLevelChangeRequestDto rootLevelRequestDto);

	public LogLevelChangeResponseDto filterRootLogLevelChange(LogLevelChangeRequestDto rootLevelFilterRequestDto);
}