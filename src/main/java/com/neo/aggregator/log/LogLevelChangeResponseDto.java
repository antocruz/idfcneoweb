package com.neo.aggregator.log;

import lombok.Data;

@Data
public class LogLevelChangeResponseDto {

	private String effectiveLevel;
	private String description;
}
