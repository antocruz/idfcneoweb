package com.neo.aggregator.log;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.SimpleMessage;
import org.apache.logging.log4j.spi.ExtendedLogger;
import org.apache.logging.log4j.spi.ExtendedLoggerWrapper;


public class Log4j2Wrapper implements Log4j2 {
	private static final String FQCN = Log4j2Wrapper.class.getName();
	private ExtendedLoggerWrapper log;

	public Log4j2Wrapper(String name) {
		Logger logger = LogManager.getLogger();
		log = new ExtendedLoggerWrapper((ExtendedLogger) logger, logger.getName(), logger.getMessageFactory());
	}

	public Log4j2Wrapper() {
		Logger logger = LogManager.getLogger();
		log = new ExtendedLoggerWrapper((ExtendedLogger) logger, logger.getName(), logger.getMessageFactory());
	}


	public Log4j2Wrapper(Class<?> clazz) {
		Logger logger = LogManager.getLogger(clazz);
		log = new ExtendedLoggerWrapper((ExtendedLogger) logger, logger.getName(), logger.getMessageFactory());
	}


	final static Level M2PINFO = Level.forName("M2PINFO", 410);
	public final static Level M2PDEBUG = Level.forName("M2PDEBUG", 450);
	final static Level M2PERROR = Level.forName("M2PERROR", 500);
	final static Level M2PTRACE = Level.forName("M2PTRACE", 230);
	final static Level M2PFATAL = Level.forName("M2PFATAL", 240);
	final static Level M2PLOG = Level.forName("M2PLOG", 100);
	final static Level M2PWARN = Level.forName("M2PWARN", 240);

	public boolean isInfoEnabled() {
		return log.isInfoEnabled();
	}

	public boolean isDebugEnabled() {
		return log.isDebugEnabled();
	}

	public boolean isErrorEnabled() {
		return log.isErrorEnabled();
	}

	public boolean isFatalEnabled() {
		return log.isFatalEnabled();
	}

	public boolean isWarnEnabled() {
		return log.isWarnEnabled();
	}

	public boolean isTraceEnabled() {
		return log.isTraceEnabled();
	}

	private Message anyString(String str) {
		return new SimpleMessage(str);
	}

	public void m2pdebug(String str) {
		log.logIfEnabled(FQCN, M2PDEBUG, null, anyString(str), null);
	}

	public void m2pdebug(String str, Throwable t) {
		log.logIfEnabled(FQCN, M2PDEBUG, null, anyString(str), t);
	}

	public boolean isM2pDebugEnabled() {
		return log.isDebugEnabled();
	}

	public void info(String str) {
		log.logIfEnabled(FQCN, Level.INFO, null, anyString(str), null);
	}

	public void info(String str, Throwable t) {
		log.logIfEnabled(FQCN, Level.INFO, null, anyString(str), t);
	}

	public void debug(String str) {
		log.logIfEnabled(FQCN, Level.DEBUG, null, anyString(str), null);
	}

	public void error(String str) {
		log.logIfEnabled(FQCN, Level.ERROR, null, anyString(str), null);
	}

	public void error(String str, Throwable t) {
		log.logIfEnabled(FQCN, Level.INFO, null, anyString(str), t);
	}

	public void fatal(String str) {
		log.logIfEnabled(FQCN, Level.FATAL, null, anyString(str), null);
	}

	public void fatal(String str, Throwable t) {
		log.logIfEnabled(FQCN, Level.INFO, null, anyString(str), t);
	}

	public void trace(String str) {
		log.logIfEnabled(FQCN, Level.TRACE, null, anyString(str), null);
	}

	public void warn(String str) {
		log.logIfEnabled(FQCN, Level.WARN, null, anyString(str), null);
	}

	public void warn(String str, Throwable t) {
		log.logIfEnabled(FQCN, Level.INFO, null, anyString(str), t);
	}

	public void cust(String str) {
		log.logIfEnabled(FQCN, Level.WARN, null, anyString(str), null);
	}
}