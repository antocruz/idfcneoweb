package com.neo.aggregator.utility;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EncryptionUtil {

	@Value("${neo.agg.sbm.key}")
	private String sbmEsbKey;

	@Value("${neo.agg.sbm.iv}")
	private String sbmEsbIv;
	
	@Value("${neo.agg.sbm.encryption.mode}")
	private String encryptionMode;

	public String encrypt(String data) {
		
		if (encryptionMode!=null && encryptionMode.equals("GCM")) {
			return encryptGCM(data);
		}
		
		String encrypt = null;
		try {
			IvParameterSpec iv = new IvParameterSpec(sbmEsbIv.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(sbmEsbKey.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

			byte[] encrypted = cipher.doFinal(data.getBytes());
			encrypt = new String(Base64.encodeBase64String(encrypted));
			return encrypt;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return encrypt;
	}

	public String decrypt(String data) {
		
		if (encryptionMode!=null && encryptionMode.equals("GCM")) {
			return decryptGCM(data);
		}
		
		String decrypt = null;
		try {
			IvParameterSpec iv = new IvParameterSpec(sbmEsbIv.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(sbmEsbKey.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
			byte[] original = cipher.doFinal(Base64.decodeBase64(data));
			decrypt = new String(original);
			return decrypt;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return decrypt;
	}
	
	public String encryptGCM(String data) {
		String encrypt = null;
		try {
			
			SecretKeySpec skeySpec = new SecretKeySpec(sbmEsbKey.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
			
			GCMParameterSpec gcmParameterSpec = new
					GCMParameterSpec(128, sbmEsbIv.getBytes("UTF-8"));
			
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, gcmParameterSpec);

			byte[] encrypted = cipher.doFinal(data.getBytes());
			encrypt = new String(Base64.encodeBase64String(encrypted));
			return encrypt;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return encrypt;
	}

	public String decryptGCM(String data) {
		String decrypt = null;
		try {
			
			SecretKeySpec skeySpec = new SecretKeySpec(sbmEsbKey.getBytes("UTF-8"), "AES");
			
			GCMParameterSpec gcmParameterSpec = new
					GCMParameterSpec(128, sbmEsbIv.getBytes("UTF-8"));

			Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, gcmParameterSpec);
			
			byte[] original = cipher.doFinal(Base64.decodeBase64(data));
			decrypt = new String(original);
			return decrypt;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return decrypt;
	}
	
	public static String HmacSHA256(String message, String secret) {
		try {

			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
			sha256_HMAC.init(secret_key);

			byte raw[] = sha256_HMAC.doFinal(message.getBytes());

			StringBuffer ls_sb = new StringBuffer();
			for (int i = 0; i < raw.length; i++)
				ls_sb.append(char2hex(raw[i]));
			return ls_sb.toString();
		} catch (Exception e) {
			return null;
		}
	}

	private static String char2hex(byte x) {
		char arr[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

		char c[] = { arr[(x & 0xF0) >> 4], arr[x & 0x0F] };
		return (new String(c));
	}

}