package com.neo.aggregator.utility;

import java.util.HashMap;
import java.util.Map;

public class TenantContextHolder {

	private static ThreadLocal<Map<String, String>> contextHolder = new ThreadLocal<Map<String, String>>();

	private static final ThreadLocal<Map<String, String>> customFields = new ThreadLocal<Map<String, String>>();

	public static void setCustomFields(Map<String, String> values) {
		customFields.set(values);
	}

	public static String getCustomField(String fieldName) {
		return customFields.get().get(fieldName);
	}

	public static void setTenant(String tenant) {
		
		Map<String, String> threadMap = contextHolder.get();
		if (threadMap == null) {
			threadMap = new HashMap<String, String>();
			contextHolder.set(threadMap);
		}
		contextHolder.get().put("TENANT", tenant);
	}

	public static String getTenant() {
		Map<String, String> threadMap = contextHolder.get();
		if (threadMap == null) {
			return null;
		}
		return (String) contextHolder.get().get("TENANT");
	}

	public static void setNeoTenant(String neoTenant) {
		Map<String, String> threadMap = contextHolder.get();
		if (threadMap == null) {
			threadMap = new HashMap<String, String>();
			contextHolder.set(threadMap);
		}
		contextHolder.get().put("NEOTENANT", neoTenant);
	}

	public static String getNeoTenant() {
		Map<String, String> threadMap = contextHolder.get();
		if (threadMap == null) {
			return null;
		}
		return (String) contextHolder.get().get("NEOTENANT");
	}

	public static void switchToDefaultTenant() {
		Map<String, String> threadMap = contextHolder.get();
		if (threadMap == null) {
			threadMap = new HashMap<String, String>();
			contextHolder.set(threadMap);
		}
		contextHolder.get().put("TENANT", "default");
	}

	public static void setBankId(String bankId) {
		Map<String, String> threadMap = contextHolder.get();
		if (threadMap == null) {
			threadMap = new HashMap<String, String>();
			contextHolder.set(threadMap);
		}
		contextHolder.get().put("BANKID", bankId);
	}

	public static String getBankId() {
		Map<String, String> threadMap = contextHolder.get();
		if (threadMap == null) {
			return null;
		}
		return (String) contextHolder.get().get("BANKID");
	}
}
