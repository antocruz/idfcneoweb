package com.neo.aggregator.utility;

import java.io.UnsupportedEncodingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.neo.aggregator.constant.IsoConstants;
import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.iso8583.IsoValue;
import com.neo.aggregator.iso8583.util.HexCodec;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.utility.Iso8583.VisaHeaders;

import antlr.StringUtils;

@Component
public class IsoMessageToIso8583Converter {
	
	@Value("${switch.network}")
    private String switchNetwork;
	
	@Autowired
	private VisaFieldParser visaParser;

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(IsoMessageToIso8583Converter.class);
	
	public Iso8583 createIso8583Object(IsoMessage request) {
		Iso8583 iso8583 = new Iso8583();
		try {
			iso8583.setMti(ISOUtil.padleft(Integer.toHexString(request.getType()), 4, '0'));
		} catch (ISOException e) {
			logger.info("Iso Exception ::: "+ e.getLocalizedMessage());
			return null;
		}
		
		if(switchNetwork!=null && switchNetwork.toUpperCase().equals("VISA")) {
			if(request.getVisaHeaders()!=null) {
				VisaHeaders visaHeaders = new Iso8583().new VisaHeaders();
				visaHeaders.setHeaderLength(request.getVisaHeaders().get(IsoConstants.HEADER_LENGTH));
				visaHeaders.setHeaderFlagFormat(request.getVisaHeaders().get(IsoConstants.HEADER_FLAG_FORMAT));
				visaHeaders.setTextFormat(request.getVisaHeaders().get(IsoConstants.TEXT_FORMAT));
				visaHeaders.setDestinationStationId(request.getVisaHeaders().get(IsoConstants.DST_STAT_ID));
				visaHeaders.setSourceStationId(request.getVisaHeaders().get(IsoConstants.SRC_STAT_ID));
				visaHeaders.setRndConIf(request.getVisaHeaders().get(IsoConstants.RND_CON_INF));
				visaHeaders.setBase1Flag(request.getVisaHeaders().get(IsoConstants.BASE_1_FLAG));
				visaHeaders.setMessageStatusFlag(request.getVisaHeaders().get(IsoConstants.MSG_STATUS_FLAG));
				visaHeaders.setBatNo(request.getVisaHeaders().get(IsoConstants.BAT_NO));
				visaHeaders.setReserved(request.getVisaHeaders().get(IsoConstants.RESERVED));
				visaHeaders.setUserInfo(request.getVisaHeaders().get(IsoConstants.USR_INFO));
				iso8583.setVisaHeaders(visaHeaders);
			}
		}
		
		for (int i = 2; i < 193; i++) {
			if (request.hasField(i)) {
				IsoValue<?> isoValue = ((IsoValue<?>)request.getField(i));
				String value = isoValue.toString();
				switch (i) {

				case 2:
					iso8583.setPan(value);
					break;
				case 3:
					iso8583.setProcessingCode(value);
					break;
				case 4:
					iso8583.setTransactionAmount(value);
					break;

				case 5:
					iso8583.setSettlementAmount(value);
					break;
				case 6:
					iso8583.setCardHolderBillingAmount(value);
					break;
				case 7:
					iso8583.setDateTimeTransaction(value);
					break;
				case 9:
					iso8583.setSettlementConversionRate(value);
					break;
				case 10:
					iso8583.setCardholderBillingConversionRate(value);
					break;
				case 11:
					iso8583.setStan(value);
					break;
				case 12:
					iso8583.setTimeLocalTransaction(value);
					break;
				case 13:
					iso8583.setDateLocalTransaction(value);
					break;
				case 14:
					iso8583.setExpiryDate(value);
					break;
				case 15:
					iso8583.setSettlementDate(value);
					break;
				case 16:
					iso8583.setConversionDate(value);
					break;
				case 18:
					iso8583.setMcc(value);
					break;
				case 19:
					iso8583.setAcquiringCountryCode(value);
					break;
				case 20:
					iso8583.setVisaPanExtended_DE20((byte[])request.getField(i).getValue());
					break;
				case 22:
					iso8583.setPosEntryMode(value);
					break;
				case 23:
					iso8583.setCardSeqNumber(value);
					break;
				case 25:
					iso8583.setPointOfServiceConditionCode(value);
					break;
				case 28:
					iso8583.setFeesAmount(value);
					break;
				case 32:
					iso8583.setAcquiringInstitutionCode(StringUtils.stripFront(value, '0'));
					break;
				case 33:
					iso8583.setForwardingInstitutionCode(StringUtils.stripFront(value, '0'));
					break;
				case 35:
					iso8583.setTrack2Data(value);
					break;
				case 37:
					iso8583.setRrn(value);
					break;
				case 38:
					iso8583.setAuthorizationIdResponse(value);
					break;
				case 39:
					iso8583.setResponseCode(value);
					break;
				case 40:
					iso8583.setServiceConditionCode(value);
					break;
				case 41:
					iso8583.setCardAcceptorTerminalId(value);
					break;
				case 42:
					iso8583.setCardAcceptorId(value);
					break;
				case 43:
					iso8583.setCardAcceptorNameLocation(value);
					break;
				case 44:
					iso8583.setAdditionalResponseData(value);
					break;
				case 45:
					try {
						byte[] arrayData = (byte[])isoValue.getValue();
						iso8583.setTrack1Data(new String(arrayData, 0, arrayData.length, "Cp1047"));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					break;
				case 48:
					iso8583.setAdditionalData48(value);
					break;
				case 49:
					iso8583.setTransactionCurrencyCode(value);
					break;
				case 50:
					iso8583.setSettlementCurrencyCode(value);
					break;
				case 51:
					iso8583.setCardholderBillingCurrencyCode(value);
					break;
				case 52:
					iso8583.setPinData(value);
					break;
				case 54:
					iso8583.setAdditionalAmount(value);
					break;
				case 55:
					iso8583.setChipData(value.toUpperCase());
					break;
				case 56:
					iso8583.setOriginalDataElement(value);
					break;
				case 59:
					iso8583.setVisaPosGeoData_DE59((byte[])request.getField(i).getValue()); 
					break;
				case 60:
					if (switchNetwork.toUpperCase().equals("VISA")) {
						byte[] addPosInfo = (byte[]) request.getField(i).getValue();
						iso8583.setVisaAdditionalPosInformation(visaParser
								.parseAdditionalPosInfo(HexCodec.hexEncode(addPosInfo, 0, addPosInfo.length)));
					} else {
						iso8583.setAdviceReasonCode(value);
					}
					break;
				case 61:
					iso8583.setPosDataCode(value);
					break;
				case 62:
					if (switchNetwork.toUpperCase().equals("VISA")) {
							iso8583.setVisaCustomPaymentServiceField(visaParser.parsePaymentInfo((byte[])request.getField(i).getValue()));
					}
					break;
				case 63:
					if (switchNetwork.toUpperCase().equals("VISA")) {
							iso8583.setVisaPrivateNetworkField(visaParser.parseNetworkInfo((byte[])request.getField(i).getValue()));
					}
					break;					
				case 70:
					iso8583.setNetworkManagementCode(value);
					break;
				
				case 73:
					iso8583.setVisaDateAction(value);
					break;
					
				case 90:
					iso8583.setOriginalDataElement(value);
					break;
				
				case 91:
					iso8583.setFileUpdateCode(value);
					break;
					
				case 95:
					iso8583.setReplacementAmount(value);
					break;
					
				case 101:
					if (switchNetwork.toUpperCase().equals("VISA")) {
					iso8583.setVisaFileName_DE101((byte[])request.getField(i).getValue());
					}
					break;
					
				case 104:
					if (switchNetwork.toUpperCase().equals("VISA")) {
					iso8583.setVisaTransactionDescription_DE104((byte[])request.getField(i).getValue());
					}
					break;
				
				case 121:
					if (switchNetwork.toUpperCase().equals("VISA")) {
					iso8583.setVisaCustomerRelatedDate_DE121((byte[])request.getField(i).getValue());
					}
					break;
					
				case 123:
					if (switchNetwork.toUpperCase().equals("VISA")) {
					iso8583.setVisaVerificationData_DE123((byte[])request.getField(i).getValue());
					} else {
						iso8583.setHostName_DE123(value);
					}
					break;
				case 124:
						iso8583.setTransactionOrigin_DE124(value);
						break;
				case 125:
					if (switchNetwork.toUpperCase().equals("VISA")) {
					iso8583.setVisaSupportingInfo_DE125((byte[])request.getField(i).getValue());
					} else
						iso8583.setStatement_DE125(value);
					break;
					
				case 126:
					if (switchNetwork.toUpperCase().equals("VISA")) {
					iso8583.setVisaPrivateMerchantField(visaParser.parsMerchantAcqInfo((byte[])request.getField(i).getValue()));
					}
					break;
				
				case 127:
					if (switchNetwork.toUpperCase().equals("VISA")) {
					iso8583.setVisaFileRecord_DE127((byte[])request.getField(i).getValue());
					}
					break;
					
				case 152:
					iso8583.setNewPin(value);
					break;	
				}
			}
		}
		return iso8583;
	}
}
