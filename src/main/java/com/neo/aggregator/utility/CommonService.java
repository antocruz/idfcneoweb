package com.neo.aggregator.utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.bank.rbl.BusinessEntityListDto;
import com.neo.aggregator.dao.BusinessEntityDao;
import com.neo.aggregator.dao.PartnerServiceConfigRepo;
import com.neo.aggregator.dao.core.TransactionDao;
import com.neo.aggregator.dto.FundTransferRequestDto;
import com.neo.aggregator.dto.FundTransferResponseDto;
import com.neo.aggregator.dto.KitViewDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.NeoNotificationDto;
import com.neo.aggregator.dto.NeoResponse;
import com.neo.aggregator.dto.NotificationDto;
import com.neo.aggregator.dto.NotificationRequestDto;
import com.neo.aggregator.dto.PartnerServiceConfigurationDto;
import com.neo.aggregator.model.BusinessEntity;
import com.neo.aggregator.model.PartnerServiceConfiguration;
import com.neo.aggregator.model.core.Transaction;
import com.neo.aggregator.service.RemoteService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.neo.aggregator.dao.IsoMessageRepo;
import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.IsoAuthorizationMessage;

import javax.jms.Queue;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.util.Iterator;
import java.util.Set;

@Component
public class CommonService {

    private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(CommonService.class);

    @Autowired
    private IsoMessageRepo isoMessageRepo;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    @Qualifier("notificationAllQueue")
    private Queue queue;

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Autowired
    private PartnerServiceConfigRepo serviceConfigRepo;

    @Autowired
    private AESEncryptionUtil aesEncryptionUtil;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    TransactionDao transactionDao;

    @Autowired
    private BusinessEntityDao businessEntityDao;

    @Autowired
    RemoteService rmService;

    @Value("${neo.keystore.path}")
    private Resource keyStoreFile;

    @Value("${neo.keystore.password}")
    private String keyStorePassword;

    @Value("${neo.agg.core.url}")
    private String coreBaseURL;

    @Value("${neo.agg.local.url}")
    private String localUrl;


    @Async
    public void saveIsoAuthResponse(IsoMessage request, Iso8583 response, String kitNo, String acqInstitutionCode) {

        try {
            Iso8583 iso8583 = BeanUtil.getBean(IsoMessageToIso8583Converter.class).createIso8583Object(request);

            IsoAuthorizationMessage model = Iso8583.setMaximusAuthorizationMessage(iso8583);

            model.setResponseCode(response.getResponseCode());
            model.setStatementDE125(response.getStatement_DE125());
            model.setAdditionalResponseData(response.getAdditionalData48());
            model.setPan(kitNo);
            model.setAcquiringInstitutionCode(acqInstitutionCode);

            isoMessageRepo.save(model);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Error saving iso message");
        }

    }

    @Async
    public void notifyFundTrfStatus(NeoNotificationDto notifyDto) {

        try {

            NotificationRequestDto request = new NotificationRequestDto();

            notifyDto.setTxnOrigin("FUND TRANSFER");
            notifyDto.setTransactionDateTime(String.valueOf(Calendar.getInstance().getTimeInMillis()));

            Map<String, String> notifyMap = mapper.convertValue(notifyDto, new TypeReference<Map<String, String>>() {
            });
            Map<String, String> emailNotifyData = mapper.convertValue(notifyDto,
                    new TypeReference<Map<String, String>>() {
                    });
            emailNotifyData.put("to_email", "");

            String[] args = new String[]{notifyDto.getMobileNo(), notifyDto.getUtr(), notifyDto.getTxnStatus(),
                    notifyDto.getEntityId(), notifyDto.getDescription()};
            request.setTransactionType("neo_fund_transfer");
            request.setServerNotifyData(notifyMap);
            request.setNotificationType("C");
            request.setBusiness(notifyDto.getBusiness());
            request.setEmailNotifyData(emailNotifyData);
            request.setArgs(args);

            String msg = mapper.writeValueAsString(request);
            logger.info("Sending data to Queue ::" + msg);
            this.jmsMessagingTemplate.convertAndSend(queue, msg);

        } catch (Exception e) {
            logger.info("Error : ", e);
        }

    }

    public void notifyRBLVAStatus(NeoNotificationDto notifyDto) {

        try {
            NotificationRequestDto request = new NotificationRequestDto();
            request.setTransactionType(notifyDto.getTransactionType());

            notifyDto.setTxnOrigin("VIRTUAL");
            notifyDto.setTransactionDateTime(String.valueOf(Calendar.getInstance().getTimeInMillis()));
            notifyDto.setTransactionType("VIRTUAL_ACCOUNT");

            Map<String, String> notifyMap = mapper.convertValue(notifyDto, new TypeReference<Map<String, String>>() {
            });

            String[] args = new String[]{notifyDto.getTxnStatus(), notifyDto.getEntityId()};

            request.setServerNotifyData(notifyMap);
            request.setNotificationType("C");
            request.setBusiness(notifyDto.getBusiness());
            request.setArgs(args);

            String msg = mapper.writeValueAsString(request);
            logger.info("Sending data to Queue ::" + msg);
            this.jmsMessagingTemplate.convertAndSend(queue, msg);

        } catch (Exception e) {
            logger.info("Exception Occured :: ", e);
        }
    }

    public Boolean upsertPartnerServiceConfig(PartnerServiceConfigurationDto request, MultipartFile publicKeyFile) {
        byte[] publicKey = null;
        try {
            PartnerServiceConfiguration serviceConfig = serviceConfigRepo.findByBankIdAndBankServiceId(request.getBankId(), request.getBankServiceId());

            if (publicKeyFile != null) {
                InputStream dataStream = publicKeyFile.getInputStream();
                String data = IOUtils.toString(dataStream);
                publicKey = aesEncryptionUtil.encrypt(data);
            }

            if (serviceConfig == null) {
                serviceConfig = new DozerBeanMapper().map(request, PartnerServiceConfiguration.class);
            } else {
                serviceConfig.setServiceEndpoint(request.getServiceEndpoint());
            }

            serviceConfig.setBankPublicKey(publicKey);
            serviceConfigRepo.save(serviceConfig);

            return true;
        } catch (Exception e) {
            logger.error("Exception ::" + e);
            return false;
        }
    }

    public String pipeSptdMsg(Map<String, String> map) {
        Set<Map.Entry<String, String>> mapSet = map.entrySet();
        StringBuilder paramBuilder = new StringBuilder();
        int setSize = mapSet.size();
        Iterator<Map.Entry<String, String>> mapEntryIterator = mapSet.iterator();
        for (int i = 1; i <= setSize; i++) {
            String val = mapEntryIterator.next().getValue();
            if (StringUtils.isNotEmpty(val)) {
                paramBuilder.append(val);
            } else {
                paramBuilder.append("");
            }

            if (i != setSize)
                paramBuilder.append("|");
        }

        logger.info("Req msg constructed before external call :: " + paramBuilder);

        return paramBuilder.toString();
    }

    public Map<String, String> decodePipeStdMsg(String msg, Map<String, String> targetMap) {
        String[] msgArray = msg.split("/|");
        Iterator<Map.Entry<String, String>> mapIterator = targetMap.entrySet().iterator();
        for (int i = 0; i < msgArray.length; i++) {
            if (mapIterator.hasNext())
                mapIterator.next().setValue(msgArray[i]);
        }
        return targetMap;
    }

    private RestTemplate sslConfig(String alias, String tlsVersion) throws KeyStoreException, NoSuchAlgorithmException,
            CertificateException, IOException, KeyManagementException, UnrecoverableKeyException {

        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(keyStoreFile.getInputStream(), keyStorePassword.toCharArray());

        try {
            keyStoreFile.getInputStream().close();
        } catch (IOException e) {
            logger.info("IOException KeyStore File :: " + e.getMessage());
        }

        SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
                new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy())
                        .loadKeyMaterial(keyStore, keyStorePassword.toCharArray(), (aliases, socket) -> alias).build(),
                new String[]{tlsVersion}, null, NoopHostnameVerifier.INSTANCE);

        HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();

        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);

        return new RestTemplate(requestFactory);
    }

    public String exchangeForEntityWithHeaders(String request, String url, HttpMethod method, HttpHeaders headers,
                                               Boolean logRequired, String alias, Boolean sslEnabled) {

        try {

            logger.info("URL ::" + url);
            if (logRequired) {
                logger.info("Request from Server ::" + request);
            } else {
                logger.info("Request log disabled");
            }

            if (sslEnabled != null && sslEnabled) {
                logger.info("SSL config loaded");
                restTemplate = sslConfig(alias, "TLSv1.2");
            } else {
                logger.info("SSL config disabled");
            }

            logger.info("HTTP Headers ::" + headers);
            HttpEntity<String> entity = null;
            if (method == HttpMethod.GET) {
                entity = new HttpEntity<>(headers);
            } else if (method == HttpMethod.POST) {
                entity = new HttpEntity<>(request, headers);
            }

            ResponseEntity<String> response = restTemplate.exchange(url, method, entity, String.class,
                    new Object[]{url});

            logger.info("Response from Server ::" + response.getBody());

            return response.getBody();
        } catch (Exception e) {
            logger.info("Exception Occured ::" + e.getMessage());
            logger.m2pdebug("Exception Occured :: ", e);
        }
        return null;
    }

    public Boolean updateTxnRRN(String requestStr) throws JsonProcessingException {
        String extnTxnId;
        String rrn;

        Map<String, String> request = new ObjectMapper().readValue(requestStr, Map.class);
        extnTxnId = request.get("extnTxnId");
        rrn = request.get("rrn");

        Transaction transaction = transactionDao.findByExtTxnId(extnTxnId);

        if (transaction != null) {
            transaction.setRrn(rrn);
            transactionDao.save(transaction);
            logger.info("Saving RRN :: " + rrn + " for Extn Txn Id :: " + extnTxnId);
            return true;
        }
        return false;
    }

    @Async
    public void sendNotificationInFundTransfer(FundTransferRequestDto fundTransferRequestDto,
                                               FundTransferResponseDto fundTransferResponseDto)
            throws NeoException, JsonProcessingException {

        HashMap<String, String> emailData = new HashMap<>();
        DecimalFormat df = new DecimalFormat("#.00");
        Double balance = 0D;
        String actualAmount = "";
        String fromTenant = "";
        String corporate = null;
        KitViewDto kitDto = null;
        String cardNo = null;

        fromTenant = fundTransferRequestDto.getFromTenant();
        NotificationDto notification = new NotificationDto();
        notification.setAmount(Double.parseDouble(fundTransferRequestDto.getAmount()));
        notification.setCrdr("DEBIT");
        notification.setTransactionType(fundTransferRequestDto.getTransactionType().concat("_DEBIT"));
        notification.setTransactionOrigin(fundTransferRequestDto.getTransactionOrigin());

        //if (fromEntityDto != null) {
        notification.setBalance(df.format(balance));
        notification.setMobileNo(fundTransferRequestDto.getRemmiterMobileNo());
        notification.setEntityId(fundTransferRequestDto.getEntityId());

        BusinessEntity businessEntityDto = null;

           /* String balApi = coreBaseURL.concat(
                    "business-entity-manager/fetchbalance/".concat(fundTransferRequestDto.getEntityId()));

            NeoResponse fetchBalResponse = postToCore(balApi, HttpMethod.GET, null, fromTenant);

            String balResponse = new ObjectMapper().writeValueAsString(fetchBalResponse.getResult());
            fetchBalanceDto = objectMapper.readValue(balResponse, FetchBalanceDto[].class);

            if (fetchBalanceDto.length > 0) {
                balance = Double.parseDouble(fetchBalanceDto[0].getBalance());
            } else
                balance = 0D;*/
        balance = Double.parseDouble(fundTransferRequestDto.getPostTxnBalance());

        try {
            String kitSearchApi = coreBaseURL.concat("support-manager/fetchKitBySearch?entityId=".concat(fundTransferRequestDto.getEntityId()));
            NeoResponse fetchKitResp = postToCore(kitSearchApi, HttpMethod.GET, null, fromTenant);
            String kitSearchResponse = new ObjectMapper().writeValueAsString(fetchKitResp.getResult());
            kitDto = mapper.readValue(kitSearchResponse, KitViewDto.class);
        } catch (Exception ye) {

        }

        if (kitDto != null) {
            if (kitDto.getMaskedCardNo() != null && kitDto.getMaskedCardNo().length() == 16) {
                cardNo = kitDto.getMaskedCardNo().substring(12, 16);
            }
            notification.setKitNo(kitDto.getKitNo());
            notification.setCardEnding(cardNo);
            notification.setProxyCardNo(kitDto.getKitNo());
            emailData.put("card_no", cardNo);
        }

        try {
            BusinessEntityListDto entityData = fetchEntities(fundTransferRequestDto.getEntityId(), fromTenant);
            businessEntityDto = entityData.getBusinessEntityList().get(0);
            emailData.put("to_email", businessEntityDto.getEmailAddress());
            emailData.put("transaction_amount", df.format(Double.parseDouble(fundTransferRequestDto.getAmount())));
            emailData.put("balance", df.format(balance));
            emailData.put("actual_amount", actualAmount);
            emailData.put("merchant_name", fundTransferRequestDto.getBeneficiaryName());
            emailData.put("transaction_date", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
            emailData.put("to_name", fundTransferRequestDto.getBeneficiaryName());
            emailData.put("from_name", (businessEntityDto.getFirstName() + " " + businessEntityDto.getLastName()));

        } catch (Exception e) {
            logger.info("Error : ", e);
        }
        //  }

        notification.setExternalTransactionId(fundTransferRequestDto.getExternalTransactionId());
        notification.setFirstName(fundTransferRequestDto.getBeneficiaryName());
        notification.setDescription(fundTransferRequestDto.getDescription());
        notification.setType("DEBIT");
        notification.setTime(new Date());

        if (!(fundTransferResponseDto.getStatus().equalsIgnoreCase("SUCCESS") ||
                fundTransferResponseDto.getStatus().equalsIgnoreCase("INITIATED") || fundTransferResponseDto.getStatus().equalsIgnoreCase("IN PROGRESS")
                || fundTransferResponseDto.getStatus().equalsIgnoreCase("ON HOLD"))) {
            notification.setTransactionStatus("PAYMENT_FAILURE");
        } else if (fundTransferResponseDto.getStatus().equalsIgnoreCase("INITIATED") || fundTransferResponseDto.getStatus().equalsIgnoreCase("IN PROGRESS")
                || fundTransferResponseDto.getStatus().equalsIgnoreCase("ON HOLD")) {
            notification.setTransactionStatus("IN_PROCESS");
        } else {
            notification.setTransactionStatus("PAYMENT_SUCCESS");
        }

        notification.setMerchantName(fundTransferRequestDto.getBeneficiaryName());
        notification.setRetrievalRefNo(fundTransferResponseDto.getBankReferenceNo());
        notification.setTransactionAmount(Double.parseDouble(fundTransferRequestDto.getAmount()));
        notification.setTxnDate(new Date());

        List<String> args = new ArrayList<>();

        notification.setTemplate("fund_transfer");
        args.add(df.format(Double.parseDouble(fundTransferRequestDto.getAmount())));
        args.add(fundTransferRequestDto.getBeneficiaryName());
        args.add(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        args.add(df.format(balance));
        args.add(cardNo);

        notification.setArgs(args.toArray());
        notification.setEmailMap(emailData);
        notification.setBusiness(corporate);

        postToCore(coreBaseURL.concat("support-manager/notify"), HttpMethod.POST,
                new ObjectMapper().writeValueAsString(notification), fromTenant);//push the data to support-manager/notify
    }


    private NeoResponse postToCore(String coreUrl, HttpMethod method, String coreRequest, String tenant) {

        NeoResponse coreResp = null;
        ResponseEntity<String> resp;
        logger.info("Core Url :: " + coreUrl);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Basic YWRtaW46YWRtaW4=");
        headers.set("tenant", tenant);
        headers.setContentType(MediaType.APPLICATION_JSON);

        try {
            logger.info("Core Request Payload :: " + coreRequest);
            HttpEntity<String> entity = new HttpEntity<>(coreRequest, headers);
            resp = restTemplate.exchange(coreUrl, method, entity, String.class,
                    coreUrl);
        } catch (HttpStatusCodeException e) {
            resp = ResponseEntity.status(e.getRawStatusCode()).headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
        } catch (Exception e) {
            return new NeoResponse("failure", null, null);
        }
        logger.info("Core Response :: " + resp.getBody());

        try {
            coreResp = new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .readValue(resp.getBody(), NeoResponse.class);
        } catch (Exception e) {
            logger.error("Core Response Mapping Failed");
            return new NeoResponse("failure", null, null);
        }
        return coreResp;
    }

    @SuppressWarnings("unused")
    private BusinessEntityListDto fetchEntities(String entityId, String tenant) throws JsonProcessingException {

        BusinessEntity dto = new BusinessEntity();
        dto.setEntityId(entityId);

        NeoResponse response = rmService.fetchData(dto, localUrl + "/neo-lego/common/virtual-account/fetchEntities",
                tenant, HttpMethod.POST);

        String responseStr = new ObjectMapper().writeValueAsString(response.getResult());
        return new ObjectMapper().readValue(responseStr, BusinessEntityListDto.class);
    }

}
