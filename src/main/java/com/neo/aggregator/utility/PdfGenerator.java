package com.neo.aggregator.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neo.aggregator.bank.equitas.dto.KycRequestDto;
import com.neo.aggregator.bank.equitas.dto.KycResponseDto;
import com.neo.aggregator.dto.v2.AddressDto;
import com.neo.aggregator.enums.EmploymentType;
import com.neo.aggregator.serviceimpl.EquitasEbsServiceImpl;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;

@Service
public class PdfGenerator {

	@Autowired
	private EquitasEbsServiceImpl eqService;

	private static final Logger logger = LoggerFactory.getLogger(PdfGenerator.class);

	public void createPdf(KycResponseDto response, KycRequestDto request, String htmlName) {
		try {

			InputStream inputStream = PdfGenerator.class.getClassLoader().getResourceAsStream(File.separator+"html"+File.separator+ htmlName + ".html");
            String htmlString =  IOUtils.toString(inputStream, "UTF-8");
            
			DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
			Calendar cal = Calendar.getInstance();
			Date dateobj = new Date();

			String aadhar = request.getIdNumber();
			aadhar = "********"+aadhar.substring(8);
			if (request.getTitle().equalsIgnoreCase("1")) {
				request.setTitle("MR");
			} else if (request.getTitle().equalsIgnoreCase("5")) {
				request.setTitle("MRS");
			} else if (request.getTitle().equalsIgnoreCase("15")) {
				request.setTitle("MISS");
			}

			if (request.getRegistrationRequestDtoV2().getMaritalStatus() != null) {
				if (request.getRegistrationRequestDtoV2().getMaritalStatus().equalsIgnoreCase("1")) {
					request.setMaritalStatus("SINGLE");
				} else if (request.getRegistrationRequestDtoV2().getMaritalStatus().equalsIgnoreCase("2")) {
					request.setMaritalStatus("MARRIED");
				}
			}

			if (EmploymentType.EMPLOYED.name()
					.equals(request.getRegistrationRequestDtoV2().getEmploymentType().toString())) {
				request.setOccupation(EmploymentType.SALARIED.name());
			} else if (EmploymentType.HOUSEWORK.name()
					.equals(request.getRegistrationRequestDtoV2().getEmploymentType().toString())) {
				request.setOccupation(EmploymentType.HOUSEWIFE.name());
			} else if (EmploymentType.SELF_EMPLOYED.name()
					.equals(request.getRegistrationRequestDtoV2().getEmploymentType().toString())) {
				request.setOccupation(EmploymentType.SELF_EMPLOYED.name());
			} else if (EmploymentType.STUDENT.name()
					.equals(request.getRegistrationRequestDtoV2().getEmploymentType().toString())) {
				request.setOccupation(EmploymentType.STUDENT.name());
			}


			if (request.getGrossAnnualIncome() != null) {
				if (request.getGrossAnnualIncome().equals("1")) {
					request.setGrossAnnualIncome("0 - 50K");
				} else if (request.getGrossAnnualIncome().equals("2")) {
					request.setGrossAnnualIncome("50K - 1L");
				} else if (request.getGrossAnnualIncome().equals("3")) {
					request.setGrossAnnualIncome("1L - 2.5L");
				} else if (request.getGrossAnnualIncome().equals("4")) {
					request.setGrossAnnualIncome("2.5L - 5L");
				} else if (request.getGrossAnnualIncome().equals("5")) {
					request.setGrossAnnualIncome("5L - 7.5L");
				} else if (request.getGrossAnnualIncome().equals("6")) {
					request.setGrossAnnualIncome("7.5L - 10L");
				} else if (request.getGrossAnnualIncome().equals("7")) {
					request.setGrossAnnualIncome("10L - 15L");
				} else if (request.getGrossAnnualIncome().equals("8")) {
					request.setGrossAnnualIncome("15L - 20L");
				} else if (request.getGrossAnnualIncome().equals("9")) {
					request.setGrossAnnualIncome("20L - 30L");
				} else if (request.getGrossAnnualIncome().equals("10")) {
					request.setGrossAnnualIncome("30L - 50L");
				} else if (request.getGrossAnnualIncome().equals("11")) {
					request.setGrossAnnualIncome("50L - 1C");
				} else if (request.getGrossAnnualIncome().equals("12")) {
					request.setGrossAnnualIncome("10000001 - 9999999999");
				}
				
			}
			
			if (htmlName.equalsIgnoreCase("accountOpeningForm")) {
				htmlString = htmlString.replace("$authcode", Objects.toString(response.getAuthCode(), ""));
				htmlString = htmlString.replace("$rrn", Objects.toString(response.getRrn(), ""));
				htmlString = htmlString.replace("$otpVerificationCode",
						Objects.toString(request.getOtpVerificationCode(), ""));
				htmlString = htmlString.replace("$ucic", Objects.toString(response.getCustId(), ""));
				htmlString = htmlString.replace("$title", Objects.toString(response.getTitle(), ""));
				htmlString = htmlString.replace("$name", Objects.toString(response.getName(), ""));
				if(response.getRegistrationRequestDtoV2().getKycInfo()!=null && "PAN".equalsIgnoreCase(response.getRegistrationRequestDtoV2().getKycInfo().get(0).getDocumentType())){
					htmlString = htmlString.replace("$panOrForm60", Objects
							.toString(response.getRegistrationRequestDtoV2().getKycInfo().get(0).getDocumentNo(), ""));
				} else {
					htmlString = htmlString.replace("$panOrForm60", Objects
							.toString(response.getRegistrationRequestDtoV2().getKycInfo().get(0).getDocumentType(), ""));
				}
				htmlString = htmlString.replace("$dob", Objects.toString(response.getDob(), ""));
				htmlString = htmlString.replace("$mother", Objects.toString(request.getMotherMaidenName(), ""));
				htmlString = htmlString.replace("$father", Objects.toString(request.getFatherName(), ""));
				htmlString = htmlString.replace("$gender", Objects.toString(request.getGender().toString(), ""));
				htmlString = htmlString.replace("$maritalStatus", Objects.toString(request.getRegistrationRequestDtoV2().getMaritalStatus(), ""));
				htmlString = htmlString.replace("$mobile", Objects.toString(
						response.getRegistrationRequestDtoV2().getCommunicationInfo().get(0).getContactNo(), ""));
				htmlString = htmlString.replace("$email", Objects.toString(
						response.getRegistrationRequestDtoV2().getCommunicationInfo().get(0).getEmailId(), ""));
				htmlString = htmlString.replace("$aadhar", Objects.toString(aadhar, ""));
				htmlString = htmlString.replace("$occupation", Objects.toString(request.getOccupation(), ""));
				htmlString = htmlString.replace("$fundSource", Objects.toString(request.getFundSource(), ""));
				if (request.getPoliticallyExposedPerson().equals("1")) {
					htmlString = htmlString.replace("$pep", Objects.toString("Yes", ""));
				} else {
					htmlString = htmlString.replace("$pep", Objects.toString("No", ""));
				}
				htmlString = htmlString.replace("$nationality",
						Objects.toString(request.getRegistrationRequestDtoV2().getCountry(), ""));
				htmlString = htmlString.replace("$grossIncome", Objects.toString(request.getGrossAnnualIncome(), ""));
				htmlString = htmlString.replace("$mailingAddress", Objects.toString(response.getAdd(), ""));
				htmlString = htmlString.replace("$permanentAddress", Objects.toString(response.getAdd(), ""));
				htmlString = htmlString.replace("$currentDateTime", Objects.toString(df.format(dateobj), ""));
				htmlString = htmlString.replace("$agentName", Objects.toString(request.getAgentName(), ""));
				htmlString = htmlString.replace("$agentUcic", Objects.toString(request.getAgentUcic(), ""));
				htmlString = htmlString.replace("$agentAccountNumber",
						Objects.toString(request.getAgentAccountNumber(), ""));
				htmlString = htmlString.replace("$agentAddress", Objects.toString(request.getAgentAddress(), ""));
				htmlString = htmlString.replace("$agentCity", Objects.toString(request.getAgentCity(), ""));
				htmlString = htmlString.replace("$agentState", Objects.toString(request.getAgentState(), ""));
				htmlString = htmlString.replace("$agentPincode", Objects.toString(request.getAgentPincode(), ""));
				htmlString = htmlString.replace("$corporateName", Objects.toString(request.getCorporateName(), ""));
				htmlString = htmlString.replace("$corporateAddress",
						Objects.toString(request.getCorporateAddress(), ""));
				htmlString = htmlString.replace("$corporateCity", Objects.toString(request.getCorporateCity(), ""));
				htmlString = htmlString.replace("$corporateState", Objects.toString(request.getCorporateState(), ""));
				htmlString = htmlString.replace("$corporatePincode",
						Objects.toString(request.getCorporatePincode(), ""));
			} else if (htmlName.equalsIgnoreCase("form60")) {
				htmlString = htmlString.replace("$firstName", Objects.toString(request.getFirstName(), ""));
				htmlString = htmlString.replace("$middleName", Objects.toString(request.getMiddleName(), ""));
				htmlString = htmlString.replace("$surname", Objects.toString(request.getLastName(), ""));
				
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			    Date date = formatter.parse(response.getDob());
				htmlString = htmlString.replace("$date1",
						Objects.toString(new SimpleDateFormat("dd-MM-yyyy").format(date), ""));
				htmlString = htmlString.replace("$fName", Objects.toString(request.getFatherName(), ""));
				htmlString = htmlString.replace("$mName", Objects.toString("", ""));
				htmlString = htmlString.replace("$sname", Objects.toString("", ""));
				for (AddressDto address : request.getRegistrationRequestDtoV2().getAddressInfo()) {
					htmlString = htmlString.replace("$primises", Objects.toString(address.getAddress1(), ""));
					htmlString = htmlString.replace("$road", Objects.toString(address.getAddress2(), ""));
					htmlString = htmlString.replace("$area", Objects.toString(address.getAddress3(), ""));
					htmlString = htmlString.replace("$city",
							Objects.toString(request.getRegistrationRequestDtoV2().getCity(), ""));
					htmlString = htmlString.replace("$district", Objects.toString(request.getRegistrationRequestDtoV2().getDistrict(), ""));
					htmlString = htmlString.replace("$state",
							Objects.toString(request.getRegistrationRequestDtoV2().getState(), ""));
					htmlString = htmlString.replace("$pincode", Objects.toString(address.getPincode(), ""));
				}
				htmlString = htmlString.replace("$room", Objects.toString("", ""));
				htmlString = htmlString.replace("$floor", Objects.toString("", ""));
				htmlString = htmlString.replace("$block", Objects.toString("", ""));
				htmlString = htmlString.replace("$telephone", Objects.toString("", ""));
				htmlString = htmlString.replace("$mobile", Objects.toString(
						request.getRegistrationRequestDtoV2().getCommunicationInfo().get(0).getContactNo(), ""));
				htmlString = htmlString.replace("$amount", Objects.toString("NA", ""));
				htmlString = htmlString.replace("$tdate1", Objects.toString("", ""));
				htmlString = htmlString.replace("$date2", Objects.toString("", ""));
				htmlString = htmlString.replace("$aadhar", Objects.toString(aadhar, ""));
				htmlString = htmlString.replace("$agriculturalIncome",
						Objects.toString(request.getRegistrationRequestDtoV2().getEstimatedAgriculturalIncome(), ""));
				htmlString = htmlString.replace("$nonAgriculturalIncome", Objects
						.toString(request.getRegistrationRequestDtoV2().getEstimatedNonAgriculturalIncome(), ""));
				htmlString = htmlString.replace("$documentType1", Objects.toString("AADHAAR", ""));
				htmlString = htmlString.replace("$documentCode1", Objects.toString("01", ""));
				htmlString = htmlString.replace("$documentNumber1", Objects.toString(aadhar, ""));
				htmlString = htmlString.replace("$documentType2", Objects.toString("AADHAAR", ""));
				htmlString = htmlString.replace("$documentCode2", Objects.toString("01", ""));
				htmlString = htmlString.replace("$documentNumber2", Objects.toString(aadhar, ""));
				htmlString = htmlString.replace("$name", Objects.toString(response.getName(), ""));
				htmlString = htmlString.replace("$dd",
						Objects.toString(new SimpleDateFormat("dd").format(cal.getTime()), ""));
				htmlString = htmlString.replace("$month",
						Objects.toString(new SimpleDateFormat("MMM").format(cal.getTime()), ""));
				htmlString = htmlString.replace("$yy",
						Objects.toString(new SimpleDateFormat("yy").format(cal.getTime()), ""));
				htmlString = htmlString.replace("$signature", Objects.toString(request.getSignature(), ""));

			}

			File newHtmlFile = File.createTempFile("temp", ".html");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);
			String htmlToPdf = FileUtils.readFileToString(newHtmlFile);
			File pdfFile = File.createTempFile("temp", ".pdf");

			renderPDF(htmlToPdf, new FileOutputStream(pdfFile));
			String pdfBin = encodeFileToBase64Binary(pdfFile);
			FileInputStream fis = new FileInputStream(newHtmlFile);
			fis.close();
			FileInputStream fisPdf = new FileInputStream(pdfFile);
			fisPdf.close();
			newHtmlFile.delete();
			pdfFile.delete();
			eqService.addAndUpdateDoc(pdfBin, htmlName, request.getEntityId());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static String encodeFileToBase64Binary(File file) {
		try (FileInputStream fileInputStreamReader = new FileInputStream(file)){
			byte[] bytes = new byte[(int) file.length()];
			fileInputStreamReader.read(bytes);
			return new String(Base64.getEncoder().encode(bytes));
		} catch (Exception e) {
			logger.info("Can't convert image to Base64. {}", e.getMessage());
		}
		
		return null;
	}

	private static void renderPDF(String html, OutputStream outputStream) throws Exception {
		try {
			PdfRendererBuilder builder = new PdfRendererBuilder();
			builder.withHtmlContent(html, html);
			builder.toStream(outputStream);
			builder.run();
		} catch (Exception e) {
			logger.info("Can't read XHTML embedded image. {}", e.getMessage());
		} finally {
			outputStream.close();
		}
	}
}
