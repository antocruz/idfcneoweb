package com.neo.aggregator.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateUtilityFunction {

	public static long noOfDaysInBetween(Date fromDate, Date toDate) {
		long diff = toDate.getTime() - fromDate.getTime();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	public static String getCurrentYearMonthDay(Date date, char t) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		switch (t) {
		case 'Y':
			int year = c.get(Calendar.YEAR);
			return Integer.toString(year);
		case 'M':
			int month = c.get(Calendar.MONTH) + 1; // since JAN starts from 0
			return Integer.toString(month);
		case 'D':
			int day = c.get(Calendar.DATE);
			return Integer.toString(day);
		case 'W':
			int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			switch (dayOfWeek) {
			case 0:
				return "SUN";
			case 1:
				return "MON";
			case 2:
				return "TUE";
			case 3:
				return "WED";
			case 4:
				return "THU";
			case 5:
				return "FRI";
			case 6:
				return "SAT";
			}
		}
		return null;
	}

	public static Date formatDateFromFormat(String date, String format, Boolean eod) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			Date parsedDate = sdf.parse(date);
			if (eod != null && eod) {
				return atEndOfDay(parsedDate);
			} else if (eod != null && !eod) {
				return atStartOfDay(parsedDate);
			} else {
				return parsedDate;
			}
		} catch (ParseException e) {
			// TODO: HANDLE EXCEPTION
		}
		return null;
	}

	public static String dateInFormat(Date date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	public static Date atStartOfDay(Date date) {
		LocalDateTime localDateTime = dateToLocalDateTime(date);
		LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
		return localDateTimeToDate(startOfDay);
	}

	public static Date atEndOfDay(Date date) {
		LocalDateTime localDateTime = dateToLocalDateTime(date);
		LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
		return localDateTimeToDate(endOfDay);
	}

	public static Date addNoOfDaysToDate(Date date, Integer days) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DAY_OF_MONTH, days);
		return c.getTime();
	}

	public static Date addNoOfMonthsToDate(Date date, Integer months) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, months);
		return c.getTime();
	}

	public static LocalDateTime dateToLocalDateTime(Date date) {
		return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
	}

	public static Date localDateTimeToDate(LocalDateTime localDateTime) {
		return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

	public static boolean isLeapYear() {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
	}

	public static String getISOdateToNewFormat(String data, String currentFormat, String newFormat) {
		SimpleDateFormat sdfOld = new SimpleDateFormat(currentFormat);
		SimpleDateFormat sdfNew = new SimpleDateFormat(newFormat);

		Date newDate = null;
		try {
			newDate = sdfOld.parse(data);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar c = Calendar.getInstance();
		int currentYear = c.get(Calendar.YEAR);
		c.setTime(newDate);
		c.set(Calendar.YEAR, currentYear);

		return sdfNew.format(c.getTime());

	}
	
	public static LocalDate dateToLocalDate(Date date) {
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}
	
	public static Period calculatePeriodBetweenDate(Date fromDate) {

		LocalDate fromDateLocal = dateToLocalDate(fromDate);

		LocalDate toDateLocal = LocalDate.now();

		return Period.between(fromDateLocal, toDateLocal);
	}
}