package com.neo.aggregator.utility;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.neo.aggregator.model.IsoAuthorizationMessage;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class Iso8583 {

	private String mti;

	private String pan;

	private String processingCode;

	private String transactionAmount;

	private String settlementAmount;

	private String cardHolderBillingAmount;

	private String dateTimeTransaction;

	private String settlementConversionRate;

	private String cardholderBillingConversionRate;

	private String stan;

	private String timeLocalTransaction;

	private String dateLocalTransaction;

	private String expiryDate;

	private String settlementDate;

	private String conversionDate;

	private String mcc;

	private String acquiringCountryCode;

	private String posEntryMode;

	private String cardSeqNumber;

	private String pointOfServiceConditionCode;

	private String feesAmount;

	private String acquiringInstitutionCode;

	private String forwardingInstitutionCode;

	private String track2Data;

	private String rrn;

	private String authorizationIdResponse;

	private String responseCode;

	private String serviceConditionCode;

	private String cardAcceptorTerminalId;

	private String cardAcceptorId;

	private String cardAcceptorNameLocation;

	private String additionalResponseData;

	private String track1Data;

	private String additionalData48;

	private String transactionCurrencyCode;

	private String settlementCurrencyCode;

	private String cardholderBillingCurrencyCode;

	private String pinData;

	private String additionalAmount;

	private String chipData;

	private String adviceReasonCode;

	private String posDataCode;

	private String privateData1BiometricData;

	private String privateData2EncryptedBiometricData;

	private String networkManagementCode;

	private String originalDataElement;

	private String fileUpdateCode;

	private String replacementAmount;

	private String fileName101;

	private String accountIdentification1;

	private String accountIdentification2;

	private String newPin;

	private String checkSum;

	private String description;

	private VisaAdditionalPosInformation visaAdditionalPosInformation;

	private String authenticationField;

	private VisaHeaders visaHeaders;

	private byte[] visaVerificationData_DE123;

	private byte[] visaSupportingInfo_DE125;

	private byte[] visaTransactionDescription_DE104;

	private byte[] visaPanExtended_DE20;

	private byte[] visaCustomerRelatedDate_DE56;

	private byte[] visaCustomerRelatedDate_DE121;

	private byte[] visaPosGeoData_DE59;

	private byte[] visaFileName_DE101;

	private byte[] visaFileRecord_DE127;

	private String visaDateAction;

	private Boolean isExternalAuth = false;

	private String hostName_DE123;

	private String transactionOrigin_DE124;

	private String primaryAccountNoExtended;

	private String statement_DE125;

	@Data
	@ToString
	public class VisaAdditionalPosInformation {
		private int terminalType = 0;
		private int terminalEntryCapability = 0;
		private int chipConditionCode = 0;
		private int specialConditionIndicator = 0;
		private int chipTransactionIndicator = 0;
		private int chipReliabilityIndicator = 0;
		private int eciIndicator = 0;
		private int cardIdIndicator = 0;
		private int additionalAuthIndicator = 0;
	}

	private VisaCustomPaymentServiceField visaCustomPaymentServiceField;

	@Data
	@ToString
	public class VisaCustomPaymentServiceField {
		private String authCharactInd;
		private String tranId;
		private String validationCode;
		private String marketSpecDataId;
		private String duration;
		private String prestigiousPropInd;
		private String purchaseIdentifier;
		private String autoRentalChkoutChkinDate;
		private String noShowIndicator;
		private String extraCharges;
		private String clearingSeqNo;
		private String clearingSeqCnt;
		private String resTcktId;
		private String authorizedAmt;
		private String reqPaymentService;
		private String chargeBackRightsInd;
		private String gatewayTxnId;
		private String exclTxnIdReasonCde;
		private String ecomGoodsInd;
		private String mvv;
		private String riskScore;
		private String riskCondCode;
		private String productId;
		private String programId;
		private String spendQualifiedInd;
		private String accountStatus;
	}

	private VisaPrivateNetworkField visaPrivateNetworkField;

	@Data
	@ToString
	public class VisaPrivateNetworkField {
		private String networkId;
		private String preAuthLimit;
		private String messageReasonCode;
		private String switchReasonCode;
		private String feeProgInd;
		private String decimalIndicator;
		private String base2flags;
		private String visaAcqBusinessId;
		private String reimbAttribute;
		private String fraudData;
		private String sharingGroupCode;
		private String issuerCrncyConversion;
		private String reserved;
		private String chargeIndicator;
	}

	private VisaPrivateMerchantField visaPrivateMerchantField;

	@Data
	@ToString
	public class VisaPrivateMerchantField {
		private String merchantId;
		private String cardSerialNum;
		private String merchantSerialNum;
		private String txnId;
		private String cavv;
		private String amexKey;
		private String cvv2;
		private String serviceInd;
		private String posEnv;
		private String masterUCAFInd;
		private String masterUCAFField;
		private String agentUniqueAcctResult;
		private String dccInd;
		private String three_dInd;
	}

	private VisaAdditionalResponseField visaAdditionalResponseField;

	@Data
	@ToString
	public class VisaAdditionalResponseField {
		private String reasonCode;
		private String avr;
		private String cvv_icvv_result;
		private String pacmDiversionLevel;
		private String pacmDiversionReasonCode;
		private String cardAuthResult;
		private String cvv2_result;
		private String originalRespCode;
		private String cavvResultCode;
		private String respReasonCode;
		private String panLastFour;
		private String cvmReqPinLess;
	}

	public static IsoAuthorizationMessage setAuthorizationMessage(Iso8583 iso8583) {

		IsoAuthorizationMessage authMessage = new IsoAuthorizationMessage();

		authMessage.setMti(iso8583.getMti());
		authMessage.setNetworkManagementCode(iso8583.getNetworkManagementCode());
		authMessage.setResponseCode(iso8583.getResponseCode());
		authMessage.setStan(iso8583.getStan());
		authMessage.setRrn(iso8583.getRrn());
		authMessage.setDescription(iso8583.getDescription());

		return authMessage;
	}

	public static IsoAuthorizationMessage setMaximusAuthorizationMessage(Iso8583 iso8583) {

		IsoAuthorizationMessage authMessage = new IsoAuthorizationMessage();

		authMessage.setMti(iso8583.getMti());
		authMessage.setPan(iso8583.getPan());
		authMessage.setProcessingCode(iso8583.getProcessingCode());
		authMessage.setTimeLocalTransaction(iso8583.getTimeLocalTransaction());
		authMessage.setDateLocalTransaction(iso8583.getDateLocalTransaction());
		authMessage.setDateTimeTransaction(iso8583.getDateTimeTransaction());
		authMessage.setTransactionAmount(iso8583.getTransactionAmount());
		authMessage.setTransactionCurrencyCode(iso8583.getTransactionCurrencyCode());
		authMessage.setCardAcceptorId(iso8583.getCardAcceptorId());
		authMessage.setCardAcceptorNameLocation(iso8583.getCardAcceptorNameLocation());
		authMessage.setStan(iso8583.getStan());
		authMessage.setRrn(iso8583.getRrn());
		authMessage.setAccountIdentification1(iso8583.getAccountIdentification1());
		authMessage.setAccountIdentification2(authMessage.getAccountIdentification2());
		authMessage.setAcquiringInstitutionCode(iso8583.getAcquiringInstitutionCode());
		authMessage.setCardAcceptorTerminalId(iso8583.getCardAcceptorTerminalId());

		String dateTimeLocal = DateUtilityFunction.getISOdateToNewFormat(iso8583.getTimeLocalTransaction(),
				"MMddHHmmss",
				"YYYYMMddHHmmss");

		authMessage.setCaptureDate(dateTimeLocal.substring(0, 8));

		return authMessage;
	}

	@Data
	@ToString
	public class VisaHeaders {
		private String headerLength = "16";
		private String headerFlagFormat = "01";
		private String textFormat = "02";
		private String destinationStationId = "000000";
		private String sourceStationId = "000000";
		private String rndConIf = "00";
		private String base1Flag = "0000";
		private String messageStatusFlag = "000000";
		private String batNo = "00";
		private String reserved = "000000";
		private String userInfo = "00";
	}

	@Override
	public String toString() {

		return "mti=" + mti + ", pan=" + ISOUtil.maskDataAndType(pan, "pan") + ", processingCode=" + processingCode
				+ ", transactionAmount=" + transactionAmount + ", settlementAmount=" + settlementAmount
				+ ", cardHolderBillingAmount=" + cardHolderBillingAmount + ", dateTimeTransaction="
				+ dateTimeTransaction + ", settlementConversionRate=" + settlementConversionRate
				+ ", cardholderBillingConversionRate=" + cardholderBillingConversionRate + ", stan=" + stan
				+ ", timeLocalTransaction=" + timeLocalTransaction + ", dateLocalTransaction=" + dateLocalTransaction
				+ ", expiryDate=" + expiryDate + ", settlementDate=" + settlementDate + ", conversionDate="
				+ conversionDate + ", mcc=" + mcc + ", acquiringCountryCode=" + acquiringCountryCode + ", posEntryMode="
				+ posEntryMode + ", cardSeqNumber=" + cardSeqNumber + ", pointOfServiceConditionCode="
				+ pointOfServiceConditionCode + ", feesAmount=" + feesAmount + ", acquiringInstitutionCode="
				+ acquiringInstitutionCode + ", forwardingInstitutionCode=" + forwardingInstitutionCode
				+ ", track2Data=" + (track2Data != null ? "PRESENT" : "NOT PRESENT") + ", rrn=" + rrn
				+ ", authorizationIdResponse=" + authorizationIdResponse + ", responseCode=" + responseCode
				+ ", serviceConditionCode=" + serviceConditionCode + ", cardAcceptorTerminalId="
				+ cardAcceptorTerminalId + ", cardAcceptorId=" + cardAcceptorId + ", cardAcceptorNameLocation="
				+ cardAcceptorNameLocation + ", additionalResponseData=" + additionalResponseData + ", track1Data="
				+ (track1Data != null ? "PRESENT" : "NOT PRESENT") + ", additionalData48=" + additionalData48
				+ ", transactionCurrencyCode=" + transactionCurrencyCode + ", settlementCurrencyCode="
				+ settlementCurrencyCode + ", cardholderBillingCurrencyCode=" + cardholderBillingCurrencyCode
				+ ", pinData=" + (pinData != null ? "PRESENT" : "NOT PRESENT") + ", additionalAmount="
				+ additionalAmount + ", chipData=" + chipData + ", adviceReasonCode=" + adviceReasonCode
				+ ", posDataCode=" + posDataCode + ", privateData1BiometricData=" + privateData1BiometricData
				+ ", privateData2EncryptedBiometricData=" + privateData2EncryptedBiometricData
				+ ", networkManagementCode=" + networkManagementCode + ", originalDataElement=" + originalDataElement
				+ ", fileUpdateCode=" + fileUpdateCode + ", replacementAmount=" + replacementAmount + ", fileName101="
				+ fileName101 + ", accountIdentification1=" + accountIdentification1 + ", accountIdentification2="
				+ accountIdentification2 + ", checkSum=" + checkSum + ", visaCustomPaymentServiceField="
				+ (visaCustomPaymentServiceField == null ? "" : visaCustomPaymentServiceField.toString())
				+ ", visaPrivateNetworkField="
				+ (visaPrivateNetworkField == null ? "" : visaPrivateNetworkField.toString())
				+ ", visaPrivateMerchantField="
				+ (visaPrivateMerchantField == null ? "" : visaPrivateMerchantField.toString())
				+ ", visaAdditionalResponseField="
				+ (visaAdditionalResponseField == null ? "" : visaAdditionalResponseField.toString())
				+ ", authenticationField=" + (authenticationField == null ? "" : authenticationField.toString());
	}
}