package com.neo.aggregator.utility;

import java.util.Calendar;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

@Component
public class IdGeneratorUtility {
	
    public static String getRequestId() {
		String id = RandomStringUtils.random(18, false, true);
    	return id;
    }

	public synchronized String getSBMPartnerRequestId(String partnerName, String userName) {
		
		String id = generateRandomNumber(15);
		String requestId = partnerName.substring(0, 4) + "_" + userName + "_" + id;
		return requestId;
	}
	
	public static String generateRandomNumber(int maxValue) {
        Random rand = new Random();
        int random;
        String id = String.valueOf(System.nanoTime());

        if (id.length() < maxValue) {
            int diff = Math.subtractExact(maxValue, id.length()) - 1;
            int m = (int) Math.pow(10, diff);
            random = m + rand.nextInt(9 * m);
            id = id + String.valueOf(random);
        } else {
            id = id.substring(0, maxValue);
        }
        return id;
    }

	public static String getRandomUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}

	public static String getStan() {
		Calendar cal = Calendar.getInstance();
		String mills = String.valueOf(cal.getTimeInMillis());
		return mills.substring(0, 4);
	}
	
}
