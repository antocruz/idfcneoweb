package com.neo.aggregator.utility;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import com.neo.aggregator.bank.sbm.EkycResponseDto;
import com.neo.aggregator.bank.sbm.core.request.AcctLienAddRequestDto;
import com.neo.aggregator.bank.sbm.core.request.AddOutBoundPymtRequestDto;
import com.neo.aggregator.bank.sbm.core.request.BalinqRequestDto;
import com.neo.aggregator.bank.sbm.core.request.CreditAddRequestDto;
import com.neo.aggregator.bank.sbm.core.request.DebitAddRequestDto;
import com.neo.aggregator.bank.sbm.core.request.FetchAccountStatementRequestDto;
import com.neo.aggregator.bank.sbm.core.request.IFTTransferRequestDto;
import com.neo.aggregator.bank.sbm.core.request.MiniStatementRequestDto;
import com.neo.aggregator.bank.sbm.core.request.RetCustAddRequestDto;
import com.neo.aggregator.bank.sbm.core.request.SBAcctAddRequestDto;
import com.neo.aggregator.bank.sbm.core.request.SBAcctInquiryRequestDto;
import com.neo.aggregator.bank.sbm.core.request.SBAcctModRequestDto;
import com.neo.aggregator.bank.sbm.core.request.TdAcctAddRequestDto;
import com.neo.aggregator.bank.sbm.core.request.TdAcctCloseRequestDto;
import com.neo.aggregator.bank.sbm.core.request.TdAcctInquiryRequestDto;
import com.neo.aggregator.bank.sbm.core.request.TdAcctTrailCloseRequestDto;
import com.neo.aggregator.bank.sbm.core.response.AcctLienAddResponseDto;
import com.neo.aggregator.bank.sbm.core.response.AddOutBoundPymtResponseDto;
import com.neo.aggregator.bank.sbm.core.response.BalinqResponseDto;
import com.neo.aggregator.bank.sbm.core.response.CreditAddResponseDto;
import com.neo.aggregator.bank.sbm.core.response.DebitAddResponseDto;
import com.neo.aggregator.bank.sbm.core.response.FetchAccountStatementResponseDto;
import com.neo.aggregator.bank.sbm.core.response.IFTTransferResponseDto;
import com.neo.aggregator.bank.sbm.core.response.MiniStatementResponseDto;
import com.neo.aggregator.bank.sbm.core.response.RetCustAddResponseDto;
import com.neo.aggregator.bank.sbm.core.response.SBAcctAddResponseDto;
import com.neo.aggregator.bank.sbm.core.response.SBAcctInquiryResponseDto;
import com.neo.aggregator.bank.sbm.core.response.SBModResponseDto;
import com.neo.aggregator.bank.sbm.core.response.TDAccountCloseTrailResponseDto;
import com.neo.aggregator.bank.sbm.core.response.TdAcctAddResponseDto;
import com.neo.aggregator.bank.sbm.core.response.TdAcctCloseResponseDto;
import com.neo.aggregator.bank.sbm.core.response.TdAcctInquiryResponseDto;
import com.neo.aggregator.bank.sbm.request.ESBRequestDto;
import com.neo.aggregator.bank.sbm.request.IMPSRequestDto;
import com.neo.aggregator.bank.sbm.request.LoginRequestDto;
import com.neo.aggregator.bank.sbm.request.RequestInDto;
import com.neo.aggregator.bank.sbm.request.ValidateOtpRequestDto;
import com.neo.aggregator.bank.sbm.response.LoginResponseDto;
import com.neo.aggregator.bank.sbm.response.ResponseOutDto;
import com.neo.aggregator.dto.TDAcctCloseTrailResponseDto;

public class NeoJAXBContextFactory {
	
    private static JAXBContext retCustAddRequest = null;
    
    private static JAXBContext retCustAddResponse = null;
    
    private static JAXBContext ekycResponse = null;
    
    private static JAXBContext loginRequest = null;
    
    private static JAXBContext loginResponse = null;
    
    private static JAXBContext esbRequestDto = null;
    
    private static JAXBContext sbAcctAddRequest = null;
    
    private static JAXBContext sbAcctAddResponse = null;
    
    private static JAXBContext balInqRequest = null;
    
    private static JAXBContext balInqResponse = null;
    
    private static JAXBContext fetchAccountStatementRequest = null;
    
    private static JAXBContext fetchAccountStatementResponse = null;
    
    private static JAXBContext tdAcctAddRequest = null;
    
    private static JAXBContext tdAcctAddResponse = null;
    
    private static JAXBContext addOutBoundPymtRequest = null;
    
    private static JAXBContext addOutBoundPymtResponse = null;
    
    private static JAXBContext impsRequestDto = null;
    
    private static JAXBContext iftTransferRequest = null;
    
    private static JAXBContext iftTransferResponse = null;
    
    private static JAXBContext sbAcctModRequest = null;
    
    private static JAXBContext sbAcctModResponse = null;
    
    private static JAXBContext creditAddRequest = null;
    
    private static JAXBContext creditAddResponse = null;
    
    private static JAXBContext debitAddRequest = null;
    
    private static JAXBContext debitAddResponse = null;
    
    private static JAXBContext tdAcctCloseRequest = null;
    
    private static JAXBContext tdAcctCloseResponse = null;
    
    private static JAXBContext tdAcctTrailCloseRequest = null;
    
    private static JAXBContext tdAcctTrailCloseResponse = null;
    
    private static JAXBContext tdAcctInquiryRequest = null;
    
    private static JAXBContext tdAcctInquiryResponse = null;
    
    private static JAXBContext acctLienAddRequest = null;
    
    private static JAXBContext acctLienAddResponse = null;
    
    private static JAXBContext sbAcctInquiryRequest = null;
    
    private static JAXBContext sbAcctInquiryResponse = null;
    
    private static JAXBContext responseOut = null;
    
    private static JAXBContext requestIn = null;
    
    private static JAXBContext validateOtpRequest = null;
    
    private static JAXBContext miniStatementRequest = null;
    
    private static JAXBContext miniStatementResponse = null;
    
    public static synchronized JAXBContext getRequestInContext() throws JAXBException {
    	
        if(requestIn == null){
        	requestIn = JAXBContext.newInstance(RequestInDto.class);
        }
        return requestIn;
    }
    
    public static synchronized JAXBContext getResponseOutContext() throws JAXBException {
    	
        if(responseOut == null){
        	responseOut = JAXBContext.newInstance(ResponseOutDto.class);
        }
        return responseOut;
    }
    
    public static synchronized JAXBContext getRetCustAddRequestContext() throws JAXBException {
    	
        if(retCustAddRequest == null){
        	retCustAddRequest = JAXBContext.newInstance(RetCustAddRequestDto.class);
        }
        return retCustAddRequest;
    }
    
    public static synchronized JAXBContext getRetCustAddResponseContext() throws JAXBException {
    	
        if(retCustAddResponse == null){
        	retCustAddResponse = JAXBContext.newInstance(RetCustAddResponseDto.class);
        }
        return retCustAddResponse;
    }
    
    public static synchronized JAXBContext getEkycResponseContext() throws JAXBException {
    	
        if(ekycResponse == null){
        	ekycResponse = JAXBContext.newInstance(EkycResponseDto.class);
        }
        return ekycResponse;
    }
    
    public static synchronized JAXBContext getLoginRequestContext() throws JAXBException {
    	
        if(loginRequest == null){
        	loginRequest = JAXBContext.newInstance(LoginRequestDto.class);
        }
        return loginRequest;
    }
    
    public static synchronized JAXBContext getLoginResponseContext() throws JAXBException {
    	
        if(loginResponse == null){
        	loginResponse = JAXBContext.newInstance(LoginResponseDto.class);
        }
        return loginResponse;
    }
    
    public static synchronized JAXBContext getESBRequestContext() throws JAXBException {

        if(esbRequestDto == null){
        	esbRequestDto = JAXBContext.newInstance(ESBRequestDto.class); 
        }
        return esbRequestDto;
    }
    
    public static synchronized JAXBContext getSBAcctAddRequestContext() throws JAXBException {

        if(sbAcctAddRequest == null){
        	sbAcctAddRequest = JAXBContext.newInstance(SBAcctAddRequestDto.class); 
        }
        return sbAcctAddRequest;
    }
    
    public static synchronized JAXBContext getSBAcctAddResponseContext() throws JAXBException {

        if(sbAcctAddResponse == null){
        	sbAcctAddResponse = JAXBContext.newInstance(SBAcctAddResponseDto.class); 
        }
        return sbAcctAddResponse;
    }
    
    public static synchronized JAXBContext getBalInqRequestContext() throws JAXBException {

        if(balInqRequest == null){
        	balInqRequest = JAXBContext.newInstance(BalinqRequestDto.class); 
        }
        return balInqRequest;
    }
    
    public static synchronized JAXBContext getBalInqResponseContext() throws JAXBException {

        if(balInqResponse == null){
        	balInqResponse = JAXBContext.newInstance(BalinqResponseDto.class); 
        }
        return balInqResponse;
    }
    
    public static synchronized JAXBContext getfetchAcctStatementRequestContext() throws JAXBException {

        if(fetchAccountStatementRequest == null){
        	fetchAccountStatementRequest = JAXBContext.newInstance(FetchAccountStatementRequestDto.class); 
        }
        return fetchAccountStatementRequest;
    }
    
    public static synchronized JAXBContext getfetchAcctStatementResponseContext() throws JAXBException {

        if(fetchAccountStatementResponse == null){
        	fetchAccountStatementResponse = JAXBContext.newInstance(FetchAccountStatementResponseDto.class); 
        }
        return fetchAccountStatementResponse;
    }
    
    public static synchronized JAXBContext getTdAcctAddRequestContext() throws JAXBException {

        if(tdAcctAddRequest == null){
        	tdAcctAddRequest = JAXBContext.newInstance(TdAcctAddRequestDto.class); 
        }
        return tdAcctAddRequest;
    }
    
    public static synchronized JAXBContext getTdAcctAddResponseContext() throws JAXBException {

        if(tdAcctAddResponse == null){
        	tdAcctAddResponse = JAXBContext.newInstance(TdAcctAddResponseDto.class); 
        }
        return tdAcctAddResponse;
    }
    
    public static synchronized JAXBContext getAddOutBoundPymtRequestContext() throws JAXBException {

        if(addOutBoundPymtRequest == null){
        	addOutBoundPymtRequest = JAXBContext.newInstance(AddOutBoundPymtRequestDto.class); 
        }
        return addOutBoundPymtRequest;
    }
    
    public static synchronized JAXBContext getAddOutBoundPymtResponseContext() throws JAXBException {

        if(addOutBoundPymtResponse == null){
        	addOutBoundPymtResponse = JAXBContext.newInstance(AddOutBoundPymtResponseDto.class); 
        }
        return addOutBoundPymtResponse;
    }
    
    public static synchronized JAXBContext getImpsRequestContext() throws JAXBException {

        if(impsRequestDto == null){
        	impsRequestDto = JAXBContext.newInstance(IMPSRequestDto.class); 
        }
        return impsRequestDto;
    }
    
    public static synchronized JAXBContext getIftTransferRequestContext() throws JAXBException {

        if(iftTransferRequest == null){
        	iftTransferRequest = JAXBContext.newInstance(IFTTransferRequestDto.class); 
        }
        return iftTransferRequest;
    }
    
    public static synchronized JAXBContext getIftTransferResponseContext() throws JAXBException {

        if(iftTransferResponse == null){
        	iftTransferResponse = JAXBContext.newInstance(IFTTransferResponseDto.class); 
        }
        return iftTransferResponse;
    }
    
    public static synchronized JAXBContext getSbAcctModRequestContext() throws JAXBException {

        if(sbAcctModRequest == null){
        	sbAcctModRequest = JAXBContext.newInstance(SBAcctModRequestDto.class); 
        }
        return sbAcctModRequest;
    }
    
    public static synchronized JAXBContext getSbAcctModResponseContext() throws JAXBException {

        if(sbAcctModResponse == null){
        	sbAcctModResponse = JAXBContext.newInstance(SBModResponseDto.class); 
        }
        return sbAcctModResponse;
    }
    
    public static synchronized JAXBContext getCreditAddRequestContext() throws JAXBException {

        if(creditAddRequest == null){
        	creditAddRequest = JAXBContext.newInstance(CreditAddRequestDto.class); 
        }
        return creditAddRequest;
    }
    
    public static synchronized JAXBContext getCreditAddResponseContext() throws JAXBException {

        if(creditAddResponse == null){
        	creditAddResponse = JAXBContext.newInstance(CreditAddResponseDto.class); 
        }
        return creditAddResponse;
    }
    
    public static synchronized JAXBContext getDebitAddRequestContext() throws JAXBException {

        if(debitAddRequest == null){
        	debitAddRequest = JAXBContext.newInstance(DebitAddRequestDto.class); 
        }
        return debitAddRequest;
    }
    
    public static synchronized JAXBContext getDebitAddResponseContext() throws JAXBException {

        if(debitAddResponse == null){
        	debitAddResponse = JAXBContext.newInstance(DebitAddResponseDto.class); 
        }
        return debitAddResponse;
    }
    
    public static synchronized JAXBContext getTdAcctCloseRequestContext() throws JAXBException {

        if(tdAcctCloseRequest == null){
        	tdAcctCloseRequest = JAXBContext.newInstance(TdAcctCloseRequestDto.class); 
        }
        return tdAcctCloseRequest;
    }
    
    public static synchronized JAXBContext getTdAcctCloseResponseContext() throws JAXBException {

        if(tdAcctCloseResponse == null){
        	tdAcctCloseResponse = JAXBContext.newInstance(TdAcctCloseResponseDto.class); 
        }
        return tdAcctCloseResponse;
    }
    
    public static synchronized JAXBContext getTdTrailCloseRequestContext() throws JAXBException {

        if(tdAcctTrailCloseRequest == null){
        	tdAcctTrailCloseRequest = JAXBContext.newInstance(TdAcctTrailCloseRequestDto.class); 
        }
        return tdAcctTrailCloseRequest;
    }
    
    public static synchronized JAXBContext getTdTrailCloseResponseContext() throws JAXBException {

        if(tdAcctTrailCloseResponse == null){
        	tdAcctTrailCloseResponse = JAXBContext.newInstance(TDAccountCloseTrailResponseDto.class); 
        }
        return tdAcctTrailCloseResponse;
    }
    
    public static synchronized JAXBContext getTdAcctInquiryRequestContext() throws JAXBException {

        if(tdAcctInquiryRequest == null){
        	tdAcctInquiryRequest = JAXBContext.newInstance(TdAcctInquiryRequestDto.class); 
        }
        return tdAcctInquiryRequest;
    }
    
    public static synchronized JAXBContext getTdAcctInquiryResponseContext() throws JAXBException {

        if(tdAcctInquiryResponse == null){
        	tdAcctInquiryResponse = JAXBContext.newInstance(TdAcctInquiryResponseDto.class); 
        }
        return tdAcctInquiryResponse;
    }
    
    public static synchronized JAXBContext getAcctLienAddRequestContext() throws JAXBException {

        if(acctLienAddRequest == null){
        	acctLienAddRequest = JAXBContext.newInstance(AcctLienAddRequestDto.class); 
        }
        return acctLienAddRequest;
    }
    
    public static synchronized JAXBContext getAcctLienAddResponseContext() throws JAXBException {

        if(acctLienAddResponse == null){
        	acctLienAddResponse = JAXBContext.newInstance(AcctLienAddResponseDto.class); 
        }
        return acctLienAddResponse;
    }
    
    public static synchronized JAXBContext getSBAcctInquiryRequestContext() throws JAXBException {

        if(sbAcctInquiryRequest == null){
        	sbAcctInquiryRequest = JAXBContext.newInstance(SBAcctInquiryRequestDto.class); 
        }
        return sbAcctInquiryRequest;
    }
    
    public static synchronized JAXBContext getSBAcctInquiryResponseContext() throws JAXBException {

        if(sbAcctInquiryResponse == null){
        	sbAcctInquiryResponse = JAXBContext.newInstance(SBAcctInquiryResponseDto.class); 
        }
        return sbAcctInquiryResponse;
    }
    
    public static synchronized JAXBContext getValidateOtpRequestContext() throws JAXBException {

        if(validateOtpRequest == null){
        	validateOtpRequest = JAXBContext.newInstance(ValidateOtpRequestDto.class); 
        }
        return validateOtpRequest;
    }
    
    public static synchronized JAXBContext getMiniStatementRequestContext() throws JAXBException {

        if(miniStatementRequest == null){
        	miniStatementRequest = JAXBContext.newInstance(MiniStatementRequestDto.class); 
        }
        return miniStatementRequest;
    }
    
    public static synchronized JAXBContext getMiniStatementResponseContext() throws JAXBException {

        if(miniStatementResponse == null){
        	miniStatementResponse = JAXBContext.newInstance(MiniStatementResponseDto.class); 
        }
        return miniStatementResponse;
    }
    

}
