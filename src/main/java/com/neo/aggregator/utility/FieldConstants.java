package com.neo.aggregator.utility;

public class FieldConstants {
	
	public static final Integer PAN_IDENTIFIER = 2;

	public static final Integer TRANSACTION_IDENTIFIER = 3;
	
	public static final Integer TRANSACTION_AMOUNT = 4;
	
	
	public static final Integer DATE_TIME_TRANSMISSION = 7;
	
	public static final Integer SYSTEM_TRACE_IDENTIFIER = 11;
	
	public static final Integer LOCAL_TIME_IDENTIFIER = 12;
	
	public static final Integer LOCAL_DATE_IDENTIFIER = 13;
	
	public static final Integer MCC_IDENTIFIER = 18;
	
	public static final Integer ACQ_COUNTRY_IDENTIFIER = 19;
	
	public static final Integer POS_ENTRY_MODE_IDENTIFIER = 22;
	
	public static final Integer CARD_SEQUENCE_NUMBER = 23;
	
	public static final Integer POS_CONDITION_CODE = 25;
	
	public static final Integer FEE_AMOUNT = 28;
	
	public static final Integer ACQ_INSTITUTION_IDENTIFIER = 32;
	
	public static final Integer FWD_INSTITUTION_CODE = 33;
	
	public static final Integer RETRIVAL_REFERNCE_NO_ID = 37;
	
	public static final Integer AUTHORIZATION_CODE = 38;
	
	public static final Integer RESPONSE_CODE = 39;
	
	public static final Integer SERVICE_CODE = 40;
	
	public static final Integer CARD_ACCEPTOR_TERMINAL = 41;
	
	public static final Integer CARD_ACCEPTOR_ID = 42;
	
	public static final Integer CARD_ACCEPTOR_NAME = 43;
	
	public static final Integer TRACK_DATA1 = 45;
	
	public static final Integer ADDITIONAL_DATA = 48;
	
	
	public static final Integer TXN_CURRENCY_CODE = 49;
	
	public static final Integer SETTLEMENT_CURRENCY_CODE = 50;
	
	public static final Integer CARDHOLDER_BILLING_CURRENCY_CODE = 51;
	
	public static final Integer PIN_DATA = 52;
	
	public static final Integer CHIP_DATA = 55;
	
	public static final Integer POS_DATA_CODE = 61;
	
	public static final Integer ACCOUND_ID_1 = 102;
	
	public static final Integer ACCOUND_ID_2 = 103;
	
	public static final Integer PRIVATE_DATA3 = 120;
	
	public static final String PRIVATE_DATA3_TAG001_TXN_TYPE = "001";
	
	public static final String PRIVATE_DATA3_TAG002_PRODUCT_INDICATOR= "002";
	
	public static final Integer PRIVATE_DATA4 = 121;
	
	public static final Integer PRIVATE_DATA5 = 122;
	
	public static final Integer PRIVATE_DATA6 = 123;
	
	public static final Integer OCT_ISSUER_ADDITIONAL_DATA = 104;
	
	
	
	public static final String POS_TXN_ID = "00";
	
	public static final String ECOM_TXN_ID = "00";
	
	public static final Integer MAGNETIC_STRIP_ID_35 = 35;
	
	public static final Integer MAGNETIC_STRIP_ID_45 = 45;
	
	public static final Integer POS_ENTRY_MODE_ID = 22;
	
	
	public static final String POS_ENTRY_MODE_VALUE = "81";

	public static final int NETWORK_MANAGEMENT_CODE_ID = 70;
}
