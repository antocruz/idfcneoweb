package com.neo.aggregator.utility;

import org.springframework.stereotype.Component;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

@Component
public class AESEncryptionUtil {

	public static final String encDecKey="M2PFinTechSolutn";
	public static final String encDecIv="f7380293bb113241";

	public byte[] encrypt(String data) {
		byte[] encrypt = null;
		try {
			IvParameterSpec iv = new IvParameterSpec(encDecIv.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(encDecKey.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

			byte[] encrypted = cipher.doFinal(data.getBytes());
			return encrypted;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return encrypt;
	}

	public byte[] decrypt(byte[] data) {
		byte[] decrypt = null;
		try {
			IvParameterSpec iv = new IvParameterSpec(encDecIv.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(encDecKey.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
			byte[] original = cipher.doFinal(data);
			return original;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return decrypt;
	}

}