package com.neo.aggregator.utility;

public final class NeoExceptionConstant {
	
	public static final String ACCOUNT_NUMBER_NOT_FOUND = "neo.account.number.not.found";
	public static final String GEO_MASTER_NOT_FOUND = "neo.geo.master.not.found";
	public static final String TENANT_NOT_FOUND = "neo.tenant.not.found";
	public static final String SERVICE_NOT_FOUND = "neo.service.not.found";
	public static final String UNABLE_FETCH_ACCOUNT = "neo.unable.fetch.account";
	public static final String REGISTRATION_FAILED = "neo.registration.failed";
	public static final String SOMETHING_WENT_WRONG = "neo.something.went.wrong";
	public static final String PRODUCT_CONFIG_NOT_FOUND = "neo.product.config.not.found";
	public static final String SERVICE_UNAVAILABLE = "neo.service.unavailable";
	public static final String SYSTEM_ERROR = "neo.system.error";
	public static final String ACCOUNTINFO_INCORRECT = "neo.accountinfo.incorrect";
	public static final String INVALID_CIF_RESPONSE = "neo.invalid.cif.response";
	public static final String REGISTRATION_PRODUCT_ALREADY_EXISTS = "neo.registration.product.already.exists";
	public static final String NOMINEE_CONTACTINFO_INCORRECT = "neo.nominee.contactinfo.incorrect";
	public static final String NOMINEE_INFO_INCORRECT = "neo.nominee.info.incorrect";
	public static final String REPAY_ACCOUNT_INVALID = "neo.repay.account.invalid";
	public static final String TD_TRAIL_CLOSURE_FAILED = "neo.td.trail.closure.failed";
	public static final String BANK_EXCEPTION_OCCURED = "neo.bank.exception.occured";
	public static final String INVALID_TRANSACTION_MODULE_ID = "neo.invalid.transaction.moduleid";
	public static final String INVALID_REMITTER_NUMBER = "neo.invalid.remitter.number";
	public static final String INVALID_ENTITY_ID = "neo.invalid.entity.id";
	
	public static final String INTERNAL_SERVER_ERROR = "Internal Server Error";
	
	private NeoExceptionConstant() {
		
	}
}
