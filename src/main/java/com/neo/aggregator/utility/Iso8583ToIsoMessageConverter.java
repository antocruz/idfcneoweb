package com.neo.aggregator.utility;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.neo.aggregator.constant.IsoConstants;
import com.neo.aggregator.enums.FieldType;
import com.neo.aggregator.enums.VisaFieldType;
import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.iso8583.IsoValue;
import com.neo.aggregator.iso8583.util.HexCodec;
import com.neo.aggregator.utility.Iso8583.VisaCustomPaymentServiceField;
import com.neo.aggregator.utility.Iso8583.VisaPrivateMerchantField;
import com.neo.aggregator.utility.Iso8583.VisaPrivateNetworkField;

@Component
public class Iso8583ToIsoMessageConverter implements CustomConverter {
	
	@Autowired
	private VisaFieldParser visaParser;

	@Override
	public IsoMessage convert(Iso8583 input) {
		
		IsoMessage isoMessage = new IsoMessage();
		if (!StringUtils.isEmpty(input.getPan())) {
			isoMessage.setField(FieldType.PAN.getPosition(),
					new IsoValue<String>(FieldType.PAN.getFieldType(), input.getPan()));
		}

		if (!StringUtils.isEmpty(input.getProcessingCode())) {
			isoMessage.setField(FieldType.PROCESSING_CODE.getPosition(),
					new IsoValue<String>(FieldType.PROCESSING_CODE.getFieldType(), input.getProcessingCode()));
		}

		if (!StringUtils.isEmpty(input.getTransactionAmount())) {
			isoMessage.setField(FieldType.TRANSACTION_AMOUNT.getPosition(), new IsoValue<String>(
					FieldType.TRANSACTION_AMOUNT.getFieldType(), input.getTransactionAmount()));
		}

		if (!StringUtils.isEmpty(input.getSettlementAmount())) {
			isoMessage.setField(FieldType.SETTLEMENT_AMOUNT.getPosition(),
					new IsoValue<String>(FieldType.SETTLEMENT_AMOUNT.getFieldType(), input.getSettlementAmount()));
		}

		if (!StringUtils.isEmpty(input.getCardHolderBillingAmount())) {
			isoMessage.setField(FieldType.CARDHOLDER_BILLING_AMOUNT.getPosition(), new IsoValue<String>(
					FieldType.CARDHOLDER_BILLING_AMOUNT.getFieldType(), input.getCardHolderBillingAmount()));
		}

		if (!StringUtils.isEmpty(input.getCardHolderBillingAmount())) {
			isoMessage.setField(FieldType.CARDHOLDER_BILLING_AMOUNT.getPosition(), new IsoValue<String>(
					FieldType.CARDHOLDER_BILLING_AMOUNT.getFieldType(), input.getCardHolderBillingAmount()));
		}

		if (!StringUtils.isEmpty(input.getDateTimeTransaction())) {
			isoMessage.setField(FieldType.TRANSMISSION_DATE_TIME.getPosition(), new IsoValue<String>(
					FieldType.TRANSMISSION_DATE_TIME.getFieldType(), input.getDateTimeTransaction()));
		}

		if (!StringUtils.isEmpty(input.getSettlementConversionRate())) {
			isoMessage.setField(FieldType.SETTLMENT_CONVERSION_RATE.getPosition(), new IsoValue<String>(
					FieldType.SETTLMENT_CONVERSION_RATE.getFieldType(), input.getSettlementConversionRate()));
		}

		if (!StringUtils.isEmpty(input.getCardholderBillingConversionRate())) {
			isoMessage.setField(FieldType.CARDHOLDER_SETTLMENT_CONVERSION_RATE.getPosition(),
					new IsoValue<String>(FieldType.CARDHOLDER_SETTLMENT_CONVERSION_RATE.getFieldType(),
							input.getCardholderBillingConversionRate()));
		}

		if (!StringUtils.isEmpty(input.getStan())) {
			isoMessage.setField(FieldType.STAN.getPosition(),
					new IsoValue<String>(FieldType.STAN.getFieldType(), input.getStan()));
		}

		if (!StringUtils.isEmpty(input.getTimeLocalTransaction())) {
			isoMessage.setField(FieldType.TIME_LOCAL_TXN.getPosition(), new IsoValue<String>(
					FieldType.TIME_LOCAL_TXN.getFieldType(), input.getTimeLocalTransaction()));
		}

		if (!StringUtils.isEmpty(input.getDateLocalTransaction())) {
			isoMessage.setField(FieldType.DATE_LOCAL_TXN.getPosition(), new IsoValue<String>(
					FieldType.DATE_LOCAL_TXN.getFieldType(), input.getDateLocalTransaction()));
		}

//		if (!StringUtils.isEmpty(input.getExpiryDate())) {
//			isoMessage.setField(RupayFieldType.EXPIRY_DATE.getPosition(),
//					new IsoValue<String>(RupayFieldType.EXPIRY_DATE.getFieldType(), input.getExpiryDate()));
//		}

		if (!StringUtils.isEmpty(input.getSettlementDate())) {
			isoMessage.setField(FieldType.SETTLEMENT_DATE.getPosition(),
					new IsoValue<String>(FieldType.SETTLEMENT_DATE.getFieldType(), input.getSettlementDate()));
		}

		if (!StringUtils.isEmpty(input.getConversionDate())) {
			isoMessage.setField(FieldType.CONVERSION_DATE.getPosition(),
					new IsoValue<String>(FieldType.CONVERSION_DATE.getFieldType(), input.getConversionDate()));
		}

		if (!StringUtils.isEmpty(input.getMcc())) {
			isoMessage.setField(FieldType.MCC.getPosition(),
					new IsoValue<String>(FieldType.MCC.getFieldType(), input.getMcc()));
		}

		if (!StringUtils.isEmpty(input.getAcquiringCountryCode())) {
			isoMessage.setField(FieldType.ACQUIRING_COUNRY_CODE.getPosition(), new IsoValue<String>(
					FieldType.ACQUIRING_COUNRY_CODE.getFieldType(), input.getAcquiringCountryCode()));
		}

		if (!StringUtils.isEmpty(input.getPosEntryMode())) {
			isoMessage.setField(FieldType.POS_ENTRY_MODE.getPosition(),
					new IsoValue<String>(FieldType.POS_ENTRY_MODE.getFieldType(), input.getPosEntryMode()));
		}

		if (!StringUtils.isEmpty(input.getCardSeqNumber())) {
			isoMessage.setField(FieldType.CARD_SEQ_NO.getPosition(),
					new IsoValue<String>(FieldType.CARD_SEQ_NO.getFieldType(), input.getCardSeqNumber()));
		}

		if (!StringUtils.isEmpty(input.getPointOfServiceConditionCode())) {
			isoMessage.setField(FieldType.POS_CONDITION_CODE.getPosition(), new IsoValue<String>(
					FieldType.POS_CONDITION_CODE.getFieldType(), input.getPointOfServiceConditionCode()));
		}

		if (!StringUtils.isEmpty(input.getFeesAmount())) {
			isoMessage.setField(FieldType.FEES_AMOUNT.getPosition(),
					new IsoValue<String>(FieldType.FEES_AMOUNT.getFieldType(), input.getFeesAmount()));
		}

		if (!StringUtils.isEmpty(input.getAcquiringInstitutionCode())) {
			isoMessage.setField(FieldType.ACQUIRING_INSTITUTION_CODE.getPosition(), new IsoValue<String>(
					FieldType.ACQUIRING_INSTITUTION_CODE.getFieldType(), input.getAcquiringInstitutionCode()));
		}

		if (!StringUtils.isEmpty(input.getForwardingInstitutionCode())) {
			isoMessage.setField(FieldType.FORWARDING_INSTITUTION_CODE.getPosition(), new IsoValue<String>(
					FieldType.FORWARDING_INSTITUTION_CODE.getFieldType(), input.getForwardingInstitutionCode()));
		}

		if (!StringUtils.isEmpty(input.getTrack2Data())) {
			isoMessage.setField(FieldType.TRACK_DATA2.getPosition(),
					new IsoValue<String>(FieldType.TRACK_DATA2.getFieldType(), input.getTrack2Data()));
		}

		if (!StringUtils.isEmpty(input.getRrn())) {
			isoMessage.setField(FieldType.RRN.getPosition(),
					new IsoValue<String>(FieldType.RRN.getFieldType(), input.getRrn()));
		}

		if (!StringUtils.isEmpty(input.getAuthorizationIdResponse())) {
			isoMessage.setField(FieldType.AUTHORIZATION_ID_RESPONSE.getPosition(), new IsoValue<String>(
					FieldType.AUTHORIZATION_ID_RESPONSE.getFieldType(), input.getAuthorizationIdResponse()));
		}

		if (!StringUtils.isEmpty(input.getResponseCode())) {
			isoMessage.setField(FieldType.RESPONSE_CODE.getPosition(),
					new IsoValue<String>(FieldType.RESPONSE_CODE.getFieldType(), input.getResponseCode()));
		}

		if (!StringUtils.isEmpty(input.getServiceConditionCode())) {
			isoMessage.setField(FieldType.SERVICE_CONDITION_CODE.getPosition(), new IsoValue<String>(
					FieldType.SERVICE_CONDITION_CODE.getFieldType(), input.getServiceConditionCode()));
		}

		if (!StringUtils.isEmpty(input.getCardAcceptorTerminalId())) {
			isoMessage.setField(FieldType.CARD_ACCEPTOR_TERMINAL_ID.getPosition(), new IsoValue<String>(
					FieldType.CARD_ACCEPTOR_TERMINAL_ID.getFieldType(), input.getCardAcceptorTerminalId()));
		}

		if (!StringUtils.isEmpty(input.getCardAcceptorId())) {
			isoMessage.setField(FieldType.CARD_ACCEPTOR_ID.getPosition(),
					new IsoValue<String>(FieldType.CARD_ACCEPTOR_ID.getFieldType(), input.getCardAcceptorId()));
		}

		if (!StringUtils.isEmpty(input.getCardAcceptorNameLocation())) {
			isoMessage.setField(FieldType.CARD_ACCEPTOR_NAME.getPosition(), new IsoValue<String>(
					FieldType.CARD_ACCEPTOR_NAME.getFieldType(), input.getCardAcceptorNameLocation()));
		}

		if (!StringUtils.isEmpty(input.getAdditionalResponseData())) {
			isoMessage.setField(FieldType.ADDITIONAL_RESPONSE_DATA.getPosition(), new IsoValue<String>(
					FieldType.ADDITIONAL_RESPONSE_DATA.getFieldType(), input.getAdditionalResponseData()));
		}

		if (!StringUtils.isEmpty(input.getTrack1Data())) {
			isoMessage.setField(FieldType.TRACK_DATA1.getPosition(),
					new IsoValue<String>(FieldType.TRACK_DATA1.getFieldType(), input.getTrack1Data()));
		}

		if (!StringUtils.isEmpty(input.getAdditionalData48())) {
			isoMessage.setField(FieldType.ADDITIONAL_DATA_48.getPosition(), new IsoValue<String>(
					FieldType.ADDITIONAL_DATA_48.getFieldType(), input.getAdditionalData48()));
		}

		if (!StringUtils.isEmpty(input.getTransactionCurrencyCode())) {
			isoMessage.setField(FieldType.TXN_CURRENCY_CODE.getPosition(), new IsoValue<String>(
					FieldType.TXN_CURRENCY_CODE.getFieldType(), input.getTransactionCurrencyCode()));
		}

		if (!StringUtils.isEmpty(input.getSettlementCurrencyCode())) {
			isoMessage.setField(FieldType.SETTLEMEMNT_CURRENCY_CODE.getPosition(), new IsoValue<String>(
					FieldType.SETTLEMEMNT_CURRENCY_CODE.getFieldType(), input.getSettlementCurrencyCode()));
		}

		if (!StringUtils.isEmpty(input.getCardholderBillingCurrencyCode())) {
			isoMessage.setField(FieldType.CARDHOLDER_BILLING_CURRENCY_CODE.getPosition(),
					new IsoValue<String>(FieldType.CARDHOLDER_BILLING_CURRENCY_CODE.getFieldType(),
							input.getCardholderBillingCurrencyCode()));
		}

		if (!StringUtils.isEmpty(input.getPosDataCode())) {
			isoMessage.setField(FieldType.POS_DATA_CODE.getPosition(),
					new IsoValue<String>(FieldType.POS_DATA_CODE.getFieldType(), input.getPosDataCode()));
		}

		if (!StringUtils.isEmpty(input.getNetworkManagementCode())) {
			isoMessage.setField(FieldType.NETWORK_MANAGEMENT_INFO_CODE.getPosition(), new IsoValue<String>(
					FieldType.NETWORK_MANAGEMENT_INFO_CODE.getFieldType(), input.getNetworkManagementCode()));
		}

		if (!StringUtils.isEmpty(input.getChipData())) {
			isoMessage.setField(FieldType.CHIP_DATA.getPosition(),
					new IsoValue<>(FieldType.CHIP_DATA.getFieldType(), input.getChipData()));
		}

		if (!StringUtils.isEmpty(input.getPinData())) {
			isoMessage.setField(FieldType.PIN_DATA.getPosition(),
					new IsoValue<String>(FieldType.PIN_DATA.getFieldType(), input.getPinData()));
		}

		if (!StringUtils.isEmpty(input.getAdviceReasonCode())) {
			isoMessage.setField(FieldType.ADVICE_REASON_CODE.getPosition(), new IsoValue<String>(
					FieldType.ADVICE_REASON_CODE.getFieldType(), input.getAdviceReasonCode()));
		}

		if (!StringUtils.isEmpty(input.getAdditionalAmount())) {
			isoMessage.setField(FieldType.ADDITIONAL_AMOUNT.getPosition(),
					new IsoValue<String>(FieldType.ADDITIONAL_AMOUNT.getFieldType(), input.getAdditionalAmount()));
		}

		if (!StringUtils.isEmpty(input.getPrivateData1BiometricData())) {
			isoMessage.setField(FieldType.BIOMETRIC_DATA.getPosition(), new IsoValue<String>(
					FieldType.BIOMETRIC_DATA.getFieldType(), input.getPrivateData1BiometricData()));
		}

		if (!StringUtils.isEmpty(input.getPrivateData2EncryptedBiometricData())) {
			isoMessage.setField(FieldType.ENCRYPTED_BIOMETRIC_DATA.getPosition(),
					new IsoValue<String>(FieldType.ENCRYPTED_BIOMETRIC_DATA.getFieldType(),
							input.getPrivateData2EncryptedBiometricData()));
		}

		return isoMessage;
	}

	public IsoMessage visaConvert(Iso8583 input) {
		IsoMessage isoMessage = new IsoMessage();
		
		Map<String, String> visaHeaders=new HashMap<String, String>();
		
			if(input.getVisaHeaders()==null) {
				input.setVisaHeaders(new Iso8583().new VisaHeaders());
			}
			
			visaHeaders.put(IsoConstants.HEADER_LENGTH, input.getVisaHeaders().getHeaderLength());
			visaHeaders.put(IsoConstants.HEADER_FLAG_FORMAT, input.getVisaHeaders().getHeaderFlagFormat());
			visaHeaders.put(IsoConstants.TEXT_FORMAT, input.getVisaHeaders().getTextFormat());
			visaHeaders.put(IsoConstants.DST_STAT_ID, input.getVisaHeaders().getDestinationStationId());
			visaHeaders.put(IsoConstants.SRC_STAT_ID, input.getVisaHeaders().getSourceStationId());
			visaHeaders.put(IsoConstants.RND_CON_INF, input.getVisaHeaders().getRndConIf());
			visaHeaders.put(IsoConstants.BASE_1_FLAG, input.getVisaHeaders().getBase1Flag());
			visaHeaders.put(IsoConstants.MSG_STATUS_FLAG, input.getVisaHeaders().getMessageStatusFlag());
			visaHeaders.put(IsoConstants.BAT_NO, input.getVisaHeaders().getBatNo());
			visaHeaders.put(IsoConstants.RESERVED, input.getVisaHeaders().getReserved());
			visaHeaders.put(IsoConstants.USR_INFO, input.getVisaHeaders().getUserInfo());
			
			isoMessage.setVisaHeaders(visaHeaders);
			isoMessage.setVisaHeader(true);

			isoMessage.setType(Integer.parseInt(input.getMti(), 16));
			isoMessage.setBinary(true);
			isoMessage.setBinaryBitmap(true);
			isoMessage.setCharacterEncoding("8859_1");
			isoMessage.setEbcdicMsgType(false);
			isoMessage.setEtx(-1);

			isoMessage = convertVisa8583ToIsoMessage(input, isoMessage);
		
		return isoMessage;
	}
	
	public IsoMessage visaConvert(Iso8583 input,IsoMessage isoMessage) {
		
		return convertVisa8583ToIsoMessage(input, isoMessage);
	}

	public IsoMessage convertVisa8583ToIsoMessage(Iso8583 input, IsoMessage isoMessage) {
		
		if (!StringUtils.isEmpty(input.getPan())) {
			isoMessage.setValue(VisaFieldType.PAN.getPosition(),HexCodec.hexDecode(input.getPan()),VisaFieldType.PAN.getFieldType(),VisaFieldType.PAN.getLength(),"hex");
		}

		if (!StringUtils.isEmpty(input.getProcessingCode())) {
			isoMessage.setValue(VisaFieldType.PROCESSING_CODE.getPosition(),HexCodec.hexDecode(input.getProcessingCode()),VisaFieldType.PROCESSING_CODE.getFieldType(), VisaFieldType.PROCESSING_CODE.getLength());
		}

		if (!StringUtils.isEmpty(input.getTransactionAmount())) {
			isoMessage.setValue(VisaFieldType.TRANSACTION_AMOUNT.getPosition(),HexCodec.hexDecode(input.getTransactionAmount()), VisaFieldType.TRANSACTION_AMOUNT.getFieldType(), VisaFieldType.TRANSACTION_AMOUNT.getLength());
		}
		
		if (!StringUtils.isEmpty(input.getSettlementAmount())) {
			isoMessage.setValue(VisaFieldType.SETTLEMENT_AMOUNT.getPosition(),HexCodec.hexDecode(input.getSettlementAmount()),VisaFieldType.SETTLEMENT_AMOUNT.getFieldType(), VisaFieldType.SETTLEMENT_AMOUNT.getLength());
		}

		if (!StringUtils.isEmpty(input.getCardHolderBillingAmount())) {
			isoMessage.setValue(VisaFieldType.CARDHOLDER_BILLING_AMOUNT.getPosition(),HexCodec.hexDecode(input.getCardHolderBillingAmount()),VisaFieldType.CARDHOLDER_BILLING_AMOUNT.getFieldType(), VisaFieldType.CARDHOLDER_BILLING_AMOUNT.getLength());
		}

		if (!StringUtils.isEmpty(input.getDateTimeTransaction())) {
			isoMessage.setValue(VisaFieldType.TRANSMISSION_DATE_TIME.getPosition(), HexCodec.hexDecode(input.getDateTimeTransaction()),
					VisaFieldType.TRANSMISSION_DATE_TIME.getFieldType(), VisaFieldType.TRANSMISSION_DATE_TIME.getLength());
		}

		if (!StringUtils.isEmpty(input.getSettlementConversionRate())) {
			isoMessage.setValue(VisaFieldType.SETTLMENT_CONVERSION_RATE.getPosition(), HexCodec.hexDecode(input.getSettlementConversionRate()),
					VisaFieldType.SETTLMENT_CONVERSION_RATE.getFieldType(), VisaFieldType.SETTLMENT_CONVERSION_RATE.getLength());
		}

		if (!StringUtils.isEmpty(input.getCardholderBillingConversionRate())) {
			isoMessage.setValue(VisaFieldType.CARDHOLDER_SETTLMENT_CONVERSION_RATE.getPosition(),HexCodec.hexDecode(input.getCardholderBillingConversionRate()),
					VisaFieldType.CARDHOLDER_SETTLMENT_CONVERSION_RATE.getFieldType(),VisaFieldType.CARDHOLDER_SETTLMENT_CONVERSION_RATE.getLength());
		}

		if (!StringUtils.isEmpty(input.getStan())) {
			isoMessage.setValue(VisaFieldType.STAN.getPosition(),HexCodec.hexDecode(input.getStan()),VisaFieldType.STAN.getFieldType(), VisaFieldType.STAN.getLength());
		}

		if (!StringUtils.isEmpty(input.getTimeLocalTransaction())) {
			isoMessage.setValue(VisaFieldType.TIME_LOCAL_TXN.getPosition(), HexCodec.hexDecode(input.getTimeLocalTransaction()),
					VisaFieldType.TIME_LOCAL_TXN.getFieldType(), VisaFieldType.TIME_LOCAL_TXN.getLength());
		}

		if (!StringUtils.isEmpty(input.getDateLocalTransaction())) {
			isoMessage.setValue(VisaFieldType.DATE_LOCAL_TXN.getPosition(), HexCodec.hexDecode(input.getDateLocalTransaction()),
					VisaFieldType.DATE_LOCAL_TXN.getFieldType(), VisaFieldType.DATE_LOCAL_TXN.getLength());
		}

		if (!StringUtils.isEmpty(input.getSettlementDate())) {
			isoMessage.setValue(VisaFieldType.SETTLEMENT_DATE.getPosition(),HexCodec.hexDecode(input.getSettlementDate()),
					VisaFieldType.SETTLEMENT_DATE.getFieldType(), VisaFieldType.SETTLEMENT_DATE.getLength());
		}

		if (!StringUtils.isEmpty(input.getConversionDate())) {
			isoMessage.setValue(VisaFieldType.CONVERSION_DATE.getPosition(),HexCodec.hexDecode(input.getConversionDate()),VisaFieldType.CONVERSION_DATE.getFieldType(), 
					VisaFieldType.CONVERSION_DATE.getLength());
		}

		if (!StringUtils.isEmpty(input.getMcc())) {
			isoMessage.setValue(VisaFieldType.MCC.getPosition(),HexCodec.hexDecode(input.getMcc()),
					VisaFieldType.MCC.getFieldType(), VisaFieldType.MCC.getLength());
		}

		if (!StringUtils.isEmpty(input.getAcquiringCountryCode())) {
			isoMessage.setValue(VisaFieldType.ACQUIRING_COUNRY_CODE.getPosition(), HexCodec.hexDecode(input.getAcquiringCountryCode()),
					VisaFieldType.ACQUIRING_COUNRY_CODE.getFieldType(), VisaFieldType.ACQUIRING_COUNRY_CODE.getLength());
		}

		if (!StringUtils.isEmpty(input.getPosEntryMode())) {
			isoMessage.setValue(VisaFieldType.POS_ENTRY_MODE.getPosition(),HexCodec.hexDecode(input.getPosEntryMode()),
					VisaFieldType.POS_ENTRY_MODE.getFieldType(),VisaFieldType.POS_ENTRY_MODE.getLength());
		}

		if (!StringUtils.isEmpty(input.getCardSeqNumber())) {
			isoMessage.setValue(VisaFieldType.CARD_SEQ_NO.getPosition(),HexCodec.hexDecode(input.getCardSeqNumber()),
					VisaFieldType.CARD_SEQ_NO.getFieldType(), VisaFieldType.CARD_SEQ_NO.getLength());
		}

		if (!StringUtils.isEmpty(input.getPointOfServiceConditionCode())) {
			isoMessage.setValue(VisaFieldType.POS_CONDITION_CODE.getPosition(), HexCodec.hexDecode(input.getPointOfServiceConditionCode()),
					VisaFieldType.POS_CONDITION_CODE.getFieldType(), VisaFieldType.POS_CONDITION_CODE.getLength());
		}

		if (!StringUtils.isEmpty(input.getFeesAmount())) {
			isoMessage.setValue(VisaFieldType.FEES_AMOUNT.getPosition(),input.getFeesAmount(),
					VisaFieldType.FEES_AMOUNT.getFieldType(), VisaFieldType.FEES_AMOUNT.getLength(),"Cp1047");
		}

		if (!StringUtils.isEmpty(input.getAcquiringInstitutionCode())) {
			
			isoMessage.setValue(VisaFieldType.ACQUIRING_INSTITUTION_CODE.getPosition(),HexCodec.hexDecode(input.getAcquiringInstitutionCode()),
					VisaFieldType.ACQUIRING_INSTITUTION_CODE.getFieldType(), VisaFieldType.ACQUIRING_INSTITUTION_CODE.getLength(),"hex");
		}

		if (!StringUtils.isEmpty(input.getForwardingInstitutionCode())) {
			isoMessage.setValue(VisaFieldType.FORWARDING_INSTITUTION_CODE.getPosition(),HexCodec.hexDecode(input.getForwardingInstitutionCode()),
					VisaFieldType.FORWARDING_INSTITUTION_CODE.getFieldType(), VisaFieldType.FORWARDING_INSTITUTION_CODE.getLength(),"hex");
		}

		if (!StringUtils.isEmpty(input.getTrack2Data())) {
			isoMessage.setValue(VisaFieldType.TRACK_DATA2.getPosition(),HexCodec.hexDecode(input.getTrack2Data()),VisaFieldType.TRACK_DATA2.getFieldType(), VisaFieldType.TRACK_DATA2.getLength(),"hex");
		}

		if (!StringUtils.isEmpty(input.getRrn())) {
			isoMessage.setValue(VisaFieldType.RRN.getPosition(),input.getRrn(),VisaFieldType.RRN.getFieldType(), VisaFieldType.RRN.getLength(),"Cp1047");
		}

		if (!StringUtils.isEmpty(input.getAuthorizationIdResponse())) {
			isoMessage.setValue(VisaFieldType.AUTHORIZATION_ID_RESPONSE.getPosition(), input.getAuthorizationIdResponse(),
					VisaFieldType.AUTHORIZATION_ID_RESPONSE.getFieldType(), VisaFieldType.AUTHORIZATION_ID_RESPONSE.getLength(),"Cp1047");
		}

		if (!StringUtils.isEmpty(input.getResponseCode())) {
			isoMessage.setValue(VisaFieldType.RESPONSE_CODE.getPosition(),input.getResponseCode(),
					VisaFieldType.RESPONSE_CODE.getFieldType(), VisaFieldType.RESPONSE_CODE.getLength(),"Cp1047");
		}

		/*if (!StringUtils.isEmpty(input.getServiceConditionCode())) {
			isoMessage.setValue(VisaFieldType.SERVICE_CONDITION_CODE.getPosition(),input.getServiceConditionCode(),
					VisaFieldType.SERVICE_CONDITION_CODE.getFieldType(), VisaFieldType.SERVICE_CONDITION_CODE.getLength(),"Cp1047");
		}*/

		if (!StringUtils.isEmpty(input.getCardAcceptorTerminalId())) {
			isoMessage.setValue(VisaFieldType.CARD_ACCEPTOR_TERMINAL_ID.getPosition(), input.getCardAcceptorTerminalId(),
					VisaFieldType.CARD_ACCEPTOR_TERMINAL_ID.getFieldType(), VisaFieldType.CARD_ACCEPTOR_TERMINAL_ID.getLength(),"Cp1047");
		}

		if (!StringUtils.isEmpty(input.getCardAcceptorId())) {
			isoMessage.setValue(VisaFieldType.CARD_ACCEPTOR_ID.getPosition(),input.getCardAcceptorId(),
					VisaFieldType.CARD_ACCEPTOR_ID.getFieldType(),VisaFieldType.CARD_ACCEPTOR_ID.getLength(),"Cp1047");
		}

		if (!StringUtils.isEmpty(input.getCardAcceptorNameLocation())) {
			isoMessage.setValue(VisaFieldType.CARD_ACCEPTOR_NAME.getPosition(), input.getCardAcceptorNameLocation(),
					VisaFieldType.CARD_ACCEPTOR_NAME.getFieldType(), VisaFieldType.CARD_ACCEPTOR_NAME.getLength(),"Cp1047");
		}

		if (!StringUtils.isEmpty(input.getAdditionalResponseData())) {
			isoMessage.setValue(VisaFieldType.ADDITIONAL_RESPONSE_DATA.getPosition(), HexCodec.hexDecode(input.getAdditionalResponseData()),
					VisaFieldType.ADDITIONAL_RESPONSE_DATA.getFieldType(), VisaFieldType.ADDITIONAL_RESPONSE_DATA.getLength(),"hex2");
		}

		if (!StringUtils.isEmpty(input.getTrack1Data())) {
			isoMessage.setValue(VisaFieldType.TRACK_DATA1.getPosition(), input.getTrack1Data(),
					VisaFieldType.TRACK_DATA1.getFieldType(), VisaFieldType.TRACK_DATA1.getLength(),"Cp1047");
		}

		if (!StringUtils.isEmpty(input.getAdditionalData48())) {
			isoMessage.setValue(VisaFieldType.ADDITIONAL_DATA_48.getPosition(), HexCodec.hexDecode(input.getAdditionalData48()),
					VisaFieldType.ADDITIONAL_DATA_48.getFieldType(), VisaFieldType.ADDITIONAL_DATA_48.getLength(),"hex2");
		}

		if (!StringUtils.isEmpty(input.getTransactionCurrencyCode())) {
			isoMessage.setValue(VisaFieldType.TXN_CURRENCY_CODE.getPosition(),HexCodec.hexDecode(input.getTransactionCurrencyCode()),
					VisaFieldType.TXN_CURRENCY_CODE.getFieldType(), VisaFieldType.TXN_CURRENCY_CODE.getLength());
		}

		if (!StringUtils.isEmpty(input.getSettlementCurrencyCode())) {
			isoMessage.setValue(VisaFieldType.SETTLEMEMNT_CURRENCY_CODE.getPosition(),HexCodec.hexDecode(input.getSettlementCurrencyCode()),
					VisaFieldType.SETTLEMEMNT_CURRENCY_CODE.getFieldType(), VisaFieldType.SETTLEMEMNT_CURRENCY_CODE.getLength());
		}

		if (!StringUtils.isEmpty(input.getCardholderBillingCurrencyCode())) {
			isoMessage.setValue(VisaFieldType.CARDHOLDER_BILLING_CURRENCY_CODE.getPosition(),HexCodec.hexDecode(input.getCardholderBillingCurrencyCode())
					,VisaFieldType.CARDHOLDER_BILLING_CURRENCY_CODE.getFieldType(),VisaFieldType.CARDHOLDER_BILLING_CURRENCY_CODE.getLength());
		}

		if (!StringUtils.isEmpty(input.getPosDataCode())) {
			isoMessage.setValue(VisaFieldType.POS_DATA_CODE.getPosition(),input.getPosDataCode(),VisaFieldType.POS_DATA_CODE.getFieldType(), 
					VisaFieldType.POS_DATA_CODE.getLength(),"Cp1047");
		}

		if (!StringUtils.isEmpty(input.getNetworkManagementCode())) {
			isoMessage.setValue(VisaFieldType.NETWORK_MANAGEMENT_INFO_CODE.getPosition(),input.getNetworkManagementCode(),
					VisaFieldType.NETWORK_MANAGEMENT_INFO_CODE.getFieldType(), VisaFieldType.NETWORK_MANAGEMENT_INFO_CODE.getLength(),"Cp1047");
		}

		if (!StringUtils.isEmpty(input.getChipData())) {
			isoMessage.setValue(VisaFieldType.CHIP_DATA.getPosition(),HexCodec.hexDecode(input.getChipData()),
					VisaFieldType.CHIP_DATA.getFieldType(), VisaFieldType.CHIP_DATA.getLength(),"hex2");
		}

		if (!StringUtils.isEmpty(input.getPinData())) {
			isoMessage.setValue(VisaFieldType.PIN_DATA.getPosition(),HexCodec.hexDecode(input.getPinData()),
					VisaFieldType.PIN_DATA.getFieldType(), VisaFieldType.PIN_DATA.getLength());
		}

		if (input.getVisaAdditionalPosInformation()!=null) {
			isoMessage.setValue(VisaFieldType.ADDITIONAL_POS_INFO.getPosition(), HexCodec.hexDecode(visaParser.parseAdditionalPosInfo(input.getVisaAdditionalPosInformation())),
					VisaFieldType.ADDITIONAL_POS_INFO.getFieldType(), VisaFieldType.ADDITIONAL_POS_INFO.getLength(),"hex2");
		}

		if (!StringUtils.isEmpty(input.getAdditionalAmount())) {
			isoMessage.setValue(VisaFieldType.ADDITIONAL_AMOUNT.getPosition(), HexCodec.hexDecode(input.getAdditionalAmount()),
					VisaFieldType.ADDITIONAL_AMOUNT.getFieldType(), VisaFieldType.ADDITIONAL_AMOUNT.getLength(),"hex2");
		}

		if (!StringUtils.isEmpty(input.getPrivateData1BiometricData())) {
			isoMessage.setValue(VisaFieldType.BIOMETRIC_DATA.getPosition(), HexCodec.hexDecode(input.getPrivateData1BiometricData()),
					VisaFieldType.BIOMETRIC_DATA.getFieldType(), VisaFieldType.BIOMETRIC_DATA.getLength(),"hex2");
		}

		if (!StringUtils.isEmpty(input.getPrivateData2EncryptedBiometricData())) {
			isoMessage.setValue(VisaFieldType.ENCRYPTED_BIOMETRIC_DATA.getPosition(),HexCodec.hexDecode(input.getPrivateData2EncryptedBiometricData()),
					VisaFieldType.ENCRYPTED_BIOMETRIC_DATA.getFieldType(),VisaFieldType.ENCRYPTED_BIOMETRIC_DATA.getLength());
		}

		if (!StringUtils.isEmpty(input.getAuthenticationField())) {
			isoMessage.setValue(VisaFieldType.AUTHENTICATION_FIELD.getPosition(),HexCodec.hexDecode(input.getAuthenticationField()),
					VisaFieldType.AUTHENTICATION_FIELD.getFieldType(),VisaFieldType.AUTHENTICATION_FIELD.getLength(),"hex2");
		}
		
		if(input.getVisaPrivateNetworkField()!=null) {
			VisaPrivateNetworkField network  = input.getVisaPrivateNetworkField();
				isoMessage.setValue(VisaFieldType.VISA_NETWORK_DATA.getPosition(), visaParser.parseNetworkInfo(network), 
						VisaFieldType.VISA_NETWORK_DATA.getFieldType(), VisaFieldType.VISA_NETWORK_DATA.getLength(), "hex2");
		}
		
		if(!StringUtils.isEmpty(input.getTrack2Data())) {
				isoMessage.setValue(VisaFieldType.TRACK_DATA2.getPosition(),HexCodec.hexDecode(input.getTrack2Data()),
						VisaFieldType.TRACK_DATA2.getFieldType(), VisaFieldType.TRACK_DATA2.getLength(), "hex");
		}
		
		if(!StringUtils.isEmpty(input.getExpiryDate())) {
			isoMessage.setValue(VisaFieldType.EXPIRY_DATE.getPosition(), HexCodec.hexDecode(input.getExpiryDate()), 
					VisaFieldType.EXPIRY_DATE.getFieldType(), VisaFieldType.EXPIRY_DATE.getLength());
		}
		
		if(input.getVisaCustomPaymentServiceField()!=null) {
			VisaCustomPaymentServiceField network  = input.getVisaCustomPaymentServiceField();
				isoMessage.setValue(VisaFieldType.VISA_CUSTOM_DATA.getPosition(), visaParser.parseCustomPaymentInfo(network), 
						VisaFieldType.VISA_CUSTOM_DATA.getFieldType(), VisaFieldType.VISA_CUSTOM_DATA.getLength(), "hex2");
		}
		
		if(input.getVisaPrivateMerchantField()!=null) {
			VisaPrivateMerchantField network  = input.getVisaPrivateMerchantField();
				isoMessage.setValue(VisaFieldType.VISA_PRIVATE_MERCHANT_FIELD.getPosition(), visaParser.parsMerchantAcqInfo(network), 
						VisaFieldType.VISA_PRIVATE_MERCHANT_FIELD.getFieldType(), VisaFieldType.VISA_PRIVATE_MERCHANT_FIELD.getLength(), "hex2");
		}
		
		if(input.getVisaVerificationData_DE123()!=null) {
				isoMessage.setValue(VisaFieldType.VISA_VERIFICATION_DATA_FIELD.getPosition(), input.getVisaVerificationData_DE123(), 
						VisaFieldType.VISA_VERIFICATION_DATA_FIELD.getFieldType(), VisaFieldType.VISA_VERIFICATION_DATA_FIELD.getLength(), "hex2");
		}
		
		if(input.getVisaCustomerRelatedDate_DE121()!=null) {
			isoMessage.setValue(VisaFieldType.ISSUING_INSTITUTION_CODE.getPosition(), input.getVisaCustomerRelatedDate_DE121(), 
					VisaFieldType.ISSUING_INSTITUTION_CODE.getFieldType(), VisaFieldType.ISSUING_INSTITUTION_CODE.getLength(), "hex2");
		}
		
		if(input.getVisaSupportingInfo_DE125()!=null) {
			isoMessage.setValue(VisaFieldType.VISA_SUPPORTING_INFO_FIELD.getPosition(), input.getVisaSupportingInfo_DE125(), 
					VisaFieldType.VISA_SUPPORTING_INFO_FIELD.getFieldType(), VisaFieldType.VISA_SUPPORTING_INFO_FIELD.getLength(), "hex2");
		}
		
		if(input.getVisaTransactionDescription_DE104()!=null) {
			isoMessage.setValue(VisaFieldType.VISA_TRANSACTION_DESCRIPTION_FIELD.getPosition(), input.getVisaTransactionDescription_DE104(), 
					VisaFieldType.VISA_TRANSACTION_DESCRIPTION_FIELD.getFieldType(), VisaFieldType.VISA_TRANSACTION_DESCRIPTION_FIELD.getLength(), "hex2");
		}
		
		if(input.getOriginalDataElement()!=null) {
			isoMessage.setValue(VisaFieldType.ORIGINAL_DATA_ELEMENT_FIELD.getPosition(), HexCodec.hexDecode(input.getOriginalDataElement()), 
					VisaFieldType.ORIGINAL_DATA_ELEMENT_FIELD.getFieldType(), VisaFieldType.ORIGINAL_DATA_ELEMENT_FIELD.getLength());
		}
		
		if(input.getReplacementAmount()!=null) {
			isoMessage.setValue(VisaFieldType.REPLACEMENT_AMOUNT_FIELD.getPosition(), input.getReplacementAmount(), 
					VisaFieldType.REPLACEMENT_AMOUNT_FIELD.getFieldType(), VisaFieldType.REPLACEMENT_AMOUNT_FIELD.getLength(),"Cp1047");
		}
		
		if(input.getVisaPanExtended_DE20()!=null) {
			isoMessage.setValue(VisaFieldType.PAN_EXTENDED_COUNTRY_CODE.getPosition(), input.getVisaPanExtended_DE20(), 
					VisaFieldType.PAN_EXTENDED_COUNTRY_CODE.getFieldType(), VisaFieldType.PAN_EXTENDED_COUNTRY_CODE.getLength());
		}
		
		if(input.getVisaCustomerRelatedDate_DE56()!=null) {
			isoMessage.setValue(VisaFieldType.CUSTOMER_RELATED_DATA.getPosition(), input.getVisaCustomerRelatedDate_DE56(), 
					VisaFieldType.CUSTOMER_RELATED_DATA.getFieldType(), VisaFieldType.CUSTOMER_RELATED_DATA.getLength(), "hex2");
		}
		
		if(input.getVisaPosGeoData_DE59()!=null) {
			isoMessage.setValue(VisaFieldType.POS_GEO_DATA.getPosition(), input.getVisaPosGeoData_DE59(), VisaFieldType.POS_GEO_DATA.getFieldType(),
					VisaFieldType.POS_GEO_DATA.getLength(),"hex2");
		}
		
		if(input.getFileUpdateCode()!=null) {
			isoMessage.setValue(VisaFieldType.VISA_FILE_UPDATE_CODE.getPosition(), input.getFileUpdateCode(), VisaFieldType.VISA_FILE_UPDATE_CODE.getFieldType(),
					VisaFieldType.VISA_FILE_UPDATE_CODE.getLength(),"Cp1047");
		}
		
		if(input.getVisaFileName_DE101()!=null) {
			isoMessage.setValue(VisaFieldType.VISA_FILE_NAME.getPosition(), input.getVisaFileName_DE101(), VisaFieldType.VISA_FILE_NAME.getFieldType(),
					VisaFieldType.VISA_FILE_NAME.getLength(),"hex2");
		}
		
		if (!StringUtils.isEmpty(input.getVisaDateAction())) {
			isoMessage.setValue(VisaFieldType.DATE_ACTION.getPosition(), HexCodec.hexDecode(input.getVisaDateAction()),
					VisaFieldType.DATE_ACTION.getFieldType(), VisaFieldType.DATE_ACTION.getLength());
		}
		
		if(input.getVisaFileRecord_DE127()!=null) {
				isoMessage.setValue(VisaFieldType.VISA_FILE_RECORDS_ACTION.getPosition(), input.getVisaFileRecord_DE127(), 
						VisaFieldType.VISA_FILE_RECORDS_ACTION.getFieldType(), VisaFieldType.VISA_FILE_RECORDS_ACTION.getLength(), "hex2");
		}
		
		return isoMessage;
	}

}
