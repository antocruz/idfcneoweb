package com.neo.aggregator.utility;

import com.neo.aggregator.iso8583.IsoMessage;

public interface CustomConverter {

	IsoMessage convert(Iso8583 input);
}
