package com.neo.aggregator.utility;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class TxnIdGenerator {

	public static String getReferenceNumber() {
		Random rand = new Random();
		int random = rand.nextInt(900) + 100;
		int maxVal = 15;
		String id = String.valueOf(System.nanoTime());
		maxVal = (id.length() < maxVal) ? id.length() : maxVal;
		id = id.substring(0, maxVal) + String.valueOf(random);

		return id;
	}
	public static String getReferenceNumber(int maxLength) {
		Random rand = new Random();
		int random;
		String id = String.valueOf(System.nanoTime());

		if (id.length() < maxLength) {
			int diff = Math.subtractExact(maxLength, id.length()) - 1;
			int m = (int) Math.pow(10, diff);
			random = m + rand.nextInt(9 * m);
			id = id + String.valueOf(random);
		} else {
			id = id.substring(0, maxLength);
		}
		return id;
	}
}
