package com.neo.aggregator.utility;

import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;

public class ShowableException extends Exception {
	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(IsoMessageToIso8583Converter.class);


    public void notifyUserWithToast() {       
        logger.info("ShowableException ::: "+ toString());
    }
}
