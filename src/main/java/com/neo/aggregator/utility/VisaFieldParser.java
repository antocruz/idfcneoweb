package com.neo.aggregator.utility;

public interface VisaFieldParser {
	public Iso8583.VisaAdditionalPosInformation parseAdditionalPosInfo(String data);
	public Iso8583.VisaCustomPaymentServiceField parsePaymentInfo(byte[] data);
	public Iso8583.VisaPrivateNetworkField parseNetworkInfo(byte[] data);
	public Iso8583.VisaPrivateMerchantField parsMerchantAcqInfo(byte[] data);
	public byte[] parseNetworkInfo(Iso8583.VisaPrivateNetworkField data);
	public byte[] parseCustomPaymentInfo(Iso8583.VisaCustomPaymentServiceField data);
	public byte[] parseAddResponseInfo(Iso8583.VisaAdditionalResponseField data);
	public byte[] parsMerchantAcqInfo(Iso8583.VisaPrivateMerchantField data);
	public String parseAdditionalPosInfo(Iso8583.VisaAdditionalPosInformation data);
}
