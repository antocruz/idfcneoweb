package com.neo.aggregator.utility;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;

import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.serviceimpl.NotificationAlertService;

@Component
public class NeoExceptionBuilder {

    @Autowired
    private ResourceBundleMessageSource messageSource;
    
    @Autowired
    private NotificationAlertService alertService;
    
    private static final Logger logger = LoggerFactory.getLogger(NeoExceptionBuilder.class);

    public NeoException build(String code, String[] args, Locale languageCode, String displayMessage) {
        String message = this.exceptionMessage(code, args, languageCode);
        String errorCode = this.exceptionMessage(code,args,Locale.ROOT);
        
        return new NeoException(message, message, languageCode.getLanguage(), errorCode, message);
    }
    
    public String exceptionMessage(String code, String[] args, Locale languageCode) {
        return messageSource.getMessage(code, args, languageCode);
    }

    public void setMessageSource(ResourceBundleMessageSource messageSource) {
        this.messageSource = messageSource;
    }
    
    //New Method
    public NeoException buildAndNotify(String code, String detailMessage,String[] args, Locale languageCode,String request,String response,String product) {
        String message = this.exceptionMessage(code, args, languageCode);
        String errorCode = this.exceptionMessage(code,args,Locale.ROOT);
        
        if(detailMessage==null)
        	detailMessage = message;
        
        NeoException exception = new NeoException(message, detailMessage, languageCode.getLanguage(), errorCode, message);
        
        alertService.notifyExceptionAlert(exception, TenantContextHolder.getNeoTenant(), request, response, product);
        
        return exception;
    }
}
