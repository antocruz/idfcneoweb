package com.neo.aggregator.utility;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neo.aggregator.iso8583.codecs.EbcdicEncoder;
import com.neo.aggregator.iso8583.util.HexCodec;
import com.neo.aggregator.utility.Iso8583.VisaAdditionalPosInformation;
import com.neo.aggregator.utility.Iso8583.VisaAdditionalResponseField;
import com.neo.aggregator.utility.Iso8583.VisaPrivateMerchantField;

@Service
public class VisaFieldParserImpl implements VisaFieldParser{
	
	@Autowired
	private EbcdicEncoder ebcidicEnc;

	public Iso8583.VisaAdditionalPosInformation parseAdditionalPosInfo(String data){
		Iso8583.VisaAdditionalPosInformation posInfo = new Iso8583().new VisaAdditionalPosInformation();
		for(int index=0; index<data.length(); index++){
			switch(index+1){
				case 1:
					posInfo.setTerminalType(Integer.parseInt(data.substring(index, index+1)));
					break;
				case 2:
					posInfo.setTerminalEntryCapability(Integer.parseInt(data.substring(index, index+1)));
					break;
				case 3:
					posInfo.setChipConditionCode(Integer.parseInt(data.substring(index, index+1)));
					break;
				case 4:
					posInfo.setSpecialConditionIndicator(Integer.parseInt(data.substring(index, index+1)));
					break;
				case 5:
					index++;
					break;
				case 7:
					posInfo.setChipTransactionIndicator(Integer.parseInt(data.substring(index, index+1)));
					break;
				case 8:
					posInfo.setChipReliabilityIndicator(Integer.parseInt(data.substring(index, index+1)));
					break;
				case 9:
					posInfo.setEciIndicator(Integer.parseInt(data.substring(index, index+2)));
					index++;
					break;
				case 11:
					posInfo.setCardIdIndicator(Integer.parseInt(data.substring(index, index+1)));
					break;
				case 12:
					posInfo.setAdditionalAuthIndicator(Integer.parseInt(data.substring(index, index+1)));
					break;
			}
		}
		
		return posInfo;
	}
	
	public Iso8583.VisaCustomPaymentServiceField parsePaymentInfo(byte[] data){
		Iso8583.VisaCustomPaymentServiceField posInfo = new Iso8583().new VisaCustomPaymentServiceField();
		byte[] bitmap = new byte[8];
		BitSet bs = new BitSet();
		System.arraycopy(data, 0, bitmap, 0, 8);
		String hexString = ISOUtil.byte2hex(bitmap);
		bs = ISOUtil.hex2BitSet(bs, hexString.getBytes(), 0);
		int offset=0;
		offset=offset+8;
		for(int index=0; index<64; index++){
			if(bs.get(index+1)){
				try{
					switch(index+1){
						case 1:
							posInfo.setAuthCharactInd(new String(data, offset, 1, "Cp1047"));
							offset+=1;
							break;
						case 2:
							posInfo.setTranId(HexCodec.hexEncode(data, offset, 8));
							offset+=8;
							break;
						case 3:
							posInfo.setValidationCode(new String(data, offset, 4, "Cp1047"));
							offset+=4;
							break;
						case 4:
							posInfo.setMarketSpecDataId(new String(data, offset, 1, "Cp1047"));
							offset+=1;
							break;
						case 5:
							posInfo.setDuration(HexCodec.hexEncode(data, offset, 1));
							offset+=1;
							break;
						case 6:
							posInfo.setPrestigiousPropInd(new String(data, offset, 1, "Cp1047"));
							offset+=1;
							break;	
						case 7:
							posInfo.setPurchaseIdentifier(new String(data, offset, 26, "Cp1047"));
							offset+=26;
							break;
						case 8:
							//posInfo.setAutoRentalChkoutChkinDate(new String(data, offset, 3));
							posInfo.setAutoRentalChkoutChkinDate(HexCodec.hexEncode(data, offset, 3));
							offset+=3;
							break;
						case 9:
							posInfo.setNoShowIndicator(new String(data, offset, 1, "Cp1047"));
							offset+=1;
							break;
						case 10:
							//posInfo.setExtraCharges(new String(data, offset, 3));
							posInfo.setExtraCharges(HexCodec.hexEncode(data, offset, 3));
							offset+=3;
							break;
						case 11:
							//posInfo.setClearingSeqNo(new String(data, offset, 1));
							posInfo.setClearingSeqNo(HexCodec.hexEncode(data, offset, 1));
							offset+=1;
							break;
						case 12:
							//posInfo.setClearingSeqCnt(new String(data, offset, 1));
							posInfo.setClearingSeqCnt(HexCodec.hexEncode(data, offset, 1));
							offset+=1;
							break;
						case 13:
							posInfo.setResTcktId(new String(data, offset, 1, "Cp1047"));
							offset+=1;
							break;
						case 14:
							posInfo.setAuthorizedAmt(HexCodec.hexEncode(data, offset, 6));
							offset+=6;
							break;
						case 15:
							posInfo.setReqPaymentService(new String(data, offset, 1, "Cp1047"));
							offset+=1;
							break;
						case 16:
							posInfo.setChargeBackRightsInd(new String(data, offset, 2, "Cp1047"));
							offset+=2;
							break;
						case 17:
							posInfo.setGatewayTxnId(new String(data, offset, 15, "Cp1047"));
							offset+=15;
							break;
						case 18:
							posInfo.setExclTxnIdReasonCde(new String(data, offset, 1, "Cp1047"));
							offset+=1;
							break;
						case 19:
							posInfo.setEcomGoodsInd(new String(data, offset, 2, "Cp1047"));
							offset+=2;
							break;
						case 20:
							posInfo.setMvv(HexCodec.hexEncode(data, offset, 5));
							offset+=5;
							break;
						case 21:
							posInfo.setRiskScore(new String(data, offset, 4, "Cp1047"));
							offset+=4;
							break;
						case 22:
							posInfo.setRiskCondCode(new String(data, offset, 6, "Cp1047"));
							offset+=6;
							break;
						case 23:
							posInfo.setProductId(new String(data, offset, 2, "Cp1047"));
							offset+=2;
							break;
						case 24:
							posInfo.setProgramId(new String(data, offset, 6, "Cp1047"));
							offset+=6;
							break;
						case 25:
							posInfo.setSpendQualifiedInd(new String(data, offset, 1, "Cp1047"));
							offset+=1;
							break;
						case 26:
							posInfo.setAccountStatus(new String(data, offset, 1, "Cp1047"));
							offset+=1;
							break;
					}
				}catch(UnsupportedEncodingException e){
					e.printStackTrace();
				}
			}
		}
		
		return posInfo;
	}
	
	public Iso8583.VisaPrivateNetworkField parseNetworkInfo(byte[] data){
		Iso8583.VisaPrivateNetworkField posInfo = new Iso8583().new VisaPrivateNetworkField();
		byte[] bitmap = new byte[3];
		BitSet bs = new BitSet(24);
		System.arraycopy(data, 0, bitmap, 0, 3);
		String hexString = ISOUtil.byte2hex(bitmap);
		bs = ISOUtil.hex2BitSet(bs, hexString.getBytes(), 0);
		int offset=0;
		offset=offset+3;
		for(int index=0; index<24; index++){
			if(bs.get(index+1)){
				try{
					switch(index+1){
						case 1:
							posInfo.setNetworkId(HexCodec.hexEncode(data, offset, 2));
							offset+=2;
							break;
						case 2:
							posInfo.setPreAuthLimit(HexCodec.hexEncode(data, offset, 2));
							offset+=2;
							break;
						case 3:
							posInfo.setMessageReasonCode(HexCodec.hexEncode(data, offset, 2));
							offset+=2;
							break;
						case 4:
							posInfo.setSwitchReasonCode(HexCodec.hexEncode(data, offset, 2));
							offset+=2;
							break;
						case 5:
							offset+=3;
							break;
						case 6:
							posInfo.setBase2flags(new String(data, offset, 7, "Cp1047"));
							offset+=7;
							break;
						case 8:
							posInfo.setVisaAcqBusinessId(HexCodec.hexEncode(data, offset, 4));
							offset+=4;
							break;
						case 9:
							posInfo.setFraudData(new String(data, offset, 14, "Cp1047"));
							offset+=14;
							break;
						case 11:
							posInfo.setReimbAttribute(new String(data, offset, 1, "Cp1047"));
							offset+=1;
							break;
						case 12:
							posInfo.setSharingGroupCode(new String(data, offset, 30, "Cp1047"));
							offset+=30;
							break;
						case 13:
							posInfo.setDecimalIndicator(HexCodec.hexEncode(data, offset, 3));
							offset+=3;
							break;
						case 14:
							posInfo.setIssuerCrncyConversion(new String(data, offset, 36, "Cp1047"));
							offset+=36;
							break;
						case 15:
							posInfo.setReserved(new String(data, offset, 9, "Cp1047"));
							offset+=9;
							break;
						case 19:
							posInfo.setFeeProgInd(new String(data, offset, 3, "Cp1047"));
							offset+=3;
							break;
						case 21:
							posInfo.setChargeIndicator(new String(data, offset, 1, "Cp1047"));
							offset+=1;
							break;		
					}
				}catch(UnsupportedEncodingException e){
					e.printStackTrace();
				}
			}
		}
		
		return posInfo;
	}
	
	public Iso8583.VisaPrivateMerchantField parsMerchantAcqInfo(byte[] data){
		Iso8583.VisaPrivateMerchantField posInfo = new Iso8583().new VisaPrivateMerchantField();
		byte[] bitmap = new byte[8];
		BitSet bs = new BitSet(64);
		System.arraycopy(data, 0, bitmap, 0, 8);
		String hexString = ISOUtil.byte2hex(bitmap);
		bs = ISOUtil.hex2BitSet(bs, hexString.getBytes(), 0);

		int offset=0;
		offset=offset+8;
		for(int index=0; index<64; index++){
			if(bs.get(index+1)){
				try{
					switch(index+1){
						case 5:
							posInfo.setMerchantId(new String(data, offset, 8, "Cp1047"));
							offset+=8;
							break;
						case 6:
							posInfo.setCardSerialNum(HexCodec.hexEncode(data, offset, 17));
							offset+=17;
							break;						
						case 7:
							posInfo.setMerchantSerialNum(HexCodec.hexEncode(data, offset, 17));
							offset+=17;
							break;
						case 8:
							posInfo.setTxnId(HexCodec.hexEncode(data, offset, 20));
							offset+=20;
							break;
						case 9:
							posInfo.setCavv(HexCodec.hexEncode(data, offset, 20));
							offset+=20;
							break;
						case 10:
							String cvv2 = new String(data, offset, 6, "Cp1047");
							posInfo.setCvv2(cvv2.substring(cvv2.length()-3));
							offset+=6;
							break;
						case 12:
							posInfo.setServiceInd(HexCodec.hexEncode(data, offset, 3));
							offset+=3;
							break;
						case 13:
							posInfo.setPosEnv(new String(data, offset, 1, "Cp1047"));
							offset+=1;
							break;
						case 15:
							posInfo.setMasterUCAFInd(new String(data, offset, 1, "Cp1047"));
							offset+=1;
							break;
						case 16:
							posInfo.setMasterUCAFField(new String(data, offset, 33, "Cp1047"));
							offset+=33;
							break;	
						case 18:
							posInfo.setAgentUniqueAcctResult(new String(data, offset, 12, "Cp1047"));
							offset+=12;
							break;
						case 19:
							posInfo.setDccInd(new String(data, offset, 1, "Cp1047"));
							offset+=1;
							break;		
						case 20:
							posInfo.setThree_dInd(new String(data, offset, 1, "Cp1047"));
							offset+=1;
							break;
					}
				}catch(UnsupportedEncodingException e){
					e.printStackTrace();
				}
			}
		}
		
		return posInfo;
	}
	
	public byte[] parseNetworkInfo(Iso8583.VisaPrivateNetworkField posInfo){
		BitSet bs = new BitSet(24);
		List<Byte> byteDataList = new ArrayList<Byte>();
		int length = 0;
		if(posInfo.getNetworkId()!=null){
			bs.set(1, true);
			length+=2;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getNetworkId());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getPreAuthLimit()!=null){
			bs.set(2, true);
			length+=2;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getNetworkId());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getMessageReasonCode()!=null){
			bs.set(3, true);
			length+=2;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getMessageReasonCode());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		byte[] bitmap = ISOUtil.bitSet2byte(bs, 3);
		byte[] finaldata = new byte[3+length];
		System.arraycopy(bitmap, 0, finaldata, 0, 3);
		byte[] dataArray = new byte[byteDataList.size()];
		for(int i=0; i<dataArray.length; i++){
			dataArray[i]=byteDataList.get(i);
		}
		System.arraycopy(dataArray, 0, finaldata, 3, dataArray.length);
		return finaldata;
	}
	
	public byte[] parseCustomPaymentInfo(Iso8583.VisaCustomPaymentServiceField posInfo){
		BitSet bs = new BitSet();
		List<Byte> byteDataList = new ArrayList<Byte>();
		int length = 0;
		if(posInfo.getAuthCharactInd()!=null) {
			bs.set(1,true);
			length+=1;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getAuthCharactInd());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getTranId()!=null) {
			bs.set(2, true);
			length+=8;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getTranId());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getValidationCode()!=null) {
			bs.set(3,true);
			length+=4;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getValidationCode());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getMarketSpecDataId()!=null) {
			bs.set(4,true);
			length+=1;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getMarketSpecDataId());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getDuration()!=null) {
			bs.set(5,true);
			length+=1;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getDuration());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getPrestigiousPropInd()!=null) {
			bs.set(6,true);
			length+=1;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getPrestigiousPropInd());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getPurchaseIdentifier()!=null) {
			bs.set(7,true);
			length+=26;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getPurchaseIdentifier());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getAutoRentalChkoutChkinDate()!=null) {
			bs.set(8,true);
			length+=3;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getAutoRentalChkoutChkinDate());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getNoShowIndicator()!=null) {
			bs.set(9,true);
			length+=1;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getNoShowIndicator());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getExtraCharges()!=null) {
			bs.set(10,true);
			length+=3;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getExtraCharges());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getClearingSeqNo()!=null) {
			bs.set(11,true);
			length+=1;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getClearingSeqNo());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getClearingSeqCnt()!=null) {
			bs.set(12,true);
			length+=1;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getClearingSeqCnt());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getResTcktId()!=null) {
			bs.set(13,true);
			length+=1;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getResTcktId());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getAuthorizedAmt()!=null) {
			bs.set(14,true);
			length+=6;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getAuthorizedAmt());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getReqPaymentService()!=null) {
			bs.set(15,true);
			length+=1;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getReqPaymentService());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getChargeBackRightsInd()!=null) {
			bs.set(16,true);
			length+=2;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getChargeBackRightsInd());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getGatewayTxnId()!=null) {
			bs.set(17,true);
			length+=15;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getGatewayTxnId());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getExclTxnIdReasonCde()!=null) {
			bs.set(18,true);
			length+=15;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getExclTxnIdReasonCde());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getEcomGoodsInd()!=null) {
			bs.set(19,true);
			length+=2;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getEcomGoodsInd());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getMvv()!=null) {
			bs.set(20,true);
			length+=5;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getMvv());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getRiskScore()!=null) {
			bs.set(21,true);
			length+=4;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getRiskScore());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getRiskCondCode()!=null) {
			bs.set(22,true);
			length+=6;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getRiskCondCode());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getProductId()!=null) {
			bs.set(23,true);
			length+=2;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getProductId());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getProgramId()!=null) {
			bs.set(24,true);
			length+=6;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getProgramId());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getSpendQualifiedInd()!=null) {
			bs.set(25,true);
			length+=1;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getSpendQualifiedInd());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getAccountStatus()!=null) {
			bs.set(26,true);
			length+=1;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getAccountStatus());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		byte[] bitmap = ISOUtil.bitSet2byte(bs, 8);
		byte[] finaldata = new byte[8+length];
		System.arraycopy(bitmap, 0, finaldata, 0, 8);
		byte[] dataArray = new byte[byteDataList.size()];
		for(int i=0; i<dataArray.length; i++){
			dataArray[i]=byteDataList.get(i);
		}
		System.arraycopy(dataArray, 0, finaldata, 8, dataArray.length);
		return finaldata;
	}

	@Override
	public byte[] parseAddResponseInfo(VisaAdditionalResponseField data) {
		StringBuffer sb = new StringBuffer();
		if(data.getReasonCode()==null){
			sb.append(" ");
		}else{
			sb.append(data.getRespReasonCode());
		}
		
		if(data.getAvr()==null){
			sb.append(" ");
		}else{
			sb.append(data.getAvr());
		}
		
		sb.append("  "); // Byte 3 and 4 Reserved for future use
		
		if(data.getCvv_icvv_result()==null){
			sb.append(" ");
		}else{
			sb.append(data.getCvv_icvv_result());
		}
		
		sb.append("   "); // PACM(2) && PACM DIVERSION
		
		if(data.getCardAuthResult()==null){
			sb.append(" ");
		}else{
			sb.append(data.getCardAuthResult());
		}
		
		sb.append(" "); // Reserved
		
		if(data.getCvv2_result()==null){
			sb.append(" ");
		}else{
			sb.append(data.getCvv2_result());
		}
		
		sb.append("   ");
		
		if(data.getCavvResultCode()==null){
			sb.append(" ");
		}else{
			sb.append(data.getCavvResultCode());
		}
		
		if(data.getRespReasonCode()==null){
			sb.append(" ");
		}else{
			sb.append(data.getRespReasonCode());
		}
		
		String respData = sb.toString();
		respData = ISOUtil.unPadRight(respData, ' ');
		try {
			return respData.getBytes("Cp1047");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public byte[] parsMerchantAcqInfo(VisaPrivateMerchantField posInfo) {
		BitSet bs = new BitSet(64);
		List<Byte> byteDataList = new ArrayList<Byte>();
		int length = 0;
		if(posInfo.getMerchantId()!=null) {
			bs.set(5, true);
			length+=8;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getMerchantId());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getCardSerialNum()!=null) {
			bs.set(6, true);
			length+=17;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getCardSerialNum());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getMerchantSerialNum()!=null) {
			bs.set(7, true);
			length+=17;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getMerchantSerialNum());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getTxnId()!=null) {
			bs.set(8, true);
			length+=20;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getTxnId());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getCavv()!=null) {
			bs.set(9, true);
			length+=20;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getCavv());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getCvv2()!=null) {
			bs.set(10, true);
			length+=6;
			//String newVal = StringUtils.leftPad(posInfo.getCvv2(), 6, "0");
			String newVal = "11 "+posInfo.getCvv2();
			
			byte[] dataArray = ebcidicEnc.encodeBinaryField(newVal);
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getServiceInd()!=null) {
			bs.set(12, true);
			length+=3;
			byte[] dataArray = HexCodec.hexDecode(posInfo.getServiceInd());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getPosEnv()!=null) {
			bs.set(13, true);
			length+=1;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getPosEnv());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getMasterUCAFInd()!=null) {
			bs.set(15, true);
			length+=1;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getMasterUCAFInd());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getMasterUCAFField()!=null) {
			bs.set(16, true);
			length+=33;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getMasterUCAFField());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getAgentUniqueAcctResult()!=null) {
			bs.set(18, true);
			length+=12;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getAgentUniqueAcctResult());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getDccInd()!=null) {
			bs.set(19, true);
			length+=1;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getDccInd());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		if(posInfo.getThree_dInd()!=null) {
			bs.set(20, true);
			length+=1;
			byte[] dataArray = ebcidicEnc.encodeBinaryField(posInfo.getThree_dInd());
			for(byte byteData: dataArray)
				byteDataList.add(byteData);
		}
		
		byte[] bitmap = ISOUtil.bitSet2byte(bs, 8);
		byte[] finaldata = new byte[8+length];
		System.arraycopy(bitmap, 0, finaldata, 0, 8);
		byte[] dataArray = new byte[byteDataList.size()];
		for(int i=0; i<dataArray.length; i++){
			dataArray[i]=byteDataList.get(i);
		}
		System.arraycopy(dataArray, 0, finaldata, 8, dataArray.length);
		return finaldata;
	}

	@Override
	public String parseAdditionalPosInfo(VisaAdditionalPosInformation data) {
		StringBuffer sb = new StringBuffer();
		
		sb.append(data.getTerminalType());
		sb.append(data.getTerminalEntryCapability());
		sb.append(data.getChipConditionCode());
		sb.append(data.getSpecialConditionIndicator());
		sb.append("00");
		sb.append(data.getChipTransactionIndicator());
		sb.append(data.getChipReliabilityIndicator());
		
		if(String.valueOf(data.getEciIndicator()).length()==1)
			sb.append("0"+data.getEciIndicator());
		else
			sb.append(data.getEciIndicator());
		
		sb.append(data.getCardIdIndicator());
		sb.append(data.getAdditionalAuthIndicator());
		
		String respData = sb.toString();
		respData = ISOUtil.unPadRight(respData, ' ');
		
		return respData;
	}
	
}
