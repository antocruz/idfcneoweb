package com.neo.aggregator.utility;

import java.io.IOException;
import java.io.StringReader;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.bank.equitas.dto.KycRequestDto;
import com.neo.aggregator.bank.equitas.request.CreateDigiCustLeadwitheKYCReqDto;
import com.neo.aggregator.bank.equitas.request.GenerateDigiOTPReqDto;
import com.neo.aggregator.bank.equitas.response.CreateDigiCustLeadwitheKYCResDto;
import com.neo.aggregator.bank.equitas.response.GenerateDigiOTPResDto;
import com.neo.aggregator.dto.AadhaarOtpRequest;
import com.neo.aggregator.dto.KYCBioRequestDto;

public class Utils {
	
	private Utils() {}
	
	private static final String MASKED = "***Masked***";
	
	public static String uniqueIdentifier() {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddhhmmssSSS");
		Date currentDate = new Date();
		SecureRandom random = new SecureRandom();
		int num = random.nextInt(100000);
		String formatted = String.format("%05d", num);
		return "M2P" + formatter.format(currentDate) + formatted;

	}
	
	public static String currentTimestamp() {
		Date currentDate = new Date();
		ZoneId zone = ZoneId.systemDefault();
		Instant instant = Instant.now();
		ZoneOffset zo= zone.getRules().getOffset(instant);
		return (OffsetDateTime.of(LocalDateTime.ofInstant(currentDate.toInstant(), ZoneId.systemDefault()), zo)).toString();
	}
	
	public static String formatTimestamp() {
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		return formatter.format(currentDate);
	}
	
	public static String currentDateTimeMMDDHHmmSS() {
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("MMddHHmmss");
		return formatter.format(currentDate);
	}
	
	public static String generateStan() {
		SecureRandom random = new SecureRandom();
		int num = random.nextInt(1000000);
		return String.format("%06d", num);
	}
	
	public static String currentTimeHHmmss() {
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
		return formatter.format(currentDate);
	}
	
	public static String currentDateMMDD() {
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("MMdd");
		return formatter.format(currentDate);
	}

	public static String convertModelToString(Object obj) {
    	ObjectMapper mapper = new ObjectMapper();
    	String str = null;
    	try {
			str = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} finally {
			mapper = null;
		}
    	return str;
    }
	
	public static String convertModelToStringWithMaskedAadhar(Object obj) {
		ObjectMapper mapper = new ObjectMapper();
		String str = null;
		String idNumber = null;
		String aadhar = null;
		String sKey = null;
		String ci = null;
		String mc = null;
		String dataType = null;
		String dataValue = null;
		String hmac = null;
		String mi = null;
		String rdsId = null;
		String rdsVer = null;
		String dpId = null;
		String dc = null;
		String eKYCXMLStringReqPayload = null;
		String eKYCXMLStringRepPayload = null;

		try {
			if (obj instanceof KYCBioRequestDto) {
				KYCBioRequestDto req = (KYCBioRequestDto) obj;
				if (req.getIdNumber() != null) {
					idNumber = req.getIdNumber();
					req.setIdNumber("********" + req.getIdNumber().substring(8));
				}
				if (req.getSkey() != null) {
					sKey = req.getSkey();
					req.setSkey(MASKED);
				}
				if (req.getCi() != null) {
					ci = req.getCi();
					req.setCi(MASKED);
				}
				if (req.getMc() != null) {
					mc = req.getMc();
					req.setMc(MASKED);
				}
				if (req.getDataType() != null) {
					dataType = req.getDataType();
					req.setDataType(MASKED);
				}
				if (req.getDataValue() != null) {
					dataValue = req.getDataValue();
					req.setDataValue(MASKED);
				}
				if (req.getHmac() != null) {
					hmac = req.getHmac();
					req.setHmac(MASKED);
				}
				if (req.getMi() != null) {
					mi = req.getMi();
					req.setMi(MASKED);
				}
				if (req.getRdsId() != null) {
					rdsId = req.getRdsId();
					req.setRdsId(MASKED);
				}
				if (req.getRdsVer() != null) {
					rdsVer = req.getRdsVer();
					req.setRdsVer(MASKED);
				}
				if (req.getDpId() != null) {
					dpId = req.getDpId();
					req.setDpId(MASKED);
				}
				if (req.getDc() != null) {
					dc = req.getDc();
					req.setDc(MASKED);
				}
				
				str = mapper.writeValueAsString(req);
				req.setIdNumber(idNumber);
				req.setSkey(sKey);
				req.setCi(ci);
				req.setMc(mc);
				req.setDataType(dataType);
				req.setDataValue(dataValue);
				req.setHmac(hmac);
				req.setMi(mi);
				req.setRdsId(rdsId);
				req.setRdsVer(rdsVer);
				req.setDpId(dpId);
				req.setDc(dc);
				
			} else if (obj instanceof AadhaarOtpRequest) {
				AadhaarOtpRequest req = (AadhaarOtpRequest) obj;
				if (req.getIdNumber() != null) {
					idNumber = req.getIdNumber();
					req.setIdNumber("********" + req.getIdNumber().substring(8));
				}
				str = mapper.writeValueAsString(req);
				req.setIdNumber(idNumber);
			} else if (obj instanceof KycRequestDto) {
				KycRequestDto req = (KycRequestDto) obj;
				if (req.getIdNumber() != null) {
					idNumber = req.getIdNumber();
					req.setIdNumber("********" + req.getIdNumber().substring(8));
				}
				if (req.getAadhar() != null) {
					aadhar = req.getAadhar();
					req.setAadhar("********" + req.getAadhar().substring(8));
				}
				if (req.getSkey() != null) {
					sKey = req.getSkey();
					req.setSkey(MASKED);
				}
				if (req.getCi() != null) {
					ci = req.getCi();
					req.setCi(MASKED);
				}
				if (req.getMc() != null) {
					mc = req.getMc();
					req.setMc(MASKED);
				}
				if (req.getDataType() != null) {
					dataType = req.getDataType();
					req.setDataType(MASKED);
				}
				if (req.getDataValue() != null) {
					dataValue = req.getDataValue();
					req.setDataValue(MASKED);
				}
				if (req.getHmac() != null) {
					hmac = req.getHmac();
					req.setHmac(MASKED);
				}
				if (req.getMi() != null) {
					mi = req.getMi();
					req.setMi(MASKED);
				}
				if (req.getRdsId() != null) {
					rdsId = req.getRdsId();
					req.setRdsId(MASKED);
				}
				if (req.getRdsVer() != null) {
					rdsVer = req.getRdsVer();
					req.setRdsVer(MASKED);
				}
				if (req.getDpId() != null) {
					dpId = req.getDpId();
					req.setDpId(MASKED);
				}
				if (req.getDc() != null) {
					dc = req.getDc();
					req.setDc(MASKED);
				}
				str = mapper.writeValueAsString(req);
				req.setIdNumber(idNumber);
				req.setAadhar(aadhar);
				req.setSkey(sKey);
				req.setCi(ci);
				req.setMc(mc);
				req.setDataType(dataType);
				req.setDataValue(dataValue);
				req.setHmac(hmac);
				req.setMi(mi);
				req.setRdsId(rdsId);
				req.setRdsVer(rdsVer);
				req.setDpId(dpId);
				req.setDc(dc);
				
			} else if (obj instanceof CreateDigiCustLeadwitheKYCReqDto) {
				CreateDigiCustLeadwitheKYCReqDto req = (CreateDigiCustLeadwitheKYCReqDto) obj;
				if (req.getDigiCustLeadwitheKYCMessageBody() != null
						&& req.getDigiCustLeadwitheKYCMessageBody().getLeadDetails() != null
						&& req.getDigiCustLeadwitheKYCMessageBody().getLeadDetails().getIndividualEntry() != null
						&& req.getDigiCustLeadwitheKYCMessageBody().getLeadDetails().getIndividualEntry()
								.getAadhar() != null) {
					aadhar = req.getDigiCustLeadwitheKYCMessageBody().getLeadDetails().getIndividualEntry().getAadhar();
					req.getDigiCustLeadwitheKYCMessageBody().getLeadDetails().getIndividualEntry()
							.setAadhar("********" + req.getDigiCustLeadwitheKYCMessageBody().getLeadDetails()
									.getIndividualEntry().getAadhar().substring(8));
				}
				if (req.getDigiCustLeadwitheKYCMessageBody() != null
						&& req.getDigiCustLeadwitheKYCMessageBody().geteKYCDetails() != null
						&& req.getDigiCustLeadwitheKYCMessageBody().geteKYCDetails()
								.geteKYCXMLStringReqPayload() != null) {
					
					eKYCXMLStringReqPayload = req.getDigiCustLeadwitheKYCMessageBody().geteKYCDetails()
							.geteKYCXMLStringReqPayload();
					
					req.getDigiCustLeadwitheKYCMessageBody().geteKYCDetails().seteKYCXMLStringReqPayload(MASKED);
					

					/*
					 * NodeList uid = maskUID(
					 * req.getDigiCustLeadwitheKYCMessageBody().geteKYCDetails().
					 * geteKYCXMLStringReqPayload());
					 * 
					 * if (uid != null) { String masked = uid.item(0).getTextContent(); String
					 * original = req.getDigiCustLeadwitheKYCMessageBody().geteKYCDetails()
					 * .geteKYCXMLStringReqPayload(); eKYCXMLStringReqPayload =
					 * req.getDigiCustLeadwitheKYCMessageBody().geteKYCDetails()
					 * .geteKYCXMLStringReqPayload(); if (masked!=null && !masked.isEmpty()) {
					 * original = original.replace(masked, "********" + masked.substring(8));
					 * req.getDigiCustLeadwitheKYCMessageBody().geteKYCDetails().
					 * seteKYCXMLStringReqPayload(original); } }
					 */
				}
				str = mapper.writeValueAsString(req);
				
				if (req.getDigiCustLeadwitheKYCMessageBody() != null
						&& req.getDigiCustLeadwitheKYCMessageBody().getLeadDetails() != null
						&& req.getDigiCustLeadwitheKYCMessageBody().getLeadDetails().getIndividualEntry() != null) {
					req.getDigiCustLeadwitheKYCMessageBody().getLeadDetails().getIndividualEntry().setAadhar(aadhar);
				}
				if (req.getDigiCustLeadwitheKYCMessageBody() != null
						&& req.getDigiCustLeadwitheKYCMessageBody().geteKYCDetails() != null) {
					req.getDigiCustLeadwitheKYCMessageBody().geteKYCDetails()
					.seteKYCXMLStringReqPayload(eKYCXMLStringReqPayload);
				}
			} else if (obj instanceof CreateDigiCustLeadwitheKYCResDto) {
				CreateDigiCustLeadwitheKYCResDto req = (CreateDigiCustLeadwitheKYCResDto) obj;
				if (req.getMsgBdy() != null && req.getMsgBdy().getKycDetails() != null
						&& req.getMsgBdy().getKycDetails().geteKYCXMLStringRepPayload() != null) {

					NodeList uid = maskUID(req.getMsgBdy().getKycDetails().geteKYCXMLStringRepPayload());

					if (uid != null) {
						String masked = uid.item(0).getTextContent();
						String original = req.getMsgBdy().getKycDetails().geteKYCXMLStringRepPayload();
						eKYCXMLStringRepPayload = req.getMsgBdy().getKycDetails().geteKYCXMLStringRepPayload();
						if (masked!=null && !masked.isEmpty()) {
							original = original.replace(masked, "********" + masked.substring(8));
							req.getMsgBdy().getKycDetails().seteKYCXMLStringRepPayload(original);
						}
					}
				}
				str = mapper.writeValueAsString(req);
				if (req.getMsgBdy() != null && req.getMsgBdy().getKycDetails() != null){
					req.getMsgBdy().getKycDetails().seteKYCXMLStringRepPayload(eKYCXMLStringRepPayload);
				}
			} else if (obj instanceof GenerateDigiOTPResDto) {
				GenerateDigiOTPResDto req = (GenerateDigiOTPResDto) obj;
				if (req.getMsgBdy() != null && req.getMsgBdy().geteKYCXMLStringRepPayload() != null) {
					NodeList uid = maskUID(req.getMsgBdy().geteKYCXMLStringRepPayload());
					if (uid != null) {
						String masked = uid.item(0).getTextContent();
						String original = req.getMsgBdy().geteKYCXMLStringRepPayload();
						eKYCXMLStringRepPayload = req.getMsgBdy().geteKYCXMLStringRepPayload();
						if (masked != null && !masked.isEmpty()) {
							original = original.replace(masked, "********" + masked.substring(8));
							req.getMsgBdy().seteKYCXMLStringRepPayload(original);
						}
					}
				}
				str = mapper.writeValueAsString(req);
				if (req.getMsgBdy() != null) {
					req.getMsgBdy().seteKYCXMLStringRepPayload(eKYCXMLStringRepPayload);
				}
			} else if (obj instanceof GenerateDigiOTPReqDto) {
				GenerateDigiOTPReqDto req = (GenerateDigiOTPReqDto) obj;
				if (req.getGenerateDigiOTPMessageBody() != null && req.getGenerateDigiOTPMessageBody().geteKYCXMLStringReqPayload() != null) {
					NodeList uid = maskUID(req.getGenerateDigiOTPMessageBody().geteKYCXMLStringReqPayload());
					if (uid != null) {
						String masked = uid.item(0).getTextContent();
						String original = req.getGenerateDigiOTPMessageBody().geteKYCXMLStringReqPayload();
						eKYCXMLStringReqPayload = req.getGenerateDigiOTPMessageBody().geteKYCXMLStringReqPayload();
						if (masked != null && !masked.isEmpty()) {
							original = original.replace(masked, "********" + masked.substring(8));
							req.getGenerateDigiOTPMessageBody().seteKYCXMLStringReqPayload(original);
						}
					}
				}
				str = mapper.writeValueAsString(req);
				if (req.getGenerateDigiOTPMessageBody() != null) {
					req.getGenerateDigiOTPMessageBody().seteKYCXMLStringReqPayload(eKYCXMLStringReqPayload);
				}
			}
				
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} finally {
			mapper = null;
		}

		return str;
	}
	
	private static NodeList maskUID(String data) {
		NodeList uid = null;
		try {
			DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
			df.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, ""); 
			df.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
			df.setFeature("http://xml.org/sax/features/external-general-entities", false);
			df.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			df.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
			df.setValidating(true);
			DocumentBuilder builder = df.newDocumentBuilder();

			InputSource src = new InputSource();
			src.setCharacterStream(new StringReader(data));

			Document doc = builder.parse(src);

			uid = doc.getElementsByTagName("UID");
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		return uid;
	}

	public static String nullCheckToEmpty(String source) {

		return source != null ? source : "";
	}
}
