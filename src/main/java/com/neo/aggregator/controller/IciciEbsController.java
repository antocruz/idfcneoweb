package com.neo.aggregator.controller;

import com.neo.aggregator.bank.icici.request.CIBRegistrationRequest;
import com.neo.aggregator.bank.icici.response.CIBRegistrationResponse;
import com.neo.aggregator.dto.ImpsTransactionRequestDto;
import com.neo.aggregator.dto.ImpsTransactionResponseDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.NeoResponse;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.service.IciciEbsService;
import com.neo.aggregator.utility.TenantContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/icici")
public class IciciEbsController {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(IciciEbsController.class);

	@Autowired
	private IciciEbsService service;

	@PostMapping(value = "/cibRegistration", consumes = "application/json")
	public ResponseEntity<NeoResponse> cibRegistration(@RequestBody CIBRegistrationRequest request) {

		try {
			logger.info("Tenant :: "+ TenantContextHolder.getNeoTenant());

			logger.info("Request Received CIB Registration:: " + request.toString());

			CIBRegistrationResponse response = service.cibRegistration(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/fundTransfer", consumes = "application/json")
	public ResponseEntity<NeoResponse> internalFundTransfer(@RequestBody ImpsTransactionRequestDto request) {

		try {
			logger.info("Tenant :: "+ TenantContextHolder.getNeoTenant());

			logger.info("ICICI :: Request Received for Internal Fund Movement :: " + request.toString());

			ImpsTransactionResponseDto response = service.internalFundTransfer(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/upiCollect/refund", consumes = "text/plain")
	public ResponseEntity<NeoResponse> internalFundTransfer(@RequestBody String request) {

		try {
			logger.info("Tenant :: "+ TenantContextHolder.getNeoTenant());

			logger.info("ICICI :: Request Received for UPI collect Refund :: " + request);

			Boolean response = service.upiCollectRefund(request);

			logger.info("Response Sent ::" + response);
			return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
