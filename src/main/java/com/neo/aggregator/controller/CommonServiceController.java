package com.neo.aggregator.controller;

import java.util.List;
import java.util.Locale;

import com.neo.aggregator.dto.ecollect.EcollectRequestDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.bank.rbl.BusinessEntityListDto;
import com.neo.aggregator.dao.BusinessEntityDao;
import com.neo.aggregator.dao.NeoAccountInfoDao;
import com.neo.aggregator.dao.NeoCustomerDao;
import com.neo.aggregator.dao.EcollectRequestDao;
import com.neo.aggregator.dao.VirtualAccountCreationRegisterDao;
import com.neo.aggregator.dto.NeoCustomerDataDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.NeoResponse;
import com.neo.aggregator.dto.PartnerServiceConfigurationDto;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.BusinessEntity;
import com.neo.aggregator.model.EcollectRequest;
import com.neo.aggregator.model.NeoAccountDetails;
import com.neo.aggregator.model.NeoCustomerData;
import com.neo.aggregator.model.VirtualAccountCreationRegister;
import com.neo.aggregator.utility.CommonService;
import com.neo.aggregator.utility.NeoExceptionBuilder;
import com.neo.aggregator.utility.NeoExceptionConstant;
import com.neo.aggregator.utility.TenantContextHolder;
import com.neo.aggregator.utility.Utils;

@Controller
@RequestMapping("/neo-lego/common")
public class CommonServiceController {

    private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(CommonServiceController.class);
    private static final String tenantStr="Tenant ::";

    @Autowired
    private NeoCustomerDao customerDao;

    @Autowired
    private BusinessEntityDao businessEntityDao;

    @Autowired
    private VirtualAccountCreationRegisterDao vaCreationRegisterDao;
    
    @Autowired
    private NeoAccountInfoDao accountsDao;

    @Autowired
    CommonService commonService;
    
    @Autowired
    private NeoExceptionBuilder exceptionBuilder;

    @Autowired
    private EcollectRequestDao ecollectRequestDao;

    @RequestMapping(value = "/fetchNeoCustomerData", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<NeoResponse> fetchNeoCustomerData(@RequestBody NeoCustomerDataDto request) {

    	String tenant = TenantContextHolder.getTenant();
        String neoTenant = TenantContextHolder.getNeoTenant();

        try {
            logger.info(tenantStr + tenant);
            logger.info("NeoTenant ::" + neoTenant);
            
            if(StringUtils.isBlank(request.getEntityId()))
            	throw exceptionBuilder.build(NeoExceptionConstant.INVALID_ENTITY_ID, null, Locale.US, NeoExceptionConstant.INVALID_ENTITY_ID);

            logger.info("Request Received fetchNeoCustomerData ::" + request.toString());

            NeoCustomerData customData = customerDao.findByEntityId(request.getEntityId());

			return new ResponseEntity<>(new NeoResponse(customData, null, null), HttpStatus.OK);
			
        } catch (NeoException e) {
        	return new ResponseEntity<>(new NeoResponse(null,e,null), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
			return new ResponseEntity<>(new NeoResponse(null,
                    new NeoException(e.getMessage(), e.getMessage(), null, null, "Unable to fetch Customer Data"),
                    null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/updateNeoCustomerData", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<NeoResponse> updateNeoCustomerData(@RequestBody NeoCustomerDataDto request) {

        String tenant = TenantContextHolder.getNeoTenant();

        try {

            logger.info(tenantStr + tenant);

            logger.info("Request Received updateNeoCustomerData ::" + request.toString());
            
            if(StringUtils.isBlank(request.getEntityId()))
            	throw exceptionBuilder.build(NeoExceptionConstant.INVALID_ENTITY_ID, null, Locale.US, NeoExceptionConstant.INVALID_ENTITY_ID);

            NeoCustomerData customData = customerDao.findByEntityId(request.getEntityId());

            if (customData == null) {
                NeoCustomerData model = new NeoCustomerData();
                model.setCifNumber(request.getCifNumber());
                model.setEntityId(request.getEntityId());
				model.setCorporate(request.getCorporate());
                customerDao.save(model);
			} else if (customData.getCifNumber() == null) {
                customData.setCifNumber(request.getCifNumber());
                customerDao.save(customData);
            }
            
			return new ResponseEntity<>(new NeoResponse(customData, null, null), HttpStatus.OK);
		
        } catch (NeoException e) {
        	return new ResponseEntity<>(new NeoResponse(null,e,null), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			return new ResponseEntity<>(new NeoResponse(null,
                    new NeoException(e.getMessage(), e.getMessage(), null, null, "Unable to update Customer Data"),
                    null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/virtual-account/fetchEntities", consumes = "application/json")
    public ResponseEntity<NeoResponse> fetchBusinessEntities(@RequestBody BusinessEntity request) {

        String tenant = TenantContextHolder.getNeoTenant();

        try {
            logger.info(tenantStr + tenant);

            logger.info("Request Received fetchEntities For RBL ::" + request.toString());

            List<BusinessEntity> entityData;
            if (request.getEntityId() != null)
                entityData = businessEntityDao.findByEntityId(request.getEntityId());
            else
                entityData = businessEntityDao.getAllEntityDetails();

            BusinessEntityListDto entities = BusinessEntityListDto.builder().businessEntityList(entityData).build();

            return new ResponseEntity<NeoResponse>(new NeoResponse(entities, null, null), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<NeoResponse>(new NeoResponse(null,
                    new NeoException(e.getMessage(), e.getMessage(), null, null, "Unable to fetch Business Entity Data"),
                    null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/virtual-account/fetchVADetails", consumes = "application/json")
    public ResponseEntity<NeoResponse> fetchVADetails(@RequestBody VirtualAccountCreationRegister request) {

        String tenant = TenantContextHolder.getTenant();

        try {
            logger.info(tenantStr + tenant);

            logger.info("Request Received fetchVADetails :: " + Utils.convertModelToString(request));

            VirtualAccountCreationRegister vaData = vaCreationRegisterDao.findByEntityIdAndCorporateCreditAndVirtualAccNum(request.getEntityId(),
                    request.getCorporateCredit(), request.getVirtualAccNum());

            return new ResponseEntity<>(new NeoResponse(vaData, null, null), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new NeoResponse(null,
                    new NeoException(e.getMessage(), e.getMessage(), null, null, "Unable to fetch Virtual Account Data"),
                    null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/virtual-account/updateVAData", consumes = "application/json")
    public ResponseEntity<NeoResponse> updateVAData(@RequestBody VirtualAccountCreationRegister request) {

        String tenant = TenantContextHolder.getTenant();

        try {
            logger.info(tenantStr + tenant);

            logger.info("Request Received updateVAData :: " + Utils.convertModelToString(request));

            VirtualAccountCreationRegister vaData = vaCreationRegisterDao.save(request);

            return new ResponseEntity<>(new NeoResponse(vaData, null, null), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new NeoResponse(null,
                    new NeoException(e.getMessage(), e.getMessage(), null, null, "Unable to Save Virtual Account Data"),
                    null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

	@PostMapping(value = "/virtual-account/fetchFailedVADetails", consumes = "application/json")
	public ResponseEntity<NeoResponse> fetchFailedVADetails(@RequestBody VirtualAccountCreationRegister request) {

		String tenant = TenantContextHolder.getTenant();
		try {
			logger.info(tenantStr + tenant);

			logger.info("Request Received fetchFailedVADetails :: " + Utils.convertModelToString(request));

			List<VirtualAccountCreationRegister> vaData = vaCreationRegisterDao
					.findByResultAndRegCount(request.getRetryCount());

			return new ResponseEntity<>(new NeoResponse(vaData, null, null), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new NeoResponse(null,
					new NeoException(e.getMessage(), e.getMessage(), null, null, "Unable to fetch VA Failed Data"),
					null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @RequestMapping(value = "/fetchNeoAccountData", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<NeoResponse> fetchNeoAccountData(@RequestBody NeoCustomerDataDto request) {

        String tenant = TenantContextHolder.getNeoTenant();

        try {
            logger.info(tenantStr + tenant);

            logger.info("Request Received fetchNeoAccountData ::" + request.toString());

            List<NeoAccountDetails> accountData = accountsDao.findByEntityId(request.getEntityId());

			return new ResponseEntity<>(new NeoResponse(accountData, null, null), HttpStatus.OK);
        } catch (Exception e) {
			return new ResponseEntity<>(new NeoResponse(null,
                    new NeoException(e.getMessage(), e.getMessage(), null, null, "Unable to fetch Customer Data"),
                    null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

	@PostMapping(value = "/virtual-account/fetchYapEcollectRequestData", consumes = "application/json")
	public ResponseEntity<NeoResponse> fetchEcollectRequestData(@RequestBody EcollectRequest request) {

		String tenant = TenantContextHolder.getTenant();
		try {
			logger.info(tenantStr + tenant);

			logger.info("In fetchEcollectRequestData...");

			logger.info("Request Received for fetchEcollectRequestData :: " + Utils.convertModelToString(request));

			String created = request.getCreditedAt();
			String corpClientId = "000" + request.getEcollectCode();

			List<String> reqData = ecollectRequestDao.findByApplication(created, corpClientId);

			return new ResponseEntity<>(new NeoResponse(reqData, null, null), HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Exception Occured :: " + e.getMessage());
			return new ResponseEntity<>(new NeoResponse(null,
					new NeoException(e.getMessage(), e.getMessage(), null, null, "Unable to Fetch Recon Data"), null),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @PostMapping(value = "/upsertPartnerServiceConfig", consumes ={MediaType.APPLICATION_JSON_VALUE,MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<NeoResponse> upsertPartnerServiceConfig(@RequestPart("data") String request,@RequestPart("file") MultipartFile file) {
        try {
            logger.info("Request Received Load Public Key ::" + request);

            PartnerServiceConfigurationDto configurationDto= new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false)
                    .readValue(request,PartnerServiceConfigurationDto.class);
            Boolean result = commonService.upsertPartnerServiceConfig(configurationDto,file);

            return new ResponseEntity<>(new NeoResponse(result, null, null), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new NeoResponse(false,
                    new NeoException(e.getMessage(), e.getMessage(), null, null, "Unable to Upsert Public Key"),
                    null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/fetchEcollectRequestData", consumes = "application/json")
    public ResponseEntity<NeoResponse> fetchEcollectData(@RequestBody EcollectRequestDto request) {

        String tenant = TenantContextHolder.getTenant();
        try {
            logger.info(tenantStr + tenant);

            logger.info("Request Received for fetchEcollectRequestData :: " + Utils.convertModelToString(request));

            List<EcollectRequest> reqData = ecollectRequestDao.findByTransferUniqueNumberAndEcollectCode(request.getTransferUniqueNumber(),
                    request.getEcollectCode());

            return new ResponseEntity<>(new NeoResponse(reqData, null, null), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Exception Occured :: " + e.getMessage());
            return new ResponseEntity<>(new NeoResponse(null,
                    new NeoException(e.getMessage(), e.getMessage(), null, null, "Unable to Fetch Ecollect Data"), null),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(value = "/updateRrn", consumes = "application/json")
    public ResponseEntity<NeoResponse> updateRrn(@RequestBody String request) {

        String tenant = TenantContextHolder.getTenant();
        try {
            logger.info(tenantStr + tenant);

            logger.info("Request Received for updating RRN :: " + request);

            Boolean respData = commonService.updateTxnRRN(request);

            return new ResponseEntity<>(new NeoResponse(respData, null, null), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Exception Occured :: " + e.getMessage());
            return new ResponseEntity<>(new NeoResponse(null,
                    new NeoException(e.getMessage(), e.getMessage(), null, null, "Unable to Update RRN"), null),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
