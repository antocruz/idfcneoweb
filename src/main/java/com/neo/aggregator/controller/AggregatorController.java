package com.neo.aggregator.controller;

import com.neo.aggregator.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.service.AggregatorInterfaceService;
import com.neo.aggregator.service.AggregatorService;
import com.neo.aggregator.utility.TenantContextHolder;
import com.neo.aggregator.utility.Utils;

@Controller
@RequestMapping("/neo-aggregator")
public class AggregatorController {

	@Autowired
	private AggregatorInterfaceService aggregatorService;

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(AggregatorController.class);

	@RequestMapping(value = "/createRetailCif", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> retCustAdd(@RequestBody RegistrationRequestV2Dto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received createReailCif ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);

			RegistrationResponseDto response = service.retailCifCreation(request);

			logger.info("Response Sent createReailCif ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/sbAccountCreate", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> sbAcctAdd(@RequestBody AccountCreationRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received sbAccountCreate ::" + request.toString());

			TenantContextHolder.switchToDefaultTenant();

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);

			AccountCreationResponseDto response = service.savingsAccountCreation(request);

			logger.info("Response Sent sbAccountCreate ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/fetchBalance", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> balinq(@RequestBody BalanceCheckRequest request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received fetchBalance ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);

			BalanceCheckResponseDto response = service.fetchAccountBalance(request);

			logger.info("Response Sent fetchBalance ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/fetchStatement", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> getFullAccountStatementWithPagination(
			@RequestBody FetchAccountStatementRequest request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received fetchStatement ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			FetchAccountStatemetResponse response = service.fetchAccountStatement(request);

			logger.info("Response Sent fetchStatement ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/validate/pan", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> validatePan(@RequestBody PanValidationRequest request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received validatePan ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			PanValidationResponse response = service.validatePan(request);

			logger.info("Response Sent validatePan ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/generateOtp/aadhaar", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> generateOtpAadhaar(@RequestBody AadhaarOtpRequest request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received generateAOtp ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			AadhaarOtpResponse response = service.generateOtpAadhaar(request);

			logger.info("Response Sent generateAOtp ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/validateOtp/aadhaar", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> validateOtpAadhaar(@RequestBody AadhaarOtpRequest request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received validateAOtp ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);

			AadhaarOtpResponse response = service.validateOtpAadhaar(request);

			logger.info("Response Sent validateAOtp ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/addProduct", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> addProduct(@RequestBody RegistrationRequestV2Dto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received addProduct ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			RegistrationResponseDto response = service.addProduct(request);

			logger.info("Response Sent addProduct ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/fundTransfer", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> fundTransfer(@RequestBody FundTransferRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received fundTransfer ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			FundTransferResponseDto response = service.fundTransfer(request);

			logger.info("Response Sent fundTransfer ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/sbAccountModify", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> sdAccountModify(@RequestBody AccountModificationRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received sdAccountModify ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			AccountModificationResponseDto response = service.savingsAccountModification(request);

			logger.info("Response Sent sdAccountModify ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/fdAccountClosure", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> tdAccountClosure(@RequestBody AccountClosureRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received fdAccountClosure ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			AccountClosureResponseDto response = service.tdAccountClosure(request);

			logger.info("Response Sent fdAccountClosure ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/fdAccountTrailClosure", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> tdAccountCloseTrailClosure(@RequestBody TdAccountTrailCloseRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();
		
		try {

			logger.info("Request Received fdAccountClosure ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			TDAcctCloseTrailResponseDto response = service.tdTrailAccountClosure(request);

			logger.info("Response Sent fdAccountClosure ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/fdAccountInquiry", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> tdAccountInquiry(@RequestBody AccountInquiryRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received fdAccountInquiry ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			AccountInquiryResponseDto response = service.tdAccountInquiry(request);

			logger.info("Response Sent fdAccountInquiry ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/fdAccountCreate", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> tdAcctAdd(@RequestBody AccountCreationRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received fdAccountCreate ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			AccountCreationResponseDto response = service.termDepositCreation(request);

			logger.info("Response Sent fdAccountCreate ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/eKycBio/OneTimeToken/{eKycRefnum}", consumes = "application/json")
	public ResponseEntity<NeoResponse> eKycBioOneTimeToken(@RequestBody RegistrationRequestV2Dto request,
			@PathVariable String eKycRefnum) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received eKycBioOTT ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			KYCBioResponseDto response = service.bioEKycOneTimeToken(request, eKycRefnum);

			logger.info("Response Sent eKycBioOTT ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/eKycBio/validate", consumes = "application/json")
	public ResponseEntity<NeoResponse> eKycBioValidation(@RequestBody KYCBioRequestDto request) {

        String tenant = TenantContextHolder.getNeoTenant();

        try {
            logger.info("Request Received eKycValidateBio ::" + Utils.convertModelToStringWithMaskedAadhar(request));

            AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
            KYCBioResponseDto response = service.eKycBioValidate(request);

            logger.info("Response Sent eKycValidateBio ::" + Utils.convertModelToString(response));

            return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
        } catch (NeoException e) {
            return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

	@PostMapping(value = "/eKycBio/getDemographicData", consumes = "application/json")
	public ResponseEntity<NeoResponse> eKycBioGetDemographicData(@RequestBody KYCBioRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received eKyc Get Demographic Data ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			KYCBioResponseDto response = service.getDemographicData(request, null);

			logger.info("Response Sent eKyc Get Demographic Data ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/fundTransferWithOtp", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> fundTransferWithOtp(@RequestBody FundTransferRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received fundTransferWithOtp ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			FundTransferResponseDto response = service.fundTransferWithOtp(request);

			logger.info("Response Sent fundTransferWithOtp ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/fundTransferValidateOtp", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> fundTransferValidateOtp(@RequestBody FundTransferRequestDto request) {
		
		String tenant = TenantContextHolder.getNeoTenant();
		
		try {
			
			logger.info("Request Received fundTransferValidateOtp ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			FundTransferResponseDto response = service.fundTransferValidateOtp(request);

			logger.info("Response Sent fundTransferValidateOtp ::" + response.toString());
			
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
			
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/sbAccountInquiry", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> sbAccountInquiry(@RequestBody AccountInquiryRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received sbAccountInquiry ::" + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			AccountInquiryResponseDto response = service.savingsAccountInquiry(request);

			logger.info("Response Sent sbAccountInquiry ::" + response.toString());

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/fundTransfer/statusCheck", consumes = "application/json")
	public ResponseEntity<NeoResponse> paymentStatusCheck(@RequestBody FundTransferRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received Fund Trf Status ::" + Utils.convertModelToString(request));

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			FundTransferResponseDto response = service.paymentStatusCheck(request);

			logger.info("Response Sent Fund Trf Status ::" + Utils.convertModelToString(response));

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/kyc/invokeAadharXml", consumes = "application/json")
	public ResponseEntity<NeoResponse> eKycInvokeAadharXml(@RequestBody RegistrationRequestV2Dto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received eKyc Invoke Aadhar Xml ::" + Utils.convertModelToString(request));

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			KYCBioResponseDto response = service.aadharXmlInvoke(request);

			logger.info("Response Sent eKyc Invoke Aadhar Xml ::" + Utils.convertModelToString(response));

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(new NeoResponse(null,
					new NeoException(null,e.getMessage(),null,null,e.getLocalizedMessage()),
					null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/kyc/validateAadharXml", consumes = "application/json")
	public ResponseEntity<NeoResponse> eKycValidateAadharXml(@RequestBody AadhaarOtpRequest request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received eKyc Validate Aadhar Xml ::" + Utils.convertModelToString(request));

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			KYCBioResponseDto response = service.validateAadhaarXml(request);

			logger.info("Response Sent eKyc Validate Aadhar Xml ::" + Utils.convertModelToString(response));

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(value = "/fetchAccountDetails", consumes = "application/json")
	public ResponseEntity<NeoResponse> fetchAccountDetails(@RequestBody AccountInquiryRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {

			logger.info("Request Received fetchAccountDetails ::" + Utils.convertModelToString(request));

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			AccountInquiryResponseDto response = service.fetchAccountDetails(request);

			logger.info("Response Sent fetchAccountDetails ::" + Utils.convertModelToString(response));

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PostMapping(value = "/dedupCheck", consumes = "application/json")
	public ResponseEntity<NeoResponse> dedupCheck(@RequestBody RegistrationRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {
			logger.info("Tenant :: " + TenantContextHolder.getNeoTenant());

			logger.info("Request Received for Dedup Check :: " + request.toString());

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			CustomerDedupCheckResponseDto response = service.dedupCheck(request);

			logger.info("Response Sent ::" + response.toString());

			return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Deprecated
	@PostMapping(value = "/payLater/accountDiscovery", consumes = "application/json")
	public ResponseEntity<NeoResponse> payLaterFindAccount(@RequestBody PayLaterRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {
			logger.info("Request Received PayLater Find Account :: " + Utils.convertModelToString(request));

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			PayLaterResponseDto response = service.payLaterAccountDiscovery(request);

			logger.info("Response Sent PayLater Find Account :: " + Utils.convertModelToString(response));

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Deprecated
	@PostMapping(value = "/payLater/balanceInquiry", consumes = "application/json")
	public ResponseEntity<NeoResponse> payLaterbalanceInquiry(@RequestBody PayLaterRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {
			logger.info("Request Received PayLater Account Balance :: " + Utils.convertModelToString(request));

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			PayLaterResponseDto response = service.payLaterAcctBalInquiry(request);

			logger.info("Response Sent PayLater Account Balance :: " + Utils.convertModelToString(response));

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Deprecated
	@PostMapping(value = "/payLater/otpCreation", consumes = "application/json")
	public ResponseEntity<NeoResponse> payLaterOtpCreation(@RequestBody PayLaterRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {
			logger.info("Request Received PayLater OTP Create :: " + Utils.convertModelToString(request));

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			PayLaterResponseDto response = service.payLaterOtpCreation(request);

			logger.info("Response Sent PayLater OTP Create :: " + Utils.convertModelToString(response));

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Deprecated
	@PostMapping(value = "/payLater/otpVerify", consumes = "application/json")
	public ResponseEntity<NeoResponse> payLaterOtpVerification(@RequestBody PayLaterRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {
			logger.info("Request Received PayLater OTP Verify :: " + Utils.convertModelToString(request));

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			PayLaterResponseDto response = service.payLaterOtpVerification(request);

			logger.info("Response Sent PayLater OTP Verify :: " + Utils.convertModelToString(response));

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Deprecated
	@PostMapping(value = "/payLater/debit", consumes = "application/json")
	public ResponseEntity<NeoResponse> payLaterDebitOrchestration(@RequestBody PayLaterRequestDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {
			logger.info("Request Received PayLater Debit :: " + Utils.convertModelToString(request));

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			PayLaterResponseDto response = service.payLaterDebitOrchestration(request);

			logger.info("Response Sent PayLater Debit :: " + Utils.convertModelToString(response));

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/fundTransfer/addBeneficiary", consumes = "application/json")
	public ResponseEntity<NeoResponse> addBeneficiary(@RequestBody BeneficiaryManagementReqDto request) {

		String tenant = TenantContextHolder.getNeoTenant();

		try {
			logger.info("Request Received Beneficiary Addition :: " + Utils.convertModelToString(request));

			AggregatorService service = aggregatorService.findServiceBasedonTenant(tenant);
			BeneficiaryManagementResDto response = service.beneficiaryAddition(request);

			logger.info("Response Sent Beneficiary Addition :: " + Utils.convertModelToString(response));

			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
