package com.neo.aggregator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.neo.aggregator.dto.AadhaarOtpRequest;
import com.neo.aggregator.dto.AadhaarOtpResponse;
import com.neo.aggregator.dto.AccountCreationRequestDto;
import com.neo.aggregator.dto.AccountCreationResponseDto;
import com.neo.aggregator.dto.BalanceCheckRequest;
import com.neo.aggregator.dto.BalanceCheckResponseDto;
import com.neo.aggregator.dto.FetchAccountStatementRequest;
import com.neo.aggregator.dto.FetchAccountStatemetResponse;
import com.neo.aggregator.dto.FundTransferRequestDto;
import com.neo.aggregator.dto.FundTransferResponseDto;
import com.neo.aggregator.dto.LienMarkRequestDto;
import com.neo.aggregator.dto.LienMarkResponseDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.NeoResponse;
import com.neo.aggregator.dto.PanValidationRequest;
import com.neo.aggregator.dto.PanValidationResponse;
import com.neo.aggregator.dto.RegistrationRequestV2Dto;
import com.neo.aggregator.dto.RegistrationResponseDto;
import com.neo.aggregator.dto.SessionLoginDto;
import com.neo.aggregator.dto.TestAPIRequestDto;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.service.SbmEbsService;

@Controller
@RequestMapping("/sbm")
public class SbmEbsController {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(SbmEbsController.class);

	@Autowired
	private SbmEbsService service;

	@PostMapping(value = "/retCustAdd", consumes = "application/json")
	public ResponseEntity<NeoResponse> retCustAdd(@RequestBody RegistrationRequestV2Dto request) {

		try {

			logger.info("Request Received retCustAdd:: " + request.toString());

			RegistrationResponseDto response = service.retailCifCreation(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/sbAcctAdd", consumes = "application/json")
	public ResponseEntity<NeoResponse> sbAcctAdd(@RequestBody AccountCreationRequestDto request) {

		try {

			logger.info("Request Received sbAcctAdd:: " + request.toString());

			AccountCreationResponseDto response = service.savingsAccountCreation(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/balinq", consumes = "application/json")
	public ResponseEntity<NeoResponse> balinq(@RequestBody BalanceCheckRequest request) {

		try {

			logger.info("Request Received balinq:: " + request.toString());

			BalanceCheckResponseDto response = service.fetchAccountBalance(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/getFullAccountStatementWithPagination", consumes = "application/json")
	public ResponseEntity<NeoResponse> getFullAccountStatementWithPagination(
			@RequestBody FetchAccountStatementRequest request) {

		try {

			logger.info("Request Received getFullAccountStatement:: " + request.toString());

			FetchAccountStatemetResponse response = service.fetchAccountStatement(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/tdAcctAdd", consumes = "application/json")
	public ResponseEntity<NeoResponse> tdAcctAdd(
			@RequestBody AccountCreationRequestDto request) {

		try {

			logger.info("Request Received tdAcctAdd:: " + request.toString());

			AccountCreationResponseDto response = service.termDepositCreation(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/outboundPayment", consumes = "application/json")
	public ResponseEntity<NeoResponse> outboundPayment(@RequestBody FundTransferRequestDto request) {

		try {

			logger.info("Request Received outboundPayment:: " + request.toString());

			FundTransferResponseDto response = service.addOutboundPymt(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/validatePan", consumes = "application/json")
	public ResponseEntity<NeoResponse> validatePan(@RequestBody PanValidationRequest request) {

		try {

			logger.info("Request Received validatePan:: " + request.toString());

			PanValidationResponse response = service.validatePan(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(value = "/generateOtp/aadhaar", consumes = "application/json")
	public ResponseEntity<NeoResponse> generateOtpAadhaar(@RequestBody AadhaarOtpRequest request) {

		try {

			logger.info("Request Received generateAOTP:: " + request.toString());

			AadhaarOtpResponse response = service.generateOtpAadhaar(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/validateOtp/aadhaar", consumes = "application/json")
	public ResponseEntity<NeoResponse> validateOtpAadhaar(@RequestBody AadhaarOtpRequest request) {

		try {

			logger.info("Request Received validateAOTP:: " + request.toString());

			AadhaarOtpResponse response = service.validateOtpAadhaar(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/login", consumes = "application/json")
	public ResponseEntity<NeoResponse> login(@RequestBody SessionLoginDto request) {

		try {

			logger.info("Request Received login:: " + request.toString());

			SessionLoginDto response = service.loginCreation(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/imps", consumes = "application/json")
	public ResponseEntity<NeoResponse> imps(@RequestBody FundTransferRequestDto request) {

		try {

			logger.info("Request Received IMPS:: " + request.toString());

			FundTransferResponseDto response = service.impsTransfer(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/directDebit", consumes = "application/json")
	public ResponseEntity<NeoResponse> directDebit(@RequestBody FundTransferRequestDto request) {

		try {

			logger.info("Request Received directDebit:: " + request.toString());

			FundTransferResponseDto response = service.directDebit(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/directCredit", consumes = "application/json")
	public ResponseEntity<NeoResponse> directCredit(@RequestBody FundTransferRequestDto request) {

		try {

			logger.info("Request Received directCredit:: " + request.toString());

			FundTransferResponseDto response = service.directCredit(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/addLienMark", consumes = "application/json")
	public ResponseEntity<NeoResponse> addLienMark(@RequestBody LienMarkRequestDto request) {

		try {

			logger.info("Request addLienMark addLienMark:: " + request.toString());

			LienMarkResponseDto response = service.addLienMark(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/testAPI", consumes = "application/json")
	public ResponseEntity<NeoResponse> testAPI(@RequestBody TestAPIRequestDto request) {

		try {

			String response = "";

			response = service.testAPI(request);
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(value = "/sbAcctInquiry", consumes = "application/json")
	public ResponseEntity<NeoResponse> sbAcctInquiry(@RequestBody LienMarkRequestDto request) {

		try {

			logger.info("Request addLienMark addLienMark:: " + request.toString());

			LienMarkResponseDto response = service.addLienMark(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(value = "/getMiniAccountStatement", consumes = "application/json")
	public ResponseEntity<NeoResponse> getMiniAccountStatement(@RequestBody FetchAccountStatementRequest request) {

		try {

			logger.info("Request getMiniAccountStatement:: " + request.toString());

			FetchAccountStatemetResponse response = service.fetchMiniStatement(request);

			logger.info("Response Sent ::" + response.toString());
			return new ResponseEntity<>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
