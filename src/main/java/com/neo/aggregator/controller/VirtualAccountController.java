package com.neo.aggregator.controller;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.neo.aggregator.model.EcollectRequest;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.bank.icici.core.IciciCoreDto;
import com.neo.aggregator.bank.icici.request.DynamicVpaNotifyRequest;
import com.neo.aggregator.bank.icici.request.DynamicVpaValidateRequest;
import com.neo.aggregator.bank.icici.response.DynamicVpaValidateResponse;
import com.neo.aggregator.bank.icici.utils.EncryptionDecryption;
import com.neo.aggregator.commonservice.IciciCommonService;
import com.neo.aggregator.constant.IciciPropertyConstants;
import com.neo.aggregator.constant.NeoCustomFieldConstants;
import com.neo.aggregator.constant.ProductConstants;
import com.neo.aggregator.dao.EcollectCoreUrlConfigDao;
import com.neo.aggregator.dao.PartnerServiceConfigRepo;
import com.neo.aggregator.dao.UpiCollectRequestDao;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.NeoResponse;
import com.neo.aggregator.dto.ecollect.AddVARequestDto;
import com.neo.aggregator.dto.ecollect.EcollectData;
import com.neo.aggregator.dto.ecollect.EcollectRequestDto;
import com.neo.aggregator.dto.ecollect.YesEcollectRequestDto;
import com.neo.aggregator.dto.ecollect.YesEcollectResponseDto;
import com.neo.aggregator.dto.ecollect.YesEcollectValidateRepsonseDto;
import com.neo.aggregator.dto.ecollect.icici.IciciEcollectDto;
import com.neo.aggregator.dto.ecollect.rbl.RblEcollectRequestDto;
import com.neo.aggregator.dto.ecollect.rbl.RblEcollectResponseDto;
import com.neo.aggregator.dto.ecollect.upi.UpicollectRequestDto;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.EcollectCoreUrlConfig;
import com.neo.aggregator.model.NeoBusinessCustomField;
import com.neo.aggregator.model.PartnerServiceConfiguration;
import com.neo.aggregator.model.UpiCollectRequest;
import com.neo.aggregator.service.NeoBusinessCustomFieldService;
import com.neo.aggregator.service.VirtualAccountService;
import com.neo.aggregator.serviceimpl.RblEsbServiceImpl;
import com.neo.aggregator.utility.EncryptionUtil;
import com.neo.aggregator.utility.TxnIdGenerator;

@Controller
@RequestMapping("/virtual-account")
public class VirtualAccountController {

    @Autowired
    RestTemplate restTemplate;

    @Value("${core.ecollecturl}")
    private String coreURL;

    @Value("${core.ecollectValidateurl}")
    private String coreValidateUrl;

    @Value("${neo.agg.core.url}")
    private String coreBaseUrl;

    @Autowired
    RblEsbServiceImpl rblService;

    @Autowired
    EcollectCoreUrlConfigDao coreUrlConfigDao;
    
    @Autowired
    private VirtualAccountService virtualAccountService;

    @Autowired
    EncryptionDecryption encryptionDecryption;

    @Autowired
    IciciCommonService iciciCommonService;
    
    @Autowired
    private NeoBusinessCustomFieldService customFieldService;
    
	@Value("${neo.agg.yes.ecollect.secret:3qt5Bdsk0617}")
	private String yesEcollectSecret;

    @Autowired
    DozerBeanMapper dozerBeanMapper;

    @Autowired
    UpiCollectRequestDao upiCollectRequestDao;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    PartnerServiceConfigRepo apiConfig;

    private static Log4j2Wrapper LOGGER = (Log4j2Wrapper) Log4j2LogManager.getLog(VirtualAccountController.class);
    private static final String ICICI_STR ="ICICI";
    private static final String ACCEPT_STR ="ACCEPT";
    private static final String REJECT_STR ="REJECT";
    private static final String REQUEST_MAPPING_SUCCESS_STR ="Request Mapping success";
    private static final String FAILED_TRANSACTION_STR ="Failed Transaction";
    private static final String INVALID_STR="INVALID";
    private static final String UPICOLLECT_STR="UPICOLLECT";
    private static final String FAILED_STR="Failed";

    @PostMapping(value = {"/icicinotify", "/icicinotify/{vaCodeLength}"}, consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> icicinotify(HttpServletRequest httpreq, @RequestBody String request,
                                                        @PathVariable Optional<Integer> vaCodeLength) throws Exception {

        LOGGER.info("Entering ICICI eCollect api");
        IciciEcollectDto dto = null;
        ObjectMapper writeJSON = new ObjectMapper();
        NeoResponse coreResp = new NeoResponse();
        EcollectCoreUrlConfig coreUrlConfig;
        String referenceNum = "";
        String response = "";
        String coreUrl = "";
        boolean isReqEncrypted = false;
        String encryptedResponse="";

        PartnerServiceConfiguration serviceModel = apiConfig.findByBankIdAndBankServiceId(ICICI_STR,
                ProductConstants.ICICI_ECOLLECT);
        try {
            LOGGER.info("ICICI JSON :: "+ request);

            try {
                IciciCoreDto encryptedRequestDto = new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(request, IciciCoreDto.class);

                if (encryptedRequestDto.getEncryptedData() != null) {
                    String decryptedRequest = encryptionDecryption.aesDecrypt(encryptedRequestDto.getEncryptedData(),
                            encryptedRequestDto.getEncryptedKey(), IciciPropertyConstants.ICICI_M2P_4096KEY);
                    dto = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                            .readValue(decryptedRequest, IciciEcollectDto.class);

                    isReqEncrypted = true;
                } else {
                    dto = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                            .readValue(request, IciciEcollectDto.class);
                }

            } catch (Exception jEx) {
                LOGGER.error("Json Processing Failure :: " + jEx.getMessage());
                if(dto == null)
                    dto = new IciciEcollectDto();
                dto.setRejectReason("Failure");
                dto.setStatus("Failed, invalid Message Type");
                response = writeJSON.writeValueAsString(dto);
                LOGGER.info(response);
                encryptedResponse = iciciCommonService.buildIciciCoreRequest(serviceModel.getBankPublicKey(), response);
                return new ResponseEntity<>(encryptedResponse, HttpStatus.BAD_REQUEST);
            }

            LOGGER.info(REQUEST_MAPPING_SUCCESS_STR);

            EcollectRequestDto requestDto = new EcollectRequestDto();

            referenceNum = TxnIdGenerator.getReferenceNumber();
            requestDto.setEcollectCode(dto.getVaNumber());
            requestDto.setAmount(dto.getAmt());
            requestDto.setTransferUniqueNumber(dto.getUtrNo());
            requestDto.setCustomerCode(dto.getCustomerCode());
            requestDto.setVirtualAccountNo(dto.getVaNumber());
            requestDto.setTransferType(dto.getTransactionMode());
            requestDto.setRemitterAccountNumber(dto.getRemitterAccountNumber());
            requestDto.setDescription(dto.getSenderRemark());
            requestDto.setRemitterIFSCCode(dto.getRemitterIfscCode());
            requestDto.setRemitterFullName(dto.getRemitterName());
            requestDto.setCreditedAt(dto.getRemitterPaymentDate());
            requestDto.setApplication(ICICI_STR);
            requestDto.setM2pReferenceNumber(referenceNum);
            if (vaCodeLength.isPresent())
                requestDto.setVaCodeLength(vaCodeLength.get());

            List<EcollectRequest> duplicateTxns = virtualAccountService.fetchEcollectData("YAP",requestDto);

            if(duplicateTxns.isEmpty()) {
                LOGGER.info("Request to Core :: " + new ObjectMapper().writeValueAsString(requestDto));

                String eCollectCode = dto.getVaNumber().substring(0, requestDto.getVaCodeLength());
                coreUrlConfig = coreUrlConfigDao.findByEcollectCode(eCollectCode);

                if (coreUrlConfig != null) {
                    coreUrl = coreUrlConfig.getCoreServiceUrl();
                }

                coreResp = postToCore(new ObjectMapper().writeValueAsString(requestDto), coreUrl);

                if (coreResp.getResult() == Boolean.TRUE) {
                    dto.setRejectReason("");
                    dto.setStatus(ACCEPT_STR);
                } else {
                    dto.setRejectReason(FAILED_TRANSACTION_STR);
                    dto.setStatus(REJECT_STR);
                }
            } else {
                //dup txn found, so returning the already posted response
                if (duplicateTxns.get(0).getResult().equalsIgnoreCase("true")) {
                    dto.setRejectReason("");
                    dto.setStatus(ACCEPT_STR);
                } else {
                    dto.setRejectReason(FAILED_TRANSACTION_STR);
                    dto.setStatus(REJECT_STR);
                }
            }

            response = writeJSON.writeValueAsString(dto);
            LOGGER.info(response);
            if (isReqEncrypted) {
                encryptedResponse = iciciCommonService.buildIciciCoreRequest(serviceModel.getBankPublicKey(), response);
                return new ResponseEntity<>(encryptedResponse, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            if(dto == null)
                dto = new IciciEcollectDto();
            dto.setRejectReason("Service not available");
            dto.setStatus("REJECT");
            response = new ObjectMapper().writeValueAsString(dto);
            LOGGER.info(response);
            if (isReqEncrypted) {
                encryptedResponse = iciciCommonService.buildIciciCoreRequest(serviceModel.getBankPublicKey(), response);
                return new ResponseEntity<>(encryptedResponse, HttpStatus.INTERNAL_SERVER_ERROR);
            }else {
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    @PostMapping(value = {"/sbmnotify", "/sbmnotify/{vaCodeLength}"}, consumes = "application/json")
    public ResponseEntity<String> sbmnotify(HttpServletRequest httpreq, @RequestBody String request,
                                              @PathVariable Optional<Integer> vaCodeLength) throws Exception {

        LOGGER.info("Entering SBM eCollect api");
        IciciEcollectDto dto = new IciciEcollectDto();
        ObjectMapper writeJSON = new ObjectMapper();
        NeoResponse coreResp = new NeoResponse();
        EcollectCoreUrlConfig coreUrlConfig;
        String referenceNum = "";
        String response = "";
        String coreUrl = "";
        String encryptedResponse="";

        PartnerServiceConfiguration serviceModel = apiConfig.findByBankIdAndBankServiceId(ICICI_STR,
                ProductConstants.SBM_ICICI_ECOLLECT);

        try {
            LOGGER.info("SBM JSON :: "+ request);

            try {
                IciciCoreDto encryptedRequestDto = new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(request, IciciCoreDto.class);

                String decryptedRequest = encryptionDecryption.aesDecrypt(encryptedRequestDto.getEncryptedData(),
                        encryptedRequestDto.getEncryptedKey(), IciciPropertyConstants.ICICI_M2P_4096KEY);
                dto = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(decryptedRequest, IciciEcollectDto.class);
                LOGGER.info(REQUEST_MAPPING_SUCCESS_STR);
            } catch (Exception e) {
                LOGGER.info("Request Mapping failed :: "+ e.getMessage());
                dto.setRejectReason("Failure");
                dto.setStatus("Failed, invalid Message Type");
                response = writeJSON.writeValueAsString(dto);
                LOGGER.info(response);
                encryptedResponse=iciciCommonService.buildIciciCoreRequest(serviceModel.getBankPublicKey(),response);
                return new ResponseEntity<>(encryptedResponse, HttpStatus.BAD_REQUEST);
            }

            EcollectRequestDto requestDto = new EcollectRequestDto();

            referenceNum = TxnIdGenerator.getReferenceNumber();
            requestDto.setEcollectCode(dto.getVaNumber());
            requestDto.setAmount(dto.getAmt());
            requestDto.setTransferUniqueNumber(dto.getUtrNo());
            requestDto.setCustomerCode(dto.getCustomerCode());
            requestDto.setVirtualAccountNo(dto.getVaNumber());
            requestDto.setTransferType(dto.getTransactionMode());
            requestDto.setRemitterAccountNumber(dto.getRemitterAccountNumber());
            requestDto.setDescription(dto.getSenderRemark());
            requestDto.setRemitterIFSCCode(dto.getRemitterIfscCode());
            requestDto.setRemitterFullName(dto.getRemitterName());
            requestDto.setCreditedAt(dto.getRemitterPaymentDate());
            requestDto.setApplication("SBM");
            requestDto.setM2pReferenceNumber(referenceNum);
            if (vaCodeLength.isPresent())
                requestDto.setVaCodeLength(vaCodeLength.get());

            List<EcollectRequest> duplicateTxns = virtualAccountService.fetchEcollectData("YAP",requestDto);

            if(duplicateTxns.isEmpty()) {
                LOGGER.info("Request to Core :: " + new ObjectMapper().writeValueAsString(requestDto));

                String eCollectCode = dto.getVaNumber().substring(0, requestDto.getVaCodeLength());
                coreUrlConfig = coreUrlConfigDao.findByEcollectCode(eCollectCode);

                if (coreUrlConfig != null) {
                    coreUrl = coreUrlConfig.getCoreServiceUrl();
                }

                coreResp = postToCore(new ObjectMapper().writeValueAsString(requestDto), coreUrl);

                if (coreResp.getResult() == Boolean.TRUE) {
                    dto.setRejectReason("");
                    dto.setStatus(ACCEPT_STR);
                } else {
                    dto.setRejectReason(FAILED_TRANSACTION_STR);
                    dto.setStatus(REJECT_STR);
                }
            } else {
                //dup txn found, so returning the already posted response
                if (duplicateTxns.get(0).getResult().equalsIgnoreCase("true")) {
                    dto.setRejectReason("");
                    dto.setStatus(ACCEPT_STR);
                } else {
                    dto.setRejectReason(FAILED_TRANSACTION_STR);
                    dto.setStatus(REJECT_STR);
                }
            }
            response = writeJSON.writeValueAsString(dto);
            LOGGER.info(response);
            encryptedResponse=iciciCommonService.buildIciciCoreRequest(serviceModel.getBankPublicKey(),response);
            return new ResponseEntity<>(encryptedResponse, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.info("Request failed :: "+ e.getMessage());
            dto.setRejectReason("Service not available");
            dto.setStatus(REJECT_STR);
            response = new ObjectMapper().writeValueAsString(dto);
            LOGGER.info("Resposne to bank :: "+response);
            encryptedResponse=iciciCommonService.buildIciciCoreRequest(serviceModel.getBankPublicKey(),response);
            return new ResponseEntity<>(encryptedResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @SuppressWarnings("unchecked")
    @PostMapping(value = {"/rblnotify", "/rblnotify/{vaCodeLength}"}, consumes = "application/json")
    public ResponseEntity<RblEcollectResponseDto> rblnotify(HttpServletRequest httpreq, @RequestBody String request,
                                                            @PathVariable Optional<Integer> vaCodeLength) throws JsonProcessingException {

        LOGGER.info("Entering RBL Notify API...");

        RblEcollectResponseDto response = new RblEcollectResponseDto();
        NeoResponse coreResp = new NeoResponse();
        ObjectMapper mapper = new ObjectMapper();
        String coreUrl = "";
        EcollectCoreUrlConfig coreUrlConfig;

        try {

            String referenceNum = TxnIdGenerator.getReferenceNumber();

            RblEcollectRequestDto[] reqDto = null;

            LOGGER.info("RBL Request received::: "+ request);

            try {
                Map<String, String> reqMapper = mapper.readValue(request, Map.class);

                reqDto = mapper.readValue(new ObjectMapper().writeValueAsString(reqMapper.get("Data")),
                        RblEcollectRequestDto[].class);

                LOGGER.info("Request Mapping Success");
            } catch (Exception e) {
                LOGGER.info("Request Mapping failed");
                
                response.setErrorCode("1");
                response.setErrorMsg(FAILED_STR);
                response.setResult("1");

                LOGGER.info(new ObjectMapper().writeValueAsString(response));

                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }

            EcollectRequestDto ecollectRequest = new EcollectRequestDto();
            for (RblEcollectRequestDto dto : reqDto) {

                String transferType="";

                if (dto.getUTRNumber().contains("UPI")){
                    transferType="UPI";
                }else{
                    transferType=dto.getMessageType();
                }

                ecollectRequest.setEcollectCode(dto.getBeneficiaryAccountNumber());
                ecollectRequest.setAmount(dto.getAmount());
                ecollectRequest.setTransferUniqueNumber(dto.getUTRNumber());
                ecollectRequest.setCustomerCode(dto.getClientCodeMaster());
                ecollectRequest.setVirtualAccountNo(dto.getBeneficiaryAccountNumber());
                ecollectRequest.setTransferType(transferType);
                ecollectRequest.setRemitterAccountNumber(dto.getSenderAccountNumber());
                ecollectRequest.setRemitterAccountType(dto.getSenderAccountType());
                ecollectRequest.setRemitterIFSCCode(dto.getSenderIFSC());
                ecollectRequest.setRemitterFullName(dto.getSenderName());
                ecollectRequest.setCreditedAt(dto.getCreditDate());
                ecollectRequest.setCreditAccountNo(dto.getSenderAccountNumber());
                ecollectRequest.setApplication("RBL");
                ecollectRequest.setM2pReferenceNumber(referenceNum);
                if (vaCodeLength.isPresent())
                    ecollectRequest.setVaCodeLength(vaCodeLength.get());
            }

            String eCollectCode = ecollectRequest.getEcollectCode().substring(0, ecollectRequest.getVaCodeLength());
            coreUrlConfig = coreUrlConfigDao.findByEcollectCode(eCollectCode);

            if (coreUrlConfig != null) {
                coreUrl = coreUrlConfig.getCoreServiceUrl();
            }

            coreResp = postToCore(mapper.writeValueAsString(ecollectRequest), coreUrl);

            if (coreResp.getResult() == Boolean.TRUE) {
            	response.setErrorCode("0");
                response.setErrorMsg("Success");
                response.setResult("0");
            } else {
                response.setErrorCode("1");
                response.setErrorMsg(FAILED_STR);
                response.setResult("1");
            }
            LOGGER.info("Response from Core :: "+ mapper.writeValueAsString(response));

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.info("Request Failed :: "+e.getMessage());
        	response.setErrorCode("1");
            response.setErrorMsg(FAILED_STR);
            response.setResult("1");

            LOGGER.info(mapper.writeValueAsString(response));

            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/rblVACreate", consumes = "application/json")
    public ResponseEntity<NeoResponse> rblVACreate(HttpServletRequest httpreq, @RequestBody String request) {

        AddVARequestDto requestDto = new AddVARequestDto();

        LOGGER.info("Entering RBL VA Creation API...");
        try {

            try {
                requestDto = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(request, AddVARequestDto.class);
                LOGGER.info("Core " + REQUEST_MAPPING_SUCCESS_STR);
            } catch (Exception e) {
                LOGGER.info("Core Request Mapping failed :: "+ e.getMessage());
                return new ResponseEntity<>(
                        new NeoResponse(null,
                                new NeoException(null, e.getMessage(), null, null, e.getLocalizedMessage()), null),
                        HttpStatus.BAD_REQUEST);
            }

            LOGGER.info("Request Received :: "+ mapper.writeValueAsString(requestDto));

            NeoResponse response = rblService.createVirtualAccountNumber(requestDto);

            LOGGER.info("Response :: "+ mapper.writeValueAsString(response));

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(new NeoResponse(null,
                    new NeoException(null, e.getMessage(), null, null, e.getLocalizedMessage()), null),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/iciciUpiNotify", consumes = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody
    void iciciUpiCollect(HttpServletRequest httpreq, @RequestBody String request) throws Exception {
        LOGGER.info("Entering ICICI UPI Notify api...");
        DynamicVpaNotifyRequest notifyRequest = new DynamicVpaNotifyRequest();
        NeoResponse coreResp = new NeoResponse();
        UpiCollectRequest upiCollectRequestModel = null;
        UpiCollectRequest collectRequest=null;
        String referenceNum = "";
        String result = "False";
        String rejectReason = "";
        String decryptedRequest = "";

        LOGGER.info("ICICI UPI JSON :: "+ request);

        try {
            referenceNum = TxnIdGenerator.getReferenceNumber();
            try {
                decryptedRequest = encryptionDecryption.rsaDecrypt(Base64.getDecoder().decode(request), IciciPropertyConstants.ICICI_M2P_4096KEY);
                notifyRequest = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .readValue(decryptedRequest, DynamicVpaNotifyRequest.class);
                LOGGER.info(REQUEST_MAPPING_SUCCESS_STR +" :: "+ mapper.writeValueAsString(notifyRequest));
            } catch (Exception e) {
                LOGGER.info("Request Mapping failed::"+ e.getMessage());
                throw e;
            }

            upiCollectRequestModel = dozerBeanMapper.map(notifyRequest, UpiCollectRequest.class);
            upiCollectRequestModel.setAmount(notifyRequest.getPayerAmount());
            upiCollectRequestModel.setTransferUniqueNumber(notifyRequest.getBankRRN());
            upiCollectRequestModel.setCustomerCode(notifyRequest.getMerchantTranId());
            upiCollectRequestModel.setTransferType(UPICOLLECT_STR);
            upiCollectRequestModel.setRemitterMobile(notifyRequest.getPayerMobile());
            upiCollectRequestModel.setDescription(notifyRequest.getTerminalId());
            upiCollectRequestModel.setRemitterFullName(notifyRequest.getPayerName());
            upiCollectRequestModel.setPayerVpa(notifyRequest.getPayerVA());
            upiCollectRequestModel.setTxnCompletionDt(notifyRequest.getTxnCompletionDate());
            upiCollectRequestModel.setTxnInitDt(notifyRequest.getTxnInitDate());
            upiCollectRequestModel.setApplication(ICICI_STR);
            collectRequest = upiCollectRequestDao.save(upiCollectRequestModel);

            LOGGER.info("Saving Request model");
            if (!notifyRequest.getTxnStatus().toUpperCase().contains("FAIL")) {
                UpicollectRequestDto requestDto = new UpicollectRequestDto();

                requestDto.setAmount(notifyRequest.getPayerAmount());
                requestDto.setTransferUniqueNumber(notifyRequest.getBankRRN());
                requestDto.setMerchantId(notifyRequest.getMerchantId());
                requestDto.setCustomerCode(notifyRequest.getMerchantTranId());
                requestDto.setMerchantTranId(notifyRequest.getMerchantTranId());
                requestDto.setTransferType(UPICOLLECT_STR);
                requestDto.setPayerVpa(notifyRequest.getPayerVA());
                requestDto.setRemitterMobile(notifyRequest.getPayerMobile());
                requestDto.setTerminalId(notifyRequest.getTerminalId());
                requestDto.setDescription("Mobile :: " + notifyRequest.getPayerMobile() + " :: Terminal :: " +
                        notifyRequest.getTerminalId());
                requestDto.setRemitterFullName(notifyRequest.getPayerName());
                requestDto.setTxnCompletionDt(notifyRequest.getTxnCompletionDate());
                requestDto.setTxnInitDt(notifyRequest.getTxnInitDate());
                requestDto.setSubMerchantId(notifyRequest.getSubMerchantId());
                requestDto.setTxnStatus(notifyRequest.getTxnStatus());
                requestDto.setM2pReferenceNumber(referenceNum);
                requestDto.setApplication(ICICI_STR);


                coreResp = postToCore(mapper.writeValueAsString(requestDto), coreBaseUrl + "notification-manager/upiCollectNotify");

                LOGGER.info("Core Response :: " + coreResp.getResult());

                if ("true".equalsIgnoreCase((String) coreResp.getResult())) {
                    result = "True";
                } else {
                    result = "False";
                    rejectReason = (String) coreResp.getResult();
                }
            }else{
                result = null;
                rejectReason = "Failure received from Bank";
            }
        } catch (Exception e) {
            LOGGER.info("Exception Occured:: "+ e.getMessage());
            rejectReason = e.getMessage();
        } finally {
            if(collectRequest!=null) {
                collectRequest.setResult(result);
                collectRequest.setReject_reason(rejectReason.substring(0, Math.min(255,rejectReason.length())));
                upiCollectRequestDao.save(collectRequest);
            }
        }
    }

    @PostMapping(value = "/iciciUpiValidate", consumes = MediaType.TEXT_XML_VALUE)
    public ResponseEntity<String> iciciUpiValidate(HttpServletRequest httpreq, @RequestBody String request)
            throws Exception {
        LOGGER.info("Entering ICICI UPI Validate api...");
        DynamicVpaValidateRequest validateRequest = new DynamicVpaValidateRequest();
        DynamicVpaValidateResponse validateResponse = new DynamicVpaValidateResponse();
        NeoResponse coreResp = new NeoResponse();
        StringWriter stringWriter = new StringWriter();
        String referenceNum = "";
        String merchantId = "";

        JAXBContext requestInJAXB = JAXBContext.newInstance(DynamicVpaValidateResponse.class);
        Marshaller responseMarshaller = requestInJAXB.createMarshaller();

        LOGGER.info("ICICI UPI Validate :: "+ request);

        merchantId = httpreq.getHeader("x-api-key");
        LOGGER.info("MerchantId:: "+ merchantId);

        if (StringUtils.isEmpty(merchantId)) {
            validateResponse.setActCode("1");
            validateResponse.setMessage(INVALID_STR);
            responseMarshaller.marshal(validateResponse, stringWriter);
            return new ResponseEntity<>(stringWriter.toString(), HttpStatus.OK);
        }

        try {
            try {
                JAXBContext jaxbResContext = JAXBContext.newInstance(DynamicVpaValidateRequest.class);
                Unmarshaller um = jaxbResContext.createUnmarshaller();

                StringReader decryptedResponsereader = new StringReader(request);
                validateRequest = (DynamicVpaValidateRequest) um.unmarshal(decryptedResponsereader);
                LOGGER.info(REQUEST_MAPPING_SUCCESS_STR);
            } catch (Exception e) {
                LOGGER.info("Request Mapping failed :: "+ e.getMessage());
                validateResponse.setActCode("1");
                validateResponse.setMessage(INVALID_STR);
                responseMarshaller.marshal(validateResponse, stringWriter);
                return new ResponseEntity<>(stringWriter.toString(), HttpStatus.OK);
            }

            EcollectRequestDto requestDto = new EcollectRequestDto();

            referenceNum = TxnIdGenerator.getReferenceNumber();
            requestDto.setEcollectCode(merchantId);
            requestDto.setCustomerCode(validateRequest.getSubscriberId());
            requestDto.setTransferUniqueNumber(validateRequest.getTxnId());
            requestDto.setTransferType(UPICOLLECT_STR);
            requestDto.setDescription(validateRequest.getSource());
            requestDto.setM2pReferenceNumber(referenceNum);
            requestDto.setApplication(ICICI_STR);

            coreResp = postToCore(mapper.writeValueAsString(requestDto), coreBaseUrl + "notification-manager/upiCollectValidate");

            LOGGER.info("Core Response :: " + coreResp.getResult().toString());

            Map<String, String> validateCoreResp = new ObjectMapper().readValue(coreResp.getResult().toString(), Map.class);

            if ("TRUE".equalsIgnoreCase(validateCoreResp.get("result"))) {
                validateResponse.setActCode("0");
                validateResponse.setMessage("VALID");
                validateResponse.setCustName(validateCoreResp.get("custName"));
                validateResponse.setTxnId(validateRequest.getTxnId());
            } else {
                validateResponse.setActCode("1");
                validateResponse.setMessage(INVALID_STR);
            }
            responseMarshaller.marshal(validateResponse, stringWriter);
            return new ResponseEntity<>(stringWriter.toString(), HttpStatus.OK);

        } catch (Exception e) {
            validateResponse.setActCode("1");
            validateResponse.setMessage(INVALID_STR);
            responseMarshaller.marshal(validateResponse, stringWriter);
            return new ResponseEntity<>(stringWriter.toString(), HttpStatus.OK);
        }
    }

    private NeoResponse postToCore(String request, String coreUrl) {
        String url = "";
        NeoResponse coreResp = new NeoResponse();
        ResponseEntity<String> resp;

        if (!coreUrl.isEmpty()) {
            url = coreUrl;
        } else {
            url = coreURL;
        }

        HttpHeaders headers= new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        LOGGER.info("Calling Core API..."+ url);
        HttpEntity<String> entity = new HttpEntity<>(request, headers);

        try {
            resp = restTemplate.exchange(url, HttpMethod.POST, entity, String.class, url);
        } catch (HttpStatusCodeException e) {
            resp = ResponseEntity.status(e.getRawStatusCode()).headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
        }
        LOGGER.info("Core Response ::: "+ resp.getBody());
        try {
            coreResp = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .readValue(resp.getBody(), NeoResponse.class);
            LOGGER.info("Core Response Mapping Success");
        } catch (Exception e) {
            LOGGER.info("Core Response Mapping failed");
            e.printStackTrace();
            return new NeoResponse("false", null, null);
        }
        return coreResp;
    }
    
    @RequestMapping(value = "/ecollectNotify", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<NeoResponse> ecollectNotify(@RequestBody EcollectRequestDto request) {
    	 
    	NeoResponse result = null;
    	
    	try {    		
    		result = virtualAccountService.eCollectNotify(request);
    		return new ResponseEntity<>(new NeoResponse(result, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch(Exception e) {
			return new ResponseEntity<>(new NeoResponse(null, new NeoException(e.getMessage(), e.getMessage(), null, null, null), null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
    
	@RequestMapping(value = "/ecollectValidate", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> ecollectValidate(@RequestBody EcollectRequestDto request) {

		NeoResponse result = null;
    	
    	try {    		
    		result = virtualAccountService.eCollectValidate(request);
    		
    		return new ResponseEntity<>(new NeoResponse(result, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch(Exception e) {
			return new ResponseEntity<>(new NeoResponse(null, new NeoException(e.getMessage(), e.getMessage(), null, null, null), null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
    @RequestMapping(value = "/yesNotify", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<YesEcollectResponseDto> yesEcollectNotifiy (HttpServletRequest httpReq, HttpServletResponse httpRes) {
    	
    		YesEcollectRequestDto dto = new YesEcollectRequestDto();
		
    		EcollectRequestDto request = new EcollectRequestDto();
    		YesEcollectResponseDto yesRes = new YesEcollectResponseDto();
    		
    		Boolean result = null;
    		String referenceNum = TxnIdGenerator.getReferenceNumber();
    	
    	try {
    		
    		String authHeader = httpReq.getHeader("Authorization");
    		authHeader = authHeader.replace("Basic ", "");
        	byte[] authBytes = Base64.getDecoder().decode(authHeader);
        	String authString = new String(authBytes);
       
        	NeoBusinessCustomField customfield = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.NOTIFY_ECOLLECT_AUTH, "YESBANK");
        	
        	if(customfield!=null && !customfield.getFieldValue().equals(EncryptionUtil.HmacSHA256(authString,yesEcollectSecret))) {
        		yesRes.getNotifyResult().setResult("Unauthorized");
        		return new ResponseEntity<>(yesRes, HttpStatus.UNAUTHORIZED);
        	}
        	
        	String inputData = httpReq.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        	
        	LOGGER.info("Ecollect Request  ::"+inputData);
			
			try {
				dto = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).readValue(inputData, YesEcollectRequestDto.class);
				LOGGER.info("Mapping success");
			} catch(Exception e) {
				LOGGER.info("Mapping failed");
			}
			
			EcollectData data = dto.getNotify();
        	
    		request.setEcollectCode(data.getBene_account_no());
    		request.setAmount(data.getTransfer_amt());
    		request.setRemitterAccountNumber(data.getRmtr_account_no());
    		request.setRemitterFullName(data.getRmtr_full_name());
    		request.setTransferUniqueNumber(data.getTransfer_unique_no());
    		request.setCustomerCode(data.getCustomer_code());
    		
    		request.setVirtualAccountNo(data.getBene_account_no());
    		request.setTransferType(data.getTransfer_type());
    		request.setRemitterAccountNumber(data.getRmtr_account_no());
    		request.setRemitterAccountType(data.getRmtr_account_type());
    		request.setRemitterIFSCCode(data.getRmtr_account_ifsc());
    		request.setRemitterFullName(data.getRmtr_full_name());
    		request.setNote(data.getRmtr_to_bene_note());
    		request.setCreditedAt(data.getCredited_at());
    		request.setCreditAccountNo(data.getCredit_acct_no());
    		request.setApplication("YES");
    		request.setStatus(data.getStatus());
    		request.setM2pReferenceNumber(referenceNum);
    		
    		NeoResponse neoResponse = virtualAccountService.eCollectNotify(request);
    		
    		if(neoResponse.getResult()!=null && neoResponse.getResult() instanceof Boolean) {
    			result = Boolean.valueOf(neoResponse.getResult().toString());
    		} else {
    			result = Boolean.FALSE;
    		}
     		
			if (result) {
				yesRes.getNotifyResult().setResult("ok");
			} else {
				yesRes.getNotifyResult().setResult("retry");
			}
    		
			return new ResponseEntity<>(yesRes, HttpStatus.OK);
		} catch (NeoException e) {
			LOGGER.info("NeoException ::"+e.getDetailMessage());
			return new ResponseEntity<>(new YesEcollectResponseDto(e.getShortMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch(Exception e) {
			LOGGER.info("Exception Occured ", e);
			return new ResponseEntity<>(new YesEcollectResponseDto(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
    
    
    @RequestMapping(value = "/yesValidate", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<YesEcollectValidateRepsonseDto> yesEcollectValidate(HttpServletRequest httpReq, HttpServletResponse httpRes) {
    	
    		YesEcollectRequestDto dto = new YesEcollectRequestDto();
		
    		EcollectRequestDto request = new EcollectRequestDto();
    		YesEcollectValidateRepsonseDto yesRes = new YesEcollectValidateRepsonseDto();
    		
    		Boolean result = null;
    		String referenceNum = TxnIdGenerator.getReferenceNumber();
    	
    	try {
    		
    		String authHeader = httpReq.getHeader("Authorization");
    		authHeader = authHeader.replace("Basic ", "");
        	byte[] authBytes = Base64.getDecoder().decode(authHeader);
        	String authString = new String(authBytes);

        	NeoBusinessCustomField customfield = customFieldService.findByFieldNameAndTenant(NeoCustomFieldConstants.NOTIFY_ECOLLECT_AUTH, "YESBANK");
        	
        	if(customfield!=null && !customfield.getFieldValue().equals(EncryptionUtil.HmacSHA256(authString,yesEcollectSecret))) {
        		yesRes.getValidateResponse().setResult("Unauthorized");
        		return new ResponseEntity<>(yesRes, HttpStatus.UNAUTHORIZED);
        	}
        	
        	String inputData = httpReq.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        	
        	LOGGER.info("Ecollect Request  ::"+inputData);
			
			try {
				dto = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).readValue(inputData, YesEcollectRequestDto.class);
				LOGGER.info("Mapping success");
			} catch(Exception e) {
				LOGGER.info("Mapping failed :: Bad Request");
				yesRes.getValidateResponse().setResult("Bad Request");
				return new ResponseEntity<>(yesRes, HttpStatus.BAD_REQUEST);
			}
			
			EcollectData data = dto.getValidate();
        	
    		request.setEcollectCode(data.getBene_account_no());
    		request.setAmount(data.getTransfer_amt());
    		request.setRemitterAccountNumber(data.getRmtr_account_no());
    		request.setRemitterFullName(data.getRmtr_full_name());
    		request.setTransferUniqueNumber(data.getTransfer_unique_no());
    		request.setCustomerCode(data.getCustomer_code());
    		
    		request.setVirtualAccountNo(data.getBene_account_no());
    		request.setTransferType(data.getTransfer_type());
    		request.setRemitterAccountNumber(data.getRmtr_account_no());
    		request.setRemitterAccountType(data.getRmtr_account_type());
    		request.setRemitterIFSCCode(data.getRmtr_account_ifsc());
    		request.setRemitterFullName(data.getRmtr_full_name());
    		request.setNote(data.getRmtr_to_bene_note());
    		request.setCreditedAt(data.getCredited_at());
    		request.setCreditAccountNo(data.getCredit_acct_no());
    		request.setApplication("YES");
    		request.setM2pReferenceNumber(referenceNum);
    		
    		NeoResponse neoResponse = virtualAccountService.eCollectValidate(request);
    		
    		if(neoResponse.getResult()!=null && neoResponse.getResult() instanceof Boolean) {
    			result = Boolean.valueOf(neoResponse.getResult().toString());
    		} else {
    			result = Boolean.FALSE;
    		}
    		
			if (result) {
				yesRes.getValidateResponse().setDecision("pass");
			} else {
				yesRes.getValidateResponse().setDecision("reject");
				yesRes.getValidateResponse().setReject_reason("invalid request");
			}
    		
			return new ResponseEntity<>(yesRes, HttpStatus.OK);
		} catch (NeoException e) {
			LOGGER.info("NeoException ::"+e.getDetailMessage());
			return new ResponseEntity<>(new YesEcollectValidateRepsonseDto(e.getShortMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch(Exception e) {
			LOGGER.info("Exception Occured", e);
			return new ResponseEntity<>(new YesEcollectValidateRepsonseDto(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

}
