package com.neo.aggregator.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.NeoResponse;

@Controller
@RequestMapping("/cache-manager")
public class CacheManagerController {
	private Logger logger = LoggerFactory.getLogger(CacheManagerController.class);

	@Autowired
	private CacheManager cacheManager;

	@RequestMapping(value = "/clear/{cacheName}", method = RequestMethod.POST)
	public @ResponseBody NeoResponse getServiceRequest(@PathVariable("cacheName") String cacheName) {
		NeoResponse neoResponse = null;
		try {
			logger.info("Clear Cache ::: " + cacheName);
			Cache cache = cacheManager.getCache(cacheName);
			cache.clear();
			neoResponse = new NeoResponse(true, null, null);

		} catch (Exception e) {
			logger.info("Error exception message :" + e.getMessage());
			neoResponse = new NeoResponse(null, new NeoException(e.getMessage(), e.getLocalizedMessage(), null, null, cacheName),
					null);
		}
		return neoResponse;
	}
}