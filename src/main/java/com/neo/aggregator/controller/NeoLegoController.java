package com.neo.aggregator.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.NeoResponse;
import com.neo.aggregator.dto.RegistrationRequestV2Dto;
import com.neo.aggregator.dto.RegistrationResponseDto;
import com.neo.aggregator.dto.neolego.PanDataDto;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.service.NeoLegoService;
import com.neo.aggregator.serviceimpl.NeoLegoServiceImpl;

@Controller
@RequestMapping("/neo-lego")
public class NeoLegoController {

	@Autowired
	private NeoLegoServiceImpl neolegoservice;

	@Autowired
	private NeoLegoService service;

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(NeoLegoController.class);

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/validate/pan", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> validatePAN(@RequestBody String request) {

		logger.info("Request Received ::" + request);

		ObjectMapper responseMapper = new ObjectMapper();
		Map<String, String> outData = null;
		PanDataDto panData = new PanDataDto();
		try {

			panData = neolegoservice.validatePan(request);

			try {
				ObjectMapper mapper = new ObjectMapper();
				responseMapper.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);
				responseMapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
				String panDataStr = responseMapper.writeValueAsString(panData);
				outData = mapper.readValue(panDataStr, Map.class);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
				return new ResponseEntity<NeoResponse>(new NeoResponse(null,
						new NeoException("", e.getLocalizedMessage(), "", "", e.getMessage()), null),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}

			logger.info("Response Sent ::" + outData);

			return new ResponseEntity<NeoResponse>(new NeoResponse(outData, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);

		} finally {
			panData = null;
			responseMapper = null;
			outData = null;
		}
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> register(@RequestBody RegistrationRequestV2Dto request) {

		try {

			logger.info("Request Received Register:: " + request.toString());
			RegistrationResponseDto response = service.register(request);

			logger.info("Response Sent :: " + response.toString());
			return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/addProduct", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> addProduct(@RequestBody RegistrationRequestV2Dto request) {

		try {

			logger.info("Request Received addProduct:: " + request.toString());

			RegistrationResponseDto response = service.addProduct(request);

			logger.info("Response Sent :: " + response.toString());

			return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/registerWithCore", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<NeoResponse> registerWithCore(@RequestBody RegistrationRequestV2Dto request) {

		try {

			logger.info("Request Received Register:: " + request.toString());
			RegistrationResponseDto response = service.registerWithCore(request);

			logger.info("Response Sent :: " + response.toString());
			return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
