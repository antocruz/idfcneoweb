package com.neo.aggregator.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.bank.rbl.response.KycAadhaarXmlResponseDto;
import com.neo.aggregator.dto.*;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.BusinessEntity;
import com.neo.aggregator.service.RblEbsService;
import com.neo.aggregator.serviceimpl.RblEsbServiceImpl;
import com.neo.aggregator.utility.TenantContextHolder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/rbl")
public class RblEbsController {

    private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(RblEbsController.class);

    @Autowired
    @Lazy
    private RblEsbServiceImpl serviceImpl;

    @Autowired
    private RblEbsService service;

    @Autowired
    RestTemplate restTemplate;

    @Value("${neo.agg.core.url}")
    private String coreUrl;

    @PostMapping(value = "/okyc/dataPush", consumes = "application/json")
    public ResponseEntity<NeoResponse> okycDataPush(@RequestBody RegistrationRequestV2Dto request) {

        try {
            logger.info("Tenant :: " + TenantContextHolder.getNeoTenant());

            logger.info("Request Received OKYC Data Push:: " + request);

            KycAadhaarXmlResponseDto response = service.okycDataPush(request);

            logger.info("Response Sent ::" + response.toString());
            return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
        } catch (NeoException e) {
            return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/Customer/KYC/Onboard", consumes = "application/json")
    public ResponseEntity<NeoResponse> productRegistration(@RequestBody RegistrationRequestV2Dto request) {
        try {
            logger.info("Tenant :: " + TenantContextHolder.getNeoTenant());

            logger.info("Request Received for Product Registration :: " + request);

            RegistrationResponseDto response = service.productRegistration(request, request.getBioeKycReqType());

            logger.info("Response Sent ::" + response.toString());

            return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
        } catch (NeoException e) {
            return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/get/details", consumes = "application/json")
    public ResponseEntity<NeoResponse> getDetails(@RequestBody CustomerGetDetailsRequestDto request) {
        try {
            logger.info("Tenant :: " + TenantContextHolder.getNeoTenant());

            logger.info("Request Received for Get Details :: " + request);

            CustomerGetDetailsResponseDto response = service.getDetails(request);

            logger.info("Response Sent ::" + response.toString());

            return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
        } catch (NeoException e) {
            return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/kyc/dataUpdate", consumes = "application/json")
    public ResponseEntity<NeoResponse> biometricDataUpdate(@RequestBody KYCBioRequestDto request)
            throws ParseException, JsonProcessingException {
        BusinessEntity businessEntity = new BusinessEntity();
        String firstName = "";
        String lastName = "";
        try {
            logger.info("Tenant :: " + TenantContextHolder.getNeoTenant());

            logger.info("Request Received Bio Data Update:: " + request);

            KYCBioResponseDto response = service.fetchDemographicData(request);

            if ("1".equals(response.getStatus())) {
                // update the data in tenant db
                String sDate1 = response.getDob();
                SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyyy");
                Date date1 = formatter1.parse(sDate1);

                int size = response.getName().split(" ").length;
                if(size > 1) {
                    String responseName = response.getName();
                    int lastSpace = responseName.lastIndexOf(" ");
                    firstName = responseName.substring(0, lastSpace);
                    lastName = responseName.substring(lastSpace+1);
                } else {
                    firstName = response.getName();
                }
                businessEntity.setEntityId(request.getEntityId());
                businessEntity.setAddress(response.getAddressDto().getAddress1() + " " +
                        response.getAddressDto().getAddress2());
                businessEntity.setAddress2(response.getAddressDto().getAddress3());
                businessEntity.setSpecialDate(date1);
                businessEntity.setCity(response.getAddressDto().getCity());
                businessEntity.setState(response.getAddressDto().getState());
                businessEntity.setCountry(response.getAddressDto().getCountry());
                businessEntity.setPincode(Integer.parseInt(response.getAddressDto().getPincode()));
                businessEntity.setGender(response.getGender());
                businessEntity.setTitle(response.getTitle());
                businessEntity.setFirstName(firstName);
                businessEntity.setLastName(lastName);
                businessEntity.setIdNumber("");
                businessEntity.setIdType("");

                NeoResponse neoResponse = postToCore(businessEntity, TenantContextHolder.getNeoTenant());

                if (neoResponse.getResult() == Boolean.TRUE) {
                    logger.info("Data Updated Successfully!!");
                } else {
                    logger.info("Data Updation Failed!!");
                }
            }
            logger.info("Response Sent ::" + response.toString());
            return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
        } catch (NeoException e) {
            return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private NeoResponse postToCore(BusinessEntity request, String tenant) throws JsonProcessingException {

        NeoResponse coreResp = new NeoResponse();
        ResponseEntity<String> resp;
        String url = coreUrl + "business-entity-manager/updateentity";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("TENANT", tenant);

        logger.info("Calling Core API..." + url);
        HttpEntity<String> entity = null;
        entity = new HttpEntity<String>(new ObjectMapper().writeValueAsString(request), headers);
        try {
            resp = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        } catch (HttpStatusCodeException e) {
            resp = ResponseEntity.status(e.getRawStatusCode()).headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
        }

        logger.info("Core Response ::: " + resp.getBody());

        try {
            coreResp = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .readValue(resp.getBody(), NeoResponse.class);
            logger.info("Core Response Mapping Success");
        } catch (Exception e) {
            logger.info("Core Response Mapping failed");
            e.printStackTrace();
            return new NeoResponse("false", null, null);
        }

        return coreResp;
    }

	@PostMapping(value = "/virtual-account/vaFailureRetry", consumes = "application/json")
	public ResponseEntity<NeoResponse> vaFailureRetry() {
		try {
			String response = serviceImpl.vaFailureRetry();

			logger.info(response);
			return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			logger.error("Exception Occured in RBL VA Retry :: ", e);
			return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/recon", consumes = "application/json")
	public ResponseEntity<NeoResponse> rblRecon() {
		try {
			String response = serviceImpl.rblRecon();

			logger.info("Response Sent for Recon :: " + response);
			return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			logger.error("Exception Occured in RBL Recon :: ", e);
			return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
