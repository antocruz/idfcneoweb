package com.neo.aggregator.controller;

import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo.aggregator.config.ClientInit;
import com.neo.aggregator.dto.AuthorizationRequestDto;
import com.neo.aggregator.dto.AuthorizationResponseDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.dto.NeoResponse;
import com.neo.aggregator.dto.ReversalRequestDto;
import com.neo.aggregator.dto.ReversalResponseDto;
import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.iso8583.MessageFactory;
import com.neo.aggregator.jreactive8583.client.Iso8583Client;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.service.IsoService;
import com.neo.aggregator.utility.Iso8583;

@RestController
@RequestMapping("/iso")
public class IsoController {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(IsoController.class);

	@Autowired
	private ClientInit clientInit;

	@Autowired
	@Qualifier("isoConcurrentMap")
	private ConcurrentHashMap<String, Iso8583> isoHashMap;

	@Value("${switch.network}")
	private String switchNetwork;

	@Autowired
	private IsoService service;

	@RequestMapping(value = "/issuer/operation", method = RequestMethod.GET)
	@ResponseBody
	public String issuerSignin(@RequestParam("type") String messageType, @RequestParam("tenant") String tenant) {

		try {
			Iso8583Client<IsoMessage> clientISoClient = clientInit.getPosIssuerClientMap().get(tenant);
			MessageFactory<IsoMessage> isoMessageFactory = clientISoClient.getIsoMessageFactory();
			clientISoClient.send(isoMessageFactory.newVisaNmmMessage(0x800, messageType));
		} catch (InterruptedException e) {
			e.printStackTrace();
			return "Failed";
		}
		return "Success";

	}

	@RequestMapping(value = "/authorize/iso", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<NeoResponse> authorizeIsoTransaction(@RequestBody Iso8583 request, String tenant) {
		
		logger.info("Request Input received ::" + request.toString());

		try {
			Iso8583 response = service.authorizeTransaction(request, tenant);
			return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			return new ResponseEntity<NeoResponse>(
					new NeoResponse(null,
							new NeoException(e.getMessage(), e.getMessage(), null, null, "Authorization Failed"), null),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/authorize", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<NeoResponse> externalAuthorize(@RequestBody AuthorizationRequestDto request) {

		ObjectMapper mapper = new ObjectMapper();

		try {
			logger.info("Request Input Json :: " + mapper.writeValueAsString(request));
			AuthorizationResponseDto response = service.authorizeTransaction(request);
			logger.info("Response Output Json :: " + mapper.writeValueAsString(response));
			return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			return new ResponseEntity<NeoResponse>(
					new NeoResponse(null,
							new NeoException(e.getMessage(), e.getMessage(), null, null, "Authorization Failed"), null),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/reversal", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<NeoResponse> externalReversal(@RequestBody ReversalRequestDto request) {

		ObjectMapper mapper = new ObjectMapper();

		try {
			logger.info("Request Input Json :: " + mapper.writeValueAsString(request));
			ReversalResponseDto response = service.reversalTransaction(request);
			logger.info("Response Output Json :: " + mapper.writeValueAsString(response));
			return new ResponseEntity<NeoResponse>(new NeoResponse(response, null, null), HttpStatus.OK);
		} catch (NeoException e) {
			return new ResponseEntity<NeoResponse>(new NeoResponse(null, e, null), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			return new ResponseEntity<NeoResponse>(
					new NeoResponse(null,
							new NeoException(e.getMessage(), e.getMessage(), null, null, "Authorization Failed"), null),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}
