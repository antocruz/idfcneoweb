package com.neo.aggregator.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.ToString;

@Entity
@Table(name = "business_entity")
@ToString
public class BusinessEntity extends AbstractEntity implements Serializable {

	private Logger logger = LoggerFactory.getLogger(BusinessEntity.class);

	private static final long serialVersionUID = -907831608042276967L;
	private String entityId;

	private String sorEntityId;

	private String emailAddress;

	private String idType;

	private String idNumber;

	private String countryOfIssue;

	private String address2;

	private String title;

	private String gender;

	private String firstName;

	private String lastName;

	private String privateKey;

	private byte[] publicKey;

	private String description;

	private String address;

	private String city;

	private String state;

	private String country;

	private Integer pincode;

	private String contactNo;

	private String authCode;

	private String appGuid;

	private Date specialDate;

	private String yapCode;

	private Boolean isDependent;

	private BusinessEntity parent;

	//private String product;

	private String entityType;

	private String status;

	private String kit;

	private Boolean isBlocked = false;

	private String connectionUrl;

	private Boolean hasCustomer;

	private String encryptedYapcode;

	private String branch;

	@Column(name = "entity_id")
	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	@Column
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Column
	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	@Column
	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {

		if (this.idType != null && idNumber != null && this.idType.equalsIgnoreCase("aadhaar")) {
			this.idNumber = StringUtils.overlay(idNumber, StringUtils.repeat("X", 6), 0, idNumber.length() - 4);
		} else {
			this.idNumber = idNumber;
		}
	}

	@Column
	public String getCountryOfIssue() {
		return countryOfIssue;
	}

	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}

	@Column
	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	@Column
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Column
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "city")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "state")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "country")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "pincode")
	public Integer getPincode() {
		return pincode;
	}

	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}

	@Column(name = "contact_no")
	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	@Column(name = "auth_code")
	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	@Column(name = "app_guid")
	public String getAppGuid() {
		return appGuid;
	}

	public void setAppGuid(String appGuid) {
		this.appGuid = appGuid;
	}

	@Column(name = "special_date")
	public Date getSpecialDate() {
		return specialDate;
	}

	public void setSpecialDate(Date specialDate) {
		this.specialDate = specialDate;
	}

	@Transient
	public String getYapCode() {
		return yapCode;
	}

	public void setYapCode(String yapCode) {
		this.yapCode = yapCode;
		/*
		 * try { if(yapCode != null) { String hashedYapcode =
		 * YapcodeHashService.getSaltedHash(yapCode.toString());
		 * setEncryptedYapcode(hashedYapcode); } } catch (Exception e){
		 * logger.info("Error : ", e); }
		 */
	}

	@Column(name = "dependent")
	public Boolean getIsDependent() {
		return isDependent;
	}

	public void setIsDependent(Boolean isDependent) {
		this.isDependent = isDependent;
	}

	@ManyToOne
	@JoinColumn(name = "parent_fkey")
	public BusinessEntity getParent() {
		return parent;
	}

	public void setParent(BusinessEntity parent) {
		this.parent = parent;
	}

	/*
	 * public String getProduct() { return product; }
	 * 
	 * public void setProduct(String product) { this.product = product; }
	 */

	@Column(name = "entity_type")
	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getKit() {
		return kit;
	}

	public void setKit(String kit) {
		this.kit = kit;
	}

	@Column
	public Boolean getIsBlocked() {
		return isBlocked;
	}

	public void setIsBlocked(Boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	@Column
	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	@Column
	@Lob
	public byte[] getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(byte[] publicKey) {
		this.publicKey = publicKey;
	}

	@Column
	public String getConnectionUrl() {
		return connectionUrl;
	}

	public void setConnectionUrl(String connectionUrl) {
		this.connectionUrl = connectionUrl;
	}

	@Column
	public Boolean getHasCustomer() {
		return hasCustomer;
	}

	public void setHasCustomer(Boolean hasCustomer) {
		this.hasCustomer = hasCustomer;
	}

	@Column(name = "enc_yapcode")
	public String getEncryptedYapcode() {
		return encryptedYapcode;
	}

	public void setEncryptedYapcode(String encryptedYapcode) {
		this.encryptedYapcode = encryptedYapcode;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	@Column(name = "sor_entity_id")
	public String getSorEntityId() {
		return sorEntityId;
	}

	public void setSorEntityId(String sorEntityId) {
		this.sorEntityId = sorEntityId;
	}

}
