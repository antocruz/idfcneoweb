package com.neo.aggregator.model;

import java.sql.Timestamp;
import java.util.Calendar;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class PersistenceListener {

	@PrePersist
	void onPreCreate(Object entity) {
		AbstractEntity baseEntity = (AbstractEntity) entity;
		baseEntity.setCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
		baseEntity.setChanged(new Timestamp(Calendar.getInstance().getTime().getTime()));
		String createdBy = "neo_aggregator";
		baseEntity.setCreator(createdBy);
		baseEntity.setChanger(createdBy);
		if (baseEntity.getDeleted() == null) {
			baseEntity.setDeleted(false);
		}

	}

	@PreUpdate
	void onPreUpdate(Object entity) {
		AbstractEntity baseEntity = (AbstractEntity) entity;
		baseEntity.setChanged(new Timestamp(Calendar.getInstance().getTime().getTime()));
		String changedBy = "system";
		baseEntity.setChanger(changedBy);
		if (baseEntity.getDeleted() == null) {
			baseEntity.setDeleted(false);
		}
	}
}
