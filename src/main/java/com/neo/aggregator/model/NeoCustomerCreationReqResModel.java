package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "neo_customer_creation_req_res")
@Getter
@Setter
public class NeoCustomerCreationReqResModel extends AbstractEntity {
	
	@Column
	private String requestId;
	
	@Column
	private String entityId;
	
	@Column
	private String bankId;
	
	@Column
	private String tenant;
	
	@Column
	private String firstName;
	
	@Column
	private String middleName;
	
	@Column
	private String lastName;
	
	@Column
	private String solId;
	
	@Column
	private String taxDeductionTable;
	
	@Column
	private Boolean isNewCustomer;
	
	@Column
	private Boolean isNRECustomer;
	
	@Column
	private Boolean isMinor;
	
	@Column
	private String responseDescription;
	
	@Column
	private String status;
	
	@Column
	private String cifNumber;

}
