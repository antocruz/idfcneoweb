package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "neo_audit")
@Getter
@Setter
public class NeoAudit extends AbstractEntity {
	
	@Column
	private String entityId;
	
	@Column
	private String bankId;
	
	@Column
	private String partnerName;
	
	@Column
	private String api;
	
	@Column
	private String description;
	
	@Column
	private Boolean status;

}
