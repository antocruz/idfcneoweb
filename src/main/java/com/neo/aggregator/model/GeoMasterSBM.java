package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "geo_master_sbm")
@Getter
@Setter
public class GeoMasterSBM extends AbstractEntity {

	@Column
	private String country;

	@Column
	private String iso2;

	@Column
	private String iso3;

	@Column
	private String state;

	@Column
	private String state_code;

	@Column
	private String district;

	@Column
	private String district_code;

	@Column
	private String taluk;

	@Column
	private String pincode;

	@Column
	private String postoffice_name;

	@Column
	private String suboffice_name;

	@Column
	private String headoffice_name;
	
	@Column
	private String latitude;

	@Column
	private String longitude;
}
