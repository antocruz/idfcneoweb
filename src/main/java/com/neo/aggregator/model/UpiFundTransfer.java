package com.neo.aggregator.model;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "upi_fund_transfer")
@Getter
@Setter
public class UpiFundTransfer extends AbstractEntity{
	String merchantId;
    String orderNo;
    String txnNote;
    String amount;
    String currency;
    String paymentType;
    String txnType;
    String mcc;
    String expTime;
    String payeeAccNo;
    String payeeIfsc;
    String payeeAadhar;
    String payeeMobNo;
    String payeeVpa;
    String subMerchantId;
    String whiteListedAcc;
    String payeeMMid;
    String refUrl;
    String transferType;
    String payeeName;
    String payeeAddress;
    String payeeEmail;
    String payerAccNo;
    String payerIfsc;
    String payerMbNo;
    String payeeVpaType;
    String yblTxnId;
	String date;
	String statusCode;
	String responseCode;
	String approvalNum;
	String payerVpa;
	String npciTxnId;
	String custRefId;
	String payerAccountName;
	String errorCode;
	String responseErrorCode;
	String errorMsg;
	String txnStatus;
}
