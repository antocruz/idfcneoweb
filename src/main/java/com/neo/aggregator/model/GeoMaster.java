package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "geo_master",  schema="neo_aggregator", catalog="neo_aggregator")
@JsonIgnoreProperties("isBlocked")
@Getter
@Setter
public class GeoMaster extends AbstractEntity {

	@Column
	private String countryId;
	
	@Column
	private String districtId;
	
	@Column
	private String stateId;
	
	@Column
	private String pincode;
	
	@Column
	private String cityId;
	
	@Column
	private String cityName;
	
}
