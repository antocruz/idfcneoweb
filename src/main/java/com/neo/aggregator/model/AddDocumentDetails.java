package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "add_document_details")
@JsonIgnoreProperties("isBlocked")
@Getter
@Setter
public class AddDocumentDetails extends AbstractEntity {

	@Column
	private String entityId;

	@Column
	private String cnvId;

	@Column
	private String timestamp;
	
	@Column
	private String custLeadId;
	
	@Column
	private String docSbCtgry;
	
	@Column
	private String docNm;
	
	@Column
	private String docIndx;
}
