package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "notification_alert_model",uniqueConstraints={
	    @UniqueConstraint(columnNames = {"errorCode", "business"})
	})
@Getter
@Setter
public class NotificationAlertModel extends AbstractEntity {
	
	private String errorCode;
	
	@Column(columnDefinition = "boolean default false")
	private Boolean alertEnabled;
	
	private String subject;
	
	private String toAddress;
	
	private String toMobile;
	
	private String template;
	
	private String business;

}
