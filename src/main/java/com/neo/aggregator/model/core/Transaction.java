package com.neo.aggregator.model.core;

import com.neo.aggregator.model.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "transaction")
@Getter
@Setter
public class Transaction extends AbstractEntity {

    private Double amount;
    
    private Double convertedAmount;

    private String description;

    private Long terminal;

    private Long txRef;

    private Integer transactionStatus;

    private String bankTid;

    private Integer txnType;

    private Integer txnOrigin;

    private Double fromWalletBalance;

    private Double toWalletBalance;

    private Long fromWallet;

    private Long toWallet;
    
    private Long wallet;

    private String transactionFees;

    private String otherPartyName;

    private String otherPartyId;

    private Long combinedTxnRef;

    private String otherPartyBusiness;
    
    private String externalTransactionId;
    
    private String billRefNo;
    
    private String sorTxnId;
    
    private String mcc;
    
    private String acquirerId;
    
    private String kitNo;
    
    private String rrn;
    
    private String authCode;
    
    private Integer networkType;
    
    private String stan;
    
	private String acquirerCountryCode;
	
	private String transactionCurrencyCode;
	
	private String limitCurrencyCode;
	
	private Long channel_fkey;
	
	private Integer subTxnType;

    @Column(name="rrn")
    public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	@Column(name="auth_code")
	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	@Column(name="network_type")
	public Integer getNetworkType() {
		return networkType;
	}

	public void setNetworkType(Integer networkType) {
		this.networkType = networkType;
	}

	@Column(name="sor_txn_id")
    public String getSorTxnId() {
    return sorTxnId;
    }

    public void setSorTxnId(String sorTxnId) {
    this.sorTxnId = sorTxnId;
    }

    @Column
    public Integer getTxnType() {
        return txnType;
    }

    public void setTxnType(Integer txnType) {
        this.txnType = txnType;
    }

    @Column
    public Integer getTxnOrigin() {
        return txnOrigin;
    }

    public void setTxnOrigin(Integer txnOrigin) {
        this.txnOrigin = txnOrigin;
    }


    @Column(name = "amount")
    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "tx_ref")
    public Long getTxRef() {
        return txRef;
    }

    public void setTxRef(Long txRef) {
        this.txRef = txRef;
    }

    @Column(name = "tx_status")
    public Integer getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(Integer transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    @Column(name = "bank_tid")
    public String getBankTid() {
        return bankTid;
    }

    public void setBankTid(String bankTid) {
        this.bankTid = bankTid;
    }

    @Column(name = "from_wallet")
    public Long getFromWallet() {
    	return fromWallet;
    }

    public void setFromWallet(Long fromWallet) {
        this.fromWallet = fromWallet;
    }

    @Column(name = "to_wallet")
    public Long getToWallet() {
        return toWallet;
    }

    public void setToWallet(Long toWallet) {
        this.toWallet = toWallet;
    }

    @Column(name = "terminal_id")
    public Long getTerminal() {
        return terminal;
    }

    public void setTerminal(Long terminal) {
        this.terminal = terminal;
    }

    @Column
    public String getOtherPartyName() {
        return otherPartyName;
    }

    public void setOtherPartyName(String otherPartyName) {
        this.otherPartyName = otherPartyName;
    }

    @Column
    public String getOtherPartyId() {
        return otherPartyId;
    }

    public void setOtherPartyId(String otherPartyId) {
        this.otherPartyId = otherPartyId;
    }

    @Column
    public Long getCombinedTxnRef() {
        return combinedTxnRef;
    }

    public void setCombinedTxnRef(Long combinedTxnRef) {
        this.combinedTxnRef = combinedTxnRef;
    }

    @Column
    public Double getFromWalletBalance() {
        return fromWalletBalance;
    }

    public void setFromWalletBalance(Double fromWalletBalance) {
        this.fromWalletBalance = fromWalletBalance;
    }

    @Column
    public Double getToWalletBalance() {
        return toWalletBalance;
    }

    public void setToWalletBalance(Double toWalletBalance) {
        this.toWalletBalance = toWalletBalance;
    }

    @Column
    public String getTransactionFees() {
        return transactionFees;
    }

    public void setTransactionFees(String transactionFees) {
        this.transactionFees = transactionFees;
    }

    @Column
    public String getOtherPartyBusiness() {
        return otherPartyBusiness;
    }

    public void setOtherPartyBusiness(String otherPartyBusiness) {
        this.otherPartyBusiness = otherPartyBusiness;
    }
    
    @Column(name = "ext_txn_id")
    public String getExternalTransactionId() {
        return externalTransactionId;
    }

    public void setExternalTransactionId(String externalTransactionId) {
        this.externalTransactionId = externalTransactionId;
    }

    @Column(name="bill_ref_no")
	public String getBillRefNo() {
		return billRefNo;
	}
	

	public void setBillRefNo(String billRefNo) {
		this.billRefNo = billRefNo;
	}

	@Column
	public Double getConvertedAmount() {
		return convertedAmount;
	}

	public void setConvertedAmount(Double convertedAmount) {
		this.convertedAmount = convertedAmount;
	}

    @Column(name = "mcc")
	public String getMcc() {
		return mcc;
	}

	public void setMcc(String mcc) {
		this.mcc = mcc;
	}

    @Column(name = "acquirer_id")
	public String getAcquirerId() {
		return acquirerId;
	}

	public void setAcquirerId(String acquirerId) {
		this.acquirerId = acquirerId;
	}

    @Column(name = "kit_no")
	public String getKitNo() {
		return kitNo;
	}

	public void setKitNo(String kitNo) {
		this.kitNo = kitNo;
	}

	@Column(name = "stan")
	public String getStan() {
		return stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	@Column(name="acquirer_country_code")
	public String getAcquirerCountryCode() {
		return acquirerCountryCode;
	}

	public void setAcquirerCountryCode(String acquirerCountryCode) {
		this.acquirerCountryCode = acquirerCountryCode;
	}

	@Column(name="transaction_currency_code")
	public String getTransactionCurrencyCode() {
		return transactionCurrencyCode;
	}

	public void setTransactionCurrencyCode(String transactionCurrencyCode) {
		this.transactionCurrencyCode = transactionCurrencyCode;
	}

	@Column(name="limit_currency_code")
	public String getLimitCurrencyCode() {
		return limitCurrencyCode;
	}

	public void setLimitCurrencyCode(String limitCurrencyCode) {
		this.limitCurrencyCode = limitCurrencyCode;
	}

	@Column(name="channel_fkey")
	public Long getChannel_fkey() {
		return channel_fkey;
	}

	public void setChannel_fkey(Long channel_fkey) {
		this.channel_fkey = channel_fkey;
	}
	
	@Column(name="sub_txn_type")
	public Integer getSubTxnType() {
		return subTxnType;
	}
	
	public void setSubTxnType(Integer subTxnType) {
		this.subTxnType = subTxnType;
	}

	@Column(name = "wallet")
	public Long getWallet() {
		return this.wallet;
	}
	
	public void setWallet(Long wallet) {
		this.wallet=wallet;
	}
	
}
