package com.neo.aggregator.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.neo.aggregator.enums.AccountStatus;
import com.neo.aggregator.enums.AccountType;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "account_details",uniqueConstraints=
@UniqueConstraint(columnNames={"defaultAccount","entityId","accountNo"}))
@Getter
@Setter
public class NeoAccountDetails extends AbstractEntity {
	
	private String accountNo;

	private String branchId;

	private String bankId;

	private Boolean defaultAccount;

	private String ifscCode;

	private String accountCurrency;

	private AccountType accountType;
	
	private String entityId;
	
	private AccountStatus accountStatus;
	
	private Double amount;
	
	private String schemeCode;

	private String kitNo;
	
	private String modeOfCreation;

}
