package com.neo.aggregator.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "iso_authorization_message", uniqueConstraints = @UniqueConstraint(columnNames = { "mti", "stan",
		"dateTimeTransaction", "acquiringInstitutionCode", "cardAcceptorTerminalId" }))
@Access(AccessType.FIELD)
public class IsoAuthorizationMessage extends AbstractEntity {

	@Column
	private String mti;

	@Column
	private String pan;

	@Column
	private String processingCode;

	@Column
	private String transactionAmount;

	@Column
	private String settlementAmount;

	@Column
	private String cardHolderBillingAmount;

	@Column
	private String dateTimeTransaction;

	@Column
	private String settlementConversionRate;

	@Column
	private String cardholderBillingConversionRate;

	@Column
	private String stan;

	@Column
	private String timeLocalTransaction;

	@Column
	private String dateLocalTransaction;

	@Column
	private String expiryDate;

	@Column
	private String settlementDate;

	@Column
	private String conversionDate;

	@Column
	private String mcc;

	@Column
	private String acquiringCountryCode;

	@Column
	private String posEntryMode;

	@Column
	private String cardSeqNumber;

	@Column
	private String pointOfServiceConditionCode;

	@Column
	private String feesAmount;

	@Column
	private String acquiringInstitutionCode;

	@Column
	private String forwardingInstitutionCode;

	@Column
	private String rrn;

	@Column
	private String authorizationIdResponse;

	@Column
	private String responseCode;

	@Column
	private String serviceConditionCode;

	@Column
	private String cardAcceptorTerminalId;

	@Column
	private String cardAcceptorId;

	@Column
	private String cardAcceptorNameLocation;

	@Column
	private String additionalResponseData;

	@Column
	private String transactionCurrencyCode;

	@Column
	private String settlementCurrencyCode;

	@Column
	private String cardholderBillingCurrencyCode;

	@Column
	private String additionalAmount;

	@Column
	private String adviceReasonCode;

	@Column
	private String posDataCode;

	@Column
	private String networkManagementCode;

	@Column
	private String replacementAmount;

	@Column
	private String accountIdentification1;

	@Column
	private String accountIdentification2;

	@Column
	private String transactionId;

	@Column
	private int updated;

	@Column
	private String kitNo;

	@Column
	private String description;

	@Column
	private String originalDataElement;

	@Column
	private String statementDE125;

	@Column
	private String captureDate;

	public String getCaptureDate() {
		return captureDate;
	}

	public void setCaptureDate(String captureDate) {
		this.captureDate = captureDate;
	}

	public String getStatementDE125() {
		return statementDE125;
	}

	public void setStatementDE125(String statementDE125) {
		this.statementDE125 = statementDE125;
	}

	public String getMti() {
		return mti;
	}

	public void setMti(String mti) {
		this.mti = mti;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public String getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public String getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(String settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public String getCardHolderBillingAmount() {
		return cardHolderBillingAmount;
	}

	public void setCardHolderBillingAmount(String cardHolderBillingAmount) {
		this.cardHolderBillingAmount = cardHolderBillingAmount;
	}

	public String getDateTimeTransaction() {
		return dateTimeTransaction;
	}

	public void setDateTimeTransaction(String dateTimeTransaction) {
		this.dateTimeTransaction = dateTimeTransaction;
	}

	public String getSettlementConversionRate() {
		return settlementConversionRate;
	}

	public void setSettlementConversionRate(String settlementConversionRate) {
		this.settlementConversionRate = settlementConversionRate;
	}

	public String getCardholderBillingConversionRate() {
		return cardholderBillingConversionRate;
	}

	public void setCardholderBillingConversionRate(String cardholderBillingConversionRate) {
		this.cardholderBillingConversionRate = cardholderBillingConversionRate;
	}

	public String getStan() {
		return stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	public String getTimeLocalTransaction() {
		return timeLocalTransaction;
	}

	public void setTimeLocalTransaction(String timeLocalTransaction) {
		this.timeLocalTransaction = timeLocalTransaction;
	}

	public String getDateLocalTransaction() {
		return dateLocalTransaction;
	}

	public void setDateLocalTransaction(String dateLocalTransaction) {
		this.dateLocalTransaction = dateLocalTransaction;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getSettlementDate() {
		return settlementDate;
	}

	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}

	public String getConversionDate() {
		return conversionDate;
	}

	public void setConversionDate(String conversionDate) {
		this.conversionDate = conversionDate;
	}

	public String getMcc() {
		return mcc;
	}

	public void setMcc(String mcc) {
		this.mcc = mcc;
	}

	public String getAcquiringCountryCode() {
		return acquiringCountryCode;
	}

	public void setAcquiringCountryCode(String acquiringCountryCode) {
		this.acquiringCountryCode = acquiringCountryCode;
	}

	public String getPosEntryMode() {
		return posEntryMode;
	}

	public void setPosEntryMode(String posEntryMode) {
		this.posEntryMode = posEntryMode;
	}

	public String getCardSeqNumber() {
		return cardSeqNumber;
	}

	public void setCardSeqNumber(String cardSeqNumber) {
		this.cardSeqNumber = cardSeqNumber;
	}

	public String getPointOfServiceConditionCode() {
		return pointOfServiceConditionCode;
	}

	public void setPointOfServiceConditionCode(String pointOfServiceConditionCode) {
		this.pointOfServiceConditionCode = pointOfServiceConditionCode;
	}

	public String getFeesAmount() {
		return feesAmount;
	}

	public void setFeesAmount(String feesAmount) {
		this.feesAmount = feesAmount;
	}

	public String getAcquiringInstitutionCode() {
		return acquiringInstitutionCode;
	}

	public void setAcquiringInstitutionCode(String acquiringInstitutionCode) {
		this.acquiringInstitutionCode = acquiringInstitutionCode;
	}

	public String getForwardingInstitutionCode() {
		return forwardingInstitutionCode;
	}

	public void setForwardingInstitutionCode(String forwardingInstitutionCode) {
		this.forwardingInstitutionCode = forwardingInstitutionCode;
	}

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getAuthorizationIdResponse() {
		return authorizationIdResponse;
	}

	public void setAuthorizationIdResponse(String authorizationIdResponse) {
		this.authorizationIdResponse = authorizationIdResponse;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getServiceConditionCode() {
		return serviceConditionCode;
	}

	public void setServiceConditionCode(String serviceConditionCode) {
		this.serviceConditionCode = serviceConditionCode;
	}

	public String getCardAcceptorTerminalId() {
		return cardAcceptorTerminalId;
	}

	public void setCardAcceptorTerminalId(String cardAcceptorTerminalId) {
		this.cardAcceptorTerminalId = cardAcceptorTerminalId;
	}

	public String getCardAcceptorId() {
		return cardAcceptorId;
	}

	public void setCardAcceptorId(String cardAcceptorId) {
		this.cardAcceptorId = cardAcceptorId;
	}

	public String getCardAcceptorNameLocation() {
		return cardAcceptorNameLocation;
	}

	public void setCardAcceptorNameLocation(String cardAcceptorNameLocation) {
		this.cardAcceptorNameLocation = cardAcceptorNameLocation;
	}

	public String getAdditionalResponseData() {
		return additionalResponseData;
	}

	public void setAdditionalResponseData(String additionalResponseData) {
		this.additionalResponseData = additionalResponseData;
	}

	public String getTransactionCurrencyCode() {
		return transactionCurrencyCode;
	}

	public void setTransactionCurrencyCode(String transactionCurrencyCode) {
		this.transactionCurrencyCode = transactionCurrencyCode;
	}

	public String getSettlementCurrencyCode() {
		return settlementCurrencyCode;
	}

	public void setSettlementCurrencyCode(String settlementCurrencyCode) {
		this.settlementCurrencyCode = settlementCurrencyCode;
	}

	public String getCardholderBillingCurrencyCode() {
		return cardholderBillingCurrencyCode;
	}

	public void setCardholderBillingCurrencyCode(String cardholderBillingCurrencyCode) {
		this.cardholderBillingCurrencyCode = cardholderBillingCurrencyCode;
	}

	public String getAdditionalAmount() {
		return additionalAmount;
	}

	public void setAdditionalAmount(String additionalAmount) {
		this.additionalAmount = additionalAmount;
	}

	public String getAdviceReasonCode() {
		return adviceReasonCode;
	}

	public void setAdviceReasonCode(String adviceReasonCode) {
		this.adviceReasonCode = adviceReasonCode;
	}

	public String getPosDataCode() {
		return posDataCode;
	}

	public void setPosDataCode(String posDataCode) {
		this.posDataCode = posDataCode;
	}

	public String getNetworkManagementCode() {
		return networkManagementCode;
	}

	public void setNetworkManagementCode(String networkManagementCode) {
		this.networkManagementCode = networkManagementCode;
	}

	public String getReplacementAmount() {
		return replacementAmount;
	}

	public void setReplacementAmount(String replacementAmount) {
		this.replacementAmount = replacementAmount;
	}

	public String getAccountIdentification1() {
		return accountIdentification1;
	}

	public void setAccountIdentification1(String accountIdentification1) {
		this.accountIdentification1 = accountIdentification1;
	}

	public String getAccountIdentification2() {
		return accountIdentification2;
	}

	public void setAccountIdentification2(String accountIdentification2) {
		this.accountIdentification2 = accountIdentification2;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public int getUpdated() {
		return updated;
	}

	public void setUpdated(int updated) {
		this.updated = updated;
	}

	public String getKitNo() {
		return kitNo;
	}

	public void setKitNo(String kitNo) {
		this.kitNo = kitNo;
	}

	public String getOriginalDataElement() {
		return originalDataElement;
	}

	public void setOriginalDataElement(String originalDataElement) {
		this.originalDataElement = originalDataElement;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
