package com.neo.aggregator.model;

public interface IEntity {

	public Long getPkey();

    public void setPkey(Long pkey);

    public Boolean getDeleted();

    public void setDeleted(Boolean deleted);
}
