package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "partner_master_configuration")
@Getter
@Setter
public class PartnerMasterConfiguration extends AbstractEntity {

	@Column
	private String bankId;

	@Column
	private String bankName;

	@Column
	private String hostModel;

	@Column
	private String tenant;

	@Column
	private String corporate;

	@Column
	private String bankServiceId;

	@Column
	private Boolean connectESB;

	@Column
	private String onRegistrationProduct;

	@Column
	private Boolean registerCore;
	

}
