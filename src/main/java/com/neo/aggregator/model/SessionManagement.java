package com.neo.aggregator.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "session_management")
@Getter
@Setter
public class SessionManagement extends AbstractEntity {

	@Column
	private String bankId;

	@Column
	private String channelId;

	@Column
	private Date expiryDate;

	@Column
	private String sessionId;

	@Column
	private String partnerName;

	@Column
	private String userName;

	@Column
	private Boolean isExpired;
}
