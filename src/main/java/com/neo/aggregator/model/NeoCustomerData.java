package com.neo.aggregator.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "neo_customer_data")
@Getter
@Setter
public class NeoCustomerData extends AbstractEntity {
	private String cifNumber;
	private String entityId;
	private String corporate;
}
