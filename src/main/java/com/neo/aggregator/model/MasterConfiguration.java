package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "master_configuration")
@Getter
@Setter
public class MasterConfiguration extends AbstractEntity {

	@Column
	private String bankId;

	@Column
	private String bankName;

	@Column
	private String hostModel;

	@Column
	private String userName;

	@Column
	private String password;

	@Column
	private Integer sessionInterval;

	@Column
	private String partnerName;

	@Column
	private String channelName;

	@Column
	private String partnerId;
	
	@Column
	private String cbsChannelId;
	
	@Column
	private String sslAlias;
	
	@Column
	private Boolean sslEnabled;

}
