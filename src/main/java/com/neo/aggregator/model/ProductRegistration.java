package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.neo.aggregator.enums.AddressCategory;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "product_registration")
@Getter
@Setter
@Data
public class ProductRegistration extends AbstractEntity {

	@Column
	private String title;
	@Column
	private String name;
	@Column
	private String mobilenumber;
	@Column
	private String emailaddress;
	@Column
	private String eKycRefNum;
	@Column
	private String entityId;
	@Column
	private String entityType;
	@Column
	private String tenant;
	@Column
	private String gender;
	@Column
	private String dob;
	@Column
	private String nationality;
	@Column
	private String occupation;
	@Column
	private String SourceIncomeType;
	@Column
	private String AnnualIncome;
	@Column
	private String CustomerType;
	@Column
	private String CustomerStatus;
	@Column
	private AddressCategory addressCategory;
	@Column
	private String address1;
	@Column
	private String address2;
	@Column
	private String address3;
	@Column
	private String city;
	@Column
	private String district;
	@Column
	private String state;
	@Column
	private String pincode;
	@Column
	private String country;
	@Column
	private String CardAlias;
	@Column
	private String Reqtype;
	@Column
	private String politicallyExposed;
	@Column
	private String fatcaDecl;
	@Column
	private String customerId;
	@Column
	private String description;
	@Column
	private String status;
	@Column
	private Boolean isLog;
}
