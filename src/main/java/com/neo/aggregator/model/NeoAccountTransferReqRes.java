package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "neo_account_transfer_req_res")
@Getter
@Setter
public class NeoAccountTransferReqRes extends AbstractEntity {

	@Column
	private String requestId;

	@Column
	private String entityId;

	@Column
	private String bankId;

	@Column
	private String tenant;

	@Column
	private String status;

	@Column
	private String responseDescription;

	@Column
	private String productId;

	@Column
	private String fromAccountNo;

	@Column
	private String toAccountNo;

	@Column
	private String remitterName;

	@Column
	private String description;

	@Column
	private String amount;

	@Column
	private String transactionCurrency;
	
	@Column
	private String transactionOrigin;
	
	@Column
	private String externalTransactionId;
	
	@Column
	private String beneficiaryAccountType;
	
	@Column
	private String beneficiaryName;
	
	@Column
	private String beneficiaryIfsc;
	
	@Column
	private String transactionType;

	@Column
	private String transactionMode;

	@Column
	private String transactionExpiry;

	@Column
	private String reoccurrence;

}
