package com.neo.aggregator.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "sbmerch_ob_req_res")
public class MerchantOBReqRes extends AbstractEntity{
	String pgMerchantId;
	String action;
	String mebussname;
	String merVirtualAdd;
	String awlmcc;
	String requestUrl1;
	String requestUrl2;
	String integrationType;
	String panNo;
	String cntEmail;
	String strEmailId;
	String gstn;
	String meBussntype;
	String pdayTxnCount;
	String pdayTxnLmt;
	String pdayTxnAmt;
	String strCntMobile;
	String extMID;
	String extTID;
	String add;
	String city;
	String state;
	String requestId;
	String addinfo1;
	String addinfo2;
	String addinfo3;
	String addinfo4;
	String addinfo5;
	String addinfo6;
	String addinfo7;
	String addinfo8;
	String addinfo9;
	String addinfo10;
	String sms;
	String email;
	String statusDesc;
	String status;
	String crtDate;
	String loginaccess;
}
