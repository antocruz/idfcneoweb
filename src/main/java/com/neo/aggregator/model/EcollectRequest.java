package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "ecollect_request")
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class EcollectRequest extends AbstractEntity {

	@Column
	private String ecollectCode;

	@Column
	private String amount;

	@Column
	private String remitterAccountNumber;

	@Column
	private String remitterFullName;

	@Column
	private String transferUniqueNumber;

	@Column
	private String customerCode;

	@Column
	private String description;

	@Column
	private String programCode;

	@Column
	private String program;

	@Column
	private String remitterIFSCCode;

	@Column
	private String virtualAccountNo;

	@Column
	private String creditAccountNo;

	@Column
	private String transferType;

	@Column
	private String creditedAt;

	@Column
	private String note;

	@Column
	private String remitterAccountType;

	@Column
	private String application;

	@Column
	private String corporate;

	@Column
	private String m2pReferenceNumber;

	@Column
	private String result;

}
