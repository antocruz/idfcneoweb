package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "ecollect_core_config")
@Getter
@Setter
public class EcollectCoreUrlConfig extends AbstractEntity {

    private String business;
    private String ecollectCode;
    private String coreServiceUrl;

    @Column
    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    @Column
    public String getEcollectCode() {
        return ecollectCode;
    }

    public void setEcollectCode(String ecollectCode) {
        this.ecollectCode = ecollectCode;
    }

    @Column
    public String getCoreServiceUrl() {
        return coreServiceUrl;
    }

    public void setCoreServiceUrl(String coreServiceUrl) {
        this.coreServiceUrl = coreServiceUrl;
    }
}
