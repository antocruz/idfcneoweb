package com.neo.aggregator.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "neo_lien_mark_req_res")
@Getter
@Setter
public class NeoLienMarkReqResModel extends AbstractEntity {
	
	@Column
	private String requestId;

	@Column
	private String entityId;
	
	@Column
	private String bankId;
	
	@Column
	private String tenant;
	
	@Column
	private String accountId;
	
	@Column
	private String lienId;
	
	@Column
	private Date lienStart;
	
	@Column
	private Date lienEnd;
	
	@Column
	private String lienReasonCode;
	
	@Column
	private String lienModuleType;
	
	@Column
	private String status;
	
	@Column
	private String responseDescription;
	
	@Column
	private String productId;
	
	@Column
	private String amount;
	
	@Column
	private Integer retryCount;

}
