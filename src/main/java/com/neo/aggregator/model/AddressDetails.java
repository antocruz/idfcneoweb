package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "address_details")
@JsonIgnoreProperties("isBlocked")
@Getter
@Setter
public class AddressDetails extends AbstractEntity {

	@Column
	private String entityId;

	@Column
	private String cnvId;

	@Column
	private String timestamp;
	
	@Column
	private String custLeadId;
	
	@Column
	private String addressId;
	
	@Column
	private String addressType;
}
