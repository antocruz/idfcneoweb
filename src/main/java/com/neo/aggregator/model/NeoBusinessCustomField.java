package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "neo_business_custom_field", uniqueConstraints = @UniqueConstraint(columnNames = { "tenant",
		"fieldName" }))
@Getter
@Setter
public class NeoBusinessCustomField extends AbstractEntity {

	@Column(name = "field_name")
	private String fieldName;

	@Column(name = "field_value")
	private String fieldValue;

	@Column(name = "tenant")
	private String tenant;
	
	@Column(name = "description")
	private String description;

}
