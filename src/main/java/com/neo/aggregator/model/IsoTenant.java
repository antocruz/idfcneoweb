package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "iso_tenant")
public class IsoTenant extends AbstractEntity {

	private String business;

	private String posIssuerClientUrl;

	private Integer posIssuerClientPort;

	private String posIssuerServerUrl;

	private Integer posIssuerServerPort;

	private String networkId;

	private Long timeOut;

	@Column(name = "time_out")
	public Long getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(Long timeOut) {
		this.timeOut = timeOut;
	}

	@Column(name = "network_id")
	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	@Column
	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}

	@Column
	public String getPosIssuerClientUrl() {
		return posIssuerClientUrl;
	}

	public void setPosIssuerClientUrl(String posIssuerClientUrl) {
		this.posIssuerClientUrl = posIssuerClientUrl;
	}

	@Column
	public Integer getPosIssuerClientPort() {
		return posIssuerClientPort;
	}

	public void setPosIssuerClientPort(Integer posIssuerClientPort) {
		this.posIssuerClientPort = posIssuerClientPort;
	}

	@Column
	public String getPosIssuerServerUrl() {
		return posIssuerServerUrl;
	}

	public void setPosIssuerServerUrl(String posIssuerServerUrl) {
		this.posIssuerServerUrl = posIssuerServerUrl;
	}

	@Column
	public Integer getPosIssuerServerPort() {
		return posIssuerServerPort;
	}

	public void setPosIssuerServerPort(Integer posIssuerServerPort) {
		this.posIssuerServerPort = posIssuerServerPort;
	}
}
