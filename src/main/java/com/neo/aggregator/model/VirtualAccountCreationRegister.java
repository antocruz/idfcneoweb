package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "virtualaccount_creation_register")
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class VirtualAccountCreationRegister extends AbstractEntity {

	@Column
	private String uniqTxnId;
	@Column
	private String corpId;
	@Column
	private String entityId;
	@Column
	private String virtualAccNum;
	@Column
	private String cifNum;
	@Column
	private Boolean corporateCredit;
	@Column
	private String result;
	@Column
	private int retryCount;
	@Column
	private String kitNo;
	@Column
	private String customerName;

}
