package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "partner_service_configuration")
@Getter
@Setter
public class PartnerServiceConfiguration extends AbstractEntity {

	@Column
	private String bankId;

	@Column
	private String serviceEndpoint;

	@Column
	private String ebsConnectivity;

	@Column
	private String bankServiceId;

	@Column
	private byte[] bankPublicKey;

}
