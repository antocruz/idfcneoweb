package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "default_data")
@JsonIgnoreProperties("isBlocked")
@Getter
@Setter
public class DefaultData {

	@Id
    private Long id;
	
	@Column
	private String appId = "FOS";
	
	@Column
	private String associatedWith = "1";
	
	@Column
	private Boolean ignoreProbableMatch = true;
	
	@Column
	private String entityType = "I";
	
	@Column
	private Boolean fromMADP = false;
	
	@Column
	private String deDupChkReqByCustCount = "Y";
	
	@Column
	private Boolean isAadhar = true;
	
	@Column
	private Boolean mappedToAccountLead = false;
	
	@Column
	private String purposeOfCreation = "16";
	
	@Column
	private Boolean isCurrentAddressSameAsPermanent = true;
	
	@Column
	private Boolean isPhotoRequired = false;
	
	@Column
	private String branchId = "1001";
	
	@Column
	private String authUserId = "OBUSER";
	
	@Column
	private String preferredLanguage = "ENG";
	
	@Column
	private String cityOfBirth = null;
	
	@Column
	private String countryOfBirth = "IND";
	
	@Column
	private Boolean taxResident = false;
	
	@Column
	private String usrTkn = "newgen";
	
	@Column
	private String usrNm = "00474";
	
	@Column
	private String usrPwd = "00474";
	
	@Column
	private String nationality = "IND";
	
	@Column
	private String usrId = "34464";
	
	@Column
	private String authenticationToken = "e3XIdKW6zoWyJnBT0GeDghtUc7ChQXcMKP7TS+YltmgfXZJCEMLuKFgxM9RtZPcl";
	
	@Column
	private String entityFlagType = "I";
	
	@Column
	private String sourceBranch = "9999";
	
	@Column
	private String homeBranch = "9999";
	
	@Column
	private String mcc = "6012";
	
	@Column
	private String posEntryMode = "019";
	
	@Column
	private String posCode = "05";
	
	@Column
	private String caId = "EQT000000000001";
	
	@Column
	private String caTid = "11205764";
	
	@Column
	private String caTa = "CSB NERUL MUMBAI MHIN";
	
	@Column
	private String politicallyExposedPerson = "0";
}
