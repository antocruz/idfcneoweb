package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "neo_account_creation_req_res")
@Getter
@Setter
public class NeoAccountCreationReqResModel extends AbstractEntity {
	
	@Column
	private String requestId;

	@Column
	private String entityId;
	
	@Column
	private String bankId;
	
	@Column
	private String tenant;
		
	@Column
	private String schemeCode;
	
	@Column
	private String nomineeName;
	
	@Column
	private String solId;
	
	@Column
	private String modeOfOperation;
	
	@Column
	private String accountCurrency;
	
	@Column
	private String accountId;
	
	@Column
	private String freezeCode;
	
	@Column
	private String freezeReason;
	
	@Column
	private String status;
	
	@Column
	private String responseDescription;
	
	@Column
	private String productId;
	
	@Column
	private Double amount;
	
	@Column
	private String paymentRef;
	
}

