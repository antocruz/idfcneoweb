package com.neo.aggregator.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "upicollect_request")
@Getter
@Setter
public class UpiCollectRequest extends AbstractEntity {
    private String amount;
    private String application;
    private String customerCode;
    private String description;
    private String merchantId;
    private String merchantTranId;
    private String payerVpa;
    private String payeeVpa;
    private String terminalId;
    private String txnStatus;
    private String txnCompletionDt;
    private String txnInitDt;
    private String subMerchantId;
    private String remitterAccountNumber;
    private String remitterAccountType;
    private String remitterFullName;
    private String remitterIFSCCode;
    private String remitterMobile;
    private String transferType;
    private String transferUniqueNumber;
    private String corporate;
    private String note;
    private String program;
    private String programCode;
    private String result;
    private String reject_reason;
    private String refund_status;
    private String refund_txn_id;
    private String refund_bankTid;

    @Column
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Column
    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    @Column
    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    @Column
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column
    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    @Column
    public String getMerchantTranId() {
        return merchantTranId;
    }

    public void setMerchantTranId(String merchantTranId) {
        this.merchantTranId = merchantTranId;
    }

    @Column
    public String getPayerVpa() {
        return payerVpa;
    }

    public void setPayerVpa(String payerVpa) {
        this.payerVpa = payerVpa;
    }

    @Column
    public String getPayeeVpa() {
        return payeeVpa;
    }

    public void setPayeeVpa(String payeeVpa) {
        this.payeeVpa = payeeVpa;
    }

    @Column
    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    @Column
    public String getTxnStatus() {
        return txnStatus;
    }

    public void setTxnStatus(String txnStatus) {
        this.txnStatus = txnStatus;
    }

    @Column
    public String getTxnCompletionDt() {
        return txnCompletionDt;
    }

    public void setTxnCompletionDt(String txnCompletionDt) {
        this.txnCompletionDt = txnCompletionDt;
    }

    @Column
    public String getTxnInitDt() {
        return txnInitDt;
    }

    public void setTxnInitDt(String txnInitDt) {
        this.txnInitDt = txnInitDt;
    }

    @Column
    public String getSubMerchantId() {
        return subMerchantId;
    }

    public void setSubMerchantId(String subMerchantId) {
        this.subMerchantId = subMerchantId;
    }

    @Column
    public String getRemitterAccountNumber() {
        return remitterAccountNumber;
    }

    public void setRemitterAccountNumber(String remitterAccountNumber) {
        this.remitterAccountNumber = remitterAccountNumber;
    }

    @Column
    public String getRemitterAccountType() {
        return remitterAccountType;
    }

    public void setRemitterAccountType(String remitterAccountType) {
        this.remitterAccountType = remitterAccountType;
    }

    @Column
    public String getRemitterFullName() {
        return remitterFullName;
    }

    public void setRemitterFullName(String remitterFullName) {
        this.remitterFullName = remitterFullName;
    }

    @Column
    public String getRemitterIFSCCode() {
        return remitterIFSCCode;
    }

    public void setRemitterIFSCCode(String remitterIFSCCode) {
        this.remitterIFSCCode = remitterIFSCCode;
    }

    @Column
    public String getRemitterMobile() {
        return remitterMobile;
    }

    public void setRemitterMobile(String remitterMobile) {
        this.remitterMobile = remitterMobile;
    }

    @Column
    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    @Column
    public String getTransferUniqueNumber() {
        return transferUniqueNumber;
    }

    public void setTransferUniqueNumber(String transferUniqueNumber) {
        this.transferUniqueNumber = transferUniqueNumber;
    }

    @Column
    public String getCorporate() {
        return corporate;
    }

    public void setCorporate(String corporate) {
        this.corporate = corporate;
    }

    @Column
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Column
    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    @Column
    public String getProgramCode() {
        return programCode;
    }

    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }

    @Column
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Column
    public String getReject_reason() {
        return reject_reason;
    }

    public void setReject_reason(String reject_reason) {
        this.reject_reason = reject_reason;
    }

    @Column
    public String getRefund_status() {
        return refund_status;
    }

    public void setRefund_status(String refund_status) {
        this.refund_status = refund_status;
    }

    @Column
    public String getRefund_txn_id() {
        return refund_txn_id;
    }

    public void setRefund_txn_id(String refund_txn_id) {
        this.refund_txn_id = refund_txn_id;
    }

    @Column
    public String getRefund_bankTid() {
        return refund_bankTid;
    }

    public void setRefund_bankTid(String refund_bankTid) {
        this.refund_bankTid = refund_bankTid;
    }
}
