package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "product_config")
@Getter
@Setter
public class ProductConfig extends AbstractEntity {

	@Column
	private String branchId;

	@Column
	private String product;

	@Column
	private String defaultSchemeCode;

	@Column
	private String defaultAccountCurrency;

	@Column
	private String defaultAccountType;
	
	@Column
	private String defaultRePaymentAccountType;

	@Column
	private String defaultNationality;

	@Column
	private String defaultFatcaCountry;

	@Column
	private String partner;

}
