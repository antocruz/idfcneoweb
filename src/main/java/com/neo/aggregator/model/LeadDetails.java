package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "lead_details",  schema="neo_aggregator", catalog="neo_aggregator")
@JsonIgnoreProperties("isBlocked")
@Getter
@Setter
public class LeadDetails extends AbstractEntity {

	@Column
	private String entityId;

	@Column
	private String custLeadId;

	@Column
	private String cnvId;

	@Column
	private String timestamp;

}
