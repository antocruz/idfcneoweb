package com.neo.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "update_document_details")
@JsonIgnoreProperties("isBlocked")
@Getter
@Setter
public class UpdateDocumentDetails extends AbstractEntity {

	@Column
	private String entityId;

	@Column
	private String cnvId;

	@Column
	private String timestamp;

	@Column
	private String isInsert;

	@Column
	private String isFirstInserted;

	@Column
	private String proceedToNLTable;

	@Column
	private String dmsDocumentID;

	@Column
	private String documentID;

	@Column
	private String documentNo;

	@Column
	private String documentType;

	@Column
	private String mappedCustomerLead;

}
