package com.neo.aggregator.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "neo_fund_transfer", schema = "neo_aggregator", catalog = "neo_aggregator")
@Getter
@Setter
public class NeoFundTransfer extends AbstractEntity {

    @Column
    private String externalTransactionId;
    @Column
    private String entityId;
    @Column
    private String tenant;
    @Column
    private String remitterAccountNo;
    @Column
    private String remitterIfsc;
    @Column
    private String remitterName;
    @Column
    private String beneficiaryAccountNo;
    @Column
    private String beneficiaryAccountType;
    @Column
    private String beneficiaryName;
    @Column
    private String beneficiaryIfsc;
    @Column
    private String beneficiaryMobile;
    @Column
    private String beneficiaryEmail;
    @Column
    private String description;
    @Column
    private String transactionAmount;
    @Column
    private String transactionCurrency;
    @Column
    private String transactionOrigin;
    @Column
    private String transactionType;
    @Column
    private String transactionMode;
    @Column
    private String transactionExpiry;
    @Column
    private String transactionStatus;
    @Column
    private String transactionFailureReason;
    @Column
    private String bankTransactionId;
}
