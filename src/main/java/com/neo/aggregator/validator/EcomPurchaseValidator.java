package com.neo.aggregator.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.neo.aggregator.utility.FieldConstants;
import com.neo.aggregator.utility.Iso8583;

@Component("ecomPurchaseValidator")
public class EcomPurchaseValidator implements MandatoryFieldCheckValidator {

	@Override
	public boolean validate(Iso8583 isoMessage) {

		if (checkNullMessage(isoMessage)) {
			String processingCode = isoMessage.getProcessingCode();

			if (!processingCode.startsWith(FieldConstants.ECOM_TXN_ID) || processingCode.length() != 6) {
				return false;
			}

			String posEntryMode = isoMessage.getPosEntryMode();

			if (StringUtils.isEmpty(posEntryMode) || !posEntryMode.equals(FieldConstants.POS_ENTRY_MODE_VALUE)) {
				return false;
			}
		}
		return true;
	}

}
