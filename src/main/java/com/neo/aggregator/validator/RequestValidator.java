package com.neo.aggregator.validator;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.neo.aggregator.bank.equitas.dto.KycRequestDto;
import com.neo.aggregator.bank.equitas.dto.KycResponseDto;
import com.neo.aggregator.constant.EqConstants;
import com.neo.aggregator.dto.AadhaarOtpRequest;
import com.neo.aggregator.dto.AadhaarOtpResponse;
import com.neo.aggregator.dto.KYCBioRequestDto;
import com.neo.aggregator.dto.KYCBioResponseDto;
import com.neo.aggregator.dto.NeoException;
import com.neo.aggregator.enums.EmploymentType;
import com.neo.aggregator.utility.Utils;

@Service
public class RequestValidator {

	private static final Logger logger = LoggerFactory.getLogger(RequestValidator.class);

	public KycRequestDto validateEqRequest(KycRequestDto request) throws NeoException {

		if (request.getIdNumber() != null && request.getIdTypeValidation() != null) {

			if (request.getIdTypeValidation().equalsIgnoreCase("U")) {
				request.setAadhar(request.getIdNumber());
			} else if (request.getIdTypeValidation().equalsIgnoreCase("V")) {
				request.setVirtualId(request.getIdNumber());
			} 

		}
		
		if (request.getTitle() != null) {

			if (EqConstants.MR.equalsIgnoreCase(request.getTitle())) {
				request.setTitle("1");
			} else if (EqConstants.MRS.equalsIgnoreCase(request.getTitle())) {
				request.setTitle("5");
			} else if (EqConstants.MISS.equalsIgnoreCase(request.getTitle())) {
				request.setTitle("15");
			}

		}

		if (request.getRegistrationRequestDtoV2() != null) {
			if (request.getRegistrationRequestDtoV2().getMaritalStatus() != null) {
				if (EqConstants.SINGLE.equalsIgnoreCase(request.getRegistrationRequestDtoV2().getMaritalStatus())) {
					request.setMaritalStatus("1");
				} else if (EqConstants.MARRIED.equalsIgnoreCase(request.getRegistrationRequestDtoV2().getMaritalStatus())) {
					request.setMaritalStatus("2");
				}
			}

			if (request.getRegistrationRequestDtoV2().getEmploymentType() != null) {
				if (EmploymentType.EMPLOYED.equals(request.getRegistrationRequestDtoV2().getEmploymentType())) {
					request.setOccupation("33");
					request.setOccuapation("33");
				} else if (EmploymentType.SELF_EMPLOYED.equals(request.getRegistrationRequestDtoV2().getEmploymentType())) {
					request.setOccupation("41");
					request.setOccuapation("41");
				} else if (EmploymentType.HOUSEWORK.equals(request.getRegistrationRequestDtoV2().getEmploymentType())) {
					request.setOccupation("6");
					request.setOccuapation("6");
				} else if (EmploymentType.STUDENT.equals(request.getRegistrationRequestDtoV2().getEmploymentType())) {
					request.setOccupation("5");
					request.setOccuapation("5");
				}
			}
			
			if(request.getRegistrationRequestDtoV2().getKycInfo().get(0).getDocumentType()!= null) {
				if(EqConstants.PAN.equalsIgnoreCase(request.getRegistrationRequestDtoV2().getKycInfo().get(0).getDocumentType())) {
					request.setIdentityType("16");
				}else if(EqConstants.FORM_60.equalsIgnoreCase(request.getRegistrationRequestDtoV2().getKycInfo().get(0).getDocumentType())) {
					request.setIdentityType("187");
				}
			}
			
			if(request.getRegistrationRequestDtoV2().getCommunicationInfo()!=null) {
				request.setEmailID(request.getRegistrationRequestDtoV2().getCommunicationInfo().get(0).getEmailId());
			}
		}
		
		if (request.getRegistrationRequestDtoV2().getKycInfo().get(0).getDocumentType() != null && EqConstants.FORM_60
				.equalsIgnoreCase(request.getRegistrationRequestDtoV2().getKycInfo().get(0).getDocumentType()) && (Math.round(Integer.parseInt(request.getGrossAnnualIncome())) > 250000)) {
			throw new NeoException("Annual income more than 250000", "Maximum annual income for form 60 should not be more than 250000", null, null,
					"Maximum annual income for form 60 should not be more than 250000, select document type PAN.");
		}
		if (request.getGrossAnnualIncome() != null) {
			int grossIncome = Math.round(Integer.parseInt(request.getGrossAnnualIncome()));
			if (grossIncome > 0 && grossIncome <= 50000) {
				request.setGrossAnnualIncome("1");
			} else if (grossIncome > 50000 && grossIncome <= 100000) {
				request.setGrossAnnualIncome("2");
			} else if (grossIncome > 100000 && grossIncome <= 250000) {
				request.setGrossAnnualIncome("3");
			} else if (grossIncome > 250000 && grossIncome <= 500000) {
				request.setGrossAnnualIncome("4");
			} else if (grossIncome > 500000 && grossIncome <= 750000) {
				request.setGrossAnnualIncome("5");
			} else if (grossIncome > 750000 && grossIncome <= 1000000) {
				request.setGrossAnnualIncome("6");
			} else if (grossIncome > 1000000 && grossIncome <= 1500000) {
				request.setGrossAnnualIncome("7");
			} else if (grossIncome > 1500000 && grossIncome <= 2000000) {
				request.setGrossAnnualIncome("8");
			} else if (grossIncome > 2000000 && grossIncome <= 3000000) {
				request.setGrossAnnualIncome("9");
			} else if (grossIncome > 3000000 && grossIncome <= 5000000) {
				request.setGrossAnnualIncome("10");
			} else if (grossIncome > 5000000 && grossIncome <= 10000000) {
				request.setGrossAnnualIncome("11");
			} else if (grossIncome > 10000000) {
				request.setGrossAnnualIncome("12");
			}

			if (request.getEstimatedAgriculturalIncome()!=null) {
				request.setEstimatedAgriculturalIncome(Integer.toString(grossIncome));
				request.getRegistrationRequestDtoV2().setEstimatedAgriculturalIncome(Integer.toString(grossIncome));
			} else if(request.getEstimatedNonAgriculturalIncome()!=null) {
				request.setEstimatedNonAgriculturalIncome(Integer.toString(grossIncome));
				request.getRegistrationRequestDtoV2().setEstimatedNonAgriculturalIncome(Integer.toString(grossIncome));
			}
			
			if (request.getIsAgriculturalIncome() != null) {
				if (request.getIsAgriculturalIncome()) {
					request.setEstimatedAgriculturalIncome(Integer.toString(grossIncome));
					request.getRegistrationRequestDtoV2().setEstimatedAgriculturalIncome(Integer.toString(grossIncome));
				} else {
					request.setEstimatedNonAgriculturalIncome(Integer.toString(grossIncome));
					request.getRegistrationRequestDtoV2()
							.setEstimatedNonAgriculturalIncome(Integer.toString(grossIncome));
				}
			}
		}
		
		logger.info("Updated request :: {}",  Utils.convertModelToStringWithMaskedAadhar(request));

		return request;
	}
	
	public KycRequestDto validateBioRequest(KYCBioRequestDto request) {
		KycRequestDto kycRequest = new KycRequestDto();
		if (request != null && request.getEntityId() != null && request.getSkey() != null && request.getCi() != null
				&& request.getMc() != null && request.getDataType() != null && request.getDataValue() != null
				&& request.getHmac() != null && request.getMi() != null && request.getRdsId() != null
				&& request.getRdsVer() != null && request.getDpId() != null && request.getDc() != null && request.getRegistrationRequestDtoV2() != null && request.getWadh()!=null) {
			kycRequest.setEntityId(request.getEntityId());
			kycRequest.setSkey(request.getSkey());
			kycRequest.setCi(request.getCi());
			kycRequest.setMc(request.getMc());
			kycRequest.setDataType(request.getDataType());
			kycRequest.setDataValue(request.getDataValue());
			kycRequest.setHmac(request.getHmac());
			kycRequest.setMi(request.getMi());
			kycRequest.setRdsId(request.getRdsId());
			kycRequest.setRdsVer(request.getRdsVer());
			kycRequest.setDpId(request.getDpId());
			kycRequest.setDc(request.getDc());
			kycRequest.setRegistrationRequestDtoV2(request.getRegistrationRequestDtoV2());
			kycRequest.setIdTypeValidation(request.getIdTypeValidation());
			kycRequest.setIdNumber(request.getIdNumber());
			kycRequest.setMotherMaidenName(request.getMotherMaidenName());
			kycRequest.setFatherName(request.getFatherName());
			kycRequest.setGrossAnnualIncome(request.getGrossAnnualIncome());
			kycRequest.setEstimatedAgriculturalIncome(request.getEstimatedAgriculturalIncome());
			kycRequest.setEstimatedNonAgriculturalIncome(request.getEstimatedNonAgriculturalIncome());
			kycRequest.setIsAgriculturalIncome(request.getIsAgriculturalIncome());
			kycRequest.setWadh(request.getWadh());
			kycRequest.setOtpVerificationCode(request.getOtpVerificationCode());
			kycRequest.setAgentName(request.getAgentName());
			kycRequest.setAgentUcic(request.getAgentUcic());
			kycRequest.setAgentAccountNumber(request.getAgentAccountNumber());
			kycRequest.setAgentAddress(request.getAgentAddress());
			kycRequest.setAgentCity(request.getAgentCity());
			kycRequest.setAgentState(request.getAgentState());
			kycRequest.setAgentPincode(request.getAgentPincode());
			kycRequest.setCorporateAddress(request.getCorporateAddress());
			kycRequest.setCorporateName(request.getCorporateName());
			kycRequest.setCorporateCity(request.getCorporateCity());
			kycRequest.setCorporateState(request.getCorporateState());
			kycRequest.setCorporatePincode(request.getCorporatePincode());
			kycRequest.setFundSource(request.getFundSource());
			kycRequest.setSignature(request.getSignature());
	       
			logger.info("Bio Request validated :: {}",  Utils.convertModelToStringWithMaskedAadhar(kycRequest));
			return kycRequest;
		}
		return null;
	}
	
	public KYCBioResponseDto validateBioResponse(KycResponseDto kycResponse) {
		KYCBioResponseDto response = new KYCBioResponseDto();
		response.setEntityId(Objects.toString(kycResponse.getEntityId(), null));
		response.setCustLeadId(Objects.toString(kycResponse.getCustLeadId(), null));
		response.setCustId(Objects.toString(kycResponse.getCustId(), null));
		response.setAuthCode(Objects.toString(kycResponse.getAuthCode(), null));
		response.setName(Objects.toString(kycResponse.getName(), null));
		response.setDob(Objects.toString(kycResponse.getDob(), null));
		response.setAdd(Objects.toString(kycResponse.getAdd(), null));
		response.setTitle(Objects.toString(kycResponse.getTitle(), null));
		if (kycResponse.getAddressDto() != null) {
			response.setAddressDto(kycResponse.getAddressDto());
		}
		response.setRrn(Objects.toString(kycResponse.getRrn(), null));
		response.setPhoto(Objects.toString(kycResponse.getPhoto(), null));
		if (kycResponse.getRegistrationRequestDtoV2() != null) {
			response.setRegistrationRequestDtoV2(kycResponse.getRegistrationRequestDtoV2());
		}
		response.setKycStatus(Objects.toString(kycResponse.getKycStatus(), null));

		logger.info("Bio Response validated :: {}",  Utils.convertModelToString(response));
		return response;
	}
	
	public KycRequestDto validateOtpRequest(AadhaarOtpRequest request) {
		KycRequestDto kycRequest = new KycRequestDto();
		if (request != null && request.getEntityId() != null && request.getRegistrationRequestDtoV2() != null
				&& request.getStan() != null && request.getSkey() != null) {
			kycRequest.setEntityId(request.getEntityId());
			//kycRequest.setOtp(request.getOtp());
			kycRequest.setStan(request.getStan());
			kycRequest.setRegistrationRequestDtoV2(request.getRegistrationRequestDtoV2());
			kycRequest.setIdTypeValidation(request.getIdTypeValidation());
			kycRequest.setIdNumber(request.getIdNumber());
			kycRequest.setMotherMaidenName(request.getMotherMaidenName());
			kycRequest.setFatherName(request.getFatherName());
			kycRequest.setGrossAnnualIncome(request.getGrossAnnualIncome());
			kycRequest.setEstimatedAgriculturalIncome(request.getEstimatedAgriculturalIncome());
			kycRequest.setEstimatedNonAgriculturalIncome(request.getEstimatedNonAgriculturalIncome());
			kycRequest.setIsAgriculturalIncome(request.getIsAgriculturalIncome());
			kycRequest.setPidBlock(request.getPidBlock());
	       
			kycRequest.setOtpVerificationCode(request.getOtpVerificationCode());
			kycRequest.setAgentName(request.getAgentName());
			kycRequest.setAgentUcic(request.getAgentUcic());
			kycRequest.setAgentAccountNumber(request.getAgentAccountNumber());
			kycRequest.setAgentAddress(request.getAgentAddress());
			kycRequest.setAgentCity(request.getAgentCity());
			kycRequest.setAgentState(request.getAgentState());
			kycRequest.setAgentPincode(request.getAgentPincode());
			kycRequest.setCorporateAddress(request.getCorporateAddress());
			kycRequest.setCorporateName(request.getCorporateName());
			kycRequest.setCorporateCity(request.getCorporateCity());
			kycRequest.setCorporateState(request.getCorporateState());
			kycRequest.setCorporatePincode(request.getCorporatePincode());
			kycRequest.setFundSource(request.getFundSource());
			kycRequest.setSignature(request.getSignature());
			kycRequest.setSkey(request.getSkey());
			kycRequest.setCi(request.getCi());
			kycRequest.setDataType(request.getDataType());
			kycRequest.setDataValue(request.getDataValue());
			kycRequest.setHmac(request.getHmac());
			logger.info("OTP Request validated :: {}",  Utils.convertModelToStringWithMaskedAadhar(kycRequest));
			return kycRequest;
		}
		return null;
	}
	
	public AadhaarOtpResponse validateOtpResponse(KycResponseDto kycResponse) {
		AadhaarOtpResponse response = new AadhaarOtpResponse();
		response.setEntityId(Objects.toString(kycResponse.getEntityId(), null));
		response.setCustLeadId(Objects.toString(kycResponse.getCustLeadId(), null));
		response.setCustId(Objects.toString(kycResponse.getCustId(), null));
		response.setAuthCode(Objects.toString(kycResponse.getAuthCode(), null));
		response.setName(Objects.toString(kycResponse.getName(), null));
		response.setDob(Objects.toString(kycResponse.getDob(), null));
		response.setAdd(Objects.toString(kycResponse.getAdd(), null));
		response.setTitle(Objects.toString(kycResponse.getTitle(), null));
		if (kycResponse.getAddressDto() != null) {
			response.setAddressDto(kycResponse.getAddressDto());
		}
		response.setRrn(Objects.toString(kycResponse.getRrn(), null));
		response.setPhoto(Objects.toString(kycResponse.getPhoto(), null));
		if (kycResponse.getRegistrationRequestDtoV2() != null) {
			response.setRegistrationRequestDtoV2(kycResponse.getRegistrationRequestDtoV2());
		}
		response.setKycStatus(Objects.toString(kycResponse.getKycStatus(), null));

		logger.info("OTP Response validated :: {}",  Utils.convertModelToString(response));
		return response;
	}
}
