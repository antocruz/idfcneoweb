package com.neo.aggregator.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.utility.Iso8583;

@Component("octIssuerValidaor")
public class OctIssuerValidaor implements MandatoryFieldCheckValidator {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(OctIssuerValidaor.class);

	@Override
	public boolean validate(Iso8583 isoMessage) {

		if (isoMessage == null)
			return false;

		logger.info("OCT ISSUER Validator Request ::: " + isoMessage.toString());

		String pan = isoMessage.getPan();

		if (pan == null || StringUtils.isEmpty(pan) || (pan.length() < 12) || (pan.length() > 19)) {
			return false;
		}

		String processingCode = isoMessage.getProcessingCode();

		if (StringUtils.isEmpty(processingCode)) {
			return false;
		}

		String systemTraceNo = isoMessage.getStan();

		if (StringUtils.isEmpty(systemTraceNo)) {
			return false;
		}

		String acqInstnId = isoMessage.getAcquiringInstitutionCode();

		if (acqInstnId == null || StringUtils.isEmpty(acqInstnId)) {
			return false;
		}

		String merchantId = isoMessage.getCardAcceptorId();

		if (merchantId == null || StringUtils.isEmpty(merchantId)) {
			return false;
		}

		return true;
	}

}
