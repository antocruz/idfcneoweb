package com.neo.aggregator.validator;

import org.springframework.stereotype.Component;

import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.utility.Iso8583;

@Component("posPurchaseValidator")
public class PosPurchaseValidator implements MandatoryFieldCheckValidator {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(PosPurchaseValidator.class);

	@Override
	public boolean validate(Iso8583 request) {

		if (checkNullMessage(request)) {

			String processingCode = request.getProcessingCode();

			logger.info("ProcessingCode :: " + processingCode + " Message ::" + request.toString());

			return true;
		}

		return false;
	}

}
