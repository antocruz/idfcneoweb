package com.neo.aggregator.validator;

import org.apache.commons.lang3.StringUtils;

import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.utility.Iso8583;

public interface MandatoryFieldCheckValidator {

	boolean validate(Iso8583 isoMessage);

	public static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(MandatoryFieldCheckValidator.class);

	default boolean checkNullMessage(Iso8583 request) {
		if (request == null)
			return false;

		String pan = request.getPan();

		if (StringUtils.isEmpty(pan) || (pan.length() < 12) || (pan.length() > 19)) {
			logger.info("PAN is Is InValid ::");
			return false;
		}

		String processingCode = request.getProcessingCode();

		if (StringUtils.isEmpty(processingCode)) {
			logger.info("Processing Code is Is InValid ::");
			return false;
		}

		String systemTraceNo = request.getStan();

		if (StringUtils.isEmpty(systemTraceNo)) {
			logger.info("SystemTraceNo is Is InValid ::");
			return false;
		}

		String mcc = request.getMcc();

		if (StringUtils.isEmpty(mcc)) {
			logger.info("MCC is Is InValid ::");
			return false;
		}

		String posEntryMode = request.getPosEntryMode();

		if (StringUtils.isEmpty(posEntryMode)) {
			logger.info("posEntryMode is Is InValid ::");
			return false;
		}

		String acqInstnId = request.getAcquiringInstitutionCode();

		if (StringUtils.isEmpty(acqInstnId)) {
			logger.info("acqInstnId is Is InValid ::");
			return false;
		}

		String rrId = request.getRrn();

		if (StringUtils.isEmpty(rrId)) {
			logger.info("rrId is Is InValid ::");
			return false;
		}

		return true;
	}
}
