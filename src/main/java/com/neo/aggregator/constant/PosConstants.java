package com.neo.aggregator.constant;

public class PosConstants {

	public static final String INACTIVE_OR_NOSUCH_CARD = "14";
	public static final Object SUCCESS_CODE = "00";
	public static final String EXPIRED_CARD_REPONSE_CODE = "54";
	public static final String LOST_CARD_REPONSE_CODE = "41";
	public static final String STOLEN_CARD_REPONSE_CODE = "43";
	public static final String EXCEEDS_WITHDRAWL_FRQUENCY_REPONSE_CODE = "65";
	public static final String EXCEEDS_LIMIT_REPONSE_CODE = "61";
	public static final Object INVALID_CVD1_MATCH_REPONSE_CODE = "05";
	public static final Object INVALID_PIN = "55";
	public static final String INSUFFICIENT_FUND = "51";
	public static final String PIN_ATTEMPT_EXCEEDS = "75";
	public static final String ECOM_TXN_POINT_OF_SERVICE_CINDITION_CODE = "59";
	public static final Object INVALID_CVD2_MATCH_REPONSE_CODE = "05";
	public static final String COMPLAINCE_ISSUE_RESPONSE_CODE = "CA";

	public static final String POS_AUTHORIZATION_URL = "pos.authorizationurl";

	public static final String POS_REVERSAL_URL = "pos.reversalurl";

	public static final String POS_ADVISE_URL = "pos.adviseurl";
	public static final String POS_SERVER_AUTH_URL = "pos.serverauthurl";

}
