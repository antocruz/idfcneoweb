package com.neo.aggregator.constant;

public class TransactionTypeConstants {

	public static final String POS = "POS";
	public static final String ATM = "ATM";
	public static final String MICRO_ATM = "MICRO_ATM";
	public static final String CASH_AT_POS = "CASH_AT_POS";
	public static final String ECOM = "ECOM";
	public static final String LOAD_ONLINE = "LOAD_ONLINE";
	public static final String SERVICE_CREATION = "SERVICE_CREATION";
	public static final String QUASI_CASH = "QUASI_CASH";
	public static final String REFUND = "REFUND";
	public static final String PURCHASE_CASH_BACK = "PURCHASE_CASH_BACK";
	public static final String POS_REVERSAL = "POS_REVERSAL";
	public static final String ATM_REVERSAL = "ATM_REVERSAL";
	public static final String MICRO_ATM_REVERSAL = "MICRO_ATM_REVERSAL";
	public static final String CASH_AT_POS_REVERSAL = "CASH_AT_POS_REVERSAL";
	public static final String ECOM_REVERSAL = "ECOM_REVERSAL";
	public static final String LOAD_ONLINE_REVERSAL = "LOAD_ONLINE_REVERSAL";
	public static final String REFUND_REVERSAL = "REFUND_REVERSAL";
	public static final String ATM_BALANCE_ENQUIRY = "ATM_BALANCE_ENQUIRY";
	public static final String ATM_MINI_STATEMENT = "ATM_MINI_STATEMENT";

}
