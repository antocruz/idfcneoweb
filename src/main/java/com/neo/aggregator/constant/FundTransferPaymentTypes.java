package com.neo.aggregator.constant;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
public enum FundTransferPaymentTypes {
    P2P("CUSTOMER"),P2M("MERCHANT");

    String entityType;

    FundTransferPaymentTypes(String entityType) {
        this.entityType = entityType;
    }

    static Map<String, FundTransferPaymentTypes> map = new HashMap<>();

    static {
        for(FundTransferPaymentTypes type:FundTransferPaymentTypes.values()){
            map.put(type.getEntityType(),type);
        }
    }

    static FundTransferPaymentTypes getType(String entityType){
        return map.get(entityType);
    }
}
