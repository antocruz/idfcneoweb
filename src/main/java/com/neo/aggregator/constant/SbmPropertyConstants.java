package com.neo.aggregator.constant;

public class SbmPropertyConstants {

	private SbmPropertyConstants() {
		throw new IllegalStateException("Constants class");
	}

	public static final String SERVICE_REQUEST_VERSION = "10.2";
	public static final String SERVICE_REQUEST_CHANNELID = "M2P";
	public static final String SERVICE_REQUEST_BANKID = "SBMIN";
	public static final String ESB_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String ESB_LOCAL_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss:SSSS";
	public static final String CBS_LOCAL_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
	
	public static final String RETAIL_CIF_CREATION_XSD = "http://www.finacle.com/fixml RetCustAdd.xsd";
	public static final String SAVINGS_ACCOUNT_OPENING_XSD = "http://www.finacle.com/fixml SBAcctAdd.xsd";
	public static final String BALANCE_INQUIRY_XSD = "http://www.finacle.com/fixml BalInq.xsd";
	public static final String FETCH_TRANSACTION_XSD = "http://www.finacle.com/fixml getFullAccountStatementWithPagination.xsd";
	public static final String TD_FIXED_ACCOUNT_OPENING_XSD = "http://www.finacle.com/fixml TDAcctAdd.xsd";
	public static final String OUTBOUND_TRANSACTION_XSD = "http://www.finacle.com/fixml AddOutboundPymtEntryDtls.xsd";
	public static final String INTERNAL_TRANSFER_XSD = "http://www.finacle.com/fixml XferTrnAdd.xsd";
	public static final String CREDIT_ADD_XSD = "http://www.finacle.com/fixml CreditAdd.xsd";
	public static final String DEBIT_ADD_XSD = "http://www.finacle.com/fixml DebitAdd.xsd";
	public static final String TD_FIXED_ACCOUNT_CLOSE_XSD = "http://www.finacle.com/fixml DepAcctClose.xsd";
	public static final String TD_ACCOUNT_INQUIRY_XSD = "http://www.finacle.com/fixml TDAcctInq.xsd";
	public static final String ACCOUNT_LIEN_ADD_XSD = "http://www.finacle.com/fixml AcctLienAdd.xsd";
	public static final String TD_FIXED_ACCOUNT_TRAIL_CLOSE_XSD = "http://www.finacle.com/fixml executeFinacleScript.xsd";
	public static final String SAVINGS_ACCOUNT_INQUIRY_XSD = "http://www.finacle.com/fixml SBAcctInq.xsd";
	public static final String SAVINGS_ACCOUNT_MODIFICATION_XSD = "http://www.finacle.com/fixml SBAcctMod.xsd";
	public static final String FETCH_MINISTATEMENT_XSD = "http://www.finacle.com/fixml getMiniAccountStatement.xsd";
	

	public static final String RETAIL_CIF_CREATION = "RetCustAdd";
	public static final String RETAIL_CIF_INQUIRY = "RetCustInq";
	public static final String RETAIL_CIF_MODIFICATION = "RetCustMod";
	public static final String CORPORATE_CIF_CREATION = "createCorporateCustomer";
	public static final String CORPORATE_CIF_INQUIRY = "getCorporateCustomerDetails";
	public static final String CORPORATE_CIF_MODIFICATION = "updateCorpCustomer";
	public static final String SAVINGS_ACCOUNT_OPENING = "SBAcctAdd";
	public static final String CURRENT_ACCOUNT_OPENING = "CAAcctAdd";
	public static final String CHEQUE_ACCOUNT_OPENING = "ChkbkAdd";
	public static final String STOP_CHEQUE = "StopChkAdd";
	public static final String VERIFY_CORPORATE_CIF = "verifyCustomerDetails";
	public static final String VERIFY_RETAIL_CIF = "verifyCustomerDetails";
	public static final String BALANCE_INQUIRY = "BalInq";
	public static final String FETCH_TRANSACTION = "getFullAccountStatementWithPagination";
	public static final String FETCH_MINISTATEMENT = "getMiniAccountStatement";
	public static final String INTERNAL_TRANSFER = "XferTrnAdd";
	public static final String OVERDRAFT_ACCOUNT_OPENING = "ODAcctAdd";
	public static final String SAVINGS_ACCOUNT_MODIFICATION = "SBAcctMod";
	public static final String CURRENT_ACCOUNT_MODIFICATION = "CAAcctMod";
	public static final String OVERDRAFT_ACCOUNT_MODIFICATION = "ODAcctMod";
	public static final String SAVINGS_ACCOUNT_INQUIRY = "SBAcctInq";
	public static final String CURRENT_ACCOUNT_INQUIRY = "CAAcctInq";
	public static final String OVERDRAFT_ACCOUNT_INQUIRY = "ODAcctInq";
	public static final String TD_FIXED_ACCOUNT_OPENING = "TDAcctAdd";
	public static final String TD_RECURRING_ACCOUNT_OPENING = "TDAcctAdd";
	public static final String TD_ACCOUNT_INQUIRY = "TDAcctInq";
	public static final String LOAN_ACCOUNT_OPENING = "LoanAcctAdd";
	public static final String LOAN_ACCOUNT_INQUIRY = "LoanAcctInq";
	public static final String STANDING_INSTRUCTION_CREATION = "RecPmtAdd";
	public static final String STANDING_INSTRUCTION_MODIFICATION = "RecPmtMod";
	public static final String STANDING_INSTRUCTION_INQUIRY = "RecPmtInq";
	public static final String STANDING_INSTRUCTION_DELETION = "RecPmtDel";
	public static final String DEDUPE_CIF_CHECK = "executeFinacleScript";
	public static final String OUTBOUND_TRANSACTION = "AddOutboundPymtEntryDtls";
	public static final String CREDIT_ADD = "CreditAdd";
	public static final String DEBIT_ADD = "DebitAdd";
	public static final String TD_FIXED_ACCOUNT_CLOSE = "DepAcctClose";
	public static final String ACCOUNT_LIEN_ADD = "AcctLienAdd";
	public static final String TD_FIXED_ACCOUNT_TRAIL_CLOSE = "executeFinacleScript";
	
	public static final String ADDRESS_TYPE_HOME = "Home";
	public static final String ADDRESS_TYPE_MAILING = "Mailing";
	public static final String ADDRESS_TYPE_NRE_RELATIVE = "NRERelative";
	public static final String ADDRESS_TYPE_WORK = "Work";

	public static final String LANGUAGE_INDIA_ENGLISH = "India (English)";
	public static final String LANGUAGE_LOCALE_ENGLISH_US = "en_US";


	public static final String AGRICULTURE_FOOD_NATURAL_RESOURCES = "A";
	public static final String ARCHITECTURE_AND_CONSTRUCTION = "C";
	public static final String MANUFACTURING = "D";
	public static final String ARTS_AUDIO_OR_VIDEO_TECHNOLOGY_AND_COMMUNICATIONS = "O";
	public static final String BUSINESS_MANAGEMENT_AND_ADMINISTRATION = "K";
	public static final String EDUCATION_AND_TRAINING = "M";
	public static final String FINANCE = "J";
	public static final String GOVERNMENT_AND_PUBLIC_ADMINISTRATION = "L";
	public static final String HEALTH_SCIENCE = "N";
	public static final String HOSPITALITY_AND_TOURISM = "H";
	public static final String HUMAN_SERVICES = "N";
	public static final String INFORMATION_TECHNOLOGY = "J";
	public static final String LAW_PUBLIC_SAFETY_CORRECTIONS_AND_SECURITY = "N";
	public static final String MARKETING_SALES_AND_SERVICE = "J";
	public static final String SCIENCE_TECHNOLOGY_ENGINEERING_AND_MATHEMATICS = "M";
	public static final String TRANSPORTATION_DISTRIBUTION_AND_LOGISTICS = "I";

	public static final String EMAIL_TYPE_COMMUNICATION = "COMMEML";
	public static final String PHONE_TYPE_COMMUNICATION = "CELLPH";
	public static final String EMAIL_TYPE_WORK = "WORKEML";
	public static final String EMAIL_TYPE_HOME = "HOMEEML";

	public static final String EMPLOYED = "Employed";
	public static final String UNEMPLOYED = "Unemployed";
	public static final String ENTREPRENEUR = "Self employed";
	public static final String PUBLIC_SECTOR_EMPLOYEE = "Employed";
	public static final String FREELANCER = "Self employed";
	public static final String HOUSEWORK = "Housewife";
	public static final String APPRENTICE = "Other";
	public static final String RETIRED = "Retired";
	public static final String STUDENT = "Unemployed";
	public static final String SELF_EMPLOYED = "Self employed";
	public static final String MILITARY_OR_COMMUNITY_SERVICE = "Employed";

	public static final String FATCA_COUNTRY_IN = "IN";
	public static final String FATCA_COUNTRY_US = "US";
	public static final String FATCA_STATUS_ACTIVE = "ACT";
	public static final String FATCA_STATUS_DORMANT = "DOR";

	public static final String MARITAL_STATUS_DIVORCED = "DIVOR";
	public static final String MARITAL_STATUS_LEGALLY_SEPERATED = "LEGSP";
	public static final String MARITAL_STATUS_LIVEIN = "LIVTO";
	public static final String MARITAL_STATUS_MARRIED = "MARR";
	public static final String MARITAL_STATUS_UNMARRIED = "UNMAR";
	public static final String MARITAL_STATUS_WIDOWER = "WIDWR";
	public static final String MARITAL_STATUS_WIDOW = "WIDOW";

	public static final String MANAGER_ID = "GP000226";

	public static final String MINOR_GUARD_COURT_APPOINTED = "COURT APPOINTED";
	public static final String MINOR_GUARD_DEFACTO = "DEFACTO GUARDIAN";
	public static final String MINOR_GUARD_FATHER = "FATHER";
	public static final String MINOR_GUARD_MOTHER = "MOTHER";
	public static final String MINOR_GUARD_OTHERS = "OTHERS";

	public static final String SALUTATION_MR = "MR.";
	public static final String SALUTATION_MRS = "MRS.";
	public static final String SALUTATION_DR = "DR.";
	public static final String SALUTATION_MS = "MS.";
	public static final String SALUTATION_MISS = "MISS.";
	public static final String SALUTATION_MASTER = "MAST.";
	public static final String SALUTATION_PROF = "PROF.";

	public static final String COMMUNICATION_TYPE_PHONE = "PHONE";
	public static final String COMMUNICATION_TYPE_EMAIL = "EMAIL";

	public static final String REGION_RURAL = "01";
	public static final String REGION_SEMIURBAN = "02";
	public static final String REGION_URBAN = "03";
	public static final String REGION_METRO = "04";

	public static final String SECTOR_AGRICULTURE_FOOD_NATURAL_RESOURCES = "1";
	public static final String SECTOR_ARCHITECTURE_AND_CONSTRUCTION = "45";
	public static final String SECTOR_MANUFACTURING = "15";
	public static final String SECTOR_ARTS_AUDIO_OR_VIDEO_TECHNOLOGY_AND_COMMUNICATIONS = "64";
	public static final String SECTOR_BUSINESS_MANAGEMENT_AND_ADMINISTRATION = "74";
	public static final String SECTOR_EDUCATION_AND_TRAINING = "80";
	public static final String SECTOR_FINANCE = "67";
	public static final String SECTOR_GOVERNMENT_AND_PUBLIC_ADMINISTRATION = "75";
	public static final String SECTOR_HEALTH_SCIENCE = "85";
	public static final String SECTOR_HOSPITALITY_AND_TOURISM = "55";
	public static final String SECTOR_HUMAN_SERVICES = "99";
	public static final String SECTOR_INFORMATION_TECHNOLOGY = "72";
	public static final String SECTOR_LAW_PUBLIC_SAFETY_CORRECTIONS_AND_SECURITY = "99";
	public static final String SECTOR_MARKETING_SALES_AND_SERVICE = "74";
	public static final String SECTOR_SCIENCE_TECHNOLOGY_ENGINEERING_AND_MATHEMATICS = "73";
	public static final String SECTOR_TRANSPORTATION_DISTRIBUTION_AND_LOGISTICS = "63";
	public static final String SECTOR_MISC = "99";

	public static final String SUB_SECTOR_AGRICULTURE_FOOD_NATURAL_RESOURCES = "1157";
	public static final String SUB_SECTOR_ARCHITECTURE_AND_CONSTRUCTION = "45001";
	public static final String SUB_SECTOR_MANUFACTURING = "15409";
	public static final String SUB_SECTOR_ARTS_AUDIO_OR_VIDEO_TECHNOLOGY_AND_COMMUNICATIONS = "64201";
	public static final String SUB_SECTOR_BUSINESS_MANAGEMENT_AND_ADMINISTRATION = "74901";
	public static final String SUB_SECTOR_EDUCATION_AND_TRAINING = "800002";
	public static final String SUB_SECTOR_FINANCE = "67104";
	public static final String SUB_SECTOR_GOVERNMENT_AND_PUBLIC_ADMINISTRATION = "75001";
	public static final String SUB_SECTOR_HEALTH_SCIENCE = "85101";
	public static final String SUB_SECTOR_HOSPITALITY_AND_TOURISM = "55101";
	public static final String SUB_SECTOR_HUMAN_SERVICES = "99999";
	public static final String SUB_SECTOR_INFORMATION_TECHNOLOGY = "72209";
	public static final String SUB_SECTOR_LAW_PUBLIC_SAFETY_CORRECTIONS_AND_SECURITY = "99999";
	public static final String SUB_SECTOR_MARKETING_SALES_AND_SERVICE = "74301";
	public static final String SUB_SECTOR_SCIENCE_TECHNOLOGY_ENGINEERING_AND_MATHEMATICS = "73001";
	public static final String SUB_SECTOR_TRANSPORTATION_DISTRIBUTION_AND_LOGISTICS = "63011";
	public static final String SUB_SECTOR_MISC = "99999";

	public static final String SEGMENTATION_CLASS_A = "CLSA";
	public static final String SEGMENTATION_CLASS_B = "CLSB";
	public static final String SEGMENTATION_CLASS_GOLD = "GOLD";
	public static final String SEGMENTATION_CLASS_HIGH_NW = "HNW";
	public static final String SEGMENTATION_PREFERRED_CATEGORY = "PREFC";
	public static final String SEGMENTATION_CLASS_SILVER = "SILVER";

	public static final String SEGMENTATION_SUBCLASS_CLASS_A = "Sub Class A";
	public static final String SEGMENTATION_SUBCLASS_CLASS_AA = "Sub Class AA";
	public static final String SEGMENTATION_SUBCLASS_CLASS_B = "Sub Class B";

	public static final String TAX_SLAB_NOPAN = "NOPAN";
	public static final String TAX_SLAB_SRTAX = "SRTAX";
	public static final String TAX_SLAB_TD15G = "TD15G";
	public static final String TAX_SLAB_TDTAX = "TDTAX";
	public static final String TAX_SLAB_TDZER = "TDZER";

	public static final String IDTYPE_CODE_PAN = "PAN";
	public static final String DOCTYPE_CODE_PAN = "PAN";
	public static final String IDTYPE_CODE_RETAIL = "RETAIL";
	public static final String IDTYPE_CODE_CKYC = "CKYC_ID";
	public static final String DOCTYPE_CODE_FORM60 = "FRM60";
	public static final String DOCTYPE_CODE_OTHER = "OTH-R";
	public static final String DOCTYPE_CODE_PASSP = "PASSP";
	public static final String DOCTYPE_CODE_CKYC = "CKYC";
	public static final String ISSUED_ORG_INCOMETAX_DEPARTMENT = "Income Tax Department";
	public static final String ISSUED_ORG_MINISTRY_OF_EXT_AFFAIRS = "Ministry of External Affairs";
	public static final String ISSUED_ORG_CENTRAL_KYC_REGISTRY = "CENTRAL KYC REGISTRY";
	public static final String INCOME_STABLE = "Stable";
	public static final String INCOME_UNSTABLE = "UnStable";

	public static final String RELATIONSHIP_INTRODUCER = "Introducer";
	public static final String RELATIONSHIP_REFERENCE = "Reference";

	public static final String INTCODE_OPERATIVE_AC = "O";
	public static final String INTCODE_ORIGINAL_AC = "S";
	public static final String INTCODE_PAYMENT_SYSTEM = "T";
	public static final String INTCODE_PARKING_AC = "L";

	public static final String CUSTOMER_TYPE_INDIVIDUAL_MALE = "41";
	public static final String CUSTOMER_TYPE_INDIVIDUAL_FEMALE = "42";

	public static final String RELPARTY_FATHER= "001";
	public static final String RELPARTY_GRAND_FATHER = "002";
	public static final String RELPARTY_GRAND_MOTHER = "003";
	public static final String RELPARTY_MOTHER = "004";
	public static final String RELPARTY_SON = "005";
	public static final String RELPARTY_DAUGHTER = "006";
	public static final String RELPARTY_BROTHER = "007";
	public static final String RELPARTY_SISTER = "008";
	public static final String RELPARTY_PARTNERS = "009";
	public static final String RELPARTY_DIRECTOR = "010";
	public static final String RELPARTY_GUARANTOR = "011";
	public static final String RELPARTY_KARTA = "012";
	public static final String RELPARTY_CO_PARCENERS = "013";
	public static final String RELPARTY_NO_RELATION = "014";
	public static final String RELPARTY_NATURAL_GUARDIAN = "015";
	public static final String RELPARTY_LEGAL_GUARDIAN = "016";
	public static final String RELPARTY_WIFE = "017";
	public static final String RELPARTY_HUSBAND = "018";
	public static final String RELPARTY_POWER_OF_ATTORNEY_HOLDER = "019";
	public static final String RELPARTY_EXECUTOR = "020";
	public static final String RELPARTY_ADMINISTRATOR = "021";
	public static final String RELPARTY_MANAGER = "022";
	public static final String RELPARTY_MANDATE_HOLDER = "023";
	public static final String RELPARTY_OTHERS= "999";


}
