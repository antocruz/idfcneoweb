package com.neo.aggregator.constant;

public class NeoBankingServicesConstants {

	public static final String AadharOTPGen = "AADHAROTPGENERATION";
	public static final String AadharOTPVerify = "AADHAROTPVERIFICATION";
	public static final String RetailCIFCreation = "RETAILCIFCREATION";
	public static final String SBAccountCreation="SBACNTCREATION";
	public static final String CAAccountCreation="CAACNTCREATION";
	public static final String FDCreation="FDCREATION";
	public static final String RDCreation="RDCREATION";


}
