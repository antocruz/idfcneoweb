package com.neo.aggregator.constant;

import lombok.Getter;

@Getter
public enum FundTransferTypes {
    UPI_PAYOUT_CREDIT("UPI","Pay","VPA");

    String transferType;
    String transactionType;
    String vpaType;

    FundTransferTypes(String transferType, String transactionType, String vpaType) {
        this.transferType = transferType;
        this.transactionType = transactionType;
        this.vpaType = vpaType;
    }
}
