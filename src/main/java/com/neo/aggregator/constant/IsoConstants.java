package com.neo.aggregator.constant;

public class IsoConstants {
	public static final String HEADER_LENGTH 		= "HEADER_LENGTH";
	public static final String HEADER_FLAG_FORMAT	= "HEADER_FLAG_FORMAT";
	public static final String TEXT_FORMAT 			= "TEXT_FORMAT";
	public static final String TOT_MSG_LNT 			= "TOT_MSG_LNT";
	public static final String DST_STAT_ID 			= "DST_STAT_ID";
	public static final String SRC_STAT_ID 			= "SRC_STAT_ID";
	public static final String RND_CON_INF 			= "RND_CON_INF";
	public static final String BASE_1_FLAG 			= "BASE_1_FLAG";
	public static final String MSG_STATUS_FLAG 		= "MSG_STATUS_FLAG";
	public static final String BAT_NO 				= "BAT_NO";
	public static final String RESERVED 			= "RESERVED";
	public static final String USR_INFO 			= "USR_INFO";
	public static final String REJ_HEADER_LENGTH 	= "REJ_HEADER_LENGTH";
	public static final String REJ_HEADER_FLAG_FRMT = "REJ_HEADER_FLAG_FORMAT";
	public static final String REJ_TEXT_FORMAT 		= "REJ_TEXT_FORMAT";
	public static final String REJ_TOT_MSG_LNT 		= "REJ_TOT_MSG_LNT";
	public static final String REJ_DST_STAT_ID 		= "REJ_DST_STAT_ID";
	public static final String REJ_SRC_STAT_ID 		= "REJ_SRC_STAT_ID";
	public static final String REJ_RND_CON_INF 		= "REJ_RND_CON_INF";
	public static final String REJ_BASE_1_FLAG 		= "REJ_BASE_1_FLAG";
	public static final String REJ_MSG_STATUS_FLAG 	= "REJ_MSG_STATUS_FLAG";
	public static final String REJ_BAT_NO 			= "REJ_BAT_NO";
	public static final String REJ_RESERVED 		= "REJ_RESERVED";
	public static final String REJ_USR_INFO 		= "REJ_USR_INFO";
	public static final String REJ_REASON 			= "REJ_REASON";
}
