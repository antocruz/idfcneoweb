package com.neo.aggregator.constant;

public class NeoCustomFieldConstants {
	
	private NeoCustomFieldConstants() {
		throw new IllegalStateException("Constants class");
	}

	public static final String DEFAULT_ADDRESS_COUNTRY = "default.address.country";
	public static final String ADDRESS_TRIM_REQUIRED = "address.trim.required";
	public static final String ADDRESS_TRIM_LIMIT = "address.trim.limit";

	public static final String DEFAULT_ISO_AUTH_TIMEOUT = "default.iso.auth.timeout";
	public static final String SESSION_INTERVAL_MILLS = "session.interval.mills";
	public static final String SAVINGS_ACCOUNT_DEBIT_FREEZE_ENABLED = "savings.debit.freeze.enabled";
	public static final String TD_LIEN_MODULE_TYPE = "td.lien.module.type";
	public static final String TD_LIEN_REASON_CODE = "td.lien.reason.code";
	public static final String TD_LIEN_CHANNEL_ID = "td.lien.channel.id";
	public static final String ISO_TIMEOUT_MILLIS = "iso.timeout.mills";
	
	public static final String NETWORK_REVERSAL_URL = "network.reversal.url";

	public static final String ICICI_CORP_ID = "icici.corp.id";
	public static final String ICICI_CORP_USER = "icici.corp.user";
	public static final String ICICI_AGGR_ID = "icici.aggr.id";
	public static final String ICICI_AGGR_NAME = "icici.aggr.name";
	public static final String ICICI_BC_ID = "icici.bc.id";
	public static final String ICICI_IMPS_PASSCODE = "icici.imps.passcode";
	public static final String ICICI_UPI_PAYERVPA = "icici.upi.payervpa";
	public static final String ICICI_CORP_URN = "icici.corp.urn";
	public static final String ICICI_ENCRYPTION_MODE = "icici.encryption.mode";

	public static final String ICICI_CIB_APIKEY = "icici.cib.apikey";
	public static final String ICICI_CPCS_APIKEY = "icici.cpcs.apikey";
	public static final String ICICI_FUNDTRF_APIKEY = "icici.fundtrf.apikey";
	public static final String ICICI_PAYLATER_APIKEY = "icici.paylater.apikey";
	public static final String ICICI_POOLDR_NODALCR_APIKEY = "icici.poolDr.nodalCr.apikey";
	public static final String ICICI_POOLDR_CURRCR_APIKEY = "icici.poolDr.currCr.apikey";
	public static final String ICICI_POOLDR_ESCROWCR_APIKEY = "icici.poolDr.escrowCr.apikey";
	public static final String ICICI_CURRDR_POOLCR_APIKEY = "icici.currDr.poolCr.apikey";
	public static final String ICICI_ESCROWDR_POOLCR_APIKEY = "icici.escrowDr.poolCr.apikey";
	public static final String ICICI_NODALDR_POOLCR_APIKEY = "icici.nodalDr.poolCr.apikey";
	public static final String ICICI_FT_REALTIME_SETTLEMENT="icici.ft.realtime.settlement";
	public static final String RBL_PRODUCT_REGISTER = "rbl.product.register";
	public static final String TD_CREATION_RENEWAL_TENURE_DAYS="td.creation.renewal.tenure.days";
	public static final String FT_STATUS_NOTIFY="ft.status.notify";

	public static final String NOTIFY_ECOLLECT_AUTH="notify.ecollect.auth";
	public static final String ICICI_UPI_REFUND_APIKEY="icici.upi.refund.apikey";

	public static final String IMPS_MODE="imps.mode";

	public static final String ICICI_FT_CHANNEL_CODE = "icici.ft.channel.code";
	public static final String ICICI_FT_DEVICE_ID = "icici.ft.device.id";
	public static final String ICICI_FT_PROFILE_ID="icici.ft.profile.id";
	public static final String ICICI_FT_PREAPPROVED="icici.ft.preapproved";
	public static final String ICICI_FT_RETAILER_CODE="icici.ft.retailer.code";
	public static final String ICICI_FT_ACCOUNT_PROVIDER="icici.ft.account.provider";
	public static final String ICICI_FT_WORKFLOW_REQD="icici.ft.workflow.required";
	public static final String ICICI_FT_UPI_TXNTYPE="icici.ft.upi.txntype";
	public static final String ICICI_FT_UPI_MERCHANTTYPE="icici.ft.upi.merchanttype";
	public static final String ICICI_FT_UPI_MCC="icici.ft.upi.mcc";
	public static final String YBL_ACCOUNT_NO = "ybl.account.no";
	public static final String YBL_USERNAME = "ybl.username";
	public static final String YBL_PASSWORD = "ybl.password";
	public static final String YBL_APPID = "ybl.appid";
	public static final String YBL_CUSTOMERID = "ybl.customerid";
	public static final String YBL_VERSION = "ybl.version";
	public static final String YBL_CLIENT_ID = "ybl.clientid";
	public static final String YBL_CLIENT_SECRET = "ybl.clientsecret";

	public static final String API_TIMEOUT="api.timeout";
	public static final String ICICI_FT_UPI_MOBILE = "icici.ft.upi.mobile";
	public static final String RBL_VA_RETRY_TENANTS = "rbl.va.retry.tenants";
	public static final String RBL_RECON_TENANTS = "rbl.recon.tenants";
	public static final String CRON_EXPRESSION = "neo.cron.expression";
	public static final String DEFAULT_PREFERRED_DOCUMENT_TYPE = "default.preferred.document.type";

}
