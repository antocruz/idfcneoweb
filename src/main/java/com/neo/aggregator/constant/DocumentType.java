package com.neo.aggregator.constant;

public enum DocumentType {
	ADDRESS_PROOF, ID_PROOF, ACK_PROOF, RC_DOCUMENT, ACCOUNT_OPENING_FORM, FORM_60_61
}
