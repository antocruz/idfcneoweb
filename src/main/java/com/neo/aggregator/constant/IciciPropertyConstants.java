package com.neo.aggregator.constant;

public class IciciPropertyConstants {
    public static String ICICI_PUBLIC_KEY="icici.rsa.apikey";
    public static String ICICI_M2P_4096KEY="neo.yappay";
    public static String ICICI_M2P_2048KEY="yappay.ssl";
    public static String CHANNEL_CODE = "MICICI";
    public static String DEVICE_ID = "400432400432400432400432";
    public static String PROFILE_ID="2996298";
    public static String SEQNO_PREFIX="ICI";
    public static String PREAPPROVED="P";
    public static String RETAILER_CODE="rcode";
    public static String ACCOUNT_PROVIDER="74";

    public static String WORKFLOW_REQD="N";

    public static String X_PRIORITY_UPI="1000";
    public static String X_PRIORITY_IMPS="0100";
    public static String X_PRIORITY_NEFT="0010";
    public static String X_PRIORITY_RTGS="0001";

    public static  String SUCCESS_MSG="SUCCESS";
    public static  String FAILURE_MSG="FAILED";
    public static  String INPROGRESS_MSG="IN PROGRESS";

    public static String CPCS_API_KEY="f998c467-4ceb-4ae0-bc94-cf573ab69cba";
    public static String PAYLATER_API_KEY="";
    public static String CIB_API_KEY="";
    public static String FUNDTRF_API_KEY="";
}
