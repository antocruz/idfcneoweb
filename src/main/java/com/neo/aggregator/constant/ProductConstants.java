package com.neo.aggregator.constant;

public class ProductConstants {

	public static final String CIF_CREATION = "CIF_CREATION";
	public static final String SB_CREATION = "SB_CREATION";
	public static final String TD_CREATION = "TD_CREATION";

	public static final String ESB_LOGIN = "ESB_LOGIN";
	public static final String EKYC_ONETIMETOKEN = "EKYC_ONETIMETOKEN";
	public static final String EKYC_RURN = "EKYC_RURN";
	public static final String EKYC_VALIDATEBIO = "EKYC_VALIDATEBIO";
	public static final String EKYC_DEMOGRAPHICDATA = "EKYC_DEMOGRAPHICDATA";

	public static final String EKYC_VALIDATEAOTP = "EKYC_VALIDATEAOTP";
	public static final String EKYC_GENERATEAOTP = "EKYC_GENERATEAOTP";
	public static final String EKYC_VALIDATEPAN = "EKYC_VALIDATEPAN";
	
	public static final String RETAIL_CIF_CREATION = "RETAIL_CIF_CREATION";
	public static final String RETAIL_CIF_INQUIRY = "RETAIL_CIF_INQUIRY";
	public static final String RETAIL_CIF_MODIFICATION = "RETAIL_CIF_MODIFICATION";
	public static final String CORPORATE_CIF_CREATION = "CORPORATE_CIF_CREATION";
	public static final String CORPORATE_CIF_INQUIRY = "CORPORATE_CIF_INQUIRY";
	public static final String CORPORATE_CIF_MODIFICATION = "CORPORATE_CIF_MODIFICATION";
	public static final String SAVINGS_ACCOUNT_OPENING = "SAVINGS_ACCOUNT_OPENING";
	public static final String CURRENT_ACCOUNT_OPENING = "CURRENT_ACCOUNT_OPENING";
	public static final String CHEQUE_ACCOUNT_OPENING = "CHEQUE_ACCOUNT_OPENING";
	public static final String STOP_CHEQUE = "STOP_CHEQUE";
	public static final String VERIFY_CORPORATE_CIF = "VERIFY_CORPORATE_CIF";
	public static final String VERIFY_RETAIL_CIF = "VERIFY_RETAIL_CIF";
	public static final String BALANCE_INQUIRY = "BALANCE_INQUIRY";
	public static final String FETCH_TRANSACTION = "FETCH_TRANSACTION";
	public static final String FETCH_MINISTATEMENT = "FETCH_MINISTATEMENT";
	public static final String INTERNAL_TRANSFER = "INTERNAL_TRANSFER";
	public static final String INTERNAL_TRANSFER_OTP = "INTERNAL_TRANSFER_OTP";
	public static final String OVERDRAFT_ACCOUNT_OPENING = "OVERDRAFT_ACCOUNT_OPENING";
	public static final String SAVINGS_ACCOUNT_MODIFICATION = "SAVINGS_ACCOUNT_MODIFICATION";
	public static final String CURRENT_ACCOUNT_MODIFICATION = "CURRENT_ACCOUNT_MODIFICATION";
	public static final String OVERDRAFT_ACCOUNT_MODIFICATION = "OVERDRAFT_ACCOUNT_MODIFICATION";
	public static final String SAVINGS_ACCOUNT_INQUIRY = "SAVINGS_ACCOUNT_INQUIRY";
	public static final String CURRENT_ACCOUNT_INQUIRY = "CURRENT_ACCOUNT_INQUIRY";
	public static final String OVERDRAFT_ACCOUNT_INQUIRY = "OVERDRAFT_ACCOUNT_INQUIRY";
	public static final String TD_FIXED_ACCOUNT_OPENING = "TD_FIXED_ACCOUNT_OPENING";
	public static final String TD_RECURRING_ACCOUNT_OPENING = "TD_RECURRING_ACCOUNT_OPENING";
	public static final String TD_ACCOUNT_INQUIRY = "TD_ACCOUNT_INQUIRY";
	public static final String LOAN_ACCOUNT_OPENING = "LOAN_ACCOUNT_OPENING";
	public static final String LOAN_ACCOUNT_INQUIRY = "LOAN_ACCOUNT_INQUIRY";
	public static final String STANDING_INSTRUCTION_CREATION = "STANDING_INSTRUCTION_CREATION";
	public static final String STANDING_INSTRUCTION_MODIFICATION = "STANDING_INSTRUCTION_MODIFICATION";
	public static final String STANDING_INSTRUCTION_INQUIRY = "STANDING_INSTRUCTION_INQUIRY";
	public static final String STANDING_INSTRUCTION_DELETION = "STANDING_INSTRUCTION_DELETION";
	public static final String DEDUPE_CIF_CHECK = "DEDUPE_CIF_CHECK";
	public static final String OUTBOUND_TRANSACTION_NEFT = "OUTBOUND_TRANSACTION_NEFT";
	public static final String OUTBOUND_TRANSACTION_RTGS = "OUTBOUND_TRANSACTION_RTGS";
	public static final String OUTBOUND_TRANSACTION_IMPS = "OUTBOUND_TRANSACTION_IMPS";
	public static final String CREDIT_ADD = "CREDIT_ADD";
	public static final String DEBIT_ADD = "DEBIT_ADD";
	public static final String TD_FIXED_ACCOUNT_CLOSE = "TD_FIXED_ACCOUNT_CLOSE";
	public static final String ACCOUNT_LIEN_ADD = "ACCOUNT_LIEN_ADD";
	public static final String TD_FIXED_ACCOUNT_TRAIL_CLOSE = "TD_FIXED_ACCOUNT_TRAIL_CLOSE";
	public static final String FUNDTRANSFER_OTP = "FUNDTRANSFER_OTP";
	public static final String FUNDTRANSFER_VALIDATE_OTP = "FUNDTRANSFER_VALIDATE_OTP";
	public static final String FUNDTRANSFER_OTP_NEFT = "FUNDTRANSFER_OTP_NEFT";
	public static final String FUNDTRANSFER_OTP_RTGS = "FUNDTRANSFER_OTP_RTGS";
	
	public static final String FT_STATUS_CHECK="FT_STATUS_CHECK";
	public static final String FUND_TRANSFER="FUND_TRANSFER";

	public static final String EKYC_AADHARXML="EKYC_AADHARXML";

	public static final String OKYC_DATA_PUSH="OKYC_DATA_PUSH";

	public static final String CIB_CREATION="CIB_CREATION";
	public static final String GET_DETAILS = "GET_DETAILS";
	public static final String BENEFICIARY_ADDITION="BENEFICIARY_ADDITION";
	public static final String BENEFICIARY_VPA_ADDITION="BENEFICIARY_VPA_ADDITION";
	public static final String IMPS_STATUS_CHECK="IMPS_STATUS_CHECK";
	public static final String UPI_STATUS_CHECK="UPI_STATUS_CHECK";
	public static final String FT_INCREMENTAL_STATUS_CHECK="FT_INCREMENTAL_STATUS_CHECK";
	public static final String PAN_VALIDATION="PAN_VALIDATION";

	public static final String FT_POOLAC_DEBITCREDIT="FT_POOLAC_DEBITCREDIT";
	public  static final String DEDUP_CHECK="DEDUP_CHECK";

	public static final String PL_ACCOUNT_DISCOVERY = "PL_ACCOUNT_DISCOVERY";
	public static final String PL_ACCOUNT_BALANCEINQUIRY = "PL_ACCOUNT_BALANCEINQUIRY";
	public static final String PL_CREATE_OTP = "PL_CREATE_OTP";
	public static final String PL_VERIFY_OTP = "PL_VERIFY_OTP";
	public static final String PL_DEBIT_ORCHESTRATION = "PL_DEBIT_ORCHESTRATION";
	
	public static final String PRODUCT_REGISTRATION = "PRODUCT_REGISTRATION";
	public static final String GENERIC = "GENERIC";
	public static final String ECOLLECT = "ECOLLECT";
	public static final String UPI_COLLECT_REFUND="UPI_COLLECT_REFUND";
	public static final String SBM_ICICI_ECOLLECT="SBM_ICICI_ECOLLECT";
	
	public static final String OUTBOUND_TRANSACTION_IMPS_MOBILE = "OUTBOUND_TRANSACTION_IMPS_MOBILE";
	public static final String OUTBOUND_TRANSACTION_IMPS_BRANCH = "OUTBOUND_TRANSACTION_IMPS_BRANCH";
	public static final String OUTBOUND_TRANSACTION_IMPS_FIR = "OUTBOUND_TRANSACTION_IMPS_FIR";

	public static final String ICICI_ECOLLECT="ICICI_ECOLLECT";

	public static final String RBL_RECON = "RBL_RECON";
	
	public static final String NEO_SUCCESS = "SUCCESS";
	public static final String NEO_FAILURE = "FAILURE";
}
