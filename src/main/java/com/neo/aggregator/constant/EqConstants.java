package com.neo.aggregator.constant;

public class EqConstants {

	private EqConstants() {
		
	}
	
	public static final String MR = "Mr";
	public static final String MRS = "Mrs";
	public static final String MISS = "Miss";
	public static final String SINGLE = "Single";
	public static final String MARRIED = "Married";
	public static final String PAN = "PAN";
	public static final String FORM_60 = "Form 60";
}
