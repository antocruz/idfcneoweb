package com.neo.aggregator.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource({
        "classpath:rblConstants/CustomerOnboarding-${neo.agg.rbl.env:prod}.properties"
})
public class RblPropertyConstants {

    public static String KYC_TYPE_BIO = "1";
    public static String KYC_TYPE_XML ="2";
    public static String KYC_TYPE_VKYC ="3";
    public static String KYC_TYPE_PKYC ="4";

    public static String CUSTOMER_STATUS_INDIVIDUAL = "1";
    public static String CUSTOMER_STATUS_NONINDIVIDUAL = "2";

    public static String CUSTOMER_TYPE_SALARIED;
    public static String CUSTOMER_TYPE_SELFEMPLOYED;
    public static String CUSTOMER_TYPE_FARMER;
    public static String CUSTOMER_TYPE_HOUSEWIFE;
    public static String CUSTOMER_TYPE_MINOR;

    public static String SOURCEINCOME_GOVT;
    public static String SOURCEINCOME_PUBLIC;
    public static String SOURCEINCOME_PRIVATE;
    public static String SOURCEINCOME_BUSINESS;
    public static String SOURCEINCOME_AGRICULTURE;
    public static String SOURCEINCOME_DEPENDANT;

    public static String ANNUALINCOME_0L_2L;
    public static String ANNUALINCOME_2L_5L;
    public static String ANNUALINCOME_5L_10L;
    public static String ANNUALINCOME_ABOVE_10L;


    @Value("${CUSTOMER_TYPE_SALARIED}")
    public void setCustomerTypeSalaried(String val) {
        RblPropertyConstants.CUSTOMER_TYPE_SALARIED = val;
    }

    @Value("${CUSTOMER_TYPE_SELFEMPLOYED}")
    public void setCustomerTypeSelfemployed(String val) {
        RblPropertyConstants.CUSTOMER_TYPE_SELFEMPLOYED = val;
    }

    @Value("${CUSTOMER_TYPE_FARMER}")
    public void setCustomerTypeFarmer(String val) {
        RblPropertyConstants.CUSTOMER_TYPE_FARMER = val;
    }

    @Value("${CUSTOMER_TYPE_HOUSEWIFE}")
    public void setCustomerTypeHousewife(String val) {
        RblPropertyConstants.CUSTOMER_TYPE_HOUSEWIFE = val;
    }

    @Value("${CUSTOMER_TYPE_MINOR}")
    public void setCustomerTypeMinor(String val) {
        RblPropertyConstants.CUSTOMER_TYPE_MINOR = val;
    }

    @Value("${SOURCEINCOME_GOVT}")
    public void setSourceIncomeGovt(String val) {
        RblPropertyConstants.SOURCEINCOME_GOVT = val;
    }

    @Value("${SOURCEINCOME_PUBLIC}")
    public void setSourceIncomePublic(String val) {
        RblPropertyConstants.SOURCEINCOME_PUBLIC = val;
    }

    @Value("${SOURCEINCOME_PRIVATE}")
    public void setSourceIncomePrivate(String val) {
        RblPropertyConstants.SOURCEINCOME_PRIVATE = val;
    }

    @Value("${SOURCEINCOME_BUSINESS}")
    public void setSourceIncomeBusiness(String val) {
        RblPropertyConstants.SOURCEINCOME_BUSINESS = val;
    }

    @Value("${SOURCEINCOME_AGRICULTURE}")
    public void setSourceIncomeAgriculture(String val) {
        RblPropertyConstants.SOURCEINCOME_AGRICULTURE = val;
    }

    @Value("${SOURCEINCOME_DEPENDANT}")
    public void setSourceIncomeDependant(String val) {
        RblPropertyConstants.SOURCEINCOME_DEPENDANT = val;
    }

    @Value("${ANNUALINCOME_0L_2L}")
    public void setAnnualIncome0L2L(String val) {
        RblPropertyConstants.ANNUALINCOME_0L_2L = val;
    }

    @Value("${ANNUALINCOME_2L_5L}")
    public void setAnnualIncome2L5L(String val) {
        RblPropertyConstants.ANNUALINCOME_2L_5L = val;
    }

    @Value("${ANNUALINCOME_5L_10L}")
    public void setAnnualIncome5L10L(String val) {
        RblPropertyConstants.ANNUALINCOME_5L_10L = val;
    }

    @Value("${ANNUALINCOME_ABOVE_10L}")
    public void setAnnualIncomeabove10L(String val) {
        RblPropertyConstants.ANNUALINCOME_ABOVE_10L = val;
    }


}
