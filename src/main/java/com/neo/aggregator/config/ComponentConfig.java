package com.neo.aggregator.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

import org.apache.activemq.command.ActiveMQQueue;
import org.dozer.DozerBeanMapper;
import org.hibernate.ConnectionReleaseMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.neo.aggregator.iso8583.codecs.EbcdicEncoder;
import com.neo.aggregator.utility.Iso8583;

import javax.jms.Queue;

@Configuration
@EnableAspectJAutoProxy
@EnableAsync
@ComponentScan(basePackages = { "com.neo" })
@EnableScheduling
@EnableCaching
@EnableTransactionManagement
public class ComponentConfig {

	@Autowired
	private DataSourceConfig dataSourceConfiguration;

	@SuppressWarnings("deprecation")
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean lemb = new LocalContainerEntityManagerFactoryBean();
		lemb.setDataSource(dataSourceConfiguration.dataSource());
		lemb.setJpaVendorAdapter(jpaVendorAdapter());
		lemb.setJpaDialect(new HibernateJpaDialect());
		Properties prop = new Properties();
		prop.put(org.hibernate.cfg.Environment.RELEASE_CONNECTIONS, ConnectionReleaseMode.AFTER_TRANSACTION);
		lemb.setJpaProperties(prop);
		lemb.setPackagesToScan(new String[] { "com.neo.aggregator" });
		return lemb;
	}

	@Bean
	public ResourceBundleMessageSource resourceBundleMessageSource() {
		ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
		resourceBundleMessageSource.setBasename("messages");
		return resourceBundleMessageSource;
	}

	@Bean
	public PlatformTransactionManager transactionManager() {
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager(entityManagerFactory().getObject());
		Map<String, String> jpaProperties = new HashMap<>();
		jpaProperties.put("transactionTimeout", "43200");
		jpaTransactionManager.setJpaPropertyMap(jpaProperties);
		return jpaTransactionManager;
	}

	public JpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
		jpaVendorAdapter.setShowSql(false);
		jpaVendorAdapter.setGenerateDdl(true);
		return jpaVendorAdapter;
	}

	public Map<String, String> jpaPropertyMap() {
		Map<String, String> map = new HashMap<>();
		return map;
	}

	@Bean
	@Qualifier("isoConcurrentMap")
	public ConcurrentHashMap<String, Iso8583> isoConcurrentMap() {
		return new ConcurrentHashMap<>();
	}

	@Bean
	public EbcdicEncoder ebcidicEncoder() {
		return new EbcdicEncoder();
	}
	
	@Bean(name = "neoPoolExecutor")
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(100);
		executor.setMaxPoolSize(2000);
		executor.setQueueCapacity(200);
		executor.setKeepAliveSeconds(40);
		executor.setThreadNamePrefix("neoAsyncCustomPoolExecutor-");
		executor.initialize();
		return executor;
	}
	
	@Bean
	public DozerBeanMapper dozerBeanMapper() {
		return new DozerBeanMapper();
	}

	@Bean
	@Qualifier("notificationAllQueue")
	public Queue queue() {
		return new ActiveMQQueue("m2p.notification.all");
	}
	
	@Bean
	@Qualifier("notificationAlert")
	public Queue notificationAlert() {
		return new ActiveMQQueue("m2p.notification.alert");
	}


}
