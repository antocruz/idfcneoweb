package com.neo.aggregator.config;

import com.neo.aggregator.iso8583.IsoMessage;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class PosServerMessageRequest {

	private String tenant;

	private IsoMessage requestMessage;

	private IsoMessage responseMessage;

}