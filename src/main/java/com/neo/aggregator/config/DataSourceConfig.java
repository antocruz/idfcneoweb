package com.neo.aggregator.config;

import javax.sql.DataSource;

public interface DataSourceConfig {
	DataSource dataSource();
}
