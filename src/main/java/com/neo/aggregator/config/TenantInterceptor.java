package com.neo.aggregator.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.utility.TenantContextHolder;

@Service("tenantInterceptor")
public class TenantInterceptor implements HandlerInterceptor {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(TenantInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if (!StringUtils.isEmpty(request.getHeader("TENANT"))) {
			logger.debug("Request Received From :: Tenant :::" + request.getHeader("TENANT"));
			TenantContextHolder.setTenant(request.getHeader("TENANT"));
		} else {
			TenantContextHolder.setTenant("default");
			logger.debug("Request Received From :: Tenant ::: default");
		}

		if (!StringUtils.isEmpty(request.getHeader("NEOTENANT"))) {
			logger.debug("Request Received From :: NEOTENANT :::" + request.getHeader("NEOTENANT"));
			TenantContextHolder.setNeoTenant(request.getHeader("NEOTENANT"));
		}

		if (!StringUtils.isEmpty(request.getHeader("BANKID"))) {
			logger.debug("Request Received From :: Bankid :::" + request.getHeader("BANKID"));
			TenantContextHolder.setBankId(request.getHeader("BANKID"));
		}

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}
}
