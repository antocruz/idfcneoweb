package com.neo.aggregator.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import com.neo.aggregator.utility.TenantContextHolder;

public class TenantAwareRoutingSource extends AbstractRoutingDataSource {

	@Override
	protected Object determineCurrentLookupKey() {
		return TenantContextHolder.getTenant();
	}

}
