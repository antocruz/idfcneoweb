package com.neo.aggregator.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.neo.aggregator.dao.IsoTenantRepo;
import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.jreactive8583.client.Iso8583Client;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.IsoTenant;

@Component
public class ClientInit implements InitializingBean {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(ClientInit.class);

	private Map<String, Iso8583Client<IsoMessage>> posIssuerClientMap = new ConcurrentHashMap<>();

	public Map<String, Iso8583Client<IsoMessage>> getPosIssuerClientMap() {
		return posIssuerClientMap;
	}

	@Autowired
	private PosIssuerClientConfig octIssuerClientConfig;

	@Autowired
	@Qualifier("tenantRepository")
	private IsoTenantRepo tenantDao;
	
	@Value("${neo.iso.connection.enabled:true}")
	private Boolean isISOEnabled;

	@Override
	public void afterPropertiesSet() {
		
		if(!isISOEnabled) {
			return;
		}

		try {

			Iterable<IsoTenant> tenantList = tenantDao.findAll();
			for (IsoTenant tenant : tenantList) {

				if (!StringUtils.isEmpty(tenant.getPosIssuerClientUrl())) {
					try {
						Iso8583Client<IsoMessage> issuerClient = octIssuerClientConfig.getPosClient(
								tenant.getBusiness(), tenant.getPosIssuerClientUrl(), tenant.getPosIssuerClientPort(),
								tenant.getNetworkId());
						issuerClient.init();
						posIssuerClientMap.put(tenant.getBusiness(), issuerClient);
						issuerClient.connect();

					} catch (Exception e) {
						logger.info("Exeption in starting :" + e);
					}

				}

			}

		} catch (Exception e) {
			logger.info("Exeption in starting :" + e);
		}

	}

}
