package com.neo.aggregator.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class PosClientRequestProducer {

	private final ApplicationEventPublisher publisher;

	@Autowired
	public PosClientRequestProducer(ApplicationEventPublisher publisher) {
		this.publisher = publisher;

	}

	public void publishIsoMessageReceived(PosMessageRequestEvent event) {
		try {
			publisher.publishEvent(event);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
