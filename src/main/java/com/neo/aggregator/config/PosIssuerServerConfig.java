package com.neo.aggregator.config;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.iso8583.IsoType;
import com.neo.aggregator.iso8583.MessageFactory;
import com.neo.aggregator.iso8583.parse.ConfigParser;
import com.neo.aggregator.jreactive8583.IsoMessageListener;
import com.neo.aggregator.jreactive8583.server.Iso8583Server;
import com.neo.aggregator.jreactive8583.server.ServerConfiguration;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.utility.FieldConstants;
import com.neo.aggregator.utility.Iso8583;
import com.neo.aggregator.utility.IsoMessageToIso8583Converter;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;

@Configuration
public class PosIssuerServerConfig {

	@Autowired
	private PosClientRequestProducer producer;

	@Value("${switch.network.headerlength}")
	private Integer switchNetworkHeaderLength;

	@Value("${switch.network}")
	private String switchNetwork;

	@Autowired
	@Qualifier("isoConcurrentMap")
	private ConcurrentHashMap<String, Iso8583> isoHashMap;

	@Autowired
	private IsoMessageToIso8583Converter isoMessageToIso8583Converter;

	@Autowired
	@Lazy
	private PosServerMessageListner posServerMessageListner;

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(PosIssuerServerConfig.class);

	public Iso8583Server<IsoMessage> getPosServer(String tenant, String host, Integer port, String networkId)
			throws IOException {
		final ServerConfiguration configuration = ServerConfiguration.newBuilder().withAddLoggingHandler(false)
				.withLogSensitiveData(false).withEchoMessageListener(false).withLogFieldDescription(false)
				.withAddLoggingHandler(false).withReplyOnError(false).build();

		Iso8583Server<IsoMessage> server = new Iso8583Server<>(port, configuration, serverMessageFactory(networkId),
				switchNetworkHeaderLength);

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {

			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS Server 200 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x200) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {

				logger.info("POS Server 200 Listner ::");
				final IsoMessage response = server.getIsoMessageFactory().createResponse(isoMessage);
				logger.info("Response Message created :: ");
				posServerMessageListner.asyncServerHandle(isoMessage, response, tenant);
				logger.info("Published request message :: ");
				ctx.flush();
				return false;
			}
		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS Server 210 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x210) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS Server 210 Listner :: " + isoMessage.debugString());

				Iso8583 response = isoMessageToIso8583Converter.createIso8583Object(isoMessage);

				String reqResponseId = Integer.parseInt(response.getMti()) + response.getPan() + response.getStan()
						+ response.getRrn() + response.getDateTimeTransaction()
						+ response.getAcquiringInstitutionCode();

				logger.info("Response Id ::" + reqResponseId);

				isoHashMap.put(reqResponseId, response);

				ctx.flush();
				return false;
			}
		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {

			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS Server 100 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x100) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {

				logger.info("POS Server 100 Listner ::");
				ctx.flush();
				return false;
			}
		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {

			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS Server 110 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x110) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {

				logger.info("POS Server 110 Listner ::");

				Iso8583 response = isoMessageToIso8583Converter.createIso8583Object(isoMessage);

				String reqResponseId = Integer.parseInt(response.getMti()) + response.getPan() + response.getStan()
						+ response.getRrn() + response.getDateTimeTransaction()
						+ response.getAcquiringInstitutionCode();

				logger.info("Response Id ::" + reqResponseId);

				isoHashMap.put(reqResponseId, response);

				ctx.flush();
				return false;
			}
		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS Server 400 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x400) {
					final IsoMessage response = server.getIsoMessageFactory().createResponse(isoMessage);
					producer.publishIsoMessageReceived(new PosMessageRequestEvent(tenant, isoMessage, response));
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS Server 400 Listner ::" + isoMessage.debugString());
				final IsoMessage response = server.getIsoMessageFactory().createResponse(isoMessage);
				response.setField(39, IsoType.ALPHA.value("00", 2));
				response.setField(60, IsoType.LLLVAR.value("XXX", 3));
				ctx.flush();
				return false;
			}

		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS Server 410 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x410) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS Server 410 Listner ::" + isoMessage.debugString());
				Iso8583 response = isoMessageToIso8583Converter.createIso8583Object(isoMessage);

				String reqResponseId = Integer.parseInt(response.getMti()) + response.getPan() + response.getStan()
						+ response.getRrn() + response.getDateTimeTransaction()
						+ response.getAcquiringInstitutionCode();

				logger.info("Response Id ::" + reqResponseId);

				isoHashMap.put(reqResponseId, response);

				ctx.flush();
				return false;
			}

		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS PosServerConfig 420 Listner ::");
				if (isoMessage.getType() == 0x420 || isoMessage.getType() == 420) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS PosServerConfig 420 Listner ::");

				ctx.flush();
				return false;
			}

		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS PosServerConfig 430 Listner ::");
				if (isoMessage.getType() == 0x430 || isoMessage.getType() == 430) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS PosServerConfig 430 Listner ::");

				Iso8583 response = isoMessageToIso8583Converter.createIso8583Object(isoMessage);

				String reqResponseId = Integer.parseInt(response.getMti()) + response.getPan() + response.getStan()
						+ response.getRrn() + response.getDateTimeTransaction()
						+ response.getAcquiringInstitutionCode();

				logger.info("Response Id ::" + reqResponseId);

				isoHashMap.put(reqResponseId, response);

				ctx.flush();
				return false;
			}

		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS PosServerConfig 120 Listner ::");
				if (isoMessage.getType() == 0x120 || isoMessage.getType() == 120) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS PosServerConfig 120 Listner ::");

				ctx.flush();
				return false;
			}

		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS PosServerConfig 130 Listner ::");
				if (isoMessage.getType() == 0x130 || isoMessage.getType() == 130) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS PosServerConfig 130 Listner ::");

				Iso8583 response = isoMessageToIso8583Converter.createIso8583Object(isoMessage);

				String reqResponseId = Integer.parseInt(response.getMti()) + response.getPan() + response.getStan()
						+ response.getRrn() + response.getDateTimeTransaction()
						+ response.getAcquiringInstitutionCode();

				logger.info("Response Id ::" + reqResponseId);

				isoHashMap.put(reqResponseId, response);

				ctx.flush();
				return false;
			}

		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS PosServerConfig 220 Listner ::");
				if (isoMessage.getType() == 0x220 || isoMessage.getType() == 220) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS PosServerConfig 220 Listner ::");

				ctx.flush();
				return false;
			}

		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS PosServerConfig 230 Listner ::");
				if (isoMessage.getType() == 0x230 || isoMessage.getType() == 230) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS PosServerConfig 230 Listner ::");

				Iso8583 response = isoMessageToIso8583Converter.createIso8583Object(isoMessage);

				String reqResponseId = Integer.parseInt(response.getMti()) + response.getPan() + response.getStan()
						+ response.getRrn() + response.getDateTimeTransaction()
						+ response.getAcquiringInstitutionCode();

				logger.info("Response Id ::" + reqResponseId);

				isoHashMap.put(reqResponseId, response);

				ctx.flush();
				return false;
			}

		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS PosServerConfig 620 Listner ::");
				if (isoMessage.getType() == 0x620 || isoMessage.getType() == 620) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS PosServerConfig 620 Listner ::");

				ctx.flush();
				return false;
			}

		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS PosServerConfig 630 Listner ::");
				if (isoMessage.getType() == 0x630 || isoMessage.getType() == 630) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS PosServerConfig 630 Listner ::");

				Iso8583 response = isoMessageToIso8583Converter.createIso8583Object(isoMessage);

				String reqResponseId = Integer.parseInt(response.getMti()) + response.getPan() + response.getStan()
						+ response.getRrn() + response.getDateTimeTransaction()
						+ response.getAcquiringInstitutionCode();

				logger.info("Response Id ::" + reqResponseId);

				isoHashMap.put(reqResponseId, response);

				ctx.flush();
				return false;
			}

		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS Server 800 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x800) {
					final IsoMessage response = server.getIsoMessageFactory().createResponse(isoMessage);

					producer.publishIsoMessageReceived(new PosMessageRequestEvent(tenant, isoMessage, response));
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS Server 800 Listner ::" + isoMessage.debugString());
				final IsoMessage response = server.getIsoMessageFactory().createResponse(isoMessage);
				response.setValue(FieldConstants.RESPONSE_CODE, "00", IsoType.ALPHA, 2, "Cp1047");

				Channel channel = server.getChannelActive();

				if (channel != null) {
					logger.info("Active channel found and its " + channel.isActive());
					channel.writeAndFlush(response);
					logger.info("Successful");
					return false;
				}

				return false;
			}
		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS Server 810 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x810) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS Server 810 Listner ::" + isoMessage.debugString());
				return false;
			}
		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {

			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS Server 302 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x302) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {

				logger.info("POS Server 302 Listner ::");
				final IsoMessage response = server.getIsoMessageFactory().createResponse(isoMessage);
				logger.info("Response Message created :: ");
				posServerMessageListner.asyncServerHandle(isoMessage, response, tenant);
				logger.info("Published request message :: ");
				ctx.flush();
				return false;
			}
		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {

			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS Server 312 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x312) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {

				logger.info("POS Server 312 Listner ::");

				Iso8583 response = isoMessageToIso8583Converter.createIso8583Object(isoMessage);

				String reqResponseId = Integer.parseInt(response.getMti()) + response.getPan() + response.getStan()
						+ response.getRrn() + response.getDateTimeTransaction()
						+ response.getAcquiringInstitutionCode();

				logger.info("Response Id ::" + reqResponseId);

				isoHashMap.put(reqResponseId, response);
				ctx.flush();
				return false;
			}
		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {

			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS Server 322 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x322) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {

				logger.info("POS Server 322 Listner ::");
				final IsoMessage response = server.getIsoMessageFactory().createResponse(isoMessage);
				logger.info("Response Message created :: ");
				posServerMessageListner.asyncServerHandle(isoMessage, response, tenant);
				logger.info("Published request message :: ");
				ctx.flush();
				return false;
			}
		});

		server.addMessageListener(new IsoMessageListener<IsoMessage>() {

			@Override
			public boolean applies(IsoMessage isoMessage) {

				logger.info("POS Server 332 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x332) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {

				logger.info("POS Server 332 Listner ::");
				Iso8583 response = isoMessageToIso8583Converter.createIso8583Object(isoMessage);

				String reqResponseId = Integer.parseInt(response.getMti()) + response.getPan() + response.getStan()
						+ response.getRrn() + response.getDateTimeTransaction()
						+ response.getAcquiringInstitutionCode();

				logger.info("Response Id ::" + reqResponseId);

				isoHashMap.put(reqResponseId, response);
				ctx.flush();
				return false;
			}
		});

		return server;
	}

	private MessageFactory<IsoMessage> serverMessageFactory(String networkId) throws IOException {

		final MessageFactory<IsoMessage> messageFactory = ConfigParser
				.createFromReader(new FileReader(new File("./resources/j8583_".concat(switchNetwork).concat(".xml"))));
		messageFactory.setEbcdicMsgType(false);
		logger.info("Network configured :::: " + switchNetwork);
		if (switchNetwork != null && StringUtils.isNotEmpty(switchNetwork) && switchNetwork.equalsIgnoreCase("visa")) {
			messageFactory.setVisaHeader(true);
		}
		if (switchNetwork != null && StringUtils.isNotEmpty(switchNetwork)
				&& switchNetwork.equalsIgnoreCase("master")) {
			messageFactory.setEbcdicMsgType(true);
		}

		messageFactory.setCharacterEncoding("8859_1");

		messageFactory.setUseBinaryMessages(true);

		messageFactory.setUseBinaryBitmap(true);

		messageFactory.setAssignDate(false);

		messageFactory.setNetworkId(networkId);

		return messageFactory;
	}
}
