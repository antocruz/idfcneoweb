package com.neo.aggregator.config;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.neo.aggregator.dao.IsoMessageRepo;
import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.iso8583.IsoType;
import com.neo.aggregator.iso8583.MessageFactory;
import com.neo.aggregator.iso8583.parse.ConfigParser;
import com.neo.aggregator.jreactive8583.IsoMessageListener;
import com.neo.aggregator.jreactive8583.client.ClientConfiguration;
import com.neo.aggregator.jreactive8583.client.Iso8583Client;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.utility.FieldConstants;
import com.neo.aggregator.utility.Iso8583;
import com.neo.aggregator.utility.IsoMessageToIso8583Converter;

import io.netty.channel.ChannelHandlerContext;

@Configuration
public class PosIssuerClientConfig {

	@Value("${iso8583.issuer.client.connection.idleTimeout}")
	private int issuerIdleTimeout;

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(PosIssuerClientConfig.class);

	@Autowired
	private PosClientRequestProducer posRequestProducer;

	@Autowired
	private IsoMessageRepo isoMessageRepo;

	@Autowired
	IsoMessageToIso8583Converter isoConverter;

	@Value("${switch.network.headerlength}")
	private Integer switchNetworkHeaderLength;

	@Value("${switch.network}")
	private String switchNetwork;

	@Autowired
	@Qualifier("isoConcurrentMap")
	private ConcurrentHashMap<String, Iso8583> isoHashMap;

	@Autowired
	private IsoMessageToIso8583Converter isoMessageToIso8583Converter;

	@Autowired
	@Lazy
	private PosClientMessageListner posClientMessageListener;

	public Iso8583Client<IsoMessage> getPosClient(String tenant, String host, Integer port, String networkId)
			throws IOException {

		SocketAddress socketAddress = new InetSocketAddress(host, port);

		final ClientConfiguration configuration = ClientConfiguration.newBuilder().withIdleTimeout(issuerIdleTimeout)
				.withAddLoggingHandler(false).withLogSensitiveData(false).withEchoMessageListener(true)
				.withLogFieldDescription(false).withReplyOnError(false).build();

		final Iso8583Client<IsoMessage> client = new Iso8583Client<>(socketAddress, configuration,
				clientMessageFactory(networkId), switchNetworkHeaderLength);

		client.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {
				if (isoMessage.getType() == 0x200 || isoMessage.getType() == 0x100 || isoMessage.getType() == 0x101) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS PosClientConfig 200 Listner :: " + tenant);
				final IsoMessage response = client.getIsoMessageFactory().createResponse(isoMessage);
				logger.info("Response Message created :: ");
				posClientMessageListener.asyncHandle(isoMessage, response, tenant);
				logger.info("Published request message :: ");
				ctx.flush();
				return false;
			}

		});

		client.addMessageListener(new IsoMessageListener<IsoMessage>() {

			@Override
			public boolean applies(IsoMessage isoMessage) {
				if (isoMessage.getType() == 0x420 || isoMessage.getType() == 420 || isoMessage.getType() == 0x421
						|| isoMessage.getType() == 421) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS PosClientConfig 420 Listner ::" + tenant);
				final IsoMessage response = client.getIsoMessageFactory().createResponse(isoMessage);
				logger.info("POS 420 Response :::" + response.debugString() + " tenant :" + tenant);
				posClientMessageListener.asyncHandle(isoMessage, response, tenant);
				ctx.flush();
				return false;
			}

		});

		client.addMessageListener(new IsoMessageListener<IsoMessage>() {

			@Override
			public boolean applies(IsoMessage isoMessage) {
				if (isoMessage.getType() == 0x400 || isoMessage.getType() == 400 || isoMessage.getType() == 0x401
						|| isoMessage.getType() == 401) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS PosClientConfig 400 Listner ::" + tenant);
				final IsoMessage response = client.getIsoMessageFactory().createResponse(isoMessage);
				posClientMessageListener.asyncHandle(isoMessage, response, tenant);
				ctx.flush();
				return false;
			}

		});

		client.addMessageListener(new IsoMessageListener<IsoMessage>() {

			@Override
			public boolean applies(IsoMessage isoMessage) {
				if (isoMessage.getType() == 0x800 || isoMessage.getType() == 800) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS  PosClientConfig 800 Listner ::" + tenant);
				final IsoMessage response = client.getIsoMessageFactory().createResponse(isoMessage);
				response.setValue(FieldConstants.RESPONSE_CODE, 00, IsoType.NUMERIC, 2,
						response.getField(FieldConstants.RESPONSE_CODE).getCharacterEncoding());
				ctx.writeAndFlush(response);
				return false;
			}
		});

		client.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {
				if (isoMessage.getType() == 0x810) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS  PosClientConfig 810 Listner ::" + tenant);

				Iso8583 iso8583 = isoConverter.createIso8583Object(isoMessage);
				iso8583.setResponseCode("00");
				isoMessageRepo.save(Iso8583.setAuthorizationMessage(iso8583));
				ctx.flush();
				return false;
			}
		});

		client.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {
				if (isoMessage.getType() == 0x220 || isoMessage.getType() == 0x120 || isoMessage.getType() == 0x221
						|| isoMessage.getType() == 0x121) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS  PosClientConfig 220 Listner ::" + tenant);
				final IsoMessage response = client.getIsoMessageFactory().createResponse(isoMessage);
				posRequestProducer.publishIsoMessageReceived(new PosMessageRequestEvent(tenant, isoMessage, response));
				ctx.flush();
				return false;
			}
		});

		client.addMessageListener(new IsoMessageListener<IsoMessage>() {
			@Override
			public boolean applies(IsoMessage isoMessage) {
				if (isoMessage.getType() == 0x620) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS  PosClientConfig 620 Listner ::" + tenant);
				final IsoMessage response = client.getIsoMessageFactory().createResponse(isoMessage);
				logger.info("POS 620 Response :::" + response.debugString() + " tenant :" + tenant);
				posClientMessageListener.asyncHandle(isoMessage, response, tenant);
				ctx.flush();
				return false;
			}
		});

		client.addMessageListener(new IsoMessageListener<IsoMessage>() {

			@Override
			public boolean applies(IsoMessage isoMessage) {
				logger.info("POS Server 210 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x210) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS Server 210 Listner :: " + isoMessage.debugString());

				Iso8583 response = isoMessageToIso8583Converter.createIso8583Object(isoMessage);

				String reqResponseId = Integer.parseInt(response.getMti()) + response.getPan() + response.getStan()
						+ response.getRrn() + response.getDateTimeTransaction()
						+ response.getAcquiringInstitutionCode();

				logger.info("Response Id ::" + reqResponseId);

				isoHashMap.put(reqResponseId, response);

				ctx.flush();
				return false;
			}

		});

		client.addMessageListener(new IsoMessageListener<IsoMessage>() {

			@Override
			public boolean applies(IsoMessage isoMessage) {
				logger.info("POS Client 312 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x312) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS Client 312 Listner :: " + isoMessage.debugString());

				Iso8583 response = isoMessageToIso8583Converter.createIso8583Object(isoMessage);

				String reqResponseId = Integer.parseInt(response.getMti()) + response.getPan() + response.getStan()
						+ response.getRrn() + response.getDateTimeTransaction()
						+ response.getAcquiringInstitutionCode();

				logger.info("Response Id ::" + reqResponseId);

				isoHashMap.put(reqResponseId, response);

				ctx.flush();
				return false;
			}

		});

		client.addMessageListener(new IsoMessageListener<IsoMessage>() {

			@Override
			public boolean applies(IsoMessage isoMessage) {
				logger.info("POS Server 332 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x332) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS Server 332 Listner :: " + isoMessage.debugString());

				Iso8583 response = isoMessageToIso8583Converter.createIso8583Object(isoMessage);

				String reqResponseId = Integer.parseInt(response.getMti()) + response.getPan() + response.getStan()
						+ response.getRrn() + response.getDateTimeTransaction()
						+ response.getAcquiringInstitutionCode();

				logger.info("Response Id ::" + reqResponseId);

				isoHashMap.put(reqResponseId, response);

				ctx.flush();
				return false;
			}

		});

		client.addMessageListener(new IsoMessageListener<IsoMessage>() {

			@Override
			public boolean applies(IsoMessage isoMessage) {
				logger.info("POS Server 1210 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x1210) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS Server 1210 Listner :: " + isoMessage.debugString());

				Iso8583 response = isoMessageToIso8583Converter.createIso8583Object(isoMessage);

				String reqResponseId = Integer.parseInt(response.getMti()) + response.getStan() + response.getRrn();

				logger.info("Response Id ::" + reqResponseId);

				isoHashMap.put(reqResponseId, response);

				ctx.flush();
				return false;
			}

		});

		client.addMessageListener(new IsoMessageListener<IsoMessage>() {

			@Override
			public boolean applies(IsoMessage isoMessage) {
				logger.info("POS Server 1430 Listner ::" + isoMessage.debugString());
				if (isoMessage.getType() == 0x1430) {
					return true;
				}
				return false;
			}

			@Override
			public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
				logger.info("POS Server 1430 Listner :: " + isoMessage.debugString());

				Iso8583 response = isoMessageToIso8583Converter.createIso8583Object(isoMessage);

				String reqResponseId = Integer.parseInt(response.getMti()) + response.getStan() + response.getRrn();

				logger.info("Response Id ::" + reqResponseId);

				isoHashMap.put(reqResponseId, response);

				ctx.flush();
				return false;
			}

		});

		return client;
	}

	private MessageFactory<IsoMessage> clientMessageFactory(String networkId) throws IOException {

		final MessageFactory<IsoMessage> messageFactory = ConfigParser
				.createFromReader(new FileReader(new File("./resources/j8583_".concat(switchNetwork).concat(".xml"))));

		messageFactory.setEbcdicMsgType(false);
		logger.info("Network configured :::: " + switchNetwork);
		if (switchNetwork != null && StringUtils.isNotEmpty(switchNetwork) && switchNetwork.equalsIgnoreCase("visa")) {
			messageFactory.setVisaHeader(true);
		}
		if (switchNetwork != null && StringUtils.isNotEmpty(switchNetwork)
				&& switchNetwork.equalsIgnoreCase("master")) {
			messageFactory.setEbcdicMsgType(true);
		}

		messageFactory.setCharacterEncoding("8859_1");

		messageFactory.setUseBinaryMessages(false);

		messageFactory.setUseBinaryBitmap(true);

		messageFactory.setEbcdicMsgType(true);

		messageFactory.setAssignDate(false);

		messageFactory.setForceStringEncoding(false);

		messageFactory.setNetworkId(networkId);

		return messageFactory;
	}

}
