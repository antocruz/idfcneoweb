package com.neo.aggregator.config;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.neo.aggregator.dao.TenantDao;
import com.neo.aggregator.model.Tenant;
import com.zaxxer.hikari.HikariDataSource;

@Component
public class DBInit implements InitializingBean {

	@Autowired
	private TenantDao tenantRepo;

	@Autowired
	protected DataSource datasource;

	public void afterPropertiesSet() throws Exception {
		List<Tenant> tenants = tenantRepo.findAll();
		for (Tenant dto : tenants) {
			this.registerDatasource(dto);
		}
	}

	private String registerDatasource(Tenant tenantDto) {
		DataSource newdatasource = this.getDataSource(tenantDto);
		((TenantRoutingDatasource) datasource).getResolvedDataSources().put(tenantDto.getBusiness(), newdatasource);
		return tenantDto.getBusiness();
	}

	private DataSource getDataSource(Tenant tenantDto) {
		HikariDataSource datasource = new HikariDataSource();

		datasource.setJdbcUrl(tenantDto.getDburl());
		datasource.setUsername(tenantDto.getDbuser());
		datasource.setPassword(tenantDto.getDbpwd());
		datasource.setMaximumPoolSize(20);
		datasource.setMinimumIdle(5);
		datasource.setConnectionTimeout(300000);
		datasource.setLeakDetectionThreshold(300000);
		datasource.setIdleTimeout(120000);
		datasource.setPoolName("neoAggregatorTenantHikariPool");
		return datasource;

	}

}
