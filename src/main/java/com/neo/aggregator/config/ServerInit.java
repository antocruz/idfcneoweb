package com.neo.aggregator.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.neo.aggregator.dao.IsoTenantRepo;
import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.jreactive8583.client.Iso8583Client;
import com.neo.aggregator.jreactive8583.server.Iso8583Server;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.model.IsoTenant;

@Component
public class ServerInit implements InitializingBean {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(ServerInit.class);

	private Map<String, Iso8583Client<IsoMessage>> posIssuerClientMap = new ConcurrentHashMap<String, Iso8583Client<IsoMessage>>();

	public Map<String, Iso8583Client<IsoMessage>> getPosIssuerClientMap() {
		return posIssuerClientMap;
	}

	public Map<String, Iso8583Server<IsoMessage>> getPosIssuerServerMap() {
		return posIssuerServerMap;
	}

	private Map<String, Iso8583Server<IsoMessage>> posIssuerServerMap = new ConcurrentHashMap<String, Iso8583Server<IsoMessage>>();

	@Autowired
	private PosIssuerServerConfig posIssuerServerConfig;
	
	@Value("${neo.iso.connection.enabled:true}")
	private Boolean isISOEnabled;

	@Autowired
	@Qualifier("tenantRepository")
	private IsoTenantRepo tenantDao;

	@Override
	public void afterPropertiesSet() {
		
		if(!isISOEnabled) {
			return;
		}

		try {

			Iterable<IsoTenant> tenantList = tenantDao.findAll();
			for (IsoTenant tenant : tenantList) {

				if (!StringUtils.isEmpty(tenant.getPosIssuerServerUrl())) {
					Iso8583Server<IsoMessage> issuerServer = posIssuerServerConfig.getPosServer(tenant.getBusiness(),
							tenant.getPosIssuerServerUrl(), tenant.getPosIssuerServerPort(), tenant.getNetworkId());

					issuerServer.init();
					issuerServer.start();

					if (issuerServer.isStarted()) {
						logger.info("POS Issuer Server Started ::::::: " + tenant.getBusiness());
					}
					posIssuerServerMap.put(tenant.getBusiness(), issuerServer);
				}

			}

		} catch (Exception e) {
			logger.info("Exeption in starting :" + e);
			System.out.println("Hello Exception in starting :::");
		}

	}

}
