package com.neo.aggregator.config;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.neo.aggregator.enums.FieldType;
import com.neo.aggregator.iso8583.IsoMessage;
import com.neo.aggregator.iso8583.IsoType;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;
import com.neo.aggregator.service.POSServiceProcess;
import com.neo.aggregator.utility.Iso8583;
import com.neo.aggregator.utility.Iso8583ToIsoMessageConverter;
import com.neo.aggregator.utility.IsoMessageToIso8583Converter;
import com.neo.aggregator.utility.VisaFieldParser;

@Component
public class PosClientMessageListner {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(PosClientMessageListner.class);

	@Autowired
	private POSServiceProcess posService;

	@Autowired
	private ClientInit clientInit;

	@Autowired
	private VisaFieldParser visaParser;

	@Autowired
	private IsoMessageToIso8583Converter isoMessageToIso8583Converter;

	@Autowired
	private Iso8583ToIsoMessageConverter iso8553ToIsoMessageConverter;

	@Value("${switch.network}")
	private String switchNetwork;

	@EventListener(classes = { PosMessageRequestEvent.class })
	void handle(PosMessageRequestEvent event) throws DecoderException {

		IsoMessage eventMessage = event.getRequestMessage();
		IsoMessage response = event.getResponseMessage();

		if (eventMessage != null && (eventMessage.getType() == 0x200 || (eventMessage.getType() == 0x100))) {
			Iso8583 request8583 = isoMessageToIso8583Converter.createIso8583Object(event.getRequestMessage());

			Iso8583 response8583 = isoMessageToIso8583Converter.createIso8583Object(event.getResponseMessage());
			Future<Iso8583> authResponse = posService.authorizeTransaction(request8583, response8583);
			while (!authResponse.isDone()) {
				try {
					logger.info("Thread Checking Auth Response Loop Check :::");
					Thread.sleep(200);
				} catch (InterruptedException e) {
					logger.info("Interupped exception while checking auth response ", e);
					response.setValue(FieldType.RESPONSE_CODE.getPosition(), "06",
							FieldType.RESPONSE_CODE.getFieldType(), FieldType.RESPONSE_CODE.getLength(),
							response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
				}
			}

			try {
				Iso8583 authRes = authResponse.get();

				if (authRes != null && authRes.getIsExternalAuth()) {

					response = convertIsoMessageForExternalAuth(authRes, response);
				} else {
					response = setResponseForAuthMessage(eventMessage, response, request8583, authRes);
				}
			} catch (InterruptedException | ExecutionException e) {
				logger.info("Interupped exception while checking auth response ", e);

				response.removeFields(FieldType.AUTHORIZATION_ID_RESPONSE.getPosition());
				response.setValue(FieldType.RESPONSE_CODE.getPosition(), "06", FieldType.RESPONSE_CODE.getFieldType(),
						FieldType.RESPONSE_CODE.getLength(),
						response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
			} finally {

			}
		} else if (eventMessage != null && (eventMessage.getType() == 0x420 || eventMessage.getType() == 0x421
				|| eventMessage.getType() == 0x400)) {

			response.removeFields(FieldType.CHIP_DATA.getPosition());
			Future<Iso8583> authResponse = posService.reverseTransaction(event.getRequestMessage(),
					event.getResponseMessage());
			while (!authResponse.isDone()) {
				try {
					logger.info("Thread Checking Response Loop Check :::");
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
					logger.info("Thread Checking Response Exception ", e);
				}
			}

			try {
				Iso8583 authRes = authResponse.get();

				if (authRes != null && authRes.getIsExternalAuth()) {

					response = convertIsoMessageForExternalAuth(authRes, response);
				} else {
					response = setResponseForReversal(response, authRes);
				}
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
				logger.info("Thread Checking Response Exception ", e);
				response.setValue(FieldType.RESPONSE_CODE.getPosition(), "06", FieldType.RESPONSE_CODE.getFieldType(),
						FieldType.RESPONSE_CODE.getLength(),
						response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
			}

		} else if (eventMessage != null && (eventMessage.getType() == 0x220 || eventMessage.getType() == 0x221
				|| eventMessage.getType() == 0x120 || eventMessage.getType() == 0x121)) {

			response.removeFields(FieldType.CHIP_DATA.getPosition());
			Future<Iso8583> authResponse = posService.adviseTransaction(event.getRequestMessage(),
					event.getResponseMessage());
			while (!authResponse.isDone()) {
				try {
					logger.info("Thread Checking Response Loop Check :::");
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
					logger.info("Thread Checking Response Exception ", e);
				}
			}

			try {
				Iso8583 authRes = authResponse.get();

				if (authRes != null && authRes.getIsExternalAuth()) {

					response = convertIsoMessageForExternalAuth(authRes, response);
				} else {
					response = setResponseForAdvice(response, authRes);
				}
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
				response.removeFields(FieldType.CHIP_DATA.getPosition());
				response.setValue(FieldType.RESPONSE_CODE.getPosition(), "06", FieldType.RESPONSE_CODE.getFieldType(),
						FieldType.RESPONSE_CODE.getLength(),
						response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
			}

		} else {

			response.setValue(FieldType.RESPONSE_CODE.getPosition(), "12", FieldType.RESPONSE_CODE.getFieldType(),
					FieldType.RESPONSE_CODE.getLength(),
					response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());

		}

		try {
			clientInit.getPosIssuerClientMap().get(event.getTenant()).send(response);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public IsoMessage setResponseForAdvice(IsoMessage response, Iso8583 authRes) {
		if (authRes != null) {
			if (authRes.getVisaCustomPaymentServiceField() != null) {
				Iso8583.VisaCustomPaymentServiceField customInfo = new Iso8583().new VisaCustomPaymentServiceField();
				customInfo.setTranId(authRes.getVisaCustomPaymentServiceField().getTranId());
				response.setValue(FieldType.VISA_CUSTOM_DATA.getPosition(),
						visaParser.parseCustomPaymentInfo(customInfo), IsoType.LLBIN, 0, "hex2");
			}
			if (authRes.getVisaPrivateNetworkField() != null) {
				Iso8583.VisaPrivateNetworkField network = new Iso8583().new VisaPrivateNetworkField();
				network.setNetworkId(authRes.getVisaPrivateNetworkField().getNetworkId());
				response.setValue(FieldType.VISA_NETWORK_DATA.getPosition(), visaParser.parseNetworkInfo(network),
						IsoType.LLBIN, 0, "hex2");
				if (authRes.getVisaPrivateNetworkField().getSwitchReasonCode() != null
						&& authRes.getVisaPrivateNetworkField().getSwitchReasonCode().equals("9101")) {
					authRes.setResponseCode("00");
				}
			}

			response.setValue(FieldType.RESPONSE_CODE.getPosition(), authRes.getResponseCode(),
					FieldType.RESPONSE_CODE.getFieldType(), FieldType.RESPONSE_CODE.getLength(),
					response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());

		} else {
			response.setValue(FieldType.RESPONSE_CODE.getPosition(), "05", FieldType.RESPONSE_CODE.getFieldType(),
					FieldType.RESPONSE_CODE.getLength(),
					response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
		}

		return response;
	}

	public IsoMessage setResponseForReversal(IsoMessage response, Iso8583 authRes) {
		if (authRes != null) {
			if (authRes.getVisaCustomPaymentServiceField() != null) {
				Iso8583.VisaCustomPaymentServiceField customInfo = new Iso8583().new VisaCustomPaymentServiceField();
				customInfo.setTranId(authRes.getVisaCustomPaymentServiceField().getTranId());
				response.setValue(FieldType.VISA_CUSTOM_DATA.getPosition(),
						visaParser.parseCustomPaymentInfo(customInfo), IsoType.LLBIN, 0, "hex2");
			}
			if (authRes.getVisaPrivateNetworkField() != null) {
				Iso8583.VisaPrivateNetworkField network = new Iso8583().new VisaPrivateNetworkField();
				network.setNetworkId(authRes.getVisaPrivateNetworkField().getNetworkId());
				response.setValue(FieldType.VISA_NETWORK_DATA.getPosition(), visaParser.parseNetworkInfo(network),
						IsoType.LLBIN, 0, "hex2");

			}

			response.setValue(FieldType.RESPONSE_CODE.getPosition(), authRes.getResponseCode(),
					FieldType.RESPONSE_CODE.getFieldType(), FieldType.RESPONSE_CODE.getLength(),
					response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());

			response.removeFields(FieldType.CHIP_DATA.getPosition());

		} else {
			response.removeFields(FieldType.CHIP_DATA.getPosition());
			response.setValue(FieldType.RESPONSE_CODE.getPosition(), "06", FieldType.RESPONSE_CODE.getFieldType(),
					FieldType.RESPONSE_CODE.getLength(),
					response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
		}

		return response;
	}

	public IsoMessage setResponseForAuthMessage(IsoMessage eventMessage, IsoMessage response, Iso8583 request8583,
			Iso8583 authRes) throws DecoderException {
		if (request8583.getVisaCustomPaymentServiceField() != null) {
			logger.info("Custom Payment service field present");
			Iso8583.VisaCustomPaymentServiceField customInfo = new Iso8583().new VisaCustomPaymentServiceField();
			customInfo.setTranId(request8583.getVisaCustomPaymentServiceField().getTranId());
			response.setValue(FieldType.VISA_CUSTOM_DATA.getPosition(), visaParser.parseCustomPaymentInfo(customInfo),
					IsoType.LLBIN, 0, "hex2");
		}

		if (request8583.getVisaPrivateNetworkField() != null) {
			logger.info("Private network field present");
			Iso8583.VisaPrivateNetworkField network = new Iso8583().new VisaPrivateNetworkField();
			network.setNetworkId(request8583.getVisaPrivateNetworkField().getNetworkId());
			response.setValue(FieldType.VISA_NETWORK_DATA.getPosition(), visaParser.parseNetworkInfo(network),
					IsoType.LLBIN, 0, "hex2");

		}

		if (authRes.getVisaAdditionalResponseField() != null) {
			logger.info("Additional Respones data present");
			response.setValue(FieldType.ADDITIONAL_RESPONSE_DATA.getPosition(),
					visaParser.parseAddResponseInfo(authRes.getVisaAdditionalResponseField()), IsoType.LLBIN, 0,
					"hex2");
		} else {
			response.removeFields(FieldType.ADDITIONAL_RESPONSE_DATA.getPosition());
		}

		if (!request8583.getProcessingCode().substring(0, 2).equals("30")) {
			response.removeFields(FieldType.ADDITIONAL_AMOUNT.getPosition());
		}

		response.removeFields(FieldType.CHIP_DATA.getPosition());
		if (authRes != null && !StringUtils.isEmpty(authRes.getResponseCode())
				&& "00".equals(authRes.getResponseCode())) {
			response.setValue(FieldType.RESPONSE_CODE.getPosition(), authRes.getResponseCode(),
					FieldType.RESPONSE_CODE.getFieldType(), FieldType.RESPONSE_CODE.getLength(),
					response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
			response.setValue(FieldType.AUTHORIZATION_ID_RESPONSE.getPosition(), authRes.getAuthorizationIdResponse(),
					FieldType.AUTHORIZATION_ID_RESPONSE.getFieldType(), FieldType.AUTHORIZATION_ID_RESPONSE.getLength(),
					response.getAt(FieldType.AUTHORIZATION_ID_RESPONSE.getPosition()).getCharacterEncoding());
			if (authRes != null && authRes.getChipData() != null) {
				String chipData = "";
				if (eventMessage.isVisaHeader()) {
					/*
					 * try {
					 * chipData="01"+ISOUtil.padleft(Integer.toHexString(authRes.getChipData().
					 * length()/2),4,'0')+authRes.getChipData(); } catch (ISOException e) {
					 * e.printStackTrace(); }
					 */
					chipData = authRes.getChipData();
					if ((chipData.length() % 2) != 0) {
						chipData = "0" + chipData;
					}
				} else {
					chipData = authRes.getChipData();
				}
				logger.info("Chip Data ::: " + chipData);
				response.setValue(FieldType.CHIP_DATA.getPosition(), Hex.decodeHex(chipData.toCharArray()),
						eventMessage.getAt(FieldType.CHIP_DATA.getPosition()).getType(), 0,
						eventMessage.getAt(FieldType.CHIP_DATA.getPosition()).getCharacterEncoding());

			}

		} else if (authRes != null && !StringUtils.isEmpty(authRes.getResponseCode())
				&& !"00".equals(authRes.getResponseCode())) {
			response.removeFields(FieldType.AUTHORIZATION_ID_RESPONSE.getPosition());

			response.setValue(FieldType.RESPONSE_CODE.getPosition(), authRes.getResponseCode(),
					FieldType.RESPONSE_CODE.getFieldType(), FieldType.RESPONSE_CODE.getLength(),
					response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
		} else {
			response.removeFields(FieldType.AUTHORIZATION_ID_RESPONSE.getPosition());
			response.setValue(FieldType.RESPONSE_CODE.getPosition(), "06", FieldType.RESPONSE_CODE.getFieldType(),
					FieldType.RESPONSE_CODE.getLength(),
					response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
		}
		if (authRes != null && authRes.getAdditionalData48() != null) {
			response.setValue(FieldType.ADDITIONAL_DATA_48.getPosition(), authRes.getAdditionalData48(),
					FieldType.ADDITIONAL_DATA_48.getFieldType(), FieldType.ADDITIONAL_DATA_48.getLength(),
					response.getAt(FieldType.ADDITIONAL_DATA_48.getPosition()).getCharacterEncoding());
		}
		response.removeFields(FieldType.EXPIRY_DATE.getPosition());

		return response;
	}

	@Async("threadPoolExecutor")
	void asyncHandle(IsoMessage request, IsoMessage response, String tenant) {

		logger.debug("Original Parsed Response " + response.debugString());

		if (request != null
				&& (request.getType() == 0x200 || (request.getType() == 0x100) || (request.getType() == 0x101))) {
			Iso8583 request8583 = isoMessageToIso8583Converter.createIso8583Object(request);

			Iso8583 response8583 = isoMessageToIso8583Converter.createIso8583Object(response);
			Future<Iso8583> authResponse = posService.authorizeTransaction(request8583, response8583);
			while (!authResponse.isDone()) {
				try {
					logger.info("Thread Checking Auth Response Loop Check :::");
					Thread.sleep(100);
				} catch (InterruptedException e) {
					logger.info("Interupped exception while checking auth response ", e);
					response.setValue(FieldType.RESPONSE_CODE.getPosition(), "06",
							FieldType.RESPONSE_CODE.getFieldType(), FieldType.RESPONSE_CODE.getLength(),
							response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
				}
			}

			try {
				Iso8583 authRes = authResponse.get();

				if (authRes != null && authRes.getIsExternalAuth()) {

					response = convertIsoMessageForExternalAuth(authRes, response);

				} else {
					response = setResponseForAuthV2Message(request, response, request8583, authRes);
				}
			} catch (InterruptedException | ExecutionException | DecoderException e) {
				logger.info("Interupped exception while checking auth response ", e);

				response.removeFields(FieldType.AUTHORIZATION_ID_RESPONSE.getPosition());
				response.setValue(FieldType.RESPONSE_CODE.getPosition(), "05", FieldType.RESPONSE_CODE.getFieldType(),
						FieldType.RESPONSE_CODE.getLength(),
						response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
			} finally {

			}
		} else if (request != null && (request.getType() == 0x420 || request.getType() == 0x421
				|| request.getType() == 0x400 || request.getType() == 0x401)) {
			response.removeFields(FieldType.CHIP_DATA.getPosition());
			Future<Iso8583> authResponse = posService.reverseTransaction(request, response);
			while (!authResponse.isDone()) {
				try {
					logger.info("Thread Checking Response Loop Check :::");
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
					logger.info("Thread Checking Response Exception ", e);
				}
			}

			try {
				Iso8583 authRes = authResponse.get();
				if (authRes != null && authRes.getIsExternalAuth()) {

					response = convertIsoMessageForExternalAuth(authRes, response);
				} else {
					response = setResponseForReversal(response, authRes);
				}
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
				logger.info("Thread Checking Response Exception ", e);
				response.setValue(FieldType.RESPONSE_CODE.getPosition(), "06", FieldType.RESPONSE_CODE.getFieldType(),
						FieldType.RESPONSE_CODE.getLength(),
						response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
			}

		} else if (request != null && (request.getType() == 0x220 || request.getType() == 0x221
				|| request.getType() == 0x120 || request.getType() == 0x121 || request.getType() == 0x620)) {
			logger.info("Original Parsed Response " + response.debugString());

			response.removeFields(FieldType.CHIP_DATA.getPosition());
			Future<Iso8583> authResponse = posService.adviseTransaction(request, response);
			while (!authResponse.isDone()) {
				try {
					logger.info("Thread Checking Response Loop Check :::");
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
					logger.info("Thread Checking Response Exception ", e);
				}
			}

			try {
				Iso8583 authRes = authResponse.get();
				if (authRes != null && authRes.getIsExternalAuth()) {
					response = convertIsoMessageForExternalAuth(authRes, response);
				} else {
					response = setResponseForAdvice(response, authRes);
				}
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
				response.removeFields(FieldType.CHIP_DATA.getPosition());
				response.setValue(FieldType.RESPONSE_CODE.getPosition(), "06", FieldType.RESPONSE_CODE.getFieldType(),
						FieldType.RESPONSE_CODE.getLength(),
						response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
			}
		} else {
			logger.info("Newly created response " + response.debugString());
			response.setValue(FieldType.RESPONSE_CODE.getPosition(), "12", FieldType.RESPONSE_CODE.getFieldType(),
					FieldType.RESPONSE_CODE.getLength(),
					response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());

		}

		try {
			clientInit.getPosIssuerClientMap().get(tenant).send(response);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public IsoMessage convertIsoMessageForExternalAuth(Iso8583 authRes, IsoMessage response) {
		IsoMessage message;

		String network = switchNetwork.toUpperCase();

		switch (network) {

		case "VISA":
			message = iso8553ToIsoMessageConverter.visaConvert(authRes);
			break;

		default:
			message = iso8553ToIsoMessageConverter.convert(authRes);

		}
		return message;
	}

	public IsoMessage setResponseForAuthV2Message(IsoMessage request, IsoMessage response, Iso8583 request8583,
			Iso8583 authRes) throws DecoderException {
		if (request8583.getVisaCustomPaymentServiceField() != null) {
			logger.info("Custom Payment service field present");
			Iso8583.VisaCustomPaymentServiceField customInfo = new Iso8583().new VisaCustomPaymentServiceField();
			customInfo.setTranId(request8583.getVisaCustomPaymentServiceField().getTranId());
			response.setValue(FieldType.VISA_CUSTOM_DATA.getPosition(), visaParser.parseCustomPaymentInfo(customInfo),
					IsoType.LLBIN, 0, "hex2");
		}

		if (request8583.getVisaPrivateNetworkField() != null) {
			logger.info("Private network field present");
			Iso8583.VisaPrivateNetworkField network = new Iso8583().new VisaPrivateNetworkField();
			network.setNetworkId(request8583.getVisaPrivateNetworkField().getNetworkId());
			response.setValue(FieldType.VISA_NETWORK_DATA.getPosition(), visaParser.parseNetworkInfo(network),
					IsoType.LLBIN, 0, "hex2");
		}

		if (authRes.getVisaAdditionalResponseField() != null) {
			logger.info("Additional Respones data present");
			response.setValue(FieldType.ADDITIONAL_RESPONSE_DATA.getPosition(),
					visaParser.parseAddResponseInfo(authRes.getVisaAdditionalResponseField()), IsoType.LLBIN, 0,
					"hex2");
		} else {
			if (request8583.getVisaPrivateNetworkField() != null) {
				response.setValue(FieldType.ADDITIONAL_RESPONSE_DATA.getPosition(),
						visaParser.parseAddResponseInfo(new Iso8583().new VisaAdditionalResponseField()), IsoType.LLBIN,
						0, "hex2");
			}
		}

		if (authRes.getAdditionalAmount() != null) {
			if (request8583.getProcessingCode().substring(0, 2).equals("30")) {
				response.setValue(FieldType.ADDITIONAL_AMOUNT.getPosition(), authRes.getAdditionalAmount(),
						IsoType.LLVAR, 0, "Cp1047");
			} else {
				response.removeFields(FieldType.ADDITIONAL_AMOUNT.getPosition());
			}
		} else {
			response.removeFields(FieldType.ADDITIONAL_AMOUNT.getPosition());
		}
		response.removeFields(FieldType.CHIP_DATA.getPosition());
		if (authRes != null && !StringUtils.isEmpty(authRes.getResponseCode())
				&& "00".equals(authRes.getResponseCode())) {
			response.setValue(FieldType.RESPONSE_CODE.getPosition(), authRes.getResponseCode(),
					FieldType.RESPONSE_CODE.getFieldType(), FieldType.RESPONSE_CODE.getLength(),
					response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
			response.setValue(FieldType.AUTHORIZATION_ID_RESPONSE.getPosition(), authRes.getAuthorizationIdResponse(),
					FieldType.AUTHORIZATION_ID_RESPONSE.getFieldType(), FieldType.AUTHORIZATION_ID_RESPONSE.getLength(),
					response.getAt(FieldType.AUTHORIZATION_ID_RESPONSE.getPosition()).getCharacterEncoding());
			if (authRes != null && authRes.getChipData() != null) {
				String chipData = "";
				if (request.isVisaHeader()) {
					/*
					 * try {
					 * chipData="01"+ISOUtil.padleft(Integer.toHexString(authRes.getChipData().
					 * length()/2),4,'0')+authRes.getChipData(); } catch (ISOException e) {
					 * e.printStackTrace(); }
					 */
					chipData = authRes.getChipData();
					if ((chipData.length() % 2) != 0) {
						chipData = "0" + chipData;
					}
				} else {
					chipData = authRes.getChipData();
				}
				logger.info("Chip Data ::: " + chipData);
				response.setValue(FieldType.CHIP_DATA.getPosition(), Hex.decodeHex(chipData.toCharArray()),
						request.getAt(FieldType.CHIP_DATA.getPosition()).getType(), 0,
						request.getAt(FieldType.CHIP_DATA.getPosition()).getCharacterEncoding());

			}

		} else if (authRes != null && !StringUtils.isEmpty(authRes.getResponseCode())
				&& !"00".equals(authRes.getResponseCode())) {
			response.removeFields(FieldType.AUTHORIZATION_ID_RESPONSE.getPosition());

			response.setValue(FieldType.RESPONSE_CODE.getPosition(), authRes.getResponseCode(),
					FieldType.RESPONSE_CODE.getFieldType(), FieldType.RESPONSE_CODE.getLength(),
					response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
		} else {
			response.removeFields(FieldType.AUTHORIZATION_ID_RESPONSE.getPosition());
			response.setValue(FieldType.RESPONSE_CODE.getPosition(), "06", FieldType.RESPONSE_CODE.getFieldType(),
					FieldType.RESPONSE_CODE.getLength(),
					response.getAt(FieldType.RESPONSE_CODE.getPosition()).getCharacterEncoding());
		}
		if (response.getAt(FieldType.RESPONSE_CODE.getPosition()) != null
				&& switchNetwork.toUpperCase().equals("VISA")) {
			response.removeFields(FieldType.ADDITIONAL_DATA_48.getPosition());
		}
		response.removeFields(FieldType.EXPIRY_DATE.getPosition());

		return response;
	}

}
