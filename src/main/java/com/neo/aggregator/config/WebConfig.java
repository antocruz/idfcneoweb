package com.neo.aggregator.config;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.neo.aggregator.hsm.HsmServiceImpl;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;

@Configuration
@EnableWebMvc
@EnableAspectJAutoProxy
public class WebConfig implements WebMvcConfigurer {

	@Autowired
	@Qualifier("tenantInterceptor")
	private HandlerInterceptor tenantRequestHandler;

	@Value("${key.part.one}")
	private String keyPartOne;

	@Value("${key.index.default}")
	private String keyIndex;

	@Value("${key.encrypted.npciswitch}")
	private String switchKey;

	@Value("${key.encrypted.core}")
	private String coreKey;

	@Autowired
	private HsmServiceImpl hsmService;

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(WebConfig.class);

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(tenantRequestHandler);
	}

	@Bean("createEncryptCypher")
	public Cipher createEncryptCipherBean() {
		Cipher ecipher = null;
		try {
			String decryptKey = hsmService.decryptKey(switchKey, "01", keyPartOne, "0D");
			logger.info("Check sum generation key " + decryptKey);
			Key key = new SecretKeySpec(decryptKey.getBytes(), "AES");
			ecipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			ecipher.init(Cipher.ENCRYPT_MODE, key);

		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
			logger.error("Error while creating cypher instance ::" + e.getMessage());
		}
		return ecipher;
	}

	@Bean("createDecryptCypher")
	public Cipher createDecryptCipherBean() {
		Cipher ecipher = null;
		try {
			String decryptKey = hsmService.decryptKey(coreKey, "01", keyPartOne, "0D");
			logger.info("Check sum generation key " + decryptKey);
			Key key = new SecretKeySpec(decryptKey.getBytes(), "AES");
			ecipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			ecipher.init(Cipher.DECRYPT_MODE, key);

		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
			logger.error("Error while creating decypher instance ::" + e.getMessage());
		}
		return ecipher;
	}

}
