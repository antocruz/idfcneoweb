package com.neo.aggregator.config;

import java.io.IOException;
import java.io.StringWriter;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import org.w3c.dom.Document;

public class SoapRequestHeaderModifier implements WebServiceMessageCallback {

	@Value("${yes.auth.username}")
    private String username;
	
	@Value("${yes.auth.password}")
    private String password;
	
	@Value("${yes.client.id}")
    private String clientId;
	
	@Value("${yes.client.secret}")
    private String clientSecret;

	private String auth;
	
	public SoapRequestHeaderModifier(String auth, String clientId, String clientSecret) {
		this.auth = auth;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
	}
	
	@Override
	public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
		
		
		
		if (message instanceof SaajSoapMessage) {
			SaajSoapMessage soapMessage = (SaajSoapMessage) message;
			MimeHeaders mimeHeader = soapMessage.getSaajMessage().getMimeHeaders();
			/*
			mimeHeader.addHeader("Authorization", auth); //"Basic dGVzdGNsaWVudDp0ZXN0QDEyMw==");
			mimeHeader.addHeader("X-IBM-Client-Id", "c218f5a9-8d3a-4bb0-b1e6-06c7fb834f92");
			mimeHeader.addHeader("X-IBM-Client-Secret", "rM4fM6bO8aX5lS6mE7wH5iV8yK4sB7gO0bR0uN1gR8dN3jK0fT");
			mimeHeader.addHeader("Content-Type", "application/xml");*/
			
			mimeHeader.addHeader("Authorization", auth); //"Basic NTc4OTc0MTpMZGFwQDEyMw==");
			mimeHeader.addHeader("X-IBM-Client-Id", clientId);
			mimeHeader.addHeader("X-IBM-Client-Secret",	clientSecret);
			mimeHeader.addHeader("Content-Type", "application/xml");
		}
		
		SaajSoapMessage saajSoapMessage = (SaajSoapMessage) message;

		try {
			SOAPMessage soapMessage = saajSoapMessage.getSaajMessage();
    		SOAPPart soapPart = soapMessage.getSOAPPart();
    		SOAPEnvelope envelope = soapPart.getEnvelope();
    		envelope.removeNamespaceDeclaration(envelope.getPrefix());
    		envelope.setPrefix("soap");
    		envelope.addNamespaceDeclaration("soap", "http://www.w3.org/2003/05/soap-envelope");
    		SOAPHeader header = soapMessage.getSOAPHeader();
    		SOAPBody body = soapMessage.getSOAPBody();
    		header.setPrefix("soap");    			
    		body.setPrefix("soap");
		} catch(SOAPException e) {
			e.printStackTrace();
		}
	}

	public static String toString(Document doc) {
	    try {
	        StringWriter sw = new StringWriter();
	        TransformerFactory tf = TransformerFactory.newInstance();
	        Transformer transformer = tf.newTransformer();
	        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
	        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
	        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

	        transformer.transform(new DOMSource(doc), new StreamResult(sw));
	        return sw.toString();
	    } catch (Exception ex) {
	        throw new RuntimeException("Error converting to String", ex);
	    }
	}
}
