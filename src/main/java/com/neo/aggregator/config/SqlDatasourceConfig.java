package com.neo.aggregator.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.neo.aggregator.hsm.HsmServiceImpl;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class SqlDatasourceConfig extends HikariConfig implements DataSourceConfig {

	@Value("${jdbc.driver.class}")
	private String driverClass;

	@Value("${jdbc.url}")
	private String jdbcUrl;

	@Value("${jdbc.username}")
	private String username;

	@Value("${jdbc.password}")
	private String password;

	@Value("${key.default.index}")
	private String keyIndex;

	@Value("${key.default.partone}")
	private String icv;

	@Autowired
	private HsmServiceImpl hsmServiceImpl;

	@Bean
	@Override
	public DataSource dataSource() {

		TenantRoutingDatasource tenantRoutingDS = new TenantRoutingDatasource();
		tenantRoutingDS.setDefaultTargetDataSource(getDataSource());
		Map<Object, Object> targetDataSources = new ConcurrentHashMap<>();
		targetDataSources.put("default", getDefaultDataSource());
		tenantRoutingDS.setTargetDataSources(targetDataSources);
		return tenantRoutingDS;
	}

	public DataSource getDefaultDataSource() {
		
		HikariDataSource datasource = new HikariDataSource();
		datasource.setJdbcUrl(jdbcUrl);
		datasource.setUsername(username);
		datasource.setPassword(hsmServiceImpl.decryptString(password, "01", icv, keyIndex));
		datasource.setMaximumPoolSize(20);
		datasource.setMinimumIdle(5);
		datasource.setConnectionTimeout(300000);
		datasource.setLeakDetectionThreshold(300000);
		datasource.setIdleTimeout(120000);
		datasource.setPoolName("neoAggregatorDefaultHikariPool");
		return datasource;
	}

}
