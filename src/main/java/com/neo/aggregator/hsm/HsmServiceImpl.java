package com.neo.aggregator.hsm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.neo.aggregator.dto.DecryptKeyDto;
import com.neo.aggregator.log.Log4j2LogManager;
import com.neo.aggregator.log.Log4j2Wrapper;

@Service
public class HsmServiceImpl {

	private static Log4j2Wrapper logger = (Log4j2Wrapper) Log4j2LogManager.getLog(HsmServiceImpl.class);

	@Value("${hsm.decryptUrl}")
	private String decryptUrl;

	@Value("${hsm.decryptString}")
	private String decryptString;

	@Value("${hsm.host}")
	private String hsmHost;

	@Autowired
	private RestTemplate restTemplate;

	public String decryptKey(String encryptedKey, String encryptionMode, String inputChainingValue, String keyIndex) {
		logger.info("calling decrypt key");
		String result = "";
		try {

			DecryptKeyDto dto = new DecryptKeyDto(encryptedKey, inputChainingValue, keyIndex, encryptionMode);
			HttpEntity<DecryptKeyDto> req = new HttpEntity<DecryptKeyDto>(dto);
			ResponseEntity<String> response = restTemplate.postForEntity(hsmHost + decryptUrl, req, String.class,
					new Object[] { decryptUrl });
			logger.info("Before Calling HSM For decrypt key :::");
			result = response.getBody();
			if (result != null && result.substring(0, 2).equals("00")) {
				logger.info("Decryption successfull");
				return result.substring(2);
			}
		} catch (Exception e) {
			logger.error("Error When calling HSM", e);
		}
		return null;
	}

	public String decryptString(String encryptedKey, String encryptionMode, String inputChainingValue,
			String keyIndex) {
		logger.info("calling decrypt key");
		String result = "";
		try {

			DecryptKeyDto dto = new DecryptKeyDto(encryptedKey, inputChainingValue, keyIndex, encryptionMode);
			HttpEntity<DecryptKeyDto> req = new HttpEntity<DecryptKeyDto>(dto);
			ResponseEntity<String> response = restTemplate.postForEntity(hsmHost + decryptString, req, String.class,
					new Object[] { decryptUrl });
			logger.info("Before Calling HSM For decrypt key :::");
			result = response.getBody();
			if (result != null && result.substring(0, 2).equals("00")) {
				logger.info("Decryption successfull");
				return result.substring(2);
			}
		} catch (Exception e) {
			logger.error("Error When calling HSM", e);
		}
		return null;
	}
}
