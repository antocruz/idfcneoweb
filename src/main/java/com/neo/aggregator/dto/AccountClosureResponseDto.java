package com.neo.aggregator.dto;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
public class AccountClosureResponseDto {

	private String status;

	private String accountId;

	private String accountCurrency;

	private String customerId;

	private String customerName;

	private String repaymentAccountId;

	private String closureDate;

	private String withdrawAmount;

	private String withdrawCurrency;

	private String balanceAmount;

	private String balanceCurrency;

	private String liftedAmount;
	
	private String profitPercentage;

	private String payoutAmount;

	private String profitAmount;

	private String currentDepositAmount;

	private String currentDepositCurrency;

	private String closureTransactionId;
	
	private List<Transaction> txnList;

	@Getter
	@Setter
	@AllArgsConstructor
	@NoArgsConstructor
	public static class Transaction {

		private String amount;

		private String balance;

		private String type;

		private Long time;

		private Date txnDate;

		private String txnCategory;

		private String txnId;

		private String description;

		private String currency;

	}

}
