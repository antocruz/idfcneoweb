package com.neo.aggregator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Wadh {
	private String ts;
	private String ver;
	private String ra;
	private String rc;
	private String lr;
	private String de;
	private String pfr;
	
	@JsonProperty("ts")
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	
	@JsonProperty("ver")
	public String getVer() {
		return ver;
	}
	public void setVer(String ver) {
		this.ver = ver;
	}
	
	@JsonProperty("ra")
	public String getRa() {
		return ra;
	}
	public void setRa(String ra) {
		this.ra = ra;
	}
	
	@JsonProperty("rc")
	public String getRc() {
		return rc;
	}
	public void setRc(String rc) {
		this.rc = rc;
	}
	
	@JsonProperty("lr")
	public String getLr() {
		return lr;
	}
	public void setLr(String lr) {
		this.lr = lr;
	}
	
	@JsonProperty("de")
	public String getDe() {
		return de;
	}
	public void setDe(String de) {
		this.de = de;
	}
	
	@JsonProperty("pfr")
	public String getPfr() {
		return pfr;
	}
	public void setPfr(String pfr) {
		this.pfr = pfr;
	}
}
