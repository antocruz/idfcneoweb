package com.neo.aggregator.dto;

import lombok.Data;

@Data
public class TestAPIRequestDto {

	private String plainRequest;

	private String url;

	private String partnerName;

	private String requestType;

	private String moduleId;

	private String authType;

	private String channelId;

	private String partnerUserName;

}
