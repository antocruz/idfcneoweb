package com.neo.aggregator.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NeoResponse {
	
	private Object result;
	private NeoException exception;
	private NeoPaginate pagination;

}
