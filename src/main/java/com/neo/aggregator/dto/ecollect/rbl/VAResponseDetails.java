package com.neo.aggregator.dto.ecollect.rbl;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class VAResponseDetails {

	@JsonProperty(value = "Account_Number")
	private String AccountNo;

	@JsonProperty(value = "VA_Number")
	private String VAnumber;

	@JsonProperty(value = "Full_VA_Number")
	private String FullVAnumber;

	@JsonProperty(value = "Short_Name")
	private String ShortName;

	@JsonProperty(value = "CIF")
	private String CIF;

	@JsonProperty(value = "VA_BENEFICIARY")
	private String VaBeneficiary;

	@JsonProperty(value = "Status_Reason")
	private String StatusReason;

}
