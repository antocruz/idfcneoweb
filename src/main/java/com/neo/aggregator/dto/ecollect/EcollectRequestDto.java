package com.neo.aggregator.dto.ecollect;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EcollectRequestDto {
	
	private String ecollectCode;
	
	private String amount;
	
	private String remitterAccountNumber;
	
	private String remitterFullName;
	
	private String transferUniqueNumber;
	
	private String customerCode;
	
	private String description;

	private String programCode;

	private String program;
	
	private String remitterIFSCCode;
	
	private String virtualAccountNo;
	
	private String creditAccountNo;
	
	private String transferType;
	
	private String creditedAt;
	
	private String note;
	
	private String remitterAccountType;	
	
	private String application;
	
	private String m2pReferenceNumber;
	
	private String status;
	
	private Integer vaCodeLength;
	
}

