package com.neo.aggregator.dto.ecollect;

import lombok.Data;

@Data
public class YesEcollectResponseDto {

	private EcollectData notifyResult;
	
	public YesEcollectResponseDto() {
		notifyResult = new EcollectData();
	}
	
	public YesEcollectResponseDto(String result) {
		notifyResult = new EcollectData();
		notifyResult.setResult(result);
	}
	
}
