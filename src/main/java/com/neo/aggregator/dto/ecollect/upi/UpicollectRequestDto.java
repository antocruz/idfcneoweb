package com.neo.aggregator.dto.ecollect.upi;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpicollectRequestDto {

	private String amount;
	private String application;
	private String customerCode;
	private String description;
	private String merchantId;
	private String merchantTranId;
	private String payerVpa;
	private String payeeVpa;
	private String terminalId;
	private String txnStatus;
	private String txnCompletionDt;
	private String txnInitDt;
	private String subMerchantId;
	private String remitterAccountNumber;
	private String remitterAccountType;
	private String remitterFullName;
	private String remitterIFSCCode;
	private String remitterMobile;
	private String transferType;
	private String transferUniqueNumber;
	private String corporate;
	private String note;
	private String program;
	private String programCode;
	private String result;
	private String reject_reason;
	private String refund_status;
	private String m2pReferenceNumber;
	private String refund_txn_id;
	private String refund_bankTid;
	
}

