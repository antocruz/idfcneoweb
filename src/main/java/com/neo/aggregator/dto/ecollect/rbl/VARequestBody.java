package com.neo.aggregator.dto.ecollect.rbl;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VARequestBody {

	@JsonProperty(value = "Account_No")
	private String AccountNo;

	@JsonProperty(value = "Client_Id")
	private String ClientId;

	@JsonProperty(value = "VA_SerialNo")
	private String VaSerialNo;

	@JsonProperty(value = "VA_Beneficiary")
	private String VaBeneficiary;

}
