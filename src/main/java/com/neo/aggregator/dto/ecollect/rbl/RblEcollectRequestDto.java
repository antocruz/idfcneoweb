package com.neo.aggregator.dto.ecollect.rbl;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RblEcollectRequestDto {

	@JsonProperty(value = "messageType")
	private String messageType;

	@JsonProperty(value = "amount")
	private String amount;

	@JsonProperty(value = "UTRNumber")
	private String UTRNumber;

	@JsonProperty(value = "senderIFSC")
	private String senderIFSC;

	@JsonProperty(value = "senderAccountNumber")
	private String senderAccountNumber;

	@JsonProperty(value = "senderAccountType")
	private String senderAccountType;

	@JsonProperty(value = "senderName")
	private String senderName;

	@JsonProperty(value = "beneficiaryAccountType")
	private String beneficiaryAccountType;

	@JsonProperty(value = "beneficiaryAccountNumber")
	private String beneficiaryAccountNumber;

	@JsonProperty(value = "creditDate")
	private String creditDate;

	@JsonProperty(value = "creditAccountNumber")
	private String creditAccountNumber;

	@JsonProperty(value = "corporateCode")
	private String corporateCode;

	@JsonProperty(value = "clientCodeMaster")
	private String clientCodeMaster;

	@JsonProperty(value = "senderInformation")
	private String senderInformation;

}
