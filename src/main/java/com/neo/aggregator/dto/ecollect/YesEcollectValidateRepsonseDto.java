package com.neo.aggregator.dto.ecollect;

import lombok.Data;

@Data
public class YesEcollectValidateRepsonseDto {
	
	private EcollectData validateResponse;
	
	public YesEcollectValidateRepsonseDto() {
		validateResponse = new EcollectData();
	}
	
	public YesEcollectValidateRepsonseDto(String result) {
		validateResponse = new EcollectData();
		validateResponse.setResult(result);
	}

}
