package com.neo.aggregator.dto.ecollect;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddVARequestDto {

	@JsonProperty(value="tenant")
	private String tenant;

	@JsonProperty(value = "corpaccountnumber")
	private String corpAccNum;

	@JsonProperty(value = "institutioncode")
	private String corpClientId;

	@JsonProperty(value="fundloadmode")
	private String fundLoadMode;

	@JsonProperty(value="entityid")
	private String entityId;

	@JsonProperty(value="kitno")
	private String kitNo;

	@JsonProperty(value="firstname")
	private String firstName;

	@JsonProperty(value="lastname")
	private String lastName;

}
