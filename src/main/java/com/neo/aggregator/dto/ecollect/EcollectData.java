package com.neo.aggregator.dto.ecollect;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class EcollectData {

	private String customer_code;
	private String bene_account_no;
	private String bene_account_ifsc;
	private String bene_full_name;
	private String transfer_type;
	private String transfer_unique_no;
	private String transfer_timestamp;
	private String transfer_ccy;
	private String transfer_amt;
	private String rmtr_account_no;
	private String rmtr_account_ifsc;
	private String rmtr_account_type;
	private String rmtr_full_name;
	private String rmtr_to_bene_note;
	private String attempt_no;
	private String status;
	private String credit_acct_no;
	private String credited_at;
	private String reject_code;
	private String reject_reason;
	private String result;
	private String decision;
	
}

