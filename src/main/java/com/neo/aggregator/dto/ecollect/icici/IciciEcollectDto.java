package com.neo.aggregator.dto.ecollect.icici;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
@ToString
public class IciciEcollectDto {

	@JsonProperty(value = "Client Code")
	private String customerCode;

	@JsonProperty(value = "Virtual Account Number")
	private String vaNumber;

	@JsonProperty(value = "Mode")
	private String transactionMode;

	@JsonProperty(value = "UTR number")
	private String utrNo;

	@JsonProperty(value = "STRI")
	private String senderRemark;

	@JsonProperty(value = "CustomerAccountNo")
	private String CustomerAccountNo;

	@JsonProperty(value = "Transaction Amount")
	private String Amt;

	@JsonProperty(value = "Remitter Name")
	private String remitterName;

	@JsonProperty(value = "Remitter Account Number")
	private String remitterAccountNumber;

	@JsonProperty(value = "Remitter IFSC Code")
	private String remitterIfscCode;

	@JsonProperty(value = "Date")
	private String remitterPaymentDate;

	@JsonProperty(value = "BankInternalTransactionNumber")
	private String bnkInternalTranNumber;

	@JsonProperty(value="Status")
	private String status;
	
	@JsonProperty(value = "Reject Reason")
	private String rejectReason;

	@JsonProperty(value = "Refund code")
	private String refundCode;

}
