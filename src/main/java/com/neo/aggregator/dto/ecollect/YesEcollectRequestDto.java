package com.neo.aggregator.dto.ecollect;

import lombok.Data;

@Data
public class YesEcollectRequestDto {

	private EcollectData notify;
	
	private EcollectData validate;
	
}
