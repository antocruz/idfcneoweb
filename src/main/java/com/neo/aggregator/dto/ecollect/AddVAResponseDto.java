package com.neo.aggregator.dto.ecollect;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class AddVAResponseDto {
	
	@JsonProperty(value="EntityId")
	private String entityId;
	
	@JsonProperty(value="FullName")
	private String fullName;
	
	@JsonProperty(value="Virtual Account")
	private String vaNum;
	
	@JsonProperty(value="FundLoadMode")
	private String fundLoadMode;
	
	@JsonProperty(value="Status")
	private String status;

	@JsonProperty(value = "StatusReason")
	private String StatusReason;

}
