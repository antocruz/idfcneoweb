package com.neo.aggregator.dto.ecollect.rbl;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@JsonInclude(Include.NON_NULL)
public class VARequestHeader {

	@JsonProperty(value = "TranID")
	private String TranID;

	@JsonProperty(value = "Corp_ID")
	private String CorpID;

	@JsonProperty(value = "Maker_ID")
	private String MakerID;

	@JsonProperty(value = "Checker_ID")
	private String CheckerID;

	@JsonProperty(value = "Approver_ID")
	private String ApproverID;
	
	@JsonProperty(value="Status")
	private String Status;
	
	@JsonProperty(value="Error_Cde")
	private JsonNode ErrorCode;
	
	@JsonProperty(value="Error_Desc")
	private JsonNode ErrorDescription;

	@JsonProperty(value ="Resp_cde")
	private String responseCode;

	@JsonProperty(value = "Status_Reason")
	private String StatusReason;

}
