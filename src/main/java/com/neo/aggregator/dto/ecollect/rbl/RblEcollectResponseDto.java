package com.neo.aggregator.dto.ecollect.rbl;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RblEcollectResponseDto {

	//@JsonProperty(value = "Status")
	//private String Status;
	
	@JsonProperty(value = "ErrorCode")
	private String ErrorCode;
	
	@JsonProperty(value = "ErrorMsg")
	private String ErrorMsg;
	
	@JsonProperty(value = "Result")
	private String Result; 
}
