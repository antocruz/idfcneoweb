package com.neo.aggregator.dto.ecollect.rbl;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.Getter;
import lombok.Setter;


@JsonRootName(value = "create_VA")
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class RblVACreateResponseDto {
	
	@JsonProperty(value="Header")
	private VARequestHeader header;
	
	@JsonProperty(value="Details")
	private VAResponseDetails details;

}
