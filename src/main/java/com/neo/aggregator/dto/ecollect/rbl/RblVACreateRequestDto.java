package com.neo.aggregator.dto.ecollect.rbl;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.Builder;


@JsonRootName(value = "create_VA")
@JsonInclude(Include.NON_NULL)
@Builder
public class RblVACreateRequestDto {
	
	@JsonProperty(value="Header")
	private VARequestHeader header;
	
	@JsonProperty(value="Body")
	private VARequestBody body;
	
}
