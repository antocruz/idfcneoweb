package com.neo.aggregator.dto.ecollect.icici;

import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.cert.Certificate;

import javax.crypto.Cipher;

public class RSAEncAndDec {

	public byte[] encrypt(String plainText, String keyStorePassword) throws Exception {
		Key publicKey = readKey("PUBLIC", keyStorePassword);
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		byte[] cipherText = cipher.doFinal(plainText.getBytes());
		return cipherText;
	}

	public String decrypt(byte[] cipherTextArray, String keyStorePassword) throws Exception {
		Key privateKey = readKey("PRIVATE", keyStorePassword);
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		byte[] decryptedTextArray = cipher.doFinal(cipherTextArray);
		return new String(decryptedTextArray);
	}

	private Key readKey(String keyType, String keyStorePassword) throws IOException {
		Key key = null;
		try {
			KeyStore ks = KeyStore.getInstance("JKS");
			InputStream ksStream = this.getClass().getResourceAsStream("/yappay.in.jks");
			char[] password = keyStorePassword.toCharArray();
			ks.load(ksStream, password);
			if (keyType.equalsIgnoreCase("private")) {
				key = ks.getKey("yappay.rsa", password);
			} else {
				Certificate cert = ks.getCertificate("yappay.rsa");
				key = cert.getPublicKey();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return key;
	}
}
