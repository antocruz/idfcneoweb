package com.neo.aggregator.dto;

import java.util.Date;

import lombok.Data;

@Data
public class LienMarkRequestDto {
	
	private String entityId;

	private String accountId;

	private String amount;

	private Date startDate;

	private Date endDate;

	private String accountCurrency;

	private String reasonCode;

	private String remarks;

	private String moduleType;
}
