package com.neo.aggregator.dto;


import lombok.Data;

@Data
public class TDAcctCloseTrailResponseDto {
	
	private String status;
  
    private String tdAccount;
   
    private String withdrwlAmt;
  
    private String repayAccount;
   
    private String maturityDate;
    
    private String maturityAmt;
 
    private String depositAmt;
   
    private String lienAmt;

    private String contIntPcnt;
 
    private String effIntPcnt;

    private String actIntPcnt;

    private String intRecoverable;

    private String intPayable;

    private String intTaken;

    private String actualIntAmt;

    private String penalIntPcnt;

    private String penalIntAmt;

    private String taxPayable;

    private String taxTillDt;
 
    private String rePayAmount;

}
