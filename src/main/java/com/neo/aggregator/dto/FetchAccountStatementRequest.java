package com.neo.aggregator.dto;

import java.util.Date;

import com.neo.aggregator.enums.AccountType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FetchAccountStatementRequest {

	private String entityId;

	private String accountNo;

	private AccountType accountType;

	private String fromDate;

	private String toDate;

	private String branchId;

	private String lastBalance;

	private String lastBalanceCurrency;

	private Date lastTxnDate;

	private Date lastValueDate;

	private String lastBankTxnId;

	private String lastSerialNo;

}
