package com.neo.aggregator.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class AccountInquiryResponseDto {

	private String status;

	private String accountNo;

	private String schemeCode;

	private String schemeType;

	private String accountCurrency;

	private String accountOpenDate;

	private String modeOfOpertion;

	private String bankAccountStatus;

	private String customerId;

	private String customerName;

	private String netInterestRate;

	private String totalInterestAmount;

	private String initialDepositAmount;

	private String maturityAmount;

	private String depositTerm;

	private String maturityDate;

	private String repaymentAccountId;
	
	private String nomineeName;
	
	private String nomineeRelationship;
	
	private String partnerName;
	

}
