package com.neo.aggregator.dto;

import java.util.List;

import com.neo.aggregator.dto.v2.AccountsDto;
import com.neo.aggregator.dto.v2.AddressDto;
import com.neo.aggregator.dto.v2.CommunicationDto;
import com.neo.aggregator.dto.v2.CreditDto;
import com.neo.aggregator.dto.v2.KYCDto;
import com.neo.aggregator.dto.v2.KitInfoDto;
import com.neo.aggregator.dto.v2.NomineeDetailsDto;
import com.neo.aggregator.dto.v2.PreferenceDto;
import com.neo.aggregator.dto.v2.SecurityQuestionsDto;
import com.neo.aggregator.dto.v2.SpecialDatesDto;
import com.neo.aggregator.enums.EmploymentType;
import com.neo.aggregator.enums.EntityType;
import com.neo.aggregator.enums.GenderType;
import com.neo.aggregator.enums.HealthCategory;
import com.neo.aggregator.enums.IncomeCategory;
import com.neo.aggregator.enums.Industry;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RegistrationRequestDtoV2 {

	private String entityId;
	
	private EntityType entityCategory;
	
	private EntityType entityType;
	
	private String businessId;
	
	private String kycStatus;
	
	private String title;
	
	private String firstName;

	private String middleName;
	
	private String lastName;
	
	private GenderType gender;
	
	private String parentEntityId;
	
	private String branchCode;
	
	private String branchId;
		
	private Boolean addOnCard;
	
	private Boolean parentLimit;
	
	private Boolean parentStatement;
	
	private Boolean isNRICustomer;

	private Boolean isMinor;

	private Boolean isDependant;
	
	private Boolean isNRECustomer;
	
	private String maritalStatus;

	private List<KitInfoDto> kitInfo;
	
	private List<AddressDto> addressInfo;
	
	private List<CommunicationDto> communicationInfo;
	
	private List<AccountsDto> accountInfo;
	
	private List<KYCDto> kycInfo;
	
	private List<SpecialDatesDto> dateInfo;
	
	private List<SecurityQuestionsDto> securityQuestionsInfo;
	
	private List<NomineeDetailsDto> nomineeInfo;
	
	private CreditDto creditInfo;
	
	private PreferenceDto preferenceInfo;
	
	private String businessType;
	
	private String business;
	
	private EmploymentType employmentType;
	
	private Industry employmentIndustry;
	
	private String channelName;
	
	private Boolean rented;

	private Double avgCreditCardPayment;

	private Double monthlyEmi;

	private Double monthlyIncome;

	private Double yearlyIncome;
	
	private Integer noOfCreditCards;
	
	private Integer noOfLoans;
	
	private Boolean metro;
	
	private IncomeCategory incomeCategory;
	
	private Double asset;
	
	private String companyProfile;

	private String companyWebsite;

	private String mailId;

	private Boolean married;
	
	private Integer childrens;
	
	private Integer dependants;
	
	private HealthCategory healthCategory;
	
	private String custLeadId;
	
	private String custId;
	
	private String authCode;
	
	private String name;
	
	private String dob;
	
	private AddressDto addressDto;
	
	private String rrn;
	
	private String photo;
	
	private String motherMaidenName;
	
	private String fatherName;
	
	private String grossAnnualIncome;
	
	private String estimatedAgriculturalIncome;
	
	private String estimatedNonAgriculturalIncome;
	
	private String city;
	
	private String state;
	
	private String country;
	
	private String district;
	
	private Boolean isAgriculturalIncome;
	
	private String occupation;
	
	private String customerStatus;
	
	private String politicallyExposed; //yes or no
	
	private String fatcaDecl; // 1 or 0
	
	private String eKycRefNum;
	
	private String bioeKycUserId;
	
	private String bioeKycPassword;
	
}
