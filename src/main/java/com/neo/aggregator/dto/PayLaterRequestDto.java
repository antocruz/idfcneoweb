package com.neo.aggregator.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(exclude = { "otp" })
public class PayLaterRequestDto {

    private String mobileNumber;
    private String accountNumber;
    private String otp;
    private String transactionIdentifier;
    private String amount;
    private String consumerNumber;
    private String billRefInfo;
    private String consumerCode;
    private String txnParticulars;

}
