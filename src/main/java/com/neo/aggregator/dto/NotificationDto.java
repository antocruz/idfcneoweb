package com.neo.aggregator.dto;

import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Data
@ToString(exclude= {"mobileNo", "businessId", "args"})
public class NotificationDto {

    private String businessId;
    private String firstName;
    private String lastName;
    private String balance;
    private String entityId;
    private Double amount;
    private String template;
    private String mailId;
    private List<String> appGuids;
    private String merchantLocation;
    private String wallet;
    private String mobileNo;
    private String description;
    private String yourWallet;
    private String type;
    private Date time;
    private String transactionType;
    private String transactionOrigin;
    private String transactionStatus;
    private String externalTransactionId;
    private Object[] args;
    private Long txRef;
    private String productId;
    private String mcc;
    private String cardNo;
    private String merchantId;
    private String kitNo;
    private String channel;
    private String curCode;
    private String institutionCode;
    private String merchantName;
    private String prodType;
    private String productCode;
    private String proxyCardNo;
    private String retrievalRefNo;
    private String terminalId;
    private String traceNo;
    private Double transactionAmount;
    private String transactionCurrency;
    private Date txnDate;
    private String txnId;
    private String txnStatus;
    private String authCode;
    private String acquirerId;
    private String network;
    private HashMap<String,String> emailMap;
    private String dateTime;
    private Double transactionFees;
    private String readerTime;
    private String business;
    private String billingCurrency;
    private String billingAmount;
    private String settlementCurrency;
    private String settlementAmount;
    private String accqInstitutionCode;
    private String corporate;
    private String virtualAccountNo;
    private String crdr;
    private String cardEnding;
    private String subTemplate;
    private String merchantUrl;
}
