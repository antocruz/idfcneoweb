package com.neo.aggregator.dto;

import com.neo.aggregator.dto.v2.AddressDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

public class AadhaarOtpResponse {
	
	private AadhaarOtpInnerValidationResponse validationResponse = new AadhaarOtpInnerValidationResponse();
	
	private String custLeadId;
	private String custId;
	private String add;
	private String title;
	private RegistrationRequestDtoV2 registrationRequestDtoV2;
	private String kycStatus;
	private String result;
	private String entityId;
	private String name;
	private String gender;
	private String dob;
	private AddressDto addressDto;
	private String photo;
	private String authCode;
	private String rrn;

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public AddressDto getAddressDto() {
		return addressDto;
	}

	public void setAddressDto(AddressDto addressDto) {
		this.addressDto = addressDto;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getCustLeadId() {
		return custLeadId;
	}

	public void setCustLeadId(String custLeadId) {
		this.custLeadId = custLeadId;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getAdd() {
		return add;
	}

	public void setAdd(String add) {
		this.add = add;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public RegistrationRequestDtoV2 getRegistrationRequestDtoV2() {
		return registrationRequestDtoV2;
	}

	public void setRegistrationRequestDtoV2(RegistrationRequestDtoV2 registrationRequestDtoV2) {
		this.registrationRequestDtoV2 = registrationRequestDtoV2;
	}

	public String getKycStatus() {
		return kycStatus;
	}

	public void setKycStatus(String kycStatus) {
		this.kycStatus = kycStatus;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public AadhaarOtpInnerValidationResponse getValidationResponse()
	{
		return validationResponse;
	}

	public void setValidationResponse(AadhaarOtpInnerValidationResponse validationResponse) {
		this.validationResponse = validationResponse;
	}

	@Builder
	@AllArgsConstructor
	@NoArgsConstructor
	public static class AadhaarOtpInnerValidationResponse {
		
		private String requestId;

		private String responseCode;

		private String responseMessage;

		private String description;

		private String ekycRefNo;

		private String otpReferenceNo;

		private EkycData ekycData;

		public String getRequestId() {
			return requestId;
		}

		public void setRequestId(String requestId) {
			this.requestId = requestId;
		}

		public String getResponseCode() {
			return responseCode;
		}

		public void setResponseCode(String responseCode) {
			this.responseCode = responseCode;
		}

		public String getResponseMessage() {
			return responseMessage;
		}

		public void setResponseMessage(String responseMessage) {
			this.responseMessage = responseMessage;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getEkycRefNo() {
			return ekycRefNo;
		}

		public void setEkycRefNo(String ekycRefNo) {
			this.ekycRefNo = ekycRefNo;
		}

		public String getOtpReferenceNo() {
			return otpReferenceNo;
		}

		public void setOtpReferenceNo(String otpReferenceNo) {
			this.otpReferenceNo = otpReferenceNo;
		}

		public EkycData getEkycData() {
			return ekycData;
		}

		public void setEkycData(EkycData ekycData) {
			this.ekycData = ekycData;
		}

	}

	@Builder
	@AllArgsConstructor
	@NoArgsConstructor
	public static class EkycData {

		private String firstName;

		private String lastName;

		private String country;

		private String address;

		private String city;

		private String house;

		private String kycImage;

		private String area;

		private String dob;

		private String street;

		private String district;

		private String pinCode;

		private String state;

		private String careOf;

		private String gender;

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getHouse() {
			return house;
		}

		public void setHouse(String house) {
			this.house = house;
		}

		public String getKycImage() {
			return kycImage;
		}

		public void setKycImage(String kycImage) {
			this.kycImage = kycImage;
		}

		public String getArea() {
			return area;
		}

		public void setArea(String area) {
			this.area = area;
		}

		public String getDob() {
			return dob;
		}

		public void setDob(String dob) {
			this.dob = dob;
		}

		public String getStreet() {
			return street;
		}

		public void setStreet(String street) {
			this.street = street;
		}

		public String getDistrict() {
			return district;
		}

		public void setDistrict(String district) {
			this.district = district;
		}

		public String getPinCode() {
			return pinCode;
		}

		public void setPinCode(String pinCode) {
			this.pinCode = pinCode;
		}

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

		public String getCareOf() {
			return careOf;
		}

		public void setCareOf(String careOf) {
			this.careOf = careOf;
		}

	}

}
