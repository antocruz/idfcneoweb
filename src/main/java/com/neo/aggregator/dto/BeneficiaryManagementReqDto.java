package com.neo.aggregator.dto;

import com.neo.aggregator.enums.AccountType;
import lombok.Data;

import java.util.Set;

@Data
public class BeneficiaryManagementReqDto {

    private String beneficiaryAccountNo;

    private String beneficiaryId;

    private String cardNo;

    private String beneficiaryType;

    private String ifsc;

    private String beneficiaryName;

    private String otp;

    private String entityId;

    private AccountType accountType;

    private Set<String> transferMode;

    private String nickName;

    private String beneficiaryMobile;

    private String beneficiaryEmail;

    private String beneficiaryBankName;
}
