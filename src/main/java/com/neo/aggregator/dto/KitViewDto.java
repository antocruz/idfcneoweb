package com.neo.aggregator.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class KitViewDto {
    String kitNo;
    String maskedCardNo;
    String expiryDate;
    String status;
}
