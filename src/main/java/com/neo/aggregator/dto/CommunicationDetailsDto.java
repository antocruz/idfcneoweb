package com.neo.aggregator.dto;

import lombok.Data;

@Data
public class CommunicationDetailsDto {

	private String comType;
	private Boolean preferred;
	private String countryCode;
	private String contactNo;
	private String email;

}
