package com.neo.aggregator.dto;

import java.util.List;

import com.neo.aggregator.dto.v2.AccountsDto;
import com.neo.aggregator.dto.v2.AddressDto;
import com.neo.aggregator.dto.v2.NomineeDetailsDto;

import lombok.Data;

@Data
public class AccountCreationRequestDto {

	private String entityId;

	private String businessType;

	private String cifNumber;

	private List<AccountsDto> accountInfo;
	
	private List<NomineeDetailsDto> nomineeInfo;
	
	private String debitAccount;
	
	private String renewalMonths;

	private Boolean autoClosureOnMaturity;

	private Boolean autoRenewal;

	private String amount;

	private String depositMonths;
	
	private List<AddressDto> nomineeContactInfo;

}
