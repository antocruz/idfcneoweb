package com.neo.aggregator.dto.yesbank;

import lombok.Data;

@Data
public class ValidateVpaReq {
    String merchantId;
    String merchantTxnId;
    String virtualAddr;
    String checkType;
    String app;
    String geoCode;
    String location;
    String ipAddr;
    String type;
    String capability;
    String os;
    String deviceId;
    String simId;
    String systemUniqueId;
    String bluetoothMac;
    String wifiMac;
}
