package com.neo.aggregator.dto.yesbank;

import lombok.Data;

@Data
public class NeoPayRequest {
    String merchantId;
    String orderNo;
    String txnNote;
    String amount;
    String currency;
    String paymentType;
    String txnType;
    String mcc;
    String expTime;
    String payeeAccNo;
    String payeeIfsc;
    String payeeAadhar;
    String payeeMobNo;
    String payeeVpa;
    String subMerchantId;
    String whiteListedAcc;
    String payeeMMid;
    String refUrl;
    String transferType;
    String payeeName;
    String payeeAddress;
    String payeeEmail;
    String payerAccNo;
    String payerIfsc;
    String payerMbNo;
    String payeeVpaType;
}
