package com.neo.aggregator.dto.yesbank;

import lombok.Data;

@Data
public class ValidateVpaRes {
	String yblRefNo;
	String vpa;
	String customerName;
	String status;
	String statusDesc;
}
