package com.neo.aggregator.dto.yesbank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PayRequest {
    String requestMsg;
    String pgMerchantId;
}
