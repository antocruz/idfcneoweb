package com.neo.aggregator.dto.yesbank;

import lombok.Data;

@Data
public class MerchantOBRes {
	String mebussname;
	String pgMerchantId;
	String action;
	String statusDesc;
	String status;
	String integrationType;
	String merVirtualAdd;
	String crtDate;
	String requestId;
	String loginaccess;
}
