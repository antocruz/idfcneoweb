package com.neo.aggregator.dto.yesbank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class NeoPayResponse {
	String yblTxnId;
	String orderNo;
	String amount;
	String date;
	String statusCode;
	String statusDesc;
	String responseCode;
	String approvalNum;
	String payerVpa;
	String npciTxnId;
	String custRefId;
	String payerAccNo;
	String payerIfsc;
	String payerAccountName;
	String errorCode;
	String responseErrorCode;
	String transferType;
}
