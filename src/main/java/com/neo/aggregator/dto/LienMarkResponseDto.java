package com.neo.aggregator.dto;

import lombok.Data;

@Data
public class LienMarkResponseDto {

	private String accountId;

	private String lienId;

	private String status;

}
