package com.neo.aggregator.dto;

import lombok.Data;

@Data
public class ImpsBalanceResponseDto {

	private String version;
    private String accountCurrencyCode;
    private Float accountBalanceAmount;
    private String lowBalanceAlert;
    private String result;
    private Double balanceAmt;
    private String error;
    
    public ImpsBalanceResponseDto() {
	}
    
    public ImpsBalanceResponseDto(NeoResponse response) {
    	
	}
    
}