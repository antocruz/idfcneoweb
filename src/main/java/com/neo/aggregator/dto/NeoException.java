package com.neo.aggregator.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@JsonIgnoreProperties("stackTrace")
public class NeoException extends Exception {

	private static final long serialVersionUID = -1558369527290009966L;
	private String shortMessage;
    private String detailMessage;
    private String languageCode;
    private String errorCode;
	private String displayMessage;
	
	public NeoException(String message) {
		this.shortMessage = message;
		this.detailMessage = message;
	}
}
