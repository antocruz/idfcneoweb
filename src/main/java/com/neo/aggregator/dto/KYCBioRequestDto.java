package com.neo.aggregator.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown=true)
public class KYCBioRequestDto {

        private String ekycRefnum;

        private String entityId;

        private String skey;

        private String ci;

        private String mc;

        private String dataType;

        private String dataValue;

        private String hmac;

        private String mi;

        private String rdsId;

        private String rdsVer;

        private String dpId;

        private String dc;

        private RegistrationRequestDtoV2 registrationRequestDtoV2;

        private String idTypeValidation;

        private String idNumber;

        private String motherMaidenName;

        private String fatherName;

        private String grossAnnualIncome;

        private String estimatedAgriculturalIncome;

        private String estimatedNonAgriculturalIncome;
        
        private Wadh wadh;

        private String consent;

        //these 3 keys are used for RBL Bio Ekyc flow
        private String bioeKycUserId;
        private String bioeKycPassword;
        private String bioeKycBCAgentId;
        private String bioeKycRefPrefix;
        
        private String otpVerificationCode;
    	private String agentName;
    	private String agentUcic;
    	private String agentAccountNumber;
    	private String agentAddress;
    	private String agentCity;
    	private String agentState;
    	private String agentPincode;
    	private String corporateName;
    	private String corporateAddress;
    	private String corporateCity;
    	private String corporateState;
    	private String corporatePincode;
    	private String fundSource;
    	private String signature;

    	private Boolean isAgriculturalIncome;
}
