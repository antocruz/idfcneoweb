package com.neo.aggregator.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BalanceCheckResponseDto {

	private String accountNo;

	private String branchId;

	private String currency;

	private List<AccountBalance> accountBalance;

	@Getter
	@Setter
	@AllArgsConstructor
	@NoArgsConstructor
	public static class AccountBalance {

		private String productId;

		private String balance;

		private String currency;

	}

}
