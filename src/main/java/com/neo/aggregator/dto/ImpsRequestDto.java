package com.neo.aggregator.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ImpsRequestDto {

	private String version;
	private String uniqueRequestNo;
	
	@NotNull(message="Amount should not be null or empty")
	private BigDecimal amount;
	
	private String description;
	private String appId;
	private String customerId;
	
	private String beneficiaryAccountNumber;
	private String beneficiaryName;
	private String beneficiaryIfscCode;
	private String beneficiaryMobileNo;
	private String beneficiaryMMID;
	private String beneficiaryContactNo;
	private String beneficiaryEmail;
	private String beneficiaryAddress1;
	private String beneficiaryAddress2;
	private String beneficiaryAddress3;
	private String beneficiaryCity;
	private String beneficiaryCountry;
	private String beneficiaryPostalCode;
	private String beneficiaryStateProvince;
	
	private String debitAccountNo;
	private String debitAccountType;
	private String remmiterName;
	private String remmiterIfscCode;
	private String remmiterMobileNo;
	private String remmiterMMID;
	private String remmiterContactNo;
	private String remmiterEmail;
	private String remmiterAddress1;
	private String remmiterAddress2;
	private String remmiterAddress3;
	private String remmiterCity;
	private String remmiterCountry;
	private String remmiterPostalCode;
	private String remmiterStateProvince;
	
	private String remitterToBeneficiaryInfo;
	
	// @NotNull(message="Transfer Type should not be null or empty")
	// @NotEmpty(message="Transfer Type should not be null or empty")
	private String transferType;
	
	private String transferCurrencyCode;
	private boolean isReversal;
	private String branchId;
	private String userId;
	private String originalRefNo;
	
	private String externalTransactionId;
	private String fromTenant;
	private String toTenant;
	private String corporate;
	
	private String transferMode;
	
	// @NotNull(message="Bank should not be null or empty")
	// @NotEmpty(message="Bank should not be null or empty")
	private String bank;
	
	private String transactionType;
}