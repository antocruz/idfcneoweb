package com.neo.aggregator.dto;

import lombok.Data;

@Data
public class NeoPaginate {
	
    private Boolean isList;
    private Integer pageSize;
    private Integer pageNo;
    private Integer totalPages;
    private Integer totalElements;

}
