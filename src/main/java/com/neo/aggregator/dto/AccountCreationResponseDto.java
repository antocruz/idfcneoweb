package com.neo.aggregator.dto;

import lombok.Data;

@Data
public class AccountCreationResponseDto {

	private String description;

	private String status;

	private String entityId;

	private String accountId;

}
