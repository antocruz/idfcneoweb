package com.neo.aggregator.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(exclude = { "idNumber" })
public class PanValidationRequest {

	private String entityId;

	private String idType;

	private String idNumber;

	private String firstName;

	private String middleName;

	private String lastName;

	private String txnRef;

}
