package com.neo.aggregator.dto;

import lombok.Data;

@Data
public class AccountModificationResponseDto {

	private String description;

	private String status;

	private String entityId;

	private String accountId;

}
