package com.neo.aggregator.dto;

import lombok.Data;

@Data
public class ImpsTransactionResponseDto {

    private String version;
    private String uniqueResponseNo;
    private String attemptNo;
    private String transferType;
    private String lowBalanceAlert;
    private TransactionStatus transactionStatus;
    private String nameWithBeneficiaryBank;
    private String requestReferenceNo;

    @Data
    public static class TransactionStatus {
        private String statusCode;
        private String subStatusCode;
        private String bankReferenceNo;
        private String beneficiaryReferenceNo;
        private String reason;
    }
}
