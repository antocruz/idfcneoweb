package com.neo.aggregator.dto;

import java.util.List;

import com.neo.aggregator.dto.v2.AddressDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(exclude = {"otp" })
@Data
public class FundTransferRequestDto {

	private String fromAccountNo;
	private String toAccountNo;
	private String entityId;
	private String remitterName;
	private String description;
	private String amount;
	private String transactionCurrency;
	private String transactionOrigin;
	private String externalTransactionId;
	private String beneficiaryAccountType;
	private String beneficiaryName;
	private String beneficiaryIfsc;
	private String transactionType;
	private String transactionMode;
	private String transactionExpiry;
	private String reoccurrence;
	private List<AddressDto> address;
	private String remitterNumber;

	// validateOTP
	private String otp;
	private String stan;
	
	private String beneficiaryMobile;
	private String beneficiaryEmail;
	private String remitterIfsc;

	private String isWalletDebit;
	
	private Boolean resendOtp;
	private String version;
	private String uniqueRequestNo;
	
	
	private String appId;
	private String customerId;
	
	private String beneficiaryAccountNumber;
	private String beneficiaryIfscCode;
	private String beneficiaryMobileNo;
	private String beneficiaryMMID;
	private String beneficiaryContactNo;
	private String beneficiaryAddress1;
	private String beneficiaryAddress2;
	private String beneficiaryAddress3;
	private String beneficiaryCity;
	private String beneficiaryCountry;
	private String beneficiaryPostalCode;
	private String beneficiaryStateProvince;
	
	private String debitAccountNo;
	private String debitAccountType;
	private String remmiterName;
	private String remmiterIfscCode;
	private String remmiterMobileNo;
	private String remmiterMMID;
	private String remmiterContactNo;
	private String remmiterEmail;
	private String remmiterAddress1;
	private String remmiterAddress2;
	private String remmiterAddress3;
	private String remmiterCity;
	private String remmiterCountry;
	private String remmiterPostalCode;
	private String remmiterStateProvince;
	
	private String remitterToBeneficiaryInfo;
	
	// @NotNull(message="Transfer Type should not be null or empty")
	// @NotEmpty(message="Transfer Type should not be null or empty")
	
	private String transferCurrencyCode;
	private boolean isReversal;
	private String branchId;
	private String userId;
	private String originalRefNo;
	
	private String fromTenant;
	private String toTenant;
	private String corporate;
	
	private String transferMode;
	
	// @NotNull(message="Bank should not be null or empty")
	// @NotEmpty(message="Bank should not be null or empty")
	private String bank;
	private String postTxnBalance;
}
