package com.neo.aggregator.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(exclude = { "idNumber", "otp" })
public class AadhaarOtpRequest {

	private String entityId;

	private String idType;

	private String idTypeValidation;

	private String idNumber;

	private String firstName;

	private String middleName;

	private String lastName;

	private String stan;

	private String requestId;

	private String localTransactionDateTime;

	private String channel;

	private String bank;

	private String tpa;

	private String txnRef;

	private String otp;

	private String otpReferenceNo;

	private RegistrationRequestDtoV2 registrationRequestDtoV2;

    private String motherMaidenName;

    private String fatherName;

    private String grossAnnualIncome;

    private String estimatedAgriculturalIncome;

    private String estimatedNonAgriculturalIncome;
    
    private String pidBlock;
    
    private String consent;
    
    private String otpVerificationCode;
	private String agentName;
	private String agentUcic;
	private String agentAccountNumber;
	private String agentAddress;
	private String agentCity;
	private String agentState;
	private String agentPincode;
	private String corporateName;
	private String corporateAddress;
	private String corporateCity;
	private String corporateState;
	private String corporatePincode;
	private String fundSource;
	private String signature;

	private String skey;
	private String ci;
	private String dataType;
	private String dataValue;
	private String hmac;

	private String bioeKycUserId;
	private String bioeKycPassword;
	
	private Boolean isAgriculturalIncome;

}
