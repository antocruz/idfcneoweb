package com.neo.aggregator.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.neo.aggregator.dto.v2.AddressDto;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KYCBioResponseDto {

    private String entityId;
    private String status;
    private String responsecode;
    private String responsemessage;
    private String name;
    private String careOf;
    private String gender;
    private String dob;
    private AddressDto addressDto;
    private String photo;
    private String uidtoken;
    private String authCode;
    private String rrn;
    private String kycRefNo;
    private String kycRedirectUrl;
    //private RegistrationRequestV2Dto registrationRequestDtoV2;

	private String custLeadId;
	private String custId;
	private String add;
	private String title;
	private RegistrationRequestDtoV2 registrationRequestDtoV2;
	private String kycStatus;
	private String description;
	private String result;
	private String encryptedData;
}
