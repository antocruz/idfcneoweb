package com.neo.aggregator.dto;

public class AuthorizationResponseDto {

	private Response offIssuerPurchaseResult = new Response();

	public Response getOffIssuerPurchaseResult() {
		return offIssuerPurchaseResult;
	}

	public void setOffIssuerPurchaseResult(Response offIssuerPurchaseResult) {
		this.offIssuerPurchaseResult = offIssuerPurchaseResult;
	}

	public static class Response {
		private String TxnRefNo = "13152454252343";
		private String Status = "true";
		private String Description = "test";
		private String SettlementDate = "20150504";
		private String CurrencyCode = "INR";
		private String Balance = "0D";
		private String Mcc = "6012";
		private String TxnID;
		private String AuthID;
		private String fundingSource;

		public String getTxnRefNo() {
			return TxnRefNo;
		}

		public void setTxnRefNo(String txnRefNo) {
			TxnRefNo = txnRefNo;
		}

		public String getStatus() {
			return Status;
		}

		public void setStatus(String status) {
			Status = status;
		}

		public String getDescription() {
			return Description;
		}

		public void setDescription(String description) {
			Description = description;
		}

		public String getSettlementDate() {
			return SettlementDate;
		}

		public void setSettlementDate(String settlementDate) {
			SettlementDate = settlementDate;
		}

		public String getCurrencyCode() {
			return CurrencyCode;
		}

		public void setCurrencyCode(String currencyCode) {
			CurrencyCode = currencyCode;
		}

		public String getBalance() {
			return Balance;
		}

		public void setBalance(String balance) {
			Balance = balance;
		}

		public String getMcc() {
			return Mcc;
		}

		public void setMcc(String mcc) {
			Mcc = mcc;
		}

		public String getTxnID() {
			return TxnID;
		}

		public void setTxnID(String txnID) {
			TxnID = txnID;
		}

		public String getAuthID() {
			return AuthID;
		}

		public void setAuthID(String authID) {
			AuthID = authID;
		}

		public String getFundingSource() {
			return fundingSource;
		}

		public void setFundingSource(String fundingSource) {
			this.fundingSource = fundingSource;
		}

	}
}
