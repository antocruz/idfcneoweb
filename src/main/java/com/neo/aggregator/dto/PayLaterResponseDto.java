package com.neo.aggregator.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PayLaterResponseDto {

    private String accountNumber;
    private String status;
    private String message;
    private String errCode;
    private String paymentId;
    private String txnAmount;
    private String responseCode;
    private String appName;
    private String mobileNumber;
    private String transactionIdentifier;
    private String amount;

    private List<AccountBalance> accountBalance;

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class AccountBalance {

        private String productId;

        private String balance;

        private String currency;

    }
}
