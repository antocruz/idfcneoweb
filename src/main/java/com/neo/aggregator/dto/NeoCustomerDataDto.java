package com.neo.aggregator.dto;

import java.util.List;

import com.neo.aggregator.dto.v2.AccountsDto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NeoCustomerDataDto {

	private String cifNumber;

	private List<AccountsDto> accountInfo;

	private String entityId;

	private String corporate;

}
