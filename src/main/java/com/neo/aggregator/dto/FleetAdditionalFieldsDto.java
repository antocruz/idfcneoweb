package com.neo.aggregator.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties
@JsonInclude(Include.NON_NULL)
public class FleetAdditionalFieldsDto {

	private String registrationDate;
	private String rcStatus;
	private String fuelType;
	private String makerModel;
	private String emissonNorm;
	private String financierName;
	private String engineNumber;

}
