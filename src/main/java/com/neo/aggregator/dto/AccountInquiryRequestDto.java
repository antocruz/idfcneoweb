package com.neo.aggregator.dto;

import lombok.Data;

@Data
public class AccountInquiryRequestDto {

	private String description;

	private String status;

	private String entityId;

	private String accountId;

}
