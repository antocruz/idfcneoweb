package com.neo.aggregator.dto;

import java.util.Date;
import java.util.List;

import com.neo.aggregator.dto.v2.AddressDto;
import com.neo.aggregator.dto.v2.DocumentDto;
import com.neo.aggregator.dto.v2.Preference;

import lombok.Data;

@Data
public class RegistrationRequestDto {

	private String entityId;
	private String entityType;
	private String businessType;
	private String businessId;
	private String countryofIssue;
	private String cardType;
	private String kitNo;
	private String title;
	private String firstName;
	private String middleName;
	private String lastName;
	private String gender;
	private Date dateOfBirth;
	private String contactNo;
	private String emailAddress;
	private String address;
	private String address2;
	private String city;
	private String state;
	private String country;
	private String pincode;
	private String idType;
	private String idNumber;
	private String idExpiry;
	private String eKycRefNo;
	private String kycStatus;
	private String countryCode;
	private String programType;
	private String salutation;
	private Boolean isMinor;
	private Boolean isNRI;
	private String occupation;
	private String maritalStatus;
	private List<AddressDto> addressDetails;
	private List<CommunicationDetailsDto> communicationDetails;
	private List<DocumentDto> documents;
	private Preference preference;
	private Date specialDate;
}
