package com.neo.aggregator.dto;

import lombok.Data;

@Data
public class ImpsTransactionRequestDto {

    private String version = "2";
    private String uniqueRequestNo;
    private String appId;
    private String customerId;
    private Double amount;
    private String description;
    private String debitAccountNo;
    private String beneficiaryName;
    private String beneficiaryAccountNumber;
    private String beneficiaryIfscCode;
    private String beneficiaryMobileNo;
    private String beneficiaryContactNo;
    private String beneficiaryEmail;
    private String beneficiaryAddress1 = "22 CHENNAI";
    private String beneficiaryAddress2;
    private String beneficiaryAddress3;
    private String beneficiaryCity;
    private String beneficiaryCountry;
    private String beneficiaryPostalCode;
    private String beneficiaryStateProvince;
    private String remitterToBeneficiaryInfo;
    private String transferType;

}

