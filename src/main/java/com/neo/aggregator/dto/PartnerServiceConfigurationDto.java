package com.neo.aggregator.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Data
public class PartnerServiceConfigurationDto {

	@NotEmpty(message = "Bank Id is Mandatory")
	private String bankId;

	@NotEmpty(message = "Api Url is mandatory")
	private String serviceEndpoint;

	@NotEmpty(message = "Bank Service Id is Mandatory")
	private String bankServiceId;
}
