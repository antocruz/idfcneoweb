package com.neo.aggregator.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


import lombok.Data;

@Data
public class ImpsStatusDto {
	
	private String version;
	private String appID;
	private String customerID;
	private String requestReferenceNo;
	
	@NotNull(message="Bank should not be null or empty")
	@NotEmpty(message="Bank should not be null or empty")
	private String bank;
	private String tenant;
	private String corporate;
	private String transferType;
	private String accountType;
}
