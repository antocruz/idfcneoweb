package com.neo.aggregator.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DecryptKeyDto {

	private String encryptedKey;

	private String icv;

	private String keyIndex;

	private String encryptionMode;

}
