package com.neo.aggregator.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class FetchAccountStatemetResponse {

	private String accountId;

	private String availableBalance;

	private String accountCurrency;

	private String branchId;

	private String ledgerBalance;

	private List<Transaction> transaction;

	private String hasMoreData;

	@Getter
	@Setter
	@AllArgsConstructor
	@NoArgsConstructor
	@JsonInclude(Include.NON_NULL)
	public static class Transaction {

		private String amount;

		private String balance;

		private String type;

		private Long time;

		private Date txnDate;

		private Date valueDate;

		private String txnCategory;

		private String txnId;

		private String description;

		private String currency;

		private String serialNo;

	}

}
