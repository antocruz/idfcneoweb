package com.neo.aggregator.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonIgnoreProperties
@JsonInclude(Include.NON_NULL)
public class ForexAddField {
	private String travelType;
	private String pot;
	private String passportIssuedAt;
	private String passportIssueDate;
	private String passportExpiryDate;
	private String fatherName;
	private String motherMaidenName;
	private String countryOfTaxResidence;
	private String taxINumber;
	private String identificationType;
	private String taxDeclaration;
	private String cityOfBirth;
	private String countryOfBirth;
	private String travelDuration;
	private String applicationDate;
	private String countryOfTravel;
	private String tripStartDate;
	private String tripEndDate;
	private String passportType;
	private String customerCategory;
	private String nationality;
	private String nomineeName;
	private String nomineeRelationShip;
	private String travelAddField1;
	private String travelAddField2;
	private String travelAddField3;
	private String travelAddField4;
	private String travelAddField5;
	private String travelAddField6;
	private String travelAddField7;
	private String travelAddField8;
	private String travelAddField9;
	private String travelAddField10;

}