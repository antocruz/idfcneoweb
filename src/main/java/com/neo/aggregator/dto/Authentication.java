package com.neo.aggregator.dto;

import lombok.Data;

@Data
public class Authentication {
	private String tcc;
	private String entry;
	private String otp = "NA";
	private String cavv = "NA";
	private String cvv1 = "NA";
	private String cvv2 = "NA";
	private String pin = "NA";
	private String emv = "NA";
}