package com.neo.aggregator.dto.neolego;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName(value = "panData")
public class PanDataDto {

	@JsonProperty(value = "panNo")
	private String panNo;

	@JsonProperty(value = "exists")
	private String exists;

	@JsonProperty(value = "lastName")
	private String lastName;

	@JsonProperty(value = "firstName")
	private String firstName;

	@JsonProperty(value = "middleName")
	private String middleName;

	@JsonProperty(value = "title")
	private String title;

	@JsonProperty(value = "issueDate")
	private String issueDate;

	@JsonProperty(value = "nameOnCard")
	private String nameOnCard;

	@JsonProperty(value = "valid")
	private Boolean valid;

	@JsonProperty(value = "lastUpdated")
	private String lastUpdated;

	@JsonProperty(value = "message")
	private String message;

	@JsonProperty(value = "pannumber")
	private String pannumber;

	@JsonProperty(value = "validationId")
	private String validationId;

}
