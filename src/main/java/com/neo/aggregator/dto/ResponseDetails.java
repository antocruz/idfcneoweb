package com.neo.aggregator.dto;

import lombok.Data;

@Data
public class ResponseDetails {

	private String status;
	private String description;

}
