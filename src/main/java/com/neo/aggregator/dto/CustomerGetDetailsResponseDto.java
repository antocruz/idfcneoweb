package com.neo.aggregator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Data
public class CustomerGetDetailsResponseDto {

	@JsonProperty(value = "status")
	private String status;
	@JsonProperty(value = "firstname")
	private String firstname;
	@JsonProperty(value = "middlename")
	private String middlename;
	@JsonProperty(value = "lastname")
	private String lastname;
	@JsonProperty(value = "dateofbirth")
	private String dateofbirth;
	@JsonProperty(value = "gender")
	private String gender;
	@JsonProperty(value = "mothermaidenname")
	private String mothermaidenname;
	@JsonProperty(value = "nationality")
	private String nationality;
	@JsonProperty(value = "resaddress1")
	private String resaddress1;
	@JsonProperty(value = "resaddress2")
	private String resaddress2;
	@JsonProperty(value = "city")
	private String city;
	@JsonProperty(value = "state")
	private String state;
	@JsonProperty(value = "pincode")
	private String pincode;
	@JsonProperty(value = "rescountry")
	private String rescountry;
	@JsonProperty(value = "mobilenumber")
	private String mobilenumber;
	@JsonProperty(value = "emailaddress")
	private String emailaddress;
	@JsonProperty(value = "laddress1")
	private String laddress1;
	@JsonProperty(value = "laddress2")
	private String laddress2;
	@JsonProperty(value = "lcity")
	private String lcity;
	@JsonProperty(value = "lstate")
	private String lstate;
	@JsonProperty(value = "lpincode")
	private String lpincode;
	@JsonProperty(value = "lcountry")
	private String lcountry;
	@JsonProperty(value = "CustomerStatus")
	private String CustomerStatus;
	@JsonProperty(value = "CustomerType")
	private String CustomerType;
	@JsonProperty(value = "SourceIncomeType")
	private String SourceIncomeType;
	@JsonProperty(value = "AnnualIncome")
	private String AnnualIncome;
	@JsonProperty(value = "PoliticallyExposedPerson")
	private String PoliticallyExposedPerson;
	@JsonProperty(value = "customerid")
	private String customerid;
	@JsonProperty(value = "Approvalstatus")
	private String Approvalstatus;
	@JsonProperty(value = "cardAlias")
	private String cardAlias;
	@JsonProperty(value = "AccountNumber")
	private String AccountNumber;
	@JsonProperty(value = "idproofname")
	private String idproofname;
	@JsonProperty(value = "idproofnumber")
	private String idproofnumber;
	@JsonProperty(value = "Reqtype")
	private String Reqtype;
	@JsonProperty(value = "Reserved1")
	private String Reserved1;
	@JsonProperty(value = "Reserved2")
	private String Reserved2;
	@JsonProperty(value = "Reserved3")
	private String Reserved3;
	@JsonProperty(value = "Reserved4")
	private String Reserved4;

}
