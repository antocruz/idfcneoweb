package com.neo.aggregator.dto.v2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.neo.aggregator.enums.AddressCategory;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(Include.NON_NULL)
public class AddressDto {

	private AddressCategory addressCategory;
	
	private String address1;
	private String address2;
	private String address3;
	private String city;
	private String state;
	private String country;
	private String pincode;
	private String district;
	private String isoCountryCode;
	
}
