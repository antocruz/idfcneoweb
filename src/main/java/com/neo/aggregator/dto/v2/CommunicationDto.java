package com.neo.aggregator.dto.v2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(Include.NON_NULL)
public class CommunicationDto {

	private String contactNo;
	private String emailId;
	private Boolean notification;
	private String appId;
	private String prefferedTypes;
	private String comType;
	private String countryCode;
	
}
