package com.neo.aggregator.dto.v2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.neo.aggregator.enums.AccountStatus;
import com.neo.aggregator.enums.AccountType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(Include.NON_NULL)
public class AccountsDto {

	private String accountNo;
	
	private AccountType accountType;
	
	private String accountsCurrency;
	
	private String product;
	
	private AccountStatus accountStatus;
	
	private Boolean defaultAccount = false;
	
	private String ifscCode;
	
	private String branchId;
	
	private Double amount;
	
	private String schemeCode;
	
	private String paymentRef;
	
	private Integer depositMonths;

}
