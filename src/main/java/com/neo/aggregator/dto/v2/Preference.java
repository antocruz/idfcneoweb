package com.neo.aggregator.dto.v2;

import lombok.Data;

@Data
public class Preference {
	
	private String currency = "INR";
	
	private String preferedLocale = "en_US";

}
