package com.neo.aggregator.dto.v2;

import java.util.Date;

import lombok.Data;

@Data
public class DocumentDto {
	
	private String docId;
	private String docType;
	private String docNo;
	private Date docIssueDate;
	private Date docExpDate;
	private String countryOfIssue;
	private String docIssuedOrganisation;
	private Boolean eSign;
	private String docContentType;

}
