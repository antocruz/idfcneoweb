package com.neo.aggregator.dto.v2;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class PreferenceDto {

	private String atm;
	
	private String pos;
	
	private String ecom;
	
	private String international;
	
	private String dcc;
	
	private String contactless;
	
	private String limit;

	private String country;
	
	private String secKitNo;
	
}
