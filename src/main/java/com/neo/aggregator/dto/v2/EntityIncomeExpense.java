package com.neo.aggregator.dto.v2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.neo.aggregator.enums.HealthCategory;
import com.neo.aggregator.enums.IncomeCategory;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class EntityIncomeExpense{

	private Boolean rented;

	private Double avgCreditCardPayment;

	private Double monthlyEmi;

	private Double monthlyIncome;

	private Double yearlyIncome;
	
	private Integer noOfCreditCards;
	
	private Integer noOfLoans;
	
	private Boolean metro;
	
	private IncomeCategory incomeCategory;
	
	private Double asset;
	
	private String companyProfile;

	private String companyWebsite;

	private String mailId;

	private Boolean married;
	
	private Integer childrens;
	
	private Integer dependants;
	
	private HealthCategory healthCategory;
	
}
