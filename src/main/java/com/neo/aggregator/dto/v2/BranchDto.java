package com.neo.aggregator.dto.v2;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BranchDto {

	private String branchCode;
	
	private String branchId;
	
	private String branchName;
	
	private String ifscCode;
	
	private String chqCode;
	
	private String micrCode;
	
}
