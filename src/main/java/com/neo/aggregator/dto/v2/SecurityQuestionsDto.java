package com.neo.aggregator.dto.v2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(exclude = { "securityAnswer" })
@JsonInclude(Include.NON_NULL)
public class SecurityQuestionsDto {

	private String securityQuestion;
	private String securityAnswer;
	
}
