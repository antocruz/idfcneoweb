package com.neo.aggregator.dto.v2;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DecryptKeyDto {

	private String encryptedKey;

	private String icv;

	private String keyIndex;

	private String encryptionMode;

}
