package com.neo.aggregator.dto.v2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(Include.NON_NULL)
public class CreditDto {

	private String cardCategory;
	private String creditLimit;
	private Integer score;
	private String statementDate;
	private String dueDate;
	
}
