package com.neo.aggregator.dto.v2;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.neo.aggregator.enums.CardCategory;
import com.neo.aggregator.enums.CardRegStatus;
import com.neo.aggregator.enums.CardType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class KitInfoDto {
	
	private String kitNo;
	
	private CardType cardType;
	
	private CardCategory cardCategory;
	
	private CardRegStatus cardRegStatus;
	
	private Date expDate;
}
