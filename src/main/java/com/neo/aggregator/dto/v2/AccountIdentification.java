package com.neo.aggregator.dto.v2;

import lombok.Data;

@Data
public class AccountIdentification {

	private String accountNo;

	private String solId;

	private String bankId;

	private Boolean defaultAccount;

	private String ifscCode;

	private Boolean isDebit;

	private String transactionCurrency;

	private String isoTransactionCurrencyCode;

}
