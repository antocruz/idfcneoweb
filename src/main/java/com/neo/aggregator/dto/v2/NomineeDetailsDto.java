package com.neo.aggregator.dto.v2;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(Include.NON_NULL)
public class NomineeDetailsDto {

	private String nomineeName;

	private String nomineeRelationship;

	private Date nomineeDOB;

	private Boolean isMinor = Boolean.FALSE;

	private String guardianName;

}
