package com.neo.aggregator.dto.v2;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(exclude = { "documentNo" })
@JsonInclude(Include.NON_NULL)
public class KYCDto {

	private String kycRefNo;
	
	private String documentType;
	
	private String documentNo;
	
	private String documentPath;

	private Date documentExpiry;

	private String countryOfIssue;

	private String documentIssuedBy;
	
	private Date documentIssuanceDate;
	
	private String firstName;
	
	private String middleName;
	
	private String lastName;

}
