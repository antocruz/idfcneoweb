package com.neo.aggregator.dto;

import lombok.Data;

import java.util.Map;

@Data
public class NotificationRequestDto {

    private String business;

    private String mobileNo;

    private String transactionType;

    private Object[] args;

    private Map<String,String> serverNotifyData;

    private String notificationType;

    private Map<String,String> emailNotifyData;

    @Override
    public String toString() {
        return "NotificationRequestDto [business=" + business + ", mobileNo=xxxxxxxxxxxx" + ", transactionType="
                + transactionType + ", args=" + ", serverNotifyData=" + serverNotifyData
                + ", notificationType=" + notificationType + ", emailNotifyData=" + emailNotifyData + "]";
    }

}

