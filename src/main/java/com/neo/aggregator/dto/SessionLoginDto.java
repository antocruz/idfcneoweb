package com.neo.aggregator.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SessionLoginDto {

	private String bankId;

	private Boolean refreshSession;

	private String sessionId;

	private String responseCode;

	private String description;

	private String responseMessage;
	
	private String oldSessionId;

}
