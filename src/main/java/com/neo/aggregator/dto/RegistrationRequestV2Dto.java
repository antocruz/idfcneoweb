package com.neo.aggregator.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neo.aggregator.dto.v2.AccountsDto;
import com.neo.aggregator.dto.v2.AddressDto;
import com.neo.aggregator.dto.v2.CommunicationDto;
import com.neo.aggregator.dto.v2.CreditDto;
import com.neo.aggregator.dto.v2.EntityIncomeExpense;
import com.neo.aggregator.dto.v2.KYCDto;
import com.neo.aggregator.dto.v2.KitInfoDto;
import com.neo.aggregator.dto.v2.NomineeDetailsDto;
import com.neo.aggregator.dto.v2.PreferenceDto;
import com.neo.aggregator.dto.v2.SecurityQuestionsDto;
import com.neo.aggregator.dto.v2.SpecialDatesDto;
import com.neo.aggregator.enums.EmploymentType;
import com.neo.aggregator.enums.EntityType;
import com.neo.aggregator.enums.GenderType;
import com.neo.aggregator.enums.Industry;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class RegistrationRequestV2Dto {

	private String entityId;

	private EntityType entityCategory;

	private EntityType entityType;

	private String businessId;

	private String kycStatus;

	private String title;

	private String firstName;

	private String middleName;

	private String lastName;

	private GenderType gender;

	private String parentEntityId;

	private String branchCode;

	private String branchId;

	private Boolean addOnCard;

	private Boolean parentLimit;

	private Boolean parentStatement;

	private Boolean isNRICustomer;

	private Boolean isMinor;

	private Boolean isDependant;

	private Boolean isNRECustomer;

	private String maritalStatus;

	private List<KitInfoDto> kitInfo;

	private List<AddressDto> addressInfo;

	private List<CommunicationDto> communicationInfo;

	private List<AccountsDto> accountInfo;

	private List<KYCDto> kycInfo;

	private List<SpecialDatesDto> dateInfo;

	private List<SecurityQuestionsDto> securityQuestionsInfo;

	private List<NomineeDetailsDto> nomineeInfo;

	private EntityIncomeExpense incomeExpenseInfo;

	private FleetAdditionalFieldsDto fleetInfo;

	private ForexAddField forexInfo;

	private CreditDto creditInfo;

	private PreferenceDto preferenceInfo;

	private String businessType;

	private String business;

	private EmploymentType employmentType;

	private Industry employmentIndustry;

	private String channelName;

	private String productId;

	private String nationality;

	private String cifNumber;

	//these are used for RBL Bio Ekyc flow
	private String bioeKycUserId;
	private String bioeKycPassword;
	private String bioeKycBCAgentId;
	private String bioeKycRefPrefix;
	private String bioeKycUrlId;
	private String bioeKycReqType;

	@JsonProperty
	private String eKycRefNum;

	private String occupation;
	private String customerStatus;
	private String politicallyExposed; //yes or no
	private String fatcaDecl; // 1 or 0
	private String grossAnnualIncome;
	private String aadhaarXml;
}
