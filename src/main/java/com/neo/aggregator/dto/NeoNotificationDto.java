package com.neo.aggregator.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class NeoNotificationDto {

    private String entityId;

    private String mobileNo;

    private String description;

    private String transactionDateTime;

    private String prodType = "GENERAL";

    private String txnStatus;

    private String transactionType;

    private String txnOrigin;

    private String utr;

    private String amount;

    private String business;

    private String extTxnId;

    private String accountNum;

}
