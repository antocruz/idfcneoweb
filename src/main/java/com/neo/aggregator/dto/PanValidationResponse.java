package com.neo.aggregator.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

public class PanValidationResponse {

	private PanValidationInnerResponse panData = new PanValidationInnerResponse();

	public PanValidationInnerResponse getValidationResponse() {
		return panData;
	}

	public void setValidationResponse(PanValidationInnerResponse panData) {
		this.panData = panData;
	}

	@NoArgsConstructor
	@AllArgsConstructor
	@Builder
	public static class PanValidationInnerResponse {

		private String panNo;

		private String exists;

		private String lastName;

		private String firstName;

		private String middleName;

		private String title;

		private String issueDate;

		private String nameOnCard;

		private String lastUpdated;

		private Boolean valid;

		public String getPanNo() {
			return panNo;
		}

		public void setPanNo(String panNo) {
			this.panNo = panNo;
		}

		public String getExists() {
			return exists;
		}

		public void setExists(String exists) {
			this.exists = exists;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getMiddleName() {
			return middleName;
		}

		public void setMiddleName(String middleName) {
			this.middleName = middleName;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getIssueDate() {
			return issueDate;
		}

		public void setIssueDate(String issueDate) {
			this.issueDate = issueDate;
		}

		public String getNameOnCard() {
			return nameOnCard;
		}

		public void setNameOnCard(String nameOnCard) {
			this.nameOnCard = nameOnCard;
		}

		public String getLastUpdated() {
			return lastUpdated;
		}

		public void setLastUpdated(String lastUpdated) {
			this.lastUpdated = lastUpdated;
		}

		public Boolean getValid() {
			return valid;
		}

		public void setValid(Boolean valid) {
			this.valid = valid;
		}

	}

}
