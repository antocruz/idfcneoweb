package com.neo.aggregator.dto;

import java.util.List;

import com.neo.aggregator.utility.Iso8583;

import lombok.ToString;

public class AuthorizationRequestDto {

	private String targetUrl;

	private String tenant;

	private boolean isOn;

	private String headers;

	private String processingCode;

	private String posEntryMode;

	private String pointOfServiceConditionCode;

	private String provider;

	private Authentication auth;

	private String offUsIssuerPurchase;

	private Request offUsIssuerPurchaseRequest;

	private String authTimeOut;

	private Iso8583 isoRequest;

	private String transactionCurrencyName;

	private List<AccountIdentification> accountIdentification;

	public String getTransactionCurrencyName() {
		return transactionCurrencyName;
	}

	public void setTransactionCurrencyName(String transactionCurrencyName) {
		this.transactionCurrencyName = transactionCurrencyName;
	}

	public List<AccountIdentification> getAccountIdentification() {
		return accountIdentification;
	}

	public void setAccountIdentification(List<AccountIdentification> accountIdentification) {
		this.accountIdentification = accountIdentification;
	}

	public Iso8583 getIsoRequest() {
		return isoRequest;
	}

	public void setIsoRequest(Iso8583 isoRequest) {
		this.isoRequest = isoRequest;
	}

	public String getTargetUrl() {
		return targetUrl;
	}

	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}

	public String getTenant() {
		return tenant;
	}

	public void setTenant(String tenant) {
		this.tenant = tenant;
	}

	public boolean isOn() {
		return isOn;
	}

	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}

	public String getHeaders() {
		return headers;
	}

	public void setHeaders(String headers) {
		this.headers = headers;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public String getPosEntryMode() {
		return posEntryMode;
	}

	public void setPosEntryMode(String posEntryMode) {
		this.posEntryMode = posEntryMode;
	}

	public String getPointOfServiceConditionCode() {
		return pointOfServiceConditionCode;
	}

	public void setPointOfServiceConditionCode(String pointOfServiceConditionCode) {
		this.pointOfServiceConditionCode = pointOfServiceConditionCode;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public Authentication getAuth() {
		return auth;
	}

	public void setAuth(Authentication auth) {
		this.auth = auth;
	}

	public String getOffUsIssuerPurchase() {
		return offUsIssuerPurchase;
	}

	public void setOffUsIssuerPurchase(String offUsIssuerPurchase) {
		this.offUsIssuerPurchase = offUsIssuerPurchase;
	}

	public Request getOffUsIssuerPurchaseRequest() {
		return offUsIssuerPurchaseRequest;
	}

	public void setOffUsIssuerPurchaseRequest(Request offUsIssuerPurchaseRequest) {
		this.offUsIssuerPurchaseRequest = offUsIssuerPurchaseRequest;
	}

	public String getAuthTimeOut() {
		return authTimeOut;
	}

	public void setAuthTimeOut(String authTimeOut) {
		this.authTimeOut = authTimeOut;
	}

	@ToString
	public static class Request {
		private String TxnRefNo;
		private String Network;
		private String CurCode;
		private String Channel;
		private String TransactionType;
		private String TransactionCurrency;
		private String TransactionAmount;
		private String SettlementCurrency;
		private String SettlementAmount;
		private String BillingCurrency;
		private String BillingAmount;
		private String DccCurrency;
		private String DccAmount;
		private String MerchantID;
		private String ProxyCardNo;
		private String MerchantName;
		private String TerminalID;
		private String MCC;
		private String Amount;
		private String TraceNo;
		private String InstitutionCode;
		private String ProductCode;
		private String RetrievalRefNo;
		private String SenderAccountNo;
		private String SenderAddress;
		private String SenderCity;
		private String SenderContryCode;
		private String SenderName;
		private String SenderStateCode;
		private String SourceFundCode;
		private String SystemTraceAuditNumber;
		private String TrasactionCurrency;
		private String BusinessAppID;
		private String EntityId;
		private String TxnID;
		private String AuthID;
		private String MerchantId;
		private String ReceiverFirstName;
		private String ReceiverMiddleName;
		private String ReceiverLastName;
		private String ReceiverStreetAddress;
		private String ReceiverCity;
		private String ReceiverStateCode;
		private String ReceiverCountry;
		private String ReceiverPostalCode;
		private String ReceiverPhoneNo;
		private String ReceiverDateOfBirth;
		private String ReceiverAccountNo;
		private String ReceiverIDType;
		private String ReceiverIDNo;
		private String ReceiverIDCountryCode;
		private String ReceiverIDExpiryDate;
		private String ReceiverNationality;
		private String ReceiverCountryOfBirth;
		private String SenderFirstName;
		private String SenderMiddleName;
		private String SenderLastName;
		private String SenderStreetAddress;
		private String SenderCountry;
		private String SenderPostalCode;
		private String SenderPhoneNo;
		private String SenderDateOfBirth;
		private String SenderIDType;
		private String SenderIDNo;
		private String SenderIDCountryCode;
		private String SenderIDExpiryDate;
		private String SenderNationality;
		private String SenderCountryOfBirth;
		private String UniqueTxnRef;
		private String AdditionalMessage;
		private String ParticipationID;
		private String TxnPurpose;
		private String UniqueRefNo;
		private String SenderID;
		private String AdditionalInfo;
		private String TranDate;
		private String TxnParam;

		public String getTxnRefNo() {
			return TxnRefNo;
		}

		public void setTxnRefNo(String txnRefNo) {
			TxnRefNo = txnRefNo;
		}

		public String getNetwork() {
			return Network;
		}

		public void setNetwork(String network) {
			Network = network;
		}

		public String getCurCode() {
			return CurCode;
		}

		public void setCurCode(String curCode) {
			CurCode = curCode;
		}

		public String getChannel() {
			return Channel;
		}

		public void setChannel(String channel) {
			Channel = channel;
		}

		public String getTransactionType() {
			return TransactionType;
		}

		public void setTransactionType(String transactionType) {
			TransactionType = transactionType;
		}

		public String getTransactionCurrency() {
			return TransactionCurrency;
		}

		public void setTransactionCurrency(String transactionCurrency) {
			TransactionCurrency = transactionCurrency;
		}

		public String getTransactionAmount() {
			return TransactionAmount;
		}

		public void setTransactionAmount(String transactionAmount) {
			TransactionAmount = transactionAmount;
		}

		public String getSettlementCurrency() {
			return SettlementCurrency;
		}

		public void setSettlementCurrency(String settlementCurrency) {
			SettlementCurrency = settlementCurrency;
		}

		public String getSettlementAmount() {
			return SettlementAmount;
		}

		public void setSettlementAmount(String settlementAmount) {
			SettlementAmount = settlementAmount;
		}

		public String getBillingCurrency() {
			return BillingCurrency;
		}

		public void setBillingCurrency(String billingCurrency) {
			BillingCurrency = billingCurrency;
		}

		public String getBillingAmount() {
			return BillingAmount;
		}

		public void setBillingAmount(String billingAmount) {
			BillingAmount = billingAmount;
		}

		public String getDccCurrency() {
			return DccCurrency;
		}

		public void setDccCurrency(String dccCurrency) {
			DccCurrency = dccCurrency;
		}

		public String getDccAmount() {
			return DccAmount;
		}

		public void setDccAmount(String dccAmount) {
			DccAmount = dccAmount;
		}

		public String getMerchantID() {
			return MerchantID;
		}

		public void setMerchantID(String merchantID) {
			MerchantID = merchantID;
		}

		public String getProxyCardNo() {
			return ProxyCardNo;
		}

		public void setProxyCardNo(String proxyCardNo) {
			ProxyCardNo = proxyCardNo;
		}

		public String getMerchantName() {
			return MerchantName;
		}

		public void setMerchantName(String merchantName) {
			MerchantName = merchantName;
		}

		public String getTerminalID() {
			return TerminalID;
		}

		public void setTerminalID(String terminalID) {
			TerminalID = terminalID;
		}

		public String getMCC() {
			return MCC;
		}

		public void setMCC(String mCC) {
			MCC = mCC;
		}

		public String getAmount() {
			return Amount;
		}

		public void setAmount(String amount) {
			Amount = amount;
		}

		public String getTraceNo() {
			return TraceNo;
		}

		public void setTraceNo(String traceNo) {
			TraceNo = traceNo;
		}

		public String getInstitutionCode() {
			return InstitutionCode;
		}

		public void setInstitutionCode(String institutionCode) {
			InstitutionCode = institutionCode;
		}

		public String getProductCode() {
			return ProductCode;
		}

		public void setProductCode(String productCode) {
			ProductCode = productCode;
		}

		public String getRetrievalRefNo() {
			return RetrievalRefNo;
		}

		public void setRetrievalRefNo(String retrievalRefNo) {
			RetrievalRefNo = retrievalRefNo;
		}

		public String getSenderAccountNo() {
			return SenderAccountNo;
		}

		public void setSenderAccountNo(String senderAccountNo) {
			SenderAccountNo = senderAccountNo;
		}

		public String getSenderAddress() {
			return SenderAddress;
		}

		public void setSenderAddress(String senderAddress) {
			SenderAddress = senderAddress;
		}

		public String getSenderCity() {
			return SenderCity;
		}

		public void setSenderCity(String senderCity) {
			SenderCity = senderCity;
		}

		public String getSenderContryCode() {
			return SenderContryCode;
		}

		public void setSenderContryCode(String senderContryCode) {
			SenderContryCode = senderContryCode;
		}

		public String getSenderName() {
			return SenderName;
		}

		public void setSenderName(String senderName) {
			SenderName = senderName;
		}

		public String getSenderStateCode() {
			return SenderStateCode;
		}

		public void setSenderStateCode(String senderStateCode) {
			SenderStateCode = senderStateCode;
		}

		public String getSourceFundCode() {
			return SourceFundCode;
		}

		public void setSourceFundCode(String sourceFundCode) {
			SourceFundCode = sourceFundCode;
		}

		public String getSystemTraceAuditNumber() {
			return SystemTraceAuditNumber;
		}

		public void setSystemTraceAuditNumber(String systemTraceAuditNumber) {
			SystemTraceAuditNumber = systemTraceAuditNumber;
		}

		public String getTrasactionCurrency() {
			return TrasactionCurrency;
		}

		public void setTrasactionCurrency(String trasactionCurrency) {
			TrasactionCurrency = trasactionCurrency;
		}

		public String getBusinessAppID() {
			return BusinessAppID;
		}

		public void setBusinessAppID(String businessAppID) {
			BusinessAppID = businessAppID;
		}

		public String getEntityId() {
			return EntityId;
		}

		public void setEntityId(String entityId) {
			EntityId = entityId;
		}

		public String getTxnID() {
			return TxnID;
		}

		public void setTxnID(String txnID) {
			TxnID = txnID;
		}

		public String getAuthID() {
			return AuthID;
		}

		public void setAuthID(String authID) {
			AuthID = authID;
		}

		public String getMerchantId() {
			return MerchantId;
		}

		public void setMerchantId(String merchantId) {
			MerchantId = merchantId;
		}

		public String getReceiverFirstName() {
			return ReceiverFirstName;
		}

		public void setReceiverFirstName(String receiverFirstName) {
			ReceiverFirstName = receiverFirstName;
		}

		public String getReceiverMiddleName() {
			return ReceiverMiddleName;
		}

		public void setReceiverMiddleName(String receiverMiddleName) {
			ReceiverMiddleName = receiverMiddleName;
		}

		public String getReceiverLastName() {
			return ReceiverLastName;
		}

		public void setReceiverLastName(String receiverLastName) {
			ReceiverLastName = receiverLastName;
		}

		public String getReceiverStreetAddress() {
			return ReceiverStreetAddress;
		}

		public void setReceiverStreetAddress(String receiverStreetAddress) {
			ReceiverStreetAddress = receiverStreetAddress;
		}

		public String getReceiverCity() {
			return ReceiverCity;
		}

		public void setReceiverCity(String receiverCity) {
			ReceiverCity = receiverCity;
		}

		public String getReceiverStateCode() {
			return ReceiverStateCode;
		}

		public void setReceiverStateCode(String receiverStateCode) {
			ReceiverStateCode = receiverStateCode;
		}

		public String getReceiverCountry() {
			return ReceiverCountry;
		}

		public void setReceiverCountry(String receiverCountry) {
			ReceiverCountry = receiverCountry;
		}

		public String getReceiverPostalCode() {
			return ReceiverPostalCode;
		}

		public void setReceiverPostalCode(String receiverPostalCode) {
			ReceiverPostalCode = receiverPostalCode;
		}

		public String getReceiverPhoneNo() {
			return ReceiverPhoneNo;
		}

		public void setReceiverPhoneNo(String receiverPhoneNo) {
			ReceiverPhoneNo = receiverPhoneNo;
		}

		public String getReceiverDateOfBirth() {
			return ReceiverDateOfBirth;
		}

		public void setReceiverDateOfBirth(String receiverDateOfBirth) {
			ReceiverDateOfBirth = receiverDateOfBirth;
		}

		public String getReceiverAccountNo() {
			return ReceiverAccountNo;
		}

		public void setReceiverAccountNo(String receiverAccountNo) {
			ReceiverAccountNo = receiverAccountNo;
		}

		public String getReceiverIDType() {
			return ReceiverIDType;
		}

		public void setReceiverIDType(String receiverIDType) {
			ReceiverIDType = receiverIDType;
		}

		public String getReceiverIDNo() {
			return ReceiverIDNo;
		}

		public void setReceiverIDNo(String receiverIDNo) {
			ReceiverIDNo = receiverIDNo;
		}

		public String getReceiverIDCountryCode() {
			return ReceiverIDCountryCode;
		}

		public void setReceiverIDCountryCode(String receiverIDCountryCode) {
			ReceiverIDCountryCode = receiverIDCountryCode;
		}

		public String getReceiverIDExpiryDate() {
			return ReceiverIDExpiryDate;
		}

		public void setReceiverIDExpiryDate(String receiverIDExpiryDate) {
			ReceiverIDExpiryDate = receiverIDExpiryDate;
		}

		public String getReceiverNationality() {
			return ReceiverNationality;
		}

		public void setReceiverNationality(String receiverNationality) {
			ReceiverNationality = receiverNationality;
		}

		public String getReceiverCountryOfBirth() {
			return ReceiverCountryOfBirth;
		}

		public void setReceiverCountryOfBirth(String receiverCountryOfBirth) {
			ReceiverCountryOfBirth = receiverCountryOfBirth;
		}

		public String getSenderFirstName() {
			return SenderFirstName;
		}

		public void setSenderFirstName(String senderFirstName) {
			SenderFirstName = senderFirstName;
		}

		public String getSenderMiddleName() {
			return SenderMiddleName;
		}

		public void setSenderMiddleName(String senderMiddleName) {
			SenderMiddleName = senderMiddleName;
		}

		public String getSenderLastName() {
			return SenderLastName;
		}

		public void setSenderLastName(String senderLastName) {
			SenderLastName = senderLastName;
		}

		public String getSenderStreetAddress() {
			return SenderStreetAddress;
		}

		public void setSenderStreetAddress(String senderStreetAddress) {
			SenderStreetAddress = senderStreetAddress;
		}

		public String getSenderCountry() {
			return SenderCountry;
		}

		public void setSenderCountry(String senderCountry) {
			SenderCountry = senderCountry;
		}

		public String getSenderPostalCode() {
			return SenderPostalCode;
		}

		public void setSenderPostalCode(String senderPostalCode) {
			SenderPostalCode = senderPostalCode;
		}

		public String getSenderPhoneNo() {
			return SenderPhoneNo;
		}

		public void setSenderPhoneNo(String senderPhoneNo) {
			SenderPhoneNo = senderPhoneNo;
		}

		public String getSenderDateOfBirth() {
			return SenderDateOfBirth;
		}

		public void setSenderDateOfBirth(String senderDateOfBirth) {
			SenderDateOfBirth = senderDateOfBirth;
		}

		public String getSenderIDType() {
			return SenderIDType;
		}

		public void setSenderIDType(String senderIDType) {
			SenderIDType = senderIDType;
		}

		public String getSenderIDNo() {
			return SenderIDNo;
		}

		public void setSenderIDNo(String senderIDNo) {
			SenderIDNo = senderIDNo;
		}

		public String getSenderIDCountryCode() {
			return SenderIDCountryCode;
		}

		public void setSenderIDCountryCode(String senderIDCountryCode) {
			SenderIDCountryCode = senderIDCountryCode;
		}

		public String getSenderIDExpiryDate() {
			return SenderIDExpiryDate;
		}

		public void setSenderIDExpiryDate(String senderIDExpiryDate) {
			SenderIDExpiryDate = senderIDExpiryDate;
		}

		public String getSenderNationality() {
			return SenderNationality;
		}

		public void setSenderNationality(String senderNationality) {
			SenderNationality = senderNationality;
		}

		public String getSenderCountryOfBirth() {
			return SenderCountryOfBirth;
		}

		public void setSenderCountryOfBirth(String senderCountryOfBirth) {
			SenderCountryOfBirth = senderCountryOfBirth;
		}

		public String getUniqueTxnRef() {
			return UniqueTxnRef;
		}

		public void setUniqueTxnRef(String uniqueTxnRef) {
			UniqueTxnRef = uniqueTxnRef;
		}

		public String getAdditionalMessage() {
			return AdditionalMessage;
		}

		public void setAdditionalMessage(String additionalMessage) {
			AdditionalMessage = additionalMessage;
		}

		public String getParticipationID() {
			return ParticipationID;
		}

		public void setParticipationID(String participationID) {
			ParticipationID = participationID;
		}

		public String getTxnPurpose() {
			return TxnPurpose;
		}

		public void setTxnPurpose(String txnPurpose) {
			TxnPurpose = txnPurpose;
		}

		public String getUniqueRefNo() {
			return UniqueRefNo;
		}

		public void setUniqueRefNo(String uniqueRefNo) {
			UniqueRefNo = uniqueRefNo;
		}

		public String getSenderID() {
			return SenderID;
		}

		public void setSenderID(String senderID) {
			SenderID = senderID;
		}

		public String getAdditionalInfo() {
			return AdditionalInfo;
		}

		public void setAdditionalInfo(String additionalInfo) {
			AdditionalInfo = additionalInfo;
		}

		public String getTranDate() {
			return TranDate;
		}

		public void setTranDate(String tranDate) {
			TranDate = tranDate;
		}

		public String getTxnParam() {
			return TxnParam;
		}

		public void setTxnParam(String txnParam) {
			TxnParam = txnParam;
		}

	}
}
