package com.neo.aggregator.dto;

public class ReversalResponseDto {

	private Response response = new Response();

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public static class Response {

		private String status = "true";

		private String description = "test";

		private String txnRefNo = "21345324378645736253q";

		private String settlementDate = "20151001";

		private String TxnID;

		private String AuthID;

		private String fundingSource;

		private String balance;

		public String getBalance() {
			return balance;
		}

		public void setBalance(String balance) {
			this.balance = balance;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getTxnRefNo() {
			return txnRefNo;
		}

		public void setTxnRefNo(String txnRefNo) {
			this.txnRefNo = txnRefNo;
		}

		public String getSettlementDate() {
			return settlementDate;
		}

		public void setSettlementDate(String settlementDate) {
			this.settlementDate = settlementDate;
		}

		public String getTxnID() {
			return TxnID;
		}

		public void setTxnID(String txnID) {
			TxnID = txnID;
		}

		public String getAuthID() {
			return AuthID;
		}

		public void setAuthID(String authID) {
			AuthID = authID;
		}

		public String getFundingSource() {
			return fundingSource;
		}

		public void setFundingSource(String fundingSource) {
			this.fundingSource = fundingSource;
		}

	}
}
