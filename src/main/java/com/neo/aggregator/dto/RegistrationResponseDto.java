package com.neo.aggregator.dto;

import lombok.Data;

@Data
public class RegistrationResponseDto {
	
	private String customerId;
	
	private String description;
	
	private String status;
	
	private String entityId;

	private String accountId;
	
	private String kitNo;

}
