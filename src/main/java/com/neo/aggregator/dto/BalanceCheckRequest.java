package com.neo.aggregator.dto;

import com.neo.aggregator.enums.AccountType;

import lombok.Data;

@Data
public class BalanceCheckRequest {

	private String entityId;

	private String accountNo;

	private AccountType accountType;

}
