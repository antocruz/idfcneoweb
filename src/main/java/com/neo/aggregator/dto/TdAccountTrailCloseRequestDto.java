package com.neo.aggregator.dto;

import lombok.Data;

@Data
public class TdAccountTrailCloseRequestDto {

	private String entityId;
	
	private String accountId;
	
	private String rePayAccount;
	
	
	
}
