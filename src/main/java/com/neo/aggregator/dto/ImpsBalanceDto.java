package com.neo.aggregator.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ImpsBalanceDto {
	
	@NotNull(message="Account number should not be null or empty")
	@NotEmpty(message="Account number should not be null or empty")
	private String accountNumber;

	private String appId;
	private String customerId;
	private String version;
	private String branchId;
	private String userId;
	
	@NotNull(message="Bank should not be null or empty")
	@NotEmpty(message="Bank should not be null or empty")
	private String bank;
	private String tenant;
	private String corporate;
	private String accountType;
}