package com.neo.aggregator.dto;

import lombok.Data;

import java.util.Date;

@Data
public class BeneficiaryManagementResDto {

    private Boolean status;

    private String description;

    private Date activationTime;

    private String beneficiaryId;

}
