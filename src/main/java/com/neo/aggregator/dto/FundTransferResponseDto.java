package com.neo.aggregator.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FundTransferResponseDto {

	private String description;
	private String externalTransactionId;
	private String status;
	private String bankReferenceNo;

}
