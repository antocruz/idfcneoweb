package com.neo.aggregator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class CustomerDedupCheckResponseDto {

    @JsonProperty(value = "rematchcount")
    private String rematchcount;
    @JsonProperty(value = "requestId")
    private String requestId;
    @JsonProperty(value = "message")
    private String message;
    @JsonProperty(value = "customermatchcount")
    private String customermatchcount;
    @JsonProperty(value = "status")
    private String status;


}

