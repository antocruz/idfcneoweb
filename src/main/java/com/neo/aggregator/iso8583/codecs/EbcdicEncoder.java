package com.neo.aggregator.iso8583.codecs;

import java.io.UnsupportedEncodingException;

import com.neo.aggregator.iso8583.CustomBinaryField;

public class EbcdicEncoder implements CustomBinaryField<String> {

	@Override
	public String decodeBinaryField(byte[] value, int offset, int length) {
		try {
			return new String(value, offset, length, "Cp1047");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public byte[] encodeBinaryField(String value) {
		try {
			return value.getBytes("Cp1047");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String encodeField(String value) {
		return value;
	}

	@Override
	public String decodeField(String value) {
		return value;
	}

}
