package com.neo.aggregator.bank.idfc.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

public class MDMSearchByFieldsRequestDto {
	
	@JsonProperty(value = "searchByFieldsSPReq")
    public SearchByFieldsSPReq searchByFieldsSPReq;
	
	@Builder
    @Getter
    @Setter
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
	public static class SearchByFieldsSPReq{
		@JsonProperty(value = "msgHdr")
	    public MessageHeader messageHeader;

	    @JsonProperty(value = "msgBdy")
	    public MessageBody messageBody;
	}


    @Builder
    @Getter
    @Setter
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class MessageHeader {

        @JsonProperty(value = "frm")
        private From frm;

        @JsonProperty(value = "hdrFlds")
        private HeaderFields headerFields;
    }

    @Builder
    @Getter
    @Setter
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class From {
        @JsonProperty(value = "id")
        private String id;
    }

    @Builder
    @Getter
    @Setter
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class HeaderFields {
        @JsonProperty(value = "cnvId")
        private String cnvId;

        @JsonProperty(value = "msgId")
        private String msgId;

        @JsonProperty(value = "timestamp")
        private String timestamp;

    }

    @Builder
    @Getter
    @Setter
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class MessageBody {

        @JsonProperty(value = "ucic")
        private String ucic;

        @JsonProperty(value = "acctNo")
        private String acctNo;

       
        @JsonProperty(value = "panNb")
        private String panNb;

        @JsonProperty(value = "aadharNo")
        private String aadharNo;

        @JsonProperty(value = "tan")
        private String tan;

        @JsonProperty(value = "email")
        private String email;

        @JsonProperty(value = "adharrfno")
        private String adharrfno;
        
        @JsonProperty(value = "mobNb")
        private String mobNb;

    }

	
}
