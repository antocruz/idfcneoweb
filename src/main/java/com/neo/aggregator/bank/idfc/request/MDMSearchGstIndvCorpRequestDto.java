package com.neo.aggregator.bank.idfc.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

public class MDMSearchGstIndvCorpRequestDto {
	
	 @JsonProperty(value = "searchGSTIndvCorpReq")
	 public SearchGSTIndvCorpReq searchGSTIndvCorpReq;
		
		@Builder
	    @Getter
	    @Setter
	    @Data
	    @JsonInclude(JsonInclude.Include.NON_NULL)
		public static class SearchGSTIndvCorpReq{
			@JsonProperty(value = "msgHdr")
		    public MessageHeader messageHeader;

		    @JsonProperty(value = "msgBdy")
		    public MessageBody messageBody;
		}


	    @Builder
	    @Getter
	    @Setter
	    @Data
	    @JsonInclude(JsonInclude.Include.NON_NULL)
	    public static class MessageHeader {

	        @JsonProperty(value = "frm")
	        private From frm;

	        @JsonProperty(value = "hdrFlds")
	        private HeaderFields headerFields;
	    }

	    @Builder
	    @Getter
	    @Setter
	    @Data
	    @JsonInclude(JsonInclude.Include.NON_NULL)
	    public static class From {
	        @JsonProperty(value = "id")
	        private String id;
	    }

	    @Builder
	    @Getter
	    @Setter
	    @Data
	    @JsonInclude(JsonInclude.Include.NON_NULL)
	    public static class HeaderFields {
	        @JsonProperty(value = "cnvId")
	        private String cnvId;

	        @JsonProperty(value = "msgId")
	        private String msgId;

	        @JsonProperty(value = "timestamp")
	        private String timestamp;

	    }

	    @Builder
	    @Getter
	    @Setter
	    @Data
	    @JsonInclude(JsonInclude.Include.NON_NULL)
	    public static class MessageBody {
	    	
	    	@JsonProperty(value = "ucic")
	        private String ucic;
	    	
	    	@JsonProperty(value = "gstNumber")
	        private String gstNumber;
	    	
	    	@JsonProperty(value = "createdDate")
	        private String createdDate;
	    	
	    	@JsonProperty(value = "modifiedDate")
	        private String modifiedDate;
	    		    
	    }

}
