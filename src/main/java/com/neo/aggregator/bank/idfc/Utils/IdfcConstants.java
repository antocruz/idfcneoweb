package com.neo.aggregator.bank.idfc.Utils;

public final class IdfcConstants {

	public static final String BASE_URL;
	public static final String IDFC_FUND_TRANSFER;
	
	static {
		BASE_URL = "https://esbmuat.idfcfirstbank.com:31297/";
		IDFC_FUND_TRANSFER = "initiateAuthGenericFundTransferAPIReq";
	}
}
