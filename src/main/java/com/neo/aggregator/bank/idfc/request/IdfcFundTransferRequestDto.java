package com.neo.aggregator.bank.idfc.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.neo.aggregator.utility.MandateCheck;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Data
public class IdfcFundTransferRequestDto {

    @JsonProperty(value = "msgHdr")
    public MessageHeader messageHeader;

    @JsonProperty(value = "msgBdy")
    public MessageBody messageBody;


    @Builder
    @Getter
    @Setter
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class MessageHeader {

        @JsonProperty(value = "frm")
        private From frm;

        @JsonProperty(value = "hdrFlds")
        private HeaderFields headerFields;
    }

    @Builder
    @Getter
    @Setter
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class From {
    	
    	@MandateCheck
        @JsonProperty(value = "id")
        private String id;
    }

    @Builder
    @Getter
    @Setter
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class HeaderFields {
    	@MandateCheck
        @JsonProperty(value = "cnvId")
        private String cnvId;
    	
    	@MandateCheck
        @JsonProperty(value = "msgId")
        private String msgId;

    	@MandateCheck
        @JsonProperty(value = "timestamp")
        private String timestamp;

        @JsonProperty(value = "hdrProp")
        private HDProp hdProp;

    }

    @Builder
    @Getter
    @Setter
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class HDProp {
        @JsonProperty(value = "nm")
        private Nm nm;
    }

    @Builder
    @Getter
    @Setter
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Nm {
        @JsonProperty(value = "BrchNum")
        private String branchNm;

        @JsonProperty(value = "TellerNum")
        private String tellerNum;
    }

    @Builder
    @Getter
    @Setter
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class MessageBody {

        @JsonProperty(value = "txnId")
        @MandateCheck
        private String txnId;

        @MandateCheck
        @JsonProperty(value = "dbtrAcctId")
        private String dbtrAcctId;

        @MandateCheck
        @JsonProperty(value = "cdtrAcctId")
        private String cdtrAcctId;
       
        @JsonProperty(value = "dbtrNm")
        private String dbtrNm;

        @MandateCheck
        @JsonProperty(value = "amt")
        private String amt;

        @MandateCheck
        @JsonProperty(value = "ccy")
        private String ccy;

        @MandateCheck
        @JsonProperty(value = "txnTp")
        private String txnTp;

        @JsonProperty(value = "pmtDesc")
        private String pmtDesc;

        @JsonProperty(value = "finInstnId")
        private String finInstnId;

        @JsonProperty(value = "bnfcryNm")
        private String bnfcryNm;

        @JsonProperty(value = "bnfcryAddr")
        private String bnfcryAddr;

        @JsonProperty(value = "emlId")
        private String emlId;

        @JsonProperty(value = "mobNb")
        private String mobNb;

        @JsonProperty(value = "senderInfo")
        private String senderInfo;

        @JsonProperty(value = "receiverInfo")
        private String receiverInfo;
    }

}
