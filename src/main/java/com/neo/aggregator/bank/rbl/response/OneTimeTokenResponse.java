package com.neo.aggregator.bank.rbl.response;


import java.net.URL;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@JsonRootName(value = "ekyconetimetokenres")
public class OneTimeTokenResponse {

	@JsonProperty(value = "status")
	private String status;

	@JsonProperty(value = "url")
	private URL url;

}
