package com.neo.aggregator.bank.rbl.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Data
public class ReconStatusResponse {

	@JsonProperty(value = "getRecord")
	private GetRecord getRecord;

	@Data
	public static class GetRecord {

		@JsonProperty(value = "Header")
		private Header header;
	}

	@Data
	public static class Header {
		@JsonProperty(value = "TranID")
		private String TranID;
		@JsonProperty(value = "Status")
		private String status;
		@JsonProperty(value = "Status_Code")
		private String statusCode;
		@JsonProperty(value = "Status_Desc")
		private String statusDesc;
	}
}
