package com.neo.aggregator.bank.rbl.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.dto.ecollect.rbl.VARequestHeader;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName(value = "get_Single_Payment_Status_Corp_Res")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentStatusResponse {

    @JsonProperty(value = "Header")
    private VARequestHeader header;

    @JsonProperty(value = "Body")
    private StatusReqBody body;

    @JsonProperty(value = "Signature")
    private StatusReqSignature sign;

    @Getter @Setter
    public static class StatusReqBody {
        @JsonProperty(value="ORGTRANSACTIONID")
        private String orgTxnId;
        @JsonProperty(value="AMOUNT")
        private String amount;
        @JsonProperty(value = "REFNO")
        private String refNo;
        @JsonProperty(value = "UTRNO")
        private String utr;
        @JsonProperty(value="PONUM")
        private String poNum;
        @JsonProperty(value = "RRN")
        private String rrn;
        @JsonProperty(value = "BEN_ACCT_NO")
        private String beneAcntNo;
        @JsonProperty(value = "BENIFSC")
        private String beneIfsc;
        @JsonProperty(value="BEN_CONF_RECEIVED")
        private String beneConfReceived;
        @JsonProperty(value = "TXNSTATUS")
        private String txnStatus;
        @JsonProperty(value="STATUSDESC")
        private String statusDesc;
        @JsonProperty(value = "TXNTIME")
        private String txnTime;
        @JsonProperty(value="PAYMENTSTATUS")
        private String paymentStatus;
    }

    @Getter @Setter
    public static class StatusReqSignature {
        @JsonProperty(value = "Signature")
        private String signature;
    }
    
}