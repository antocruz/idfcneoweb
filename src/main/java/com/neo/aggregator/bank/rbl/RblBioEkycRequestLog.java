package com.neo.aggregator.bank.rbl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.neo.aggregator.model.AbstractEntity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "rbl_bioekyc_request_log")
@Getter
@Setter
public class RblBioEkycRequestLog extends AbstractEntity {
    @Column
    private String cpuniquerefno;
    @Column
    private String rbluniquerefno;
    @Column
    private String bioekycrefno;
    @Column
    private int bioekycattempts;
    @Column
    private String ekycstatus = "0";
}
