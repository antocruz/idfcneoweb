package com.neo.aggregator.bank.rbl.request;

import javax.xml.bind.annotation.*;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "", propOrder = { "header", "refno", "requestdate", "partnerrefno", "gender", "mothermaidenname",
		"nationality", "occupation", "emailaddress", "laddress1", "laddress2", "lcity", "lstate", "lpincode",
		"lcountry", "CustomerStatus", "CustomerType", "SourceIncomeType", "AnnualIncome", "PoliticallyExposedPerson",
		"CardAlias", "product", "fatcadecl", "mobilenumber", "Reqtype", "Kyctype", "Documenttype", "Documentnumber",
		"Reserved1", "Reserved2", "Reserved3", "Reserved4" })
@XmlRootElement(name = "CustomerKYCOnboardingreq")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@Data
public class CustomerKYCOnboardingRequest {

	@XmlElement(name = "header", required = true)
	private RequestHeader header;
	@XmlElement(name = "refno")
	private String refno;
	@XmlElement(name = "requestdate")
	private String requestdate;
	@XmlElement(name = "partnerrefno")
	private String partnerrefno;
	@XmlElement(name = "gender")
	private String gender;
	@XmlElement(name = "mothermaidenname")
	private String mothermaidenname;
	@XmlElement(name = "nationality")
	private String nationality;
	@XmlElement(name = "occupation")
	private String occupation;
	@XmlElement(name = "emailaddress")
	private String emailaddress;
	@XmlElement(name = "laddress1")
	private String laddress1;
	@XmlElement(name = "laddress2")
	private String laddress2;
	@XmlElement(name = "lcity")
	private String lcity;
	@XmlElement(name = "lstate")
	private String lstate;
	@XmlElement(name = "lpincode")
	private String lpincode;
	@XmlElement(name = "lcountry")
	private String lcountry;
	@XmlElement(name = "CustomerStatus")
	private String CustomerStatus;
	@XmlElement(name = "CustomerType")
	private String CustomerType;
	@XmlElement(name = "SourceIncomeType")
	private String SourceIncomeType;
	@XmlElement(name = "AnnualIncome")
	private String AnnualIncome;
	@XmlElement(name = "PoliticallyExposedPerson")
	private String PoliticallyExposedPerson;
	@XmlElement(name = "CardAlias")
	private String CardAlias;
	@XmlElement
	private String product;
	@XmlElement
	private String fatcadecl;
	@XmlElement
	private String mobilenumber;
	@XmlElement(name = "Reqtype")
	private String Reqtype;
	@XmlElement(name = "Kyctype")
	private String Kyctype;
	@XmlElement(name = "Documenttype")
	private String Documenttype;
	@XmlElement(name = "Documentnumber")
	private String Documentnumber;
	@XmlElement(name = "Reserved1")
	private String Reserved1;
	@XmlElement(name = "Reserved2")
	private String Reserved2;
	@XmlElement(name = "Reserved3")
	private String Reserved3;
	@XmlElement(name = "Reserved4")
	private String Reserved4;

	@Builder
	@AllArgsConstructor
	@NoArgsConstructor
	public static class RequestHeader {
		@XmlElement
		private String username;
		@XmlElement
		private String password;
	}
}
