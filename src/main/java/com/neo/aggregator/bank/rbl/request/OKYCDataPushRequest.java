package com.neo.aggregator.bank.rbl.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName(value = "okycrequest")
@JsonPropertyOrder({"header",
        "kycType", "name", "address", "photo", "dob", "gender", "verifiedMobileNumber", "verifiedEmailId", "hashedEmail",
        "hashedMobileNumber", "faceAuthStatus", "faceAuthScore", "faceAuthImage",
        "transactionTimestamp", "kycTimestamp", "rawXml", "version", "transactionReferenceNumber",
        "nameMatchScore", "dobMatchScore", "addressMatchScore", "Latitude", "Longitude", "clientid", "okycrequestid"
})
public class OKYCDataPushRequest {

    @JsonProperty(value = "header")
    private RequestHeader header;

    @JsonProperty(value = "kycType")
    private String kycType;
    @JsonProperty(value = "name")
    private String name;
    @JsonProperty(value = "address")
    private String address;
    @JsonProperty(value = "photo")
    private String photo;
    @JsonProperty(value = "dob")
    private String dateOfBirth;
    @JsonProperty(value = "gender")
    private String gender;
    @JsonProperty(value = "verifiedMobileNumber")
    private String verifiedMobileNumber;
    @JsonProperty(value = "verifiedEmailId")
    private String verifiedEmailId;
    @JsonProperty(value = "hashedEmail")
    private String hashedEmail;
    @JsonProperty(value = "hashedMobileNumber")
    private String hashedMobileNumber;
    @JsonProperty(value = "faceAuthStatus")
    private String faceAuthStatus;
    @JsonProperty(value = "faceAuthScore")
    private String faceAuthScore;
    @JsonProperty(value = "faceAuthImage")
    private String faceAuthImage;
    @JsonProperty(value = "transactionTimestamp")
    private String transactionTimestamp;
    @JsonProperty(value = "kycTimestamp")
    private String kycTimestamp;
    @JsonProperty(value = "rawXml")
    private String rawXml;
    @JsonProperty(value = "version")
    private String version;
    @JsonProperty(value = "transactionReferenceNumber")
    private String transactionReferenceNumber;
    @JsonProperty(value = "nameMatchScore")
    private String nameMatchScore;
    @JsonProperty(value = "dobMatchScore")
    private String dobMatchScore;
    @JsonProperty(value = "addressMatchScore")
    private String addressMatchScore;
    @JsonProperty(value = "Latitude")
    private String latitude;
    @JsonProperty(value = "Longitude")
    private String longitude;
    @JsonProperty(value = "clientid")
    private String clientId;
    @JsonProperty(value = "okycrequestid")
    private String okycRequestId;

    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class RequestHeader {
        @JsonProperty
        private String username;

        @JsonProperty
        private String password;
    }
}
