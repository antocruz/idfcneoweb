package com.neo.aggregator.bank.rbl.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.neo.aggregator.bank.rbl.RblBioEkycRequestLog;

@Repository
public interface RblBioEkycRequestLogDao extends CrudRepository<RblBioEkycRequestLog, Long> {

    RblBioEkycRequestLog findByCpuniquerefno(String cpuniquerefno);
}
