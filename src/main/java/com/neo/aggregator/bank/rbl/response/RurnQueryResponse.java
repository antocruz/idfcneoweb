package com.neo.aggregator.bank.rbl.response;

import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.Data;

@Data
@JsonRootName(value="ekycrequeryrurnres")
public class RurnQueryResponse {
    private String status;
    private String rbluniquerefno;
}
