package com.neo.aggregator.bank.rbl.request;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@JsonRootName(value = "panInquiry")
@Builder
@Getter
@Setter
@Data
public class PANInquiryRequest {

	@JsonProperty(value = "Header")
	private PANInquiryRequestHeader Header;

	@JsonProperty(value = "Body")
	private PANInquiryRequestBody Body;

	@JsonProperty(value = "Signature")
	private PANInquiryRequestSignature Signature;

	@NoArgsConstructor
	@AllArgsConstructor
	@Builder
	@Getter
	@Setter
	@Data
	@JsonInclude(Include.NON_NULL)
	public static class PANInquiryRequestHeader {

		@JsonProperty(value = "TranID")
		private String TranID;

		@JsonProperty(value = "Corp_ID")
		private String CorpID;

		@JsonProperty(value = "Maker_ID")
		private String MakerID;

		@JsonProperty(value = "Checker_ID")
		private String CheckerID;

		@JsonProperty(value = "Approver_ID")
		private String ApproverID;

		@JsonProperty(value = "Nsdl_UserID")
		private String NSDLUserID;

	}

	@NoArgsConstructor
	@AllArgsConstructor
	@Builder
	@Getter
	@Setter
	@Data
	public static class PANInquiryRequestBody {

		@JsonProperty(value = "panNumbers")
		private List<Map<String, String>> panNumbers;
	}

	@NoArgsConstructor
	@AllArgsConstructor
	@Builder
	@Getter
	@Setter
	@Data
	public static class PANInquiryRequestSignature {

		@JsonProperty(value = "Signature")
		private String Signature;
	}
}