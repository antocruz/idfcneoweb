package com.neo.aggregator.bank.rbl.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.rbl.request.PANInquiryRequest.PANInquiryRequestHeader;
import com.neo.aggregator.bank.rbl.request.PANInquiryRequest.PANInquiryRequestSignature;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@JsonRootName(value = "panInquiryResponse")
@Builder
@Data
public class PANInquiryResponse {

		@JsonProperty(value = "Header")
		private PANInquiryRequestHeader header;

		@JsonProperty(value = "Body")
		private PANInquiryResponseBody body;

		@JsonProperty(value = "Signature")
		private PANInquiryRequestSignature signature;

	@NoArgsConstructor
	@AllArgsConstructor
	@Builder
	@Data
	public static class PANInquiryResponseBody {

		@JsonProperty(value = "esbreturncode")
		private String esbReturnCode;

		@JsonProperty(value = "esbreturndesc")
		private String esbReturnDesc;

		@JsonProperty(value = "returncode")
		private String returnCode;

		@JsonProperty(value = "panDetails")
		private List<PANInquiryDetails> panDetails;
	}

	@NoArgsConstructor
	@AllArgsConstructor
	@Builder
	@Data
	public static class PANInquiryDetails {

		@JsonProperty(value = "pan")
		private String panNumber;

		@JsonProperty(value = "panstatus")
		private String panStatus;

		@JsonProperty(value = "lastname")
		private String lastName;

		@JsonProperty(value = "firstname")
		private String firstName;

		@JsonProperty(value = "middlename")
		private String middleName;

		@JsonProperty(value = "pan-title")
		private String panTitle;

		@JsonProperty(value = "last-update-date")
		private String lastUpdateDate;

		@JsonProperty(value = "filler1")
		private String filler1;

		@JsonProperty(value = "filler2")
		private String filler2;

		@JsonProperty(value = "filler3")
		private String filler3;

		@JsonProperty(value = "aadhar-seeding-status")
		private String aadharSeedingStatus;
	}
}
