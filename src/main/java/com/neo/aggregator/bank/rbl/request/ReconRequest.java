package com.neo.aggregator.bank.rbl.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@JsonRootName(value = "getRecord")
@Builder
@Getter
@Setter
@Data
public class ReconRequest {
	@JsonProperty(value = "Header")
	private ReconHeader Header;

	@JsonProperty(value = "Body")
	private ReconBody Body;

	@Builder
	@Getter
	@Setter
	@Data
	@JsonInclude(Include.NON_NULL)
	public static class ReconHeader {

		@JsonProperty(value = "TranID")
		private String TranID;

		@JsonProperty(value = "Corp_ID")
		private String CorpID;
	}

	@Builder
	@Getter
	@Setter
	@Data
	@JsonInclude(Include.NON_NULL)
	public static class ReconBody {

		@JsonProperty(value = "CLIENTID")
		private String ClientID;

		@JsonProperty(value = "VAN")
		private String van;

		@JsonProperty(value = "TIME")
		private String time;

		@JsonProperty(value = "DATE")
		private String date;
	}
}
