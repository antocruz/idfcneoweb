package com.neo.aggregator.bank.rbl.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName(value = "ekyconetimetokenreq")
@JsonPropertyOrder({"userid", "password", "bcagentid", "cpuniquerefno", "datetime", "name", "dob",
        "address", "state", "city", "pincode", "mobile", "email", "pan", "req", "bseg", "type",
        "field13", "field14", "field15", "field16"})
public class OneTimeTokenRequest {

    private String userid;
    private String password;
    private String bcagentid;
    private String cpuniquerefno;
    private String datetime;
    private String name;
    private String dob;
    private String address;
    private String state;
    private String city;
    private String pincode;
    private String mobile;
    private String email;
    private String pan;
    private String req;
    private String bseg;
    private String type;
    private String field13;
    private String field14;
    private String field15;
    private String field16;

    @JsonProperty(value = "userid")
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    @JsonProperty(value = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty(value = "bcagentid")
    public String getBcagentid() {
        return bcagentid;
    }

    public void setBcagentid(String bcagentid) {
        this.bcagentid = bcagentid;
    }

    @JsonProperty(value = "cpuniquerefno")
    public String getCpuniquerefno() {
        return cpuniquerefno;
    }

    public void setCpuniquerefno(String cpuniquerefno) {
        this.cpuniquerefno = cpuniquerefno;
    }

    @JsonProperty(value = "datetime")
    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    @JsonProperty(value = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty(value = "dob")
    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    @JsonProperty(value = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty(value = "state")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @JsonProperty(value = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty(value = "pincode")
    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    @JsonProperty(value = "mobile")
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @JsonProperty(value = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty(value = "pan")
    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    @JsonProperty(value = "req")
    public String getReq() {
        return req;
    }

    public void setReq(String req) {
        this.req = req;
    }

    @JsonProperty(value = "bseg")
    public String getBseg() {
        return bseg;
    }

    public void setBseg(String bseg) {
        this.bseg = bseg;
    }

    @JsonProperty(value = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty(value = "field13")
    public String getField13() {
        return field13;
    }

    public void setField13(String field13) {
        this.field13 = field13;
    }

    @JsonProperty(value = "field14")
    public String getField14() {
        return field14;
    }

    public void setField14(String field14) {
        this.field14 = field14;
    }

    @JsonProperty(value = "field15")
    public String getField15() {
        return field15;
    }

    public void setField15(String field15) {
        this.field15 = field15;
    }

    @JsonProperty(value = "field16")
    public String getField16() {
        return field16;
    }

    public void setField16(String field16) {
        this.field16 = field16;
    }


}
