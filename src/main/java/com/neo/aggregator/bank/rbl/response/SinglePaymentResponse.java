package com.neo.aggregator.bank.rbl.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.dto.ecollect.rbl.VARequestHeader;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName(value = "Single_Payment_Corp_Resp")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"Header", "Body", "Signature"})
public class SinglePaymentResponse {

    @JsonProperty(value = "Header")
    private VARequestHeader header;

    @JsonProperty(value = "Body")
    private PaymentRespBody body;

    @JsonProperty(value = "Signature")
    private PaymentRespSignature sign;

    @Getter
    @Setter
    public static class PaymentRespBody {
        @JsonProperty(value = "Channelpartnerrefno")
        private String channelPartnerRefno;
        @JsonProperty(value = "RRN")
        private String rrn;
        @JsonProperty(value = "RefNo")
        private String refno;
        @JsonProperty(value = "UTRNo")
        private String utrno;
        @JsonProperty(value = "PONum")
        private String poNum;
        @JsonProperty(value = "Amount")
        private String amount;
        @JsonProperty(value = "Ben_Acct_No")
        private String benAcctNo;
        @JsonProperty(value = "Txn_Time")
        private String txnTime;
        @JsonProperty(value = "Ben_IFSC")
        private String benIfsc;
    }

    @Getter
    @Setter
    public static class PaymentRespSignature {
        @JsonProperty(value = "Signature")
        private String signature;
    }

}