package com.neo.aggregator.bank.rbl;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "OfflinePaperlessKyc")
@Data
public class AadhaarXmlDto {

    protected String referenceId;
    protected UidData uidData;
    protected AadhaarXmlSignatureDto signature;

    @XmlAttribute(name = "referenceId", required = true)
    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    @XmlElement(name = "UidData", required = true)
    public UidData getUidData() {
        return uidData;
    }

    public void setUidData(UidData uidData) {
        this.uidData = uidData;
    }

    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    public AadhaarXmlSignatureDto getSignature() {
        return signature;
    }

    public void setSignature(AadhaarXmlSignatureDto signature) {
        this.signature = signature;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "poi", "poa",  "pht" })
    @Data
    public static class UidData {
        @XmlElement(name = "Poi", required = true)
        protected Poi poi;

        @XmlElement(name = "Poa", required = true)
        protected Poa poa;

        @XmlElement(name = "Pht", required = true)
        protected String pht;

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        @Data
        public static class Poa {

            @XmlAttribute(name = "careof", required = true)
            protected String co;
            @XmlAttribute(name = "country", required = true)
            protected String country;
            @XmlAttribute(name = "dist", required = true)
            protected String dist;
            @XmlAttribute(name = "house", required = true)
            protected String house;
            @XmlAttribute(name = "landmark", required = true)
            protected String landmark;
            @XmlAttribute(name = "loc", required = true)
            protected String loc;
            @XmlAttribute(name = "pc", required = true)
            protected String pc;
            @XmlAttribute(name = "po", required = true)
            protected String po;
            @XmlAttribute(name = "state", required = true)
            protected String state;
            @XmlAttribute(name = "street", required = true)
            protected String street;
            @XmlAttribute(name = "subdist", required = true)
            protected String subdist;
            @XmlAttribute(name = "vtc", required = true)
            protected String vtc;

            public String getCo() {
                return co;
            }

            public void setCo(String value) {
                this.co = value;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String value) {
                this.country = value;
            }

            public String getDist() {
                return dist;
            }

            public void setDist(String value) {
                this.dist = value;
            }

            public String getHouse() {
                return house;
            }

            public void setHouse(String value) {
                this.house = value;
            }

            public String getPc() {
                return pc;
            }

            public void setPc(String value) {
                this.pc = value;
            }

            public String getState() {
                return state;
            }

            public void setState(String value) {
                this.state = value;
            }

            public String getStreet() {
                return street;
            }

            public void setStreet(String value) {
                this.street = value;
            }

            public String getVtc() {
                return vtc;
            }

            public void setVtc(String value) {
                this.vtc = value;
            }

            public String getLandmark() {
                return landmark;
            }

            public void setLandmark(String landmark) {
                this.landmark = landmark;
            }

            public String getLoc() {
                return loc;
            }

            public void setLoc(String loc) {
                this.loc = loc;
            }

            public String getPo() {
                return po;
            }

            public void setPo(String po) {
                this.po = po;
            }

            public String getSubdist() {
                return subdist;
            }

            public void setSubdist(String subdist) {
                this.subdist = subdist;
            }
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        @Data
        public static class Poi {

            @XmlAttribute(name = "dob", required = true)
            protected String dob;
            @XmlAttribute(name = "gender", required = true)
            protected String gender;
            @XmlAttribute(name = "name", required = true)
            protected String name;

            public String getDob() {
                return dob;
            }

            public void setDob(String value) {
                this.dob = value;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String value) {
                this.gender = value;
            }

            public String getName() {
                return name;
            }

            public void setName(String value) {
                this.name = value;
            }

        }
    }

}
