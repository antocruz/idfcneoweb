package com.neo.aggregator.bank.rbl.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.dto.ecollect.rbl.VARequestHeader;
import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName(value = "get_Single_Payment_Status_Corp_Req")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"Header", "Body","Signature"})
public class PaymentStatusRequest {

    @JsonProperty(value = "Header")
    private VARequestHeader header;

    @JsonProperty(value = "Body")
    private StatusReqBody body;

    @JsonProperty(value = "Signature")
    private StatusReqSignature sign;

    @Getter @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({"RefNo", "UTRNo","RRN"})
    public static class StatusReqBody {
        @JsonProperty(value = "RefNo")
        private String refNo;
        @JsonProperty(value = "UTRNo")
        private String utr;
        @JsonProperty(value = "RRN")
        private String rrn;
        @JsonProperty(value = "OrgTransactionID")
        private String orgTxnId;
    }

    @Getter @Setter
    public static class StatusReqSignature {
        @JsonProperty(value = "Signature")
        private String signature;
    }

}