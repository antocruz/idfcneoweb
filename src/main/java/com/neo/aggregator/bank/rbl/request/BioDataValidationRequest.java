package com.neo.aggregator.bank.rbl.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName(value = "ekycbidetailsreq")
@JsonPropertyOrder({"userid", "password", "bcagentid", "rbluniquerefno", "uniquerefno",
        "ekyctype", "encryptedpid", "encryptedhmac", "sessionkeyvalue", "certificateidentifier",
        "registereddeviceserviceid", "registereddeviceserviceversion",
        "registereddeviceproviderid", "registereddevicecode", "registereddevicemodelid", "registereddevicepublickey"})
public class BioDataValidationRequest {

    @JsonProperty
    private String userid;
    @JsonProperty
    private String password;
    @JsonProperty
    private String bcagentid;
    @JsonProperty
    private String rbluniquerefno;
    @JsonProperty
    private String uniquerefno;
    @JsonProperty
    private Integer ekyctype;
    @JsonProperty
    private String encryptedpid;
    @JsonProperty
    private String encryptedhmac;
    @JsonProperty
    private String sessionkeyvalue;
    @JsonProperty
    private String certificateidentifier;
    @JsonProperty
    private String registereddeviceserviceid;
    @JsonProperty
    private String registereddeviceserviceversion;
    @JsonProperty
    private String registereddeviceproviderid;
    @JsonProperty
    private String registereddevicecode;
    @JsonProperty
    private String registereddevicemodelid;
    @JsonProperty
    private String registereddevicepublickey;

}