package com.neo.aggregator.bank.rbl.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ReconResponse {

	@JsonProperty(value = "getRecordRes")
	private List<GetRecordRes> getRecordRes;

	@Data
	public static class GetRecordRes {
		@JsonProperty(value = "Payload")
		private Payload payload;
	}

	@Data
	public static class Payload {
		@JsonProperty(value = "ServiceName")
		private String serviceName;
		@JsonProperty(value = "Action")
		private String action;
		@JsonProperty(value = "Data")
		private Datas data;
	}

	@Data
	public static class Datas {
		@JsonProperty(value = "Item")
		private Item item;
	}

	@Data
	public static class Item {
		@JsonProperty(value = "messageType")
		private String messageType;
		@JsonProperty(value = "amount")
		private String amount;
		@JsonProperty(value = "UTRNumber")
		private String UTRNumber;
		@JsonProperty(value = "senderIFSC")
		private String senderIFSC;
		@JsonProperty(value = "senderAccountNumber")
		private String senderAccountNumber;
		@JsonProperty(value = "senderAccountType")
		private String senderAccountType;
		@JsonProperty(value = "senderName")
		private String senderName;
		@JsonProperty(value = "beneficiaryAccountType")
		private String beneficiaryAccountType;
		@JsonProperty(value = "beneficiaryAccountNumber")
		private String beneficiaryAccountNumber;
		@JsonProperty(value = "creditDate")
		private String creditDate;
		@JsonProperty(value = "creditAccountNumber")
		private String creditAccountNumber;
		@JsonProperty(value = "corporateCode")
		private String corporateCode;
		@JsonProperty(value = "clientCodeMaster")
		private String clientCodeMaster;
		@JsonProperty(value = "senderInformation")
		private String senderInformation;
	}

}
