package com.neo.aggregator.bank.rbl.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonRootName(value="ekycrequeryrurnreq")
@JsonPropertyOrder({"userid", "password", "bcagentid", "cpuniquerefno"})
public class RurnQueryRequest {

    @JsonProperty
    private String userid;
    @JsonProperty
    private String password;
    @JsonProperty
    private String bcagentid;
    @JsonProperty
    private String cpuniquerefno;
}
