package com.neo.aggregator.bank.rbl;

import java.util.List;

import com.neo.aggregator.model.BusinessEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor @AllArgsConstructor
@Getter @Setter
public class BusinessEntityListDto {
    private List<BusinessEntity> businessEntityList;
}
