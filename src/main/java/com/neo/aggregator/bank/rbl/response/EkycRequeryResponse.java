package com.neo.aggregator.bank.rbl.response;

import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.Data;

@Data @JsonRootName(value = "ekycrequeryresponse")
public class EkycRequeryResponse {

    private String status;
    private String responsecode;
    private String responsemessage;
    private String name;
    private String careOf;
    private String gender;
    private String dob;
    private String building;
    private String street;
    private String area;
    private String district;
    private String city;
    private String pin;
    private String state;
    private String country;
    private String photo;
    private String uidtoken;
    private String uniquerequestid;
    private String authenticationcode;
    private String rrn;
    private String txnid;
    private String authxmlerrorcode;
    private String authxmlerrormessage;

}
