package com.neo.aggregator.bank.rbl.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class KycAadhaarXmlResponseDto {

    private Data data;

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    public static class Data {
        private boolean error;

        private String errorMsg;

        private Result result;

        @JsonProperty("BasicInfo")
        private BasicInfo basicInfo;

        @JsonProperty("Image")
        private String image;

        @JsonProperty("Address")
        private Address address;

        @JsonProperty("AadhaarInfo")
        private String aadhaarInfo;

        private String crn;

        private float status;

        private String request_timestamp;

        private String response_timestamp;

        private Long total_time;

    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    public static class Address {

        private String careof;

        private String house;

        private String street;

        private String landmark;

        private String loc;

        private String vtc;

        private String po;

        private String pc;

        private String subdist;

        private String dist;

        private String state;

        private String country;

    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    public static class BasicInfo {
        private String name;

        private String dob;

        private String gender;
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    public static class Result {

        @JsonProperty("OfflineAadhaarDownloadedAt")
        private String offlineAadhaarDownloadedAt;

        @JsonProperty("signaturedata")
        private Signaturedata signatureData;

    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    public static class Signaturedata {

        private String issuer;

        private boolean signed;

    }
}