package com.neo.aggregator.bank.rbl.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Data
@XmlRootElement(name = "GetppiCustDetailsres")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerGetDetailsResponse {

	@XmlElement(name = "status")
	private String status;
	@XmlElement(name = "firstname")
	private String firstname;
	@XmlElement(name = "middlename")
	private String middlename;
	@XmlElement(name = "lastname")
	private String lastname;
	@XmlElement(name = "dateofbirth")
	private String dateofbirth;
	@XmlElement(name = "gender")
	private String gender;
	@XmlElement(name = "mothermaidenname")
	private String mothermaidenname;
	@XmlElement(name = "nationality")
	private String nationality;
	@XmlElement(name = "resaddress1")
	private String resaddress1;
	@XmlElement(name = "resaddress2")
	private String resaddress2;
	@XmlElement(name = "city")
	private String city;
	@XmlElement(name = "state")
	private String state;
	@XmlElement(name = "pincode")
	private String pincode;
	@XmlElement(name = "rescountry")
	private String rescountry;
	@XmlElement(name = "mobilenumber")
	private String mobilenumber;
	@XmlElement(name = "emailaddress")
	private String emailaddress;
	@XmlElement(name = "laddress1")
	private String laddress1;
	@XmlElement(name = "laddress2")
	private String laddress2;
	@XmlElement(name = "lcity")
	private String lcity;
	@XmlElement(name = "lstate")
	private String lstate;
	@XmlElement(name = "lpincode")
	private String lpincode;
	@XmlElement(name = "lcountry")
	private String lcountry;
	@XmlElement(name = "CustomerStatus")
	private String CustomerStatus;
	@XmlElement(name = "CustomerType")
	private String CustomerType;
	@XmlElement(name = "SourceIncomeType")
	private String SourceIncomeType;
	@XmlElement(name = "AnnualIncome")
	private String AnnualIncome;
	@XmlElement(name = "PoliticallyExposedPerson")
	private String PoliticallyExposedPerson;
	@XmlElement(name = "customerid")
	private String customerid;
	@XmlElement(name = "Approvalstatus")
	private String Approvalstatus;
	@XmlElement(name = "cardAlias")
	private String cardAlias;
	@XmlElement(name = "AccountNumber")
	private String AccountNumber;
	@XmlElement(name = "idproofname")
	private String idproofname;
	@XmlElement(name = "idproofnumber")
	private String idproofnumber;
	@XmlElement(name = "Reqtype")
	private String Reqtype;
	@XmlElement(name = "Reserved1")
	private String Reserved1;
	@XmlElement(name = "Reserved2")
	private String Reserved2;
	@XmlElement(name = "Reserved3")
	private String Reserved3;
	@XmlElement(name = "Reserved4")
	private String Reserved4;

}
