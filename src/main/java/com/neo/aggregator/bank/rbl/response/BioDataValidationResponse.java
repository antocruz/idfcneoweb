package com.neo.aggregator.bank.rbl.response;

import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.Data;

@Data
@JsonRootName(value="ekycbidetailsres")
public class BioDataValidationResponse {
    private String status;
    private String responsemessage;
    private String responsecode;
    private String authxmlerrorcode;
    private String authxmlerrormessage;
    private String authenticationCode;
    private String rrn;
    private String txnid;
    private String uniquerequestid;
}
