package com.neo.aggregator.bank.rbl;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"signedInfo", "signatureValue"})
@Data
public class AadhaarXmlSignatureDto {

    @XmlElement(name = "SignedInfo", required = true)
    protected SignedInfo signedInfo;
    @XmlElement(name = "SignatureValue", required = true)
    protected Object signatureValue;

    public SignedInfo getSignedInfo() {
        return signedInfo;
    }

    public void setSignedInfo(SignedInfo value) {
        this.signedInfo = value;
    }

    public Object getSignatureValue() {
        return signatureValue;
    }

    public void setSignatureValue(Object value) {
        this.signatureValue = value;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"canonicalizationMethod", "signatureMethod", "reference"})
    @Data
    public static class SignedInfo {

        @XmlElement(name = "CanonicalizationMethod", required = true)
        protected CanonicalizationMethod canonicalizationMethod;
        @XmlElement(name = "SignatureMethod", required = true)
        protected SignatureMethod signatureMethod;
        @XmlElement(name = "Reference", required = true)
        protected Reference reference;

        public CanonicalizationMethod getCanonicalizationMethod() {
            return canonicalizationMethod;
        }

        public void setCanonicalizationMethod(CanonicalizationMethod value) {
            this.canonicalizationMethod = value;
        }

        public SignatureMethod getSignatureMethod() {
            return signatureMethod;
        }

        public void setSignatureMethod(SignatureMethod value) {
            this.signatureMethod = value;
        }

        public Reference getReference() {
            return reference;
        }

        public void setReference(Reference value) {
            this.reference = value;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class CanonicalizationMethod {

            @XmlAttribute(name = "Algorithm", required = true)
            protected String algorithm;

            public String getAlgorithm() {
                return algorithm;
            }

            public void setAlgorithm(String value) {
                this.algorithm = value;
            }

        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {"transforms", "digestMethod", "digestValue"})
        public static class Reference {

            @XmlElement(name = "Transforms", required = true)
            protected Transforms transforms;
            @XmlElement(name = "DigestMethod", required = true)
            protected DigestMethod digestMethod;
            @XmlElement(name = "DigestValue", required = true)
            protected String digestValue;
            @XmlAttribute(name = "URI", required = true)
            protected String uri;

            public Transforms getTransforms() {
                return transforms;
            }

            public void setTransforms(Transforms value) {
                this.transforms = value;
            }

            public DigestMethod getDigestMethod() {
                return digestMethod;
            }

            public void setDigestMethod(DigestMethod value) {
                this.digestMethod = value;
            }

            public String getDigestValue() {
                return digestValue;
            }

            public void setDigestValue(String value) {
                this.digestValue = value;
            }

            public String getURI() {
                return uri;
            }

            public void setURI(String value) {
                this.uri = value;
            }

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class DigestMethod {

                @XmlAttribute(name = "Algorithm", required = true)
                protected String algorithm;

                public String getAlgorithm() {
                    return algorithm;
                }

                public void setAlgorithm(String value) {
                    this.algorithm = value;
                }

            }

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {"transform"})
            public static class Transforms {

                @XmlElement(name = "Transform", required = true)
                protected Transform transform;

                public Transform getTransform() {
                    return transform;
                }

                public void setTransform(Transform value) {
                    this.transform = value;
                }

                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Transform {

                    @XmlAttribute(name = "Algorithm", required = true)
                    protected String algorithm;

                    public String getAlgorithm() {
                        return algorithm;
                    }

                    public void setAlgorithm(String value) {
                        this.algorithm = value;
                    }

                }

            }

        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class SignatureMethod {

            @XmlAttribute(name = "Algorithm", required = true)
            protected String algorithm;

            public String getAlgorithm() {
                return algorithm;
            }

            public void setAlgorithm(String value) {
                this.algorithm = value;
            }

        }

    }

}
