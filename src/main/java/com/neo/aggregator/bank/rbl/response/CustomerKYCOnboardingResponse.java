package com.neo.aggregator.bank.rbl.response;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "CustomerKYCOnboardingres")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class CustomerKYCOnboardingResponse {

	@XmlElement(name = "status")
	private String status;
	@XmlElement(name = "customerid")
	private String customerid;
	@XmlElement(name = "Description")
	private String Description;
}
