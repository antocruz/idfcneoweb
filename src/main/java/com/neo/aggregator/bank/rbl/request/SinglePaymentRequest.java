package com.neo.aggregator.bank.rbl.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.dto.ecollect.rbl.VARequestHeader;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName(value = "Single_Payment_Corp_Req")
@JsonPropertyOrder({"Header", "Body", "Signature"})
public class SinglePaymentRequest {

    @JsonProperty(value = "Header")
    private VARequestHeader header;

    @JsonProperty(value = "Body")
    private PaymentReqBody body;

    @JsonProperty(value = "Signature")
    private PaymentReqSignature sign;

    @Builder
    @JsonPropertyOrder({"Amount", "Debit_Acct_No", "Debit_Acct_Name", "Debit_IFSC", "Debit_Mobile", "Debit_TrnParticulars",
            "Debit_PartTrnRmks", "Ben_IFSC", "Ben_Acct_No", "Ben_Name", "Ben_Address", "Ben_BankName", "Ben_BankCd",
            "Ben_BranchCd", "Ben_Email", "Ben_Mobile", "Ben_TrnParticulars", "Ben_PartTrnRmks", "Issue_BranchCd", "Mode_of_Pay", "Remarks"})
    public static class PaymentReqBody {
        @JsonProperty(value = "Amount")
        private String amount;
        @JsonProperty(value = "Debit_Acct_No")
        private String debitAcctNo;
        @JsonProperty(value = "Debit_Acct_Name")
        private String debitAcctName;
        @JsonProperty(value = "Debit_IFSC")
        private String debitIfsc;
        @JsonProperty(value = "Debit_Mobile")
        private String debitMobile;
        @JsonProperty(value = "Debit_TrnParticulars")
        private String debitTrnparticulars;
        @JsonProperty(value = "Debit_PartTrnRmks")
        private String debitParttrnrmks;
        @JsonProperty(value = "Ben_IFSC")
        private String benIfsc;
        @JsonProperty(value = "Ben_Acct_No")
        private String benAcctNo;
        @JsonProperty(value = "Ben_Name")
        private String benName;
        @JsonProperty(value = "Ben_Address")
        private String benAddress;
        @JsonProperty(value = "Ben_BankName")
        private String benBankname;
        @JsonProperty(value = "Ben_BankCd")
        private String benBankcd;
        @JsonProperty(value = "Ben_BranchCd")
        private String benBranchcd;
        @JsonProperty(value = "Ben_Email")
        private String benEmail;
        @JsonProperty(value = "Ben_Mobile")
        private String benMobile;
        @JsonProperty(value = "Ben_TrnParticulars")
        private String benTrnparticulars;
        @JsonProperty(value = "Ben_PartTrnRmks")
        private String benParttrnrmks;
        @JsonProperty(value = "Issue_BranchCd")
        private String issueBranchcd;
        @JsonProperty(value = "Mode_of_Pay")
        private String modeOfPay;
        @JsonProperty(value = "Remarks")
        private String remarks;

    }

    @Builder
    public static class PaymentReqSignature {
        @JsonProperty(value = "Signature")
        private String signature;
    }

}