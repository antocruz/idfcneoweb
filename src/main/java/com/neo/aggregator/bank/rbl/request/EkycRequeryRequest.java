package com.neo.aggregator.bank.rbl.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonRootName(value = "ekycrequeryrequest")
@JsonPropertyOrder({"header", "uniquerequestid"})
public class EkycRequeryRequest {

    @JsonProperty
    private RequestHeader header;
    @JsonProperty
    private String uniquerequestid;

    @Builder
    @JsonPropertyOrder({"username", "password"})
    public static class RequestHeader {
        @JsonProperty
        private String username;
        @JsonProperty
        private String password;
    }
}
