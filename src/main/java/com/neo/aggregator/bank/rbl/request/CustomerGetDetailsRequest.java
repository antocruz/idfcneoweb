package com.neo.aggregator.bank.rbl.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "GetppiCustDetailsreq")
@XmlType(name = "", propOrder = { "header", "refno", "mobilenumber" })
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@Data
public class CustomerGetDetailsRequest {

	@XmlElement(name = "header", required = true)
	private RequestHeader header;

	@XmlElement(name = "refno")
	private String refno;

	@XmlElement(name = "mobilenumber")
	private String mobilenumber;

	@Builder
	@AllArgsConstructor
	@NoArgsConstructor
	public static class RequestHeader {
		@XmlElement(name = "username")
		private String username;

		@XmlElement(name = "password")
		private String password;
	}

}
