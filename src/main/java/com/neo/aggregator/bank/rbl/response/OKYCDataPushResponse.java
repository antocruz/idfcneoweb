package com.neo.aggregator.bank.rbl.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value = "okycresponse")
public class OKYCDataPushResponse {

    @JsonProperty(value = "status")
    private String status;
    @JsonProperty(value = "description")
   private String description;
}
