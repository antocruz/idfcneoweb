package com.neo.aggregator.bank.rbl.response;

import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.Data;

@Data
@JsonRootName(value="errorres")
public class ErrorResponse {

    private String status;
    private String description;
}
