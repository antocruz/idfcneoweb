package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FetchAccountLeadMessageBody {
	private String accountLeadId;
	private String authenticationToken;

	@JsonProperty("accountLeadId")
	public String getAccountLeadId() {
		return accountLeadId;
	}

	public void setAccountLeadId(String accountLeadId) {
		this.accountLeadId = accountLeadId;
	}

	@JsonProperty("authenticationToken")
	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

}
