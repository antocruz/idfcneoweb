package com.neo.aggregator.bank.equitas.dto;

import lombok.Data;

@Data
public class CustomerPreferencesDto {
	private Boolean isStaff;
	private Boolean allSMSAlerts;
	private Boolean onlyTransactionAlerts;
	private Boolean netBanking;
	private Boolean mobileBankingNumber;
	private Boolean sms;
	private Boolean passbook;
	private Boolean physicalStatement;
	private Boolean emailStatement;
	private String netBankingRights;
	private String mappedAccountLead;
	private String preferenceID;
	private String mappedCustomer;
	private String mappedCustomerLead;
	private String mappedAccount;
	private String bankGuarantee;
	private String letterofCredit;
	private String businessLoan;
	private String doorStepBanking;
	private String doorStepBankingOnCall;
	private String doorStepBankingBeat;
	private String tradeForex;
	private String loanAgainstProperty;
	private String overdraftsagainstFD;
	private String isDelete;
	private String entityType;
	private String entityFlag;
	private String brnchId;
	private String alertEmailID;
}
