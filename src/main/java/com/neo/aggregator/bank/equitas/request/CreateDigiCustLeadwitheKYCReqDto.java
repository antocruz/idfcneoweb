package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.DigiCustLeadwitheKYCMessageBody;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.dto.KycRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("createDigiCustLeadwitheKYCReq")
public class CreateDigiCustLeadwitheKYCReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;
	
	protected LeadMessageHeader leadMessageHeader;
	protected DigiCustLeadwitheKYCMessageBody digiCustLeadwitheKYCMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public DigiCustLeadwitheKYCMessageBody getDigiCustLeadwitheKYCMessageBody() {
		return digiCustLeadwitheKYCMessageBody;
	}

	public void setDigiCustLeadwitheKYCMessageBody(DigiCustLeadwitheKYCMessageBody digiCustLeadwitheKYCMessageBody) {
		this.digiCustLeadwitheKYCMessageBody = digiCustLeadwitheKYCMessageBody;
	}
	
	public CreateDigiCustLeadwitheKYCReqDto build(KycRequestDto request) throws NeoException {

		CreateDigiCustLeadwitheKYCReqDto requestDto = null;

		try {
			
			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);
			
			
			DigiCustLeadwitheKYCMessageBody digiCustLeadwitheKYCMessage = equitasCommonService.buildDigiCustLeadwitheKYCMessageBody(request);
			
			requestDto = CreateDigiCustLeadwitheKYCReqDto.builder().digiCustLeadwitheKYCMessageBody(digiCustLeadwitheKYCMessage).leadMessageHeader(leadMessage)
					.build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}

}
