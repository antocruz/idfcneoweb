package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.LeadUpdateMessageBody;
import com.neo.aggregator.bank.equitas.dto.KycRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("updateDigiCustLeadReq")
public class UpdateLeadReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;
	
	protected LeadMessageHeader leadMessageHeader;
	protected LeadUpdateMessageBody leadUpdateMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public LeadUpdateMessageBody getLeadUpdateMessageBody() {
		return leadUpdateMessageBody;
	}

	public void setLeadUpdateMessageBody(LeadUpdateMessageBody leadUpdateMessageBody) {
		this.leadUpdateMessageBody = leadUpdateMessageBody;
	}

	public UpdateLeadReqDto build(KycRequestDto request) throws NeoException {

		UpdateLeadReqDto requestDto = null;

		try {
			
			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);
			
			
			LeadUpdateMessageBody leadUpdateMessage = equitasCommonService.buildUpdateLeadMessageBody(request);
			
			requestDto = UpdateLeadReqDto.builder().leadUpdateMessageBody(leadUpdateMessage).leadMessageHeader(leadMessage)
					.build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}
}
