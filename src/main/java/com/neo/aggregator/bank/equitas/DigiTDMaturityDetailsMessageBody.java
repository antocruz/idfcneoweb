package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DigiTDMaturityDetailsMessageBody {

	private String tdAcctNb;
	private String dpstNb;

	@JsonProperty("tdAcctNb")
	public String getTdAcctNb() {
		return tdAcctNb;
	}

	public void setTdAcctNb(String tdAcctNb) {
		this.tdAcctNb = tdAcctNb;
	}

	@JsonProperty("dpstNb")
	public String getDpstNb() {
		return dpstNb;
	}

	public void setDpstNb(String dpstNb) {
		this.dpstNb = dpstNb;
	}

}
