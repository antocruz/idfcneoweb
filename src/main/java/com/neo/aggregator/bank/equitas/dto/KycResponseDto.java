package com.neo.aggregator.bank.equitas.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.neo.aggregator.bank.equitas.response.UpdateLeadResDto.Body.AddressDetailRes;
import com.neo.aggregator.dto.RegistrationRequestDtoV2;
import com.neo.aggregator.dto.v2.AddressDto;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KycResponseDto {

	private String rslt;
	private String isInsert;
	private String dmsDocIDs;
	private String isFirstInserted;
	private String id;
	private String proceedToNLTable;
	private String stan;
	private String eKYCXMLStringRepPayload;
	private String kycStatus;
	private String entityId;
	private String sorCustomerId;
	private String kycExpiryDate;
	private String kycRefNo;
	private Integer status;
	private String custLeadId;
	private String custId;
	private String authCode;
	private String title;
	private String name;
	private String dob;
	private AddressDto addressDto;
	private String rrn;
	private String photo;
	private String add;
	private DocumentDto[] documents;
	private AddDocRespDto addDocResp;
	private AddressDetailRes[] address;
	private RegistrationRequestDtoV2 registrationRequestDtoV2;
	
}
