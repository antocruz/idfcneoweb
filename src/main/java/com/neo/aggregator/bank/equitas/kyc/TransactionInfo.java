package com.neo.aggregator.bank.equitas.kyc;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@XmlType(propOrder = {"uid", "transmDateTime", "stan", "localTransTime", "localDate", "caId", "caTid", "caTa"})
public class TransactionInfo {

	private UID uid;
	private String transmDateTime;
	private String stan;
	private String localTransTime;
	private String localDate;
	private String caId;
	private String caTid;
	private String caTa;

	@XmlElement(name = "UID")
	public UID getUid() {
		return uid;
	}

	public void setUid(UID uid) {
		this.uid = uid;
	}

	@XmlElement(name = "Transm_Date_time")
	public String getTransmDateTime() {
		return transmDateTime;
	}

	public void setTransmDateTime(String transmDateTime) {
		this.transmDateTime = transmDateTime;
	}

	@XmlElement(name = "Stan")
	public String getStan() {
		return stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	@XmlElement(name = "Local_Trans_Time")
	public String getLocalTransTime() {
		return localTransTime;
	}

	public void setLocalTransTime(String localTransTime) {
		this.localTransTime = localTransTime;
	}

	@XmlElement(name = "Local_date")
	public String getLocalDate() {
		return localDate;
	}

	public void setLocalDate(String localDate) {
		this.localDate = localDate;
	}

	@XmlElement(name = "CA_ID")
	public String getCaId() {
		return caId;
	}

	public void setCaId(String caId) {
		this.caId = caId;
	}

	@XmlElement(name = "CA_TID")
	public String getCaTid() {
		return caTid;
	}

	public void setCaTid(String caTid) {
		this.caTid = caTid;
	}

	@XmlElement(name = "CA_TA")
	public String getCaTa() {
		return caTa;
	}

	public void setCaTa(String caTa) {
		this.caTa = caTa;
	}

}
