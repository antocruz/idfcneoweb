package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IndividualEntry {

	private String title;
	private String firstName;
	private String lastName;
	private String middleName;
	private String shortName;
	private String mobilePhone;
	private String dob;
	private String aadhar;
	private String PAN;
	private String motherMaidenName;
	private String identityType;
	private String iFullName;
	private String NLFound;
	private String reasonNotApplicable;
	private String reason;
	private String voterid;
	private String purposeOfCreation;
	
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@JsonProperty("firstName")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@JsonProperty("lastName")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@JsonProperty("middleName")
	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@JsonProperty("shortName")
	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	@JsonProperty("mobilePhone")
	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	@JsonProperty("dob")
	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	@JsonProperty("aadhar")
	public String getAadhar() {
		return aadhar;
	}

	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}

	@JsonProperty("PAN")
	public String getPAN() {
		return PAN;
	}

	public void setPAN(String PAN) {
		this.PAN = PAN;
	}

	@JsonProperty("motherMaidenName")
	public String getMotherMaidenName() {
		return motherMaidenName;
	}

	public void setMotherMaidenName(String motherMaidenName) {
		this.motherMaidenName = motherMaidenName;
	}

	@JsonProperty("identityType")
	public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}

	@JsonProperty("iFullName")
	public String getiFullName() {
		return iFullName;
	}

	public void setiFullName(String iFullName) {
		this.iFullName = iFullName;
	}

	@JsonProperty("NLFound")
	public String getNLFound() {
		return NLFound;
	}

	public void setNLFound(String nLFound) {
		this.NLFound = nLFound;
	}

	@JsonProperty("reasonNotApplicable")
	public String getReasonNotApplicable() {
		return reasonNotApplicable;
	}

	public void setReasonNotApplicable(String reasonNotApplicable) {
		this.reasonNotApplicable = reasonNotApplicable;
	}

	@JsonProperty("reason")
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@JsonProperty("voterid")
	public String getVoterid() {
		return voterid;
	}

	public void setVoterid(String voterid) {
		this.voterid = voterid;
	}

	@JsonProperty("purposeOfCreation")
	public String getPurposeOfCreation() {
		return purposeOfCreation;
	}

	public void setPurposeOfCreation(String purposeOfCreation) {
		this.purposeOfCreation = purposeOfCreation;
	}
	
	
}
