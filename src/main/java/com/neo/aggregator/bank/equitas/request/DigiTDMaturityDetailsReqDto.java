package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.DigiTDMaturityDetailsMessageBody;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.dto.DigiAccountRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("getDigiTDMaturityDetailsRequest")
public class DigiTDMaturityDetailsReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;
	
	protected LeadMessageHeader leadMessageHeader;
	protected DigiTDMaturityDetailsMessageBody digiTDMaturityDetailsMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public DigiTDMaturityDetailsMessageBody getDigiTDMaturityDetailsMessageBody() {
		return digiTDMaturityDetailsMessageBody;
	}

	public void setDigiTDMaturityDetailsMessageBody(DigiTDMaturityDetailsMessageBody digiTDMaturityDetailsMessageBody) {
		this.digiTDMaturityDetailsMessageBody = digiTDMaturityDetailsMessageBody;
	}
	
	public DigiTDMaturityDetailsReqDto build(DigiAccountRequestDto request) throws NeoException {

		DigiTDMaturityDetailsReqDto requestDto = null;

		try {
			
			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);
			
			
			DigiTDMaturityDetailsMessageBody digiTDMaturityDetailsMessage = equitasCommonService.buildDigiTDMaturityDetailsMessageBody(request);
			
			requestDto = DigiTDMaturityDetailsReqDto.builder().digiTDMaturityDetailsMessageBody(digiTDMaturityDetailsMessage).leadMessageHeader(leadMessage)
					.build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}


}
