package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.MessageBody;
import com.neo.aggregator.bank.equitas.MessageHeader;
import com.neo.aggregator.bank.equitas.dto.KycRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("createDigiCustomerByLeadReq")
public class CreateDigiCustomerByLeadReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;
	
	protected MessageHeader messageHeader;
	protected MessageBody messageBody;

	@JsonProperty("msgHdr")
	public MessageHeader getMessageHeader() {
		return messageHeader;
	}

	public void setMessageHeader(MessageHeader messageHeader) {
		this.messageHeader = messageHeader;
	}

	@JsonProperty("msgBdy")
	public MessageBody getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(MessageBody messageBody) {
		this.messageBody = messageBody;
	}

	public CreateDigiCustomerByLeadReqDto build(KycRequestDto request) throws NeoException {

		CreateDigiCustomerByLeadReqDto requestDto = null;

		try {
			
			MessageHeader messageHeaderBuild = equitasCommonService.buildMessageHeader(request);
			
			MessageBody messageBodyBuild = equitasCommonService.buildMessageBody(request);
			
			requestDto = CreateDigiCustomerByLeadReqDto.builder().messageBody(messageBodyBuild).messageHeader(messageHeaderBuild)
					.build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}
}
