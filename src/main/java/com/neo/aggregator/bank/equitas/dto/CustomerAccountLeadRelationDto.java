package com.neo.aggregator.bank.equitas.dto;

import lombok.Data;

@Data
public class CustomerAccountLeadRelationDto {

	private Boolean isPrimaryHolder;
	private Boolean isInsert;
	private String customerAccountRelation;
	private String id;
	private String completedDataEntryD0;
	private String ucic;

}
