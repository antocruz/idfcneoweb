package com.neo.aggregator.bank.equitas.dto;

import lombok.Data;

@Data
public class DocumentDto {

	private String documentType;

	private String documentNo;

	private String documentExpDate;

	private String documentFilePath;

	private String documentFileName;

	private String ekycRefNo;

	private String dmsDocumentID;
	
	private String categoryCode;
	
	private String documentID;
	
	private String expiryDate;
	
	private String issueDate;
	
	private String issuedAt;
	
	private String mappedUCIC;
	
	private String mappedCustomerLead;
	
	private String mappedServiceRequest;
	
	private String subcategoryCode;
	
	private String mappedAccount;
	
	private String mappedAccountLead;
	
	private String mappedBeneficialOwner;
	
	private String mappedGuardian;
	
	private String mappedNominee;
	
	private String mappedDependent;
	
	private String operation;
	
	private String mappedException;
	
	private String verifiedOn;
	
	private String verifiedBy;
	
	private String verificationStatus;
	
	private String docTypeNOVD;
	
	private String bsPyld;
}
