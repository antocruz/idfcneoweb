package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubmitAccountLeadMessageBody {

	private String authToken;
	private String emailId;
	private String loginUserId;
	private String accountLeadId;

	@JsonProperty("authToken")
	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	@JsonProperty("emailId")
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@JsonProperty("loginUserId")
	public String getLoginUserId() {
		return loginUserId;
	}

	public void setLoginUserId(String loginUserId) {
		this.loginUserId = loginUserId;
	}

	@JsonProperty("accountLeadId")
	public String getAccountLeadId() {
		return accountLeadId;
	}

	public void setAccountLeadId(String accountLeadId) {
		this.accountLeadId = accountLeadId;
	}

}
