package com.neo.aggregator.bank.equitas.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("getDigiTDMaturityDetailsResponse")
public class DigiTDMaturityDetailsResDto {

	@JsonProperty("msgHdr")
	protected DigiTDMaturityDetailsResDto.Header msgHdr;
	@JsonProperty("msgBdy")
	protected DigiTDMaturityDetailsResDto.Body msgBdy;

	public DigiTDMaturityDetailsResDto.Header getMsgHdr() {
		return msgHdr;
	}

	public void setMsgHdr(DigiTDMaturityDetailsResDto.Header msgHdr) {
		this.msgHdr = msgHdr;
	}

	public DigiTDMaturityDetailsResDto.Body getMsgBdy() {
		return msgBdy;
	}

	public void setMsgBdy(DigiTDMaturityDetailsResDto.Body msgBdy) {
		this.msgBdy = msgBdy;
	}

	public static class Header {
		@JsonProperty(value = "rslt")
		protected String rslt;

		@JsonProperty(value = "error")
		protected List<Error> error;

		public List<Error> getError() {
			return error;
		}

		public void setError(List<Error> error) {
			this.error = error;
		}

		public String getRslt() {
			return rslt;
		}

		public void setRslt(String rslt) {
			this.rslt = rslt;
		}

		public static class Error {

			@JsonProperty(value = "cd")
			protected String cd;
			@JsonProperty(value = "rsn")
			protected String rsn;

			public String getCd() {
				return cd;
			}

			public void setCd(String cd) {
				this.cd = cd;
			}

			public String getRsn() {
				return rsn;
			}

			public void setRsn(String rsn) {
				this.rsn = rsn;
			}
		}
	}

	public static class Body {

		@JsonProperty(value = "tdPytInstr")
		protected List<TdPytInstr> tdPytInstr;

		public static class TdPytInstr {
			@JsonProperty(value = "tdAcctNb")
			private String tdAcctNb;
			@JsonProperty(value = "dpstNb")
			private String dpstNb;
			@JsonProperty(value = "pytTp")
			private String pytTp;
			@JsonProperty(value = "pytFrq")
			private String pytFrq;
			@JsonProperty(value = "pytOpt")
			private String pytOpt;
			@JsonProperty(value = "pytAcctNb")
			private String pytAcctNb;
			@JsonProperty(value = "prtFlFlg")
			private String prtFlFlg;
			@JsonProperty(value = "fxdPrcntgFlg")
			private String fxdPrcntgFlg;
			@JsonProperty(value = "amtOrPrcntg")
			private String amtOrPrcntg;
			@JsonProperty(value = "instrNb")
			private String instrNb;
			@JsonProperty(value = "prtryNb")
			private String prtryNb;

			public String getTdAcctNb() {
				return tdAcctNb;
			}

			public void setTdAcctNb(String tdAcctNb) {
				this.tdAcctNb = tdAcctNb;
			}

			public String getDpstNb() {
				return dpstNb;
			}

			public void setDpstNb(String dpstNb) {
				this.dpstNb = dpstNb;
			}

			public String getPytTp() {
				return pytTp;
			}

			public void setPytTp(String pytTp) {
				this.pytTp = pytTp;
			}

			public String getPytFrq() {
				return pytFrq;
			}

			public void setPytFrq(String pytFrq) {
				this.pytFrq = pytFrq;
			}

			public String getPytOpt() {
				return pytOpt;
			}

			public void setPytOpt(String pytOpt) {
				this.pytOpt = pytOpt;
			}

			public String getPytAcctNb() {
				return pytAcctNb;
			}

			public void setPytAcctNb(String pytAcctNb) {
				this.pytAcctNb = pytAcctNb;
			}

			public String getPrtFlFlg() {
				return prtFlFlg;
			}

			public void setPrtFlFlg(String prtFlFlg) {
				this.prtFlFlg = prtFlFlg;
			}

			public String getFxdPrcntgFlg() {
				return fxdPrcntgFlg;
			}

			public void setFxdPrcntgFlg(String fxdPrcntgFlg) {
				this.fxdPrcntgFlg = fxdPrcntgFlg;
			}

			public String getAmtOrPrcntg() {
				return amtOrPrcntg;
			}

			public void setAmtOrPrcntg(String amtOrPrcntg) {
				this.amtOrPrcntg = amtOrPrcntg;
			}

			public String getInstrNb() {
				return instrNb;
			}

			public void setInstrNb(String instrNb) {
				this.instrNb = instrNb;
			}

			public String getPrtryNb() {
				return prtryNb;
			}

			public void setPrtryNb(String prtryNb) {
				this.prtryNb = prtryNb;
			}

		}

	}

}
