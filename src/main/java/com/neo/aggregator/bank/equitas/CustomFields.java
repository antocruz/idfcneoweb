package com.neo.aggregator.bank.equitas;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomFields {

		@JsonProperty("cstmFld")
		private List<CstmFld> cstmFld;
		
		public List<CstmFld> getCstmFld() {
			return cstmFld;
		}

		public void setCstmFld(List<CstmFld> cstmFld) {
			this.cstmFld = cstmFld;
		}

}
