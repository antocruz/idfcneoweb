package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateDigiTDMaturityDetailsMessageBody {

	private String tdAcctNb;
	private String pytTp;
	private String pytOpt;
	private String pytFrq;
	private String dpstNb;
	private String pytAcctNb;

	private CurrentTDMaturityDetails crntTDMtrtyDtls;

	@JsonProperty("tdAcctNb")
	public String getTdAcctNb() {
		return tdAcctNb;
	}

	public void setTdAcctNb(String tdAcctNb) {
		this.tdAcctNb = tdAcctNb;
	}

	@JsonProperty("pytTp")
	public String getPytTp() {
		return pytTp;
	}

	public void setPytTp(String pytTp) {
		this.pytTp = pytTp;
	}

	@JsonProperty("pytOpt")
	public String getPytOpt() {
		return pytOpt;
	}

	public void setPytOpt(String pytOpt) {
		this.pytOpt = pytOpt;
	}

	@JsonProperty("pytFrq")
	public String getPytFrq() {
		return pytFrq;
	}

	public void setPytFrq(String pytFrq) {
		this.pytFrq = pytFrq;
	}

	@JsonProperty("dpstNb")
	public String getDpstNb() {
		return dpstNb;
	}

	public void setDpstNb(String dpstNb) {
		this.dpstNb = dpstNb;
	}

	@JsonProperty("pytAcctNb")
	public String getPytAcctNb() {
		return pytAcctNb;
	}

	public void setPytAcctNb(String pytAcctNb) {
		this.pytAcctNb = pytAcctNb;
	}

	@JsonProperty("crntTDMtrtyDtls")
	public CurrentTDMaturityDetails getCrntTDMtrtyDtls() {
		return crntTDMtrtyDtls;
	}

	public void setCrntTDMtrtyDtls(CurrentTDMaturityDetails crntTDMtrtyDtls) {
		this.crntTDMtrtyDtls = crntTDMtrtyDtls;
	}

}
