package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IdentityInfoDetails {

	private String estimatedAgriculturalIncome;
	private String estimatedNonAgriculturalIncome;

	@JsonProperty("estimatedAgriculturalIncome")
	public String getEstimatedAgriculturalIncome() {
		return estimatedAgriculturalIncome;
	}

	public void setEstimatedAgriculturalIncome(String estimatedAgriculturalIncome) {
		this.estimatedAgriculturalIncome = estimatedAgriculturalIncome;
	}

	@JsonProperty("estimatedNonAgriculturalIncome")
	public String getEstimatedNonAgriculturalIncome() {
		return estimatedNonAgriculturalIncome;
	}

	public void setEstimatedNonAgriculturalIncome(String estimatedNonAgriculturalIncome) {
		this.estimatedNonAgriculturalIncome = estimatedNonAgriculturalIncome;
	}

}
