package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.CreateDigiCaseMessageBody;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.dto.DigiAccountRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("createDigiCaseRequest")
public class CreateDigiCaseReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;
	
	protected LeadMessageHeader leadMessageHeader;
	protected CreateDigiCaseMessageBody createDigiCaseMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public CreateDigiCaseMessageBody getCreateDigiCaseMessageBody() {
		return createDigiCaseMessageBody;
	}

	public void setCreateDigiCaseMessageBody(CreateDigiCaseMessageBody createDigiCaseMessageBody) {
		this.createDigiCaseMessageBody = createDigiCaseMessageBody;
	}
	
	public CreateDigiCaseReqDto build(DigiAccountRequestDto request) throws NeoException {

		CreateDigiCaseReqDto requestDto = null;

		try {
			
			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);
			
			
			CreateDigiCaseMessageBody createDigiCaseMessage = equitasCommonService.buildCreateDigiCaseMessageBody(request);
			
			requestDto = CreateDigiCaseReqDto.builder().createDigiCaseMessageBody(createDigiCaseMessage).leadMessageHeader(leadMessage)
					.build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}

}
