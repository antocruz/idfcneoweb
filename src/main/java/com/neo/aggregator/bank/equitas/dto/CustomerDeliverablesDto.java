package com.neo.aggregator.bank.equitas.dto;

import lombok.Data;

@Data
public class CustomerDeliverablesDto {

	private Boolean welcomeKit;
	private Boolean debitCard;
	private Boolean iKITChequeBook;
	private Boolean iKIT;
	private Boolean chequeBook;
	private String mappedCustomer;
	private String deliverableID;
}
