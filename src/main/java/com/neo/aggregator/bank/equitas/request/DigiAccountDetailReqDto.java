package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.DigiAccountDetailMessageBody;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.dto.DigiAccountRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("getDigiAccountDetailRequest")
public class DigiAccountDetailReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;
	
	protected LeadMessageHeader leadMessageHeader;
	protected DigiAccountDetailMessageBody digiAccountDetailMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public DigiAccountDetailMessageBody getDigiAccountDetailMessageBody() {
		return digiAccountDetailMessageBody;
	}

	public void setDigiAccountDetailMessageBody(DigiAccountDetailMessageBody digiAccountDetailMessageBody) {
		this.digiAccountDetailMessageBody = digiAccountDetailMessageBody;
	}
	
	public DigiAccountDetailReqDto build(DigiAccountRequestDto request) throws NeoException {

		DigiAccountDetailReqDto requestDto = null;

		try {
			
			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);
			
			
			DigiAccountDetailMessageBody digiAccountDetailMessage = equitasCommonService.buildDigiAccountDetailMessageBody(request);
			
			requestDto = DigiAccountDetailReqDto.builder().digiAccountDetailMessageBody(digiAccountDetailMessage).leadMessageHeader(leadMessage)
					.build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}

}
