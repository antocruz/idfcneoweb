package com.neo.aggregator.bank.equitas.dto;

import java.util.List;

import com.neo.aggregator.bank.equitas.CreateCaseReq;
import com.neo.aggregator.bank.equitas.CstmFld;
import com.neo.aggregator.bank.equitas.CurrentTDMaturityDetails;
import com.neo.aggregator.bank.equitas.CustomFields;

import lombok.Data;

@Data
public class DigiAccountRequestDto {

	private String brnchId;
	private String authUserId;
	private String appId;
	private Boolean fromMADP;
	private String usrId;
	private String accountLeadID;
	private String authenticationToken;
	private String userCreator;
	private String PmayIndicator;
	private String PmaySuccessIndicator;
	private String isQC;
	private String instaKitIndicator;
	private String instaKitSuccessIndicator;
	private String emailId;
	private String loginUserId;
	private List<CustomerAccountLeadRelationDto> customerAccountLeadRelationDto;
	private AccountLeadDto accountLeadDto;
	private List<CustomerPreferencesDto> customerPreferencesDto;
	private List<NomineeAddressDto> nomineeAddressDto;
	private List<CustomerDeliverablesDto> CustomerDeliverablesDto;
	private List<GuardianAddresssDto> guardianAddressDto;
	private List<NomineeDto> nomineeDto;
	private List<GuardianDto> guardianDto;
	
	//TD
	private String acctTp;
	private String accountNo;
	private String dpstNb;
	private CurrentTDMaturityDetails currentTDMaturityDetails;
	private String tdAcctNb;
	private String pytTp;
	private String pytOpt;
	private String pytFrq;
	private String pytAcctNb;
	private String rdmptnToAcct;
	private String rdmptnAmt;
	private String acctId;
	private String txnTp;
	private String userId;
	private String id;
	private String associatedWith;
	
	private List<CreateCaseReq> createCaseReq;
	private CustomFields customFields;
	private List<CstmFld> cstmFld;
	private String pgNum;
	private String lastBalanceAmount;
	private String frDt;
	private String nbOfTxns;
	private String toDt;
}
