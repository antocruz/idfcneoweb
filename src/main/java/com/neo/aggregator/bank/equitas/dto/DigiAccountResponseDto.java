package com.neo.aggregator.bank.equitas.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DigiAccountResponseDto {

	private String rslt;
	private String accountLeadID;
	
}
