package com.neo.aggregator.bank.equitas.kyc;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UID {

	
	private String uidData;
	
	private String type;

	@XmlValue
	public String getUidData() {
		return uidData;
	}

	public void setUidData(String uidData) {
		this.uidData = uidData;
	}

	@XmlAttribute(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
