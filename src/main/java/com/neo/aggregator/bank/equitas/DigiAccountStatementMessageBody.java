package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DigiAccountStatementMessageBody {

	private String pgNum;
	private String lastBalanceAmount;
	private String acctId;
	private String frDt;
	private String nbOfTxns;
	private String toDt;

	@JsonProperty("pgNum")
	public String getPgNum() {
		return pgNum;
	}

	public void setPgNum(String pgNum) {
		this.pgNum = pgNum;
	}

	@JsonProperty("lastBalanceAmount")
	public String getLastBalanceAmount() {
		return lastBalanceAmount;
	}

	public void setLastBalanceAmount(String lastBalanceAmount) {
		this.lastBalanceAmount = lastBalanceAmount;
	}

	@JsonProperty("acctId")
	public String getAcctId() {
		return acctId;
	}

	public void setAcctId(String acctId) {
		this.acctId = acctId;
	}

	@JsonProperty("frDt")
	public String getFrDt() {
		return frDt;
	}

	public void setFrDt(String frDt) {
		this.frDt = frDt;
	}

	@JsonProperty("nbOfTxns")
	public String getNbOfTxns() {
		return nbOfTxns;
	}

	public void setNbOfTxns(String nbOfTxns) {
		this.nbOfTxns = nbOfTxns;
	}

	@JsonProperty("toDt")
	public String getToDt() {
		return toDt;
	}

	public void setToDt(String toDt) {
		this.toDt = toDt;
	}

}
