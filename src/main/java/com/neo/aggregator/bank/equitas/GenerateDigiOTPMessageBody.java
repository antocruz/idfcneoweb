package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GenerateDigiOTPMessageBody {

	private String eKYCXMLStringReqPayload;

	@JsonProperty("eKYCXMLStringReqPayload")
	public String geteKYCXMLStringReqPayload() {
		return eKYCXMLStringReqPayload;
	}

	public void seteKYCXMLStringReqPayload(String eKYCXMLStringReqPayload) {
		this.eKYCXMLStringReqPayload = eKYCXMLStringReqPayload;
	}
	
	
}
