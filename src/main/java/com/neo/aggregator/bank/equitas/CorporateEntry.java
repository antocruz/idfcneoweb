package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CorporateEntry {
	private String companyName;
	private String companyName2;
	private String companyName3;
	private String companyPhone;
	private String aadhar;
	private String pocNumber;
	private String pocName;
	private String cinNumber;
	private String dateOfIncorporation;
	private String pan;
	private String coordinatorName;
	private String iFullName;
	private String tanNumber;
	private String tinNumber;
	
	@JsonProperty("companyName")
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	@JsonProperty("companyName2")
	public String getCompanyName2() {
		return companyName2;
	}
	public void setCompanyName2(String companyName2) {
		this.companyName2 = companyName2;
	}
	@JsonProperty("companyName3")
	public String getCompanyName3() {
		return companyName3;
	}
	public void setCompanyName3(String companyName3) {
		this.companyName3 = companyName3;
	}
	@JsonProperty("companyPhone")
	public String getCompanyPhone() {
		return companyPhone;
	}
	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}
	@JsonProperty("aadhar")
	public String getAadhar() {
		return aadhar;
	}
	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}
	@JsonProperty("pocNumber")
	public String getPocNumber() {
		return pocNumber;
	}
	public void setPocNumber(String pocNumber) {
		this.pocNumber = pocNumber;
	}
	@JsonProperty("pocName")
	public String getPocName() {
		return pocName;
	}
	public void setPocName(String pocName) {
		this.pocName = pocName;
	}
	@JsonProperty("cinNumber")
	public String getCinNumber() {
		return cinNumber;
	}
	public void setCinNumber(String cinNumber) {
		this.cinNumber = cinNumber;
	}
	@JsonProperty("dateOfIncorporation")
	public String getDateOfIncorporation() {
		return dateOfIncorporation;
	}
	public void setDateOfIncorporation(String dateOfIncorporation) {
		this.dateOfIncorporation = dateOfIncorporation;
	}
	@JsonProperty("pan")
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	@JsonProperty("coordinatorName")
	public String getCoordinatorName() {
		return coordinatorName;
	}
	public void setCoordinatorName(String coordinatorName) {
		this.coordinatorName = coordinatorName;
	}
	@JsonProperty("iFullName")
	public String getiFullName() {
		return iFullName;
	}
	public void setiFullName(String iFullName) {
		this.iFullName = iFullName;
	}
	@JsonProperty("tanNumber")
	public String getTanNumber() {
		return tanNumber;
	}
	public void setTanNumber(String tanNumber) {
		this.tanNumber = tanNumber;
	}
	@JsonProperty("tinNumber")
	public String getTinNumber() {
		return tinNumber;
	}
	public void setTinNumber(String tinNumber) {
		this.tinNumber = tinNumber;
	}
	
	
}
