package com.neo.aggregator.bank.equitas.kyc;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Auth {

	
	private String txnValue;
	
	private String txn;

	@XmlValue
	public String getTxnValue() {
		return txnValue;
	}

	public void setTxnValue(String txnValue) {
		this.txnValue = txnValue;
	}

	@XmlAttribute(name = "txn")
	public String getTxn() {
		return txn;
	}

	public void setTxn(String txn) {
		this.txn = txn;
	}

}
