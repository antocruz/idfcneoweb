package com.neo.aggregator.bank.equitas.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("createDigiCustLeadwitheKYCRep")
public class CreateDigiCustLeadwitheKYCResDto {

	@JsonProperty("msgHdr")
	protected CreateDigiCustLeadwitheKYCResDto.Header msgHdr;
	@JsonProperty("msgBdy")
	protected CreateDigiCustLeadwitheKYCResDto.Body msgBdy;

	public CreateDigiCustLeadwitheKYCResDto.Header getMsgHdr() {
		return msgHdr;
	}

	public void setMsgHdr(CreateDigiCustLeadwitheKYCResDto.Header msgHdr) {
		this.msgHdr = msgHdr;
	}

	public CreateDigiCustLeadwitheKYCResDto.Body getMsgBdy() {
		return msgBdy;
	}

	public void setMsgBdy(CreateDigiCustLeadwitheKYCResDto.Body msgBdy) {
		this.msgBdy = msgBdy;
	}

	public static class Header {
		@JsonProperty(value = "rslt")
		protected String rslt;

		@JsonProperty(value = "error")
		protected List<Error> error;

		public List<Error> getError() {
			return error;
		}

		public void setError(List<Error> error) {
			this.error = error;
		}

		public String getRslt() {
			return rslt;
		}

		public void setRslt(String rslt) {
			this.rslt = rslt;
		}

		public static class Error {

			@JsonProperty(value = "cd")
			protected String cd;
			@JsonProperty(value = "rsn")
			protected String rsn;

			public String getCd() {
				return cd;
			}

			public void setCd(String cd) {
				this.cd = cd;
			}

			public String getRsn() {
				return rsn;
			}

			public void setRsn(String rsn) {
				this.rsn = rsn;
			}
		}
	}

	public static class Body {

		@JsonProperty(value = "leadDetails")
		protected LeadDetails leadDetails;

		@JsonProperty(value = "KycDetails")
		protected KycDetails KycDetails;
		
		@JsonProperty(value = "panLoanInformationList")
		protected List<PanLoanInformationList> panLoanInformationList;
		
		@JsonProperty(value = "dedupIndividualCustomerarray")
		protected List<DedupIndividualCustomerArray> dedupIndividualCustomerarray;
		
		@JsonProperty(value = "dedupCorporateCustomerarray")
		protected List<DedupCorporateCustomerarray> dedupCorporateCustomerarray;

		public List<PanLoanInformationList> getPanLoanInformationList() {
			return panLoanInformationList;
		}

		public void setPanLoanInformationList(List<PanLoanInformationList> panLoanInformationList) {
			this.panLoanInformationList = panLoanInformationList;
		}

		public List<DedupIndividualCustomerArray> getDedupIndividualCustomerarray() {
			return dedupIndividualCustomerarray;
		}

		public void setDedupIndividualCustomerarray(List<DedupIndividualCustomerArray> dedupIndividualCustomerarray) {
			this.dedupIndividualCustomerarray = dedupIndividualCustomerarray;
		}

		public List<DedupCorporateCustomerarray> getDedupCorporateCustomerarray() {
			return dedupCorporateCustomerarray;
		}

		public void setDedupCorporateCustomerarray(List<DedupCorporateCustomerarray> dedupCorporateCustomerarray) {
			this.dedupCorporateCustomerarray = dedupCorporateCustomerarray;
		}

		public LeadDetails getLeadDetails() {
			return leadDetails;
		}

		public void setLeadDetails(LeadDetails leadDetails) {
			this.leadDetails = leadDetails;
		}

		public KycDetails getKycDetails() {
			return KycDetails;
		}

		public void setKycDetails(KycDetails kycDetails) {
			KycDetails = kycDetails;
		}
	
		public static class LeadDetails {

			@JsonProperty(value = "customerLeadID")
			protected String customerLeadID;

			public String getCustomerLeadID() {
				return customerLeadID;
			}

			public void setCustomerLeadID(String customerLeadID) {
				this.customerLeadID = customerLeadID;
			}

		}

		public static class KycDetails {

			@JsonProperty(value = "eKYCXMLStringRepPayload")
			protected String eKYCXMLStringRepPayload;

			public String geteKYCXMLStringRepPayload() {
				return eKYCXMLStringRepPayload;
			}

			public void seteKYCXMLStringRepPayload(String eKYCXMLStringRepPayload) {
				this.eKYCXMLStringRepPayload = eKYCXMLStringRepPayload;
			}

		}

		public static class PanLoanInformationList {

			@JsonProperty(value = "NSDLFirstName")
			protected String NSDLFirstName;
			@JsonProperty(value = "NSDLLastName")
			protected String NSDLLastName;
			@JsonProperty(value = "NSDLMiddleName")
			protected String NSDLMiddleName;
			@JsonProperty(value = "NSDLpanTitle")
			protected String NSDLpanTitle;
			@JsonProperty(value = "allowToLeadSubmission")
			protected String allowToLeadSubmission;
			@JsonProperty(value = "customerLeadID")
			protected String customerLeadID;
			@JsonProperty(value = "nameonCard")
			protected String nameonCard;
			@JsonProperty(value = "panNSDLStatus")
			protected String panNSDLStatus;
			@JsonProperty(value = "panNSDLStatusDesc")
			protected String panNSDLStatusDesc;
			@JsonProperty(value = "panNumber")
			protected String panNumber;
			@JsonProperty(value = "panVerified")
			protected String panVerified;
			@JsonProperty(value = "wizardFirstName")
			protected String wizardFirstName;
			@JsonProperty(value = "wizardLastName")
			protected String wizardLastName;
			@JsonProperty(value = "wizardMiddleName")
			protected String wizardMiddleName;
			@JsonProperty(value = "wizardTitle")
			protected String wizardTitle;
			@JsonProperty(value = "wizardTitleName")
			protected String wizardTitleName;
			
			public String getNSDLFirstName() {
				return NSDLFirstName;
			}
			public void setNSDLFirstName(String nSDLFirstName) {
				NSDLFirstName = nSDLFirstName;
			}
			public String getNSDLLastName() {
				return NSDLLastName;
			}
			public void setNSDLLastName(String nSDLLastName) {
				NSDLLastName = nSDLLastName;
			}
			public String getNSDLMiddleName() {
				return NSDLMiddleName;
			}
			public void setNSDLMiddleName(String nSDLMiddleName) {
				NSDLMiddleName = nSDLMiddleName;
			}
			public String getNSDLpanTitle() {
				return NSDLpanTitle;
			}
			public void setNSDLpanTitle(String nSDLpanTitle) {
				NSDLpanTitle = nSDLpanTitle;
			}
			public String getAllowToLeadSubmission() {
				return allowToLeadSubmission;
			}
			public void setAllowToLeadSubmission(String allowToLeadSubmission) {
				this.allowToLeadSubmission = allowToLeadSubmission;
			}
			public String getCustomerLeadID() {
				return customerLeadID;
			}
			public void setCustomerLeadID(String customerLeadID) {
				this.customerLeadID = customerLeadID;
			}
			public String getNameonCard() {
				return nameonCard;
			}
			public void setNameonCard(String nameonCard) {
				this.nameonCard = nameonCard;
			}
			public String getPanNSDLStatus() {
				return panNSDLStatus;
			}
			public void setPanNSDLStatus(String panNSDLStatus) {
				this.panNSDLStatus = panNSDLStatus;
			}
			public String getPanNSDLStatusDesc() {
				return panNSDLStatusDesc;
			}
			public void setPanNSDLStatusDesc(String panNSDLStatusDesc) {
				this.panNSDLStatusDesc = panNSDLStatusDesc;
			}
			public String getPanNumber() {
				return panNumber;
			}
			public void setPanNumber(String panNumber) {
				this.panNumber = panNumber;
			}
			public String getPanVerified() {
				return panVerified;
			}
			public void setPanVerified(String panVerified) {
				this.panVerified = panVerified;
			}
			public String getWizardFirstName() {
				return wizardFirstName;
			}
			public void setWizardFirstName(String wizardFirstName) {
				this.wizardFirstName = wizardFirstName;
			}
			public String getWizardLastName() {
				return wizardLastName;
			}
			public void setWizardLastName(String wizardLastName) {
				this.wizardLastName = wizardLastName;
			}
			public String getWizardMiddleName() {
				return wizardMiddleName;
			}
			public void setWizardMiddleName(String wizardMiddleName) {
				this.wizardMiddleName = wizardMiddleName;
			}
			public String getWizardTitle() {
				return wizardTitle;
			}
			public void setWizardTitle(String wizardTitle) {
				this.wizardTitle = wizardTitle;
			}
			public String getWizardTitleName() {
				return wizardTitleName;
			}
			public void setWizardTitleName(String wizardTitleName) {
				this.wizardTitleName = wizardTitleName;
			}

		}
		
		public static class DedupIndividualCustomerArray {

			@JsonProperty(value = "ucic")
			protected String ucic;
			@JsonProperty(value = "fullName")
			protected String fullName;
			@JsonProperty(value = "dob")
			protected String dob;
			@JsonProperty(value = "mobile")
			protected String mobile;
			@JsonProperty(value = "pan")
			protected String pan;
			@JsonProperty(value = "aadhar")
			protected String aadhar;
			@JsonProperty(value = "passport")
			protected String passport;
			@JsonProperty(value = "voterID")
			protected String voterID;
			@JsonProperty(value = "IDProof")
			protected String iDProof;
			@JsonProperty(value = "matchedCriteria")
			protected String matchedCriteria;
			@JsonProperty(value = "isProbableMatch")
			protected String isProbableMatch;
			@JsonProperty(value = "isExactMatch")
			protected String isExactMatch;
			@JsonProperty(value = "ForeignerPassport")
			protected String foreignerPassport;
			@JsonProperty(value = "DocumentPassport")
			protected String documentPassport;
			@JsonProperty(value = "DocumentVoterID")
			protected String documentVoterID;
			@JsonProperty(value = "DocumentIDProof")
			protected String documentIDProof;
			@JsonProperty(value = "DocumentForeignerPassport")
			protected String documentForeignerPassport;
			@JsonProperty(value = "ProspectVoterID")
			protected String prospectVoterID;
			@JsonProperty(value = "ProspectPassport")
			protected String prospectPassport;
			@JsonProperty(value = "ProspectdrivingLicense")
			protected String prospectdrivingLicense;
			@JsonProperty(value = "ProspectCKYCNumber")
			protected String prospectCKYCNumber;
			@JsonProperty(value = "DocumentCKYCNumber")
			protected String documentCKYCNumber;
			@JsonProperty(value = "DocumentdrivingLicense")
			protected String documentdrivingLicense;

			public String getUcic() {
				return ucic;
			}

			public void setUcic(String ucic) {
				this.ucic = ucic;
			}

			public String getFullName() {
				return fullName;
			}

			public void setFullName(String fullName) {
				this.fullName = fullName;
			}

			public String getDob() {
				return dob;
			}

			public void setDob(String dob) {
				this.dob = dob;
			}

			public String getMobile() {
				return mobile;
			}

			public void setMobile(String mobile) {
				this.mobile = mobile;
			}

			public String getPan() {
				return pan;
			}

			public void setPan(String pan) {
				this.pan = pan;
			}

			public String getAadhar() {
				return aadhar;
			}

			public void setAadhar(String aadhar) {
				this.aadhar = aadhar;
			}

			public String getPassport() {
				return passport;
			}

			public void setPassport(String passport) {
				this.passport = passport;
			}

			public String getVoterID() {
				return voterID;
			}

			public void setVoterID(String voterID) {
				this.voterID = voterID;
			}

			public String getiDProof() {
				return iDProof;
			}

			public void setiDProof(String iDProof) {
				this.iDProof = iDProof;
			}

			public String getMatchedCriteria() {
				return matchedCriteria;
			}

			public void setMatchedCriteria(String matchedCriteria) {
				this.matchedCriteria = matchedCriteria;
			}

			public String getIsProbableMatch() {
				return isProbableMatch;
			}

			public void setIsProbableMatch(String isProbableMatch) {
				this.isProbableMatch = isProbableMatch;
			}

			public String getIsExactMatch() {
				return isExactMatch;
			}

			public void setIsExactMatch(String isExactMatch) {
				this.isExactMatch = isExactMatch;
			}

			public String getForeignerPassport() {
				return foreignerPassport;
			}

			public void setForeignerPassport(String foreignerPassport) {
				this.foreignerPassport = foreignerPassport;
			}

			public String getDocumentPassport() {
				return documentPassport;
			}

			public void setDocumentPassport(String documentPassport) {
				this.documentPassport = documentPassport;
			}

			public String getDocumentVoterID() {
				return documentVoterID;
			}

			public void setDocumentVoterID(String documentVoterID) {
				this.documentVoterID = documentVoterID;
			}

			public String getDocumentIDProof() {
				return documentIDProof;
			}

			public void setDocumentIDProof(String documentIDProof) {
				this.documentIDProof = documentIDProof;
			}

			public String getDocumentForeignerPassport() {
				return documentForeignerPassport;
			}

			public void setDocumentForeignerPassport(String documentForeignerPassport) {
				this.documentForeignerPassport = documentForeignerPassport;
			}

			public String getProspectVoterID() {
				return prospectVoterID;
			}

			public void setProspectVoterID(String prospectVoterID) {
				this.prospectVoterID = prospectVoterID;
			}

			public String getProspectPassport() {
				return prospectPassport;
			}

			public void setProspectPassport(String prospectPassport) {
				this.prospectPassport = prospectPassport;
			}

			public String getProspectdrivingLicense() {
				return prospectdrivingLicense;
			}

			public void setProspectdrivingLicense(String prospectdrivingLicense) {
				this.prospectdrivingLicense = prospectdrivingLicense;
			}

			public String getProspectCKYCNumber() {
				return prospectCKYCNumber;
			}

			public void setProspectCKYCNumber(String prospectCKYCNumber) {
				this.prospectCKYCNumber = prospectCKYCNumber;
			}

			public String getDocumentCKYCNumber() {
				return documentCKYCNumber;
			}

			public void setDocumentCKYCNumber(String documentCKYCNumber) {
				this.documentCKYCNumber = documentCKYCNumber;
			}

			public String getDocumentdrivingLicense() {
				return documentdrivingLicense;
			}

			public void setDocumentdrivingLicense(String documentdrivingLicense) {
				this.documentdrivingLicense = documentdrivingLicense;
			}

		}

		public static class DedupCorporateCustomerarray{
			
		}
	}

}
