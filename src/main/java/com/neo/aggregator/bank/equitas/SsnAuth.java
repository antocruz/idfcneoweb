package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SsnAuth {

	private String usrTkn;
	private String usrNm;
	private String usrPwd;

	@JsonProperty("usrTkn")
	public String getUsrTkn() {
		return usrTkn;
	}

	public void setUsrTkn(String usrTkn) {
		this.usrTkn = usrTkn;
	}

	@JsonProperty("usrNm")
	public String getUsrNm() {
		return usrNm;
	}

	public void setUsrNm(String usrNm) {
		this.usrNm = usrNm;
	}

	@JsonProperty("usrPwd")
	public String getUsrPwd() {
		return usrPwd;
	}

	public void setUsrPwd(String usrPwd) {
		this.usrPwd = usrPwd;
	}

}
