package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.FetchAccountLeadMessageBody;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.dto.DigiAccountRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("fetchDigiAccountLeadReq")
public class FetchDigiAccountLeadReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;

	protected LeadMessageHeader leadMessageHeader;
	protected FetchAccountLeadMessageBody fetchAccountLeadMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public FetchAccountLeadMessageBody getFetchAccountLeadMessageBody() {
		return fetchAccountLeadMessageBody;
	}

	public void setFetchAccountLeadMessageBody(FetchAccountLeadMessageBody fetchAccountLeadMessageBody) {
		this.fetchAccountLeadMessageBody = fetchAccountLeadMessageBody;
	}

	public FetchDigiAccountLeadReqDto build(DigiAccountRequestDto request) throws NeoException {

		FetchDigiAccountLeadReqDto requestDto = null;

		try {

			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);

			FetchAccountLeadMessageBody fetchAccountLeadMessage = equitasCommonService
					.buildFetchAccountLeadMessageBody(request);

			requestDto = FetchDigiAccountLeadReqDto.builder().fetchAccountLeadMessageBody(fetchAccountLeadMessage)
					.leadMessageHeader(leadMessage).build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}

}
