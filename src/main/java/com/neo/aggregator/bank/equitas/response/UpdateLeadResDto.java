package com.neo.aggregator.bank.equitas.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("updateDigiCustLeadRep")
public class UpdateLeadResDto {

	@JsonProperty("msgHdr")
	protected UpdateLeadResDto.Header msgHdr;
	@JsonProperty("msgBdy")
	protected UpdateLeadResDto.Body msgBdy;

	public UpdateLeadResDto.Header getMsgHdr() {
		return msgHdr;
	}

	public void setMsgHdr(UpdateLeadResDto.Header msgHdr) {
		this.msgHdr = msgHdr;
	}

	public UpdateLeadResDto.Body getMsgBdy() {
		return msgBdy;
	}

	public void setMsgBdy(UpdateLeadResDto.Body msgBdy) {
		this.msgBdy = msgBdy;
	}

	public static class Header {
		@JsonProperty(value = "rslt")
		protected String rslt;

		@JsonProperty(value = "error")
		protected List<Error> error;

		public List<Error> getError() {
			return error;
		}

		public void setError(List<Error> error) {
			this.error = error;
		}

		public String getRslt() {
			return rslt;
		}

		public void setRslt(String rslt) {
			this.rslt = rslt;
		}

		public static class Error {

			@JsonProperty(value = "cd")
			protected String cd;
			@JsonProperty(value = "rsn")
			protected String rsn;

			public String getCd() {
				return cd;
			}

			public void setCd(String cd) {
				this.cd = cd;
			}

			public String getRsn() {
				return rsn;
			}

			public void setRsn(String rsn) {
				this.rsn = rsn;
			}
		}
	}

	public static class Body {

		@JsonProperty(value = "custLeadID")
		protected String customerLeadID;

		@JsonProperty(value = "addressDetails")
		protected AddressDetailRes[] addressDetailRes;
		
		
		public AddressDetailRes[] getAddressDetailRes() {
			return addressDetailRes;
		}

		public void setAddressDetailRes(AddressDetailRes[] addressDetailRes) {
			this.addressDetailRes = addressDetailRes;
		}

		public String getCustomerLeadID() {
			return customerLeadID;
		}

		public void setCustomerLeadID(String customerLeadID) {
			this.customerLeadID = customerLeadID;
		}
		
		public static class AddressDetailRes {

			@JsonProperty(value = "addressID")
			protected String addressID;
			@JsonProperty(value = "addressType")
			protected String addressType;
			
			public String getAddressID() {
				return addressID;
			}
			public void setAddressID(String addressID) {
				this.addressID = addressID;
			}
			public String getAddressType() {
				return addressType;
			}
			public void setAddressType(String addressType) {
				this.addressType = addressType;
			}

			
		}

	}

}
