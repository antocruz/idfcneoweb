package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DocRefId {

	private String idTp;
	private int id;

	@JsonProperty("idTp")
	public String getIdTp() {
		return idTp;
	}

	public void setIdTp(String idTp) {
		this.idTp = idTp;
	}

	@JsonProperty("id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
