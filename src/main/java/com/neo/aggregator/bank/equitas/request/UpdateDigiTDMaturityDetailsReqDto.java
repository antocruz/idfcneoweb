package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.UpdateDigiTDMaturityDetailsMessageBody;
import com.neo.aggregator.bank.equitas.dto.DigiAccountRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("updateDigiTDMaturityDetailsRequest")
public class UpdateDigiTDMaturityDetailsReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;
	
	protected LeadMessageHeader leadMessageHeader;
	protected UpdateDigiTDMaturityDetailsMessageBody updateDigiTDMaturityDetailsMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public UpdateDigiTDMaturityDetailsMessageBody getUpdateDigiTDMaturityDetailsMessageBody() {
		return updateDigiTDMaturityDetailsMessageBody;
	}

	public void setUpdateDigiTDMaturityDetailsMessageBody(
			UpdateDigiTDMaturityDetailsMessageBody updateDigiTDMaturityDetailsMessageBody) {
		this.updateDigiTDMaturityDetailsMessageBody = updateDigiTDMaturityDetailsMessageBody;
	}
	
	public UpdateDigiTDMaturityDetailsReqDto build(DigiAccountRequestDto request) throws NeoException {

		UpdateDigiTDMaturityDetailsReqDto requestDto = null;

		try {
			
			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);
			
			
			UpdateDigiTDMaturityDetailsMessageBody updateDigiTDMaturityDetailsMessage = equitasCommonService.buildUpdateDigiTDMaturityDetailsMessageBody(request);
			
			requestDto = UpdateDigiTDMaturityDetailsReqDto.builder().updateDigiTDMaturityDetailsMessageBody(updateDigiTDMaturityDetailsMessage).leadMessageHeader(leadMessage)
					.build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}


}
