package com.neo.aggregator.bank.equitas.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("redeemDigiDepositResponse")
public class RedeemDigiTDMaturityDetailsResDto {

	@JsonProperty("msgHdr")
	protected RedeemDigiTDMaturityDetailsResDto.Header msgHdr;
	@JsonProperty("msgBdy")
	protected RedeemDigiTDMaturityDetailsResDto.Body msgBdy;

	public RedeemDigiTDMaturityDetailsResDto.Header getMsgHdr() {
		return msgHdr;
	}

	public void setMsgHdr(RedeemDigiTDMaturityDetailsResDto.Header msgHdr) {
		this.msgHdr = msgHdr;
	}

	public RedeemDigiTDMaturityDetailsResDto.Body getMsgBdy() {
		return msgBdy;
	}

	public void setMsgBdy(RedeemDigiTDMaturityDetailsResDto.Body msgBdy) {
		this.msgBdy = msgBdy;
	}

	public static class Header {
		@JsonProperty(value = "rslt")
		protected String rslt;

		@JsonProperty(value = "error")
		protected List<Error> error;

		public List<Error> getError() {
			return error;
		}

		public void setError(List<Error> error) {
			this.error = error;
		}

		public String getRslt() {
			return rslt;
		}

		public void setRslt(String rslt) {
			this.rslt = rslt;
		}

		public static class Error {

			@JsonProperty(value = "cd")
			protected String cd;
			@JsonProperty(value = "rsn")
			protected String rsn;

			public String getCd() {
				return cd;
			}

			public void setCd(String cd) {
				this.cd = cd;
			}

			public String getRsn() {
				return rsn;
			}

			public void setRsn(String rsn) {
				this.rsn = rsn;
			}
		}
	}

	public static class Body {

		@JsonProperty(value = "pstngDt")
		protected String pstngDt;
		@JsonProperty(value = "netRdmptnAmt")
		protected String netRdmptnAmt;
		@JsonProperty(value = "depstAmt")
		protected String depstAmt;
		@JsonProperty(value = "rdmptnAmt")
		protected String rdmptnAmt;
		@JsonProperty(value = "prncplDue")
		protected String prncplDue;
		@JsonProperty(value = "prncplUncllctd")
		protected String prncplUncllctd;
		@JsonProperty(value = "prncplUncllctdPMI")
		protected String prncplUncllctdPMI;
		@JsonProperty(value = "intrstDue")
		protected String intrstDue;
		@JsonProperty(value = "intrstUncllctd")
		protected String intrstUncllctd;
		@JsonProperty(value = "intrstUncllctdPMI")
		protected String intrstUncllctdPMI;
		@JsonProperty(value = "pnltyIntrst")
		protected String pnltyIntrst;
		@JsonProperty(value = "deducdIntrst")
		protected String deducdIntrst;
		@JsonProperty(value = "prmryTxAmt")
		protected String prmryTxAmt;
		@JsonProperty(value = "secTxAmt")
		protected String secTxAmt;
		@JsonProperty(value = "prtlRdmptnCntr")
		protected String prtlRdmptnCntr;
		@JsonProperty(value = "rdmptnToAcct")
		protected String rdmptnToAcct;

		public String getPstngDt() {
			return pstngDt;
		}

		public void setPstngDt(String pstngDt) {
			this.pstngDt = pstngDt;
		}

		public String getNetRdmptnAmt() {
			return netRdmptnAmt;
		}

		public void setNetRdmptnAmt(String netRdmptnAmt) {
			this.netRdmptnAmt = netRdmptnAmt;
		}

		public String getDepstAmt() {
			return depstAmt;
		}

		public void setDepstAmt(String depstAmt) {
			this.depstAmt = depstAmt;
		}

		public String getRdmptnAmt() {
			return rdmptnAmt;
		}

		public void setRdmptnAmt(String rdmptnAmt) {
			this.rdmptnAmt = rdmptnAmt;
		}

		public String getPrncplDue() {
			return prncplDue;
		}

		public void setPrncplDue(String prncplDue) {
			this.prncplDue = prncplDue;
		}

		public String getPrncplUncllctd() {
			return prncplUncllctd;
		}

		public void setPrncplUncllctd(String prncplUncllctd) {
			this.prncplUncllctd = prncplUncllctd;
		}

		public String getPrncplUncllctdPMI() {
			return prncplUncllctdPMI;
		}

		public void setPrncplUncllctdPMI(String prncplUncllctdPMI) {
			this.prncplUncllctdPMI = prncplUncllctdPMI;
		}

		public String getIntrstDue() {
			return intrstDue;
		}

		public void setIntrstDue(String intrstDue) {
			this.intrstDue = intrstDue;
		}

		public String getIntrstUncllctd() {
			return intrstUncllctd;
		}

		public void setIntrstUncllctd(String intrstUncllctd) {
			this.intrstUncllctd = intrstUncllctd;
		}

		public String getIntrstUncllctdPMI() {
			return intrstUncllctdPMI;
		}

		public void setIntrstUncllctdPMI(String intrstUncllctdPMI) {
			this.intrstUncllctdPMI = intrstUncllctdPMI;
		}

		public String getPnltyIntrst() {
			return pnltyIntrst;
		}

		public void setPnltyIntrst(String pnltyIntrst) {
			this.pnltyIntrst = pnltyIntrst;
		}

		public String getDeducdIntrst() {
			return deducdIntrst;
		}

		public void setDeducdIntrst(String deducdIntrst) {
			this.deducdIntrst = deducdIntrst;
		}

		public String getPrmryTxAmt() {
			return prmryTxAmt;
		}

		public void setPrmryTxAmt(String prmryTxAmt) {
			this.prmryTxAmt = prmryTxAmt;
		}

		public String getSecTxAmt() {
			return secTxAmt;
		}

		public void setSecTxAmt(String secTxAmt) {
			this.secTxAmt = secTxAmt;
		}

		public String getPrtlRdmptnCntr() {
			return prtlRdmptnCntr;
		}

		public void setPrtlRdmptnCntr(String prtlRdmptnCntr) {
			this.prtlRdmptnCntr = prtlRdmptnCntr;
		}

		public String getRdmptnToAcct() {
			return rdmptnToAcct;
		}

		public void setRdmptnToAcct(String rdmptnToAcct) {
			this.rdmptnToAcct = rdmptnToAcct;
		}

	}

}
