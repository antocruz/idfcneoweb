package com.neo.aggregator.bank.equitas;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddDocRq {

	private String docTp;
	private String docSbCtgry;
	private String docNm;
	private int docCtgryCd;
	private String docCatg;
	private int docTypCd;
	private int docSbCtgryCd;
	private String flLoc;
	private String docCmnts;
	private String bsPyld;

	private List<DocRefId> docRefId;

	@JsonProperty("docTp")
	public String getDocTp() {
		return docTp;
	}

	public void setDocTp(String docTp) {
		this.docTp = docTp;
	}

	@JsonProperty("docSbCtgry")
	public String getDocSbCtgry() {
		return docSbCtgry;
	}

	public void setDocSbCtgry(String docSbCtgry) {
		this.docSbCtgry = docSbCtgry;
	}

	@JsonProperty("docNm")
	public String getDocNm() {
		return docNm;
	}

	public void setDocNm(String docNm) {
		this.docNm = docNm;
	}

	@JsonProperty("docCtgryCd")
	public int getDocCtgryCd() {
		return docCtgryCd;
	}

	public void setDocCtgryCd(int docCtgryCd) {
		this.docCtgryCd = docCtgryCd;
	}

	@JsonProperty("docCatg")
	public String getDocCatg() {
		return docCatg;
	}

	public void setDocCatg(String docCatg) {
		this.docCatg = docCatg;
	}

	@JsonProperty("docTypCd")
	public int getDocTypCd() {
		return docTypCd;
	}

	public void setDocTypCd(int docTypCd) {
		this.docTypCd = docTypCd;
	}

	@JsonProperty("docSbCtgryCd")
	public int getDocSbCtgryCd() {
		return docSbCtgryCd;
	}

	public void setDocSbCtgryCd(int docSbCtgryCd) {
		this.docSbCtgryCd = docSbCtgryCd;
	}

	@JsonProperty("flLoc")
	public String getFlLoc() {
		return flLoc;
	}

	public void setFlLoc(String flLoc) {
		this.flLoc = flLoc;
	}

	@JsonProperty("docCmnts")
	public String getDocCmnts() {
		return docCmnts;
	}

	public void setDocCmnts(String docCmnts) {
		this.docCmnts = docCmnts;
	}

	@JsonProperty("bsPyld")
	public String getBsPyld() {
		return bsPyld;
	}

	public void setBsPyld(String bsPyld) {
		this.bsPyld = bsPyld;
	}

	@JsonProperty("docRefId")
	public List<DocRefId> getDocRefId() {
		return docRefId;
	}

	public void setDocRefId(List<DocRefId> docRefId) {
		this.docRefId = docRefId;
	}

	
}
