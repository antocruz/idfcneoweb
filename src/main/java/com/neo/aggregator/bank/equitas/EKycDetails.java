package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EKycDetails {

	private String eKYCXMLStringReqPayload;

	@JsonProperty("eKYCXMLStringReqPayload")
	public String geteKYCXMLStringReqPayload() {
		return eKYCXMLStringReqPayload;
	}

	public void seteKYCXMLStringReqPayload(String eKYCXMLStringReqPayload) {
		this.eKYCXMLStringReqPayload = eKYCXMLStringReqPayload;
	}

}
