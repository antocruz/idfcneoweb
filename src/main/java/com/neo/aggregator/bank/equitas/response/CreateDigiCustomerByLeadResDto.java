package com.neo.aggregator.bank.equitas.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("createDigiCustomerByLeadRep")
public class CreateDigiCustomerByLeadResDto {
	
	@JsonProperty("msgHdr")
	protected CreateDigiCustomerByLeadResDto.Header msgHdr;
	@JsonProperty("msgBdy")
	protected CreateDigiCustomerByLeadResDto.Body msgBdy;
	
	public CreateDigiCustomerByLeadResDto.Header getHeader() {
		return msgHdr;
	}

	public void setHeader(CreateDigiCustomerByLeadResDto.Header msgHdr) {
		this.msgHdr = msgHdr;
	}

	public CreateDigiCustomerByLeadResDto.Body getBody() {
		return msgBdy;
	}

	public void setBody(CreateDigiCustomerByLeadResDto.Body msgBdy) {
		this.msgBdy = msgBdy;
	}
	
	public static class Header {
		@JsonProperty(value = "rslt")
		protected String rslt;
		
		@JsonProperty(value = "error")
		protected List<Error> error;
		
		public List<Error> getError() {
			return error;
		}

		public void setError(List<Error> error) {
			this.error = error;
		}

		public String getRslt() {
			return rslt;
		}

		public void setRslt(String rslt) {
			this.rslt = rslt;
		}
		
		public static class Error {

			@JsonProperty(value = "cd")
			protected String cd;
			@JsonProperty(value = "rsn")
			protected String rsn;
			
			public String getCd() {
				return cd;
			}
			public void setCd(String cd) {
				this.cd = cd;
			}
			public String getRsn() {
				return rsn;
			}
			public void setRsn(String rsn) {
				this.rsn = rsn;
			}
			
			
		}
	}
	
	public static class Body {

		@JsonProperty(value = "custID")
		protected String custID;

		public String getCustID() {
			return custID;
		}

		public void setCustID(String custID) {
			this.custID = custID;
		}

	}

}
