package com.neo.aggregator.bank.equitas.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("submitDigiAccountLeadRep")
public class SubmitDigiAccountLeadResDto {

	@JsonProperty("msgHdr")
	protected SubmitDigiAccountLeadResDto.Header msgHdr;
	@JsonProperty("msgBdy")
	protected SubmitDigiAccountLeadResDto.Body msgBdy;
	
	public SubmitDigiAccountLeadResDto.Header getMsgHdr() {
		return msgHdr;
	}

	public void setMsgHdr(SubmitDigiAccountLeadResDto.Header msgHdr) {
		this.msgHdr = msgHdr;
	}

	public SubmitDigiAccountLeadResDto.Body getMsgBdy() {
		return msgBdy;
	}

	public void setMsgBdy(SubmitDigiAccountLeadResDto.Body msgBdy) {
		this.msgBdy = msgBdy;
	}
	
	public static class Header {
		@JsonProperty(value = "rslt")
		protected String rslt;
		
		@JsonProperty(value = "error")
		protected List<Error> error;
		
		public List<Error> getError() {
			return error;
		}

		public void setError(List<Error> error) {
			this.error = error;
		}

		public String getRslt() {
			return rslt;
		}

		public void setRslt(String rslt) {
			this.rslt = rslt;
		}
		
		public static class Error {
			
			@JsonProperty(value = "cd")
			protected String cd;
			@JsonProperty(value = "rsn")
			protected String rsn;
			
			public String getCd() {
				return cd;
			}
			public void setCd(String cd) {
				this.cd = cd;
			}
			public String getRsn() {
				return rsn;
			}
			public void setRsn(String rsn) {
				this.rsn = rsn;
			}
		}
	}
	
	public static class Body {

		@JsonProperty(value = "accountLeadID")
		protected String accountLeadID;

		public String getAccountLeadID() {
			return accountLeadID;
		}

		public void setAccountLeadID(String accountLeadID) {
			this.accountLeadID = accountLeadID;
		}

	}

}

