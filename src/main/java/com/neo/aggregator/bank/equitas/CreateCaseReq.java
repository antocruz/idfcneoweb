package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateCaseReq {

	@JsonProperty("sbCtgry")
	private String sbCtgry;

	@JsonProperty("ctgry")
	private String ctgry;

	@JsonProperty("ctgry")
	private String sbSbCtgry;

	@JsonProperty("brnch")
	private String brnch;

	@JsonProperty("prrty")
	private String prrty;

	@JsonProperty("sbjct")
	private String sbjct;

	@JsonProperty("cstmFlds")
	private CustomFields cstmFlds;

	public String getSbCtgry() {
		return sbCtgry;
	}

	public void setSbCtgry(String sbCtgry) {
		this.sbCtgry = sbCtgry;
	}

	public String getCtgry() {
		return ctgry;
	}

	public void setCtgry(String ctgry) {
		this.ctgry = ctgry;
	}

	public String getSbSbCtgry() {
		return sbSbCtgry;
	}

	public void setSbSbCtgry(String sbSbCtgry) {
		this.sbSbCtgry = sbSbCtgry;
	}

	public String getBrnch() {
		return brnch;
	}

	public void setBrnch(String brnch) {
		this.brnch = brnch;
	}

	public String getPrrty() {
		return prrty;
	}

	public void setPrrty(String prrty) {
		this.prrty = prrty;
	}

	public String getSbjct() {
		return sbjct;
	}

	public void setSbjct(String sbjct) {
		this.sbjct = sbjct;
	}

	public CustomFields getCstmFlds() {
		return cstmFlds;
	}

	public void setCstmFlds(CustomFields cstmFlds) {
		this.cstmFlds = cstmFlds;
	}
}
