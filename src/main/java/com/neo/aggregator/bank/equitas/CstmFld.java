package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CstmFld {

	@JsonProperty("vlu")
	private String vlu;
	@JsonProperty("nm")
	private String nm;

	public String getVlu() {
		return vlu;
	}

	public void setVlu(String vlu) {
		this.vlu = vlu;
	}

	public String getNm() {
		return nm;
	}

	public void setNm(String nm) {
		this.nm = nm;
	}

}
