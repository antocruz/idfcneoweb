package com.neo.aggregator.bank.equitas.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("fetchDigiAccountLeadRep")
public class FetchDigiAccountLeadResDto {

	@JsonProperty("msgHdr")
	protected FetchDigiAccountLeadResDto.Header msgHdr;
	@JsonProperty("msgBdy")
	protected FetchDigiAccountLeadResDto.Body msgBdy;

	public FetchDigiAccountLeadResDto.Header getMsgHdr() {
		return msgHdr;
	}

	public void setMsgHdr(FetchDigiAccountLeadResDto.Header msgHdr) {
		this.msgHdr = msgHdr;
	}

	public FetchDigiAccountLeadResDto.Body getMsgBdy() {
		return msgBdy;
	}

	public void setMsgBdy(FetchDigiAccountLeadResDto.Body msgBdy) {
		this.msgBdy = msgBdy;
	}

	public static class Header {
		@JsonProperty(value = "rslt")
		protected String rslt;

		@JsonProperty(value = "error")
		protected List<Error> error;

		public List<Error> getError() {
			return error;
		}

		public void setError(List<Error> error) {
			this.error = error;
		}

		public String getRslt() {
			return rslt;
		}

		public void setRslt(String rslt) {
			this.rslt = rslt;
		}

		public static class Error {

			@JsonProperty(value = "cd")
			protected String cd;
			@JsonProperty(value = "rsn")
			protected String rsn;

			public String getCd() {
				return cd;
			}

			public void setCd(String cd) {
				this.cd = cd;
			}

			public String getRsn() {
				return rsn;
			}

			public void setRsn(String rsn) {
				this.rsn = rsn;
			}
		}
	}

	public static class Body {

		@JsonProperty(value = "FetchAcctLeadResponse")
		protected FetchAcctLeadResponse fetchAcctLeadResponse;

		public FetchAcctLeadResponse getFetchAcctLeadResponse() {
			return fetchAcctLeadResponse;
		}

		public void setFetchAcctLeadResponse(FetchAcctLeadResponse fetchAcctLeadResponse) {
			this.fetchAcctLeadResponse = fetchAcctLeadResponse;
		}

		public static class FetchAcctLeadResponse {

			@JsonProperty(value = "guardianIDForAddress")
			private String guardianIDForAddress;
			@JsonProperty(value = "isPmay")
			private String isPmay;
			@JsonProperty(value = "fetchAccountLeadID")
			private String fetchAccountLeadID;
			@JsonProperty(value = "accountLeadID")
			private String accountLeadID;
			@JsonProperty(value = "accountNumber")
			private String accountNumber;
			@JsonProperty(value = "nomineeIDForAddress")
			private String nomineeIDForAddress;
			@JsonProperty(value = "accountLead")
			private AccountLead accountLead;
			@JsonProperty(value = "customerAccountLeadRelation")
			private List<CustomerAccountLeadRelation> customerAccountLeadRelation;
			@JsonProperty(value = "accountLeadNomineeMap")
			private AccountLeadNomineeMap accountLeadNomineeMap;
			@JsonProperty(value = "accountLeadBeneficialOwner")
			private List<AccountLeadBeneficialOwner> accountLeadBeneficialOwner;
			@JsonProperty(value = "customerDeliverables")
			private List<CustomerDeliverables> customerDeliverables;
			@JsonProperty(value = "customerPreferences")
			private List<CustomerPreferences> customerPreferences;
			@JsonProperty(value = "beneficialOwner")
			private List<BeneficialOwner> beneficialOwner;
			@JsonProperty(value = "nominee")
			private Nominee nominee;
			@JsonProperty(value = "guardian")
			private Guardian guardian;
			@JsonProperty(value = "guardianAddress")
			private List<GuardianAddress> guardianAddress;
			@JsonProperty(value = "nomineeAddress")
			private List<NomineeAddress> nomineeAddress;
			@JsonProperty(value = "accountDetails_D0")
			private AccountDetails_D0 accountDetails_D0;
			@JsonProperty(value = "customerAccountLeadRelationship_D0")
			private List<CustomerAccountLeadRelationship_D0> customerAccountLeadRelationship_D0;
			@JsonProperty(value = "customerAgeDetails")
			private CustomerAgeDetails customerAgeDetails;
			@JsonProperty(value = "custPreferEmail")
			private List<CustPreferEmail> custPreferEmail;
			@JsonProperty(value = "chequeBookLov")
			private List<ChequeBookLov> chequeBookLov;
			@JsonProperty(value = "BulkAccountDetails")
			private BulkAccountDetails bulkAccountDetails;

			public String getGuardianIDForAddress() {
				return guardianIDForAddress;
			}

			public void setGuardianIDForAddress(String guardianIDForAddress) {
				this.guardianIDForAddress = guardianIDForAddress;
			}

			public String getIsPmay() {
				return isPmay;
			}

			public void setIsPmay(String isPmay) {
				this.isPmay = isPmay;
			}

			public String getFetchAccountLeadID() {
				return fetchAccountLeadID;
			}

			public void setFetchAccountLeadID(String fetchAccountLeadID) {
				this.fetchAccountLeadID = fetchAccountLeadID;
			}

			public String getAccountLeadID() {
				return accountLeadID;
			}

			public void setAccountLeadID(String accountLeadID) {
				this.accountLeadID = accountLeadID;
			}

			public String getAccountNumber() {
				return accountNumber;
			}

			public void setAccountNumber(String accountNumber) {
				this.accountNumber = accountNumber;
			}

			public String getNomineeIDForAddress() {
				return nomineeIDForAddress;
			}

			public void setNomineeIDForAddress(String nomineeIDForAddress) {
				this.nomineeIDForAddress = nomineeIDForAddress;
			}

			public AccountLead getAccountLead() {
				return accountLead;
			}

			public void setAccountLead(AccountLead accountLead) {
				this.accountLead = accountLead;
			}

			public List<CustomerAccountLeadRelation> getCustomerAccountLeadRelation() {
				return customerAccountLeadRelation;
			}

			public void setCustomerAccountLeadRelation(List<CustomerAccountLeadRelation> customerAccountLeadRelation) {
				this.customerAccountLeadRelation = customerAccountLeadRelation;
			}

			public AccountLeadNomineeMap getAccountLeadNomineeMap() {
				return accountLeadNomineeMap;
			}

			public void setAccountLeadNomineeMap(AccountLeadNomineeMap accountLeadNomineeMap) {
				this.accountLeadNomineeMap = accountLeadNomineeMap;
			}

			public List<AccountLeadBeneficialOwner> getAccountLeadBeneficialOwner() {
				return accountLeadBeneficialOwner;
			}

			public void setAccountLeadBeneficialOwner(List<AccountLeadBeneficialOwner> accountLeadBeneficialOwner) {
				this.accountLeadBeneficialOwner = accountLeadBeneficialOwner;
			}

			public List<CustomerDeliverables> getCustomerDeliverables() {
				return customerDeliverables;
			}

			public void setCustomerDeliverables(List<CustomerDeliverables> customerDeliverables) {
				this.customerDeliverables = customerDeliverables;
			}

			public List<CustomerPreferences> getCustomerPreferences() {
				return customerPreferences;
			}

			public void setCustomerPreferences(List<CustomerPreferences> customerPreferences) {
				this.customerPreferences = customerPreferences;
			}

			public List<BeneficialOwner> getBeneficialOwner() {
				return beneficialOwner;
			}

			public void setBeneficialOwner(List<BeneficialOwner> beneficialOwner) {
				this.beneficialOwner = beneficialOwner;
			}

			public Nominee getNominee() {
				return nominee;
			}

			public void setNominee(Nominee nominee) {
				this.nominee = nominee;
			}

			public Guardian getGuardian() {
				return guardian;
			}

			public void setGuardian(Guardian guardian) {
				this.guardian = guardian;
			}

			public List<GuardianAddress> getGuardianAddress() {
				return guardianAddress;
			}

			public void setGuardianAddress(List<GuardianAddress> guardianAddress) {
				this.guardianAddress = guardianAddress;
			}

			public List<NomineeAddress> getNomineeAddress() {
				return nomineeAddress;
			}

			public void setNomineeAddress(List<NomineeAddress> nomineeAddress) {
				this.nomineeAddress = nomineeAddress;
			}

			public AccountDetails_D0 getAccountDetails_D0() {
				return accountDetails_D0;
			}

			public void setAccountDetails_D0(AccountDetails_D0 accountDetails_D0) {
				this.accountDetails_D0 = accountDetails_D0;
			}

			public List<CustomerAccountLeadRelationship_D0> getCustomerAccountLeadRelationship_D0() {
				return customerAccountLeadRelationship_D0;
			}

			public void setCustomerAccountLeadRelationship_D0(
					List<CustomerAccountLeadRelationship_D0> customerAccountLeadRelationship_D0) {
				this.customerAccountLeadRelationship_D0 = customerAccountLeadRelationship_D0;
			}

			public CustomerAgeDetails getCustomerAgeDetails() {
				return customerAgeDetails;
			}

			public void setCustomerAgeDetails(CustomerAgeDetails customerAgeDetails) {
				this.customerAgeDetails = customerAgeDetails;
			}

			public List<CustPreferEmail> getCustPreferEmail() {
				return custPreferEmail;
			}

			public void setCustPreferEmail(List<CustPreferEmail> custPreferEmail) {
				this.custPreferEmail = custPreferEmail;
			}

			public List<ChequeBookLov> getChequeBookLov() {
				return chequeBookLov;
			}

			public void setChequeBookLov(List<ChequeBookLov> chequeBookLov) {
				this.chequeBookLov = chequeBookLov;
			}

			public BulkAccountDetails getBulkAccountDetails() {
				return bulkAccountDetails;
			}

			public void setBulkAccountDetails(BulkAccountDetails bulkAccountDetails) {
				this.bulkAccountDetails = bulkAccountDetails;
			}

			public static class AccountLead {
				private String leadAssignedTo;
				private String sourceBranch;
				private String sourcingChannel;
				private String sourceofLead;
				private String campaignCode;
				private String otherSourceOfLead;
				private String isPriorityLead;
				private String productCategory;
				private String productVariant;
				private String accountTitle;
				private String accountType;
				private String isNRIAccount;
				private String NREAccountType;
				private String NROAccountType;
				private String accountOpeningBranch;
				private String modeOfOperation;
				private String sourceOfFund;
				private String currencyOfDeposit;
				private String registeredForSweepIn;
				private String initialDepositMode;
				private String initialDepositAmount;
				private String cashTransactionRefNo;
				private String cashTransactionDate;
				private String chequeNo;
				private String chequeIssuedBank;
				private String chequeTransactionDate;
				private String fromESFBAccountNo;
				private String fromESFBGLAccount;
				private String branchCodeGL;
				private String transactionNarration;
				private String depositAmount;
				private String tenureMonths;
				private String tenureDays;
				private String depositVariance;
				private String iPayToESFBAccountNo;
				private String iPayToOtherBankAccountNo;
				private String iPayToOtherBankBenificiaryName;
				private String iPayToOtherBankIFSC;
				private String iPayToOtherBankName;
				private String iPayToOtherBankBranch;
				private String iPayToOtherBankMICR;
				private String iPByDDPOIssuerCode;
				private String iPByDDPOPayeeName;
				private String maturityInstruction;
				private String MICreditToESFBAccountNo;
				private String MICreditToOtherBankAccountNo;
				private String MICreditToOtherBankAccountType;
				private String MICreditToOtherBankIFSC;
				private String MICreditToOtherBankName;
				private String MICreditToOtherBankBranch;
				private String MICreditToOtherBankMICR;
				private String MIByDDPOIssuerCode;
				private String MIByDDPOPayeeName;
				private String installmentAmount;
				private String periodicity;
				private String debitAccount;
				private String SIFrequency;
				private String SIDebitAmount;
				private String FATCADeclaration;
				private String accountOpeningValueDate;
				private String favouriteAccountNo;
				private String favouriteAccountCriteria;
				private String accountNumber;
				private String accountCreatedOn;
				private String partOfAccountGroup;
				private String isRestrictedAccount;
				private String purposeOfOpeningAccount;
				private String purposeOfOpeningAccountOthers;
				private String expectedMonthlyRemittance;
				private String expectedMonthlyWithdrawal;
				private String subscripedForLPG;
				private String isDelete;
				private String isTDSAvailable;
				private String accountOpeningFlow;
				private String waiveOffTDS;
				private String leadCreatedBy;
				private String leadCreatedOn;
				private String leadModifiedBy;
				private String leadModifiedOn;
				private String otherSourceOfFund;

				public String getLeadAssignedTo() {
					return leadAssignedTo;
				}

				public void setLeadAssignedTo(String leadAssignedTo) {
					this.leadAssignedTo = leadAssignedTo;
				}

				public String getSourceBranch() {
					return sourceBranch;
				}

				public void setSourceBranch(String sourceBranch) {
					this.sourceBranch = sourceBranch;
				}

				public String getSourcingChannel() {
					return sourcingChannel;
				}

				public void setSourcingChannel(String sourcingChannel) {
					this.sourcingChannel = sourcingChannel;
				}

				public String getSourceofLead() {
					return sourceofLead;
				}

				public void setSourceofLead(String sourceofLead) {
					this.sourceofLead = sourceofLead;
				}

				public String getCampaignCode() {
					return campaignCode;
				}

				public void setCampaignCode(String campaignCode) {
					this.campaignCode = campaignCode;
				}

				public String getOtherSourceOfLead() {
					return otherSourceOfLead;
				}

				public void setOtherSourceOfLead(String otherSourceOfLead) {
					this.otherSourceOfLead = otherSourceOfLead;
				}

				public String getIsPriorityLead() {
					return isPriorityLead;
				}

				public void setIsPriorityLead(String isPriorityLead) {
					this.isPriorityLead = isPriorityLead;
				}

				public String getProductCategory() {
					return productCategory;
				}

				public void setProductCategory(String productCategory) {
					this.productCategory = productCategory;
				}

				public String getProductVariant() {
					return productVariant;
				}

				public void setProductVariant(String productVariant) {
					this.productVariant = productVariant;
				}

				public String getAccountTitle() {
					return accountTitle;
				}

				public void setAccountTitle(String accountTitle) {
					this.accountTitle = accountTitle;
				}

				public String getAccountType() {
					return accountType;
				}

				public void setAccountType(String accountType) {
					this.accountType = accountType;
				}

				public String getIsNRIAccount() {
					return isNRIAccount;
				}

				public void setIsNRIAccount(String isNRIAccount) {
					this.isNRIAccount = isNRIAccount;
				}

				public String getNREAccountType() {
					return NREAccountType;
				}

				public void setNREAccountType(String nREAccountType) {
					NREAccountType = nREAccountType;
				}

				public String getNROAccountType() {
					return NROAccountType;
				}

				public void setNROAccountType(String nROAccountType) {
					NROAccountType = nROAccountType;
				}

				public String getAccountOpeningBranch() {
					return accountOpeningBranch;
				}

				public void setAccountOpeningBranch(String accountOpeningBranch) {
					this.accountOpeningBranch = accountOpeningBranch;
				}

				public String getModeOfOperation() {
					return modeOfOperation;
				}

				public void setModeOfOperation(String modeOfOperation) {
					this.modeOfOperation = modeOfOperation;
				}

				public String getSourceOfFund() {
					return sourceOfFund;
				}

				public void setSourceOfFund(String sourceOfFund) {
					this.sourceOfFund = sourceOfFund;
				}

				public String getCurrencyOfDeposit() {
					return currencyOfDeposit;
				}

				public void setCurrencyOfDeposit(String currencyOfDeposit) {
					this.currencyOfDeposit = currencyOfDeposit;
				}

				public String getRegisteredForSweepIn() {
					return registeredForSweepIn;
				}

				public void setRegisteredForSweepIn(String registeredForSweepIn) {
					this.registeredForSweepIn = registeredForSweepIn;
				}

				public String getInitialDepositMode() {
					return initialDepositMode;
				}

				public void setInitialDepositMode(String initialDepositMode) {
					this.initialDepositMode = initialDepositMode;
				}

				public String getInitialDepositAmount() {
					return initialDepositAmount;
				}

				public void setInitialDepositAmount(String initialDepositAmount) {
					this.initialDepositAmount = initialDepositAmount;
				}

				public String getCashTransactionRefNo() {
					return cashTransactionRefNo;
				}

				public void setCashTransactionRefNo(String cashTransactionRefNo) {
					this.cashTransactionRefNo = cashTransactionRefNo;
				}

				public String getCashTransactionDate() {
					return cashTransactionDate;
				}

				public void setCashTransactionDate(String cashTransactionDate) {
					this.cashTransactionDate = cashTransactionDate;
				}

				public String getChequeNo() {
					return chequeNo;
				}

				public void setChequeNo(String chequeNo) {
					this.chequeNo = chequeNo;
				}

				public String getChequeIssuedBank() {
					return chequeIssuedBank;
				}

				public void setChequeIssuedBank(String chequeIssuedBank) {
					this.chequeIssuedBank = chequeIssuedBank;
				}

				public String getChequeTransactionDate() {
					return chequeTransactionDate;
				}

				public void setChequeTransactionDate(String chequeTransactionDate) {
					this.chequeTransactionDate = chequeTransactionDate;
				}

				public String getFromESFBAccountNo() {
					return fromESFBAccountNo;
				}

				public void setFromESFBAccountNo(String fromESFBAccountNo) {
					this.fromESFBAccountNo = fromESFBAccountNo;
				}

				public String getFromESFBGLAccount() {
					return fromESFBGLAccount;
				}

				public void setFromESFBGLAccount(String fromESFBGLAccount) {
					this.fromESFBGLAccount = fromESFBGLAccount;
				}

				public String getBranchCodeGL() {
					return branchCodeGL;
				}

				public void setBranchCodeGL(String branchCodeGL) {
					this.branchCodeGL = branchCodeGL;
				}

				public String getTransactionNarration() {
					return transactionNarration;
				}

				public void setTransactionNarration(String transactionNarration) {
					this.transactionNarration = transactionNarration;
				}

				public String getDepositAmount() {
					return depositAmount;
				}

				public void setDepositAmount(String depositAmount) {
					this.depositAmount = depositAmount;
				}

				public String getTenureMonths() {
					return tenureMonths;
				}

				public void setTenureMonths(String tenureMonths) {
					this.tenureMonths = tenureMonths;
				}

				public String getTenureDays() {
					return tenureDays;
				}

				public void setTenureDays(String tenureDays) {
					this.tenureDays = tenureDays;
				}

				public String getDepositVariance() {
					return depositVariance;
				}

				public void setDepositVariance(String depositVariance) {
					this.depositVariance = depositVariance;
				}

				public String getiPayToESFBAccountNo() {
					return iPayToESFBAccountNo;
				}

				public void setiPayToESFBAccountNo(String iPayToESFBAccountNo) {
					this.iPayToESFBAccountNo = iPayToESFBAccountNo;
				}

				public String getiPayToOtherBankAccountNo() {
					return iPayToOtherBankAccountNo;
				}

				public void setiPayToOtherBankAccountNo(String iPayToOtherBankAccountNo) {
					this.iPayToOtherBankAccountNo = iPayToOtherBankAccountNo;
				}

				public String getiPayToOtherBankBenificiaryName() {
					return iPayToOtherBankBenificiaryName;
				}

				public void setiPayToOtherBankBenificiaryName(String iPayToOtherBankBenificiaryName) {
					this.iPayToOtherBankBenificiaryName = iPayToOtherBankBenificiaryName;
				}

				public String getiPayToOtherBankIFSC() {
					return iPayToOtherBankIFSC;
				}

				public void setiPayToOtherBankIFSC(String iPayToOtherBankIFSC) {
					this.iPayToOtherBankIFSC = iPayToOtherBankIFSC;
				}

				public String getiPayToOtherBankName() {
					return iPayToOtherBankName;
				}

				public void setiPayToOtherBankName(String iPayToOtherBankName) {
					this.iPayToOtherBankName = iPayToOtherBankName;
				}

				public String getiPayToOtherBankBranch() {
					return iPayToOtherBankBranch;
				}

				public void setiPayToOtherBankBranch(String iPayToOtherBankBranch) {
					this.iPayToOtherBankBranch = iPayToOtherBankBranch;
				}

				public String getiPayToOtherBankMICR() {
					return iPayToOtherBankMICR;
				}

				public void setiPayToOtherBankMICR(String iPayToOtherBankMICR) {
					this.iPayToOtherBankMICR = iPayToOtherBankMICR;
				}

				public String getiPByDDPOIssuerCode() {
					return iPByDDPOIssuerCode;
				}

				public void setiPByDDPOIssuerCode(String iPByDDPOIssuerCode) {
					this.iPByDDPOIssuerCode = iPByDDPOIssuerCode;
				}

				public String getiPByDDPOPayeeName() {
					return iPByDDPOPayeeName;
				}

				public void setiPByDDPOPayeeName(String iPByDDPOPayeeName) {
					this.iPByDDPOPayeeName = iPByDDPOPayeeName;
				}

				public String getMaturityInstruction() {
					return maturityInstruction;
				}

				public void setMaturityInstruction(String maturityInstruction) {
					this.maturityInstruction = maturityInstruction;
				}

				public String getMICreditToESFBAccountNo() {
					return MICreditToESFBAccountNo;
				}

				public void setMICreditToESFBAccountNo(String mICreditToESFBAccountNo) {
					MICreditToESFBAccountNo = mICreditToESFBAccountNo;
				}

				public String getMICreditToOtherBankAccountNo() {
					return MICreditToOtherBankAccountNo;
				}

				public void setMICreditToOtherBankAccountNo(String mICreditToOtherBankAccountNo) {
					MICreditToOtherBankAccountNo = mICreditToOtherBankAccountNo;
				}

				public String getMICreditToOtherBankAccountType() {
					return MICreditToOtherBankAccountType;
				}

				public void setMICreditToOtherBankAccountType(String mICreditToOtherBankAccountType) {
					MICreditToOtherBankAccountType = mICreditToOtherBankAccountType;
				}

				public String getMICreditToOtherBankIFSC() {
					return MICreditToOtherBankIFSC;
				}

				public void setMICreditToOtherBankIFSC(String mICreditToOtherBankIFSC) {
					MICreditToOtherBankIFSC = mICreditToOtherBankIFSC;
				}

				public String getMICreditToOtherBankName() {
					return MICreditToOtherBankName;
				}

				public void setMICreditToOtherBankName(String mICreditToOtherBankName) {
					MICreditToOtherBankName = mICreditToOtherBankName;
				}

				public String getMICreditToOtherBankBranch() {
					return MICreditToOtherBankBranch;
				}

				public void setMICreditToOtherBankBranch(String mICreditToOtherBankBranch) {
					MICreditToOtherBankBranch = mICreditToOtherBankBranch;
				}

				public String getMICreditToOtherBankMICR() {
					return MICreditToOtherBankMICR;
				}

				public void setMICreditToOtherBankMICR(String mICreditToOtherBankMICR) {
					MICreditToOtherBankMICR = mICreditToOtherBankMICR;
				}

				public String getMIByDDPOIssuerCode() {
					return MIByDDPOIssuerCode;
				}

				public void setMIByDDPOIssuerCode(String mIByDDPOIssuerCode) {
					MIByDDPOIssuerCode = mIByDDPOIssuerCode;
				}

				public String getMIByDDPOPayeeName() {
					return MIByDDPOPayeeName;
				}

				public void setMIByDDPOPayeeName(String mIByDDPOPayeeName) {
					MIByDDPOPayeeName = mIByDDPOPayeeName;
				}

				public String getInstallmentAmount() {
					return installmentAmount;
				}

				public void setInstallmentAmount(String installmentAmount) {
					this.installmentAmount = installmentAmount;
				}

				public String getPeriodicity() {
					return periodicity;
				}

				public void setPeriodicity(String periodicity) {
					this.periodicity = periodicity;
				}

				public String getDebitAccount() {
					return debitAccount;
				}

				public void setDebitAccount(String debitAccount) {
					this.debitAccount = debitAccount;
				}

				public String getSIFrequency() {
					return SIFrequency;
				}

				public void setSIFrequency(String sIFrequency) {
					SIFrequency = sIFrequency;
				}

				public String getSIDebitAmount() {
					return SIDebitAmount;
				}

				public void setSIDebitAmount(String sIDebitAmount) {
					SIDebitAmount = sIDebitAmount;
				}

				public String getFATCADeclaration() {
					return FATCADeclaration;
				}

				public void setFATCADeclaration(String fATCADeclaration) {
					FATCADeclaration = fATCADeclaration;
				}

				public String getAccountOpeningValueDate() {
					return accountOpeningValueDate;
				}

				public void setAccountOpeningValueDate(String accountOpeningValueDate) {
					this.accountOpeningValueDate = accountOpeningValueDate;
				}

				public String getFavouriteAccountNo() {
					return favouriteAccountNo;
				}

				public void setFavouriteAccountNo(String favouriteAccountNo) {
					this.favouriteAccountNo = favouriteAccountNo;
				}

				public String getFavouriteAccountCriteria() {
					return favouriteAccountCriteria;
				}

				public void setFavouriteAccountCriteria(String favouriteAccountCriteria) {
					this.favouriteAccountCriteria = favouriteAccountCriteria;
				}

				public String getAccountNumber() {
					return accountNumber;
				}

				public void setAccountNumber(String accountNumber) {
					this.accountNumber = accountNumber;
				}

				public String getAccountCreatedOn() {
					return accountCreatedOn;
				}

				public void setAccountCreatedOn(String accountCreatedOn) {
					this.accountCreatedOn = accountCreatedOn;
				}

				public String getPartOfAccountGroup() {
					return partOfAccountGroup;
				}

				public void setPartOfAccountGroup(String partOfAccountGroup) {
					this.partOfAccountGroup = partOfAccountGroup;
				}

				public String getIsRestrictedAccount() {
					return isRestrictedAccount;
				}

				public void setIsRestrictedAccount(String isRestrictedAccount) {
					this.isRestrictedAccount = isRestrictedAccount;
				}

				public String getPurposeOfOpeningAccount() {
					return purposeOfOpeningAccount;
				}

				public void setPurposeOfOpeningAccount(String purposeOfOpeningAccount) {
					this.purposeOfOpeningAccount = purposeOfOpeningAccount;
				}

				public String getPurposeOfOpeningAccountOthers() {
					return purposeOfOpeningAccountOthers;
				}

				public void setPurposeOfOpeningAccountOthers(String purposeOfOpeningAccountOthers) {
					this.purposeOfOpeningAccountOthers = purposeOfOpeningAccountOthers;
				}

				public String getExpectedMonthlyRemittance() {
					return expectedMonthlyRemittance;
				}

				public void setExpectedMonthlyRemittance(String expectedMonthlyRemittance) {
					this.expectedMonthlyRemittance = expectedMonthlyRemittance;
				}

				public String getExpectedMonthlyWithdrawal() {
					return expectedMonthlyWithdrawal;
				}

				public void setExpectedMonthlyWithdrawal(String expectedMonthlyWithdrawal) {
					this.expectedMonthlyWithdrawal = expectedMonthlyWithdrawal;
				}

				public String getSubscripedForLPG() {
					return subscripedForLPG;
				}

				public void setSubscripedForLPG(String subscripedForLPG) {
					this.subscripedForLPG = subscripedForLPG;
				}

				public String getIsDelete() {
					return isDelete;
				}

				public void setIsDelete(String isDelete) {
					this.isDelete = isDelete;
				}

				public String getIsTDSAvailable() {
					return isTDSAvailable;
				}

				public void setIsTDSAvailable(String isTDSAvailable) {
					this.isTDSAvailable = isTDSAvailable;
				}

				public String getAccountOpeningFlow() {
					return accountOpeningFlow;
				}

				public void setAccountOpeningFlow(String accountOpeningFlow) {
					this.accountOpeningFlow = accountOpeningFlow;
				}

				public String getWaiveOffTDS() {
					return waiveOffTDS;
				}

				public void setWaiveOffTDS(String waiveOffTDS) {
					this.waiveOffTDS = waiveOffTDS;
				}

				public String getLeadCreatedBy() {
					return leadCreatedBy;
				}

				public void setLeadCreatedBy(String leadCreatedBy) {
					this.leadCreatedBy = leadCreatedBy;
				}

				public String getLeadCreatedOn() {
					return leadCreatedOn;
				}

				public void setLeadCreatedOn(String leadCreatedOn) {
					this.leadCreatedOn = leadCreatedOn;
				}

				public String getLeadModifiedBy() {
					return leadModifiedBy;
				}

				public void setLeadModifiedBy(String leadModifiedBy) {
					this.leadModifiedBy = leadModifiedBy;
				}

				public String getLeadModifiedOn() {
					return leadModifiedOn;
				}

				public void setLeadModifiedOn(String leadModifiedOn) {
					this.leadModifiedOn = leadModifiedOn;
				}

				public String getOtherSourceOfFund() {
					return otherSourceOfFund;
				}

				public void setOtherSourceOfFund(String otherSourceOfFund) {
					this.otherSourceOfFund = otherSourceOfFund;
				}

			}

			public static class CustomerAccountLeadRelation {

				private String UCIC;
				private String customerLeadID;
				private String customerAccountRelation;
				private String isPrimaryHolder;
				private String relationToPrimaryHolder;
				private String isParentAccount;
				private String id;
				private String isDelete;
				private String leadState;
				private String leadStateMsg;
				private String entityType;
				private String customerAccountRelationTitle;
				private String relationToPrimaryHolderTitle;
				private String entityTypeTitle;
				private String accountLeadID;
				private String customerName;
				private String customerPhoneNumber;
				private String isInsert;
				private String completedDataEntryD0;
				private String completedDataEntryD1;
				private String completedVerifyingD2;
				private String completedDataEntryD2;
				private String illiterateFlag;
				private String NLFound;
				private String age;
				private String isMinor;
				private String isStaff;

				public String getUCIC() {
					return UCIC;
				}

				public void setUCIC(String uCIC) {
					UCIC = uCIC;
				}

				public String getCustomerLeadID() {
					return customerLeadID;
				}

				public void setCustomerLeadID(String customerLeadID) {
					this.customerLeadID = customerLeadID;
				}

				public String getCustomerAccountRelation() {
					return customerAccountRelation;
				}

				public void setCustomerAccountRelation(String customerAccountRelation) {
					this.customerAccountRelation = customerAccountRelation;
				}

				public String getIsPrimaryHolder() {
					return isPrimaryHolder;
				}

				public void setIsPrimaryHolder(String isPrimaryHolder) {
					this.isPrimaryHolder = isPrimaryHolder;
				}

				public String getRelationToPrimaryHolder() {
					return relationToPrimaryHolder;
				}

				public void setRelationToPrimaryHolder(String relationToPrimaryHolder) {
					this.relationToPrimaryHolder = relationToPrimaryHolder;
				}

				public String getIsParentAccount() {
					return isParentAccount;
				}

				public void setIsParentAccount(String isParentAccount) {
					this.isParentAccount = isParentAccount;
				}

				public String getId() {
					return id;
				}

				public void setId(String id) {
					this.id = id;
				}

				public String getIsDelete() {
					return isDelete;
				}

				public void setIsDelete(String isDelete) {
					this.isDelete = isDelete;
				}

				public String getLeadState() {
					return leadState;
				}

				public void setLeadState(String leadState) {
					this.leadState = leadState;
				}

				public String getLeadStateMsg() {
					return leadStateMsg;
				}

				public void setLeadStateMsg(String leadStateMsg) {
					this.leadStateMsg = leadStateMsg;
				}

				public String getEntityType() {
					return entityType;
				}

				public void setEntityType(String entityType) {
					this.entityType = entityType;
				}

				public String getCustomerAccountRelationTitle() {
					return customerAccountRelationTitle;
				}

				public void setCustomerAccountRelationTitle(String customerAccountRelationTitle) {
					this.customerAccountRelationTitle = customerAccountRelationTitle;
				}

				public String getRelationToPrimaryHolderTitle() {
					return relationToPrimaryHolderTitle;
				}

				public void setRelationToPrimaryHolderTitle(String relationToPrimaryHolderTitle) {
					this.relationToPrimaryHolderTitle = relationToPrimaryHolderTitle;
				}

				public String getEntityTypeTitle() {
					return entityTypeTitle;
				}

				public void setEntityTypeTitle(String entityTypeTitle) {
					this.entityTypeTitle = entityTypeTitle;
				}

				public String getAccountLeadID() {
					return accountLeadID;
				}

				public void setAccountLeadID(String accountLeadID) {
					this.accountLeadID = accountLeadID;
				}

				public String getCustomerName() {
					return customerName;
				}

				public void setCustomerName(String customerName) {
					this.customerName = customerName;
				}

				public String getCustomerPhoneNumber() {
					return customerPhoneNumber;
				}

				public void setCustomerPhoneNumber(String customerPhoneNumber) {
					this.customerPhoneNumber = customerPhoneNumber;
				}

				public String getIsInsert() {
					return isInsert;
				}

				public void setIsInsert(String isInsert) {
					this.isInsert = isInsert;
				}

				public String getCompletedDataEntryD0() {
					return completedDataEntryD0;
				}

				public void setCompletedDataEntryD0(String completedDataEntryD0) {
					this.completedDataEntryD0 = completedDataEntryD0;
				}

				public String getCompletedDataEntryD1() {
					return completedDataEntryD1;
				}

				public void setCompletedDataEntryD1(String completedDataEntryD1) {
					this.completedDataEntryD1 = completedDataEntryD1;
				}

				public String getCompletedVerifyingD2() {
					return completedVerifyingD2;
				}

				public void setCompletedVerifyingD2(String completedVerifyingD2) {
					this.completedVerifyingD2 = completedVerifyingD2;
				}

				public String getCompletedDataEntryD2() {
					return completedDataEntryD2;
				}

				public void setCompletedDataEntryD2(String completedDataEntryD2) {
					this.completedDataEntryD2 = completedDataEntryD2;
				}

				public String getIlliterateFlag() {
					return illiterateFlag;
				}

				public void setIlliterateFlag(String illiterateFlag) {
					this.illiterateFlag = illiterateFlag;
				}

				public String getNLFound() {
					return NLFound;
				}

				public void setNLFound(String nLFound) {
					NLFound = nLFound;
				}

				public String getAge() {
					return age;
				}

				public void setAge(String age) {
					this.age = age;
				}

				public String getIsMinor() {
					return isMinor;
				}

				public void setIsMinor(String isMinor) {
					this.isMinor = isMinor;
				}

				public String getIsStaff() {
					return isStaff;
				}

				public void setIsStaff(String isStaff) {
					this.isStaff = isStaff;
				}

			}

			public static class AccountLeadNomineeMap {

				private String nomineeID;
				private String UCIC;
				private String id;
				private String isDelete;

				public String getNomineeID() {
					return nomineeID;
				}

				public void setNomineeID(String nomineeID) {
					this.nomineeID = nomineeID;
				}

				public String getUCIC() {
					return UCIC;
				}

				public void setUCIC(String uCIC) {
					UCIC = uCIC;
				}

				public String getId() {
					return id;
				}

				public void setId(String id) {
					this.id = id;
				}

				public String getIsDelete() {
					return isDelete;
				}

				public void setIsDelete(String isDelete) {
					this.isDelete = isDelete;
				}

			}

			public static class AccountLeadBeneficialOwner {

				private String beneficialOwnerID;
				private String id;
				private String isDelete;

				public String getBeneficialOwnerID() {
					return beneficialOwnerID;
				}

				public void setBeneficialOwnerID(String beneficialOwnerID) {
					this.beneficialOwnerID = beneficialOwnerID;
				}

				public String getId() {
					return id;
				}

				public void setId(String id) {
					this.id = id;
				}

				public String getIsDelete() {
					return isDelete;
				}

				public void setIsDelete(String isDelete) {
					this.isDelete = isDelete;
				}

			}

			public static class CustomerDeliverables {

				private String mappedCustomer;
				private String mappedAccount;
				private String chequeBook;
				private String noOfChequeBooks;
				private String noOfChequeLeaves;
				private String despatchMode;
				private String debitCard;
				private String debitCardType;
				private String nameOnCard;
				private String welcomeKit;
				private String iKIT;
				private String iKITReferenceNo;
				private String iKITAccountNo;
				private String iKITCustomerID;
				private String iKITDebitCardType;
				private String iKITDebitCardNo;
				private String iKITChequeBook;
				private String deliverableID;
				private String isDelete;
				private String mappedCustomerLead;
				private String mappedAccountLead;
				private String predefinedAccountNo;
				private String predefinedUCIC;

				public String getMappedCustomer() {
					return mappedCustomer;
				}

				public void setMappedCustomer(String mappedCustomer) {
					this.mappedCustomer = mappedCustomer;
				}

				public String getMappedAccount() {
					return mappedAccount;
				}

				public void setMappedAccount(String mappedAccount) {
					this.mappedAccount = mappedAccount;
				}

				public String getChequeBook() {
					return chequeBook;
				}

				public void setChequeBook(String chequeBook) {
					this.chequeBook = chequeBook;
				}

				public String getNoOfChequeBooks() {
					return noOfChequeBooks;
				}

				public void setNoOfChequeBooks(String noOfChequeBooks) {
					this.noOfChequeBooks = noOfChequeBooks;
				}

				public String getNoOfChequeLeaves() {
					return noOfChequeLeaves;
				}

				public void setNoOfChequeLeaves(String noOfChequeLeaves) {
					this.noOfChequeLeaves = noOfChequeLeaves;
				}

				public String getDespatchMode() {
					return despatchMode;
				}

				public void setDespatchMode(String despatchMode) {
					this.despatchMode = despatchMode;
				}

				public String getDebitCard() {
					return debitCard;
				}

				public void setDebitCard(String debitCard) {
					this.debitCard = debitCard;
				}

				public String getDebitCardType() {
					return debitCardType;
				}

				public void setDebitCardType(String debitCardType) {
					this.debitCardType = debitCardType;
				}

				public String getNameOnCard() {
					return nameOnCard;
				}

				public void setNameOnCard(String nameOnCard) {
					this.nameOnCard = nameOnCard;
				}

				public String getWelcomeKit() {
					return welcomeKit;
				}

				public void setWelcomeKit(String welcomeKit) {
					this.welcomeKit = welcomeKit;
				}

				public String getiKIT() {
					return iKIT;
				}

				public void setiKIT(String iKIT) {
					this.iKIT = iKIT;
				}

				public String getiKITReferenceNo() {
					return iKITReferenceNo;
				}

				public void setiKITReferenceNo(String iKITReferenceNo) {
					this.iKITReferenceNo = iKITReferenceNo;
				}

				public String getiKITAccountNo() {
					return iKITAccountNo;
				}

				public void setiKITAccountNo(String iKITAccountNo) {
					this.iKITAccountNo = iKITAccountNo;
				}

				public String getiKITCustomerID() {
					return iKITCustomerID;
				}

				public void setiKITCustomerID(String iKITCustomerID) {
					this.iKITCustomerID = iKITCustomerID;
				}

				public String getiKITDebitCardType() {
					return iKITDebitCardType;
				}

				public void setiKITDebitCardType(String iKITDebitCardType) {
					this.iKITDebitCardType = iKITDebitCardType;
				}

				public String getiKITDebitCardNo() {
					return iKITDebitCardNo;
				}

				public void setiKITDebitCardNo(String iKITDebitCardNo) {
					this.iKITDebitCardNo = iKITDebitCardNo;
				}

				public String getiKITChequeBook() {
					return iKITChequeBook;
				}

				public void setiKITChequeBook(String iKITChequeBook) {
					this.iKITChequeBook = iKITChequeBook;
				}

				public String getDeliverableID() {
					return deliverableID;
				}

				public void setDeliverableID(String deliverableID) {
					this.deliverableID = deliverableID;
				}

				public String getIsDelete() {
					return isDelete;
				}

				public void setIsDelete(String isDelete) {
					this.isDelete = isDelete;
				}

				public String getMappedCustomerLead() {
					return mappedCustomerLead;
				}

				public void setMappedCustomerLead(String mappedCustomerLead) {
					this.mappedCustomerLead = mappedCustomerLead;
				}

				public String getMappedAccountLead() {
					return mappedAccountLead;
				}

				public void setMappedAccountLead(String mappedAccountLead) {
					this.mappedAccountLead = mappedAccountLead;
				}

				public String getPredefinedAccountNo() {
					return predefinedAccountNo;
				}

				public void setPredefinedAccountNo(String predefinedAccountNo) {
					this.predefinedAccountNo = predefinedAccountNo;
				}

				public String getPredefinedUCIC() {
					return predefinedUCIC;
				}

				public void setPredefinedUCIC(String predefinedUCIC) {
					this.predefinedUCIC = predefinedUCIC;
				}

			}

			public static class CustomerPreferences {

				private String preferenceID;
				private String mappedCustomer;
				private String mappedAccount;
				private String sms;
				private String allSMSAlerts;
				private String onlyTransactionAlerts;
				private String passbook;
				private String physicalStatement;
				private String emailStatement;
				private String netBanking;
				private String netBankingRights;
				private String mobileBankingNumber;
				private String bankGuarantee;
				private String letterofCredit;
				private String businessLoan;
				private String doorStepBanking;
				private String doorStepBankingOnCall;
				private String doorStepBankingBeat;
				private String tradeForex;
				private String loanAgainstProperty;
				private String overdraftsagainstFD;
				private String isDelete;
				private String mappedCustomerLead;
				private String mappedAccountLead;
				private String entityType;
				private String entityFlag;
				private String brnchId;
				private String alertEmailID;

				public String getPreferenceID() {
					return preferenceID;
				}

				public void setPreferenceID(String preferenceID) {
					this.preferenceID = preferenceID;
				}

				public String getMappedCustomer() {
					return mappedCustomer;
				}

				public void setMappedCustomer(String mappedCustomer) {
					this.mappedCustomer = mappedCustomer;
				}

				public String getMappedAccount() {
					return mappedAccount;
				}

				public void setMappedAccount(String mappedAccount) {
					this.mappedAccount = mappedAccount;
				}

				public String getSms() {
					return sms;
				}

				public void setSms(String sms) {
					this.sms = sms;
				}

				public String getAllSMSAlerts() {
					return allSMSAlerts;
				}

				public void setAllSMSAlerts(String allSMSAlerts) {
					this.allSMSAlerts = allSMSAlerts;
				}

				public String getOnlyTransactionAlerts() {
					return onlyTransactionAlerts;
				}

				public void setOnlyTransactionAlerts(String onlyTransactionAlerts) {
					this.onlyTransactionAlerts = onlyTransactionAlerts;
				}

				public String getPassbook() {
					return passbook;
				}

				public void setPassbook(String passbook) {
					this.passbook = passbook;
				}

				public String getPhysicalStatement() {
					return physicalStatement;
				}

				public void setPhysicalStatement(String physicalStatement) {
					this.physicalStatement = physicalStatement;
				}

				public String getEmailStatement() {
					return emailStatement;
				}

				public void setEmailStatement(String emailStatement) {
					this.emailStatement = emailStatement;
				}

				public String getNetBanking() {
					return netBanking;
				}

				public void setNetBanking(String netBanking) {
					this.netBanking = netBanking;
				}

				public String getNetBankingRights() {
					return netBankingRights;
				}

				public void setNetBankingRights(String netBankingRights) {
					this.netBankingRights = netBankingRights;
				}

				public String getMobileBankingNumber() {
					return mobileBankingNumber;
				}

				public void setMobileBankingNumber(String mobileBankingNumber) {
					this.mobileBankingNumber = mobileBankingNumber;
				}

				public String getBankGuarantee() {
					return bankGuarantee;
				}

				public void setBankGuarantee(String bankGuarantee) {
					this.bankGuarantee = bankGuarantee;
				}

				public String getLetterofCredit() {
					return letterofCredit;
				}

				public void setLetterofCredit(String letterofCredit) {
					this.letterofCredit = letterofCredit;
				}

				public String getBusinessLoan() {
					return businessLoan;
				}

				public void setBusinessLoan(String businessLoan) {
					this.businessLoan = businessLoan;
				}

				public String getDoorStepBanking() {
					return doorStepBanking;
				}

				public void setDoorStepBanking(String doorStepBanking) {
					this.doorStepBanking = doorStepBanking;
				}

				public String getDoorStepBankingOnCall() {
					return doorStepBankingOnCall;
				}

				public void setDoorStepBankingOnCall(String doorStepBankingOnCall) {
					this.doorStepBankingOnCall = doorStepBankingOnCall;
				}

				public String getDoorStepBankingBeat() {
					return doorStepBankingBeat;
				}

				public void setDoorStepBankingBeat(String doorStepBankingBeat) {
					this.doorStepBankingBeat = doorStepBankingBeat;
				}

				public String getTradeForex() {
					return tradeForex;
				}

				public void setTradeForex(String tradeForex) {
					this.tradeForex = tradeForex;
				}

				public String getLoanAgainstProperty() {
					return loanAgainstProperty;
				}

				public void setLoanAgainstProperty(String loanAgainstProperty) {
					this.loanAgainstProperty = loanAgainstProperty;
				}

				public String getOverdraftsagainstFD() {
					return overdraftsagainstFD;
				}

				public void setOverdraftsagainstFD(String overdraftsagainstFD) {
					this.overdraftsagainstFD = overdraftsagainstFD;
				}

				public String getIsDelete() {
					return isDelete;
				}

				public void setIsDelete(String isDelete) {
					this.isDelete = isDelete;
				}

				public String getMappedCustomerLead() {
					return mappedCustomerLead;
				}

				public void setMappedCustomerLead(String mappedCustomerLead) {
					this.mappedCustomerLead = mappedCustomerLead;
				}

				public String getMappedAccountLead() {
					return mappedAccountLead;
				}

				public void setMappedAccountLead(String mappedAccountLead) {
					this.mappedAccountLead = mappedAccountLead;
				}

				public String getEntityType() {
					return entityType;
				}

				public void setEntityType(String entityType) {
					this.entityType = entityType;
				}

				public String getEntityFlag() {
					return entityFlag;
				}

				public void setEntityFlag(String entityFlag) {
					this.entityFlag = entityFlag;
				}

				public String getBrnchId() {
					return brnchId;
				}

				public void setBrnchId(String brnchId) {
					this.brnchId = brnchId;
				}

				public String getAlertEmailID() {
					return alertEmailID;
				}

				public void setAlertEmailID(String alertEmailID) {
					this.alertEmailID = alertEmailID;
				}

			}

			public static class BeneficialOwner {

				private String beneficialOwnerName;
				private String beneficialOwnerDesignation;
				private String beneficialUCIC;
				private String percentageHolding;
				private String beneficialSignature;
				private String signatureDocument;
				private String beneficialOwnerID;
				private String isDelete;

				public String getBeneficialOwnerName() {
					return beneficialOwnerName;
				}

				public void setBeneficialOwnerName(String beneficialOwnerName) {
					this.beneficialOwnerName = beneficialOwnerName;
				}

				public String getBeneficialOwnerDesignation() {
					return beneficialOwnerDesignation;
				}

				public void setBeneficialOwnerDesignation(String beneficialOwnerDesignation) {
					this.beneficialOwnerDesignation = beneficialOwnerDesignation;
				}

				public String getBeneficialUCIC() {
					return beneficialUCIC;
				}

				public void setBeneficialUCIC(String beneficialUCIC) {
					this.beneficialUCIC = beneficialUCIC;
				}

				public String getPercentageHolding() {
					return percentageHolding;
				}

				public void setPercentageHolding(String percentageHolding) {
					this.percentageHolding = percentageHolding;
				}

				public String getBeneficialSignature() {
					return beneficialSignature;
				}

				public void setBeneficialSignature(String beneficialSignature) {
					this.beneficialSignature = beneficialSignature;
				}

				public String getSignatureDocument() {
					return signatureDocument;
				}

				public void setSignatureDocument(String signatureDocument) {
					this.signatureDocument = signatureDocument;
				}

				public String getBeneficialOwnerID() {
					return beneficialOwnerID;
				}

				public void setBeneficialOwnerID(String beneficialOwnerID) {
					this.beneficialOwnerID = beneficialOwnerID;
				}

				public String getIsDelete() {
					return isDelete;
				}

				public void setIsDelete(String isDelete) {
					this.isDelete = isDelete;
				}

			}

			public static class Nominee {

				private String nomineeUCICIfCustomer;
				private String nomineeName;
				private String nomineeRelationship;
				private String nomineeDOB;
				private String nomineeAge;
				private String guardianID;
				private String nomineeAddrSameAsProspectCurrAddr;
				private String nomineeEmailID;
				private String sharePercentage;
				private String shareAmount;
				private String nomineeNameInPassbook;
				private String nomineeID;
				private String isDelete;

				public String getNomineeUCICIfCustomer() {
					return nomineeUCICIfCustomer;
				}

				public void setNomineeUCICIfCustomer(String nomineeUCICIfCustomer) {
					this.nomineeUCICIfCustomer = nomineeUCICIfCustomer;
				}

				public String getNomineeName() {
					return nomineeName;
				}

				public void setNomineeName(String nomineeName) {
					this.nomineeName = nomineeName;
				}

				public String getNomineeRelationship() {
					return nomineeRelationship;
				}

				public void setNomineeRelationship(String nomineeRelationship) {
					this.nomineeRelationship = nomineeRelationship;
				}

				public String getNomineeDOB() {
					return nomineeDOB;
				}

				public void setNomineeDOB(String nomineeDOB) {
					this.nomineeDOB = nomineeDOB;
				}

				public String getNomineeAge() {
					return nomineeAge;
				}

				public void setNomineeAge(String nomineeAge) {
					this.nomineeAge = nomineeAge;
				}

				public String getGuardianID() {
					return guardianID;
				}

				public void setGuardianID(String guardianID) {
					this.guardianID = guardianID;
				}

				public String getNomineeAddrSameAsProspectCurrAddr() {
					return nomineeAddrSameAsProspectCurrAddr;
				}

				public void setNomineeAddrSameAsProspectCurrAddr(String nomineeAddrSameAsProspectCurrAddr) {
					this.nomineeAddrSameAsProspectCurrAddr = nomineeAddrSameAsProspectCurrAddr;
				}

				public String getNomineeEmailID() {
					return nomineeEmailID;
				}

				public void setNomineeEmailID(String nomineeEmailID) {
					this.nomineeEmailID = nomineeEmailID;
				}

				public String getSharePercentage() {
					return sharePercentage;
				}

				public void setSharePercentage(String sharePercentage) {
					this.sharePercentage = sharePercentage;
				}

				public String getShareAmount() {
					return shareAmount;
				}

				public void setShareAmount(String shareAmount) {
					this.shareAmount = shareAmount;
				}

				public String getNomineeNameInPassbook() {
					return nomineeNameInPassbook;
				}

				public void setNomineeNameInPassbook(String nomineeNameInPassbook) {
					this.nomineeNameInPassbook = nomineeNameInPassbook;
				}

				public String getNomineeID() {
					return nomineeID;
				}

				public void setNomineeID(String nomineeID) {
					this.nomineeID = nomineeID;
				}

				public String getIsDelete() {
					return isDelete;
				}

				public void setIsDelete(String isDelete) {
					this.isDelete = isDelete;
				}

			}

			public static class Guardian {

				private String guardianUCICIfCustomer;
				private String guardianName;
				private String guardianRelationToMinorNominee;
				private String guardianAddrSameAsProspectCurrAddr;
				private String guardianEmailID;
				private String guardianID;
				private String isDelete;

				public String getGuardianUCICIfCustomer() {
					return guardianUCICIfCustomer;
				}

				public void setGuardianUCICIfCustomer(String guardianUCICIfCustomer) {
					this.guardianUCICIfCustomer = guardianUCICIfCustomer;
				}

				public String getGuardianName() {
					return guardianName;
				}

				public void setGuardianName(String guardianName) {
					this.guardianName = guardianName;
				}

				public String getGuardianRelationToMinorNominee() {
					return guardianRelationToMinorNominee;
				}

				public void setGuardianRelationToMinorNominee(String guardianRelationToMinorNominee) {
					this.guardianRelationToMinorNominee = guardianRelationToMinorNominee;
				}

				public String getGuardianAddrSameAsProspectCurrAddr() {
					return guardianAddrSameAsProspectCurrAddr;
				}

				public void setGuardianAddrSameAsProspectCurrAddr(String guardianAddrSameAsProspectCurrAddr) {
					this.guardianAddrSameAsProspectCurrAddr = guardianAddrSameAsProspectCurrAddr;
				}

				public String getGuardianEmailID() {
					return guardianEmailID;
				}

				public void setGuardianEmailID(String guardianEmailID) {
					this.guardianEmailID = guardianEmailID;
				}

				public String getGuardianID() {
					return guardianID;
				}

				public void setGuardianID(String guardianID) {
					this.guardianID = guardianID;
				}

				public String getIsDelete() {
					return isDelete;
				}

				public void setIsDelete(String isDelete) {
					this.isDelete = isDelete;
				}

			}

			public static class GuardianAddress {

				private String addressLine1;
				private String addressLine2;
				private String addressLine3;
				private String pincode;
				private String city;
				private String district;
				private String state;
				private String country;
				private String distance;
				private String landmark;
				private String pobox;
				private String accommodation;
				private String currentStay;
				private String landline;
				private String mobileNumber;
				private String addressID;
				private String addressType;
				private String geatag;
				private String operation;
				private String isDeleteIDPresent;
				private String active;
				private String faxNumber;
				private String ID;
				private String associatedWith;

				public String getAddressLine1() {
					return addressLine1;
				}

				public void setAddressLine1(String addressLine1) {
					this.addressLine1 = addressLine1;
				}

				public String getAddressLine2() {
					return addressLine2;
				}

				public void setAddressLine2(String addressLine2) {
					this.addressLine2 = addressLine2;
				}

				public String getAddressLine3() {
					return addressLine3;
				}

				public void setAddressLine3(String addressLine3) {
					this.addressLine3 = addressLine3;
				}

				public String getPincode() {
					return pincode;
				}

				public void setPincode(String pincode) {
					this.pincode = pincode;
				}

				public String getCity() {
					return city;
				}

				public void setCity(String city) {
					this.city = city;
				}

				public String getDistrict() {
					return district;
				}

				public void setDistrict(String district) {
					this.district = district;
				}

				public String getState() {
					return state;
				}

				public void setState(String state) {
					this.state = state;
				}

				public String getCountry() {
					return country;
				}

				public void setCountry(String country) {
					this.country = country;
				}

				public String getDistance() {
					return distance;
				}

				public void setDistance(String distance) {
					this.distance = distance;
				}

				public String getLandmark() {
					return landmark;
				}

				public void setLandmark(String landmark) {
					this.landmark = landmark;
				}

				public String getPobox() {
					return pobox;
				}

				public void setPobox(String pobox) {
					this.pobox = pobox;
				}

				public String getAccommodation() {
					return accommodation;
				}

				public void setAccommodation(String accommodation) {
					this.accommodation = accommodation;
				}

				public String getCurrentStay() {
					return currentStay;
				}

				public void setCurrentStay(String currentStay) {
					this.currentStay = currentStay;
				}

				public String getLandline() {
					return landline;
				}

				public void setLandline(String landline) {
					this.landline = landline;
				}

				public String getMobileNumber() {
					return mobileNumber;
				}

				public void setMobileNumber(String mobileNumber) {
					this.mobileNumber = mobileNumber;
				}

				public String getAddressID() {
					return addressID;
				}

				public void setAddressID(String addressID) {
					this.addressID = addressID;
				}

				public String getAddressType() {
					return addressType;
				}

				public void setAddressType(String addressType) {
					this.addressType = addressType;
				}

				public String getGeatag() {
					return geatag;
				}

				public void setGeatag(String geatag) {
					this.geatag = geatag;
				}

				public String getOperation() {
					return operation;
				}

				public void setOperation(String operation) {
					this.operation = operation;
				}

				public String getIsDeleteIDPresent() {
					return isDeleteIDPresent;
				}

				public void setIsDeleteIDPresent(String isDeleteIDPresent) {
					this.isDeleteIDPresent = isDeleteIDPresent;
				}

				public String getActive() {
					return active;
				}

				public void setActive(String active) {
					this.active = active;
				}

				public String getFaxNumber() {
					return faxNumber;
				}

				public void setFaxNumber(String faxNumber) {
					this.faxNumber = faxNumber;
				}

				public String getID() {
					return ID;
				}

				public void setID(String iD) {
					ID = iD;
				}

				public String getAssociatedWith() {
					return associatedWith;
				}

				public void setAssociatedWith(String associatedWith) {
					this.associatedWith = associatedWith;
				}

			}

			public static class NomineeAddress {

				private String addressLine1;
				private String addressLine2;
				private String addressLine3;
				private String pincode;
				private String city;
				private String district;
				private String state;
				private String country;
				private String distance;
				private String landmark;
				private String pobox;
				private String accommodation;
				private String currentStay;
				private String landline;
				private String mobileNumber;
				private String addressID;
				private String addressType;
				private String geatag;
				private String operation;
				private String isDeleteIDPresent;
				private String active;
				private String faxNumber;
				private String ID;
				private String associatedWith;

				public String getAddressLine1() {
					return addressLine1;
				}

				public void setAddressLine1(String addressLine1) {
					this.addressLine1 = addressLine1;
				}

				public String getAddressLine2() {
					return addressLine2;
				}

				public void setAddressLine2(String addressLine2) {
					this.addressLine2 = addressLine2;
				}

				public String getAddressLine3() {
					return addressLine3;
				}

				public void setAddressLine3(String addressLine3) {
					this.addressLine3 = addressLine3;
				}

				public String getPincode() {
					return pincode;
				}

				public void setPincode(String pincode) {
					this.pincode = pincode;
				}

				public String getCity() {
					return city;
				}

				public void setCity(String city) {
					this.city = city;
				}

				public String getDistrict() {
					return district;
				}

				public void setDistrict(String district) {
					this.district = district;
				}

				public String getState() {
					return state;
				}

				public void setState(String state) {
					this.state = state;
				}

				public String getCountry() {
					return country;
				}

				public void setCountry(String country) {
					this.country = country;
				}

				public String getDistance() {
					return distance;
				}

				public void setDistance(String distance) {
					this.distance = distance;
				}

				public String getLandmark() {
					return landmark;
				}

				public void setLandmark(String landmark) {
					this.landmark = landmark;
				}

				public String getPobox() {
					return pobox;
				}

				public void setPobox(String pobox) {
					this.pobox = pobox;
				}

				public String getAccommodation() {
					return accommodation;
				}

				public void setAccommodation(String accommodation) {
					this.accommodation = accommodation;
				}

				public String getCurrentStay() {
					return currentStay;
				}

				public void setCurrentStay(String currentStay) {
					this.currentStay = currentStay;
				}

				public String getLandline() {
					return landline;
				}

				public void setLandline(String landline) {
					this.landline = landline;
				}

				public String getMobileNumber() {
					return mobileNumber;
				}

				public void setMobileNumber(String mobileNumber) {
					this.mobileNumber = mobileNumber;
				}

				public String getAddressID() {
					return addressID;
				}

				public void setAddressID(String addressID) {
					this.addressID = addressID;
				}

				public String getAddressType() {
					return addressType;
				}

				public void setAddressType(String addressType) {
					this.addressType = addressType;
				}

				public String getGeatag() {
					return geatag;
				}

				public void setGeatag(String geatag) {
					this.geatag = geatag;
				}

				public String getOperation() {
					return operation;
				}

				public void setOperation(String operation) {
					this.operation = operation;
				}

				public String getIsDeleteIDPresent() {
					return isDeleteIDPresent;
				}

				public void setIsDeleteIDPresent(String isDeleteIDPresent) {
					this.isDeleteIDPresent = isDeleteIDPresent;
				}

				public String getActive() {
					return active;
				}

				public void setActive(String active) {
					this.active = active;
				}

				public String getFaxNumber() {
					return faxNumber;
				}

				public void setFaxNumber(String faxNumber) {
					this.faxNumber = faxNumber;
				}

				public String getID() {
					return ID;
				}

				public void setID(String iD) {
					ID = iD;
				}

				public String getAssociatedWith() {
					return associatedWith;
				}

				public void setAssociatedWith(String associatedWith) {
					this.associatedWith = associatedWith;
				}

			}

			public static class AccountDetails_D0 {

				private String accountType;
				private String productCategory;
				private String accountOpeningFlow;
				private String sourceBranch;
				private String productCode;
				private String initialDepositType;
				private String fieldEmployeeCode;
				private String applicationDate;
				private String initialDepositAmount;
				private String priorityFlag;
				private String nlFound;

				public String getAccountType() {
					return accountType;
				}

				public void setAccountType(String accountType) {
					this.accountType = accountType;
				}

				public String getProductCategory() {
					return productCategory;
				}

				public void setProductCategory(String productCategory) {
					this.productCategory = productCategory;
				}

				public String getAccountOpeningFlow() {
					return accountOpeningFlow;
				}

				public void setAccountOpeningFlow(String accountOpeningFlow) {
					this.accountOpeningFlow = accountOpeningFlow;
				}

				public String getSourceBranch() {
					return sourceBranch;
				}

				public void setSourceBranch(String sourceBranch) {
					this.sourceBranch = sourceBranch;
				}

				public String getProductCode() {
					return productCode;
				}

				public void setProductCode(String productCode) {
					this.productCode = productCode;
				}

				public String getInitialDepositType() {
					return initialDepositType;
				}

				public void setInitialDepositType(String initialDepositType) {
					this.initialDepositType = initialDepositType;
				}

				public String getFieldEmployeeCode() {
					return fieldEmployeeCode;
				}

				public void setFieldEmployeeCode(String fieldEmployeeCode) {
					this.fieldEmployeeCode = fieldEmployeeCode;
				}

				public String getApplicationDate() {
					return applicationDate;
				}

				public void setApplicationDate(String applicationDate) {
					this.applicationDate = applicationDate;
				}

				public String getInitialDepositAmount() {
					return initialDepositAmount;
				}

				public void setInitialDepositAmount(String initialDepositAmount) {
					this.initialDepositAmount = initialDepositAmount;
				}

				public String getPriorityFlag() {
					return priorityFlag;
				}

				public void setPriorityFlag(String priorityFlag) {
					this.priorityFlag = priorityFlag;
				}

				public String getNlFound() {
					return nlFound;
				}

				public void setNlFound(String nlFound) {
					this.nlFound = nlFound;
				}

			}

			public static class CustomerAccountLeadRelationship_D0 {

				private String UCIC;
				private String customerLeadID;
				private String customerAccountRelation;
				private String isPrimaryHolder;
				private String relationToPrimaryHolder;
				private String isParentAccount;
				private String id;
				private String isDelete;
				private String leadState;
				private String leadStateMsg;
				private String entityType;
				private String customerAccountRelationTitle;
				private String relationToPrimaryHolderTitle;
				private String entityTypeTitle;
				private String accountLeadID;
				private String customerName;
				private String customerPhoneNumber;
				private String isInsert;
				private String completedDataEntryD0;
				private String completedDataEntryD1;
				private String completedVerifyingD2;
				private String completedDataEntryD2;
				private String illiterateFlag;
				private String NLFound;
				private String age;
				private String isMinor;
				private String isStaff;

				public String getUCIC() {
					return UCIC;
				}

				public void setUCIC(String uCIC) {
					UCIC = uCIC;
				}

				public String getCustomerLeadID() {
					return customerLeadID;
				}

				public void setCustomerLeadID(String customerLeadID) {
					this.customerLeadID = customerLeadID;
				}

				public String getCustomerAccountRelation() {
					return customerAccountRelation;
				}

				public void setCustomerAccountRelation(String customerAccountRelation) {
					this.customerAccountRelation = customerAccountRelation;
				}

				public String getIsPrimaryHolder() {
					return isPrimaryHolder;
				}

				public void setIsPrimaryHolder(String isPrimaryHolder) {
					this.isPrimaryHolder = isPrimaryHolder;
				}

				public String getRelationToPrimaryHolder() {
					return relationToPrimaryHolder;
				}

				public void setRelationToPrimaryHolder(String relationToPrimaryHolder) {
					this.relationToPrimaryHolder = relationToPrimaryHolder;
				}

				public String getIsParentAccount() {
					return isParentAccount;
				}

				public void setIsParentAccount(String isParentAccount) {
					this.isParentAccount = isParentAccount;
				}

				public String getId() {
					return id;
				}

				public void setId(String id) {
					this.id = id;
				}

				public String getIsDelete() {
					return isDelete;
				}

				public void setIsDelete(String isDelete) {
					this.isDelete = isDelete;
				}

				public String getLeadState() {
					return leadState;
				}

				public void setLeadState(String leadState) {
					this.leadState = leadState;
				}

				public String getLeadStateMsg() {
					return leadStateMsg;
				}

				public void setLeadStateMsg(String leadStateMsg) {
					this.leadStateMsg = leadStateMsg;
				}

				public String getEntityType() {
					return entityType;
				}

				public void setEntityType(String entityType) {
					this.entityType = entityType;
				}

				public String getCustomerAccountRelationTitle() {
					return customerAccountRelationTitle;
				}

				public void setCustomerAccountRelationTitle(String customerAccountRelationTitle) {
					this.customerAccountRelationTitle = customerAccountRelationTitle;
				}

				public String getRelationToPrimaryHolderTitle() {
					return relationToPrimaryHolderTitle;
				}

				public void setRelationToPrimaryHolderTitle(String relationToPrimaryHolderTitle) {
					this.relationToPrimaryHolderTitle = relationToPrimaryHolderTitle;
				}

				public String getEntityTypeTitle() {
					return entityTypeTitle;
				}

				public void setEntityTypeTitle(String entityTypeTitle) {
					this.entityTypeTitle = entityTypeTitle;
				}

				public String getAccountLeadID() {
					return accountLeadID;
				}

				public void setAccountLeadID(String accountLeadID) {
					this.accountLeadID = accountLeadID;
				}

				public String getCustomerName() {
					return customerName;
				}

				public void setCustomerName(String customerName) {
					this.customerName = customerName;
				}

				public String getCustomerPhoneNumber() {
					return customerPhoneNumber;
				}

				public void setCustomerPhoneNumber(String customerPhoneNumber) {
					this.customerPhoneNumber = customerPhoneNumber;
				}

				public String getIsInsert() {
					return isInsert;
				}

				public void setIsInsert(String isInsert) {
					this.isInsert = isInsert;
				}

				public String getCompletedDataEntryD0() {
					return completedDataEntryD0;
				}

				public void setCompletedDataEntryD0(String completedDataEntryD0) {
					this.completedDataEntryD0 = completedDataEntryD0;
				}

				public String getCompletedDataEntryD1() {
					return completedDataEntryD1;
				}

				public void setCompletedDataEntryD1(String completedDataEntryD1) {
					this.completedDataEntryD1 = completedDataEntryD1;
				}

				public String getCompletedVerifyingD2() {
					return completedVerifyingD2;
				}

				public void setCompletedVerifyingD2(String completedVerifyingD2) {
					this.completedVerifyingD2 = completedVerifyingD2;
				}

				public String getCompletedDataEntryD2() {
					return completedDataEntryD2;
				}

				public void setCompletedDataEntryD2(String completedDataEntryD2) {
					this.completedDataEntryD2 = completedDataEntryD2;
				}

				public String getIlliterateFlag() {
					return illiterateFlag;
				}

				public void setIlliterateFlag(String illiterateFlag) {
					this.illiterateFlag = illiterateFlag;
				}

				public String getNLFound() {
					return NLFound;
				}

				public void setNLFound(String nLFound) {
					NLFound = nLFound;
				}

				public String getAge() {
					return age;
				}

				public void setAge(String age) {
					this.age = age;
				}

				public String getIsMinor() {
					return isMinor;
				}

				public void setIsMinor(String isMinor) {
					this.isMinor = isMinor;
				}

				public String getIsStaff() {
					return isStaff;
				}

				public void setIsStaff(String isStaff) {
					this.isStaff = isStaff;
				}

			}

			public static class CustomerAgeDetails {

				private String customerLeadID;
				private String UCIC;
				private String isMinor;
				private String age;

				public String getCustomerLeadID() {
					return customerLeadID;
				}

				public void setCustomerLeadID(String customerLeadID) {
					this.customerLeadID = customerLeadID;
				}

				public String getUCIC() {
					return UCIC;
				}

				public void setUCIC(String uCIC) {
					UCIC = uCIC;
				}

				public String getIsMinor() {
					return isMinor;
				}

				public void setIsMinor(String isMinor) {
					this.isMinor = isMinor;
				}

				public String getAge() {
					return age;
				}

				public void setAge(String age) {
					this.age = age;
				}

			}

			public static class CustPreferEmail {

				private String UCIC;
				private String EmailID;
				private String LeadID;

				public String getUCIC() {
					return UCIC;
				}

				public void setUCIC(String uCIC) {
					UCIC = uCIC;
				}

				public String getEmailID() {
					return EmailID;
				}

				public void setEmailID(String emailID) {
					EmailID = emailID;
				}

				public String getLeadID() {
					return LeadID;
				}

				public void setLeadID(String leadID) {
					LeadID = leadID;
				}

			}

			public static class ChequeBookLov {

				private String label;
				private String value;

				public String getLabel() {
					return label;
				}

				public void setLabel(String label) {
					this.label = label;
				}

				public String getValue() {
					return value;
				}

				public void setValue(String value) {
					this.value = value;
				}

			}

			public static class BulkAccountDetails {

				private String AccountNumber;
				private String UCIC;

				public String getAccountNumber() {
					return AccountNumber;
				}

				public void setAccountNumber(String accountNumber) {
					AccountNumber = accountNumber;
				}

				public String getUCIC() {
					return UCIC;
				}

				public void setUCIC(String uCIC) {
					UCIC = uCIC;
				}

			}
		}
	}

}
