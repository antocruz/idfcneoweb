package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.AddDocumentMessageBody;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.dto.KycRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("addDocumentReq")
public class AddDocumentReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;
	
	protected LeadMessageHeader leadMessageHeader;
	protected AddDocumentMessageBody addDocumentMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public AddDocumentMessageBody getAddDocumentMessageBody() {
		return addDocumentMessageBody;
	}

	public void setAddDocumentMessageBody(AddDocumentMessageBody addDocumentMessageBody) {
		this.addDocumentMessageBody = addDocumentMessageBody;
	}
	
	public AddDocumentReqDto build(KycRequestDto request) throws NeoException {

		AddDocumentReqDto requestDto = null;

		try {
			
			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);
			
			AddDocumentMessageBody addDocumentMessage = equitasCommonService.buildAddDocumentMessageBody(request);
			
			requestDto = AddDocumentReqDto.builder().addDocumentMessageBody(addDocumentMessage).leadMessageHeader(leadMessage)
					.build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}

}
