package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RedeemDigiTDMaturityDetailsMessageBody {

	private RedeemDigiDepositReq redeemDigiDepositReq;

	@JsonProperty("RedeemDigiDepositReq")
	public RedeemDigiDepositReq getRedeemDigiDepositReq() {
		return redeemDigiDepositReq;
	}

	public void setRedeemDigiDepositReq(RedeemDigiDepositReq redeemDigiDepositReq) {
		this.redeemDigiDepositReq = redeemDigiDepositReq;
	}
	
	
	
}
