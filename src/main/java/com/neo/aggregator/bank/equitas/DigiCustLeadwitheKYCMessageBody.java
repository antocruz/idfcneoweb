package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DigiCustLeadwitheKYCMessageBody {

	EKycDetails eKYCDetails;
	LeadDetails leadDetails;

	@JsonProperty("eKYCDetails")
	public EKycDetails geteKYCDetails() {
		return eKYCDetails;
	}

	public void seteKYCDetails(EKycDetails eKYCDetails) {
		this.eKYCDetails = eKYCDetails;
	}

	@JsonProperty("leadDetails")
	public LeadDetails getLeadDetails() {
		return leadDetails;
	}

	public void setLeadDetails(LeadDetails leadDetails) {
		this.leadDetails = leadDetails;
	}

}
