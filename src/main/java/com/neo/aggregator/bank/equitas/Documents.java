package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Documents {

	private String documentNo;
	private String associatedWith;
	private String documentType;
	private String mappedCustomerLead;
	private String categoryCode;
	private String dmsDocumentId;
	private String subcategoryCode;

	@JsonProperty("documentNo")
	public String getDocumentNo() {
		return documentNo;
	}

	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}

	@JsonProperty("associatedWith")
	public String getAssociatedWith() {
		return associatedWith;
	}

	public void setAssociatedWith(String associatedWith) {
		this.associatedWith = associatedWith;
	}

	@JsonProperty("documentType")
	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	@JsonProperty("mappedCustomerLead")
	public String getMappedCustomerLead() {
		return mappedCustomerLead;
	}

	public void setMappedCustomerLead(String mappedCustomerLead) {
		this.mappedCustomerLead = mappedCustomerLead;
	}

	@JsonProperty("categoryCode")
	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	@JsonProperty("dmsDocumentId")
	public String getDmsDocumentId() {
		return dmsDocumentId;
	}

	public void setDmsDocumentId(String dmsDocumentId) {
		this.dmsDocumentId = dmsDocumentId;
	}

	@JsonProperty("subcategoryCode")
	public String getSubcategoryCode() {
		return subcategoryCode;
	}

	public void setSubcategoryCode(String subcategoryCode) {
		this.subcategoryCode = subcategoryCode;
	}

}
