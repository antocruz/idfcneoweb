package com.neo.aggregator.bank.equitas;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateAccountLeadMessageBody {

	private List<CustomerPreferences> customerPreferences;
	private List<NomineeAddress> nomineeAddress;
	private List<CustomerAccountLeadRelation> customerAccountLeadRelation;
	private List<GuardianAddress> guardianAddress;
	private List<Nominee> nominee;
	private List<Guardian> guardian;
	private List<CustomerDeliverables> customerDeliverables;
	private AccountLead accountLead;
	private String usrId;
	private String accountLeadID;
	private Boolean fromMADP;
	private String authenticationToken;
	private String userCreator;
	private String PmayIndicator;
	private String PmaySuccessIndicator;
	private String isQC;
	private String instaKitIndicator;
	private String instaKitSuccessIndicator;
	

	@JsonProperty("customerPreferences")
	public List<CustomerPreferences> getCustomerPreferences() {
		return customerPreferences;
	}

	public void setCustomerPreferences(List<CustomerPreferences> customerPreferences) {
		this.customerPreferences = customerPreferences;
	}

	@JsonProperty("nomineeAddress")
	public List<NomineeAddress> getNomineeAddress() {
		return nomineeAddress;
	}

	public void setNomineeAddress(List<NomineeAddress> nomineeAddress) {
		this.nomineeAddress = nomineeAddress;
	}

	@JsonProperty("customerAccountLeadRelation")
	public List<CustomerAccountLeadRelation> getCustomerAccountLeadRelation() {
		return customerAccountLeadRelation;
	}

	public void setCustomerAccountLeadRelation(List<CustomerAccountLeadRelation> customerAccountLeadRelation) {
		this.customerAccountLeadRelation = customerAccountLeadRelation;
	}

	@JsonProperty("guardianAddress")
	public List<GuardianAddress> getGuardianAddress() {
		return guardianAddress;
	}

	public void setGuardianAddress(List<GuardianAddress> guardianAddress) {
		this.guardianAddress = guardianAddress;
	}

	@JsonProperty("nominee")
	public List<Nominee> getNominee() {
		return nominee;
	}

	public void setNominee(List<Nominee> nominee) {
		this.nominee = nominee;
	}

	@JsonProperty("guardian")
	public List<Guardian> getGuardian() {
		return guardian;
	}

	public void setGuardian(List<Guardian> guardian) {
		this.guardian = guardian;
	}

	@JsonProperty("customerDeliverables")
	public List<CustomerDeliverables> getCustomerDeliverables() {
		return customerDeliverables;
	}

	public void setCustomerDeliverables(List<CustomerDeliverables> customerDeliverables) {
		this.customerDeliverables = customerDeliverables;
	}

	@JsonProperty("accountLead")
	public AccountLead getAccountLead() {
		return accountLead;
	}

	public void setAccountLead(AccountLead accountLead) {
		this.accountLead = accountLead;
	}

	@JsonProperty("usrId")
	public String getUsrId() {
		return usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	@JsonProperty("accountLeadID")
	public String getAccountLeadID() {
		return accountLeadID;
	}

	public void setAccountLeadID(String accountLeadID) {
		this.accountLeadID = accountLeadID;
	}

	@JsonProperty("fromMADP")
	public Boolean getFromMADP() {
		return fromMADP;
	}

	public void setFromMADP(Boolean fromMADP) {
		this.fromMADP = fromMADP;
	}

	@JsonProperty("authenticationToken")
	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	@JsonProperty("userCreator")
	public String getUserCreator() {
		return userCreator;
	}

	public void setUserCreator(String userCreator) {
		this.userCreator = userCreator;
	}

	@JsonProperty("PmayIndicator")
	public String getPmayIndicator() {
		return PmayIndicator;
	}

	public void setPmayIndicator(String pmayIndicator) {
		PmayIndicator = pmayIndicator;
	}

	@JsonProperty("PmaySuccessIndicator")
	public String getPmaySuccessIndicator() {
		return PmaySuccessIndicator;
	}

	public void setPmaySuccessIndicator(String pmaySuccessIndicator) {
		PmaySuccessIndicator = pmaySuccessIndicator;
	}

	@JsonProperty("isQC")
	public String getIsQC() {
		return isQC;
	}

	public void setIsQC(String isQC) {
		this.isQC = isQC;
	}

	@JsonProperty("instaKitIndicator")
	public String getInstaKitIndicator() {
		return instaKitIndicator;
	}

	public void setInstaKitIndicator(String instaKitIndicator) {
		this.instaKitIndicator = instaKitIndicator;
	}

	@JsonProperty("instaKitSuccessIndicator")
	public String getInstaKitSuccessIndicator() {
		return instaKitSuccessIndicator;
	}

	public void setInstaKitSuccessIndicator(String instaKitSuccessIndicator) {
		this.instaKitSuccessIndicator = instaKitSuccessIndicator;
	}

}
