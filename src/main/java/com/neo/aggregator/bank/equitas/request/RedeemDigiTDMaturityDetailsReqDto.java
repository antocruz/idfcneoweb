package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.RedeemDigiTDMaturityDetailsMessageBody;
import com.neo.aggregator.bank.equitas.dto.DigiAccountRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("redeemDigiDepositRequest")
public class RedeemDigiTDMaturityDetailsReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;
	
	protected LeadMessageHeader leadMessageHeader;
	protected RedeemDigiTDMaturityDetailsMessageBody redeemDigiTDMaturityDetailsMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public RedeemDigiTDMaturityDetailsMessageBody getRedeemDigiTDMaturityDetailsMessageBody() {
		return redeemDigiTDMaturityDetailsMessageBody;
	}

	public void setRedeemDigiTDMaturityDetailsMessageBody(
			RedeemDigiTDMaturityDetailsMessageBody redeemDigiTDMaturityDetailsMessageBody) {
		this.redeemDigiTDMaturityDetailsMessageBody = redeemDigiTDMaturityDetailsMessageBody;
	}

	
	public RedeemDigiTDMaturityDetailsReqDto build(DigiAccountRequestDto request) throws NeoException {

		RedeemDigiTDMaturityDetailsReqDto requestDto = null;

		try {
			
			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);
			
			
			RedeemDigiTDMaturityDetailsMessageBody redeemDigiTDMaturityDetailsMessage = equitasCommonService.buildRedeemDigiTDMaturityDetailsMessageBody(request);
			
			requestDto = RedeemDigiTDMaturityDetailsReqDto.builder().redeemDigiTDMaturityDetailsMessageBody(redeemDigiTDMaturityDetailsMessage).leadMessageHeader(leadMessage)
					.build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}


}
