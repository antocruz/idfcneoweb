package com.neo.aggregator.bank.equitas.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("generateDigiOTPResponse")
public class GenerateDigiOTPResDto {

	@JsonProperty("msgHdr")
	protected GenerateDigiOTPResDto.Header msgHdr;
	@JsonProperty("msgBdy")
	protected GenerateDigiOTPResDto.Body msgBdy;

	public GenerateDigiOTPResDto.Header getMsgHdr() {
		return msgHdr;
	}

	public void setMsgHdr(GenerateDigiOTPResDto.Header msgHdr) {
		this.msgHdr = msgHdr;
	}

	public GenerateDigiOTPResDto.Body getMsgBdy() {
		return msgBdy;
	}

	public void setMsgBdy(GenerateDigiOTPResDto.Body msgBdy) {
		this.msgBdy = msgBdy;
	}

	public static class Header {
		@JsonProperty(value = "rslt")
		protected String rslt;

		@JsonProperty(value = "error")
		protected List<Error> error;

		public List<Error> getError() {
			return error;
		}

		public void setError(List<Error> error) {
			this.error = error;
		}

		public String getRslt() {
			return rslt;
		}

		public void setRslt(String rslt) {
			this.rslt = rslt;
		}

		public static class Error {

			@JsonProperty(value = "cd")
			protected String cd;
			@JsonProperty(value = "rsn")
			protected String rsn;

			public String getCd() {
				return cd;
			}

			public void setCd(String cd) {
				this.cd = cd;
			}

			public String getRsn() {
				return rsn;
			}

			public void setRsn(String rsn) {
				this.rsn = rsn;
			}
		}
	}

	public static class Body {
		
		@JsonProperty(value = "eKYCXMLStringRepPayload")
		protected String eKYCXMLStringRepPayload;

		public String geteKYCXMLStringRepPayload() {
			return eKYCXMLStringRepPayload;
		}

		public void seteKYCXMLStringRepPayload(String eKYCXMLStringRepPayload) {
			this.eKYCXMLStringRepPayload = eKYCXMLStringRepPayload;
		}

	}

}
