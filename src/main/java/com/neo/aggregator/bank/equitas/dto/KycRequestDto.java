package com.neo.aggregator.bank.equitas.dto;

import com.neo.aggregator.bank.equitas.AddDocRq;
import com.neo.aggregator.bank.equitas.AddressDetails;
import com.neo.aggregator.bank.equitas.DocRefId;
import com.neo.aggregator.bank.equitas.Documents;
import com.neo.aggregator.dto.RegistrationRequestDtoV2;
import com.neo.aggregator.enums.GenderType;
import com.neo.aggregator.dto.Wadh;

import lombok.Data;

@Data
public class KycRequestDto {

	private String title;
	private String firstName;
	private String lastName;
	private String dob;
	private GenderType gender;
	private String city;
	private String state;
	private String DocumentNo;
	private RegistrationRequestDtoV2 registrationRequestDtoV2;
	private String usrId;
	private String authenticationToken;
	private String custLeadId;
	private String custId;
	private String motherMaidenName;
	private String identityType;
	private String aadhar;
	private String middleName;
	private String PAN;
	private String entityFlagType;
	private String sourceBranch;
	private String homeBranch;
	private String fatherName;
	private String emailID;
	private String employeeNumber;
	private String nationality;
	private String maritalStatus;
	private String occupation;
	private String grossAnnualIncome;
	private Boolean priorityLead;
	private Boolean taxResident;
	private Integer age;
	private String estimatedAgriculturalIncome;
	private String estimatedNonAgriculturalIncome;
	private String cityOfBirth;
	private String countryOfBirth;
	private AddressDetails[] addressDetails;
	private String associatedWith;
	private Boolean isCurrentAddressSameAsPermanent;
	private String entityIdentityDetails;
	private String ID;
	private Documents[] kycDocuments;
	private String authToken;
	private String mappedCustomerLead;
	private String categoryCode;
	private String dmsDocumentId;
	private String subcategoryCode;
	private String usrTkn;
	private String usrNm;
	private String usrPwd;
	private AddDocRq[] addDocRq;
	private String docTp;
	private String docSbCtgry;
	private String docNm;
	private int docCtgryCd;
	private String docCatg;
	private int docTypCd;
	private int docSbCtgryCd;
	private String flLoc;
	private String docCmnts;
	private String bsPyld;
	private DocRefId[] docRefId;
	private String idTP;
	private int docId;
	private String ekycxmlreqPayload;
	private String shortName;
	private String ifullName;
	private String nlFound;
	private String reasonNotApplicable;
	private String reason;
	private String voterid;
	private String pidBlock;
	private String tid;
	private String location;
	private String timestamp;
	private String pfr;
	private String txnId;
	private String otp;
	private Wadh wadh;
	private String district;
	private String landline;
	private String landmark;
	private String stan;
	private Boolean isPhtRequired;
	private String pincode;
	private String appId;
	private Boolean ignoreProbableMatch;
	private String entityType;
	private Boolean fromMADP;
	private String deDupChkReqByCustCount;
	private Boolean isAadhar;
	private Boolean mappedToAccountLead;
	private String purposeOfCreation;
	private String branchId;
	private String authUserId;
	private String preferredLanguage;
	private String mcc;
	private String posEntryMode;
	private String posCode;
	private String caId;
	private String caTid;
	private String caTa;
	private String politicallyExposedPerson;
	private String country;
	private String add;
	private String idTypeValidation;
	private String idNumber;
	private String skey;
	private String ci;
	private String mc;
	private String dataType;
	private String dataValue;
	private String hmac;
	private String mi;
	private String rdsId;
	private String rdsVer;
	private String dpId;
	private String dc;
	private String entityId;
	private String consent;
	private String virtualId;
	private String otpVerificationCode;
	private String agentName;
	private String agentUcic;
	private String agentAccountNumber;
	private String agentAddress;
	private String agentCity;
	private String agentState;
	private String agentPincode;
	private String corporateName;
	private String corporateAddress;
	private String corporateCity;
	private String corporateState;
	private String corporatePincode;
	private String fundSource;
	private String signature;
	private String occuapation;
	private String sourceOfLead;
	private String otherSourcesOfLead;
	
	private Boolean isAgriculturalIncome;
	
}
