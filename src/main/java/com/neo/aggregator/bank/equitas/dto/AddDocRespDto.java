package com.neo.aggregator.bank.equitas.dto;

import lombok.Data;

@Data
public class AddDocRespDto {
	private String sts;
	private String docNm;
	private String docIndx;
}
