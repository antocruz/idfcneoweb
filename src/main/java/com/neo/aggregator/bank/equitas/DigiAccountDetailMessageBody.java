package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DigiAccountDetailMessageBody {

	private String acctTp;
	private String accountNo;

	@JsonProperty("acctTp")
	public String getAcctTp() {
		return acctTp;
	}

	public void setAcctTp(String acctTp) {
		this.acctTp = acctTp;
	}

	@JsonProperty("accountNo")
	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

}
