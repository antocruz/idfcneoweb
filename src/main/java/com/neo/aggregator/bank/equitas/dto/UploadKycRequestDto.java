package com.neo.aggregator.bank.equitas.dto;

import java.util.List;

import lombok.Data;

@Data
public class UploadKycRequestDto {

	private String entityId;

	private List<Document> documents;
	
	@Data
	public static class Document {
		private String documentType;
		private String documentNo;
		private String documentExpDate;
		private String documentFilePath;
		private String documentFileName;
		private String ekycRefNo;
	}
}
