package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neo.aggregator.enums.GenderType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AboutProspectDetails {

	private String lastName;
	private String fatherName;
	private String preferredLanguage;
	private GenderType gender;
	private String politicallyExposedPerson;
	private String mobileNumber;
	private String emailID;
	private String title;
	private String employeeNumber;
	private Boolean priorityLead;
	private String firstName;
	private String nationality;
	private String dob;
	private String middleName;
	private Integer age;
	private String maritalStatus;
	private String occupation;
	private String grossAnnualIncome;
	private String occuapation;

	@JsonProperty("lastName")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@JsonProperty("fatherName")
	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	@JsonProperty("preferredLanguage")
	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	@JsonProperty("gender")
	public GenderType getGender() {
		return gender;
	}

	public void setGender(GenderType gender) {
		this.gender = gender;
	}

	@JsonProperty("politicallyExposedPerson")
	public String getPoliticallyExposedPerson() {
		return politicallyExposedPerson;
	}

	public void setPoliticallyExposedPerson(String politicallyExposedPerson) {
		this.politicallyExposedPerson = politicallyExposedPerson;
	}

	@JsonProperty("mobileNumber")
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@JsonProperty("emailID")
	public String getEmailId() {
		return emailID;
	}

	public void setEmailId(String emailID) {
		this.emailID = emailID;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@JsonProperty("employeeNumber")
	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	@JsonProperty("priorityLead")
	public Boolean getPriorityLead() {
		return priorityLead;
	}

	public void setPriorityLead(Boolean priorityLead) {
		this.priorityLead = priorityLead;
	}

	@JsonProperty("firstName")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@JsonProperty("nationality")
	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	@JsonProperty("dob")
	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	@JsonProperty("middleName")
	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@JsonProperty("age")
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@JsonProperty("maritalStatus")
	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	@JsonProperty("occupation")
	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	@JsonProperty("grossAnnualIncome")
	public String getGrossAnnualIncome() {
		return grossAnnualIncome;
	}

	public void setGrossAnnualIncome(String grossAnnualIncome) {
		this.grossAnnualIncome = grossAnnualIncome;
	}

	@JsonProperty("occuapation")
	public String getOccuapation() {
		return occuapation;
	}

	public void setOccuapation(String occuapation) {
		this.occuapation = occuapation;
	}
	
	

}
