package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerAccountLeadRelation {

	private Boolean isPrimaryHolder;
	private Boolean isInsert;
	private String customerAccountRelation;
	private String id;
	private String completedDataEntryD0;
	private String ucic;

	@JsonProperty("isPrimaryHolder")
	public Boolean getIsPrimaryHolder() {
		return isPrimaryHolder;
	}

	public void setIsPrimaryHolder(Boolean isPrimaryHolder) {
		this.isPrimaryHolder = isPrimaryHolder;
	}

	@JsonProperty("isInsert")
	public Boolean getIsInsert() {
		return isInsert;
	}

	public void setIsInsert(Boolean isInsert) {
		this.isInsert = isInsert;
	}

	@JsonProperty("customerAccountRelation")
	public String getCustomerAccountRelation() {
		return customerAccountRelation;
	}

	public void setCustomerAccountRelation(String customerAccountRelation) {
		this.customerAccountRelation = customerAccountRelation;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("completedDataEntryD0")
	public String getCompletedDataEntryD0() {
		return completedDataEntryD0;
	}

	public void setCompletedDataEntryD0(String completedDataEntryD0) {
		this.completedDataEntryD0 = completedDataEntryD0;
	}

	@JsonProperty("ucic")
	public String getUcic() {
		return ucic;
	}

	public void setUcic(String ucic) {
		this.ucic = ucic;
	}

}
