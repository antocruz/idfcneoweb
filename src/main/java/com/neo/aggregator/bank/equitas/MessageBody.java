package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessageBody {
	private String authenticationToken;
	private String custLeadId;
	private String associatedWith;

	@JsonProperty("authenticationToken")
	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	@JsonProperty("custLeadID")
	public String getCustLeadId() {
		return custLeadId;
	}

	public void setCustLeadId(String custLeadId) {
		this.custLeadId = custLeadId;
	}

	@JsonProperty("associatedWith")
	public String getAssociatedWith() {
		return associatedWith;
	}

	public void setAssociatedWith(String associatedWith) {
		this.associatedWith = associatedWith;
	}

}
