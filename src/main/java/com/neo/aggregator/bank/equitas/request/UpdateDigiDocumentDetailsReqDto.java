package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.UpdateDigiDocumentMessageBody;
import com.neo.aggregator.bank.equitas.dto.KycRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("updateDigiDocumentDetailsReq")
public class UpdateDigiDocumentDetailsReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;
	
	protected LeadMessageHeader leadMessageHeader;
	protected UpdateDigiDocumentMessageBody updateDigiDocumentMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public UpdateDigiDocumentMessageBody getUpdateDigiDocumentMessageBody() {
		return updateDigiDocumentMessageBody;
	}

	public void setUpdateDigiDocumentMessageBody(UpdateDigiDocumentMessageBody updateDigiDocumentMessageBody) {
		this.updateDigiDocumentMessageBody = updateDigiDocumentMessageBody;
	}

	
	public UpdateDigiDocumentDetailsReqDto build(KycRequestDto request) throws NeoException {

		UpdateDigiDocumentDetailsReqDto requestDto = null;

		try {
			
			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);
			
			
			UpdateDigiDocumentMessageBody updateDigiDocumentMessage = equitasCommonService.buildDigiDocumentMessageBody(request);
			
			requestDto = UpdateDigiDocumentDetailsReqDto.builder().updateDigiDocumentMessageBody(updateDigiDocumentMessage).leadMessageHeader(leadMessage)
					.build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}
}
