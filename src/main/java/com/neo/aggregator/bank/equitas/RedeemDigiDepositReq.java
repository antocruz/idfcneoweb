package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RedeemDigiDepositReq {

	private String rdmptnToAcct;
	private String rdmptnAmt;
	private String dpstNb;
	private String acctId;
	private String txnTp;
	private String userId;

	@JsonProperty("rdmptnToAcct")
	public String getRdmptnToAcct() {
		return rdmptnToAcct;
	}

	public void setRdmptnToAcct(String rdmptnToAcct) {
		this.rdmptnToAcct = rdmptnToAcct;
	}

	@JsonProperty("rdmptnAmt")
	public String getRdmptnAmt() {
		return rdmptnAmt;
	}

	public void setRdmptnAmt(String rdmptnAmt) {
		this.rdmptnAmt = rdmptnAmt;
	}

	@JsonProperty("dpstNb")
	public String getDpstNb() {
		return dpstNb;
	}

	public void setDpstNb(String dpstNb) {
		this.dpstNb = dpstNb;
	}

	@JsonProperty("acctId")
	public String getAcctId() {
		return acctId;
	}

	public void setAcctId(String acctId) {
		this.acctId = acctId;
	}

	@JsonProperty("txnTp")
	public String getTxnTp() {
		return txnTp;
	}

	public void setTxnTp(String txnTp) {
		this.txnTp = txnTp;
	}

	@JsonProperty("userId")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
