package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountLead {

	private String sourceBranch;
	private Boolean isPriorityLead;
	private String sourcingChannel;
	private String initialDepositType;
	private String accountTitle;
	private Boolean isDelete;
	private String fieldEmployeeCode;
	private String accountType;
	private String accountOpeningFlow;
	private String currencyOfDeposit;
	private String productCategory;
	private String sourceOfFund;
	private String initialDepositAmount;
	private String initialDepositMode;
	private String productCode;
	private String modeOfOperation;
	private Boolean isRestrictedAccount;
	private String sourceofLead;
	private String otherSourceOfLead;
	private Boolean fatcaDeclaration;
	private String accountOpeningBranch;
	private String MICreditToESFBAccountNo;
	private String leadAssignedTo;
	private String cashTransactionRefNo;
	private String transactionNarration;
	private String accountOpeningValueDate;
	private String purposeOfOpeningAccount;
	private String interestPayout;
	private String interestCompoundFrequency;
	private String tenureDays;
	private String MICreditToOtherBankAccountNo;
	private String chequeIssuedBank;
	private String iPayToOtherBankName;
	private String depositAmount;
	private String iPayToOtherBankIFSC;
	private String tenureMonths;
	private String chequeNo;
	private String maturityInstruction;
	private String MICreditToOtherBankBranch;
	private String iPayToOtherBankBranch;
	private String iPayToOtherBankBenificiaryName;
	private String MICreditToOtherBankIFSC;
	private String iPayToESFBAccountNo;
	private String productVariant;
	private String fromESFBAccountNo;
	private String iPayToOtherBankAccountNo;
	private String MICreditToOtherBankName;
	private String campaignCode;
	private String isNRIAccount;
	private String NREAccountType;
	private String NROAccountType;
	private String registeredForSweepIn;
	private String cashTransactionDate;
	private String chequeTransactionDate;
	private String fromESFBGLAccount;
	private String branchCodeGL;
	private String effectiveInterestRate;
	private String specialInterestRateRequired;
	private String specialInterestRate;
	private String specialInterestRequestID;
	private String depositVariance;
	private String iPayToOtherBankMICR;
	private String iPByDDPOIssuerCode;
	private String iPByDDPOPayeeName;
	private String MICreditToOtherBankAccountType;
	private String MICreditToOtherBankMICR;
	private String MIBByDDPOIssuerCode;
	private String MIByDDPOPayeeName;
	private String installmentAmount;
	private String periodicity;
	private String debitAccount;
	private String SIFrequency;
	private String SIDebitAmount;
	private String FATCADeclaration;
	private String favouriteAccountNo;
	private String favouriteAccountCriteria;
	private String accountNumber;
	private String accountCreatedOn;
	private String partOfAccountGroup;
	private String purposeOfOpeningAccountOthers;
	private String expectedMonthlyRemittance;
	private String expectedMonthlyWithdrawal;
	private String subscripedForLPG;
	private String isTDSAvailable;
	private String waiveOffTDS;
	private String leadCreatedBy;
	private String leadCreatedOn;
	private String leadModifiedBy;
	private String leadModifiedOn;
	private String otherSourceOfFund;

	@JsonProperty("sourceBranch")
	public String getSourceBranch() {
		return sourceBranch;
	}

	public void setSourceBranch(String sourceBranch) {
		this.sourceBranch = sourceBranch;
	}

	@JsonProperty("isPriorityLead")
	public Boolean getIsPriorityLead() {
		return isPriorityLead;
	}

	public void setIsPriorityLead(Boolean isPriorityLead) {
		this.isPriorityLead = isPriorityLead;
	}

	@JsonProperty("sourcingChannel")
	public String getSourcingChannel() {
		return sourcingChannel;
	}

	public void setSourcingChannel(String sourcingChannel) {
		this.sourcingChannel = sourcingChannel;
	}

	@JsonProperty("initialDepositType")
	public String getInitialDepositType() {
		return initialDepositType;
	}

	public void setInitialDepositType(String initialDepositType) {
		this.initialDepositType = initialDepositType;
	}

	@JsonProperty("accountTitle")
	public String getAccountTitle() {
		return accountTitle;
	}

	public void setAccountTitle(String accountTitle) {
		this.accountTitle = accountTitle;
	}

	@JsonProperty("isDelete")
	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	@JsonProperty("fieldEmployeeCode")
	public String getFieldEmployeeCode() {
		return fieldEmployeeCode;
	}

	public void setFieldEmployeeCode(String fieldEmployeeCode) {
		this.fieldEmployeeCode = fieldEmployeeCode;
	}

	@JsonProperty("accountType")
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	@JsonProperty("accountOpeningFlow")
	public String getAccountOpeningFlow() {
		return accountOpeningFlow;
	}

	public void setAccountOpeningFlow(String accountOpeningFlow) {
		this.accountOpeningFlow = accountOpeningFlow;
	}

	@JsonProperty("currencyOfDeposit")
	public String getCurrencyOfDeposit() {
		return currencyOfDeposit;
	}

	public void setCurrencyOfDeposit(String currencyOfDeposit) {
		this.currencyOfDeposit = currencyOfDeposit;
	}

	@JsonProperty("productCategory")
	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	@JsonProperty("sourceOfFund")
	public String getSourceOfFund() {
		return sourceOfFund;
	}

	public void setSourceOfFund(String sourceOfFund) {
		this.sourceOfFund = sourceOfFund;
	}

	@JsonProperty("initialDepositAmount")
	public String getInitialDepositAmount() {
		return initialDepositAmount;
	}

	public void setInitialDepositAmount(String initialDepositAmount) {
		this.initialDepositAmount = initialDepositAmount;
	}

	@JsonProperty("initialDepositMode")
	public String getInitialDepositMode() {
		return initialDepositMode;
	}

	public void setInitialDepositMode(String initialDepositMode) {
		this.initialDepositMode = initialDepositMode;
	}

	@JsonProperty("productCode")
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	@JsonProperty("modeOfOperation")
	public String getModeOfOperation() {
		return modeOfOperation;
	}

	public void setModeOfOperation(String modeOfOperation) {
		this.modeOfOperation = modeOfOperation;
	}

	@JsonProperty("isRestrictedAccount")
	public Boolean getIsRestrictedAccount() {
		return isRestrictedAccount;
	}

	public void setIsRestrictedAccount(Boolean isRestrictedAccount) {
		this.isRestrictedAccount = isRestrictedAccount;
	}

	@JsonProperty("sourceofLead")
	public String getSourceofLead() {
		return sourceofLead;
	}

	public void setSourceofLead(String sourceofLead) {
		this.sourceofLead = sourceofLead;
	}

	@JsonProperty("otherSourceOfLead")
	public String getOtherSourceOfLead() {
		return otherSourceOfLead;
	}

	public void setOtherSourceOfLead(String otherSourceOfLead) {
		this.otherSourceOfLead = otherSourceOfLead;
	}

	@JsonProperty("fatcaDeclaration")
	public Boolean getFatcaDeclaration() {
		return fatcaDeclaration;
	}

	public void setFatcaDeclaration(Boolean fatcaDeclaration) {
		this.fatcaDeclaration = fatcaDeclaration;
	}

	@JsonProperty("accountOpeningBranch")
	public String getAccountOpeningBranch() {
		return accountOpeningBranch;
	}

	public void setAccountOpeningBranch(String accountOpeningBranch) {
		this.accountOpeningBranch = accountOpeningBranch;
	}

	@JsonProperty("MICreditToESFBAccountNo")
	public String getMICreditToESFBAccountNo() {
		return MICreditToESFBAccountNo;
	}

	public void setMICreditToESFBAccountNo(String mICreditToESFBAccountNo) {
		MICreditToESFBAccountNo = mICreditToESFBAccountNo;
	}

	@JsonProperty("leadAssignedTo")
	public String getLeadAssignedTo() {
		return leadAssignedTo;
	}

	public void setLeadAssignedTo(String leadAssignedTo) {
		this.leadAssignedTo = leadAssignedTo;
	}

	@JsonProperty("cashTransactionRefNo")
	public String getCashTransactionRefNo() {
		return cashTransactionRefNo;
	}

	public void setCashTransactionRefNo(String cashTransactionRefNo) {
		this.cashTransactionRefNo = cashTransactionRefNo;
	}

	@JsonProperty("transactionNarration")
	public String getTransactionNarration() {
		return transactionNarration;
	}

	public void setTransactionNarration(String transactionNarration) {
		this.transactionNarration = transactionNarration;
	}

	@JsonProperty("accountOpeningValueDate")
	public String getAccountOpeningValueDate() {
		return accountOpeningValueDate;
	}

	public void setAccountOpeningValueDate(String accountOpeningValueDate) {
		this.accountOpeningValueDate = accountOpeningValueDate;
	}

	@JsonProperty("purposeOfOpeningAccount")
	public String getPurposeOfOpeningAccount() {
		return purposeOfOpeningAccount;
	}

	public void setPurposeOfOpeningAccount(String purposeOfOpeningAccount) {
		this.purposeOfOpeningAccount = purposeOfOpeningAccount;
	}

	@JsonProperty("interestPayout")
	public String getInterestPayout() {
		return interestPayout;
	}

	public void setInterestPayout(String interestPayout) {
		this.interestPayout = interestPayout;
	}

	@JsonProperty("interestCompoundFrequency")
	public String getInterestCompoundFrequency() {
		return interestCompoundFrequency;
	}

	public void setInterestCompoundFrequency(String interestCompoundFrequency) {
		this.interestCompoundFrequency = interestCompoundFrequency;
	}

	@JsonProperty("tenureDays")
	public String getTenureDays() {
		return tenureDays;
	}

	public void setTenureDays(String tenureDays) {
		this.tenureDays = tenureDays;
	}

	@JsonProperty("MICreditToOtherBankAccountNo")
	public String getMICreditToOtherBankAccountNo() {
		return MICreditToOtherBankAccountNo;
	}

	public void setMICreditToOtherBankAccountNo(String mICreditToOtherBankAccountNo) {
		MICreditToOtherBankAccountNo = mICreditToOtherBankAccountNo;
	}

	@JsonProperty("chequeIssuedBank")
	public String getChequeIssuedBank() {
		return chequeIssuedBank;
	}

	public void setChequeIssuedBank(String chequeIssuedBank) {
		this.chequeIssuedBank = chequeIssuedBank;
	}

	@JsonProperty("iPayToOtherBankName")
	public String getiPayToOtherBankName() {
		return iPayToOtherBankName;
	}

	public void setiPayToOtherBankName(String iPayToOtherBankName) {
		this.iPayToOtherBankName = iPayToOtherBankName;
	}

	@JsonProperty("depositAmount")
	public String getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}

	@JsonProperty("iPayToOtherBankIFSC")
	public String getiPayToOtherBankIFSC() {
		return iPayToOtherBankIFSC;
	}

	public void setiPayToOtherBankIFSC(String iPayToOtherBankIFSC) {
		this.iPayToOtherBankIFSC = iPayToOtherBankIFSC;
	}

	@JsonProperty("tenureMonths")
	public String getTenureMonths() {
		return tenureMonths;
	}

	public void setTenureMonths(String tenureMonths) {
		this.tenureMonths = tenureMonths;
	}

	@JsonProperty("chequeNo")
	public String getChequeNo() {
		return chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	@JsonProperty("maturityInstruction")
	public String getMaturityInstruction() {
		return maturityInstruction;
	}

	public void setMaturityInstruction(String maturityInstruction) {
		this.maturityInstruction = maturityInstruction;
	}

	@JsonProperty("MICreditToOtherBankBranch")
	public String getMICreditToOtherBankBranch() {
		return MICreditToOtherBankBranch;
	}

	public void setMICreditToOtherBankBranch(String mICreditToOtherBankBranch) {
		MICreditToOtherBankBranch = mICreditToOtherBankBranch;
	}

	@JsonProperty("iPayToOtherBankBranch")
	public String getiPayToOtherBankBranch() {
		return iPayToOtherBankBranch;
	}

	public void setiPayToOtherBankBranch(String iPayToOtherBankBranch) {
		this.iPayToOtherBankBranch = iPayToOtherBankBranch;
	}

	@JsonProperty("iPayToOtherBankBenificiaryName")
	public String getiPayToOtherBankBenificiaryName() {
		return iPayToOtherBankBenificiaryName;
	}

	public void setiPayToOtherBankBenificiaryName(String iPayToOtherBankBenificiaryName) {
		this.iPayToOtherBankBenificiaryName = iPayToOtherBankBenificiaryName;
	}

	@JsonProperty("MICreditToOtherBankIFSC")
	public String getMICreditToOtherBankIFSC() {
		return MICreditToOtherBankIFSC;
	}

	public void setMICreditToOtherBankIFSC(String mICreditToOtherBankIFSC) {
		MICreditToOtherBankIFSC = mICreditToOtherBankIFSC;
	}

	@JsonProperty("iPayToESFBAccountNo")
	public String getiPayToESFBAccountNo() {
		return iPayToESFBAccountNo;
	}

	public void setiPayToESFBAccountNo(String iPayToESFBAccountNo) {
		this.iPayToESFBAccountNo = iPayToESFBAccountNo;
	}

	@JsonProperty("productVariant")
	public String getProductVariant() {
		return productVariant;
	}

	public void setProductVariant(String productVariant) {
		this.productVariant = productVariant;
	}

	@JsonProperty("fromESFBAccountNo")
	public String getFromESFBAccountNo() {
		return fromESFBAccountNo;
	}

	public void setFromESFBAccountNo(String fromESFBAccountNo) {
		this.fromESFBAccountNo = fromESFBAccountNo;
	}

	@JsonProperty("iPayToOtherBankAccountNo")
	public String getiPayToOtherBankAccountNo() {
		return iPayToOtherBankAccountNo;
	}

	public void setiPayToOtherBankAccountNo(String iPayToOtherBankAccountNo) {
		this.iPayToOtherBankAccountNo = iPayToOtherBankAccountNo;
	}

	@JsonProperty("MICreditToOtherBankName")
	public String getMICreditToOtherBankName() {
		return MICreditToOtherBankName;
	}

	public void setMICreditToOtherBankName(String mICreditToOtherBankName) {
		MICreditToOtherBankName = mICreditToOtherBankName;
	}

	@JsonProperty("campaignCode")
	public String getCampaignCode() {
		return campaignCode;
	}

	public void setCampaignCode(String campaignCode) {
		this.campaignCode = campaignCode;
	}

	@JsonProperty("isNRIAccount")
	public String getIsNRIAccount() {
		return isNRIAccount;
	}

	public void setIsNRIAccount(String isNRIAccount) {
		this.isNRIAccount = isNRIAccount;
	}

	@JsonProperty("NREAccountType")
	public String getNREAccountType() {
		return NREAccountType;
	}

	public void setNREAccountType(String nREAccountType) {
		NREAccountType = nREAccountType;
	}

	@JsonProperty("NROAccountType")
	public String getNROAccountType() {
		return NROAccountType;
	}

	public void setNROAccountType(String nROAccountType) {
		NROAccountType = nROAccountType;
	}

	@JsonProperty("registeredForSweepIn")
	public String getRegisteredForSweepIn() {
		return registeredForSweepIn;
	}

	public void setRegisteredForSweepIn(String registeredForSweepIn) {
		this.registeredForSweepIn = registeredForSweepIn;
	}

	@JsonProperty("cashTransactionDate")
	public String getCashTransactionDate() {
		return cashTransactionDate;
	}

	public void setCashTransactionDate(String cashTransactionDate) {
		this.cashTransactionDate = cashTransactionDate;
	}

	@JsonProperty("chequeTransactionDate")
	public String getChequeTransactionDate() {
		return chequeTransactionDate;
	}

	public void setChequeTransactionDate(String chequeTransactionDate) {
		this.chequeTransactionDate = chequeTransactionDate;
	}

	@JsonProperty("fromESFBGLAccount")
	public String getFromESFBGLAccount() {
		return fromESFBGLAccount;
	}

	public void setFromESFBGLAccount(String fromESFBGLAccount) {
		this.fromESFBGLAccount = fromESFBGLAccount;
	}

	@JsonProperty("branchCodeGL")
	public String getBranchCodeGL() {
		return branchCodeGL;
	}

	public void setBranchCodeGL(String branchCodeGL) {
		this.branchCodeGL = branchCodeGL;
	}

	@JsonProperty("effectiveInterestRate")
	public String getEffectiveInterestRate() {
		return effectiveInterestRate;
	}

	public void setEffectiveInterestRate(String effectiveInterestRate) {
		this.effectiveInterestRate = effectiveInterestRate;
	}

	@JsonProperty("specialInterestRateRequired")
	public String getSpecialInterestRateRequired() {
		return specialInterestRateRequired;
	}

	public void setSpecialInterestRateRequired(String specialInterestRateRequired) {
		this.specialInterestRateRequired = specialInterestRateRequired;
	}

	@JsonProperty("specialInterestRate")
	public String getSpecialInterestRate() {
		return specialInterestRate;
	}

	public void setSpecialInterestRate(String specialInterestRate) {
		this.specialInterestRate = specialInterestRate;
	}

	@JsonProperty("specialInterestRequestID")
	public String getSpecialInterestRequestID() {
		return specialInterestRequestID;
	}

	public void setSpecialInterestRequestID(String specialInterestRequestID) {
		this.specialInterestRequestID = specialInterestRequestID;
	}

	@JsonProperty("depositVariance")
	public String getDepositVariance() {
		return depositVariance;
	}

	public void setDepositVariance(String depositVariance) {
		this.depositVariance = depositVariance;
	}

	@JsonProperty("iPayToOtherBankMICR")
	public String getiPayToOtherBankMICR() {
		return iPayToOtherBankMICR;
	}

	public void setiPayToOtherBankMICR(String iPayToOtherBankMICR) {
		this.iPayToOtherBankMICR = iPayToOtherBankMICR;
	}

	@JsonProperty("iPByDDPOIssuerCode")
	public String getiPByDDPOIssuerCode() {
		return iPByDDPOIssuerCode;
	}

	public void setiPByDDPOIssuerCode(String iPByDDPOIssuerCode) {
		this.iPByDDPOIssuerCode = iPByDDPOIssuerCode;
	}

	@JsonProperty("iPByDDPOPayeeName")
	public String getiPByDDPOPayeeName() {
		return iPByDDPOPayeeName;
	}

	public void setiPByDDPOPayeeName(String iPByDDPOPayeeName) {
		this.iPByDDPOPayeeName = iPByDDPOPayeeName;
	}

	@JsonProperty("MICreditToOtherBankAccountType")
	public String getMICreditToOtherBankAccountType() {
		return MICreditToOtherBankAccountType;
	}

	public void setMICreditToOtherBankAccountType(String mICreditToOtherBankAccountType) {
		MICreditToOtherBankAccountType = mICreditToOtherBankAccountType;
	}

	@JsonProperty("MICreditToOtherBankMICR")
	public String getMICreditToOtherBankMICR() {
		return MICreditToOtherBankMICR;
	}

	public void setMICreditToOtherBankMICR(String mICreditToOtherBankMICR) {
		MICreditToOtherBankMICR = mICreditToOtherBankMICR;
	}

	@JsonProperty("MIBByDDPOIssuerCode")
	public String getMIBByDDPOIssuerCode() {
		return MIBByDDPOIssuerCode;
	}

	public void setMIBByDDPOIssuerCode(String mIBByDDPOIssuerCode) {
		MIBByDDPOIssuerCode = mIBByDDPOIssuerCode;
	}

	@JsonProperty("MIByDDPOPayeeName")
	public String getMIByDDPOPayeeName() {
		return MIByDDPOPayeeName;
	}

	public void setMIByDDPOPayeeName(String mIByDDPOPayeeName) {
		MIByDDPOPayeeName = mIByDDPOPayeeName;
	}

	@JsonProperty("installmentAmount")
	public String getInstallmentAmount() {
		return installmentAmount;
	}

	public void setInstallmentAmount(String installmentAmount) {
		this.installmentAmount = installmentAmount;
	}

	@JsonProperty("periodicity")
	public String getPeriodicity() {
		return periodicity;
	}

	public void setPeriodicity(String periodicity) {
		this.periodicity = periodicity;
	}

	@JsonProperty("debitAccount")
	public String getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	@JsonProperty("SIFrequency")
	public String getSIFrequency() {
		return SIFrequency;
	}

	public void setSIFrequency(String sIFrequency) {
		SIFrequency = sIFrequency;
	}

	@JsonProperty("SIDebitAmount")
	public String getSIDebitAmount() {
		return SIDebitAmount;
	}

	public void setSIDebitAmount(String sIDebitAmount) {
		SIDebitAmount = sIDebitAmount;
	}

	@JsonProperty("FATCADeclaration")
	public String getFATCADeclaration() {
		return FATCADeclaration;
	}

	public void setFATCADeclaration(String fATCADeclaration) {
		FATCADeclaration = fATCADeclaration;
	}

	@JsonProperty("favouriteAccountNo")
	public String getFavouriteAccountNo() {
		return favouriteAccountNo;
	}

	public void setFavouriteAccountNo(String favouriteAccountNo) {
		this.favouriteAccountNo = favouriteAccountNo;
	}

	@JsonProperty("favouriteAccountCriteria")
	public String getFavouriteAccountCriteria() {
		return favouriteAccountCriteria;
	}

	public void setFavouriteAccountCriteria(String favouriteAccountCriteria) {
		this.favouriteAccountCriteria = favouriteAccountCriteria;
	}

	@JsonProperty("accountNumber")
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@JsonProperty("accountCreatedOn")
	public String getAccountCreatedOn() {
		return accountCreatedOn;
	}

	public void setAccountCreatedOn(String accountCreatedOn) {
		this.accountCreatedOn = accountCreatedOn;
	}

	@JsonProperty("partOfAccountGroup")
	public String getPartOfAccountGroup() {
		return partOfAccountGroup;
	}

	public void setPartOfAccountGroup(String partOfAccountGroup) {
		this.partOfAccountGroup = partOfAccountGroup;
	}

	@JsonProperty("purposeOfOpeningAccountOthers")
	public String getPurposeOfOpeningAccountOthers() {
		return purposeOfOpeningAccountOthers;
	}

	public void setPurposeOfOpeningAccountOthers(String purposeOfOpeningAccountOthers) {
		this.purposeOfOpeningAccountOthers = purposeOfOpeningAccountOthers;
	}

	@JsonProperty("expectedMonthlyRemittance")
	public String getExpectedMonthlyRemittance() {
		return expectedMonthlyRemittance;
	}

	public void setExpectedMonthlyRemittance(String expectedMonthlyRemittance) {
		this.expectedMonthlyRemittance = expectedMonthlyRemittance;
	}

	@JsonProperty("expectedMonthlyWithdrawal")
	public String getExpectedMonthlyWithdrawal() {
		return expectedMonthlyWithdrawal;
	}

	public void setExpectedMonthlyWithdrawal(String expectedMonthlyWithdrawal) {
		this.expectedMonthlyWithdrawal = expectedMonthlyWithdrawal;
	}

	@JsonProperty("subscripedForLPG")
	public String getSubscripedForLPG() {
		return subscripedForLPG;
	}

	public void setSubscripedForLPG(String subscripedForLPG) {
		this.subscripedForLPG = subscripedForLPG;
	}

	@JsonProperty("isTDSAvailable")
	public String getIsTDSAvailable() {
		return isTDSAvailable;
	}

	public void setIsTDSAvailable(String isTDSAvailable) {
		this.isTDSAvailable = isTDSAvailable;
	}

	@JsonProperty("waiveOffTDS")
	public String getWaiveOffTDS() {
		return waiveOffTDS;
	}

	public void setWaiveOffTDS(String waiveOffTDS) {
		this.waiveOffTDS = waiveOffTDS;
	}

	@JsonProperty("leadCreatedBy")
	public String getLeadCreatedBy() {
		return leadCreatedBy;
	}

	public void setLeadCreatedBy(String leadCreatedBy) {
		this.leadCreatedBy = leadCreatedBy;
	}

	@JsonProperty("leadCreatedOn")
	public String getLeadCreatedOn() {
		return leadCreatedOn;
	}

	public void setLeadCreatedOn(String leadCreatedOn) {
		this.leadCreatedOn = leadCreatedOn;
	}

	@JsonProperty("leadModifiedBy")
	public String getLeadModifiedBy() {
		return leadModifiedBy;
	}

	public void setLeadModifiedBy(String leadModifiedBy) {
		this.leadModifiedBy = leadModifiedBy;
	}

	@JsonProperty("leadModifiedOn")
	public String getLeadModifiedOn() {
		return leadModifiedOn;
	}

	public void setLeadModifiedOn(String leadModifiedOn) {
		this.leadModifiedOn = leadModifiedOn;
	}

	@JsonProperty("otherSourceOfFund")
	public String getOtherSourceOfFund() {
		return otherSourceOfFund;
	}

	public void setOtherSourceOfFund(String otherSourceOfFund) {
		this.otherSourceOfFund = otherSourceOfFund;
	}

}
