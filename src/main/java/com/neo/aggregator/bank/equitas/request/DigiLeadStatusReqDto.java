package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.DigiLeadStatusMessageBody;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.dto.DigiAccountRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("getDigiLeadStatusReq")
public class DigiLeadStatusReqDto {


	@Autowired
	private EquitasCommonService equitasCommonService;
	
	protected LeadMessageHeader leadMessageHeader;
	protected DigiLeadStatusMessageBody digiLeadStatusMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public DigiLeadStatusMessageBody getDigiLeadStatusMessageBody() {
		return digiLeadStatusMessageBody;
	}

	public void setDigiLeadStatusMessageBody(DigiLeadStatusMessageBody digiLeadStatusMessageBody) {
		this.digiLeadStatusMessageBody = digiLeadStatusMessageBody;
	}
	
	public DigiLeadStatusReqDto build(DigiAccountRequestDto request) throws NeoException {

		DigiLeadStatusReqDto requestDto = null;

		try {
			
			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);
			
			
			DigiLeadStatusMessageBody digiLeadStatusMessage = equitasCommonService.buildDigiLeadStatusMessageBody(request);
			
			requestDto = DigiLeadStatusReqDto.builder().digiLeadStatusMessageBody(digiLeadStatusMessage).leadMessageHeader(leadMessage)
					.build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}


}
