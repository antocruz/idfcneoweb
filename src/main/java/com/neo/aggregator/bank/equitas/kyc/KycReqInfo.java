package com.neo.aggregator.bank.equitas.kyc;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KycReqInfo {

	private String de;
	private String lr;
	private String pfr;
	private String ra;
	private String rc;
	private String ver;
	private String ts;

	private Auth auth;

	
	@XmlAttribute(name = "de")
	public String getDe() {
		return de;
	}

	public void setDe(String de) {
		this.de = de;
	}

	@XmlAttribute(name = "lr")
	public String getLr() {
		return lr;
	}

	public void setLr(String lr) {
		this.lr = lr;
	}

	@XmlAttribute(name = "pfr")
	public String getPfr() {
		return pfr;
	}

	public void setPfr(String pfr) {
		this.pfr = pfr;
	}

	@XmlAttribute(name = "ra")
	public String getRa() {
		return ra;
	}

	public void setRa(String ra) {
		this.ra = ra;
	}

	@XmlAttribute(name = "rc")
	public String getRc() {
		return rc;
	}

	public void setRc(String rc) {
		this.rc = rc;
	}

	@XmlAttribute(name = "ver")
	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	@XmlElement(name = "Auth")
	public Auth getAuth() {
		return auth;
	}

	public void setAuth(Auth auth) {
		this.auth = auth;
	}

	@XmlAttribute(name = "ts")
	public String getTs() {
		return ts;
	}

	public void setTs(String ts) {
		this.ts = ts;
	}

}
