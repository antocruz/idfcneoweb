package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.LeadMessageBody;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.dto.KycRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("createDigiCustLeadReq")
public class CreateLeadReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;
	
	protected LeadMessageHeader leadMessageHeader;
	protected LeadMessageBody leadMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public LeadMessageBody getLeadMessageBody() {
		return leadMessageBody;
	}

	public void setLeadMessageBody(LeadMessageBody leadMessageBody) {
		this.leadMessageBody = leadMessageBody;
	}
	
	public CreateLeadReqDto build(KycRequestDto request) throws NeoException {

		CreateLeadReqDto requestDto = null;

		try {
			
			LeadMessageHeader leadMessageHeaderBuild = equitasCommonService.buildLeadCreationMessageHeader(request);
			
			LeadMessageBody leadMessageBodyBuild = equitasCommonService.buildLeadCreationMessageBody(request);
			
			requestDto = CreateLeadReqDto.builder().leadMessageBody(leadMessageBodyBuild).leadMessageHeader(leadMessageHeaderBuild)
					.build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}

	


}
