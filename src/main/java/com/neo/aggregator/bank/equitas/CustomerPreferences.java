package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerPreferences {

	private Boolean isStaff;
	private Boolean allSMSAlerts;
	private Boolean onlyTransactionAlerts;
	private Boolean netBanking;
	private Boolean mobileBankingNumber;
	private Boolean sms;
	private Boolean passbook;
	private Boolean physicalStatement;
	private Boolean emailStatement;
	private String netBankingRights;
	private String mappedAccountLead;
	private String preferenceID;
	private String mappedCustomer;
	private String mappedCustomerLead;
	private String mappedAccount;
	private String bankGuarantee;
	private String letterofCredit;
	private String businessLoan;
	private String doorStepBanking;
	private String doorStepBankingOnCall;
	private String doorStepBankingBeat;
	private String tradeForex;
	private String loanAgainstProperty;
	private String overdraftsagainstFD;
	private String isDelete;
	private String entityType;
	private String entityFlag;
	private String brnchId;
	private String alertEmailID;
	
	@JsonProperty("isStaff")
	public Boolean getIsStaff() {
		return isStaff;
	}

	public void setIsStaff(Boolean isStaff) {
		this.isStaff = isStaff;
	}

	@JsonProperty("allSMSAlerts")
	public Boolean getAllSMSAlerts() {
		return allSMSAlerts;
	}

	public void setAllSMSAlerts(Boolean allSMSAlerts) {
		this.allSMSAlerts = allSMSAlerts;
	}

	@JsonProperty("onlyTransactionAlerts")
	public Boolean getOnlyTransactionAlerts() {
		return onlyTransactionAlerts;
	}

	public void setOnlyTransactionAlerts(Boolean onlyTransactionAlerts) {
		this.onlyTransactionAlerts = onlyTransactionAlerts;
	}

	@JsonProperty("netBanking")
	public Boolean getNetBanking() {
		return netBanking;
	}

	public void setNetBanking(Boolean netBanking) {
		this.netBanking = netBanking;
	}

	@JsonProperty("mobileBankingNumber")
	public Boolean getMobileBankingNumber() {
		return mobileBankingNumber;
	}

	public void setMobileBankingNumber(Boolean mobileBankingNumber) {
		this.mobileBankingNumber = mobileBankingNumber;
	}

	@JsonProperty("sms")
	public Boolean getSms() {
		return sms;
	}

	public void setSms(Boolean sms) {
		this.sms = sms;
	}

	@JsonProperty("passbook")
	public Boolean getPassbook() {
		return passbook;
	}

	public void setPassbook(Boolean passbook) {
		this.passbook = passbook;
	}

	@JsonProperty("physicalStatement")
	public Boolean getPhysicalStatement() {
		return physicalStatement;
	}

	public void setPhysicalStatement(Boolean physicalStatement) {
		this.physicalStatement = physicalStatement;
	}

	@JsonProperty("emailStatement")
	public Boolean getEmailStatement() {
		return emailStatement;
	}

	public void setEmailStatement(Boolean emailStatement) {
		this.emailStatement = emailStatement;
	}

	@JsonProperty("netBankingRights")
	public String getNetBankingRights() {
		return netBankingRights;
	}

	public void setNetBankingRights(String netBankingRights) {
		this.netBankingRights = netBankingRights;
	}

	@JsonProperty("mappedAccountLead")
	public String getMappedAccountLead() {
		return mappedAccountLead;
	}

	public void setMappedAccountLead(String mappedAccountLead) {
		this.mappedAccountLead = mappedAccountLead;
	}

	@JsonProperty("preferenceID")
	public String getPreferenceID() {
		return preferenceID;
	}

	public void setPreferenceID(String preferenceID) {
		this.preferenceID = preferenceID;
	}

	@JsonProperty("mappedCustomer")
	public String getMappedCustomer() {
		return mappedCustomer;
	}

	public void setMappedCustomer(String mappedCustomer) {
		this.mappedCustomer = mappedCustomer;
	}

	@JsonProperty("mappedCustomerLead")
	public String getMappedCustomerLead() {
		return mappedCustomerLead;
	}

	public void setMappedCustomerLead(String mappedCustomerLead) {
		this.mappedCustomerLead = mappedCustomerLead;
	}

	@JsonProperty("mappedAccount")
	public String getMappedAccount() {
		return mappedAccount;
	}

	public void setMappedAccount(String mappedAccount) {
		this.mappedAccount = mappedAccount;
	}

	@JsonProperty("bankGuarantee")
	public String getBankGuarantee() {
		return bankGuarantee;
	}

	public void setBankGuarantee(String bankGuarantee) {
		this.bankGuarantee = bankGuarantee;
	}

	@JsonProperty("letterofCredit")
	public String getLetterofCredit() {
		return letterofCredit;
	}

	public void setLetterofCredit(String letterofCredit) {
		this.letterofCredit = letterofCredit;
	}

	@JsonProperty("businessLoan")
	public String getBusinessLoan() {
		return businessLoan;
	}

	public void setBusinessLoan(String businessLoan) {
		this.businessLoan = businessLoan;
	}

	@JsonProperty("doorStepBanking")
	public String getDoorStepBanking() {
		return doorStepBanking;
	}

	public void setDoorStepBanking(String doorStepBanking) {
		this.doorStepBanking = doorStepBanking;
	}

	@JsonProperty("doorStepBankingOnCall")
	public String getDoorStepBankingOnCall() {
		return doorStepBankingOnCall;
	}

	public void setDoorStepBankingOnCall(String doorStepBankingOnCall) {
		this.doorStepBankingOnCall = doorStepBankingOnCall;
	}

	@JsonProperty("doorStepBankingBeat")
	public String getDoorStepBankingBeat() {
		return doorStepBankingBeat;
	}

	public void setDoorStepBankingBeat(String doorStepBankingBeat) {
		this.doorStepBankingBeat = doorStepBankingBeat;
	}

	@JsonProperty("tradeForex")
	public String getTradeForex() {
		return tradeForex;
	}

	public void setTradeForex(String tradeForex) {
		this.tradeForex = tradeForex;
	}

	@JsonProperty("loanAgainstProperty")
	public String getLoanAgainstProperty() {
		return loanAgainstProperty;
	}

	public void setLoanAgainstProperty(String loanAgainstProperty) {
		this.loanAgainstProperty = loanAgainstProperty;
	}

	@JsonProperty("overdraftsagainstFD")
	public String getOverdraftsagainstFD() {
		return overdraftsagainstFD;
	}

	public void setOverdraftsagainstFD(String overdraftsagainstFD) {
		this.overdraftsagainstFD = overdraftsagainstFD;
	}

	@JsonProperty("isDelete")
	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	@JsonProperty("entityType")
	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	@JsonProperty("entityFlag")
	public String getEntityFlag() {
		return entityFlag;
	}

	public void setEntityFlag(String entityFlag) {
		this.entityFlag = entityFlag;
	}

	@JsonProperty("brnchId")
	public String getBrnchId() {
		return brnchId;
	}

	public void setBrnchId(String brnchId) {
		this.brnchId = brnchId;
	}

	@JsonProperty("alertEmailID")
	public String getAlertEmailID() {
		return alertEmailID;
	}

	public void setAlertEmailID(String alertEmailID) {
		this.alertEmailID = alertEmailID;
	}

}
