package com.neo.aggregator.bank.equitas.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("getDigiAccountStatementResponse")
public class DigiAccountStatementResDto {

	@JsonProperty("msgHdr")
	protected DigiAccountStatementResDto.Header msgHdr;
	@JsonProperty("msgBdy")
	protected DigiAccountStatementResDto.Body msgBdy;

	public DigiAccountStatementResDto.Header getMsgHdr() {
		return msgHdr;
	}

	public void setMsgHdr(DigiAccountStatementResDto.Header msgHdr) {
		this.msgHdr = msgHdr;
	}

	public DigiAccountStatementResDto.Body getMsgBdy() {
		return msgBdy;
	}

	public void setMsgBdy(DigiAccountStatementResDto.Body msgBdy) {
		this.msgBdy = msgBdy;
	}

	public static class Header {
		@JsonProperty(value = "rslt")
		protected String rslt;

		@JsonProperty(value = "error")
		protected List<Error> error;

		public List<Error> getError() {
			return error;
		}

		public void setError(List<Error> error) {
			this.error = error;
		}

		public String getRslt() {
			return rslt;
		}

		public void setRslt(String rslt) {
			this.rslt = rslt;
		}

		public static class Error {

			@JsonProperty(value = "cd")
			protected String cd;
			@JsonProperty(value = "rsn")
			protected String rsn;

			public String getCd() {
				return cd;
			}

			public void setCd(String cd) {
				this.cd = cd;
			}

			public String getRsn() {
				return rsn;
			}

			public void setRsn(String rsn) {
				this.rsn = rsn;
			}
		}
	}

	public static class Body {

		@JsonProperty(value = "account")
		protected List<Account> account;

		public List<Account> getAccount() {
			return account;
		}

		public void setAccount(List<Account> account) {
			this.account = account;
		}

		public static class Account {

			@JsonProperty(value = "id")
			private String id;
			@JsonProperty(value = "accountId")
			private String accountId;
			@JsonProperty(value = "bookingDateTime")
			private String bookingDateTime;
			@JsonProperty(value = "transactionAmount")
			private String transactionAmount;
			@JsonProperty(value = "transactionCurrency")
			private String transactionCurrency;
			@JsonProperty(value = "openingBalance")
			private String openingBalance;
			@JsonProperty(value = "balanceAmount")
			private String balanceAmount;
			@JsonProperty(value = "creditDebitIndicator")
			private String creditDebitIndicator;
			@JsonProperty(value = "description")
			private String description;
			@JsonProperty(value = "instrumentNumber")
			private String instrumentNumber;
			@JsonProperty(value = "depositNb")
			private String depositNb;
			public String getId() {
				return id;
			}
			public void setId(String id) {
				this.id = id;
			}
			public String getAccountId() {
				return accountId;
			}
			public void setAccountId(String accountId) {
				this.accountId = accountId;
			}
			public String getBookingDateTime() {
				return bookingDateTime;
			}
			public void setBookingDateTime(String bookingDateTime) {
				this.bookingDateTime = bookingDateTime;
			}
			public String getTransactionAmount() {
				return transactionAmount;
			}
			public void setTransactionAmount(String transactionAmount) {
				this.transactionAmount = transactionAmount;
			}
			public String getTransactionCurrency() {
				return transactionCurrency;
			}
			public void setTransactionCurrency(String transactionCurrency) {
				this.transactionCurrency = transactionCurrency;
			}
			public String getOpeningBalance() {
				return openingBalance;
			}
			public void setOpeningBalance(String openingBalance) {
				this.openingBalance = openingBalance;
			}
			public String getBalanceAmount() {
				return balanceAmount;
			}
			public void setBalanceAmount(String balanceAmount) {
				this.balanceAmount = balanceAmount;
			}
			public String getCreditDebitIndicator() {
				return creditDebitIndicator;
			}
			public void setCreditDebitIndicator(String creditDebitIndicator) {
				this.creditDebitIndicator = creditDebitIndicator;
			}
			public String getDescription() {
				return description;
			}
			public void setDescription(String description) {
				this.description = description;
			}
			public String getInstrumentNumber() {
				return instrumentNumber;
			}
			public void setInstrumentNumber(String instrumentNumber) {
				this.instrumentNumber = instrumentNumber;
			}
			public String getDepositNb() {
				return depositNb;
			}
			public void setDepositNb(String depositNb) {
				this.depositNb = depositNb;
			}
			
		}

	}



}
