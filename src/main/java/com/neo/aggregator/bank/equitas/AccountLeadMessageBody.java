package com.neo.aggregator.bank.equitas;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountLeadMessageBody {

	private String usrId;
	private String accountLeadId;
	private String authenticationToken;
	private Boolean fromMADP;
	
	private List<CustomerAccountLeadRelation> customerAccountLeadRelation;
	private AccountLead accountLead;
	
	@JsonProperty("usrId")
	public String getUsrId() {
		return usrId;
	}
	
	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}
	
	@JsonProperty("accountLeadId")
	public String getAccountLeadId() {
		return accountLeadId;
	}
	
	public void setAccountLeadId(String accountLeadId) {
		this.accountLeadId = accountLeadId;
	}
	
	@JsonProperty("authenticationToken")
	public String getAuthenticationToken() {
		return authenticationToken;
	}
	
	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}
	
	@JsonProperty("fromMADP")
	public Boolean getFromMADP() {
		return fromMADP;
	}
	
	public void setFromMADP(Boolean fromMADP) {
		this.fromMADP = fromMADP;
	}

	@JsonProperty("customerAccountLeadRelation")
	public List<CustomerAccountLeadRelation> getCustomerAccountLeadRelation() {
		return customerAccountLeadRelation;
	}

	public void setCustomerAccountLeadRelation(List<CustomerAccountLeadRelation> customerAccountLeadRelation) {
		this.customerAccountLeadRelation = customerAccountLeadRelation;
	}

	@JsonProperty("accountLead")
	public AccountLead getAccountLead() {
		return accountLead;
	}

	public void setAccountLead(AccountLead accountLead) {
		this.accountLead = accountLead;
	}
	
	
	
}
