package com.neo.aggregator.bank.equitas.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("createDigiCustLeadRep")
public class CreateLeadResDto {

	@JsonProperty("msgHdr")
	protected CreateLeadResDto.Header msgHdr;
	@JsonProperty("msgBdy")
	protected CreateLeadResDto.Body msgBdy;
	
	public CreateLeadResDto.Header getMsgHdr() {
		return msgHdr;
	}

	public void setMsgHdr(CreateLeadResDto.Header msgHdr) {
		this.msgHdr = msgHdr;
	}

	public CreateLeadResDto.Body getMsgBdy() {
		return msgBdy;
	}

	public void setMsgBdy(CreateLeadResDto.Body msgBdy) {
		this.msgBdy = msgBdy;
	}
	
	public static class Header {
		@JsonProperty(value = "rslt")
		protected String rslt;
		
		@JsonProperty(value = "error")
		protected List<Error> error;
		
		public List<Error> getError() {
			return error;
		}

		public void setError(List<Error> error) {
			this.error = error;
		}

		public String getRslt() {
			return rslt;
		}

		public void setRslt(String rslt) {
			this.rslt = rslt;
		}
		
		public static class Error {
			
			@JsonProperty(value = "cd")
			protected String cd;
			@JsonProperty(value = "rsn")
			protected String rsn;
			
			public String getCd() {
				return cd;
			}
			public void setCd(String cd) {
				this.cd = cd;
			}
			public String getRsn() {
				return rsn;
			}
			public void setRsn(String rsn) {
				this.rsn = rsn;
			}
		}
	}
	
	public static class Body {
		
		@JsonProperty(value = "customerLeadID")
		protected String customerLeadID;
		
		public String getCustomerLeadID() {
			return customerLeadID;
		}

		public void setCustomerLeadID(String customerLeadID) {
			this.customerLeadID = customerLeadID;
		}

	}

}
