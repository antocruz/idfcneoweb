package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.DigiAccountStatementMessageBody;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.dto.DigiAccountRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("getDigiAccountStatementRequest")
public class DigiAccountStatementReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;
	
	protected LeadMessageHeader leadMessageHeader;
	protected DigiAccountStatementMessageBody digiAccountStatementMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public DigiAccountStatementMessageBody getDigiAccountStatementMessageBody() {
		return digiAccountStatementMessageBody;
	}

	public void setDigiAccountStatementMessageBody(DigiAccountStatementMessageBody digiAccountStatementMessageBody) {
		this.digiAccountStatementMessageBody = digiAccountStatementMessageBody;
	}
	
	public DigiAccountStatementReqDto build(DigiAccountRequestDto request) throws NeoException {

		DigiAccountStatementReqDto requestDto = null;

		try {
			
			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);
			
			
			DigiAccountStatementMessageBody digiAccountStatementMessage = equitasCommonService.buildDigiAccountStatementMessageBody(request);
			
			requestDto = DigiAccountStatementReqDto.builder().digiAccountStatementMessageBody(digiAccountStatementMessage).leadMessageHeader(leadMessage)
					.build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}

}
