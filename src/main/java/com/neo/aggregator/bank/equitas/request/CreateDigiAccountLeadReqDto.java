package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.AccountLeadMessageBody;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.dto.DigiAccountRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("createDigiAccountLeadReq")
public class CreateDigiAccountLeadReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;
	
	protected LeadMessageHeader leadMessageHeader;
	protected AccountLeadMessageBody accountLeadMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public AccountLeadMessageBody getAccountLeadMessageBody() {
		return accountLeadMessageBody;
	}

	public void setAccountLeadMessageBody(AccountLeadMessageBody accountLeadMessageBody) {
		this.accountLeadMessageBody = accountLeadMessageBody;
	}
	
	public CreateDigiAccountLeadReqDto build(DigiAccountRequestDto request) throws NeoException {

		CreateDigiAccountLeadReqDto requestDto = null;

		try {
			
			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);
			
			
			AccountLeadMessageBody accountLeadMessage = equitasCommonService.buildAccountLeadMessageBody(request);
			
			requestDto = CreateDigiAccountLeadReqDto.builder().accountLeadMessageBody(accountLeadMessage).leadMessageHeader(leadMessage)
					.build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}


}
