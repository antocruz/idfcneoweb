package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LeadResponseBody {

	private String customerLeadID;
	private String error;

	@JsonProperty("customerLeadID")
	public String getCustomerLeadID() {
		return customerLeadID;
	}

	public void setCustomerLeadID(String customerLeadID) {
		this.customerLeadID = customerLeadID;
	}

	@JsonProperty("error")
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
}
