package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FatcaTaxDetailsIndividual {

	private String cityOfBirth;
	private String countryOfBirth;
	private Boolean taxResident;

	@JsonProperty("cityOfBirth")
	public String getCityOfBirth() {
		return cityOfBirth;
	}

	public void setCityOfBirth(String cityOfBirth) {
		this.cityOfBirth = cityOfBirth;
	}

	@JsonProperty("countryOfBirth")
	public String getCountryOfBirth() {
		return countryOfBirth;
	}

	public void setCountryOfBirth(String countryOfBirth) {
		this.countryOfBirth = countryOfBirth;
	}

	public Boolean getTaxResident() {
		return taxResident;
	}

	public void setTaxResident(Boolean taxResident) {
		this.taxResident = taxResident;
	}

	

}
