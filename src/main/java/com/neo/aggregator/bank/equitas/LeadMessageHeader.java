package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LeadMessageHeader {
    	  
	private String cnvId;
	private String bizObjId;
	private String extRefId;
	private String appId;
	private String msgId;
	private String timestamp;
	
	AuthInfo authInfo;

	@JsonProperty("cnvId")
	public String getCnvId() {
		return cnvId;
	}

	public void setCnvId(String cnvId) {
		this.cnvId = cnvId;
	}

	@JsonProperty("bizObjId")
	public String getBizObjId() {
		return bizObjId;
	}

	public void setBizObjId(String bizObjId) {
		this.bizObjId = bizObjId;
	}
	
	@JsonProperty("extRefId")
	public String getExtRefId() {
		return extRefId;
	}

	public void setExtRefId(String extRefId) {
		this.extRefId = extRefId;
	}

	@JsonProperty("appId")
	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	@JsonProperty("msgId")
	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	@JsonProperty("timestamp")
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@JsonProperty("authInfo")
	public AuthInfo getAuthInfo() {
		return authInfo;
	}

	public void setAuthInfo(AuthInfo authInfo) {
		this.authInfo = authInfo;
	}

}
