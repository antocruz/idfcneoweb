package com.neo.aggregator.bank.equitas.kyc;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "KycRequest")
@XmlType(propOrder = {"transactionInfo", "kycReqInfo"})
public class KycRequestData {

	private TransactionInfo transactionInfo;
	private KycReqInfo kycReqInfo;

	@XmlElement(name = "TransactionInfo")
	public TransactionInfo getTransactionInfo() {
		return transactionInfo;
	}

	
	public void setTransactionInfo(TransactionInfo transactionInfo) {
		this.transactionInfo = transactionInfo;
	}

	@XmlElement(name = "KycReqInfo")
	public KycReqInfo getKycReqInfo() {
		return kycReqInfo;
	}

	
	public void setKycReqInfo(KycReqInfo kycReqInfo) {
		this.kycReqInfo = kycReqInfo;
	}

}
