package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonRootName("ManageDigiCustLeadRep")
public class LeadResponse {

	protected LeadResponseHeader leadResponseHeader;
	protected LeadResponseBody leadResponseBody;
	
	public LeadResponseHeader getLeadResponseHeader() {
		return leadResponseHeader;
	}
	public void setLeadResponseHeader(LeadResponseHeader leadResponseHeader) {
		this.leadResponseHeader = leadResponseHeader;
	}
	public LeadResponseBody getLeadResponseBody() {
		return leadResponseBody;
	}
	public void setLeadResponseBody(LeadResponseBody leadResponseBody) {
		this.leadResponseBody = leadResponseBody;
	}
	
	
}
