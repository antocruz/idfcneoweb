package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.UpdateAccountLeadMessageBody;
import com.neo.aggregator.bank.equitas.dto.DigiAccountRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("updateDigiAccountLeadReq")
public class UpdateDigiAccountLeadReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;

	protected LeadMessageHeader leadMessageHeader;
	protected UpdateAccountLeadMessageBody updateAccountLeadMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public UpdateAccountLeadMessageBody getUpdateAccountLeadMessageBody() {
		return updateAccountLeadMessageBody;
	}

	public void setUpdateAccountLeadMessageBody(UpdateAccountLeadMessageBody updateAccountLeadMessageBody) {
		this.updateAccountLeadMessageBody = updateAccountLeadMessageBody;
	}

	public UpdateDigiAccountLeadReqDto build(DigiAccountRequestDto request) throws NeoException {

		UpdateDigiAccountLeadReqDto requestDto = null;

		try {

			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);

			UpdateAccountLeadMessageBody updateAccountLeadMessage = equitasCommonService
					.buildUpdateAccountLeadMessageBody(request);

			requestDto = UpdateDigiAccountLeadReqDto.builder().updateAccountLeadMessageBody(updateAccountLeadMessage)
					.leadMessageHeader(leadMessage).build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}

}
