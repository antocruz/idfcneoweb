package com.neo.aggregator.bank.equitas;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateDigiCaseMessageBody {

	private List<CreateCaseReq> createcaseReq;

	@JsonProperty("createcaseReq")
	public List<CreateCaseReq> getCreatecaseReq() {
		return createcaseReq;
	}

	public void setCreatecaseReq(List<CreateCaseReq> createcaseReq) {
		this.createcaseReq = createcaseReq;
	}

}
