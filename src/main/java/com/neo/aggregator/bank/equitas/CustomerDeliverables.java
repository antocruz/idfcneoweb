package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDeliverables {

	private Boolean welcomeKit;
	private Boolean debitCard;
	private Boolean iKITChequeBook;
	private Boolean iKIT;
	private Boolean chequeBook;
	private String mappedCustomer;
	private String deliverableID;

	@JsonProperty("welcomeKit")
	public Boolean getWelcomeKit() {
		return welcomeKit;
	}

	public void setWelcomeKit(Boolean welcomeKit) {
		this.welcomeKit = welcomeKit;
	}

	@JsonProperty("debitCard")
	public Boolean getDebitCard() {
		return debitCard;
	}

	public void setDebitCard(Boolean debitCard) {
		this.debitCard = debitCard;
	}

	@JsonProperty("iKITChequeBook")
	public Boolean getiKITChequeBook() {
		return iKITChequeBook;
	}

	public void setiKITChequeBook(Boolean iKITChequeBook) {
		this.iKITChequeBook = iKITChequeBook;
	}

	@JsonProperty("iKIT")
	public Boolean getiKIT() {
		return iKIT;
	}

	public void setiKIT(Boolean iKIT) {
		this.iKIT = iKIT;
	}

	@JsonProperty("chequeBook")
	public Boolean getChequeBook() {
		return chequeBook;
	}

	public void setChequeBook(Boolean chequeBook) {
		this.chequeBook = chequeBook;
	}

	@JsonProperty("mappedCustomer")
	public String getMappedCustomer() {
		return mappedCustomer;
	}

	public void setMappedCustomer(String mappedCustomer) {
		this.mappedCustomer = mappedCustomer;
	}

	@JsonProperty("deliverableID")
	public String getDeliverableID() {
		return deliverableID;
	}

	public void setDeliverableID(String deliverableID) {
		this.deliverableID = deliverableID;
	}

}
