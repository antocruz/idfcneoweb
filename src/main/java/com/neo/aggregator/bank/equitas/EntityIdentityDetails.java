package com.neo.aggregator.bank.equitas;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EntityIdentityDetails {
	private String identityDetails;

}
