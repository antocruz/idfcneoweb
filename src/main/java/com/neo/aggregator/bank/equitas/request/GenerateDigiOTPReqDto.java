package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.GenerateDigiOTPMessageBody;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.dto.KycRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("generateDigiOTPRequest")
public class GenerateDigiOTPReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;
	
	protected LeadMessageHeader leadMessageHeader;
	protected GenerateDigiOTPMessageBody generateDigiOTPMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public GenerateDigiOTPMessageBody getGenerateDigiOTPMessageBody() {
		return generateDigiOTPMessageBody;
	}

	public void setGenerateDigiOTPMessageBody(GenerateDigiOTPMessageBody generateDigiOTPMessageBody) {
		this.generateDigiOTPMessageBody = generateDigiOTPMessageBody;
	}

	public GenerateDigiOTPReqDto build(KycRequestDto request) throws NeoException {

		GenerateDigiOTPReqDto requestDto = null;

		try {
			
			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);
			
			GenerateDigiOTPMessageBody generateDigiOTPMessage = equitasCommonService.buildGenerateDigiOTPMessageBody(request);
			
			requestDto = GenerateDigiOTPReqDto.builder().generateDigiOTPMessageBody(generateDigiOTPMessage).leadMessageHeader(leadMessage)
					.build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}
}
