package com.neo.aggregator.bank.equitas.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("getDigiLeadStatusRep")
public class DigiLeadStatusResDto {

	@JsonProperty("msgHdr")
	protected DigiLeadStatusResDto.Header msgHdr;
	@JsonProperty("msgBdy")
	protected DigiLeadStatusResDto.Body msgBdy;

	public DigiLeadStatusResDto.Header getMsgHdr() {
		return msgHdr;
	}

	public void setMsgHdr(DigiLeadStatusResDto.Header msgHdr) {
		this.msgHdr = msgHdr;
	}

	public DigiLeadStatusResDto.Body getMsgBdy() {
		return msgBdy;
	}

	public void setMsgBdy(DigiLeadStatusResDto.Body msgBdy) {
		this.msgBdy = msgBdy;
	}

	public static class Header {
		@JsonProperty(value = "rslt")
		protected String rslt;

		@JsonProperty(value = "error")
		protected List<Error> error;

		public List<Error> getError() {
			return error;
		}

		public void setError(List<Error> error) {
			this.error = error;
		}

		public String getRslt() {
			return rslt;
		}

		public void setRslt(String rslt) {
			this.rslt = rslt;
		}

		public static class Error {

			@JsonProperty(value = "cd")
			protected String cd;
			@JsonProperty(value = "rsn")
			protected String rsn;

			public String getCd() {
				return cd;
			}

			public void setCd(String cd) {
				this.cd = cd;
			}

			public String getRsn() {
				return rsn;
			}

			public void setRsn(String rsn) {
				this.rsn = rsn;
			}
		}
	}

	public static class Body {

		@JsonProperty(value = "id")
		private String id;
		@JsonProperty(value = "associatedWith")
		private String associatedWith;
		@JsonProperty(value = "purposeOfCreation")
		private String purposeOfCreation;
		@JsonProperty(value = "leadDetails")
		protected List<LeadDetails> leadDetails;

		public String getId() {
			return id;
		}


		public void setId(String id) {
			this.id = id;
		}


		public String getAssociatedWith() {
			return associatedWith;
		}


		public void setAssociatedWith(String associatedWith) {
			this.associatedWith = associatedWith;
		}


		public String getPurposeOfCreation() {
			return purposeOfCreation;
		}


		public void setPurposeOfCreation(String purposeOfCreation) {
			this.purposeOfCreation = purposeOfCreation;
		}


		public List<LeadDetails> getLeadDetails() {
			return leadDetails;
		}


		public void setLeadDetails(List<LeadDetails> leadDetails) {
			this.leadDetails = leadDetails;
		}


		public static class LeadDetails {

			@JsonProperty(value = "processName")
			protected String processName;

			@JsonProperty(value = "description")
			protected String description;

			@JsonProperty(value = "userId")
			protected String userId;

			@JsonProperty(value = "completedBy")
			protected String completedBy;

			@JsonProperty(value = "completedOn")
			protected String completedOn;

			@JsonProperty(value = "remarks")
			protected String remarks;

			@JsonProperty(value = "subId")
			protected String subId;

			@JsonProperty(value = "exitState")
			protected String exitState;

			@JsonProperty(value = "exitType")
			protected String exitType;

			@JsonProperty(value = "userGroup")
			protected String userGroup;

			public String getProcessName() {
				return processName;
			}

			public void setProcessName(String processName) {
				this.processName = processName;
			}

			public String getDescription() {
				return description;
			}

			public void setDescription(String description) {
				this.description = description;
			}

			public String getUserId() {
				return userId;
			}

			public void setUserId(String userId) {
				this.userId = userId;
			}

			public String getCompletedBy() {
				return completedBy;
			}

			public void setCompletedBy(String completedBy) {
				this.completedBy = completedBy;
			}

			public String getCompletedOn() {
				return completedOn;
			}

			public void setCompletedOn(String completedOn) {
				this.completedOn = completedOn;
			}

			public String getRemarks() {
				return remarks;
			}

			public void setRemarks(String remarks) {
				this.remarks = remarks;
			}

			public String getSubId() {
				return subId;
			}

			public void setSubId(String subId) {
				this.subId = subId;
			}

			public String getExitState() {
				return exitState;
			}

			public void setExitState(String exitState) {
				this.exitState = exitState;
			}

			public String getExitType() {
				return exitType;
			}

			public void setExitType(String exitType) {
				this.exitType = exitType;
			}

			public String getUserGroup() {
				return userGroup;
			}

			public void setUserGroup(String userGroup) {
				this.userGroup = userGroup;
			}

		}

	}

}
