package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AboutLeadDetails {

	private String sourceBranch;
	private String homeBranch;
	private String sourceOfLead;
	private String otherSourcesOfLead;

	@JsonProperty("sourceBranch")
	public String getSourceBranch() {
		return sourceBranch;
	}

	public void setSourceBranch(String sourceBranch) {
		this.sourceBranch = sourceBranch;
	}

	@JsonProperty("homeBranch")
	public String getHomeBranch() {
		return homeBranch;
	}

	public void setHomeBranch(String homeBranch) {
		this.homeBranch = homeBranch;
	}

	@JsonProperty("sourceOfLead")
	public String getSourceOfLead() {
		return sourceOfLead;
	}

	public void setSourceOfLead(String sourceOfLead) {
		this.sourceOfLead = sourceOfLead;
	}

	@JsonProperty("otherSourcesOfLead")
	public String getOtherSourcesOfLead() {
		return otherSourcesOfLead;
	}

	public void setOtherSourcesOfLead(String otherSourcesOfLead) {
		this.otherSourcesOfLead = otherSourcesOfLead;
	}

	
}
