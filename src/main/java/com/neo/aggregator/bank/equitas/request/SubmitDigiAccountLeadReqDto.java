package com.neo.aggregator.bank.equitas.request;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.neo.aggregator.bank.equitas.LeadMessageHeader;
import com.neo.aggregator.bank.equitas.SubmitAccountLeadMessageBody;
import com.neo.aggregator.bank.equitas.dto.DigiAccountRequestDto;
import com.neo.aggregator.commonservice.EquitasCommonService;
import com.neo.aggregator.dto.NeoException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("submitDigiAccountLeadReq")
public class SubmitDigiAccountLeadReqDto {

	@Autowired
	private EquitasCommonService equitasCommonService;

	protected LeadMessageHeader leadMessageHeader;
	protected SubmitAccountLeadMessageBody submitAccountLeadMessageBody;

	@JsonProperty("msgHdr")
	public LeadMessageHeader getLeadMessageHeader() {
		return leadMessageHeader;
	}

	public void setLeadMessageHeader(LeadMessageHeader leadMessageHeader) {
		this.leadMessageHeader = leadMessageHeader;
	}

	@JsonProperty("msgBdy")
	public SubmitAccountLeadMessageBody getSubmitAccountLeadMessageBody() {
		return submitAccountLeadMessageBody;
	}

	public void setSubmitAccountLeadMessageBody(SubmitAccountLeadMessageBody submitAccountLeadMessageBody) {
		this.submitAccountLeadMessageBody = submitAccountLeadMessageBody;
	}
	
	public SubmitDigiAccountLeadReqDto build(DigiAccountRequestDto request) throws NeoException {

		SubmitDigiAccountLeadReqDto requestDto = null;

		try {

			LeadMessageHeader leadMessage = equitasCommonService.buildLeadCreationMessageHeader(request);

			SubmitAccountLeadMessageBody submitAccountLeadMessage = equitasCommonService
					.buildSubmitAccountLeadMessageBody(request);

			requestDto = SubmitDigiAccountLeadReqDto.builder().submitAccountLeadMessageBody(submitAccountLeadMessage)
					.leadMessageHeader(leadMessage).build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return requestDto;
	}

}