package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LeadMessageBody {

	private String authenticationToken;
	private String entityType;
	private String entityFlagType;
	private String usrId;
	private String dedupeCompanyName;
	private String ID;
	private String LeadId;
	private String sourceOfLeadCreation;
	private String purposeOfCreation;
	private String deDupChkReqByCustCount;

	private Boolean fromMADP;
	private Boolean isAadhar;
	private Boolean ignoreProbableMatch;
	private Boolean mappedToAccountLead;

	IndividualEntry individualEntry;

	@JsonProperty("authenticationToken")
	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	@JsonProperty("entityType")
	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	@JsonProperty("entityFlagType")
	public String getEntityFlagType() {
		return entityFlagType;
	}

	public void setEntityFlagType(String entityFlagType) {
		this.entityFlagType = entityFlagType;
	}

	@JsonProperty("usrId")
	public String getUsrId() {
		return usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	@JsonProperty("dedupeCompanyName")
	public String getDedupeCompanyName() {
		return dedupeCompanyName;
	}

	public void setDedupeCompanyName(String dedupeCompanyName) {
		this.dedupeCompanyName = dedupeCompanyName;
	}

	@JsonProperty("ID")
	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	@JsonProperty("LeadId")
	public String getLeadId() {
		return LeadId;
	}

	public void setLeadId(String leadId) {
		LeadId = leadId;
	}

	@JsonProperty("sourceOfLeadCreation")
	public String getSourceOfLeadCreation() {
		return sourceOfLeadCreation;
	}

	public void setSourceOfLeadCreation(String sourceOfLeadCreation) {
		this.sourceOfLeadCreation = sourceOfLeadCreation;
	}

	@JsonProperty("individualEntry")
	public IndividualEntry getIndividualEntry() {
		return individualEntry;
	}

	public void setIndividualEntry(IndividualEntry individualEntry) {
		this.individualEntry = individualEntry;
	}

	@JsonProperty("purposeOfCreation")
	public String getPurposeOfCreation() {
		return purposeOfCreation;
	}

	public void setPurposeOfCreation(String purposeOfCreation) {
		this.purposeOfCreation = purposeOfCreation;
	}

	@JsonProperty("deDupChkReqByCustCount")
	public String getDeDupChkReqByCustCount() {
		return deDupChkReqByCustCount;
	}

	public void setDeDupChkReqByCustCount(String deDupChkReqByCustCount) {
		this.deDupChkReqByCustCount = deDupChkReqByCustCount;
	}

	@JsonProperty("fromMADP")
	public Boolean getFromMADP() {
		return fromMADP;
	}

	public void setFromMADP(Boolean fromMADP) {
		this.fromMADP = fromMADP;
	}

	@JsonProperty("isAadhar")
	public Boolean getIsAadhar() {
		return isAadhar;
	}

	public void setIsAadhar(Boolean isAadhar) {
		this.isAadhar = isAadhar;
	}

	@JsonProperty("ignoreProbableMatch")
	public Boolean getIgnoreProbableMatch() {
		return ignoreProbableMatch;
	}

	public void setIgnoreProbableMatch(Boolean ignoreProbableMatch) {
		this.ignoreProbableMatch = ignoreProbableMatch;
	}

	@JsonProperty("mappedToAccountLead")
	public Boolean getMappedToAccountLead() {
		return mappedToAccountLead;
	}

	public void setMappedToAccountLead(Boolean mappedToAccountLead) {
		this.mappedToAccountLead = mappedToAccountLead;
	}

}
