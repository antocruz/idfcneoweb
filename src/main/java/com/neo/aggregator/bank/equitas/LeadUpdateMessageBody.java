package com.neo.aggregator.bank.equitas;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LeadUpdateMessageBody {

	private String associatedWith;
	private String ID;
	private String custLeadId;
	private String authenticationToken;
	private Boolean isCurrentAddressSameAsPermanent;

	private AboutLeadDetails aboutLeadDetails;
	private AboutProspectDetails aboutProspectDetails;
	private IdentityInfoDetails identityInfoDetails;
	private FatcaTaxDetailsIndividual fatcaTaxDetailsIndividual;

	private List<AddressDetails> addressDetails;
	private List<EntityIdentityDetails> entityIdentityDetails;

	@JsonProperty("associatedWith")
	public String getAssociatedWith() {
		return associatedWith;
	}

	public void setAssociatedWith(String associatedWith) {
		this.associatedWith = associatedWith;
	}

	@JsonProperty("ID")
	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	@JsonProperty("custLeadID")
	public String getCustLeadId() {
		return custLeadId;
	}

	public void setCustLeadId(String custLeadId) {
		this.custLeadId = custLeadId;
	}

	@JsonProperty("authenticationToken")
	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	@JsonProperty("isCurrentAddressSameAsPermanent")
	public Boolean getIsCurrentAddressSameAsPermanent() {
		return isCurrentAddressSameAsPermanent;
	}

	public void setIsCurrentAddressSameAsPermanent(Boolean isCurrentAddressSameAsPermanent) {
		this.isCurrentAddressSameAsPermanent = isCurrentAddressSameAsPermanent;
	}

	@JsonProperty("aboutLeadDetails")
	public AboutLeadDetails getAboutLeadDetails() {
		return aboutLeadDetails;
	}

	public void setAboutLeadDetails(AboutLeadDetails aboutLeadDetails) {
		this.aboutLeadDetails = aboutLeadDetails;
	}

	@JsonProperty("aboutProspectDetails")
	public AboutProspectDetails getAboutProspectDetails() {
		return aboutProspectDetails;
	}

	public void setAboutProspectDetails(AboutProspectDetails aboutProspectDetails) {
		this.aboutProspectDetails = aboutProspectDetails;
	}

	@JsonProperty("IdentityInfoDetails")
	public IdentityInfoDetails getIdentityInfoDetails() {
		return identityInfoDetails;
	}

	public void setIdentityInfoDetails(IdentityInfoDetails identityInfoDetails) {
		this.identityInfoDetails = identityInfoDetails;
	}

	@JsonProperty("FatcaTaxDetailsIndividual")
	public FatcaTaxDetailsIndividual getFatcaTaxDetailsIndividual() {
		return fatcaTaxDetailsIndividual;
	}

	public void setFatcaTaxDetailsIndividual(FatcaTaxDetailsIndividual fatcaTaxDetailsIndividual) {
		this.fatcaTaxDetailsIndividual = fatcaTaxDetailsIndividual;
	}

	@JsonProperty("addressDetails")
	public List<AddressDetails> getAddressDetails() {
		return addressDetails;
	}

	public void setAddressDetails(List<AddressDetails> addressDetails) {
		this.addressDetails = addressDetails;
	}

	@JsonProperty("entityIdentityDetails")
	public List<EntityIdentityDetails> getEntityIdentityDetails() {
		return entityIdentityDetails;
	}

	public void setEntityIdentityDetails(List<EntityIdentityDetails> entityIdentityDetails) {
		this.entityIdentityDetails = entityIdentityDetails;
	}

}
