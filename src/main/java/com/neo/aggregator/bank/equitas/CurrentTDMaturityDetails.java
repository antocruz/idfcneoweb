package com.neo.aggregator.bank.equitas;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CurrentTDMaturityDetails {

	private String pytTp;
	private String prtryNb;

	@JsonProperty("pytTp")
	public String getPytTp() {
		return pytTp;
	}

	public void setPytTp(String pytTp) {
		this.pytTp = pytTp;
	}

	@JsonProperty("prtryNb")
	public String getPrtryNb() {
		return prtryNb;
	}

	public void setPrtryNb(String prtryNb) {
		this.prtryNb = prtryNb;
	}

}
