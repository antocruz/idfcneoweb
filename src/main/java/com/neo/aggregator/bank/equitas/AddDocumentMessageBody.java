package com.neo.aggregator.bank.equitas;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddDocumentMessageBody {

	private SsnAuth ssnAuth;
	private List<AddDocRq> addDocRq;

	@JsonProperty("ssnAuth")
	public SsnAuth getSsnAuth() {
		return ssnAuth;
	}

	public void setSsnAuth(SsnAuth ssnAuth) {
		this.ssnAuth = ssnAuth;
	}

	@JsonProperty("addDocRq")
	public List<AddDocRq> getAddDocRq() {
		return addDocRq;
	}

	public void setAddDocRq(List<AddDocRq> addDocRq) {
		this.addDocRq = addDocRq;
	}

}
