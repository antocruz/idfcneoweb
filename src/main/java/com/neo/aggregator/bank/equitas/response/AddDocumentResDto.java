package com.neo.aggregator.bank.equitas.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("addDocumentRep")
public class AddDocumentResDto {

	@JsonProperty("msgHdr")
	protected AddDocumentResDto.Header msgHdr;
	@JsonProperty("msgBdy")
	protected AddDocumentResDto.Body msgBdy;

	public AddDocumentResDto.Header getMsgHdr() {
		return msgHdr;
	}

	public void setMsgHdr(AddDocumentResDto.Header msgHdr) {
		this.msgHdr = msgHdr;
	}

	public AddDocumentResDto.Body getMsgBdy() {
		return msgBdy;
	}

	public void setMsgBdy(AddDocumentResDto.Body msgBdy) {
		this.msgBdy = msgBdy;
	}

	public static class Header {
		@JsonProperty(value = "rslt")
		protected String rslt;

		@JsonProperty(value = "error")
		protected List<Error> error;

		public List<Error> getError() {
			return error;
		}

		public void setError(List<Error> error) {
			this.error = error;
		}

		public String getRslt() {
			return rslt;
		}

		public void setRslt(String rslt) {
			this.rslt = rslt;
		}

		public static class Error {

			@JsonProperty(value = "cd")
			protected String cd;
			@JsonProperty(value = "rsn")
			protected String rsn;

			public String getCd() {
				return cd;
			}

			public void setCd(String cd) {
				this.cd = cd;
			}

			public String getRsn() {
				return rsn;
			}

			public void setRsn(String rsn) {
				this.rsn = rsn;
			}
		}
	}

	public static class Body {

		@JsonProperty(value = "addDocResp")
		protected List<AddDocResp> addDocResp;

		public List<AddDocResp> getAddDocResp() {
			return addDocResp;
		}

		public void setAddDocResp(List<AddDocResp> addDocResp) {
			this.addDocResp = addDocResp;
		}

		public static class AddDocResp {

			@JsonProperty(value = "sts")
			protected String sts;
			@JsonProperty(value = "docNm")
			protected String docNm;
			@JsonProperty(value = "docIndx")
			protected String docIndx;

			public String getSts() {
				return sts;
			}

			public void setSts(String sts) {
				this.sts = sts;
			}

			public String getDocNm() {
				return docNm;
			}

			public void setDocNm(String docNm) {
				this.docNm = docNm;
			}

			public String getDocIndx() {
				return docIndx;
			}

			public void setDocIndx(String docIndx) {
				this.docIndx = docIndx;
			}

		}

	}

}
