package com.neo.aggregator.bank.equitas.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("getDigiAccountDetailResponse")
public class DigiAccountDetailResDto {

	@JsonProperty("msgHdr")
	protected DigiAccountDetailResDto.Header msgHdr;
	@JsonProperty("msgBdy")
	protected DigiAccountDetailResDto.Body msgBdy;
	
	public DigiAccountDetailResDto.Header getMsgHdr() {
		return msgHdr;
	}

	public void setMsgHdr(DigiAccountDetailResDto.Header msgHdr) {
		this.msgHdr = msgHdr;
	}

	public DigiAccountDetailResDto.Body getMsgBdy() {
		return msgBdy;
	}

	public void setMsgBdy(DigiAccountDetailResDto.Body msgBdy) {
		this.msgBdy = msgBdy;
	}
	
	public static class Header {
		@JsonProperty(value = "rslt")
		protected String rslt;
		
		@JsonProperty(value = "error")
		protected List<Error> error;
		
		public List<Error> getError() {
			return error;
		}

		public void setError(List<Error> error) {
			this.error = error;
		}

		public String getRslt() {
			return rslt;
		}

		public void setRslt(String rslt) {
			this.rslt = rslt;
		}
		
		public static class Error {
			
			@JsonProperty(value = "cd")
			protected String cd;
			@JsonProperty(value = "rsn")
			protected String rsn;
			
			public String getCd() {
				return cd;
			}
			public void setCd(String cd) {
				this.cd = cd;
			}
			public String getRsn() {
				return rsn;
			}
			public void setRsn(String rsn) {
				this.rsn = rsn;
			}
		}
	}
	
	public static class Body {

		@JsonProperty(value = "accStatus")
		protected String accStatus;
		@JsonProperty(value = "accStatusDsc")
		protected String accStatusDsc;
		@JsonProperty(value = "openingDate")
		protected String openingDate;
		@JsonProperty(value = "depositNm")
		protected String depositNm;
		@JsonProperty(value = "interestRate")
		protected String interestRate;
		@JsonProperty(value = "maturityDate")
		protected String maturityDate;
		@JsonProperty(value = "maturityAmount")
		protected String maturityAmount;
		@JsonProperty(value = "intAvailable")
		protected String intAvailable;
		@JsonProperty(value = "availableBalCurrency")
		protected String availableBalCurrency;
		@JsonProperty(value = "dpstTyp")
		protected String dpstTyp;
		@JsonProperty(value = "emlAddr")
		protected String emlAddr;
		@JsonProperty(value = "custFlNm")
		protected String custFlNm;
		@JsonProperty(value = "mobNmb")
		protected String mobNmb;
		@JsonProperty(value = "amtLien")
		protected String amtLien;
		@JsonProperty(value = "custId")
		protected String custId;
		@JsonProperty(value = "custAddr")
		protected CustAddr custAddr;

		public String getAccStatus() {
			return accStatus;
		}

		public void setAccStatus(String accStatus) {
			this.accStatus = accStatus;
		}

		public String getAccStatusDsc() {
			return accStatusDsc;
		}

		public void setAccStatusDsc(String accStatusDsc) {
			this.accStatusDsc = accStatusDsc;
		}

		public String getOpeningDate() {
			return openingDate;
		}

		public void setOpeningDate(String openingDate) {
			this.openingDate = openingDate;
		}

		public String getDepositNm() {
			return depositNm;
		}

		public void setDepositNm(String depositNm) {
			this.depositNm = depositNm;
		}

		public String getInterestRate() {
			return interestRate;
		}

		public void setInterestRate(String interestRate) {
			this.interestRate = interestRate;
		}

		public String getMaturityDate() {
			return maturityDate;
		}

		public void setMaturityDate(String maturityDate) {
			this.maturityDate = maturityDate;
		}

		public String getMaturityAmount() {
			return maturityAmount;
		}

		public void setMaturityAmount(String maturityAmount) {
			this.maturityAmount = maturityAmount;
		}

		public String getIntAvailable() {
			return intAvailable;
		}

		public void setIntAvailable(String intAvailable) {
			this.intAvailable = intAvailable;
		}

		public String getAvailableBalCurrency() {
			return availableBalCurrency;
		}

		public void setAvailableBalCurrency(String availableBalCurrency) {
			this.availableBalCurrency = availableBalCurrency;
		}

		public String getDpstTyp() {
			return dpstTyp;
		}

		public void setDpstTyp(String dpstTyp) {
			this.dpstTyp = dpstTyp;
		}

		public String getEmlAddr() {
			return emlAddr;
		}

		public void setEmlAddr(String emlAddr) {
			this.emlAddr = emlAddr;
		}

		public String getCustFlNm() {
			return custFlNm;
		}

		public void setCustFlNm(String custFlNm) {
			this.custFlNm = custFlNm;
		}

		public String getMobNmb() {
			return mobNmb;
		}

		public void setMobNmb(String mobNmb) {
			this.mobNmb = mobNmb;
		}

		public String getAmtLien() {
			return amtLien;
		}

		public void setAmtLien(String amtLien) {
			this.amtLien = amtLien;
		}

		public String getCustId() {
			return custId;
		}

		public void setCustId(String custId) {
			this.custId = custId;
		}

		public CustAddr getCustAddr() {
			return custAddr;
		}

		public void setCustAddr(CustAddr custAddr) {
			this.custAddr = custAddr;
		}

		public static class CustAddr {

			@JsonProperty(value = "line1")
			protected String line1;
			@JsonProperty(value = "line2")
			protected String line2;
			@JsonProperty(value = "line3")
			protected String line3;
			@JsonProperty(value = "line4")
			protected String line4;
			@JsonProperty(value = "city")
			protected String city;
			@JsonProperty(value = "cntry")
			protected String cntry;
			@JsonProperty(value = "state")
			protected String state;
			@JsonProperty(value = "zip")
			protected String zip;

			public String getLine1() {
				return line1;
			}

			public void setLine1(String line1) {
				this.line1 = line1;
			}

			public String getLine2() {
				return line2;
			}

			public void setLine2(String line2) {
				this.line2 = line2;
			}

			public String getLine3() {
				return line3;
			}

			public void setLine3(String line3) {
				this.line3 = line3;
			}

			public String getLine4() {
				return line4;
			}

			public void setLine4(String line4) {
				this.line4 = line4;
			}

			public String getCity() {
				return city;
			}

			public void setCity(String city) {
				this.city = city;
			}

			public String getCntry() {
				return cntry;
			}

			public void setCntry(String cntry) {
				this.cntry = cntry;
			}

			public String getState() {
				return state;
			}

			public void setState(String state) {
				this.state = state;
			}

			public String getZip() {
				return zip;
			}

			public void setZip(String zip) {
				this.zip = zip;
			}

		}

	}

}
