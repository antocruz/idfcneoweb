package com.neo.aggregator.bank.equitas.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("updateDigiDocumentDetailsRep")
public class UpdateDigiDocumentResDto {

	@JsonProperty("msgHdr")
	protected UpdateDigiDocumentResDto.Header msgHdr;
	@JsonProperty("msgBdy")
	protected UpdateDigiDocumentResDto.Body msgBdy;

	public UpdateDigiDocumentResDto.Header getMsgHdr() {
		return msgHdr;
	}

	public void setMsgHdr(UpdateDigiDocumentResDto.Header msgHdr) {
		this.msgHdr = msgHdr;
	}

	public UpdateDigiDocumentResDto.Body getMsgBdy() {
		return msgBdy;
	}

	public void setMsgBdy(UpdateDigiDocumentResDto.Body msgBdy) {
		this.msgBdy = msgBdy;
	}

	public static class Header {
		@JsonProperty(value = "rslt")
		protected String rslt;

		@JsonProperty(value = "error")
		protected List<Error> error;

		public List<Error> getError() {
			return error;
		}

		public void setError(List<Error> error) {
			this.error = error;
		}

		public String getRslt() {
			return rslt;
		}

		public void setRslt(String rslt) {
			this.rslt = rslt;
		}

		public static class Error {

			@JsonProperty(value = "cd")
			protected String cd;
			@JsonProperty(value = "rsn")
			protected String rsn;

			public String getCd() {
				return cd;
			}

			public void setCd(String cd) {
				this.cd = cd;
			}

			public String getRsn() {
				return rsn;
			}

			public void setRsn(String rsn) {
				this.rsn = rsn;
			}
		}
	}

	public static class Body {

		@JsonProperty(value = "isInsert")
		protected String isInsert;

		@JsonProperty(value = "dmsDocIDs")
		protected String dmsDocIDs;

		@JsonProperty(value = "isFirstInserted")
		protected String isFirstInserted;

		@JsonProperty(value = "id")
		protected String id;

		@JsonProperty(value = "proceedToNLTable")
		protected String proceedToNLTable;

		@JsonProperty(value = "documents")
		protected List<DocumentsRes> documents;

		public String getIsInsert() {
			return isInsert;
		}

		public void setIsInsert(String isInsert) {
			this.isInsert = isInsert;
		}

		public String getDmsDocIDs() {
			return dmsDocIDs;
		}

		public void setDmsDocIDs(String dmsDocIDs) {
			this.dmsDocIDs = dmsDocIDs;
		}

		public String getIsFirstInserted() {
			return isFirstInserted;
		}

		public void setIsFirstInserted(String isFirstInserted) {
			this.isFirstInserted = isFirstInserted;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getProceedToNLTable() {
			return proceedToNLTable;
		}

		public void setProceedToNLTable(String proceedToNLTable) {
			this.proceedToNLTable = proceedToNLTable;
		}

		public List<DocumentsRes> getDocuments() {
			return documents;
		}

		public void setDocuments(List<DocumentsRes> documents) {
			this.documents = documents;
		}

		public static class DocumentsRes {

			@JsonProperty(value = "dmsDocumentID")
			protected String dmsDocumentID;
			@JsonProperty(value = "categoryCode")
			protected String categoryCode;
			@JsonProperty(value = "documentID")
			protected String documentID;
			@JsonProperty(value = "documentNo")
			protected String documentNo;
			@JsonProperty(value = "documentType")
			protected String documentType;
			@JsonProperty(value = "expiryDate")
			protected String expiryDate;
			@JsonProperty(value = "issueDate")
			protected String issueDate;
			@JsonProperty(value = "issuedAt")
			protected String issuedAt;
			@JsonProperty(value = "mappedUCIC")
			protected String mappedUCIC;
			@JsonProperty(value = "mappedCustomerLead")
			protected String mappedCustomerLead;
			@JsonProperty(value = "mappedServiceRequest")
			protected String mappedServiceRequest;
			@JsonProperty(value = "subcategoryCode")
			protected String subcategoryCode;
			@JsonProperty(value = "mappedAccount")
			protected String mappedAccount;
			@JsonProperty(value = "mappedAccountLead")
			protected String mappedAccountLead;
			@JsonProperty(value = "mappedBeneficialOwner")
			protected String mappedBeneficialOwner;
			@JsonProperty(value = "mappedGuardian")
			protected String mappedGuardian;
			@JsonProperty(value = "mappedNominee")
			protected String mappedNominee;
			@JsonProperty(value = "mappedDependent")
			protected String mappedDependent;
			@JsonProperty(value = "operation")
			protected String operation;
			@JsonProperty(value = "mappedException")
			protected String mappedException;
			@JsonProperty(value = "verifiedOn")
			protected String verifiedOn;
			@JsonProperty(value = "verifiedBy")
			protected String verifiedBy;
			@JsonProperty(value = "verificationStatus")
			protected String verificationStatus;
			@JsonProperty(value = "docTypeNOVD")
			protected String docTypeNOVD;

			public String getDmsDocumentID() {
				return dmsDocumentID;
			}

			public void setDmsDocumentID(String dmsDocumentID) {
				this.dmsDocumentID = dmsDocumentID;
			}

			public String getCategoryCode() {
				return categoryCode;
			}

			public void setCategoryCode(String categoryCode) {
				this.categoryCode = categoryCode;
			}

			public String getDocumentID() {
				return documentID;
			}

			public void setDocumentID(String documentID) {
				this.documentID = documentID;
			}

			public String getDocumentNo() {
				return documentNo;
			}

			public void setDocumentNo(String documentNo) {
				this.documentNo = documentNo;
			}

			public String getDocumentType() {
				return documentType;
			}

			public void setDocumentType(String documentType) {
				this.documentType = documentType;
			}

			public String getExpiryDate() {
				return expiryDate;
			}

			public void setExpiryDate(String expiryDate) {
				this.expiryDate = expiryDate;
			}

			public String getIssueDate() {
				return issueDate;
			}

			public void setIssueDate(String issueDate) {
				this.issueDate = issueDate;
			}

			public String getIssuedAt() {
				return issuedAt;
			}

			public void setIssuedAt(String issuedAt) {
				this.issuedAt = issuedAt;
			}

			public String getMappedUCIC() {
				return mappedUCIC;
			}

			public void setMappedUCIC(String mappedUCIC) {
				this.mappedUCIC = mappedUCIC;
			}

			public String getMappedCustomerLead() {
				return mappedCustomerLead;
			}

			public void setMappedCustomerLead(String mappedCustomerLead) {
				this.mappedCustomerLead = mappedCustomerLead;
			}

			public String getMappedServiceRequest() {
				return mappedServiceRequest;
			}

			public void setMappedServiceRequest(String mappedServiceRequest) {
				this.mappedServiceRequest = mappedServiceRequest;
			}

			public String getSubcategoryCode() {
				return subcategoryCode;
			}

			public void setSubcategoryCode(String subcategoryCode) {
				this.subcategoryCode = subcategoryCode;
			}

			public String getMappedAccount() {
				return mappedAccount;
			}

			public void setMappedAccount(String mappedAccount) {
				this.mappedAccount = mappedAccount;
			}

			public String getMappedAccountLead() {
				return mappedAccountLead;
			}

			public void setMappedAccountLead(String mappedAccountLead) {
				this.mappedAccountLead = mappedAccountLead;
			}

			public String getMappedBeneficialOwner() {
				return mappedBeneficialOwner;
			}

			public void setMappedBeneficialOwner(String mappedBeneficialOwner) {
				this.mappedBeneficialOwner = mappedBeneficialOwner;
			}

			public String getMappedGuardian() {
				return mappedGuardian;
			}

			public void setMappedGuardian(String mappedGuardian) {
				this.mappedGuardian = mappedGuardian;
			}

			public String getMappedNominee() {
				return mappedNominee;
			}

			public void setMappedNominee(String mappedNominee) {
				this.mappedNominee = mappedNominee;
			}

			public String getMappedDependent() {
				return mappedDependent;
			}

			public void setMappedDependent(String mappedDependent) {
				this.mappedDependent = mappedDependent;
			}

			public String getOperation() {
				return operation;
			}

			public void setOperation(String operation) {
				this.operation = operation;
			}

			public String getMappedException() {
				return mappedException;
			}

			public void setMappedException(String mappedException) {
				this.mappedException = mappedException;
			}

			public String getVerifiedOn() {
				return verifiedOn;
			}

			public void setVerifiedOn(String verifiedOn) {
				this.verifiedOn = verifiedOn;
			}

			public String getVerifiedBy() {
				return verifiedBy;
			}

			public void setVerifiedBy(String verifiedBy) {
				this.verifiedBy = verifiedBy;
			}

			public String getVerificationStatus() {
				return verificationStatus;
			}

			public void setVerificationStatus(String verificationStatus) {
				this.verificationStatus = verificationStatus;
			}

			public String getDocTypeNOVD() {
				return docTypeNOVD;
			}

			public void setDocTypeNOVD(String docTypeNOVD) {
				this.docTypeNOVD = docTypeNOVD;
			}

		}

	}

}
