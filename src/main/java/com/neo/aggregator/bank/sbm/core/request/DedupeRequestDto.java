package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DedupeRequestDto {

	@XmlElement(name = "Header", required = true)
	protected Header header;
	@XmlElement(name = "Body", required = true)
	protected DedupeRequestDto.Body body;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header value) {
		this.header = value;
	}

	public DedupeRequestDto.Body getBody() {
		return body;
	}

	public void setBody(DedupeRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "executeFinacleScriptRequest" })
	@AllArgsConstructor
	@NoArgsConstructor
	@Builder
	public static class Body {

		@XmlElement(required = true)
		protected DedupeRequestDto.Body.ExecuteFinacleScriptRequest executeFinacleScriptRequest;

		public DedupeRequestDto.Body.ExecuteFinacleScriptRequest getExecuteFinacleScriptRequest() {
			return executeFinacleScriptRequest;
		}

		public void setExecuteFinacleScriptRequest(DedupeRequestDto.Body.ExecuteFinacleScriptRequest value) {
			this.executeFinacleScriptRequest = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "executeFinacleScriptInputVO", "executeFinacleScriptCustomData" })
		@AllArgsConstructor
		@NoArgsConstructor
		@Builder
		public static class ExecuteFinacleScriptRequest {

			@XmlElement(name = "ExecuteFinacleScriptInputVO", required = true)
			protected DedupeRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptInputVO executeFinacleScriptInputVO;
			@XmlElement(name = "executeFinacleScript_CustomData", required = true)
			protected DedupeRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptCustomData executeFinacleScriptCustomData;

			public DedupeRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptInputVO getExecuteFinacleScriptInputVO() {
				return executeFinacleScriptInputVO;
			}

			public void setExecuteFinacleScriptInputVO(
					DedupeRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptInputVO value) {
				this.executeFinacleScriptInputVO = value;
			}

			public DedupeRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptCustomData getExecuteFinacleScriptCustomData() {
				return executeFinacleScriptCustomData;
			}

			public void setExecuteFinacleScriptCustomData(
					DedupeRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptCustomData value) {
				this.executeFinacleScriptCustomData = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "custPassportNo", "custFirstName", "custType", "custDOB", "custLastName",
					"custPAN", "corpName", "corpRegNo", "inCorpDate" })
			@AllArgsConstructor
			@NoArgsConstructor
			@Builder
			public static class ExecuteFinacleScriptCustomData {

				@XmlElement(required = true)
				protected String custPassportNo;
				@XmlElement(required = true)
				protected String custFirstName;
				@XmlElement(name = "CustType", required = true)
				protected String custType;
				@XmlElement(required = true)
				protected String custDOB;
				@XmlElement(required = true)
				protected String custLastName;
				@XmlElement(required = true)
				protected String custPAN;
				@XmlElement(required = true)
				protected String corpName;
				@XmlElement(required = true)
				protected String corpRegNo;
				@XmlElement(required = true)
				protected String inCorpDate;

				public String getCustPassportNo() {
					return custPassportNo;
				}

				public void setCustPassportNo(String value) {
					this.custPassportNo = value;
				}

				public String getCustFirstName() {
					return custFirstName;
				}

				public void setCustFirstName(String value) {
					this.custFirstName = value;
				}

				public String getCustType() {
					return custType;
				}

				public void setCustType(String value) {
					this.custType = value;
				}

				public String getCustDOB() {
					return custDOB;
				}

				public void setCustDOB(String value) {
					this.custDOB = value;
				}

				public String getCustLastName() {
					return custLastName;
				}

				public void setCustLastName(String value) {
					this.custLastName = value;
				}

				public String getCustPAN() {
					return custPAN;
				}

				public void setCustPAN(String value) {
					this.custPAN = value;
				}

				public String getCorpName() {
					return corpName;
				}

				public void setCorpName(String value) {
					this.corpName = value;
				}

				public String getCorpRegNo() {
					return corpRegNo;
				}

				public void setCorpRegNo(String value) {
					this.corpRegNo = value;
				}

				public String getInCorpDate() {
					return inCorpDate;
				}

				public void setInCorpDate(String value) {
					this.inCorpDate = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "requestId" })
			@AllArgsConstructor
			@NoArgsConstructor
			@Builder
			public static class ExecuteFinacleScriptInputVO {

				@XmlElement(required = true)
				protected String requestId;

				public String getRequestId() {
					return requestId;
				}

				public void setRequestId(String value) {
					this.requestId = value;
				}

			}

		}

	}



}

