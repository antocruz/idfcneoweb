package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.Builder;

@Builder
public class RetCustAddRq {
	
	protected CustDtls custDtls;
	protected RelatedDtls relatedDtls;
	
	@XmlElement(name = "CustDtls")
	public CustDtls getCustDtls() {
		return custDtls;
	}
	public void setCustDtls(CustDtls custDtls) {
		this.custDtls = custDtls;
	}
	
	@XmlElement(name = "RelatedDtls")
	public RelatedDtls getRelatedDtls() {
		return relatedDtls;
	}
	public void setRelatedDtls(RelatedDtls relatedDtls) {
		this.relatedDtls = relatedDtls;
	}
	
	

}
