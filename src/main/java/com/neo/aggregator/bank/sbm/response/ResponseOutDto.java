package com.neo.aggregator.bank.sbm.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "Response_Out")
public class ResponseOutDto {

	@XmlElement(name = "ChannelId", required = true)
	protected String channelId;
	@XmlElement(name = "PartnerReqID", required = true)
	protected String partnerReqID;
	@XmlElement(name = "Partner_UserName", required = true)
	protected String partnerUserName;
	@XmlElement(name = "Timestamp", required = true)
	protected String timestamp;
	@XmlElement(name = "Response", required = true)
	protected String response;
	@XmlElement(name = "Status")
	protected String status;
	@XmlElement(name = "StatusCode", required = true)
	protected String statusCode;
	@XmlElement(name = "Message", required = true)
	protected String message;

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String value) {
		this.channelId = value;
	}

	public String getPartnerReqID() {
		return partnerReqID;
	}

	public void setPartnerReqID(String value) {
		this.partnerReqID = value;
	}

	public String getPartnerUserName() {
		return partnerUserName;
	}

	public void setPartnerUserName(String value) {
		this.partnerUserName = value;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String value) {
		this.timestamp = value;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String value) {
		this.response = value;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String value) {
		this.status = value;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String value) {
		this.statusCode = value;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String value) {
		this.message = value;
	}

}
