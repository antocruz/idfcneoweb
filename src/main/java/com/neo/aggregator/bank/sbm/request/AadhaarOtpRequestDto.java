package com.neo.aggregator.bank.sbm.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AadhaarOtpRequestDto {

	@JsonProperty("AadharValidation")
	private String aadhaarValidation;

	@JsonProperty("AadharNo")
	private String aadharNo;

	@JsonProperty("STAN")
	private String stan;

	@JsonProperty("RequestId")
	private String requestId;

	@JsonProperty("LocalTransactionDateTime")
	private String localTransactionDateTime;

	@JsonProperty("Channel")
	private String channel;

	@JsonProperty("Bank")
	private String bank;

	@JsonProperty("TPA")
	private String tpa;

	@JsonProperty("OTP")
	private String otp;

}
