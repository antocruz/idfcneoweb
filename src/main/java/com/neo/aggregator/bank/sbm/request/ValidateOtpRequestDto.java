package com.neo.aggregator.bank.sbm.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "requestUUID", "stan", "otp", "resendOtp", "trfType" })
@XmlRootElement(name = "data")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ValidateOtpRequestDto {

	@XmlElement(name = "RequestUUID", required = true)
	protected String requestUUID;
	@XmlElement(name = "Stan")
	protected String stan;
	@XmlElement(name = "Otp")
	protected String otp;
	@XmlElement(name = "ResendOtp", required = true)
	protected String resendOtp;
	@XmlElement(name = "TrfType", required = true)
	protected String trfType;

	public String getRequestUUID() {
		return requestUUID;
	}

	public void setRequestUUID(String value) {
		this.requestUUID = value;
	}

	public String getStan() {
		return stan;
	}

	public void setStan(String value) {
		this.stan = value;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String value) {
		this.otp = value;
	}

	public String getResendOtp() {
		return resendOtp;
	}

	public void setResendOtp(String value) {
		this.resendOtp = value;
	}

	public String getTrfType() {
		return trfType;
	}

	public void setTrfType(String value) {
		this.trfType = value;
	}

}
