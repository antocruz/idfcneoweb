package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FetchAccountStatementRequestDto {

	@XmlElement(name = "Header")
	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body", required = true)
	protected FetchAccountStatementRequestDto.Body body;

	public FetchAccountStatementRequestDto.Body getBody() {
		return body;
	}

	public void setBody(FetchAccountStatementRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "getFullAccountStatementWithPaginationRequest" })
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Body {

		@XmlElement(required = true)
		protected FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest getFullAccountStatementWithPaginationRequest;

		public FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest getGetFullAccountStatementWithPaginationRequest() {
			return getFullAccountStatementWithPaginationRequest;
		}

		public void setGetFullAccountStatementWithPaginationRequest(
				FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest value) {
			this.getFullAccountStatementWithPaginationRequest = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "paginatedAccountTransactionCriteria" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class GetFullAccountStatementWithPaginationRequest {

			@XmlElement(name = "PaginatedAccountTransactionCriteria", required = true)
			protected FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest.PaginatedAccountTransactionCriteria paginatedAccountTransactionCriteria;

			public FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest.PaginatedAccountTransactionCriteria getPaginatedAccountTransactionCriteria() {
				return paginatedAccountTransactionCriteria;
			}

			public void setPaginatedAccountTransactionCriteria(
					FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest.PaginatedAccountTransactionCriteria value) {
				this.paginatedAccountTransactionCriteria = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "acid", "branchId", "fromDate", "toDate", "paginationDetails" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class PaginatedAccountTransactionCriteria {

				@XmlElement(name = "paginationDetails", required = false)
				protected FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest.PaginatedAccountTransactionCriteria.PaginationDetails paginationDetails;

				protected String acid;
				protected String branchId;
				@XmlElement(required = true)
				protected String fromDate;
				@XmlElement(required = true)
				protected String toDate;

				public String getAcid() {
					return acid;
				}

				public void setAcid(String value) {
					this.acid = value;
				}

				public String getBranchId() {
					return branchId;
				}

				public void setBranchId(String value) {
					this.branchId = value;
				}

				public String getFromDate() {
					return fromDate;
				}

				public void setFromDate(String value) {
					this.fromDate = value;
				}

				public String getToDate() {
					return toDate;
				}

				public void setToDate(String value) {
					this.toDate = value;
				}
				
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = {
				    "lastBalance",
				    "lastPstdDate",
				    "lastTxnDate",
				    "lastTxnId",
				    "lastTxnSrlNo"
				})
				@XmlRootElement(name = "paginationDetails")
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class PaginationDetails {

				    @XmlElement(required = true)
				    protected PaginationDetails.LastBalance lastBalance;
				    @XmlElement(required = true)
				    @XmlSchemaType(name = "dateTime")
					protected String lastPstdDate;
				    @XmlElement(required = true)
				    @XmlSchemaType(name = "dateTime")
					protected String lastTxnDate;
				    @XmlElement(required = true)
				    protected String lastTxnId;
					protected String lastTxnSrlNo;

				    public PaginationDetails.LastBalance getLastBalance() {
				        return lastBalance;
				    }

				    public void setLastBalance(PaginationDetails.LastBalance value) {
				        this.lastBalance = value;
				    }

					public String getLastPstdDate() {
				        return lastPstdDate;
				    }

					public void setLastPstdDate(String value) {
				        this.lastPstdDate = value;
				    }

					public String getLastTxnDate() {
				        return lastTxnDate;
				    }

					public void setLastTxnDate(String value) {
				        this.lastTxnDate = value;
				    }

				    public String getLastTxnId() {
				        return lastTxnId;
				    }

				    public void setLastTxnId(String value) {
				        this.lastTxnId = value;
				    }

					public String getLastTxnSrlNo() {
				        return lastTxnSrlNo;
				    }

					public void setLastTxnSrlNo(String value) {
				        this.lastTxnSrlNo = value;
				    }


				    @XmlAccessorType(XmlAccessType.FIELD)
				    @XmlType(name = "", propOrder = {
				        "amountValue",
				        "currencyCode"
				    })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
				    public static class LastBalance {

						protected String amountValue;
				        @XmlElement(required = true)
				        protected String currencyCode;

						public String getAmountValue() {
				            return amountValue;
				        }

						public void setAmountValue(String value) {
				            this.amountValue = value;
				        }

				        public String getCurrencyCode() {
				            return currencyCode;
				        }

				        public void setCurrencyCode(String value) {
				            this.currencyCode = value;
				        }

				    }
				}

				public FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest.PaginatedAccountTransactionCriteria.PaginationDetails getPaginationDetails() {
					return paginationDetails;
				}

				public void setPaginationDetails(
						FetchAccountStatementRequestDto.Body.GetFullAccountStatementWithPaginationRequest.PaginatedAccountTransactionCriteria.PaginationDetails paginationDetails) {
					this.paginationDetails = paginationDetails;
				}

			}

		}

	}
}

