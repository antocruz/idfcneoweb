package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;
import com.neo.aggregator.bank.sbm.RetCustAddRequestBody;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlType(propOrder = { "header", "body" })
public class RetCustAddRequestDto {

	protected Header header;

	protected RetCustAddRequestBody body;

	@XmlElement(name = "Header")
	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body")
	public RetCustAddRequestBody getBody() {
		return body;
	}

	public void setBody(RetCustAddRequestBody body) {
		this.body = body;
	}

}
