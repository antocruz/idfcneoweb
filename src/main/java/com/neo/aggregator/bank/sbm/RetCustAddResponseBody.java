package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.Builder;

@Builder
public class RetCustAddResponseBody {
	
	
	protected RetCustAddResponse RetCustAddResponseBody;

	@XmlElement(name = "RetCustAddResponse")
	public RetCustAddResponse getRetCustAddResponseBody() {
		return RetCustAddResponseBody;
	}

	public void setRetCustAddResponseBody(RetCustAddResponse retCustAddResponseBody) {
		RetCustAddResponseBody = retCustAddResponseBody;
	}

}
