package com.neo.aggregator.bank.sbm.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IMPSResponseDto {

	@JsonProperty("MSGID")
	private String msgid;

	@JsonProperty("REMITTERACC")
	private String remitteracc;

	@JsonProperty("REMITTERNAME1")
	private String remittername1;

	@JsonProperty("REMITTERNAME2")
	private String remittername2;

	@JsonProperty("TXNAMT")
	private Double txnamt;

	@JsonProperty("BENIFICIARYACC")
	private String benificiaryacc;

	@JsonProperty("BENFNAME")
	private String benfname;

	@JsonProperty("BENFADDR1")
	private String benfaddr1;

	@JsonProperty("BENFADDR2")
	private String benfaddr2;

	@JsonProperty("BENFADDR3")
	private String benfaddr3;

	@JsonProperty("DeviceID")
	private String deviceid;

	@JsonProperty("MobileNumber")
	private String mobilenumber;

	@JsonProperty("DeviceLocation")
	private String devicelocation;

	@JsonProperty("BRANCH_CODE")
	private String branch_code;

	@JsonProperty("CUST_AC_NO")
	private String cust_ac_no;

	@JsonProperty("SOURCE")
	private String source;

	@JsonProperty("UBSCOMP")
	private String ubscomp;

	@JsonProperty("CORRELID")
	private String correlid;

	@JsonProperty("USERID")
	private String userid;

	@JsonProperty("BRANCH")
	private String branch;

	@JsonProperty("MODULEID")
	private String moduleid;

	@JsonProperty("SERVICE")
	private String service;

	@JsonProperty("OPERATION")
	private String operation;

	@JsonProperty("MSGSTAT")
	private String msgstat;

	@JsonProperty("PRD")
	private String prd;

	@JsonProperty("BRN")
	private String brn;

	@JsonProperty("MODULE")
	private String module;

	@JsonProperty("DESTINATION")
	private String destination;

	@JsonProperty("MULTITRIPID")
	private String multitripid;

	@JsonProperty("FUNCTIONID")
	private String functionid;

	@JsonProperty("ACTION")
	private String action;

	@JsonProperty("NARRATIVE")
	private String narrative;

	@JsonProperty("XREF")
	private String xref;

	@JsonProperty("FCCREF")
	private String fccref;

	@JsonProperty("OFFSETCCY")
	private String offsetccy;

	@JsonProperty("ReferenceNumber")
	private String referencenumber;

	@JsonProperty("BranchCode")
	private Long branchcode;

	@JsonProperty("BankCode")
	private Long bankcode;

	@JsonProperty("IsMobileFT")
	private Boolean ismobileft;

	@JsonProperty("IsAccountFT")
	private Boolean isaccountft;

	@JsonProperty("TransactionRefrenceNumber")
	private String transactionrefrencenumber;

	@JsonProperty("DateTime")
	private String datetime;

	@JsonProperty("SerializedMessage")
	private String serializedmessage;

	@JsonProperty("CCY")
	private String ccy;

	@JsonProperty("MSGTYPE")
	private String msgtype;

	@JsonProperty("RECEIVER")
	private String receiver;

	@JsonProperty("MSGSTATUS")
	private String msgstatus;

	@JsonProperty("TRNDT")
	private String trndt;

	@JsonProperty("ResponseData")
	private String responsedata;

	@JsonProperty("ResponseCode")
	private String responsecode;

	@JsonProperty("ResponseDesc")
	private String responsedesc;

	@JsonProperty("HOSTResponseCODE")
	private String hostresponsecode;

	@JsonProperty("HOSTResponseDesc")
	private String hostresponsedesc;

	@JsonProperty("CurrentBalance")
	private String currentbalance;

	@JsonProperty("OpeningBalance")
	private String openingbalance;

	@JsonProperty("AvailableBalance")
	private String availablebalance;

	@JsonProperty("MinistatementData")
	private String ministatementdata;

	@JsonProperty("HostWarningCode")
	private String hostwarningcode;

	@JsonProperty("HostWarningDesc")
	private String hostwarningdesc;

	@JsonProperty("AmountAvailable")
	private String amountavailable;

	@JsonProperty("FtLimit")
	private String ftlimit;

	@JsonProperty("AccountUseLimit")
	private String accountuselimit;

	@JsonProperty("AccountUseCount")
	private String accountusecount;

	@JsonProperty("LastDate")
	private String lastdate;

	@JsonProperty("LastTime")
	private String lasttime;

	@JsonProperty("MaxPinCount")
	private String maxpincount;

	@JsonProperty("MaxPinUseCount")
	private String maxpinusecount;

	@JsonProperty("PinOffset")
	private String pinoffset;

	@JsonProperty("BTResponse")
	private String btresponse;

	@JsonProperty("ConsumerName")
	private String consumername;

	@JsonProperty("OutstandingAmount")
	private String outstandingamount;

	@JsonProperty("RecentTransactions")
	private String recenttransactions;

	@JsonProperty("MinMaxData")
	private String minmaxdata;

	@JsonProperty("ListOfLoanAccounts")
	private String listofloanaccounts;

	@JsonProperty("ListOfCustomerID")
	private String listofcustomerid;

	@JsonProperty("LoanHolderName")
	private String loanholdername;

	@JsonProperty("LoanAccountNumber")
	private String loanaccountnumber;

	@JsonProperty("AmountFinanced")
	private String amountfinanced;

	@JsonProperty("PrincipalOutstanding")
	private String principaloutstanding;

	@JsonProperty("InterestOutstanding")
	private String interestoutstanding;

	@JsonProperty("PenalityAmount")
	private String penalityamount;

	@JsonProperty("TotalOutstandingAmount")
	private String totaloutstandingamount;

	@JsonProperty("LoanDisbursment")
	private String loandisbursment;

	@JsonProperty("MaturityDate")
	private String maturitydate;

	@JsonProperty("OverDueDays")
	private String overduedays;

	@JsonProperty("EMIAMOUNT")
	private String emiamount;

	@JsonProperty("ExpectedInterest")
	private String expectedinterest;

	@JsonProperty("LoanInterestRate")
	private String loaninterestrate;

	@JsonProperty("RDTD_Amount")
	private Double rdtd_amount;

	@JsonProperty("BTFUNCTIONID")
	private String btfunctionid;

	@JsonProperty("RecurringTermDetails")
	private String recurringtermdetails;

	@JsonProperty("NPPFLOANAC")
	private String nppfloanac;

	@JsonProperty("NationalID")
	private String nationalid;

	@JsonProperty("FlatCode")
	private String flatcode;

	@JsonProperty("WaterDetails")
	private String waterdetails;

	@JsonProperty("TotalAmount")
	private String totalamount;

	@JsonProperty("TotalPenalityAmount")
	private String totalpenalityamount;

	@JsonProperty("GrandTotal")
	private String grandtotal;

	@JsonProperty("VoucherNumber")
	private String vouchernumber;

	@JsonProperty("AccountNumber")
	private String accountnumber;

	@JsonProperty("OTP")
	private String otp;

	@JsonProperty("ListOfAccount")
	private String listofaccount;

	@JsonProperty("CustomerID")
	private String customerid;

	@JsonProperty("TextMessage")
	private String textmessage;

	@JsonProperty("TerminalID")
	private String terminalid;

	@JsonProperty("TerminalLocation")
	private String terminallocation;

	@JsonProperty("HMAC")
	private String hmac;

	@JsonProperty("REMITTERNAME")
	private String remittername;

	@JsonProperty("BeneficiaryIFSC")
	private String beneficiaryifsc;

	@JsonProperty("BENIFICIARYMOBILE")
	private String benificiarymobile;


}

