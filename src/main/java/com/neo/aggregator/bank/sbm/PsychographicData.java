package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.Builder;

@Builder
public class PsychographicData {
	
	protected String custCurrCode;
	protected String preferredLocale;
	protected PsychographMiscData miscData;
	
	@XmlElement(name = "PsychographMiscData")
	public PsychographMiscData getMiscData() {
		return miscData;
	}
	public void setMiscData(PsychographMiscData miscData) {
		this.miscData = miscData;
	}
	
	@XmlElement(name = "CustCurrCode")
	public String getCustCurrCode() {
		return custCurrCode;
	}
	public void setCustCurrCode(String custCurrCode) {
		this.custCurrCode = custCurrCode;
	}
	
	@XmlElement(name = "preferred_Locale")
	public String getPreferredLocale() {
		return preferredLocale;
	}
	public void setPreferredLocale(String preferredLocale) {
		this.preferredLocale = preferredLocale;
	}

}
