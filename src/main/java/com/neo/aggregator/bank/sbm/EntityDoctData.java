package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.Builder;

@Builder
public class EntityDoctData {
	
	protected String countryOfIssue;
	protected String docCode;
	protected String issueDt;
	protected String expDt;
	protected String typeCode;
	protected String placeOfIssue;
	protected String referenceNum;
	protected String preferredUniqueId;
	protected String idIssuedOrganisation;
	
	@XmlElement(name = "CountryOfIssue")
	public String getCountryOfIssue() {
		return countryOfIssue;
	}
	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}
	
	@XmlElement(name = "DocCode")
	public String getDocCode() {
		return docCode;
	}
	public void setDocCode(String docCode) {
		this.docCode = docCode;
	}
	
	@XmlElement(name = "IssueDt")
	public String getIssueDt() {
		return issueDt;
	}
	public void setIssueDt(String issueDt) {
		this.issueDt = issueDt;
	}
	
	@XmlElement(name = "ExpDt")
	public String getExpDt() {
		return expDt;
	}
	public void setExpDt(String expDt) {
		this.expDt = expDt;
	}
	
	@XmlElement(name = "TypeCode")
	public String getTypeCode() {
		return typeCode;
	}
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	
	@XmlElement(name = "PlaceOfIssue")
	public String getPlaceOfIssue() {
		return placeOfIssue;
	}
	public void setPlaceOfIssue(String placeOfIssue) {
		this.placeOfIssue = placeOfIssue;
	}
	
	@XmlElement(name = "ReferenceNum")
	public String getReferenceNum() {
		return referenceNum;
	}
	public void setReferenceNum(String referenceNum) {
		this.referenceNum = referenceNum;
	}
	
	@XmlElement(name = "preferredUniqueId")
	public String getPreferredUniqueId() {
		return preferredUniqueId;
	}
	public void setPreferredUniqueId(String preferredUniqueId) {
		this.preferredUniqueId = preferredUniqueId;
	}
	
	@XmlElement(name = "IDIssuedOrganisation")
	public String getIdIssuedOrganisation() {
		return idIssuedOrganisation;
	}
	public void setIdIssuedOrganisation(String idIssuedOrganisation) {
		this.idIssuedOrganisation = idIssuedOrganisation;
	}
}
