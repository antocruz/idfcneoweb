package com.neo.aggregator.bank.sbm.response;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AadhaarOtpResponseDto {
	
	@JsonProperty("RequestId")
	private String requestId;
	
	@JsonProperty("ResponseCode")
	private String responseCode;
	
	@JsonProperty("ResponseMsg")
	@JsonAlias({ "Responsemsg" })
	private String responseMsg;
	
	@JsonProperty("LocalTransactionDateTime")
	private String localTransactionDateTime;
	
	@JsonProperty("Stan")
	@JsonAlias({ "STAN" })
	private String stan;

	@JsonProperty("ReferenceKey")
	private String referenceKey;

	@JsonProperty("EkycData")
	private String ekycData;

}
