package com.neo.aggregator.bank.sbm;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import lombok.Builder;

@Builder
public class CustData {
	
	protected List<AddrDtls> addDtls;
	protected List<PhoneEmailDtls> phoneEmailDtls;
	
	protected String birthDt;
	protected String birthMonth;
	protected String birthYear;
	protected String createdBySystemid;
	protected String dateOfBirth;
	protected String firstName;
	protected String language;
	protected String lastName;
	protected String middleName;
	protected String isMinor;
	protected String isCustnre;
	protected String defaultAddrType;
	protected String gender;
	protected String manager;
	protected String nativeLanguageCode;
	protected String prefName;
	protected String region;
	protected String primarySolid;
	protected String relationshipOpeningDt;
	protected String salutation;
	protected String segmentationClass;
	protected String shortName;
	protected String staffFlag;
	protected String subSegment;
	protected String taxDeductionTable;
	protected String tradeFinFlag;
	protected String smsBankingMobileNumber;
	protected String isSmsBankingEnabled;
	protected String isEbankingEnabled;
	protected String occupation;
    protected String custType;
    protected String constitutionCode;
	protected String autoApprovalCif;
	protected String enableAlerts;
	protected String nreBecomingDate;
	


	@XmlElement(name = "PhoneEmailDtls")
	public List<PhoneEmailDtls> getPhoneEmailDtls() {
		return phoneEmailDtls;
	}
	public void setPhoneEmailDtls(List<PhoneEmailDtls> phoneEmailDtls) {
		this.phoneEmailDtls = phoneEmailDtls;
	}
	
	@XmlElement(name = "AddrDtls")
	public List<AddrDtls> getAddDtls() {
		return addDtls;
	}
	public void setAddDtls(List<AddrDtls> addDtls) {
		this.addDtls = addDtls;
	}
	
	@XmlElement(name = "BirthDt")
	public String getBirthDt() {
		return birthDt;
	}
	public void setBirthDt(String birthDt) {
		this.birthDt = birthDt;
	}
	
	@XmlElement(name = "BirthMonth")
	public String getBirthMonth() {
		return birthMonth;
	}
	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}
	
	@XmlElement(name = "BirthYear")
	public String getBirthYear() {
		return birthYear;
	}
	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}
	
	@XmlElement(name = "CreatedBySystemId")
	public String getCreatedBySystemid() {
		return createdBySystemid;
	}
	public void setCreatedBySystemid(String createdBySystemid) {
		this.createdBySystemid = createdBySystemid;
	}
	
	@XmlElement(name = "DateOfBirth")
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	@XmlElement(name = "FirstName")
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@XmlElement(name = "Language")
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	@XmlElement(name = "LastName")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@XmlElement(name = "MiddleName")
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	
	@XmlElement(name = "IsMinor")
	public String getIsMinor() {
		return isMinor;
	}
	public void setIsMinor(String isMinor) {
		this.isMinor = isMinor;
	}
	
	@XmlElement(name = "IsCustNRE")
	public String getIsCustnre() {
		return isCustnre;
	}
	public void setIsCustnre(String isCustnre) {
		this.isCustnre = isCustnre;
	}
	
	@XmlElement(name = "DefaultAddrType")
	public String getDefaultAddrType() {
		return defaultAddrType;
	}
	public void setDefaultAddrType(String defaultAddrType) {
		this.defaultAddrType = defaultAddrType;
	}
	
	@XmlElement(name = "Gender")
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@XmlElement(name = "Manager")
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	
	@XmlElement(name = "NativeLanguageCode")
	public String getNativeLanguageCode() {
		return nativeLanguageCode;
	}
	public void setNativeLanguageCode(String nativeLanguageCode) {
		this.nativeLanguageCode = nativeLanguageCode;
	}
	
	@XmlElement(name = "PrefName")
	public String getPrefName() {
		return prefName;
	}
	public void setPrefName(String prefName) {
		this.prefName = prefName;
	}
	
	@XmlElement(name = "Region")
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	
	@XmlElement(name = "PrimarySolId")
	public String getPrimarySolid() {
		return primarySolid;
	}
	public void setPrimarySolid(String primarySolid) {
		this.primarySolid = primarySolid;
	}
	
	@XmlElement(name = "RelationshipOpeningDt")
	public String getRelationshipOpeningDt() {
		return relationshipOpeningDt;
	}
	public void setRelationshipOpeningDt(String relationshipOpeningDt) {
		this.relationshipOpeningDt = relationshipOpeningDt;
	}
	
	@XmlElement(name = "Salutation")
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	
	@XmlElement(name = "SegmentationClass")
	public String getSegmentationClass() {
		return segmentationClass;
	}
	public void setSegmentationClass(String segmentationClass) {
		this.segmentationClass = segmentationClass;
	}
	
	@XmlElement(name = "ShortName")
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	
	@XmlElement(name = "StaffFlag")
	public String getStaffFlag() {
		return staffFlag;
	}
	public void setStaffFlag(String staffFlag) {
		this.staffFlag = staffFlag;
	}
	
	@XmlElement(name = "SubSegment")
	public String getSubSegment() {
		return subSegment;
	}
	public void setSubSegment(String subSegment) {
		this.subSegment = subSegment;
	}
	
	@XmlElement(name = "TaxDeductionTable")
	public String getTaxDeductionTable() {
		return taxDeductionTable;
	}
	public void setTaxDeductionTable(String taxDeductionTable) {
		this.taxDeductionTable = taxDeductionTable;
	}
	
	@XmlElement(name = "TradeFinFlag")
	public String getTradeFinFlag() {
		return tradeFinFlag;
	}
	public void setTradeFinFlag(String tradeFinFlag) {
		this.tradeFinFlag = tradeFinFlag;
	}
	
	@XmlElement(name = "SMSBankingMobileNumber")
	public String getSmsBankingMobileNumber() {
		return smsBankingMobileNumber;
	}
	public void setSmsBankingMobileNumber(String smsBankingMobileNumber) {
		this.smsBankingMobileNumber = smsBankingMobileNumber;
	}
	
	@XmlElement(name = "IsSMSBankingEnabled")
	public String getIsSmsBankingEnabled() {
		return isSmsBankingEnabled;
	}
	public void setIsSmsBankingEnabled(String isSmsBankingEnabled) {
		this.isSmsBankingEnabled = isSmsBankingEnabled;
	}
	
	@XmlElement(name = "IsEbankingEnabled")
	public String getIsEbankingEnabled() {
		return isEbankingEnabled;
	}
	public void setIsEbankingEnabled(String isEbankingEnabled) {
		this.isEbankingEnabled = isEbankingEnabled;
	}
	
	@XmlElement(name = "Occupation")
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	
	@XmlElement(name = "Cust_Type")	
	public String getCustType() {
		return custType;
	}
	public void setCustType(String custType) {
		this.custType = custType;
	}
	
	@XmlElement(name = "Constitution_Code")
	public String getConstitutionCode() {
		return constitutionCode;
	}
	public void setConstitutionCode(String constitutionCode) {
		this.constitutionCode = constitutionCode;
	}

	@XmlElement(name = "AutoApproval")
	public String getAutoApprovalCif() {
		return autoApprovalCif;
	}

	public void setAutoApprovalCif(String autoApprovalCif) {
		this.autoApprovalCif = autoApprovalCif;
	}
	
	@XmlElement(name = "enableAlerts")
	public String getEnableAlerts() {
		return enableAlerts;
	}

	public void setEnableAlerts(String enableAlerts) {
		this.enableAlerts = enableAlerts;
	}
	
	@XmlElement(name = "NREBecomingDt")
	public String getNreBecomingDate() {
		return nreBecomingDate;
	}
	
	public void setNreBecomingDate(String nreBecomingDate) {
		this.nreBecomingDate = nreBecomingDate;
	}
}
