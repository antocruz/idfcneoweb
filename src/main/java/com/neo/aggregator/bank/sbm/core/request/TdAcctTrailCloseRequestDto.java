package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TdAcctTrailCloseRequestDto {

	@XmlElement(name = "Header")
	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body", required = true)
	protected TdAcctTrailCloseRequestDto.Body body;

	public TdAcctTrailCloseRequestDto.Body getBody() {
		return body;
	}

	public void setBody(TdAcctTrailCloseRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "executeFinacleScriptRequest" })
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Body {

		@XmlElement(required = true)
		protected TdAcctTrailCloseRequestDto.Body.ExecuteFinacleScriptRequest executeFinacleScriptRequest;

		public TdAcctTrailCloseRequestDto.Body.ExecuteFinacleScriptRequest getExecuteFinacleScriptRequest() {
			return executeFinacleScriptRequest;
		}

		public void setExecuteFinacleScriptRequest(TdAcctTrailCloseRequestDto.Body.ExecuteFinacleScriptRequest value) {
			this.executeFinacleScriptRequest = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "executeFinacleScriptInputVO", "executeFinacleScriptCustomData" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class ExecuteFinacleScriptRequest {

			@XmlElement(name = "ExecuteFinacleScriptInputVO", required = true)
			protected TdAcctTrailCloseRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptInputVO executeFinacleScriptInputVO;
			@XmlElement(name = "executeFinacleScript_CustomData", required = true)
			protected TdAcctTrailCloseRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptCustomData executeFinacleScriptCustomData;

			public TdAcctTrailCloseRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptInputVO getExecuteFinacleScriptInputVO() {
				return executeFinacleScriptInputVO;
			}

			public void setExecuteFinacleScriptInputVO(
					TdAcctTrailCloseRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptInputVO value) {
				this.executeFinacleScriptInputVO = value;
			}

			public TdAcctTrailCloseRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptCustomData getExecuteFinacleScriptCustomData() {
				return executeFinacleScriptCustomData;
			}

			public void setExecuteFinacleScriptCustomData(
					TdAcctTrailCloseRequestDto.Body.ExecuteFinacleScriptRequest.ExecuteFinacleScriptCustomData value) {
				this.executeFinacleScriptCustomData = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "tdAccount", "rePayAccount", "clrValueDate", "clsrReason" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class ExecuteFinacleScriptCustomData {

				protected String tdAccount;
				protected String rePayAccount;
				@XmlElement(required = true)
				protected String clrValueDate;
				protected String clsrReason;

				public String getTdAccount() {
					return tdAccount;
				}

				public void setTdAccount(String value) {
					this.tdAccount = value;
				}

				public String getRePayAccount() {
					return rePayAccount;
				}

				public void setRePayAccount(String value) {
					this.rePayAccount = value;
				}

				public String getClrValueDate() {
					return clrValueDate;
				}

				public void setClrValueDate(String value) {
					this.clrValueDate = value;
				}

				public String getClsrReason() {
					return clsrReason;
				}

				public void setClsrReason(String value) {
					this.clsrReason = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "requestId" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class ExecuteFinacleScriptInputVO {

				@XmlElement(required = true)
				protected String requestId;

				public String getRequestId() {
					return requestId;
				}

				public void setRequestId(String value) {
					this.requestId = value;
				}

			}

		}

	}

}
