package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TdAcctInquiryRequestDto {

	@XmlElement(name = "Header")
	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body", required = true)
	protected TdAcctInquiryRequestDto.Body body;

	public TdAcctInquiryRequestDto.Body getBody() {
		return body;
	}

	public void setBody(TdAcctInquiryRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "tdAcctInqRequest" })
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Body {

		@XmlElement(name = "TDAcctInqRequest", required = true)
		protected TdAcctInquiryRequestDto.Body.TDAcctInqRequest tdAcctInqRequest;

		public TdAcctInquiryRequestDto.Body.TDAcctInqRequest getTDAcctInqRequest() {
			return tdAcctInqRequest;
		}

		public void setTDAcctInqRequest(TdAcctInquiryRequestDto.Body.TDAcctInqRequest value) {
			this.tdAcctInqRequest = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "tdAcctInqRq" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class TDAcctInqRequest {

			@XmlElement(name = "TDAcctInqRq", required = true)
			protected TdAcctInquiryRequestDto.Body.TDAcctInqRequest.TDAcctInqRq tdAcctInqRq;

			public TdAcctInquiryRequestDto.Body.TDAcctInqRequest.TDAcctInqRq getTDAcctInqRq() {
				return tdAcctInqRq;
			}

			public void setTDAcctInqRq(TdAcctInquiryRequestDto.Body.TDAcctInqRequest.TDAcctInqRq value) {
				this.tdAcctInqRq = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "tdAcctId" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class TDAcctInqRq {

				@XmlElement(name = "TDAcctId", required = true)
				protected TdAcctInquiryRequestDto.Body.TDAcctInqRequest.TDAcctInqRq.TDAcctId tdAcctId;

				public TdAcctInquiryRequestDto.Body.TDAcctInqRequest.TDAcctInqRq.TDAcctId getTDAcctId() {
					return tdAcctId;
				}

				public void setTDAcctId(TdAcctInquiryRequestDto.Body.TDAcctInqRequest.TDAcctInqRq.TDAcctId value) {
					this.tdAcctId = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctId" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class TDAcctId {

					@XmlElement(name = "AcctId")
					protected String acctId;

					public String getAcctId() {
						return acctId;
					}

					public void setAcctId(String value) {
						this.acctId = value;
					}

				}

			}

		}

	}
}
