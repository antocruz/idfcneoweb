package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PhoneEmailDtls {
	
	protected String email;
	
	protected String phoneEmailType;
	
	protected String phoneNum;
	
	protected String phoneNumCountryCode;
	
	protected String phoneNumLocalCode;
	
	protected String phoneOrEmail;
	
	protected String prefFlag;
	
	@XmlElement(name = "PhoneEmailType")
	public String getPhoneEmailType() {
		return phoneEmailType;
	}

	public void setPhoneEmailType(String phoneEmailType) {
		this.phoneEmailType = phoneEmailType;
	}

	@XmlElement(name = "PhoneNum")
	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	@XmlElement(name = "PhoneNumCountryCode")
	public String getPhoneNumCountryCode() {
		return phoneNumCountryCode;
	}

	public void setPhoneNumCountryCode(String phoneNumCountryCode) {
		this.phoneNumCountryCode = phoneNumCountryCode;
	}

	@XmlElement(name = "PhoneNumLocalCode")
	public String getPhoneNumLocalCode() {
		return phoneNumLocalCode;
	}

	public void setPhoneNumLocalCode(String phoneNumLocalCode) {
		this.phoneNumLocalCode = phoneNumLocalCode;
	}

	@XmlElement(name = "PhoneOrEmail")
	public String getPhoneOrEmail() {
		return phoneOrEmail;
	}

	public void setPhoneOrEmail(String phoneOrEmail) {
		this.phoneOrEmail = phoneOrEmail;
	}

	@XmlElement(name = "PrefFlag")
	public String getPrefFlag() {
		return prefFlag;
	}

	public void setPrefFlag(String prefFlag) {
		this.prefFlag = prefFlag;
	}

	@XmlElement(name = "Email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
