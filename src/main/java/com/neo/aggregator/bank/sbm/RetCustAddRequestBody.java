package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.Builder;

@Builder
public class RetCustAddRequestBody {
	
	protected RetCustAddRequest retCustAddRequest;

	@XmlElement(name = "RetCustAddRequest")
	public RetCustAddRequest getRetCustAddRequest() {
		return retCustAddRequest;
	}

	public void setRetCustAddRequest(RetCustAddRequest retCustAddRequest) {
		this.retCustAddRequest = retCustAddRequest;
	} 
	
	

}
