package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.Builder;

@Builder
public class RequestHeader {
	
	protected MessageKey messageKey;
	
	protected RequestMessageInfo requestMsgInfo;
	
	protected Security security;
	
	@XmlElement(name = "MessageKey")
	public MessageKey getMessageKey() {
		return messageKey;
	}

	public void setMessageKey(MessageKey messageKey) {
		this.messageKey = messageKey;
	}

	@XmlElement(name = "RequestMessageInfo")
	public RequestMessageInfo getRequestMsgInfo() {
		return requestMsgInfo;
	}

	public void setRequestMsgInfo(RequestMessageInfo requestMsgInfo) {
		this.requestMsgInfo = requestMsgInfo;
	}

	@XmlElement(name = "Security")
	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}
	
}
