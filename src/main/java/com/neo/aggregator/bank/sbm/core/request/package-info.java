@XmlSchema(namespace = "http://www.finacle.com/fixml", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED, xmlns = {
		@XmlNs(prefix = "xsi", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance"),
		@XmlNs(prefix = "", namespaceURI = "http://www.finacle.com/fixml") })
package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;
