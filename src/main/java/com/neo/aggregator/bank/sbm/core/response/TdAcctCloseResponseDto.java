package com.neo.aggregator.bank.sbm.core.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "body"
})
@XmlRootElement(name = "FIXML")
public class TdAcctCloseResponseDto {


    @XmlElement(name = "Header", required = true)
    protected TdAcctCloseResponseDto.Header header;
    @XmlElement(name = "Body", required = true)
    protected TdAcctCloseResponseDto.Body body;

    
    public TdAcctCloseResponseDto.Header getHeader() {
        return header;
    }

   
    public void setHeader(TdAcctCloseResponseDto.Header value) {
        this.header = value;
    }

   
    public TdAcctCloseResponseDto.Body getBody() {
        return body;
    }

    
    public void setBody(TdAcctCloseResponseDto.Body value) {
        this.body = value;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "depAcctCloseResponse","error"
    })
    public static class Body {

        @XmlElement(name = "DepAcctCloseResponse", required = true)
        protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse depAcctCloseResponse;

       
        public TdAcctCloseResponseDto.Body.DepAcctCloseResponse getDepAcctCloseResponse() {
            return depAcctCloseResponse;
        }

        
        public void setDepAcctCloseResponse(TdAcctCloseResponseDto.Body.DepAcctCloseResponse value) {
            this.depAcctCloseResponse = value;
        }

        
        
    	@XmlElement(name = "Error")
		protected Error error;

		public Error getError() {
			return error;
		}

		public void setError(Error value) {
			this.error = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)

		@XmlType(name = "")
		public static class Error {

			@XmlElement(name = "FIBusinessException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiBusinessException;

			@XmlElement(name = "FISystemException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException;

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFiSystemException() {
				return fiSystemException;
			}

			public void setFiSystemException(RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException) {
				this.fiSystemException = fiSystemException;
			}

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFIBusinessException() {
				return fiBusinessException;
			}

			public void setFIBusinessException(RetCustAddResponseDto.Body.Error.FIBusinessException value) {
				this.fiBusinessException = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)

			@XmlType(name = "", propOrder = { "errorDetail" })
			public static class FIBusinessException {

				@XmlElement(name = "ErrorDetail")
				protected RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail errorDetail;

				public RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail getErrorDetail() {
					return errorDetail;
				}

				public void setErrorDetail(RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail value) {
					this.errorDetail = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)

				@XmlType(name = "", propOrder = { "errorCode", "errorDesc", "errorType" })
				public static class ErrorDetail {

					@XmlElement(name = "ErrorCode")
					protected String errorCode;

					@XmlElement(name = "ErrorDesc")
					protected String errorDesc;

					@XmlElement(name = "ErrorType")
					protected String errorType;

					public String getErrorCode() {
						return errorCode;
					}

					public void setErrorCode(String value) {
						this.errorCode = value;
					}

					public String getErrorDesc() {
						return errorDesc;
					}

					public void setErrorDesc(String value) {
						this.errorDesc = value;
					}

					public String getErrorType() {
						return errorType;
					}

					public void setErrorType(String value) {
						this.errorType = value;
					}

				}

			}

		}

       
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "depAcctCloseRs",
            "depAcctCloseCustomData"
        })
        public static class DepAcctCloseResponse {

            @XmlElement(name = "DepAcctCloseRs", required = true)
            protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs depAcctCloseRs;
            @XmlElement(name = "DepAcctClose_CustomData", required = true)
            protected String depAcctCloseCustomData;

            public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs getDepAcctCloseRs() {
                return depAcctCloseRs;
            }

           
            public void setDepAcctCloseRs(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs value) {
                this.depAcctCloseRs = value;
            }

           
            public String getDepAcctCloseCustomData() {
                return depAcctCloseCustomData;
            }

            
            public void setDepAcctCloseCustomData(String value) {
                this.depAcctCloseCustomData = value;
            }


           
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "depAcctId",
                "custId",
                "repayAcctId",
                "closeValueDt",
                "withdrawnAmt",
                "depositAmt",
                "balAmt",
                "liftedLienAmt",
                "closeTrnId",
                "profitPercent",
                "profitAmt",
                "payoutAmt",
                "trnDetailsRec",
                "currDepAmt"
            })
            public static class DepAcctCloseRs {

                @XmlElement(name = "DepAcctId", required = true)
                protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.DepAcctId depAcctId;
                @XmlElement(name = "CustId", required = true)
                protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.CustId custId;
                @XmlElement(name = "RepayAcctId", required = true)
                protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.RepayAcctId repayAcctId;
                @XmlElement(name = "CloseValueDt", required = true)
                @XmlSchemaType(name = "dateTime")
                protected String closeValueDt;
                @XmlElement(name = "WithdrawnAmt", required = true)
                protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.WithdrawnAmt withdrawnAmt;
                @XmlElement(name = "DepositAmt", required = true)
                protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.DepositAmt depositAmt;
                @XmlElement(name = "BalAmt", required = true)
                protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.BalAmt balAmt;
                @XmlElement(name = "LiftedLienAmt", required = true)
                protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.LiftedLienAmt liftedLienAmt;
                @XmlElement(name = "CloseTrnId", required = true)
                protected String closeTrnId;
                @XmlElement(name = "ProfitPercent", required = true)
                protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.ProfitPercent profitPercent;
                @XmlElement(name = "ProfitAmt", required = true)
                protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.ProfitAmt profitAmt;
                @XmlElement(name = "PayoutAmt", required = true)
                protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.PayoutAmt payoutAmt;
                @XmlElement(name = "TrnDetailsRec")
                protected List<TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.TrnDetailsRec> trnDetailsRec;
                @XmlElement(name = "CurrDepAmt", required = true)
                protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.CurrDepAmt currDepAmt;

               
                public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.DepAcctId getDepAcctId() {
                    return depAcctId;
                }

               
                public void setDepAcctId(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.DepAcctId value) {
                    this.depAcctId = value;
                }

                
                public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.CustId getCustId() {
                    return custId;
                }

               
                public void setCustId(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.CustId value) {
                    this.custId = value;
                }

                
                public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.RepayAcctId getRepayAcctId() {
                    return repayAcctId;
                }

                
                public void setRepayAcctId(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.RepayAcctId value) {
                    this.repayAcctId = value;
                }

                
                public String getCloseValueDt() {
                    return closeValueDt;
                }

                
                public void setCloseValueDt(String value) {
                    this.closeValueDt = value;
                }

                
                public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.WithdrawnAmt getWithdrawnAmt() {
                    return withdrawnAmt;
                }

                
                public void setWithdrawnAmt(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.WithdrawnAmt value) {
                    this.withdrawnAmt = value;
                }

               
                public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.DepositAmt getDepositAmt() {
                    return depositAmt;
                }

                
                public void setDepositAmt(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.DepositAmt value) {
                    this.depositAmt = value;
                }

                
                public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.BalAmt getBalAmt() {
                    return balAmt;
                }

                
                public void setBalAmt(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.BalAmt value) {
                    this.balAmt = value;
                }

               
                public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.LiftedLienAmt getLiftedLienAmt() {
                    return liftedLienAmt;
                }

               
                public void setLiftedLienAmt(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.LiftedLienAmt value) {
                    this.liftedLienAmt = value;
                }

                
                public String getCloseTrnId() {
                    return closeTrnId;
                }

                
                public void setCloseTrnId(String value) {
                    this.closeTrnId = value;
                }

               
                public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.ProfitPercent getProfitPercent() {
                    return profitPercent;
                }

                
                public void setProfitPercent(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.ProfitPercent value) {
                    this.profitPercent = value;
                }

                
                public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.ProfitAmt getProfitAmt() {
                    return profitAmt;
                }

                
                public void setProfitAmt(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.ProfitAmt value) {
                    this.profitAmt = value;
                }

                
                public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.PayoutAmt getPayoutAmt() {
                    return payoutAmt;
                }

                
                public void setPayoutAmt(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.PayoutAmt value) {
                    this.payoutAmt = value;
                }

               
                public List<TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.TrnDetailsRec> getTrnDetailsRec() {
                    if (trnDetailsRec == null) {
                        trnDetailsRec = new ArrayList<TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.TrnDetailsRec>();
                    }
                    return this.trnDetailsRec;
                }

               
                public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.CurrDepAmt getCurrDepAmt() {
                    return currDepAmt;
                }

               
                public void setCurrDepAmt(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.CurrDepAmt value) {
                    this.currDepAmt = value;
                }


                
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "amountValue",
                    "currencyCode"
                })
                public static class BalAmt {

                    protected String amountValue;
                    @XmlElement(required = true)
                    protected String currencyCode;

                    
                    public String getAmountValue() {
                        return amountValue;
                    }

                    public void setAmountValue(String value) {
                        this.amountValue = value;
                    }

                   
                    public String getCurrencyCode() {
                        return currencyCode;
                    }

                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                }


              
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "amountValue",
                    "currencyCode"
                })
                public static class CurrDepAmt {

                    protected String amountValue;
                    @XmlElement(required = true)
                    protected String currencyCode;

                    
                    public String getAmountValue() {
                        return amountValue;
                    }

                    
                    public void setAmountValue(String value) {
                        this.amountValue = value;
                    }

                    public String getCurrencyCode() {
                        return currencyCode;
                    }

                    
                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                }


               
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "custId",
                    "personName"
                })
                public static class CustId {

                    @XmlElement(name = "CustId", required = true)
                    protected String custId;
                    @XmlElement(name = "PersonName", required = true)
                    protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.CustId.PersonName personName;

                    
                    public String getCustId() {
                        return custId;
                    }

                   
                    public void setCustId(String value) {
                        this.custId = value;
                    }

                    
                    public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.CustId.PersonName getPersonName() {
                        return personName;
                    }

                   
                    public void setPersonName(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.CustId.PersonName value) {
                        this.personName = value;
                    }


                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "lastName",
                        "firstName",
                        "middleName",
                        "name",
                        "titlePrefix"
                    })
                    public static class PersonName {

                        @XmlElement(name = "LastName", required = true)
                        protected String lastName;
                        @XmlElement(name = "FirstName", required = true)
                        protected String firstName;
                        @XmlElement(name = "MiddleName", required = true)
                        protected String middleName;
                        @XmlElement(name = "Name", required = true)
                        protected String name;
                        @XmlElement(name = "TitlePrefix", required = true)
                        protected String titlePrefix;

                       
                        public String getLastName() {
                            return lastName;
                        }

                       
                        public void setLastName(String value) {
                            this.lastName = value;
                        }

                        
                        public String getFirstName() {
                            return firstName;
                        }

                        
                        public void setFirstName(String value) {
                            this.firstName = value;
                        }

                        
                        public String getMiddleName() {
                            return middleName;
                        }

                        
                        public void setMiddleName(String value) {
                            this.middleName = value;
                        }

                        
                        public String getName() {
                            return name;
                        }


                        public void setName(String value) {
                            this.name = value;
                        }

                     
                        public String getTitlePrefix() {
                            return titlePrefix;
                        }

                       
                        public void setTitlePrefix(String value) {
                            this.titlePrefix = value;
                        }

                    }

                }


               
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "acctId",
                    "acctType",
                    "acctCurr",
                    "bankInfo"
                })
                public static class DepAcctId {

                    @XmlElement(name = "AcctId")
                    protected String acctId;
                    @XmlElement(name = "AcctType", required = true)
                    protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.DepAcctId.AcctType acctType;
                    @XmlElement(name = "AcctCurr", required = true)
                    protected String acctCurr;
                    @XmlElement(name = "BankInfo", required = true)
                    protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.DepAcctId.BankInfo bankInfo;

                    
                    public String getAcctId() {
                        return acctId;
                    }

                    public void setAcctId(String value) {
                        this.acctId = value;
                    }

                    
                    public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.DepAcctId.AcctType getAcctType() {
                        return acctType;
                    }

                    
                    public void setAcctType(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.DepAcctId.AcctType value) {
                        this.acctType = value;
                    }

                   
                    public String getAcctCurr() {
                        return acctCurr;
                    }

                  
                    public void setAcctCurr(String value) {
                        this.acctCurr = value;
                    }

                   
                    public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.DepAcctId.BankInfo getBankInfo() {
                        return bankInfo;
                    }

                 
                    public void setBankInfo(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.DepAcctId.BankInfo value) {
                        this.bankInfo = value;
                    }


                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "schmCode",
                        "schmType"
                    })
                    public static class AcctType {

                        @XmlElement(name = "SchmCode", required = true)
                        protected String schmCode;
                        @XmlElement(name = "SchmType", required = true)
                        protected String schmType;

                       
                        public String getSchmCode() {
                            return schmCode;
                        }

                        
                        public void setSchmCode(String value) {
                            this.schmCode = value;
                        }

                       
                        public String getSchmType() {
                            return schmType;
                        }

                        
                        public void setSchmType(String value) {
                            this.schmType = value;
                        }

                    }


                   
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "bankId",
                        "name",
                        "branchId",
                        "branchName",
                        "postAddr"
                    })
                    public static class BankInfo {

                        @XmlElement(name = "BankId", required = true)
                        protected String bankId;
                        @XmlElement(name = "Name", required = true)
                        protected String name;
                        @XmlElement(name = "BranchId", required = true)
                        protected String branchId;
                        @XmlElement(name = "BranchName", required = true)
                        protected String branchName;
                        @XmlElement(name = "PostAddr", required = true)
                        protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.DepAcctId.BankInfo.PostAddr postAddr;

                       
                        public String getBankId() {
                            return bankId;
                        }

                       
                        public void setBankId(String value) {
                            this.bankId = value;
                        }

                        
                        public String getName() {
                            return name;
                        }

                        
                        public void setName(String value) {
                            this.name = value;
                        }

                        
                        public String getBranchId() {
                            return branchId;
                        }

                        
                        public void setBranchId(String value) {
                            this.branchId = value;
                        }

                     
                        public String getBranchName() {
                            return branchName;
                        }

                        
                        public void setBranchName(String value) {
                            this.branchName = value;
                        }

                       
                        public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.DepAcctId.BankInfo.PostAddr getPostAddr() {
                            return postAddr;
                        }

                        public void setPostAddr(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.DepAcctId.BankInfo.PostAddr value) {
                            this.postAddr = value;
                        }


                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "addr1",
                            "addr2",
                            "addr3",
                            "city",
                            "stateProv",
                            "postalCode",
                            "country",
                            "addrType"
                        })
                        public static class PostAddr {

                            @XmlElement(name = "Addr1", required = true)
                            protected String addr1;
                            @XmlElement(name = "Addr2", required = true)
                            protected String addr2;
                            @XmlElement(name = "Addr3", required = true)
                            protected String addr3;
                            @XmlElement(name = "City", required = true)
                            protected String city;
                            @XmlElement(name = "StateProv", required = true)
                            protected String stateProv;
                            @XmlElement(name = "PostalCode", required = true)
                            protected String postalCode;
                            @XmlElement(name = "Country", required = true)
                            protected String country;
                            @XmlElement(name = "AddrType", required = true)
                            protected String addrType;

                           
                            public String getAddr1() {
                                return addr1;
                            }

                            
                            public void setAddr1(String value) {
                                this.addr1 = value;
                            }

                           
                            public String getAddr2() {
                                return addr2;
                            }

                            
                            public void setAddr2(String value) {
                                this.addr2 = value;
                            }

                           
                            public String getAddr3() {
                                return addr3;
                            }

                            
                            public void setAddr3(String value) {
                                this.addr3 = value;
                            }

                           
                            public String getCity() {
                                return city;
                            }

                            
                            public void setCity(String value) {
                                this.city = value;
                            }

                            
                            public String getStateProv() {
                                return stateProv;
                            }

                            
                            public void setStateProv(String value) {
                                this.stateProv = value;
                            }

                            
                            public String getPostalCode() {
                                return postalCode;
                            }

                           
                            public void setPostalCode(String value) {
                                this.postalCode = value;
                            }

                            
                            public String getCountry() {
                                return country;
                            }

                            
                            public void setCountry(String value) {
                                this.country = value;
                            }

                           
                            public String getAddrType() {
                                return addrType;
                            }

                          
                            public void setAddrType(String value) {
                                this.addrType = value;
                            }

                        }

                    }

                }


               
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "amountValue",
                    "currencyCode"
                })
                public static class DepositAmt {

                    protected String amountValue;
                    @XmlElement(required = true)
                    protected String currencyCode;

                    public String getAmountValue() {
                        return amountValue;
                    }

                    
                    public void setAmountValue(String value) {
                        this.amountValue = value;
                    }

                    
                    public String getCurrencyCode() {
                        return currencyCode;
                    }

                    
                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                }


                
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "amountValue",
                    "currencyCode"
                })
                public static class LiftedLienAmt {

                    protected String amountValue;
                    @XmlElement(required = true)
                    protected String currencyCode;

                    
                    public String getAmountValue() {
                        return amountValue;
                    }

                    
                    public void setAmountValue(String value) {
                        this.amountValue = value;
                    }

                    
                    public String getCurrencyCode() {
                        return currencyCode;
                    }

                    
                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                }


                
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "amountValue",
                    "currencyCode"
                })
                public static class PayoutAmt {

                    protected String amountValue;
                    @XmlElement(required = true)
                    protected String currencyCode;

                    
                    public String getAmountValue() {
                        return amountValue;
                    }

                    
                    public void setAmountValue(String value) {
                        this.amountValue = value;
                    }

                    public String getCurrencyCode() {
                        return currencyCode;
                    }

                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                }


              
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "amountValue",
                    "currencyCode"
                })
                public static class ProfitAmt {

                    protected String amountValue;
                    @XmlElement(required = true)
                    protected String currencyCode;

                    
                    public String getAmountValue() {
                        return amountValue;
                    }

                   
                    public void setAmountValue(String value) {
                        this.amountValue = value;
                    }

                   
                    public String getCurrencyCode() {
                        return currencyCode;
                    }

                  
                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                }


                
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "value"
                })
                public static class ProfitPercent {

                    protected String value;

                   
                    public String getValue() {
                        return value;
                    }

                   
                    public void setValue(String value) {
                        this.value = value;
                    }

                }


             
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "acctId",
                    "acctType",
                    "acctCurr",
                    "bankInfo"
                })
                public static class RepayAcctId {

                    @XmlElement(name = "AcctId")
                    protected String acctId;
                    @XmlElement(name = "AcctType", required = true)
                    protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.RepayAcctId.AcctType acctType;
                    @XmlElement(name = "AcctCurr", required = true)
                    protected String acctCurr;
                    @XmlElement(name = "BankInfo", required = true)
                    protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.RepayAcctId.BankInfo bankInfo;

                   
                    public String getAcctId() {
                        return acctId;
                    }

                    
                    public void setAcctId(String value) {
                        this.acctId = value;
                    }

                    
                    public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.RepayAcctId.AcctType getAcctType() {
                        return acctType;
                    }

                    
                    public void setAcctType(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.RepayAcctId.AcctType value) {
                        this.acctType = value;
                    }

                    
                    public String getAcctCurr() {
                        return acctCurr;
                    }

                    
                    public void setAcctCurr(String value) {
                        this.acctCurr = value;
                    }

                    
                    public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.RepayAcctId.BankInfo getBankInfo() {
                        return bankInfo;
                    }

                    
                    public void setBankInfo(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.RepayAcctId.BankInfo value) {
                        this.bankInfo = value;
                    }


                 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "schmCode",
                        "schmType"
                    })
                    public static class AcctType {

                        @XmlElement(name = "SchmCode", required = true)
                        protected String schmCode;
                        @XmlElement(name = "SchmType", required = true)
                        protected String schmType;

                       
                        public String getSchmCode() {
                            return schmCode;
                        }

                       
                        public void setSchmCode(String value) {
                            this.schmCode = value;
                        }

                        
                        public String getSchmType() {
                            return schmType;
                        }

                       
                        public void setSchmType(String value) {
                            this.schmType = value;
                        }

                    }


                   
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "bankId",
                        "name",
                        "branchId",
                        "branchName",
                        "postAddr"
                    })
                    public static class BankInfo {

                        @XmlElement(name = "BankId", required = true)
                        protected String bankId;
                        @XmlElement(name = "Name", required = true)
                        protected String name;
                        @XmlElement(name = "BranchId", required = true)
                        protected String branchId;
                        @XmlElement(name = "BranchName", required = true)
                        protected String branchName;
                        @XmlElement(name = "PostAddr", required = true)
                        protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.RepayAcctId.BankInfo.PostAddr postAddr;

                       
                        public String getBankId() {
                            return bankId;
                        }

                        
                        public void setBankId(String value) {
                            this.bankId = value;
                        }

                       
                        public String getName() {
                            return name;
                        }

                        
                        public void setName(String value) {
                            this.name = value;
                        }

                       
                        public String getBranchId() {
                            return branchId;
                        }

                        
                        public void setBranchId(String value) {
                            this.branchId = value;
                        }

                        
                        public String getBranchName() {
                            return branchName;
                        }

                        
                        public void setBranchName(String value) {
                            this.branchName = value;
                        }

                        
                        public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.RepayAcctId.BankInfo.PostAddr getPostAddr() {
                            return postAddr;
                        }

                        
                        public void setPostAddr(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.RepayAcctId.BankInfo.PostAddr value) {
                            this.postAddr = value;
                        }


                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "addr1",
                            "addr2",
                            "addr3",
                            "city",
                            "stateProv",
                            "postalCode",
                            "country",
                            "addrType"
                        })
                        public static class PostAddr {

                            @XmlElement(name = "Addr1", required = true)
                            protected String addr1;
                            @XmlElement(name = "Addr2", required = true)
                            protected String addr2;
                            @XmlElement(name = "Addr3", required = true)
                            protected String addr3;
                            @XmlElement(name = "City", required = true)
                            protected String city;
                            @XmlElement(name = "StateProv", required = true)
                            protected String stateProv;
                            @XmlElement(name = "PostalCode", required = true)
                            protected String postalCode;
                            @XmlElement(name = "Country", required = true)
                            protected String country;
                            @XmlElement(name = "AddrType", required = true)
                            protected String addrType;

                            
                            public String getAddr1() {
                                return addr1;
                            }

                            
                            public void setAddr1(String value) {
                                this.addr1 = value;
                            }

                           
                            public String getAddr2() {
                                return addr2;
                            }

                            
                            public void setAddr2(String value) {
                                this.addr2 = value;
                            }

                            
                            public String getAddr3() {
                                return addr3;
                            }

                           
                            public void setAddr3(String value) {
                                this.addr3 = value;
                            }

                           
                            public String getCity() {
                                return city;
                            }

                            
                            public void setCity(String value) {
                                this.city = value;
                            }

                            
                            public String getStateProv() {
                                return stateProv;
                            }

                            
                            public void setStateProv(String value) {
                                this.stateProv = value;
                            }

                            
                            public String getPostalCode() {
                                return postalCode;
                            }

                            public void setPostalCode(String value) {
                                this.postalCode = value;
                            }

                            
                            public String getCountry() {
                                return country;
                            }

                           
                            public void setCountry(String value) {
                                this.country = value;
                            }

                            
                            public String getAddrType() {
                                return addrType;
                            }

                           
                            public void setAddrType(String value) {
                                this.addrType = value;
                            }

                        }

                    }

                }


                
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "trnId",
                    "trnParticulars",
                    "trnAcctId",
                    "curCode",
                    "trnAmt",
                    "creditDebitFlg"
                })
                public static class TrnDetailsRec {

                    @XmlElement(name = "TrnId", required = true)
                    protected String trnId;
                    @XmlElement(name = "TrnParticulars", required = true)
                    protected String trnParticulars;
                    @XmlElement(name = "TrnAcctId")
                    protected String trnAcctId;
                    @XmlElement(name = "CurCode", required = true)
                    protected String curCode;
                    @XmlElement(name = "TrnAmt", required = true)
                    protected TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.TrnDetailsRec.TrnAmt trnAmt;
                    @XmlElement(name = "CreditDebitFlg", required = true)
                    protected String creditDebitFlg;

                    
                    public String getTrnId() {
                        return trnId;
                    }

                    
                    public void setTrnId(String value) {
                        this.trnId = value;
                    }

                   
                    public String getTrnParticulars() {
                        return trnParticulars;
                    }

                    
                    public void setTrnParticulars(String value) {
                        this.trnParticulars = value;
                    }

                 
                    public String getTrnAcctId() {
                        return trnAcctId;
                    }

                   
                    public void setTrnAcctId(String value) {
                        this.trnAcctId = value;
                    }

                   
                    public String getCurCode() {
                        return curCode;
                    }

                    
                    public void setCurCode(String value) {
                        this.curCode = value;
                    }

                  
                    public TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.TrnDetailsRec.TrnAmt getTrnAmt() {
                        return trnAmt;
                    }

                    
                    public void setTrnAmt(TdAcctCloseResponseDto.Body.DepAcctCloseResponse.DepAcctCloseRs.TrnDetailsRec.TrnAmt value) {
                        this.trnAmt = value;
                    }

                 
                    public String getCreditDebitFlg() {
                        return creditDebitFlg;
                    }

                  
                    public void setCreditDebitFlg(String value) {
                        this.creditDebitFlg = value;
                    }


                   
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "amountValue",
                        "currencyCode"
                    })
                    public static class TrnAmt {

                        protected String amountValue;
                        @XmlElement(required = true)
                        protected String currencyCode;

                     
                        public String getAmountValue() {
                            return amountValue;
                        }

                        
                        public void setAmountValue(String value) {
                            this.amountValue = value;
                        }

                        public String getCurrencyCode() {
                            return currencyCode;
                        }

                       
                        public void setCurrencyCode(String value) {
                            this.currencyCode = value;
                        }

                    }

                }


       
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "amountValue",
                    "currencyCode"
                })
                public static class WithdrawnAmt {

                    protected String amountValue;
                    @XmlElement(required = true)
                    protected String currencyCode;

                  
                    public String getAmountValue() {
                        return amountValue;
                    }

                   
                    public void setAmountValue(String value) {
                        this.amountValue = value;
                    }

                
                    public String getCurrencyCode() {
                        return currencyCode;
                    }

                 
                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                }

            }

        }

    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "responseHeader"
    })
    public static class Header {

        @XmlElement(name = "ResponseHeader", required = true)
        protected TdAcctCloseResponseDto.Header.ResponseHeader responseHeader;

      
        public TdAcctCloseResponseDto.Header.ResponseHeader getResponseHeader() {
            return responseHeader;
        }

       
        public void setResponseHeader(TdAcctCloseResponseDto.Header.ResponseHeader value) {
            this.responseHeader = value;
        }


      
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "requestMessageKey",
            "responseMessageInfo",
            "ubusTransaction",
            "hostTransaction",
            "hostParentTransaction",
            "customInfo"
        })
        public static class ResponseHeader {

            @XmlElement(name = "RequestMessageKey", required = true)
            protected TdAcctCloseResponseDto.Header.ResponseHeader.RequestMessageKey requestMessageKey;
            @XmlElement(name = "ResponseMessageInfo", required = true)
            protected TdAcctCloseResponseDto.Header.ResponseHeader.ResponseMessageInfo responseMessageInfo;
            @XmlElement(name = "UBUSTransaction", required = true)
            protected TdAcctCloseResponseDto.Header.ResponseHeader.UBUSTransaction ubusTransaction;
            @XmlElement(name = "HostTransaction", required = true)
            protected TdAcctCloseResponseDto.Header.ResponseHeader.HostTransaction hostTransaction;
            @XmlElement(name = "HostParentTransaction", required = true)
            protected TdAcctCloseResponseDto.Header.ResponseHeader.HostParentTransaction hostParentTransaction;
            @XmlElement(name = "CustomInfo", required = true)
            protected String customInfo;

            
            public TdAcctCloseResponseDto.Header.ResponseHeader.RequestMessageKey getRequestMessageKey() {
                return requestMessageKey;
            }

            
            public void setRequestMessageKey(TdAcctCloseResponseDto.Header.ResponseHeader.RequestMessageKey value) {
                this.requestMessageKey = value;
            }

            
            public TdAcctCloseResponseDto.Header.ResponseHeader.ResponseMessageInfo getResponseMessageInfo() {
                return responseMessageInfo;
            }

            
            public void setResponseMessageInfo(TdAcctCloseResponseDto.Header.ResponseHeader.ResponseMessageInfo value) {
                this.responseMessageInfo = value;
            }

            
            public TdAcctCloseResponseDto.Header.ResponseHeader.UBUSTransaction getUBUSTransaction() {
                return ubusTransaction;
            }

            
            public void setUBUSTransaction(TdAcctCloseResponseDto.Header.ResponseHeader.UBUSTransaction value) {
                this.ubusTransaction = value;
            }

            
            public TdAcctCloseResponseDto.Header.ResponseHeader.HostTransaction getHostTransaction() {
                return hostTransaction;
            }

            
            public void setHostTransaction(TdAcctCloseResponseDto.Header.ResponseHeader.HostTransaction value) {
                this.hostTransaction = value;
            }

           
            public TdAcctCloseResponseDto.Header.ResponseHeader.HostParentTransaction getHostParentTransaction() {
                return hostParentTransaction;
            }

          
            public void setHostParentTransaction(TdAcctCloseResponseDto.Header.ResponseHeader.HostParentTransaction value) {
                this.hostParentTransaction = value;
            }

            
            public String getCustomInfo() {
                return customInfo;
            }

            
            public void setCustomInfo(String value) {
                this.customInfo = value;
            }


           
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "id",
                "status"
            })
            public static class HostParentTransaction {

                @XmlElement(name = "Id", required = true)
                protected String id;
                @XmlElement(name = "Status", required = true)
                protected String status;

                
                public String getId() {
                    return id;
                }

                
                public void setId(String value) {
                    this.id = value;
                }

                
                public String getStatus() {
                    return status;
                }

               
                public void setStatus(String value) {
                    this.status = value;
                }

            }


           
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "id",
                "status"
            })
            public static class HostTransaction {

                @XmlElement(name = "Id", required = true)
                protected String id;
                @XmlElement(name = "Status", required = true)
                protected String status;

               
                public String getId() {
                    return id;
                }

               
                public void setId(String value) {
                    this.id = value;
                }

               
                public String getStatus() {
                    return status;
                }

             
                public void setStatus(String value) {
                    this.status = value;
                }

            }


            
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "requestUUID",
                "serviceRequestId",
                "serviceRequestVersion",
                "channelId"
            })
            public static class RequestMessageKey {

                @XmlElement(name = "RequestUUID", required = true)
                protected String requestUUID;
                @XmlElement(name = "ServiceRequestId", required = true)
                protected String serviceRequestId;
                @XmlElement(name = "ServiceRequestVersion")
                protected String serviceRequestVersion;
                @XmlElement(name = "ChannelId", required = true)
                protected String channelId;

               
                public String getRequestUUID() {
                    return requestUUID;
                }

               
                public void setRequestUUID(String value) {
                    this.requestUUID = value;
                }

                
                public String getServiceRequestId() {
                    return serviceRequestId;
                }

               
                public void setServiceRequestId(String value) {
                    this.serviceRequestId = value;
                }

               
                public String getServiceRequestVersion() {
                    return serviceRequestVersion;
                }

                
                public void setServiceRequestVersion(String value) {
                    this.serviceRequestVersion = value;
                }

             
                public String getChannelId() {
                    return channelId;
                }

                
                public void setChannelId(String value) {
                    this.channelId = value;
                }

            }


            
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "bankId",
                "timeZone",
                "messageDateTime"
            })
            public static class ResponseMessageInfo {

                @XmlElement(name = "BankId", required = true)
                protected String bankId;
                @XmlElement(name = "TimeZone", required = true)
                protected String timeZone;
                @XmlElement(name = "MessageDateTime", required = true)
                @XmlSchemaType(name = "dateTime")
                protected String messageDateTime;

              
                public String getBankId() {
                    return bankId;
                }

                
                public void setBankId(String value) {
                    this.bankId = value;
                }

              
                public String getTimeZone() {
                    return timeZone;
                }

               
                public void setTimeZone(String value) {
                    this.timeZone = value;
                }

              
                public String getMessageDateTime() {
                    return messageDateTime;
                }

             
                public void setMessageDateTime(String value) {
                    this.messageDateTime = value;
                }

            }


            
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "id",
                "status"
            })
            public static class UBUSTransaction {

                @XmlElement(name = "Id", required = true)
                protected String id;
                @XmlElement(name = "Status", required = true)
                protected String status;

              
                public String getId() {
                    return id;
                }

             
                public void setId(String value) {
                    this.id = value;
                }

           
                public String getStatus() {
                    return status;
                }

              
                public void setStatus(String value) {
                    this.status = value;
                }

            }

        }

    }

}

