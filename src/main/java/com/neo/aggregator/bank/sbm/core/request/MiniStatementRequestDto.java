package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MiniStatementRequestDto {

	@XmlElement(name = "Header")
	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body", required = true)
	protected MiniStatementRequestDto.Body body;

	public MiniStatementRequestDto.Body getBody() {
		return body;
	}

	public void setBody(MiniStatementRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "getMiniAccountStatementRequest" })
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Body {

		@XmlElement(required = true)
		protected MiniStatementRequestDto.Body.GetMiniAccountStatementRequest getMiniAccountStatementRequest;

		public MiniStatementRequestDto.Body.GetMiniAccountStatementRequest getGetMiniAccountStatementRequest() {
			return getMiniAccountStatementRequest;
		}

		public void setGetMiniAccountStatementRequest(
				MiniStatementRequestDto.Body.GetMiniAccountStatementRequest value) {
			this.getMiniAccountStatementRequest = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "accountListElement" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class GetMiniAccountStatementRequest {

			@XmlElement(name = "AccountListElement", required = true)
			protected MiniStatementRequestDto.Body.GetMiniAccountStatementRequest.AccountListElement accountListElement;

			public MiniStatementRequestDto.Body.GetMiniAccountStatementRequest.AccountListElement getAccountListElement() {
				return accountListElement;
			}

			public void setAccountListElement(
					MiniStatementRequestDto.Body.GetMiniAccountStatementRequest.AccountListElement value) {
				this.accountListElement = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "acid", "branchId" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class AccountListElement {

				@XmlElement(required = true)
				protected String acid;
				@XmlElement(required = true)
				protected String branchId;

				public String getAcid() {
					return acid;
				}

				public void setAcid(String value) {
					this.acid = value;
				}

				public String getBranchId() {
					return branchId;
				}

				public void setBranchId(String value) {
					this.branchId = value;
				}

			}

		}

	}

}
