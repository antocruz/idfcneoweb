package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BalinqRequestDto {

	@XmlElement(name = "Header")
	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body", required = true)
	protected BalinqRequestDto.Body body;

	public BalinqRequestDto.Body getBody() {
		return body;
	}

	public void setBody(BalinqRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "balInqRequest" })
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Body {

		@XmlElement(name = "BalInqRequest", required = true)
		protected BalinqRequestDto.Body.BalInqRequest balInqRequest;

		public BalinqRequestDto.Body.BalInqRequest getBalInqRequest() {
			return balInqRequest;
		}

		public void setBalInqRequest(BalinqRequestDto.Body.BalInqRequest value) {
			this.balInqRequest = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "balInqRq" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class BalInqRequest {

			@XmlElement(name = "BalInqRq", required = true)
			protected BalinqRequestDto.Body.BalInqRequest.BalInqRq balInqRq;

			public BalinqRequestDto.Body.BalInqRequest.BalInqRq getBalInqRq() {
				return balInqRq;
			}

			public void setBalInqRq(BalinqRequestDto.Body.BalInqRequest.BalInqRq value) {
				this.balInqRq = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "acctId" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class BalInqRq {

				@XmlElement(name = "AcctId", required = true)
				protected BalinqRequestDto.Body.BalInqRequest.BalInqRq.AcctId acctId;

				public BalinqRequestDto.Body.BalInqRequest.BalInqRq.AcctId getAcctId() {
					return acctId;
				}

				public void setAcctId(BalinqRequestDto.Body.BalInqRequest.BalInqRq.AcctId value) {
					this.acctId = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctId" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class AcctId {

					@XmlElement(name = "AcctId")
					protected String acctId;

					public String getAcctId() {
						return acctId;
					}

					public void setAcctId(String value) {
						this.acctId = value;
					}

				}

			}

		}

	}

}
