package com.neo.aggregator.bank.sbm;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import lombok.Builder;

@Builder
public class RelatedDtls {

	protected DemographicData demographicData;
	protected List<EntityDoctData> entityDoctData;
	protected PsychographicData pyschographicData;

	@XmlElement(name = "DemographicData")
	public DemographicData getDemographicData() {
		return demographicData;
	}

	public void setDemographicData(DemographicData demographicData) {
		this.demographicData = demographicData;
	}

	@XmlElement(name = "EntityDoctData")
	public List<EntityDoctData> getEntityDoctData() {
		return entityDoctData;
	}

	public void setEntityDoctData(List<EntityDoctData> entityDoctData) {
		this.entityDoctData = entityDoctData;
	}

	@XmlElement(name = "PsychographicData")
	public PsychographicData getPyschographicData() {
		return pyschographicData;
	}

	public void setPyschographicData(PsychographicData pyschographicData) {
		this.pyschographicData = pyschographicData;
	}

}
