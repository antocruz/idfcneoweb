package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.Builder;

@Builder
public class CustDtls {
	
	protected CustData custData;
	
	@XmlElement(name = "CustData")
	public CustData getCustData() {
		return custData;
	}
	public void setCustData(CustData custData) {
		this.custData = custData;
	}

}
