package com.neo.aggregator.bank.sbm.core.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
public class MiniStatementResponseDto {

	@XmlElement(name = "Header", required = true)
	protected MiniStatementResponseDto.Header header;
	@XmlElement(name = "Body", required = true)
	protected MiniStatementResponseDto.Body body;

	public MiniStatementResponseDto.Header getHeader() {
		return header;
	}

	public void setHeader(MiniStatementResponseDto.Header value) {
		this.header = value;
	}

	public MiniStatementResponseDto.Body getBody() {
		return body;
	}

	public void setBody(MiniStatementResponseDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "getMiniAccountStatementResponse" , "error" })
	public static class Body {

		@XmlElement(required = true)
		protected MiniStatementResponseDto.Body.GetMiniAccountStatementResponse getMiniAccountStatementResponse;

		public MiniStatementResponseDto.Body.GetMiniAccountStatementResponse getGetMiniAccountStatementResponse() {
			return getMiniAccountStatementResponse;
		}

		public void setGetMiniAccountStatementResponse(
				MiniStatementResponseDto.Body.GetMiniAccountStatementResponse value) {
			this.getMiniAccountStatementResponse = value;
		}

		@XmlElement(name = "Error")
		protected Error error;

		public Error getError() {
			return error;
		}

		public void setError(Error value) {
			this.error = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class Error {

			@XmlElement(name = "FIBusinessException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiBusinessException;

			@XmlElement(name = "FISystemException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException;

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFiSystemException() {
				return fiSystemException;
			}

			public void setFiSystemException(RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException) {
				this.fiSystemException = fiSystemException;
			}

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFIBusinessException() {
				return fiBusinessException;
			}

			public void setFIBusinessException(RetCustAddResponseDto.Body.Error.FIBusinessException value) {
				this.fiBusinessException = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)

			@XmlType(name = "", propOrder = { "errorDetail" })
			public static class FIBusinessException {

				@XmlElement(name = "ErrorDetail")
				protected RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail errorDetail;

				public RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail getErrorDetail() {
					return errorDetail;
				}

				public void setErrorDetail(RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail value) {
					this.errorDetail = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)

				@XmlType(name = "", propOrder = { "errorCode", "errorDesc", "errorType" })
				public static class ErrorDetail {

					@XmlElement(name = "ErrorCode")
					protected String errorCode;

					@XmlElement(name = "ErrorDesc")
					protected String errorDesc;

					@XmlElement(name = "ErrorType")
					protected String errorType;

					public String getErrorCode() {
						return errorCode;
					}

					public void setErrorCode(String value) {
						this.errorCode = value;
					}

					public String getErrorDesc() {
						return errorDesc;
					}

					public void setErrorDesc(String value) {
						this.errorDesc = value;
					}

					public String getErrorType() {
						return errorType;
					}

					public void setErrorType(String value) {
						this.errorType = value;
					}

				}

			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "miniStatementSummary", "getMiniAccountStatementCustomData" })
		public static class GetMiniAccountStatementResponse {

			@XmlElement(name = "MiniStatementSummary", required = true)
			protected MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary miniStatementSummary;
			@XmlElement(name = "getMiniAccountStatement_CustomData", required = true)
			protected String getMiniAccountStatementCustomData;

			public MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary getMiniStatementSummary() {
				return miniStatementSummary;
			}

			public void setMiniStatementSummary(
					MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary value) {
				this.miniStatementSummary = value;
			}

			public String getGetMiniAccountStatementCustomData() {
				return getMiniAccountStatementCustomData;
			}

			public void setGetMiniAccountStatementCustomData(String value) {
				this.getMiniAccountStatementCustomData = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "accountSummary", "accountTransactionSummary" })
			public static class MiniStatementSummary {

				@XmlElement(required = true)
				protected MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary accountSummary;
				protected List<MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountTransactionSummary> accountTransactionSummary;

				public MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary getAccountSummary() {
					return accountSummary;
				}

				public void setAccountSummary(
						MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary value) {
					this.accountSummary = value;
				}

				public List<MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountTransactionSummary> getAccountTransactionSummary() {
					if (accountTransactionSummary == null) {
						accountTransactionSummary = new ArrayList<MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountTransactionSummary>();
					}
					return this.accountTransactionSummary;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acid", "availableBalance", "branchId", "currencyCode", "ffdBalance",
						"StringingBalance", "ledgerBalance", "userDefinedBalance" })
				public static class AccountSummary {

					protected String acid;
					@XmlElement(required = true)
					protected MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary.AvailableBalance availableBalance;
					protected String branchId;
					@XmlElement(required = true)
					protected String currencyCode;
					@XmlElement(name = "fFDBalance", required = true)
					protected MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary.FFDBalance ffdBalance;
					@XmlElement(required = true)
					protected MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary.StringingBalance StringingBalance;
					@XmlElement(required = true)
					protected MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary.LedgerBalance ledgerBalance;
					@XmlElement(required = true)
					protected MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary.UserDefinedBalance userDefinedBalance;

					public String getAcid() {
						return acid;
					}

					public void setAcid(String value) {
						this.acid = value;
					}

					public MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary.AvailableBalance getAvailableBalance() {
						return availableBalance;
					}

					public void setAvailableBalance(
							MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary.AvailableBalance value) {
						this.availableBalance = value;
					}

					public String getBranchId() {
						return branchId;
					}

					public void setBranchId(String value) {
						this.branchId = value;
					}

					public String getCurrencyCode() {
						return currencyCode;
					}

					public void setCurrencyCode(String value) {
						this.currencyCode = value;
					}

					public MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary.FFDBalance getFFDBalance() {
						return ffdBalance;
					}

					public void setFFDBalance(
							MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary.FFDBalance value) {
						this.ffdBalance = value;
					}

					public MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary.StringingBalance getStringingBalance() {
						return StringingBalance;
					}

					public void setStringingBalance(
							MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary.StringingBalance value) {
						this.StringingBalance = value;
					}

					public MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary.LedgerBalance getLedgerBalance() {
						return ledgerBalance;
					}

					public void setLedgerBalance(
							MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary.LedgerBalance value) {
						this.ledgerBalance = value;
					}

					public MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary.UserDefinedBalance getUserDefinedBalance() {
						return userDefinedBalance;
					}

					public void setUserDefinedBalance(
							MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountSummary.UserDefinedBalance value) {
						this.userDefinedBalance = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					public static class AvailableBalance {

						protected String amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public String getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(String value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					public static class FFDBalance {

						protected String amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public String getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(String value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					public static class StringingBalance {

						protected String amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public String getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(String value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					public static class LedgerBalance {

						protected String amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public String getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(String value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					public static class UserDefinedBalance {

						protected String amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public String getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(String value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "instrumentId", "txnAmt", "txnDate", "txnDesc", "txnType" })
				public static class AccountTransactionSummary {

					@XmlElement(required = true)
					protected String instrumentId;
					@XmlElement(required = true)
					protected MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountTransactionSummary.TxnAmt txnAmt;
					@XmlElement(required = true)
					@XmlSchemaType(name = "dateTime")
					protected String txnDate;
					@XmlElement(required = true)
					protected String txnDesc;
					@XmlElement(required = true)
					protected String txnType;

					public String getInstrumentId() {
						return instrumentId;
					}

					public void setInstrumentId(String value) {
						this.instrumentId = value;
					}

					public MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountTransactionSummary.TxnAmt getTxnAmt() {
						return txnAmt;
					}

					public void setTxnAmt(
							MiniStatementResponseDto.Body.GetMiniAccountStatementResponse.MiniStatementSummary.AccountTransactionSummary.TxnAmt value) {
						this.txnAmt = value;
					}

					public String getTxnDate() {
						return txnDate;
					}

					public void setTxnDate(String value) {
						this.txnDate = value;
					}

					public String getTxnDesc() {
						return txnDesc;
					}

					public void setTxnDesc(String value) {
						this.txnDesc = value;
					}

					public String getTxnType() {
						return txnType;
					}

					public void setTxnType(String value) {
						this.txnType = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					public static class TxnAmt {

						protected String amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public String getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(String value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

				}

			}

		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "responseHeader" })
	public static class Header {

		@XmlElement(name = "ResponseHeader", required = true)
		protected MiniStatementResponseDto.Header.ResponseHeader responseHeader;

		public MiniStatementResponseDto.Header.ResponseHeader getResponseHeader() {
			return responseHeader;
		}

		public void setResponseHeader(MiniStatementResponseDto.Header.ResponseHeader value) {
			this.responseHeader = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "requestMessageKey", "responseMessageInfo", "ubusTransaction",
				"hostTransaction", "hostParentTransaction", "customInfo" })
		public static class ResponseHeader {

			@XmlElement(name = "RequestMessageKey", required = true)
			protected MiniStatementResponseDto.Header.ResponseHeader.RequestMessageKey requestMessageKey;
			@XmlElement(name = "ResponseMessageInfo", required = true)
			protected MiniStatementResponseDto.Header.ResponseHeader.ResponseMessageInfo responseMessageInfo;
			@XmlElement(name = "UBUSTransaction", required = true)
			protected MiniStatementResponseDto.Header.ResponseHeader.UBUSTransaction ubusTransaction;
			@XmlElement(name = "HostTransaction", required = true)
			protected MiniStatementResponseDto.Header.ResponseHeader.HostTransaction hostTransaction;
			@XmlElement(name = "HostParentTransaction", required = true)
			protected MiniStatementResponseDto.Header.ResponseHeader.HostParentTransaction hostParentTransaction;
			@XmlElement(name = "CustomInfo", required = true)
			protected String customInfo;

			public MiniStatementResponseDto.Header.ResponseHeader.RequestMessageKey getRequestMessageKey() {
				return requestMessageKey;
			}

			public void setRequestMessageKey(MiniStatementResponseDto.Header.ResponseHeader.RequestMessageKey value) {
				this.requestMessageKey = value;
			}

			public MiniStatementResponseDto.Header.ResponseHeader.ResponseMessageInfo getResponseMessageInfo() {
				return responseMessageInfo;
			}

			public void setResponseMessageInfo(
					MiniStatementResponseDto.Header.ResponseHeader.ResponseMessageInfo value) {
				this.responseMessageInfo = value;
			}

			public MiniStatementResponseDto.Header.ResponseHeader.UBUSTransaction getUBUSTransaction() {
				return ubusTransaction;
			}

			public void setUBUSTransaction(MiniStatementResponseDto.Header.ResponseHeader.UBUSTransaction value) {
				this.ubusTransaction = value;
			}

			public MiniStatementResponseDto.Header.ResponseHeader.HostTransaction getHostTransaction() {
				return hostTransaction;
			}

			public void setHostTransaction(MiniStatementResponseDto.Header.ResponseHeader.HostTransaction value) {
				this.hostTransaction = value;
			}

			public MiniStatementResponseDto.Header.ResponseHeader.HostParentTransaction getHostParentTransaction() {
				return hostParentTransaction;
			}

			public void setHostParentTransaction(
					MiniStatementResponseDto.Header.ResponseHeader.HostParentTransaction value) {
				this.hostParentTransaction = value;
			}

			public String getCustomInfo() {
				return customInfo;
			}

			public void setCustomInfo(String value) {
				this.customInfo = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostParentTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "requestUUID", "serviceRequestId", "serviceRequestVersion", "channelId" })
			public static class RequestMessageKey {

				@XmlElement(name = "RequestUUID", required = true)
				protected String requestUUID;
				@XmlElement(name = "ServiceRequestId", required = true)
				protected String serviceRequestId;
				@XmlElement(name = "ServiceRequestVersion")
				protected String serviceRequestVersion;
				@XmlElement(name = "ChannelId", required = true)
				protected String channelId;

				public String getRequestUUID() {
					return requestUUID;
				}

				public void setRequestUUID(String value) {
					this.requestUUID = value;
				}

				public String getServiceRequestId() {
					return serviceRequestId;
				}

				public void setServiceRequestId(String value) {
					this.serviceRequestId = value;
				}

				public String getServiceRequestVersion() {
					return serviceRequestVersion;
				}

				public void setServiceRequestVersion(String value) {
					this.serviceRequestVersion = value;
				}

				public String getChannelId() {
					return channelId;
				}

				public void setChannelId(String value) {
					this.channelId = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "bankId", "timeZone", "messageDateTime" })
			public static class ResponseMessageInfo {

				@XmlElement(name = "BankId", required = true)
				protected String bankId;
				@XmlElement(name = "TimeZone", required = true)
				protected String timeZone;
				@XmlElement(name = "MessageDateTime", required = true)
				@XmlSchemaType(name = "dateTime")
				protected String messageDateTime;

				public String getBankId() {
					return bankId;
				}

				public void setBankId(String value) {
					this.bankId = value;
				}

				public String getTimeZone() {
					return timeZone;
				}

				public void setTimeZone(String value) {
					this.timeZone = value;
				}

				public String getMessageDateTime() {
					return messageDateTime;
				}

				public void setMessageDateTime(String value) {
					this.messageDateTime = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class UBUSTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

		}

	}

}
