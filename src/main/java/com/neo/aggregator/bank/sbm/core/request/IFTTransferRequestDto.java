package com.neo.aggregator.bank.sbm.core.request;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IFTTransferRequestDto {

	@XmlElement(name = "Header")
	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body", required = true)
	protected IFTTransferRequestDto.Body body;

	public IFTTransferRequestDto.Body getBody() {
		return body;
	}

	public void setBody(IFTTransferRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "xferTrnAddRequest" })
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Body {

		@XmlElement(name = "XferTrnAddRequest", required = true)
		protected IFTTransferRequestDto.Body.XferTrnAddRequest xferTrnAddRequest;

		public IFTTransferRequestDto.Body.XferTrnAddRequest getXferTrnAddRequest() {
			return xferTrnAddRequest;
		}

		public void setXferTrnAddRequest(IFTTransferRequestDto.Body.XferTrnAddRequest value) {
			this.xferTrnAddRequest = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "xferTrnAddRq" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class XferTrnAddRequest {

			@XmlElement(name = "XferTrnAddRq", required = true)
			protected IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq xferTrnAddRq;

			public IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq getXferTrnAddRq() {
				return xferTrnAddRq;
			}

			public void setXferTrnAddRq(IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq value) {
				this.xferTrnAddRq = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "xferTrnHdr", "xferTrnDetail" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class XferTrnAddRq {

				@XmlElement(name = "XferTrnHdr", required = true)
				protected IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnHdr xferTrnHdr;
				@XmlElement(name = "XferTrnDetail", required = true)
				protected IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail xferTrnDetail;

				public IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnHdr getXferTrnHdr() {
					return xferTrnHdr;
				}

				public void setXferTrnHdr(IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnHdr value) {
					this.xferTrnHdr = value;
				}

				public IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail getXferTrnDetail() {
					return xferTrnDetail;
				}

				public void setXferTrnDetail(
						IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail value) {
					this.xferTrnDetail = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "partTrnRec" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class XferTrnDetail {

					@XmlElement(name = "PartTrnRec")
					protected List<IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec> partTrnRec;

					public List<IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec> getPartTrnRec() {
						if (partTrnRec == null) {
							partTrnRec = new ArrayList<IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec>();
						}
						return this.partTrnRec;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "acctId", "creditDebitFlg", "trnAmt", "trnParticulars",
							"partTrnRmks", "valueDt", "userPartTrnCode" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class PartTrnRec {

						@XmlElement(name = "AcctId", required = true)
						protected IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec.AcctId acctId;
						@XmlElement(name = "CreditDebitFlg", required = true)
						protected String creditDebitFlg;
						@XmlElement(name = "TrnAmt", required = true)
						protected IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec.TrnAmt trnAmt;
						@XmlElement(name = "TrnParticulars", required = true)
						protected String trnParticulars;
						@XmlElement(name = "PartTrnRmks", required = true)
						protected String partTrnRmks;
						@XmlElement(name = "ValueDt", required = true)
						@XmlSchemaType(name = "dateTime")
						protected String valueDt;
						@XmlElement(name = "UserPartTrnCode", required = true)
						protected String userPartTrnCode;

						public IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec.AcctId getAcctId() {
							return acctId;
						}

						public void setAcctId(
								IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec.AcctId value) {
							this.acctId = value;
						}

						public String getCreditDebitFlg() {
							return creditDebitFlg;
						}

						public void setCreditDebitFlg(String value) {
							this.creditDebitFlg = value;
						}

						public IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec.TrnAmt getTrnAmt() {
							return trnAmt;
						}

						public void setTrnAmt(
								IFTTransferRequestDto.Body.XferTrnAddRequest.XferTrnAddRq.XferTrnDetail.PartTrnRec.TrnAmt value) {
							this.trnAmt = value;
						}

						public String getTrnParticulars() {
							return trnParticulars;
						}

						public void setTrnParticulars(String value) {
							this.trnParticulars = value;
						}

						public String getPartTrnRmks() {
							return partTrnRmks;
						}

						public void setPartTrnRmks(String value) {
							this.partTrnRmks = value;
						}

						public String getValueDt() {
							return valueDt;
						}

						public void setValueDt(String value) {
							this.valueDt = value;
						}

						public String getUserPartTrnCode() {
							return userPartTrnCode;
						}

						public void setUserPartTrnCode(String value) {
							this.userPartTrnCode = value;
						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "acctId" })
						@Builder
						@NoArgsConstructor
						@AllArgsConstructor
						public static class AcctId {

							@XmlElement(name = "AcctId")
							protected String acctId;

							public String getAcctId() {
								return acctId;
							}

							public void setAcctId(String value) {
								this.acctId = value;
							}

						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
						@Builder
						@NoArgsConstructor
						@AllArgsConstructor
						public static class TrnAmt {

							protected String amountValue;
							@XmlElement(required = true)
							protected String currencyCode;

							public String getAmountValue() {
								return amountValue;
							}

							public void setAmountValue(String value) {
								this.amountValue = value;
							}

							public String getCurrencyCode() {
								return currencyCode;
							}

							public void setCurrencyCode(String value) {
								this.currencyCode = value;
							}

						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "trnType", "trnSubType" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class XferTrnHdr {

					@XmlElement(name = "TrnType", required = true)
					protected String trnType;
					@XmlElement(name = "TrnSubType", required = true)
					protected String trnSubType;

					public String getTrnType() {
						return trnType;
					}

					public void setTrnType(String value) {
						this.trnType = value;
					}

					public String getTrnSubType() {
						return trnSubType;
					}

					public void setTrnSubType(String value) {
						this.trnSubType = value;
					}

				}

			}

		}

	}

}
