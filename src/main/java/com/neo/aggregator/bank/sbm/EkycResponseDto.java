package com.neo.aggregator.bank.sbm;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "rar", "uidData", "signature" })
@XmlRootElement(name = "KycRes")
public class EkycResponseDto {

	@XmlElement(name = "Rar", required = true)
	protected Object rar;
	@XmlElement(name = "UidData", required = true)
	protected EkycResponseDto.UidData uidData;
	@XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
	protected EkycResponseSignatureDto signature;
	@XmlAttribute(name = "code", required = true)
	protected String code;
	@XmlAttribute(name = "ret", required = true)
	protected String ret;
	@XmlAttribute(name = "ts", required = true)
	@XmlSchemaType(name = "dateTime")
	protected XMLGregorianCalendar ts;
	@XmlAttribute(name = "ttl", required = true)
	@XmlSchemaType(name = "dateTime")
	protected XMLGregorianCalendar ttl;
	@XmlAttribute(name = "txn", required = true)
	protected String txn;

	public Object getRar() {
		return rar;
	}

	public void setRar(Object value) {
		this.rar = value;
	}

	public EkycResponseDto.UidData getUidData() {
		return uidData;
	}

	public void setUidData(EkycResponseDto.UidData value) {
		this.uidData = value;
	}

	public EkycResponseSignatureDto getSignature() {
		return signature;
	}

	public void setSignature(EkycResponseSignatureDto value) {
		this.signature = value;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String value) {
		this.code = value;
	}

	public String getRet() {
		return ret;
	}

	public void setRet(String value) {
		this.ret = value;
	}

	public XMLGregorianCalendar getTs() {
		return ts;
	}

	public void setTs(XMLGregorianCalendar value) {
		this.ts = value;
	}

	public XMLGregorianCalendar getTtl() {
		return ttl;
	}

	public void setTtl(XMLGregorianCalendar value) {
		this.ttl = value;
	}

	public String getTxn() {
		return txn;
	}

	public void setTxn(String value) {
		this.txn = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "poi", "poa", "lData", "pht" })
	public static class UidData {

		@XmlElement(name = "Poi", required = true)
		protected EkycResponseDto.UidData.Poi poi;
		@XmlElement(name = "Poa", required = true)
		protected EkycResponseDto.UidData.Poa poa;
		@XmlElement(name = "LData", required = true)
		protected EkycResponseDto.UidData.LData lData;
		@XmlElement(name = "Pht", required = true)
		protected String pht;
		@XmlAttribute(name = "tkn", required = true)
		protected String tkn;
		@XmlAttribute(name = "uid", required = true)
		@XmlSchemaType(name = "unsignedLong")
		protected BigInteger uid;

		public EkycResponseDto.UidData.Poi getPoi() {
			return poi;
		}

		public void setPoi(EkycResponseDto.UidData.Poi value) {
			this.poi = value;
		}

		public EkycResponseDto.UidData.Poa getPoa() {
			return poa;
		}

		public void setPoa(EkycResponseDto.UidData.Poa value) {
			this.poa = value;
		}

		public EkycResponseDto.UidData.LData getLData() {
			return lData;
		}

		public void setLData(EkycResponseDto.UidData.LData value) {
			this.lData = value;
		}

		public String getPht() {
			return pht;
		}

		public void setPht(String value) {
			this.pht = value;
		}

		public String getTkn() {
			return tkn;
		}

		public void setTkn(String value) {
			this.tkn = value;
		}

		public BigInteger getUid() {
			return uid;
		}

		public void setUid(BigInteger value) {
			this.uid = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class LData {

			@XmlAttribute(name = "co", required = true)
			protected String co;
			@XmlAttribute(name = "country", required = true)
			protected String country;
			@XmlAttribute(name = "dist", required = true)
			protected String dist;
			@XmlAttribute(name = "house", required = true)
			protected String house;
			@XmlAttribute(name = "lang", required = true)
			@XmlSchemaType(name = "unsignedByte")
			protected short lang;
			@XmlAttribute(name = "name", required = true)
			protected String name;
			@XmlAttribute(name = "pc", required = true)
			protected String pc;
			@XmlAttribute(name = "state", required = true)
			protected String state;
			@XmlAttribute(name = "street", required = true)
			protected String street;
			@XmlAttribute(name = "vtc", required = true)
			protected String vtc;

			public String getCo() {
				return co;
			}

			public void setCo(String value) {
				this.co = value;
			}

			public String getCountry() {
				return country;
			}

			public void setCountry(String value) {
				this.country = value;
			}

			public String getDist() {
				return dist;
			}

			public void setDist(String value) {
				this.dist = value;
			}

			public String getHouse() {
				return house;
			}

			public void setHouse(String value) {
				this.house = value;
			}

			public short getLang() {
				return lang;
			}

			public void setLang(short value) {
				this.lang = value;
			}

			public String getName() {
				return name;
			}

			public void setName(String value) {
				this.name = value;
			}

			public String getPc() {
				return pc;
			}

			public void setPc(String value) {
				this.pc = value;
			}

			public String getState() {
				return state;
			}

			public void setState(String value) {
				this.state = value;
			}

			public String getStreet() {
				return street;
			}

			public void setStreet(String value) {
				this.street = value;
			}

			public String getVtc() {
				return vtc;
			}

			public void setVtc(String value) {
				this.vtc = value;
			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class Poa {

			@XmlAttribute(name = "co", required = true)
			protected String co;
			@XmlAttribute(name = "country", required = true)
			protected String country;
			@XmlAttribute(name = "dist", required = true)
			protected String dist;
			@XmlAttribute(name = "house", required = true)
			protected String house;
			@XmlAttribute(name = "pc", required = true)
			protected String pc;
			@XmlAttribute(name = "state", required = true)
			protected String state;
			@XmlAttribute(name = "street", required = true)
			protected String street;
			@XmlAttribute(name = "vtc", required = true)
			protected String vtc;

			public String getCo() {
				return co;
			}

			public void setCo(String value) {
				this.co = value;
			}

			public String getCountry() {
				return country;
			}

			public void setCountry(String value) {
				this.country = value;
			}

			public String getDist() {
				return dist;
			}

			public void setDist(String value) {
				this.dist = value;
			}

			public String getHouse() {
				return house;
			}

			public void setHouse(String value) {
				this.house = value;
			}

			public String getPc() {
				return pc;
			}

			public void setPc(String value) {
				this.pc = value;
			}

			public String getState() {
				return state;
			}

			public void setState(String value) {
				this.state = value;
			}

			public String getStreet() {
				return street;
			}

			public void setStreet(String value) {
				this.street = value;
			}

			public String getVtc() {
				return vtc;
			}

			public void setVtc(String value) {
				this.vtc = value;
			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class Poi {

			@XmlAttribute(name = "dob", required = true)
			protected String dob;
			@XmlAttribute(name = "gender", required = true)
			protected String gender;
			@XmlAttribute(name = "name", required = true)
			protected String name;

			public String getDob() {
				return dob;
			}

			public void setDob(String value) {
				this.dob = value;
			}

			public String getGender() {
				return gender;
			}

			public void setGender(String value) {
				this.gender = value;
			}

			public String getName() {
				return name;
			}

			public void setName(String value) {
				this.name = value;
			}

		}

	}

}
