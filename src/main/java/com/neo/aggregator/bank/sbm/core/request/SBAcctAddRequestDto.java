package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SBAcctAddRequestDto {

	@XmlElement(name = "Header")
	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body", required = true)
	protected SBAcctAddRequestDto.Body body;

	public SBAcctAddRequestDto.Body getBody() {
		return body;
	}

	public void setBody(SBAcctAddRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "sbAcctAddRequest" })
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Body {

		@XmlElement(name = "SBAcctAddRequest", required = true)
		protected SBAcctAddRequestDto.Body.SBAcctAddRequest sbAcctAddRequest;

		public SBAcctAddRequestDto.Body.SBAcctAddRequest getSBAcctAddRequest() {
			return sbAcctAddRequest;
		}

		public void setSBAcctAddRequest(SBAcctAddRequestDto.Body.SBAcctAddRequest value) {
			this.sbAcctAddRequest = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "sbAcctAddRq", "sbAcctAddCustomData" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class SBAcctAddRequest {

			@XmlElement(name = "SBAcctAddRq", required = true)
			protected SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq sbAcctAddRq;
			@XmlElement(name = "SBAcctAdd_CustomData", required = true)
			protected SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddCustomData sbAcctAddCustomData;

			public SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq getSBAcctAddRq() {
				return sbAcctAddRq;
			}

			public void setSBAcctAddRq(SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq value) {
				this.sbAcctAddRq = value;
			}

			public SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddCustomData getSBAcctAddCustomData() {
				return sbAcctAddCustomData;
			}

			public void setSBAcctAddCustomData(SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddCustomData value) {
				this.sbAcctAddCustomData = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "solid", "calflg", "intcodecr", "chnlid", "modeofoperation",
					"sbmiscentrd", "freetext1", "freetext2", "freetext3", "freetext4", "freetext5", "freetext6",
					"freetext7", "freetext8", "freetext9", "debitcardflg", "freezeCode","freezeReasonCode" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class SBAcctAddCustomData {

				@XmlElement(name = "SOLID")
				protected String solid;
				@XmlElement(name = "CALFLG", required = true)
				protected String calflg;
				@XmlElement(name = "INTCODECR", required = true)
				protected String intcodecr;
				@XmlElement(name = "CHNLID", required = true)
				protected String chnlid;
				@XmlElement(name = "MODEOFOPERATION", required = true)
				protected String modeofoperation;
				@XmlElement(name = "SBMISCENTRD")
				protected String sbmiscentrd;
				@XmlElement(name = "FREETEXT1", required = true)
				protected String freetext1;
				@XmlElement(name = "FREETEXT2", required = true)
				protected String freetext2;
				@XmlElement(name = "FREETEXT3", required = true)
				protected String freetext3;
				@XmlElement(name = "FREETEXT4", required = true)
				protected String freetext4;
				@XmlElement(name = "FREETEXT5", required = true)
				protected String freetext5;
				@XmlElement(name = "FREETEXT6", required = true)
				protected String freetext6;
				@XmlElement(name = "FREETEXT7", required = true)
				protected String freetext7;
				@XmlElement(name = "FREETEXT8", required = true)
				protected String freetext8;
				@XmlElement(name = "FREETEXT9", required = true)
				protected String freetext9;
				@XmlElement(name = "DEBIT_CARD_FLG", required = true)
				protected String debitcardflg;
				@XmlElement(name = "FREZ_CODE")
				protected String freezeCode;
				@XmlElement(name = "FREZ_REASON_CODE")
				protected String freezeReasonCode;
				
				public String getSOLID() {
					return solid;
				}

				public void setSOLID(String value) {
					this.solid = value;
				}

				public String getCALFLG() {
					return calflg;
				}

				public void setCALFLG(String value) {
					this.calflg = value;
				}

				public String getINTCODECR() {
					return intcodecr;
				}

				public void setINTCODECR(String value) {
					this.intcodecr = value;
				}

				public String getCHNLID() {
					return chnlid;
				}

				public void setCHNLID(String value) {
					this.chnlid = value;
				}

				public String getMODEOFOPERATION() {
					return modeofoperation;
				}

				public void setMODEOFOPERATION(String value) {
					this.modeofoperation = value;
				}

				public String getSBMISCENTRD() {
					return sbmiscentrd;
				}

				public void setSBMISCENTRD(String value) {
					this.sbmiscentrd = value;
				}

				public String getFREETEXT1() {
					return freetext1;
				}

				public void setFREETEXT1(String value) {
					this.freetext1 = value;
				}

				public String getFREETEXT2() {
					return freetext2;
				}

				public void setFREETEXT2(String value) {
					this.freetext2 = value;
				}

				public String getFREETEXT3() {
					return freetext3;
				}

				public void setFREETEXT3(String value) {
					this.freetext3 = value;
				}

				public String getFREETEXT4() {
					return freetext4;
				}

				public void setFREETEXT4(String value) {
					this.freetext4 = value;
				}

				public String getFREETEXT5() {
					return freetext5;
				}

				public void setFREETEXT5(String value) {
					this.freetext5 = value;
				}

				public String getFREETEXT6() {
					return freetext6;
				}

				public void setFREETEXT6(String value) {
					this.freetext6 = value;
				}

				public String getFREETEXT7() {
					return freetext7;
				}

				public void setFREETEXT7(String value) {
					this.freetext7 = value;
				}

				public String getFREETEXT8() {
					return freetext8;
				}

				public void setFREETEXT8(String value) {
					this.freetext8 = value;
				}

				public String getFREETEXT9() {
					return freetext9;
				}

				public void setFREETEXT9(String value) {
					this.freetext9 = value;
				}

				public String getDEBITCARDFLG() {
					return debitcardflg;
				}

				public void setDEBITCARDFLG(String value) {
					this.debitcardflg = value;
				}

				public String getFreezeCode() {
					return freezeCode;
				}

				public String getFreezeReasonCode() {
					return freezeReasonCode;
				}

				public void setFreezeCode(String freezeCode) {
					this.freezeCode = freezeCode;
				}

				public void setFreezeReasonCode(String freezeReasonCode) {
					this.freezeReasonCode = freezeReasonCode;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "custId", "sbAcctId", "sbAcctGenInfo", "nomineeInfoRec" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class SBAcctAddRq {

				@XmlElement(name = "CustId", required = true)
				protected SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.CustId custId;
				@XmlElement(name = "SBAcctId", required = true)
				protected SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.SBAcctId sbAcctId;
				@XmlElement(name = "SBAcctGenInfo", required = true)
				protected SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.SBAcctGenInfo sbAcctGenInfo;
				@XmlElement(name = "NomineeInfoRec", required = true)
				protected SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec nomineeInfoRec;

				public SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.CustId getCustId() {
					return custId;
				}

				public void setCustId(SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.CustId value) {
					this.custId = value;
				}

				public SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.SBAcctId getSBAcctId() {
					return sbAcctId;
				}

				public void setSBAcctId(SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.SBAcctId value) {
					this.sbAcctId = value;
				}

				public SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.SBAcctGenInfo getSBAcctGenInfo() {
					return sbAcctGenInfo;
				}

				public void setSBAcctGenInfo(
						SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.SBAcctGenInfo value) {
					this.sbAcctGenInfo = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "custId" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class CustId {

					@XmlElement(name = "CustId", required = true)
					protected String custId;

					public String getCustId() {
						return custId;
					}

					public void setCustId(String value) {
						this.custId = value;
					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "despatchMode" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class SBAcctGenInfo {

					@XmlElement(name = "DespatchMode", required = true)
					protected String despatchMode;

					public String getDespatchMode() {
						return despatchMode;
					}

					public void setDespatchMode(String value) {
						this.despatchMode = value;
					}

				}
				
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "regNum", "nomineeName", "relType", "nomineeContactInfo",
						"nomineeMinorFlg", "nomineeBirthDt", "nomineePercent" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class NomineeInfoRec {

					@XmlElement(name = "RegNum")
					protected String regNum;
					@XmlElement(name = "NomineeName", required = true)
					protected String nomineeName;
					@XmlElement(name = "RelType")
					protected String relType;
					@XmlElement(name = "NomineeContactInfo", required = true)
					protected SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineeContactInfo nomineeContactInfo;
					@XmlElement(name = "NomineeMinorFlg", required = true)
					protected String nomineeMinorFlg;
					@XmlElement(name = "NomineeBirthDt", required = true)
					@XmlSchemaType(name = "dateTime")
					protected String nomineeBirthDt;
					@XmlElement(name = "NomineePercent", required = true)
					protected SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineePercent nomineePercent;

					public String getRegNum() {
						return regNum;
					}

					public void setRegNum(String value) {
						this.regNum = value;
					}

					public String getNomineeName() {
						return nomineeName;
					}

					public void setNomineeName(String value) {
						this.nomineeName = value;
					}

					public String getRelType() {
						return relType;
					}

					public void setRelType(String value) {
						this.relType = value;
					}

					public SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineeContactInfo getNomineeContactInfo() {
						return nomineeContactInfo;
					}

					public void setNomineeContactInfo(
							SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineeContactInfo value) {
						this.nomineeContactInfo = value;
					}

					public String getNomineeMinorFlg() {
						return nomineeMinorFlg;
					}

					public void setNomineeMinorFlg(String value) {
						this.nomineeMinorFlg = value;
					}

					public String getNomineeBirthDt() {
						return nomineeBirthDt;
					}

					public void setNomineeBirthDt(String value) {
						this.nomineeBirthDt = value;
					}

					public SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineePercent getNomineePercent() {
						return nomineePercent;
					}

					public void setNomineePercent(
							SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineePercent value) {
						this.nomineePercent = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "postAddr" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class NomineeContactInfo {

						@XmlElement(name = "PostAddr", required = true)
						protected SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineeContactInfo.PostAddr postAddr;

						public SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineeContactInfo.PostAddr getPostAddr() {
							return postAddr;
						}

						public void setPostAddr(
								SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.NomineeInfoRec.NomineeContactInfo.PostAddr value) {
							this.postAddr = value;
						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "addr1", "addr2", "addr3", "city", "stateProv", "postalCode",
								"country" })
						@Builder
						@NoArgsConstructor
						@AllArgsConstructor
						public static class PostAddr {

							@XmlElement(name = "Addr1", required = true)
							protected String addr1;
							@XmlElement(name = "Addr2", required = true)
							protected String addr2;
							@XmlElement(name = "Addr3", required = true)
							protected String addr3;
							@XmlElement(name = "City")
							protected String city;
							@XmlElement(name = "StateProv")
							protected String stateProv;
							@XmlElement(name = "PostalCode")
							protected String postalCode;
							@XmlElement(name = "Country", required = true)
							protected String country;

							public String getAddr1() {
								return addr1;
							}

							public void setAddr1(String value) {
								this.addr1 = value;
							}

							public String getAddr2() {
								return addr2;
							}

							public void setAddr2(String value) {
								this.addr2 = value;
							}

							public String getAddr3() {
								return addr3;
							}

							public void setAddr3(String value) {
								this.addr3 = value;
							}

							public String getCity() {
								return city;
							}

							public void setCity(String value) {
								this.city = value;
							}

							public String getStateProv() {
								return stateProv;
							}

							public void setStateProv(String value) {
								this.stateProv = value;
							}

							public String getPostalCode() {
								return postalCode;
							}

							public void setPostalCode(String value) {
								this.postalCode = value;
							}

							public String getCountry() {
								return country;
							}

							public void setCountry(String value) {
								this.country = value;
							}

						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "value" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class NomineePercent {

						protected String value;

						public String getValue() {
							return value;
						}

						public void setValue(String value) {
							this.value = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctType", "acctCurr" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class SBAcctId {

					@XmlElement(name = "AcctType", required = true)
					protected SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.SBAcctId.AcctType acctType;
					@XmlElement(name = "AcctCurr", required = true)
					protected String acctCurr;

					public SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.SBAcctId.AcctType getAcctType() {
						return acctType;
					}

					public void setAcctType(
							SBAcctAddRequestDto.Body.SBAcctAddRequest.SBAcctAddRq.SBAcctId.AcctType value) {
						this.acctType = value;
					}

					public String getAcctCurr() {
						return acctCurr;
					}

					public void setAcctCurr(String value) {
						this.acctCurr = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "schmCode" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class AcctType {

						@XmlElement(name = "SchmCode", required = true)
						protected String schmCode;

						public String getSchmCode() {
							return schmCode;
						}

						public void setSchmCode(String value) {
							this.schmCode = value;
						}

					}

				}

			}

		}

	}
}

