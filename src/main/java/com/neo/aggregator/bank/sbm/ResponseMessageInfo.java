package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseMessageInfo {

	protected String bankId;

	protected String timeZone;

	protected String entityId;

	protected String entityType;

	protected String armCorrelationId;

	protected String messageDateTime;

	@XmlElement(name = "BankId")
	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	@XmlElement(name = "EntityId")
	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	@XmlElement(name = "EntityType")
	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	@XmlElement(name = "ArmCorrelationId")
	public String getArmCorrelationId() {
		return armCorrelationId;
	}

	public void setArmCorrelationId(String armCorrelationId) {
		this.armCorrelationId = armCorrelationId;
	}

	@XmlElement(name = "MessageDateTime")
	public String getMessageDateTime() {
		return messageDateTime;
	}

	public void setMessageDateTime(String messageDateTime) {
		this.messageDateTime = messageDateTime;
	}

	@XmlElement(name = "TimeZone")
	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

}
