package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TdAcctCloseRequestDto {

	@XmlElement(name = "Header")
	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body", required = true)
	protected TdAcctCloseRequestDto.Body body;

	public TdAcctCloseRequestDto.Body getBody() {
		return body;
	}

	public void setBody(TdAcctCloseRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "depAcctCloseRequest", "depAcctCloseCustomData" })
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Body {

		@XmlElement(name = "DepAcctCloseRequest", required = true)
		protected TdAcctCloseRequestDto.Body.DepAcctCloseRequest depAcctCloseRequest;
		@XmlElement(name = "DepAcctClose_CustomData", required = true)
		protected TdAcctCloseRequestDto.Body.DepAcctCloseCustomData depAcctCloseCustomData;

		public TdAcctCloseRequestDto.Body.DepAcctCloseRequest getDepAcctCloseRequest() {
			return depAcctCloseRequest;
		}

		public void setDepAcctCloseRequest(TdAcctCloseRequestDto.Body.DepAcctCloseRequest value) {
			this.depAcctCloseRequest = value;
		}

		public TdAcctCloseRequestDto.Body.DepAcctCloseCustomData getDepAcctCloseCustomData() {
			return depAcctCloseCustomData;
		}

		public void setDepAcctCloseCustomData(TdAcctCloseRequestDto.Body.DepAcctCloseCustomData value) {
			this.depAcctCloseCustomData = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "withdrwlamt", "wcrncy" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class DepAcctCloseCustomData {

			@XmlElement(name = "WITHDRWLAMT")
			protected String withdrwlamt;
			@XmlElement(name = "WCRNCY", required = true)
			protected String wcrncy;

			public String getWITHDRWLAMT() {
				return withdrwlamt;
			}

			public void setWITHDRWLAMT(String value) {
				this.withdrwlamt = value;
			}

			public String getWCRNCY() {
				return wcrncy;
			}

			public void setWCRNCY(String value) {
				this.wcrncy = value;
			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "depAcctCloseRq" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class DepAcctCloseRequest {

			@XmlElement(name = "DepAcctCloseRq", required = true)
			protected TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq depAcctCloseRq;

			public TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq getDepAcctCloseRq() {
				return depAcctCloseRq;
			}

			public void setDepAcctCloseRq(TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq value) {
				this.depAcctCloseRq = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "depAcctId", "closeModeFlg", "closeAmt", "repayAcctId" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class DepAcctCloseRq {

				@XmlElement(name = "DepAcctId", required = true)
				protected TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.DepAcctId depAcctId;
				@XmlElement(name = "CloseModeFlg", required = true)
				protected String closeModeFlg;
				@XmlElement(name = "CloseAmt", required = true)
				protected TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.CloseAmt closeAmt;
				@XmlElement(name = "RepayAcctId", required = true)
				protected TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.RepayAcctId repayAcctId;

				public TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.DepAcctId getDepAcctId() {
					return depAcctId;
				}

				public void setDepAcctId(
						TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.DepAcctId value) {
					this.depAcctId = value;
				}

				public String getCloseModeFlg() {
					return closeModeFlg;
				}

				public void setCloseModeFlg(String value) {
					this.closeModeFlg = value;
				}

				public TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.CloseAmt getCloseAmt() {
					return closeAmt;
				}

				public void setCloseAmt(TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.CloseAmt value) {
					this.closeAmt = value;
				}

				public TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.RepayAcctId getRepayAcctId() {
					return repayAcctId;
				}

				public void setRepayAcctId(
						TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.RepayAcctId value) {
					this.repayAcctId = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class CloseAmt {
					
					@XmlElement(required = true)
					protected String amountValue;
					@XmlElement(required = true)
					protected String currencyCode;

					public String getAmountValue() {
						return amountValue;
					}

					public void setAmountValue(String value) {
						this.amountValue = value;
					}

					public String getCurrencyCode() {
						return currencyCode;
					}

					public void setCurrencyCode(String value) {
						this.currencyCode = value;
					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctId", "acctType" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class DepAcctId {

					@XmlElement(name = "AcctId")
					protected String acctId;

					@XmlElement(name = "AcctType", required = true)
					protected TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.DepAcctId.AcctType acctType;

					public String getAcctId() {
						return acctId;
					}

					public void setAcctId(String value) {
						this.acctId = value;
					}

					public TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.DepAcctId.AcctType getAcctType() {
						return acctType;
					}

					public void setAcctType(
							TdAcctCloseRequestDto.Body.DepAcctCloseRequest.DepAcctCloseRq.DepAcctId.AcctType value) {
						this.acctType = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "schmType" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class AcctType {

						@XmlElement(name = "SchmType", required = true)
						protected String schmType;

						public String getSchmType() {
							return schmType;
						}

						public void setSchmType(String value) {
							this.schmType = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctId" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class RepayAcctId {

					@XmlElement(name = "AcctId", required = true)
					protected String acctId;

					public String getAcctId() {
						return acctId;
					}

					public void setAcctId(String value) {
						this.acctId = value;
					}

				}

			}

		}

	}

}
