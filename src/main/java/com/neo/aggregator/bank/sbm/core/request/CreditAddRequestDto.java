package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreditAddRequestDto {

	@XmlElement(name = "Header")
	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body", required = true)
	protected CreditAddRequestDto.Body body;

	public CreditAddRequestDto.Body getBody() {
		return body;
	}

	public void setBody(CreditAddRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "creditAddRequest" })
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Body {

		@XmlElement(name = "CreditAddRequest", required = true)
		protected CreditAddRequestDto.Body.CreditAddRequest creditAddRequest;

		public CreditAddRequestDto.Body.CreditAddRequest getCreditAddRequest() {
			return creditAddRequest;
		}

		public void setCreditAddRequest(CreditAddRequestDto.Body.CreditAddRequest value) {
			this.creditAddRequest = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "creditAddRq" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class CreditAddRequest {

			@XmlElement(name = "CreditAddRq", required = true)
			protected CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq creditAddRq;

			public CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq getCreditAddRq() {
				return creditAddRq;
			}

			public void setCreditAddRq(CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq value) {
				this.creditAddRq = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "totAmtCurCode", "trnSubType", "creditPartTrnRec" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class CreditAddRq {

				@XmlElement(name = "TotAmtCurCode", required = true)
				protected String totAmtCurCode;
				@XmlElement(name = "TrnSubType", required = true)
				protected String trnSubType;
				@XmlElement(name = "CreditPartTrnRec", required = true)
				protected CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq.CreditPartTrnRec creditPartTrnRec;

				public String getTotAmtCurCode() {
					return totAmtCurCode;
				}

				public void setTotAmtCurCode(String value) {
					this.totAmtCurCode = value;
				}

				public String getTrnSubType() {
					return trnSubType;
				}

				public void setTrnSubType(String value) {
					this.trnSubType = value;
				}

				public CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq.CreditPartTrnRec getCreditPartTrnRec() {
					return creditPartTrnRec;
				}

				public void setCreditPartTrnRec(
						CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq.CreditPartTrnRec value) {
					this.creditPartTrnRec = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctId", "trnAmt", "valueDt" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class CreditPartTrnRec {

					@XmlElement(name = "AcctId", required = true)
					protected CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq.CreditPartTrnRec.AcctId acctId;
					@XmlElement(name = "TrnAmt", required = true)
					protected CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq.CreditPartTrnRec.TrnAmt trnAmt;
					@XmlElement(name = "ValueDt", required = true)
					@XmlSchemaType(name = "dateTime")
					protected String valueDt;

					public CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq.CreditPartTrnRec.AcctId getAcctId() {
						return acctId;
					}

					public void setAcctId(
							CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq.CreditPartTrnRec.AcctId value) {
						this.acctId = value;
					}

					public CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq.CreditPartTrnRec.TrnAmt getTrnAmt() {
						return trnAmt;
					}

					public void setTrnAmt(
							CreditAddRequestDto.Body.CreditAddRequest.CreditAddRq.CreditPartTrnRec.TrnAmt value) {
						this.trnAmt = value;
					}

					public String getValueDt() {
						return valueDt;
					}

					public void setValueDt(String value) {
						this.valueDt = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "acctId" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class AcctId {

						@XmlElement(name = "AcctId")
						protected String acctId;

						public String getAcctId() {
							return acctId;
						}

						public void setAcctId(String value) {
							this.acctId = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class TrnAmt {

						protected String amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public String getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(String value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

				}

			}

		}

	}

}

