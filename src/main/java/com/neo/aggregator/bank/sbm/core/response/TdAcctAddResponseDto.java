package com.neo.aggregator.bank.sbm.core.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
public class TdAcctAddResponseDto {

	@XmlElement(name = "Header", required = true)
	protected TdAcctAddResponseDto.Header header;
	@XmlElement(name = "Body", required = true)
	protected TdAcctAddResponseDto.Body body;

	public TdAcctAddResponseDto.Header getHeader() {
		return header;
	}

	public void setHeader(TdAcctAddResponseDto.Header value) {
		this.header = value;
	}

	public TdAcctAddResponseDto.Body getBody() {
		return body;
	}

	public void setBody(TdAcctAddResponseDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "tdAcctAddResponse", "error" })
	public static class Body {

		@XmlElement(name = "TDAcctAddResponse", required = true)
		protected TdAcctAddResponseDto.Body.TDAcctAddResponse tdAcctAddResponse;

		public TdAcctAddResponseDto.Body.TDAcctAddResponse getTDAcctAddResponse() {
			return tdAcctAddResponse;
		}

		public void setTDAcctAddResponse(TdAcctAddResponseDto.Body.TDAcctAddResponse value) {
			this.tdAcctAddResponse = value;
		}

		@XmlElement(name = "Error")
		protected Error error;

		public Error getError() {
			return error;
		}

		public void setError(Error value) {
			this.error = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)

		@XmlType(name = "")
		public static class Error {

			@XmlElement(name = "FIBusinessException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiBusinessException;

			@XmlElement(name = "FISystemException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException;

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFiSystemException() {
				return fiSystemException;
			}

			public void setFiSystemException(RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException) {
				this.fiSystemException = fiSystemException;
			}

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFIBusinessException() {
				return fiBusinessException;
			}

			public void setFIBusinessException(RetCustAddResponseDto.Body.Error.FIBusinessException value) {
				this.fiBusinessException = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)

			@XmlType(name = "", propOrder = { "errorDetail" })
			public static class FIBusinessException {

				@XmlElement(name = "ErrorDetail")
				protected RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail errorDetail;

				public RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail getErrorDetail() {
					return errorDetail;
				}

				public void setErrorDetail(RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail value) {
					this.errorDetail = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)

				@XmlType(name = "", propOrder = { "errorCode", "errorDesc", "errorType" })
				public static class ErrorDetail {

					@XmlElement(name = "ErrorCode")
					protected String errorCode;

					@XmlElement(name = "ErrorDesc")
					protected String errorDesc;

					@XmlElement(name = "ErrorType")
					protected String errorType;

					public String getErrorCode() {
						return errorCode;
					}

					public void setErrorCode(String value) {
						this.errorCode = value;
					}

					public String getErrorDesc() {
						return errorDesc;
					}

					public void setErrorDesc(String value) {
						this.errorDesc = value;
					}

					public String getErrorType() {
						return errorType;
					}

					public void setErrorType(String value) {
						this.errorType = value;
					}

				}

			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "tdAcctAddRs", "tdAcctAddCustomData" })
		public static class TDAcctAddResponse {

			@XmlElement(name = "TDAcctAddRs", required = true)
			protected TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddRs tdAcctAddRs;
			@XmlElement(name = "TDAcctAdd_CustomData", required = true)
			protected TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddCustomData tdAcctAddCustomData;

			public TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddRs getTDAcctAddRs() {
				return tdAcctAddRs;
			}

			public void setTDAcctAddRs(TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddRs value) {
				this.tdAcctAddRs = value;
			}

			public TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddCustomData getTDAcctAddCustomData() {
				return tdAcctAddCustomData;
			}

			public void setTDAcctAddCustomData(TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddCustomData value) {
				this.tdAcctAddCustomData = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "critsolid", "freetext9", "freetext10" })
			public static class TDAcctAddCustomData {

				@XmlElement(name = "CRITSOLID")
				protected short critsolid;
				@XmlElement(name = "FREETEXT9", required = true)
				protected String freetext9;
				@XmlElement(name = "FREETEXT10", required = true)
				protected String freetext10;

				public short getCRITSOLID() {
					return critsolid;
				}

				public void setCRITSOLID(short value) {
					this.critsolid = value;
				}

				public String getFREETEXT9() {
					return freetext9;
				}

				public void setFREETEXT9(String value) {
					this.freetext9 = value;
				}

				public String getFREETEXT10() {
					return freetext10;
				}

				public void setFREETEXT10(String value) {
					this.freetext10 = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "tdAcctId" })
			public static class TDAcctAddRs {

				@XmlElement(name = "TDAcctId", required = true)
				protected TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddRs.TDAcctId tdAcctId;

				public TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddRs.TDAcctId getTDAcctId() {
					return tdAcctId;
				}

				public void setTDAcctId(TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddRs.TDAcctId value) {
					this.tdAcctId = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctId", "acctType", "acctCurr", "bankInfo" })
				public static class TDAcctId {

					@XmlElement(name = "AcctId")
					protected String acctId;
					@XmlElement(name = "AcctType", required = true)
					protected TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddRs.TDAcctId.AcctType acctType;
					@XmlElement(name = "AcctCurr", required = true)
					protected String acctCurr;
					@XmlElement(name = "BankInfo", required = true)
					protected TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddRs.TDAcctId.BankInfo bankInfo;

					public String getAcctId() {
						return acctId;
					}

					public void setAcctId(String value) {
						this.acctId = value;
					}

					public TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddRs.TDAcctId.AcctType getAcctType() {
						return acctType;
					}

					public void setAcctType(
							TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddRs.TDAcctId.AcctType value) {
						this.acctType = value;
					}

					public String getAcctCurr() {
						return acctCurr;
					}

					public void setAcctCurr(String value) {
						this.acctCurr = value;
					}

					public TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddRs.TDAcctId.BankInfo getBankInfo() {
						return bankInfo;
					}

					public void setBankInfo(
							TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddRs.TDAcctId.BankInfo value) {
						this.bankInfo = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "schmCode", "schmType" })
					public static class AcctType {

						@XmlElement(name = "SchmCode", required = true)
						protected String schmCode;
						@XmlElement(name = "SchmType", required = true)
						protected String schmType;

						public String getSchmCode() {
							return schmCode;
						}

						public void setSchmCode(String value) {
							this.schmCode = value;
						}

						public String getSchmType() {
							return schmType;
						}

						public void setSchmType(String value) {
							this.schmType = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "bankId", "name", "branchId", "branchName", "postAddr" })
					public static class BankInfo {

						@XmlElement(name = "BankId", required = true)
						protected String bankId;
						@XmlElement(name = "Name", required = true)
						protected String name;
						@XmlElement(name = "BranchId", required = true)
						protected String branchId;
						@XmlElement(name = "BranchName", required = true)
						protected String branchName;
						@XmlElement(name = "PostAddr", required = true)
						protected TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddRs.TDAcctId.BankInfo.PostAddr postAddr;

						public String getBankId() {
							return bankId;
						}

						public void setBankId(String value) {
							this.bankId = value;
						}

						public String getName() {
							return name;
						}

						public void setName(String value) {
							this.name = value;
						}

						public String getBranchId() {
							return branchId;
						}

						public void setBranchId(String value) {
							this.branchId = value;
						}

						public String getBranchName() {
							return branchName;
						}

						public void setBranchName(String value) {
							this.branchName = value;
						}

						public TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddRs.TDAcctId.BankInfo.PostAddr getPostAddr() {
							return postAddr;
						}

						public void setPostAddr(
								TdAcctAddResponseDto.Body.TDAcctAddResponse.TDAcctAddRs.TDAcctId.BankInfo.PostAddr value) {
							this.postAddr = value;
						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "addr1", "addr2", "addr3", "city", "stateProv", "postalCode",
								"country", "addrType" })
						public static class PostAddr {

							@XmlElement(name = "Addr1", required = true)
							protected String addr1;
							@XmlElement(name = "Addr2", required = true)
							protected String addr2;
							@XmlElement(name = "Addr3", required = true)
							protected String addr3;
							@XmlElement(name = "City", required = true)
							protected String city;
							@XmlElement(name = "StateProv", required = true)
							protected String stateProv;
							@XmlElement(name = "PostalCode", required = true)
							protected String postalCode;
							@XmlElement(name = "Country", required = true)
							protected String country;
							@XmlElement(name = "AddrType", required = true)
							protected String addrType;

							public String getAddr1() {
								return addr1;
							}

							public void setAddr1(String value) {
								this.addr1 = value;
							}

							public String getAddr2() {
								return addr2;
							}

							public void setAddr2(String value) {
								this.addr2 = value;
							}

							public String getAddr3() {
								return addr3;
							}

							public void setAddr3(String value) {
								this.addr3 = value;
							}

							public String getCity() {
								return city;
							}

							public void setCity(String value) {
								this.city = value;
							}

							public String getStateProv() {
								return stateProv;
							}

							public void setStateProv(String value) {
								this.stateProv = value;
							}

							public String getPostalCode() {
								return postalCode;
							}

							public void setPostalCode(String value) {
								this.postalCode = value;
							}

							public String getCountry() {
								return country;
							}

							public void setCountry(String value) {
								this.country = value;
							}

							public String getAddrType() {
								return addrType;
							}

							public void setAddrType(String value) {
								this.addrType = value;
							}

						}

					}

				}

			}

		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "responseHeader" })
	public static class Header {

		@XmlElement(name = "ResponseHeader", required = true)
		protected TdAcctAddResponseDto.Header.ResponseHeader responseHeader;

		public TdAcctAddResponseDto.Header.ResponseHeader getResponseHeader() {
			return responseHeader;
		}

		public void setResponseHeader(TdAcctAddResponseDto.Header.ResponseHeader value) {
			this.responseHeader = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "requestMessageKey", "responseMessageInfo", "ubusTransaction",
				"hostTransaction", "hostParentTransaction", "customInfo" })
		public static class ResponseHeader {

			@XmlElement(name = "RequestMessageKey", required = true)
			protected TdAcctAddResponseDto.Header.ResponseHeader.RequestMessageKey requestMessageKey;
			@XmlElement(name = "ResponseMessageInfo", required = true)
			protected TdAcctAddResponseDto.Header.ResponseHeader.ResponseMessageInfo responseMessageInfo;
			@XmlElement(name = "UBUSTransaction", required = true)
			protected TdAcctAddResponseDto.Header.ResponseHeader.UBUSTransaction ubusTransaction;
			@XmlElement(name = "HostTransaction", required = true)
			protected TdAcctAddResponseDto.Header.ResponseHeader.HostTransaction hostTransaction;
			@XmlElement(name = "HostParentTransaction", required = true)
			protected TdAcctAddResponseDto.Header.ResponseHeader.HostParentTransaction hostParentTransaction;
			@XmlElement(name = "CustomInfo", required = true)
			protected String customInfo;

			public TdAcctAddResponseDto.Header.ResponseHeader.RequestMessageKey getRequestMessageKey() {
				return requestMessageKey;
			}

			public void setRequestMessageKey(TdAcctAddResponseDto.Header.ResponseHeader.RequestMessageKey value) {
				this.requestMessageKey = value;
			}

			public TdAcctAddResponseDto.Header.ResponseHeader.ResponseMessageInfo getResponseMessageInfo() {
				return responseMessageInfo;
			}

			public void setResponseMessageInfo(TdAcctAddResponseDto.Header.ResponseHeader.ResponseMessageInfo value) {
				this.responseMessageInfo = value;
			}

			public TdAcctAddResponseDto.Header.ResponseHeader.UBUSTransaction getUBUSTransaction() {
				return ubusTransaction;
			}

			public void setUBUSTransaction(TdAcctAddResponseDto.Header.ResponseHeader.UBUSTransaction value) {
				this.ubusTransaction = value;
			}

			public TdAcctAddResponseDto.Header.ResponseHeader.HostTransaction getHostTransaction() {
				return hostTransaction;
			}

			public void setHostTransaction(TdAcctAddResponseDto.Header.ResponseHeader.HostTransaction value) {
				this.hostTransaction = value;
			}

			public TdAcctAddResponseDto.Header.ResponseHeader.HostParentTransaction getHostParentTransaction() {
				return hostParentTransaction;
			}

			public void setHostParentTransaction(
					TdAcctAddResponseDto.Header.ResponseHeader.HostParentTransaction value) {
				this.hostParentTransaction = value;
			}

			public String getCustomInfo() {
				return customInfo;
			}

			public void setCustomInfo(String value) {
				this.customInfo = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostParentTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "requestUUID", "serviceRequestId", "serviceRequestVersion", "channelId" })
			public static class RequestMessageKey {

				@XmlElement(name = "RequestUUID", required = true)
				protected String requestUUID;
				@XmlElement(name = "ServiceRequestId", required = true)
				protected String serviceRequestId;
				@XmlElement(name = "ServiceRequestVersion")
				protected float serviceRequestVersion;
				@XmlElement(name = "ChannelId", required = true)
				protected String channelId;

				public String getRequestUUID() {
					return requestUUID;
				}

				public void setRequestUUID(String value) {
					this.requestUUID = value;
				}

				public String getServiceRequestId() {
					return serviceRequestId;
				}

				public void setServiceRequestId(String value) {
					this.serviceRequestId = value;
				}

				public float getServiceRequestVersion() {
					return serviceRequestVersion;
				}

				public void setServiceRequestVersion(float value) {
					this.serviceRequestVersion = value;
				}

				public String getChannelId() {
					return channelId;
				}

				public void setChannelId(String value) {
					this.channelId = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "bankId", "timeZone", "messageDateTime" })
			public static class ResponseMessageInfo {

				@XmlElement(name = "BankId", required = true)
				protected String bankId;
				@XmlElement(name = "TimeZone", required = true)
				protected String timeZone;
				@XmlElement(name = "MessageDateTime", required = true)
				@XmlSchemaType(name = "dateTime")
				protected XMLGregorianCalendar messageDateTime;

				public String getBankId() {
					return bankId;
				}

				public void setBankId(String value) {
					this.bankId = value;
				}

				public String getTimeZone() {
					return timeZone;
				}

				public void setTimeZone(String value) {
					this.timeZone = value;
				}

				public XMLGregorianCalendar getMessageDateTime() {
					return messageDateTime;
				}

				public void setMessageDateTime(XMLGregorianCalendar value) {
					this.messageDateTime = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class UBUSTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

		}

	}

}

