package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TdAcctAddRequestDto {

	@XmlElement(name = "Header")
	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body", required = true)
	protected TdAcctAddRequestDto.Body body;

	public TdAcctAddRequestDto.Body getBody() {
		return body;
	}

	public void setBody(TdAcctAddRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "tdAcctAddRequest" })
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Body {

		@XmlElement(name = "TDAcctAddRequest", required = true)
		protected TdAcctAddRequestDto.Body.TDAcctAddRequest tdAcctAddRequest;

		public TdAcctAddRequestDto.Body.TDAcctAddRequest getTDAcctAddRequest() {
			return tdAcctAddRequest;
		}

		public void setTDAcctAddRequest(TdAcctAddRequestDto.Body.TDAcctAddRequest value) {
			this.tdAcctAddRequest = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "tdAcctAddRq", "tdAcctAddCustomData" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class TDAcctAddRequest {

			@XmlElement(name = "TDAcctAddRq", required = true)
			protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq tdAcctAddRq;
			@XmlElement(name = "TDAcctAdd_CustomData", required = true)
			protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddCustomData tdAcctAddCustomData;

			public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq getTDAcctAddRq() {
				return tdAcctAddRq;
			}

			public void setTDAcctAddRq(TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq value) {
				this.tdAcctAddRq = value;
			}

			public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddCustomData getTDAcctAddCustomData() {
				return tdAcctAddCustomData;
			}

			public void setTDAcctAddCustomData(TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddCustomData value) {
				this.tdAcctAddCustomData = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "xferind", "critsolid", "trancremode", "intcracct", "chnlid",
					"modeofoperation", "relparty" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class TDAcctAddCustomData {

				@XmlElement(name = "XFERIND", required = true)
				protected String xferind;
				@XmlElement(name = "CRITSOLID")
				protected String critsolid;
				@XmlElement(name = "TRANCREMODE", required = true)
				protected String trancremode;
				@XmlElement(name = "INTCRACCT")
				protected String intcracct;
				@XmlElement(name = "CHNLID")
				protected String chnlid;
				@XmlElement(name = "MODEOFOPERATION", required = true)
				protected String modeofoperation;
				@XmlElement(name = "RELPARTY", required = true)
				protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddCustomData.RELPARTY relparty;

				public String getXFERIND() {
					return xferind;
				}

				public void setXFERIND(String value) {
					this.xferind = value;
				}

				public String getCRITSOLID() {
					return critsolid;
				}

				public void setCRITSOLID(String value) {
					this.critsolid = value;
				}

				public String getTRANCREMODE() {
					return trancremode;
				}

				public void setTRANCREMODE(String value) {
					this.trancremode = value;
				}

				public String getIntCRACCT() {
					return intcracct;
				}

				public void setIntCRACCT(String value) {
					this.intcracct = value;
				}

				public String getMODEOFOPERATION() {
					return modeofoperation;
				}

				public void setMODEOFOPERATION(String value) {
					this.modeofoperation = value;
				}

				public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddCustomData.RELPARTY getRELPARTY() {
					return relparty;
				}

				public void setRELPARTY(TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddCustomData.RELPARTY value) {
					this.relparty = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "passsheetflg", "loanodnoticeflg", "cifid", "xcludecombstmtflg",
						"siflg", "depositnoticeflg" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class RELPARTY {

					@XmlElement(name = "PASSSHEETFLG", required = true)
					protected String passsheetflg;
					@XmlElement(name = "LOANODNOTICEFLG", required = true)
					protected String loanodnoticeflg;
					@XmlElement(name = "CIFID")
					protected String cifid;
					@XmlElement(name = "XCLUDECOMBSTMTFLG", required = true)
					protected String xcludecombstmtflg;
					@XmlElement(name = "SIFLG", required = true)
					protected String siflg;
					@XmlElement(name = "DEPOSITNOTICEFLG", required = true)
					protected String depositnoticeflg;
					@XmlAttribute(name = "isMultiRec")
					protected String isMultiRec;

					public String getPASSSHEETFLG() {
						return passsheetflg;
					}

					public void setPASSSHEETFLG(String value) {
						this.passsheetflg = value;
					}

					public String getLOANODNOTICEFLG() {
						return loanodnoticeflg;
					}

					public void setLOANODNOTICEFLG(String value) {
						this.loanodnoticeflg = value;
					}

					public String getCIFID() {
						return cifid;
					}

					public void setCIFID(String value) {
						this.cifid = value;
					}

					public String getXCLUDECOMBSTMTFLG() {
						return xcludecombstmtflg;
					}

					public void setXCLUDECOMBSTMTFLG(String value) {
						this.xcludecombstmtflg = value;
					}

					public String getSIFLG() {
						return siflg;
					}

					public void setSIFLG(String value) {
						this.siflg = value;
					}

					public String getDEPOSITNOTICEFLG() {
						return depositnoticeflg;
					}

					public void setDEPOSITNOTICEFLG(String value) {
						this.depositnoticeflg = value;
					}

					public String getIsMultiRec() {
						return isMultiRec;
					}

					public void setIsMultiRec(String value) {
						this.isMultiRec = value;
					}

					public String getCifid() {
						return cifid;
					}

					public void setCifid(String cifid) {
						this.cifid = cifid;
					}

				}

				/**
				 * @return the chnlid
				 */
				public String getChnlid() {
					return chnlid;
				}

				/**
				 * @param chnlid the chnlid to set
				 */
				public void setChnlid(String chnlid) {
					this.chnlid = chnlid;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "custId", "tdAcctId", "tdAcctGenInfo", "initialDeposit", "depositTerm",
					"repayAcctId", "renewalDtls", "nomineeInfoRec", "trnDtls", "relPartyRec" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class TDAcctAddRq {

				@XmlElement(name = "CustId", required = true)
				protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.CustId custId;
				@XmlElement(name = "TDAcctId", required = true)
				protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TDAcctId tdAcctId;
				@XmlElement(name = "TDAcctGenInfo", required = true)
				protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TDAcctGenInfo tdAcctGenInfo;
				@XmlElement(name = "InitialDeposit", required = true)
				protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.InitialDeposit initialDeposit;
				@XmlElement(name = "DepositTerm", required = true)
				protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.DepositTerm depositTerm;
				@XmlElement(name = "RepayAcctId", required = true)
				protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RepayAcctId repayAcctId;
				@XmlElement(name = "RenewalDtls", required = true)
				protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RenewalDtls renewalDtls;
				@XmlElement(name = "NomineeInfoRec", required = true)
				protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec nomineeInfoRec;
				@XmlElement(name = "TrnDtls", required = true)
				protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TrnDtls trnDtls;
				@XmlElement(name = "RelPartyRec", required = true)
				protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RelPartyRec relPartyRec;

				public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.CustId getCustId() {
					return custId;
				}

				public void setCustId(TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.CustId value) {
					this.custId = value;
				}

				public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TDAcctId getTDAcctId() {
					return tdAcctId;
				}

				public void setTDAcctId(TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TDAcctId value) {
					this.tdAcctId = value;
				}

				public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TDAcctGenInfo getTDAcctGenInfo() {
					return tdAcctGenInfo;
				}

				public void setTDAcctGenInfo(
						TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TDAcctGenInfo value) {
					this.tdAcctGenInfo = value;
				}

				public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.InitialDeposit getInitialDeposit() {
					return initialDeposit;
				}

				public void setInitialDeposit(
						TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.InitialDeposit value) {
					this.initialDeposit = value;
				}

				public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.DepositTerm getDepositTerm() {
					return depositTerm;
				}

				public void setDepositTerm(TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.DepositTerm value) {
					this.depositTerm = value;
				}

				public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RepayAcctId getRepayAcctId() {
					return repayAcctId;
				}

				public void setRepayAcctId(TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RepayAcctId value) {
					this.repayAcctId = value;
				}

				public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RenewalDtls getRenewalDtls() {
					return renewalDtls;
				}

				public void setRenewalDtls(TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RenewalDtls value) {
					this.renewalDtls = value;
				}

				public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec getNomineeInfoRec() {
					return nomineeInfoRec;
				}

				public void setNomineeInfoRec(
						TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec value) {
					this.nomineeInfoRec = value;
				}

				public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TrnDtls getTrnDtls() {
					return trnDtls;
				}

				public void setTrnDtls(TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TrnDtls value) {
					this.trnDtls = value;
				}

				public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RelPartyRec getRelPartyRec() {
					return relPartyRec;
				}

				public void setRelPartyRec(TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RelPartyRec value) {
					this.relPartyRec = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "custId" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class CustId {

					@XmlElement(name = "CustId")
					protected String custId;

					public String getCustId() {
						return custId;
					}

					public void setCustId(String value) {
						this.custId = value;
					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "months", "days" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class DepositTerm {

					@XmlElement(name = "Months")
					protected String months;
					@XmlElement(name = "Days", required = true)
					protected String days;

					public String getMonths() {
						return months;
					}

					public void setMonths(String value) {
						this.months = value;
					}

					public String getDays() {
						return days;
					}

					public void setDays(String value) {
						this.days = value;
					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class InitialDeposit {

					protected String amountValue;
					@XmlElement(required = true)
					protected String currencyCode;

					public String getAmountValue() {
						return amountValue;
					}

					public void setAmountValue(String value) {
						this.amountValue = value;
					}

					public String getCurrencyCode() {
						return currencyCode;
					}

					public void setCurrencyCode(String value) {
						this.currencyCode = value;
					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "regNum", "nomineeName", "relType", "nomineeContactInfo",
						"nomineeMinorFlg", "nomineeBirthDt", "nomineePercent" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class NomineeInfoRec {

					@XmlElement(name = "RegNum")
					protected String regNum;
					@XmlElement(name = "NomineeName", required = true)
					protected String nomineeName;
					@XmlElement(name = "RelType")
					protected String relType;
					@XmlElement(name = "NomineeContactInfo", required = true)
					protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineeContactInfo nomineeContactInfo;
					@XmlElement(name = "NomineeMinorFlg", required = true)
					protected String nomineeMinorFlg;
					@XmlElement(name = "NomineeBirthDt", required = true)
					@XmlSchemaType(name = "dateTime")
					protected String nomineeBirthDt;
					@XmlElement(name = "NomineePercent", required = true)
					protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineePercent nomineePercent;

					public String getRegNum() {
						return regNum;
					}

					public void setRegNum(String value) {
						this.regNum = value;
					}

					public String getNomineeName() {
						return nomineeName;
					}

					public void setNomineeName(String value) {
						this.nomineeName = value;
					}

					public String getRelType() {
						return relType;
					}

					public void setRelType(String value) {
						this.relType = value;
					}

					public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineeContactInfo getNomineeContactInfo() {
						return nomineeContactInfo;
					}

					public void setNomineeContactInfo(
							TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineeContactInfo value) {
						this.nomineeContactInfo = value;
					}

					public String getNomineeMinorFlg() {
						return nomineeMinorFlg;
					}

					public void setNomineeMinorFlg(String value) {
						this.nomineeMinorFlg = value;
					}

					public String getNomineeBirthDt() {
						return nomineeBirthDt;
					}

					public void setNomineeBirthDt(String value) {
						this.nomineeBirthDt = value;
					}

					public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineePercent getNomineePercent() {
						return nomineePercent;
					}

					public void setNomineePercent(
							TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineePercent value) {
						this.nomineePercent = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "postAddr" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class NomineeContactInfo {

						@XmlElement(name = "PostAddr", required = true)
						protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineeContactInfo.PostAddr postAddr;

						public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineeContactInfo.PostAddr getPostAddr() {
							return postAddr;
						}

						public void setPostAddr(
								TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.NomineeInfoRec.NomineeContactInfo.PostAddr value) {
							this.postAddr = value;
						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "addr1", "addr2", "addr3", "city", "stateProv", "postalCode",
								"country" })
						@Builder
						@NoArgsConstructor
						@AllArgsConstructor
						public static class PostAddr {

							@XmlElement(name = "Addr1", required = true)
							protected String addr1;
							@XmlElement(name = "Addr2", required = true)
							protected String addr2;
							@XmlElement(name = "Addr3", required = true)
							protected String addr3;
							@XmlElement(name = "City")
							protected String city;
							@XmlElement(name = "StateProv")
							protected String stateProv;
							@XmlElement(name = "PostalCode")
							protected String postalCode;
							@XmlElement(name = "Country", required = true)
							protected String country;

							public String getAddr1() {
								return addr1;
							}

							public void setAddr1(String value) {
								this.addr1 = value;
							}

							public String getAddr2() {
								return addr2;
							}

							public void setAddr2(String value) {
								this.addr2 = value;
							}

							public String getAddr3() {
								return addr3;
							}

							public void setAddr3(String value) {
								this.addr3 = value;
							}

							public String getCity() {
								return city;
							}

							public void setCity(String value) {
								this.city = value;
							}

							public String getStateProv() {
								return stateProv;
							}

							public void setStateProv(String value) {
								this.stateProv = value;
							}

							public String getPostalCode() {
								return postalCode;
							}

							public void setPostalCode(String value) {
								this.postalCode = value;
							}

							public String getCountry() {
								return country;
							}

							public void setCountry(String value) {
								this.country = value;
							}

						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "value" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class NomineePercent {

						protected String value;

						public String getValue() {
							return value;
						}

						public void setValue(String value) {
							this.value = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "relPartyType", "relPartyCode", "custId" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class RelPartyRec {

					@XmlElement(name = "RelPartyType", required = true)
					protected String relPartyType;
					@XmlElement(name = "RelPartyCode")
					protected byte relPartyCode;
					@XmlElement(name = "CustId", required = true)
					protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RelPartyRec.CustId custId;

					public String getRelPartyType() {
						return relPartyType;
					}

					public void setRelPartyType(String value) {
						this.relPartyType = value;
					}

					public byte getRelPartyCode() {
						return relPartyCode;
					}

					public void setRelPartyCode(byte value) {
						this.relPartyCode = value;
					}

					public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RelPartyRec.CustId getCustId() {
						return custId;
					}

					public void setCustId(
							TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RelPartyRec.CustId value) {
						this.custId = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "custId" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class CustId {

						@XmlElement(name = "CustId")
						protected String custId;

						public String getCustId() {
							return custId;
						}

						public void setCustId(String value) {
							this.custId = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "autoCloseOnMaturityFlg", "autoRenewalflg", "renewalTerm",
						"renewalSchm", "genLedgerSubHead", "renewalOption", "renewalAmt", "renewalAddnlAmt" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class RenewalDtls {

					@XmlElement(name = "AutoCloseOnMaturityFlg", required = true)
					protected String autoCloseOnMaturityFlg;
					@XmlElement(name = "AutoRenewalflg", required = true)
					protected String autoRenewalflg;
					@XmlElement(name = "RenewalTerm", required = true)
					protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RenewalDtls.RenewalTerm renewalTerm;
					@XmlElement(name = "RenewalSchm", required = true)
					protected String renewalSchm;
					@XmlElement(name = "GenLedgerSubHead", required = true)
					protected String genLedgerSubHead;
					@XmlElement(name = "RenewalOption", required = true)
					protected String renewalOption;
					@XmlElement(name = "RenewalAmt", required = true)
					protected String renewalAmt;
					@XmlElement(name = "RenewalAddnlAmt", required = true)
					protected String renewalAddnlAmt;

					public String getAutoCloseOnMaturityFlg() {
						return autoCloseOnMaturityFlg;
					}

					public void setAutoCloseOnMaturityFlg(String value) {
						this.autoCloseOnMaturityFlg = value;
					}

					public String getAutoRenewalflg() {
						return autoRenewalflg;
					}

					public void setAutoRenewalflg(String value) {
						this.autoRenewalflg = value;
					}

					public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RenewalDtls.RenewalTerm getRenewalTerm() {
						return renewalTerm;
					}

					public void setRenewalTerm(
							TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.RenewalDtls.RenewalTerm value) {
						this.renewalTerm = value;
					}

					public String getRenewalSchm() {
						return renewalSchm;
					}

					public void setRenewalSchm(String value) {
						this.renewalSchm = value;
					}

					public String getGenLedgerSubHead() {
						return genLedgerSubHead;
					}

					public void setGenLedgerSubHead(String value) {
						this.genLedgerSubHead = value;
					}

					public String getRenewalOption() {
						return renewalOption;
					}

					public void setRenewalOption(String value) {
						this.renewalOption = value;
					}

					public String getRenewalAmt() {
						return renewalAmt;
					}

					public void setRenewalAmt(String value) {
						this.renewalAmt = value;
					}

					public String getRenewalAddnlAmt() {
						return renewalAddnlAmt;
					}

					public void setRenewalAddnlAmt(String value) {
						this.renewalAddnlAmt = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "days", "months" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class RenewalTerm {

						@XmlElement(name = "Days")
						protected String days;
						@XmlElement(name = "Months")
						protected String months;

						public String getDays() {
							return days;
						}

						public void setDays(String value) {
							this.days = value;
						}

						public String getMonths() {
							return months;
						}

						public void setMonths(String value) {
							this.months = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctId", "acctType", "bankInfo" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class RepayAcctId {

					@XmlElement(name = "AcctId")
					protected String acctId;
					@XmlElement(name = "AcctType", required = true)
					protected String acctType;
					@XmlElement(name = "BankInfo", required = true)
					protected String bankInfo;

					public String getAcctId() {
						return acctId;
					}

					public void setAcctId(String value) {
						this.acctId = value;
					}

					public String getAcctType() {
						return acctType;
					}

					public void setAcctType(String value) {
						this.acctType = value;
					}

					public String getBankInfo() {
						return bankInfo;
					}

					public void setBankInfo(String value) {
						this.bankInfo = value;
					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctStmtMode", "despatchMode" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class TDAcctGenInfo {

					@XmlElement(name = "AcctStmtMode", required = true)
					protected String acctStmtMode;
					@XmlElement(name = "DespatchMode", required = true)
					protected String despatchMode;

					public String getAcctStmtMode() {
						return acctStmtMode;
					}

					public void setAcctStmtMode(String value) {
						this.acctStmtMode = value;
					}

					public String getDespatchMode() {
						return despatchMode;
					}

					public void setDespatchMode(String value) {
						this.despatchMode = value;
					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctType", "acctCurr" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class TDAcctId {

					@XmlElement(name = "AcctType", required = true)
					protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TDAcctId.AcctType acctType;
					@XmlElement(name = "AcctCurr", required = true)
					protected String acctCurr;

					public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TDAcctId.AcctType getAcctType() {
						return acctType;
					}

					public void setAcctType(
							TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TDAcctId.AcctType value) {
						this.acctType = value;
					}

					public String getAcctCurr() {
						return acctCurr;
					}

					public void setAcctCurr(String value) {
						this.acctCurr = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "schmCode" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class AcctType {

						@XmlElement(name = "SchmCode", required = true)
						protected String schmCode;

						public String getSchmCode() {
							return schmCode;
						}

						public void setSchmCode(String value) {
							this.schmCode = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "trnType", "debitAcctId" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class TrnDtls {

					@XmlElement(name = "TrnType", required = true)
					protected String trnType;
					@XmlElement(name = "DebitAcctId", required = true)
					protected TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TrnDtls.DebitAcctId debitAcctId;

					public String getTrnType() {
						return trnType;
					}

					public void setTrnType(String value) {
						this.trnType = value;
					}

					public TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TrnDtls.DebitAcctId getDebitAcctId() {
						return debitAcctId;
					}

					public void setDebitAcctId(
							TdAcctAddRequestDto.Body.TDAcctAddRequest.TDAcctAddRq.TrnDtls.DebitAcctId value) {
						this.debitAcctId = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "acctId", "acctType", "bankInfo" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class DebitAcctId {

						@XmlElement(name = "AcctId")
						protected String acctId;
						@XmlElement(name = "AcctType", required = true)
						protected String acctType;
						@XmlElement(name = "BankInfo", required = true)
						protected String bankInfo;

						public String getAcctId() {
							return acctId;
						}

						public void setAcctId(String value) {
							this.acctId = value;
						}

						public String getAcctType() {
							return acctType;
						}

						public void setAcctType(String value) {
							this.acctType = value;
						}

						public String getBankInfo() {
							return bankInfo;
						}

						public void setBankInfo(String value) {
							this.bankInfo = value;
						}

					}

				}

			}

		}

	}

}
