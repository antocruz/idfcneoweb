package com.neo.aggregator.bank.sbm.core.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
public class SBAcctInquiryResponseDto {

	@XmlElement(name = "Header", required = true)
	protected SBAcctInquiryResponseDto.Header header;
	@XmlElement(name = "Body", required = true)
	protected SBAcctInquiryResponseDto.Body body;

	public SBAcctInquiryResponseDto.Header getHeader() {
		return header;
	}

	public void setHeader(SBAcctInquiryResponseDto.Header value) {
		this.header = value;
	}

	public SBAcctInquiryResponseDto.Body getBody() {
		return body;
	}

	public void setBody(SBAcctInquiryResponseDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "sbAcctInqResponse", "error" })
	public static class Body {

		@XmlElement(name = "SBAcctInqResponse", required = true)
		protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse sbAcctInqResponse;

		public SBAcctInquiryResponseDto.Body.SBAcctInqResponse getSBAcctInqResponse() {
			return sbAcctInqResponse;
		}

		public void setSBAcctInqResponse(SBAcctInquiryResponseDto.Body.SBAcctInqResponse value) {
			this.sbAcctInqResponse = value;
		}

		@XmlElement(name = "Error")
		protected Error error;

		public Error getError() {
			return error;
		}

		public void setError(Error value) {
			this.error = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class Error {

			@XmlElement(name = "FIBusinessException")
			protected SBAcctInquiryResponseDto.Body.Error.FIBusinessException fiBusinessException;

			@XmlElement(name = "FISystemException")
			protected SBAcctInquiryResponseDto.Body.Error.FIBusinessException fiSystemException;

			public SBAcctInquiryResponseDto.Body.Error.FIBusinessException getFiSystemException() {
				return fiSystemException;
			}

			public void setFiSystemException(
					SBAcctInquiryResponseDto.Body.Error.FIBusinessException fiSystemException) {
				this.fiSystemException = fiSystemException;
			}

			public SBAcctInquiryResponseDto.Body.Error.FIBusinessException getFIBusinessException() {
				return fiBusinessException;
			}

			public void setFIBusinessException(SBAcctInquiryResponseDto.Body.Error.FIBusinessException value) {
				this.fiBusinessException = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "errorDetail" })
			public static class FIBusinessException {

				@XmlElement(name = "ErrorDetail")
				protected SBAcctInquiryResponseDto.Body.Error.FIBusinessException.ErrorDetail errorDetail;

				public SBAcctInquiryResponseDto.Body.Error.FIBusinessException.ErrorDetail getErrorDetail() {
					return errorDetail;
				}

				public void setErrorDetail(SBAcctInquiryResponseDto.Body.Error.FIBusinessException.ErrorDetail value) {
					this.errorDetail = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "errorCode", "errorDesc", "errorType" })
				public static class ErrorDetail {

					@XmlElement(name = "ErrorCode")
					protected String errorCode;

					@XmlElement(name = "ErrorDesc")
					protected String errorDesc;

					@XmlElement(name = "ErrorType")
					protected String errorType;

					public String getErrorCode() {
						return errorCode;
					}

					public void setErrorCode(String value) {
						this.errorCode = value;
					}

					public String getErrorDesc() {
						return errorDesc;
					}

					public void setErrorDesc(String value) {
						this.errorDesc = value;
					}

					public String getErrorType() {
						return errorType;
					}

					public void setErrorType(String value) {
						this.errorType = value;
					}

				}

			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "sbAcctInqRs", "sbAcctInqCustomData" })
		public static class SBAcctInqResponse {

			@XmlElement(name = "SBAcctInqRs", required = true)
			protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs sbAcctInqRs;
			@XmlElement(name = "SBAcctInq_CustomData", required = true)
			protected String sbAcctInqCustomData;

			public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs getSBAcctInqRs() {
				return sbAcctInqRs;
			}

			public void setSBAcctInqRs(SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs value) {
				this.sbAcctInqRs = value;
			}

			public String getSBAcctInqCustomData() {
				return sbAcctInqCustomData;
			}

			public void setSBAcctInqCustomData(String value) {
				this.sbAcctInqCustomData = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "sbAcctId", "acctOpnDt", "modeOfOper", "custId", "sbAcctGenInfo",
					"acctBalCrDrInd", "acctBalAmt", "accrIntDrCrInd", "accrIntRate", "IntCalcFreq", "IntRateCode",
					"netIntDrCrInd", "netIntRate", "withHoldingTaxDtls", "nomineeInfoRec", "relPartyRec" })
			public static class SBAcctInqRs {

				@XmlElement(name = "SBAcctId", required = true)
				protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctId sbAcctId;
				@XmlElement(name = "AcctOpnDt", required = true)
				@XmlSchemaType(name = "dateTime")
				protected String acctOpnDt;
				@XmlElement(name = "ModeOfOper")
				protected String modeOfOper;
				@XmlElement(name = "CustId", required = true)
				protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.CustId custId;
				@XmlElement(name = "SBAcctGenInfo", required = true)
				protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctGenInfo sbAcctGenInfo;
				@XmlElement(name = "AcctBalCrDrInd", required = true)
				protected String acctBalCrDrInd;
				@XmlElement(name = "AcctBalAmt", required = true)
				protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.AcctBalAmt acctBalAmt;
				@XmlElement(name = "AccrIntDrCrInd", required = true)
				protected String accrIntDrCrInd;
				@XmlElement(name = "AccrIntRate", required = true)
				protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.AccrIntRate accrIntRate;
				@XmlElement(name = "IntCalcFreq", required = true)
				protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.IntCalcFreq IntCalcFreq;
				@XmlElement(name = "IntRateCode", required = true)
				protected String IntRateCode;
				@XmlElement(name = "NetIntDrCrInd", required = true)
				protected String netIntDrCrInd;
				@XmlElement(name = "NetIntRate", required = true)
				protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NetIntRate netIntRate;
				@XmlElement(name = "WithHoldingTaxDtls", required = true)
				protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.WithHoldingTaxDtls withHoldingTaxDtls;
				@XmlElement(name = "NomineeInfoRec", required = true)
				protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec nomineeInfoRec;
				@XmlElement(name = "RelPartyRec", required = true)
				protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec relPartyRec;

				public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctId getSBAcctId() {
					return sbAcctId;
				}

				public void setSBAcctId(SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctId value) {
					this.sbAcctId = value;
				}

				public String getAcctOpnDt() {
					return acctOpnDt;
				}

				public void setAcctOpnDt(String value) {
					this.acctOpnDt = value;
				}

				public String getModeOfOper() {
					return modeOfOper;
				}

				public void setModeOfOper(String value) {
					this.modeOfOper = value;
				}

				public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.CustId getCustId() {
					return custId;
				}

				public void setCustId(SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.CustId value) {
					this.custId = value;
				}

				public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctGenInfo getSBAcctGenInfo() {
					return sbAcctGenInfo;
				}

				public void setSBAcctGenInfo(
						SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctGenInfo value) {
					this.sbAcctGenInfo = value;
				}

				public String getAcctBalCrDrInd() {
					return acctBalCrDrInd;
				}

				public void setAcctBalCrDrInd(String value) {
					this.acctBalCrDrInd = value;
				}

				public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.AcctBalAmt getAcctBalAmt() {
					return acctBalAmt;
				}

				public void setAcctBalAmt(
						SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.AcctBalAmt value) {
					this.acctBalAmt = value;
				}

				public String getAccrIntDrCrInd() {
					return accrIntDrCrInd;
				}

				public void setAccrIntDrCrInd(String value) {
					this.accrIntDrCrInd = value;
				}

				public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.AccrIntRate getAccrIntRate() {
					return accrIntRate;
				}

				public void setAccrIntRate(
						SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.AccrIntRate value) {
					this.accrIntRate = value;
				}

				public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.IntCalcFreq getIntCalcFreq() {
					return IntCalcFreq;
				}

				public void setIntCalcFreq(
						SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.IntCalcFreq value) {
					this.IntCalcFreq = value;
				}

				public String getIntRateCode() {
					return IntRateCode;
				}

				public void setIntRateCode(String value) {
					this.IntRateCode = value;
				}

				public String getNetIntDrCrInd() {
					return netIntDrCrInd;
				}

				public void setNetIntDrCrInd(String value) {
					this.netIntDrCrInd = value;
				}

				public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NetIntRate getNetIntRate() {
					return netIntRate;
				}

				public void setNetIntRate(
						SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NetIntRate value) {
					this.netIntRate = value;
				}

				public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.WithHoldingTaxDtls getWithHoldingTaxDtls() {
					return withHoldingTaxDtls;
				}

				public void setWithHoldingTaxDtls(
						SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.WithHoldingTaxDtls value) {
					this.withHoldingTaxDtls = value;
				}

				public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec getNomineeInfoRec() {
					return nomineeInfoRec;
				}

				public void setNomineeInfoRec(
						SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec value) {
					this.nomineeInfoRec = value;
				}

				public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec getRelPartyRec() {
					return relPartyRec;
				}

				public void setRelPartyRec(
						SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec value) {
					this.relPartyRec = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "value" })
				public static class AccrIntRate {

					protected String value;

					public String getValue() {
						return value;
					}

					public void setValue(String value) {
						this.value = value;
					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
				public static class AcctBalAmt {

					protected String amountValue;
					@XmlElement(required = true)
					protected String currencyCode;

					public String getAmountValue() {
						return amountValue;
					}

					public void setAmountValue(String value) {
						this.amountValue = value;
					}

					public String getCurrencyCode() {
						return currencyCode;
					}

					public void setCurrencyCode(String value) {
						this.currencyCode = value;
					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "custId", "personName" })
				public static class CustId {

					@XmlElement(name = "CustId")
					protected String custId;
					@XmlElement(name = "PersonName", required = true)
					protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.CustId.PersonName personName;

					public String getCustId() {
						return custId;
					}

					public void setCustId(String value) {
						this.custId = value;
					}

					public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.CustId.PersonName getPersonName() {
						return personName;
					}

					public void setPersonName(
							SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.CustId.PersonName value) {
						this.personName = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "lastName", "firstName", "middleName", "name", "titlePrefix" })
					public static class PersonName {

						@XmlElement(name = "LastName", required = true)
						protected String lastName;
						@XmlElement(name = "FirstName", required = true)
						protected String firstName;
						@XmlElement(name = "MiddleName", required = true)
						protected String middleName;
						@XmlElement(name = "Name", required = true)
						protected String name;
						@XmlElement(name = "TitlePrefix", required = true)
						protected String titlePrefix;

						public String getLastName() {
							return lastName;
						}

						public void setLastName(String value) {
							this.lastName = value;
						}

						public String getFirstName() {
							return firstName;
						}

						public void setFirstName(String value) {
							this.firstName = value;
						}

						public String getMiddleName() {
							return middleName;
						}

						public void setMiddleName(String value) {
							this.middleName = value;
						}

						public String getName() {
							return name;
						}

						public void setName(String value) {
							this.name = value;
						}

						public String getTitlePrefix() {
							return titlePrefix;
						}

						public void setTitlePrefix(String value) {
							this.titlePrefix = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "cal", "type", "startDt", "weekDay", "weekNum", "holStat" })
				public static class IntCalcFreq {

					@XmlElement(name = "Cal", required = true)
					protected String cal;
					@XmlElement(name = "Type", required = true)
					protected String type;
					@XmlElement(name = "StartDt")
					protected String startDt;
					@XmlElement(name = "WeekDay")
					protected String weekDay;
					@XmlElement(name = "WeekNum", required = true)
					protected String weekNum;
					@XmlElement(name = "HolStat", required = true)
					protected String holStat;

					public String getCal() {
						return cal;
					}

					public void setCal(String value) {
						this.cal = value;
					}

					public String getType() {
						return type;
					}

					public void setType(String value) {
						this.type = value;
					}

					public String getStartDt() {
						return startDt;
					}

					public void setStartDt(String value) {
						this.startDt = value;
					}

					public String getWeekDay() {
						return weekDay;
					}

					public void setWeekDay(String value) {
						this.weekDay = value;
					}

					public String getWeekNum() {
						return weekNum;
					}

					public void setWeekNum(String value) {
						this.weekNum = value;
					}

					public String getHolStat() {
						return holStat;
					}

					public void setHolStat(String value) {
						this.holStat = value;
					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "value" })
				public static class NetIntRate {

					protected String value;

					public String getValue() {
						return value;
					}

					public void setValue(String value) {
						this.value = value;
					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "regNum", "nomineeName", "relType", "nomineeContactInfo",
						"nomineeMinorFlg", "nomineePercent", "guardianInfo", "recDelFlg" })
				public static class NomineeInfoRec {

					@XmlElement(name = "RegNum")
					protected String regNum;
					@XmlElement(name = "NomineeName", required = true)
					protected String nomineeName;
					@XmlElement(name = "RelType")
					protected String relType;
					@XmlElement(name = "NomineeContactInfo", required = true)
					protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.NomineeContactInfo nomineeContactInfo;
					@XmlElement(name = "NomineeMinorFlg", required = true)
					protected String nomineeMinorFlg;
					@XmlElement(name = "NomineePercent", required = true)
					protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.NomineePercent nomineePercent;
					@XmlElement(name = "GuardianInfo", required = true)
					protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.GuardianInfo guardianInfo;
					@XmlElement(name = "RecDelFlg", required = true)
					protected String recDelFlg;

					public String getRegNum() {
						return regNum;
					}

					public void setRegNum(String value) {
						this.regNum = value;
					}

					public String getNomineeName() {
						return nomineeName;
					}

					public void setNomineeName(String value) {
						this.nomineeName = value;
					}

					public String getRelType() {
						return relType;
					}

					public void setRelType(String value) {
						this.relType = value;
					}

					public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.NomineeContactInfo getNomineeContactInfo() {
						return nomineeContactInfo;
					}

					public void setNomineeContactInfo(
							SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.NomineeContactInfo value) {
						this.nomineeContactInfo = value;
					}

					public String getNomineeMinorFlg() {
						return nomineeMinorFlg;
					}

					public void setNomineeMinorFlg(String value) {
						this.nomineeMinorFlg = value;
					}

					public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.NomineePercent getNomineePercent() {
						return nomineePercent;
					}

					public void setNomineePercent(
							SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.NomineePercent value) {
						this.nomineePercent = value;
					}

					public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.GuardianInfo getGuardianInfo() {
						return guardianInfo;
					}

					public void setGuardianInfo(
							SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.GuardianInfo value) {
						this.guardianInfo = value;
					}

					public String getRecDelFlg() {
						return recDelFlg;
					}

					public void setRecDelFlg(String value) {
						this.recDelFlg = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "guardianCode", "guardianName", "guardianContactInfo" })
					public static class GuardianInfo {

						@XmlElement(name = "GuardianCode", required = true)
						protected String guardianCode;
						@XmlElement(name = "GuardianName", required = true)
						protected String guardianName;
						@XmlElement(name = "GuardianContactInfo", required = true)
						protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.GuardianInfo.GuardianContactInfo guardianContactInfo;

						public String getGuardianCode() {
							return guardianCode;
						}

						public void setGuardianCode(String value) {
							this.guardianCode = value;
						}

						public String getGuardianName() {
							return guardianName;
						}

						public void setGuardianName(String value) {
							this.guardianName = value;
						}

						public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.GuardianInfo.GuardianContactInfo getGuardianContactInfo() {
							return guardianContactInfo;
						}

						public void setGuardianContactInfo(
								SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.GuardianInfo.GuardianContactInfo value) {
							this.guardianContactInfo = value;
						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "phoneNum", "emailAddr", "postAddr" })
						public static class GuardianContactInfo {

							@XmlElement(name = "PhoneNum", required = true)
							protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.GuardianInfo.GuardianContactInfo.PhoneNum phoneNum;
							@XmlElement(name = "EmailAddr", required = true)
							protected String emailAddr;
							@XmlElement(name = "PostAddr", required = true)
							protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.GuardianInfo.GuardianContactInfo.PostAddr postAddr;

							public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.GuardianInfo.GuardianContactInfo.PhoneNum getPhoneNum() {
								return phoneNum;
							}

							public void setPhoneNum(
									SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.GuardianInfo.GuardianContactInfo.PhoneNum value) {
								this.phoneNum = value;
							}

							public String getEmailAddr() {
								return emailAddr;
							}

							public void setEmailAddr(String value) {
								this.emailAddr = value;
							}

							public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.GuardianInfo.GuardianContactInfo.PostAddr getPostAddr() {
								return postAddr;
							}

							public void setPostAddr(
									SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.GuardianInfo.GuardianContactInfo.PostAddr value) {
								this.postAddr = value;
							}

							@XmlAccessorType(XmlAccessType.FIELD)
							@XmlType(name = "", propOrder = { "telephoneNum", "faxNum", "telexNum" })
							public static class PhoneNum {

								@XmlElement(name = "TelephoneNum", required = true)
								protected String telephoneNum;
								@XmlElement(name = "FaxNum", required = true)
								protected String faxNum;
								@XmlElement(name = "TelexNum", required = true)
								protected String telexNum;

								public String getTelephoneNum() {
									return telephoneNum;
								}

								public void setTelephoneNum(String value) {
									this.telephoneNum = value;
								}

								public String getFaxNum() {
									return faxNum;
								}

								public void setFaxNum(String value) {
									this.faxNum = value;
								}

								public String getTelexNum() {
									return telexNum;
								}

								public void setTelexNum(String value) {
									this.telexNum = value;
								}

							}

							@XmlAccessorType(XmlAccessType.FIELD)
							@XmlType(name = "", propOrder = { "addr1", "addr2", "addr3", "city", "stateProv",
									"postalCode", "country", "addrType" })
							public static class PostAddr {

								@XmlElement(name = "Addr1", required = true)
								protected String addr1;
								@XmlElement(name = "Addr2", required = true)
								protected String addr2;
								@XmlElement(name = "Addr3", required = true)
								protected String addr3;
								@XmlElement(name = "City", required = true)
								protected String city;
								@XmlElement(name = "StateProv", required = true)
								protected String stateProv;
								@XmlElement(name = "PostalCode", required = true)
								protected String postalCode;
								@XmlElement(name = "Country", required = true)
								protected String country;
								@XmlElement(name = "AddrType", required = true)
								protected String addrType;

								public String getAddr1() {
									return addr1;
								}

								public void setAddr1(String value) {
									this.addr1 = value;
								}

								public String getAddr2() {
									return addr2;
								}

								public void setAddr2(String value) {
									this.addr2 = value;
								}

								public String getAddr3() {
									return addr3;
								}

								public void setAddr3(String value) {
									this.addr3 = value;
								}

								public String getCity() {
									return city;
								}

								public void setCity(String value) {
									this.city = value;
								}

								public String getStateProv() {
									return stateProv;
								}

								public void setStateProv(String value) {
									this.stateProv = value;
								}

								public String getPostalCode() {
									return postalCode;
								}

								public void setPostalCode(String value) {
									this.postalCode = value;
								}

								public String getCountry() {
									return country;
								}

								public void setCountry(String value) {
									this.country = value;
								}

								public String getAddrType() {
									return addrType;
								}

								public void setAddrType(String value) {
									this.addrType = value;
								}

							}

						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "phoneNum", "emailAddr", "postAddr" })
					public static class NomineeContactInfo {

						@XmlElement(name = "PhoneNum", required = true)
						protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.NomineeContactInfo.PhoneNum phoneNum;
						@XmlElement(name = "EmailAddr", required = true)
						protected String emailAddr;
						@XmlElement(name = "PostAddr", required = true)
						protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.NomineeContactInfo.PostAddr postAddr;

						public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.NomineeContactInfo.PhoneNum getPhoneNum() {
							return phoneNum;
						}

						public void setPhoneNum(
								SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.NomineeContactInfo.PhoneNum value) {
							this.phoneNum = value;
						}

						public String getEmailAddr() {
							return emailAddr;
						}

						public void setEmailAddr(String value) {
							this.emailAddr = value;
						}

						public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.NomineeContactInfo.PostAddr getPostAddr() {
							return postAddr;
						}

						public void setPostAddr(
								SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.NomineeInfoRec.NomineeContactInfo.PostAddr value) {
							this.postAddr = value;
						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "telephoneNum", "faxNum", "telexNum" })
						public static class PhoneNum {

							@XmlElement(name = "TelephoneNum", required = true)
							protected String telephoneNum;
							@XmlElement(name = "FaxNum", required = true)
							protected String faxNum;
							@XmlElement(name = "TelexNum", required = true)
							protected String telexNum;

							public String getTelephoneNum() {
								return telephoneNum;
							}

							public void setTelephoneNum(String value) {
								this.telephoneNum = value;
							}

							public String getFaxNum() {
								return faxNum;
							}

							public void setFaxNum(String value) {
								this.faxNum = value;
							}

							public String getTelexNum() {
								return telexNum;
							}

							public void setTelexNum(String value) {
								this.telexNum = value;
							}

						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "addr1", "addr2", "addr3", "city", "stateProv", "postalCode",
								"country", "addrType" })
						public static class PostAddr {

							@XmlElement(name = "Addr1", required = true)
							protected String addr1;
							@XmlElement(name = "Addr2", required = true)
							protected String addr2;
							@XmlElement(name = "Addr3", required = true)
							protected String addr3;
							@XmlElement(name = "City", required = true)
							protected String city;
							@XmlElement(name = "StateProv", required = true)
							protected String stateProv;
							@XmlElement(name = "PostalCode")
							protected String postalCode;
							@XmlElement(name = "Country", required = true)
							protected String country;
							@XmlElement(name = "AddrType", required = true)
							protected String addrType;

							public String getAddr1() {
								return addr1;
							}

							public void setAddr1(String value) {
								this.addr1 = value;
							}

							public String getAddr2() {
								return addr2;
							}

							public void setAddr2(String value) {
								this.addr2 = value;
							}

							public String getAddr3() {
								return addr3;
							}

							public void setAddr3(String value) {
								this.addr3 = value;
							}

							public String getCity() {
								return city;
							}

							public void setCity(String value) {
								this.city = value;
							}

							public String getStateProv() {
								return stateProv;
							}

							public void setStateProv(String value) {
								this.stateProv = value;
							}

							public String getPostalCode() {
								return postalCode;
							}

							public void setPostalCode(String value) {
								this.postalCode = value;
							}

							public String getCountry() {
								return country;
							}

							public void setCountry(String value) {
								this.country = value;
							}

							public String getAddrType() {
								return addrType;
							}

							public void setAddrType(String value) {
								this.addrType = value;
							}

						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "value" })
					public static class NomineePercent {

						protected String value;

						public String getValue() {
							return value;
						}

						public void setValue(String value) {
							this.value = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "relPartyType", "relPartyTypeDesc", "relPartyCode",
						"relPartyCodeDesc", "custId", "relPartyContactInfo", "recDelFlg" })
				public static class RelPartyRec {

					@XmlElement(name = "RelPartyType", required = true)
					protected String relPartyType;
					@XmlElement(name = "RelPartyTypeDesc", required = true)
					protected String relPartyTypeDesc;
					@XmlElement(name = "RelPartyCode", required = true)
					protected String relPartyCode;
					@XmlElement(name = "RelPartyCodeDesc", required = true)
					protected String relPartyCodeDesc;
					@XmlElement(name = "CustId", required = true)
					protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec.CustId custId;
					@XmlElement(name = "RelPartyContactInfo", required = true)
					protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec.RelPartyContactInfo relPartyContactInfo;
					@XmlElement(name = "RecDelFlg", required = true)
					protected String recDelFlg;

					public String getRelPartyType() {
						return relPartyType;
					}

					public void setRelPartyType(String value) {
						this.relPartyType = value;
					}

					public String getRelPartyTypeDesc() {
						return relPartyTypeDesc;
					}

					public void setRelPartyTypeDesc(String value) {
						this.relPartyTypeDesc = value;
					}

					public String getRelPartyCode() {
						return relPartyCode;
					}

					public void setRelPartyCode(String value) {
						this.relPartyCode = value;
					}

					public String getRelPartyCodeDesc() {
						return relPartyCodeDesc;
					}

					public void setRelPartyCodeDesc(String value) {
						this.relPartyCodeDesc = value;
					}

					public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec.CustId getCustId() {
						return custId;
					}

					public void setCustId(
							SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec.CustId value) {
						this.custId = value;
					}

					public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec.RelPartyContactInfo getRelPartyContactInfo() {
						return relPartyContactInfo;
					}

					public void setRelPartyContactInfo(
							SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec.RelPartyContactInfo value) {
						this.relPartyContactInfo = value;
					}

					public String getRecDelFlg() {
						return recDelFlg;
					}

					public void setRecDelFlg(String value) {
						this.recDelFlg = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "custId", "personName" })
					public static class CustId {

						@XmlElement(name = "CustId")
						protected String custId;
						@XmlElement(name = "PersonName", required = true)
						protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec.CustId.PersonName personName;

						public String getCustId() {
							return custId;
						}

						public void setCustId(String value) {
							this.custId = value;
						}

						public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec.CustId.PersonName getPersonName() {
							return personName;
						}

						public void setPersonName(
								SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec.CustId.PersonName value) {
							this.personName = value;
						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "lastName", "firstName", "middleName", "name",
								"titlePrefix" })
						public static class PersonName {

							@XmlElement(name = "LastName", required = true)
							protected String lastName;
							@XmlElement(name = "FirstName", required = true)
							protected String firstName;
							@XmlElement(name = "MiddleName", required = true)
							protected String middleName;
							@XmlElement(name = "Name", required = true)
							protected String name;
							@XmlElement(name = "TitlePrefix", required = true)
							protected String titlePrefix;

							public String getLastName() {
								return lastName;
							}

							public void setLastName(String value) {
								this.lastName = value;
							}

							public String getFirstName() {
								return firstName;
							}

							public void setFirstName(String value) {
								this.firstName = value;
							}

							public String getMiddleName() {
								return middleName;
							}

							public void setMiddleName(String value) {
								this.middleName = value;
							}

							public String getName() {
								return name;
							}

							public void setName(String value) {
								this.name = value;
							}

							public String getTitlePrefix() {
								return titlePrefix;
							}

							public void setTitlePrefix(String value) {
								this.titlePrefix = value;
							}

						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "phoneNum", "emailAddr", "postAddr" })
					public static class RelPartyContactInfo {

						@XmlElement(name = "PhoneNum", required = true)
						protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec.RelPartyContactInfo.PhoneNum phoneNum;
						@XmlElement(name = "EmailAddr", required = true)
						protected String emailAddr;
						@XmlElement(name = "PostAddr", required = true)
						protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec.RelPartyContactInfo.PostAddr postAddr;

						public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec.RelPartyContactInfo.PhoneNum getPhoneNum() {
							return phoneNum;
						}

						public void setPhoneNum(
								SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec.RelPartyContactInfo.PhoneNum value) {
							this.phoneNum = value;
						}

						public String getEmailAddr() {
							return emailAddr;
						}

						public void setEmailAddr(String value) {
							this.emailAddr = value;
						}

						public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec.RelPartyContactInfo.PostAddr getPostAddr() {
							return postAddr;
						}

						public void setPostAddr(
								SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.RelPartyRec.RelPartyContactInfo.PostAddr value) {
							this.postAddr = value;
						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "telephoneNum", "faxNum", "telexNum" })
						public static class PhoneNum {

							@XmlElement(name = "TelephoneNum", required = true)
							protected String telephoneNum;
							@XmlElement(name = "FaxNum", required = true)
							protected String faxNum;
							@XmlElement(name = "TelexNum", required = true)
							protected String telexNum;

							public String getTelephoneNum() {
								return telephoneNum;
							}

							public void setTelephoneNum(String value) {
								this.telephoneNum = value;
							}

							public String getFaxNum() {
								return faxNum;
							}

							public void setFaxNum(String value) {
								this.faxNum = value;
							}

							public String getTelexNum() {
								return telexNum;
							}

							public void setTelexNum(String value) {
								this.telexNum = value;
							}

						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "addr1", "addr2", "addr3", "city", "stateProv", "postalCode",
								"country", "addrType" })
						public static class PostAddr {

							@XmlElement(name = "Addr1", required = true)
							protected String addr1;
							@XmlElement(name = "Addr2", required = true)
							protected String addr2;
							@XmlElement(name = "Addr3", required = true)
							protected String addr3;
							@XmlElement(name = "City", required = true)
							protected String city;
							@XmlElement(name = "StateProv", required = true)
							protected String stateProv;
							@XmlElement(name = "PostalCode", required = true)
							protected String postalCode;
							@XmlElement(name = "Country", required = true)
							protected String country;
							@XmlElement(name = "AddrType", required = true)
							protected String addrType;

							public String getAddr1() {
								return addr1;
							}

							public void setAddr1(String value) {
								this.addr1 = value;
							}

							public String getAddr2() {
								return addr2;
							}

							public void setAddr2(String value) {
								this.addr2 = value;
							}

							public String getAddr3() {
								return addr3;
							}

							public void setAddr3(String value) {
								this.addr3 = value;
							}

							public String getCity() {
								return city;
							}

							public void setCity(String value) {
								this.city = value;
							}

							public String getStateProv() {
								return stateProv;
							}

							public void setStateProv(String value) {
								this.stateProv = value;
							}

							public String getPostalCode() {
								return postalCode;
							}

							public void setPostalCode(String value) {
								this.postalCode = value;
							}

							public String getCountry() {
								return country;
							}

							public void setCountry(String value) {
								this.country = value;
							}

							public String getAddrType() {
								return addrType;
							}

							public void setAddrType(String value) {
								this.addrType = value;
							}

						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "eotEnabled", "drStringMethodInd", "genLedgerSubHead", "acctName",
						"acctShortName", "acctStmtMode", "acctStmtFreq", "acctStmtNxtPrStringDt", "despatchMode" })
				public static class SBAcctGenInfo {

					@XmlElement(name = "EotEnabled", required = true)
					protected String eotEnabled;
					@XmlElement(name = "DrStringMethodInd", required = true)
					protected String drStringMethodInd;
					@XmlElement(name = "GenLedgerSubHead", required = true)
					protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctGenInfo.GenLedgerSubHead genLedgerSubHead;
					@XmlElement(name = "AcctName", required = true)
					protected String acctName;
					@XmlElement(name = "AcctShortName", required = true)
					protected String acctShortName;
					@XmlElement(name = "AcctStmtMode", required = true)
					protected String acctStmtMode;
					@XmlElement(name = "AcctStmtFreq", required = true)
					protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctGenInfo.AcctStmtFreq acctStmtFreq;
					@XmlElement(name = "AcctStmtNxtPrStringDt", required = true)
					@XmlSchemaType(name = "dateTime")
					protected String acctStmtNxtPrStringDt;
					@XmlElement(name = "DespatchMode", required = true)
					protected String despatchMode;

					public String getEotEnabled() {
						return eotEnabled;
					}

					public void setEotEnabled(String value) {
						this.eotEnabled = value;
					}

					public String getDrStringMethodInd() {
						return drStringMethodInd;
					}

					public void setDrStringMethodInd(String value) {
						this.drStringMethodInd = value;
					}

					public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctGenInfo.GenLedgerSubHead getGenLedgerSubHead() {
						return genLedgerSubHead;
					}

					public void setGenLedgerSubHead(
							SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctGenInfo.GenLedgerSubHead value) {
						this.genLedgerSubHead = value;
					}

					public String getAcctName() {
						return acctName;
					}

					public void setAcctName(String value) {
						this.acctName = value;
					}

					public String getAcctShortName() {
						return acctShortName;
					}

					public void setAcctShortName(String value) {
						this.acctShortName = value;
					}

					public String getAcctStmtMode() {
						return acctStmtMode;
					}

					public void setAcctStmtMode(String value) {
						this.acctStmtMode = value;
					}

					public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctGenInfo.AcctStmtFreq getAcctStmtFreq() {
						return acctStmtFreq;
					}

					public void setAcctStmtFreq(
							SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctGenInfo.AcctStmtFreq value) {
						this.acctStmtFreq = value;
					}

					public String getAcctStmtNxtPrStringDt() {
						return acctStmtNxtPrStringDt;
					}

					public void setAcctStmtNxtPrStringDt(String value) {
						this.acctStmtNxtPrStringDt = value;
					}

					public String getDespatchMode() {
						return despatchMode;
					}

					public void setDespatchMode(String value) {
						this.despatchMode = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "cal", "type", "startDt", "weekDay", "weekNum", "holStat" })
					public static class AcctStmtFreq {

						@XmlElement(name = "Cal")
						protected String cal;
						@XmlElement(name = "Type", required = true)
						protected String type;
						@XmlElement(name = "StartDt")
						protected String startDt;
						@XmlElement(name = "WeekDay")
						protected String weekDay;
						@XmlElement(name = "WeekNum", required = true)
						protected String weekNum;
						@XmlElement(name = "HolStat", required = true)
						protected String holStat;

						public String getCal() {
							return cal;
						}

						public void setCal(String value) {
							this.cal = value;
						}

						public String getType() {
							return type;
						}

						public void setType(String value) {
							this.type = value;
						}

						public String getStartDt() {
							return startDt;
						}

						public void setStartDt(String value) {
							this.startDt = value;
						}

						public String getWeekDay() {
							return weekDay;
						}

						public void setWeekDay(String value) {
							this.weekDay = value;
						}

						public String getWeekNum() {
							return weekNum;
						}

						public void setWeekNum(String value) {
							this.weekNum = value;
						}

						public String getHolStat() {
							return holStat;
						}

						public void setHolStat(String value) {
							this.holStat = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "genLedgerSubHeadCode", "curCode" })
					public static class GenLedgerSubHead {

						@XmlElement(name = "GenLedgerSubHeadCode")
						protected short genLedgerSubHeadCode;
						@XmlElement(name = "CurCode", required = true)
						protected String curCode;

						public short getGenLedgerSubHeadCode() {
							return genLedgerSubHeadCode;
						}

						public void setGenLedgerSubHeadCode(short value) {
							this.genLedgerSubHeadCode = value;
						}

						public String getCurCode() {
							return curCode;
						}

						public void setCurCode(String value) {
							this.curCode = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctId", "acctType", "acctCurr", "bankInfo" })
				public static class SBAcctId {

					@XmlElement(name = "AcctId")
					protected String acctId;
					@XmlElement(name = "AcctType", required = true)
					protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctId.AcctType acctType;
					@XmlElement(name = "AcctCurr", required = true)
					protected String acctCurr;
					@XmlElement(name = "BankInfo", required = true)
					protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctId.BankInfo bankInfo;

					public String getAcctId() {
						return acctId;
					}

					public void setAcctId(String value) {
						this.acctId = value;
					}

					public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctId.AcctType getAcctType() {
						return acctType;
					}

					public void setAcctType(
							SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctId.AcctType value) {
						this.acctType = value;
					}

					public String getAcctCurr() {
						return acctCurr;
					}

					public void setAcctCurr(String value) {
						this.acctCurr = value;
					}

					public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctId.BankInfo getBankInfo() {
						return bankInfo;
					}

					public void setBankInfo(
							SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctId.BankInfo value) {
						this.bankInfo = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "schmCode", "schmType" })
					public static class AcctType {

						@XmlElement(name = "SchmCode", required = true)
						protected String schmCode;
						@XmlElement(name = "SchmType", required = true)
						protected String schmType;

						public String getSchmCode() {
							return schmCode;
						}

						public void setSchmCode(String value) {
							this.schmCode = value;
						}

						public String getSchmType() {
							return schmType;
						}

						public void setSchmType(String value) {
							this.schmType = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "bankId", "name", "branchId", "branchName", "postAddr" })
					public static class BankInfo {

						@XmlElement(name = "BankId", required = true)
						protected String bankId;
						@XmlElement(name = "Name", required = true)
						protected String name;
						@XmlElement(name = "BranchId", required = true)
						protected String branchId;
						@XmlElement(name = "BranchName", required = true)
						protected String branchName;
						@XmlElement(name = "PostAddr", required = true)
						protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctId.BankInfo.PostAddr postAddr;

						public String getBankId() {
							return bankId;
						}

						public void setBankId(String value) {
							this.bankId = value;
						}

						public String getName() {
							return name;
						}

						public void setName(String value) {
							this.name = value;
						}

						public String getBranchId() {
							return branchId;
						}

						public void setBranchId(String value) {
							this.branchId = value;
						}

						public String getBranchName() {
							return branchName;
						}

						public void setBranchName(String value) {
							this.branchName = value;
						}

						public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctId.BankInfo.PostAddr getPostAddr() {
							return postAddr;
						}

						public void setPostAddr(
								SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.SBAcctId.BankInfo.PostAddr value) {
							this.postAddr = value;
						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "addr1", "addr2", "addr3", "city", "stateProv", "postalCode",
								"country", "addrType" })
						public static class PostAddr {

							@XmlElement(name = "Addr1", required = true)
							protected String addr1;
							@XmlElement(name = "Addr2", required = true)
							protected String addr2;
							@XmlElement(name = "Addr3", required = true)
							protected String addr3;
							@XmlElement(name = "City", required = true)
							protected String city;
							@XmlElement(name = "StateProv", required = true)
							protected String stateProv;
							@XmlElement(name = "PostalCode", required = true)
							protected String postalCode;
							@XmlElement(name = "Country", required = true)
							protected String country;
							@XmlElement(name = "AddrType", required = true)
							protected String addrType;

							public String getAddr1() {
								return addr1;
							}

							public void setAddr1(String value) {
								this.addr1 = value;
							}

							public String getAddr2() {
								return addr2;
							}

							public void setAddr2(String value) {
								this.addr2 = value;
							}

							public String getAddr3() {
								return addr3;
							}

							public void setAddr3(String value) {
								this.addr3 = value;
							}

							public String getCity() {
								return city;
							}

							public void setCity(String value) {
								this.city = value;
							}

							public String getStateProv() {
								return stateProv;
							}

							public void setStateProv(String value) {
								this.stateProv = value;
							}

							public String getPostalCode() {
								return postalCode;
							}

							public void setPostalCode(String value) {
								this.postalCode = value;
							}

							public String getCountry() {
								return country;
							}

							public void setCountry(String value) {
								this.country = value;
							}

							public String getAddrType() {
								return addrType;
							}

							public void setAddrType(String value) {
								this.addrType = value;
							}

						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "taxCategory", "floorLimitAmt", "withHoldingPercent" })
				public static class WithHoldingTaxDtls {

					@XmlElement(name = "TaxCategory", required = true)
					protected String taxCategory;
					@XmlElement(name = "FloorLimitAmt", required = true)
					protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.WithHoldingTaxDtls.FloorLimitAmt floorLimitAmt;
					@XmlElement(name = "WithHoldingPercent", required = true)
					protected SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.WithHoldingTaxDtls.WithHoldingPercent withHoldingPercent;

					public String getTaxCategory() {
						return taxCategory;
					}

					public void setTaxCategory(String value) {
						this.taxCategory = value;
					}

					public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.WithHoldingTaxDtls.FloorLimitAmt getFloorLimitAmt() {
						return floorLimitAmt;
					}

					public void setFloorLimitAmt(
							SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.WithHoldingTaxDtls.FloorLimitAmt value) {
						this.floorLimitAmt = value;
					}

					public SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.WithHoldingTaxDtls.WithHoldingPercent getWithHoldingPercent() {
						return withHoldingPercent;
					}

					public void setWithHoldingPercent(
							SBAcctInquiryResponseDto.Body.SBAcctInqResponse.SBAcctInqRs.WithHoldingTaxDtls.WithHoldingPercent value) {
						this.withHoldingPercent = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					public static class FloorLimitAmt {

						protected String amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public String getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(String value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "value" })
					public static class WithHoldingPercent {

						protected String value;

						public String getValue() {
							return value;
						}

						public void setValue(String value) {
							this.value = value;
						}

					}

				}

			}

		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "responseHeader" })
	public static class Header {

		@XmlElement(name = "ResponseHeader", required = true)
		protected SBAcctInquiryResponseDto.Header.ResponseHeader responseHeader;

		public SBAcctInquiryResponseDto.Header.ResponseHeader getResponseHeader() {
			return responseHeader;
		}

		public void setResponseHeader(SBAcctInquiryResponseDto.Header.ResponseHeader value) {
			this.responseHeader = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "requestMessageKey", "responseMessageInfo", "ubusTransaction",
				"hostTransaction", "hostParentTransaction", "customInfo" })
		public static class ResponseHeader {

			@XmlElement(name = "RequestMessageKey", required = true)
			protected SBAcctInquiryResponseDto.Header.ResponseHeader.RequestMessageKey requestMessageKey;
			@XmlElement(name = "ResponseMessageInfo", required = true)
			protected SBAcctInquiryResponseDto.Header.ResponseHeader.ResponseMessageInfo responseMessageInfo;
			@XmlElement(name = "UBUSTransaction", required = true)
			protected SBAcctInquiryResponseDto.Header.ResponseHeader.UBUSTransaction ubusTransaction;
			@XmlElement(name = "HostTransaction", required = true)
			protected SBAcctInquiryResponseDto.Header.ResponseHeader.HostTransaction hostTransaction;
			@XmlElement(name = "HostParentTransaction", required = true)
			protected SBAcctInquiryResponseDto.Header.ResponseHeader.HostParentTransaction hostParentTransaction;
			@XmlElement(name = "CustomInfo", required = true)
			protected String customInfo;

			public SBAcctInquiryResponseDto.Header.ResponseHeader.RequestMessageKey getRequestMessageKey() {
				return requestMessageKey;
			}

			public void setRequestMessageKey(SBAcctInquiryResponseDto.Header.ResponseHeader.RequestMessageKey value) {
				this.requestMessageKey = value;
			}

			public SBAcctInquiryResponseDto.Header.ResponseHeader.ResponseMessageInfo getResponseMessageInfo() {
				return responseMessageInfo;
			}

			public void setResponseMessageInfo(
					SBAcctInquiryResponseDto.Header.ResponseHeader.ResponseMessageInfo value) {
				this.responseMessageInfo = value;
			}

			public SBAcctInquiryResponseDto.Header.ResponseHeader.UBUSTransaction getUBUSTransaction() {
				return ubusTransaction;
			}

			public void setUBUSTransaction(SBAcctInquiryResponseDto.Header.ResponseHeader.UBUSTransaction value) {
				this.ubusTransaction = value;
			}

			public SBAcctInquiryResponseDto.Header.ResponseHeader.HostTransaction getHostTransaction() {
				return hostTransaction;
			}

			public void setHostTransaction(SBAcctInquiryResponseDto.Header.ResponseHeader.HostTransaction value) {
				this.hostTransaction = value;
			}

			public SBAcctInquiryResponseDto.Header.ResponseHeader.HostParentTransaction getHostParentTransaction() {
				return hostParentTransaction;
			}

			public void setHostParentTransaction(
					SBAcctInquiryResponseDto.Header.ResponseHeader.HostParentTransaction value) {
				this.hostParentTransaction = value;
			}

			public String getCustomInfo() {
				return customInfo;
			}

			public void setCustomInfo(String value) {
				this.customInfo = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostParentTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "requestUUID", "serviceRequestId", "serviceRequestVersion", "channelId" })
			public static class RequestMessageKey {

				@XmlElement(name = "RequestUUID", required = true)
				protected String requestUUID;
				@XmlElement(name = "ServiceRequestId", required = true)
				protected String serviceRequestId;
				@XmlElement(name = "ServiceRequestVersion")
				protected String serviceRequestVersion;
				@XmlElement(name = "ChannelId", required = true)
				protected String channelId;

				public String getRequestUUID() {
					return requestUUID;
				}

				public void setRequestUUID(String value) {
					this.requestUUID = value;
				}

				public String getServiceRequestId() {
					return serviceRequestId;
				}

				public void setServiceRequestId(String value) {
					this.serviceRequestId = value;
				}

				public String getServiceRequestVersion() {
					return serviceRequestVersion;
				}

				public void setServiceRequestVersion(String value) {
					this.serviceRequestVersion = value;
				}

				public String getChannelId() {
					return channelId;
				}

				public void setChannelId(String value) {
					this.channelId = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "bankId", "timeZone", "messageDateTime" })
			public static class ResponseMessageInfo {

				@XmlElement(name = "BankId", required = true)
				protected String bankId;
				@XmlElement(name = "TimeZone", required = true)
				protected String timeZone;
				@XmlElement(name = "MessageDateTime", required = true)
				@XmlSchemaType(name = "dateTime")
				protected String messageDateTime;

				public String getBankId() {
					return bankId;
				}

				public void setBankId(String value) {
					this.bankId = value;
				}

				public String getTimeZone() {
					return timeZone;
				}

				public void setTimeZone(String value) {
					this.timeZone = value;
				}

				public String getMessageDateTime() {
					return messageDateTime;
				}

				public void setMessageDateTime(String value) {
					this.messageDateTime = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class UBUSTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

		}

	}

}
