package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.Builder;

@Builder
public class PsychographMiscData {
	
	protected String countryOfIssue;
	protected String dtdt1;
	protected String strTxt10;
	protected String type;
	
	@XmlElement(name = "CountryOfIssue")
	public String getCountryOfIssue() {
		return countryOfIssue;
	}
	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}
	
	@XmlElement(name = "DTDt1")
	public String getDtdt1() {
		return dtdt1;
	}
	public void setDtdt1(String dtdt1) {
		this.dtdt1 = dtdt1;
	}
	
	@XmlElement(name = "StrText10")
	public String getStrTxt10() {
		return strTxt10;
	}
	public void setStrTxt10(String strTxt10) {
		this.strTxt10 = strTxt10;
	}
	
	@XmlElement(name = "Type")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
