package com.neo.aggregator.bank.sbm.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "Request_In")
public class RequestInDto {

	@XmlElement(name = "ChannelId", required = true)
	protected String channelId;
	@XmlElement(name = "PartnerReqID", required = true)
	protected String partnerReqID;
	@XmlElement(name = "Partner_UserName", required = true)
	protected String partnerUserName;
	@XmlElement(name = "Timestamp", required = true)
	protected String timestamp;
	@XmlElement(name = "ModuleID", required = true)
	protected String moduleID;
	@XmlElement(name = "AuthType", required = true)
	protected String authType;
	@XmlElement(name = "RequestType", required = true)
	protected String requestType;
	@XmlElement(name = "Request", required = true)
	protected String request;

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String value) {
		this.channelId = value;
	}

	public String getPartnerReqID() {
		return partnerReqID;
	}

	public void setPartnerReqID(String value) {
		this.partnerReqID = value;
	}

	public String getPartnerUserName() {
		return partnerUserName;
	}

	public void setPartnerUserName(String value) {
		this.partnerUserName = value;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String value) {
		this.timestamp = value;
	}

	public String getModuleID() {
		return moduleID;
	}

	public void setModuleID(String value) {
		this.moduleID = value;
	}

	public String getAuthType() {
		return authType;
	}

	public void setAuthType(String value) {
		this.authType = value;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String value) {
		this.requestType = value;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String value) {
		this.request = value;
	}

}
