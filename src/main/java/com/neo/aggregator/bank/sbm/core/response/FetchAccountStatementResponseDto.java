package com.neo.aggregator.bank.sbm.core.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
public class FetchAccountStatementResponseDto {

	@XmlElement(name = "Header", required = true)
	protected FetchAccountStatementResponseDto.Header header;
	@XmlElement(name = "Body", required = true)
	protected FetchAccountStatementResponseDto.Body body;

	public FetchAccountStatementResponseDto.Header getHeader() {
		return header;
	}

	public void setHeader(FetchAccountStatementResponseDto.Header value) {
		this.header = value;
	}

	public FetchAccountStatementResponseDto.Body getBody() {
		return body;
	}

	public void setBody(FetchAccountStatementResponseDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "getFullAccountStatementWithPaginationResponse", "error" })
	public static class Body {

		@XmlElement(required = true)
		protected FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse getFullAccountStatementWithPaginationResponse;

		public FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse getGetFullAccountStatementWithPaginationResponse() {
			return getFullAccountStatementWithPaginationResponse;
		}

		public void setGetFullAccountStatementWithPaginationResponse(
				FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse value) {
			this.getFullAccountStatementWithPaginationResponse = value;
		}

		@XmlElement(name = "Error")
		protected Error error;

		public Error getError() {
			return error;
		}

		public void setError(Error value) {
			this.error = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)

		@XmlType(name = "")
		public static class Error {

			@XmlElement(name = "FIBusinessException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiBusinessException;

			@XmlElement(name = "FISystemException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException;

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFiSystemException() {
				return fiSystemException;
			}

			public void setFiSystemException(RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException) {
				this.fiSystemException = fiSystemException;
			}

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFIBusinessException() {
				return fiBusinessException;
			}

			public void setFIBusinessException(RetCustAddResponseDto.Body.Error.FIBusinessException value) {
				this.fiBusinessException = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)

			@XmlType(name = "", propOrder = { "errorDetail" })
			public static class FIBusinessException {

				@XmlElement(name = "ErrorDetail")
				protected RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail errorDetail;

				public RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail getErrorDetail() {
					return errorDetail;
				}

				public void setErrorDetail(RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail value) {
					this.errorDetail = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)

				@XmlType(name = "", propOrder = { "errorCode", "errorDesc", "errorType" })
				public static class ErrorDetail {

					@XmlElement(name = "ErrorCode")
					protected String errorCode;

					@XmlElement(name = "ErrorDesc")
					protected String errorDesc;

					@XmlElement(name = "ErrorType")
					protected String errorType;

					public String getErrorCode() {
						return errorCode;
					}

					public void setErrorCode(String value) {
						this.errorCode = value;
					}

					public String getErrorDesc() {
						return errorDesc;
					}

					public void setErrorDesc(String value) {
						this.errorDesc = value;
					}

					public String getErrorType() {
						return errorType;
					}

					public void setErrorType(String value) {
						this.errorType = value;
					}

				}

			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "paginatedAccountStatement",
				"getFullAccountStatementWithPaginationCustomData" })
		public static class GetFullAccountStatementWithPaginationResponse {

			@XmlElement(name = "PaginatedAccountStatement", required = true)
			protected FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement paginatedAccountStatement;
			@XmlElement(name = "getFullAccountStatementWithPagination_CustomData", required = true)
			protected FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.GetFullAccountStatementWithPaginationCustomData getFullAccountStatementWithPaginationCustomData;

			public FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement getPaginatedAccountStatement() {
				return paginatedAccountStatement;
			}

			public void setPaginatedAccountStatement(
					FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement value) {
				this.paginatedAccountStatement = value;
			}

			public FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.GetFullAccountStatementWithPaginationCustomData getGetFullAccountStatementWithPaginationCustomData() {
				return getFullAccountStatementWithPaginationCustomData;
			}

			public void setGetFullAccountStatementWithPaginationCustomData(
					FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.GetFullAccountStatementWithPaginationCustomData value) {
				this.getFullAccountStatementWithPaginationCustomData = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "thb" })
			public static class GetFullAccountStatementWithPaginationCustomData {

				@XmlElement(name = "THB")
				protected byte thb;

				public byte getTHB() {
					return thb;
				}

				public void setTHB(byte value) {
					this.thb = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "accountBalances", "field125", "field126", "field127", "hasMoreData",
					"transactionDetails" })
			public static class PaginatedAccountStatement {

				@XmlElement(required = true)
				protected FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances accountBalances;
				@XmlElement(required = true)
				protected String field125;
				@XmlElement(required = true)
				protected String field126;
				@XmlElement(required = true)
				protected String field127;
				@XmlElement(required = true)
				protected String hasMoreData;
				protected List<FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.TransactionDetails> transactionDetails;

				public FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances getAccountBalances() {
					return accountBalances;
				}

				public void setAccountBalances(
						FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances value) {
					this.accountBalances = value;
				}

				public String getField125() {
					return field125;
				}

				public void setField125(String value) {
					this.field125 = value;
				}

				public String getField126() {
					return field126;
				}

				public void setField126(String value) {
					this.field126 = value;
				}

				public String getField127() {
					return field127;
				}

				public void setField127(String value) {
					this.field127 = value;
				}

				public String getHasMoreData() {
					return hasMoreData;
				}

				public void setHasMoreData(String value) {
					this.hasMoreData = value;
				}

				public List<FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.TransactionDetails> getTransactionDetails() {
					if (transactionDetails == null) {
						transactionDetails = new ArrayList<FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.TransactionDetails>();
					}
					return this.transactionDetails;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acid", "availableBalance", "branchId", "currencyCode", "ffdBalance",
						"floatingBalance", "ledgerBalance", "userDefinedBalance" })
				public static class AccountBalances {

					protected String acid;
					@XmlElement(required = true)
					protected FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances.AvailableBalance availableBalance;
					protected String branchId;
					@XmlElement(required = true)
					protected String currencyCode;
					@XmlElement(name = "fFDBalance", required = true)
					protected FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FFDBalance ffdBalance;
					@XmlElement(required = true)
					protected FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FloatingBalance floatingBalance;
					@XmlElement(required = true)
					protected FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances.LedgerBalance ledgerBalance;
					@XmlElement(required = true)
					protected FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances.UserDefinedBalance userDefinedBalance;

					public String getAcid() {
						return acid;
					}

					public void setAcid(String value) {
						this.acid = value;
					}

					public FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances.AvailableBalance getAvailableBalance() {
						return availableBalance;
					}

					public void setAvailableBalance(
							FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances.AvailableBalance value) {
						this.availableBalance = value;
					}

					public String getBranchId() {
						return branchId;
					}

					public void setBranchId(String value) {
						this.branchId = value;
					}

					public String getCurrencyCode() {
						return currencyCode;
					}

					public void setCurrencyCode(String value) {
						this.currencyCode = value;
					}

					public FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FFDBalance getFFDBalance() {
						return ffdBalance;
					}

					public void setFFDBalance(
							FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FFDBalance value) {
						this.ffdBalance = value;
					}

					public FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FloatingBalance getFloatingBalance() {
						return floatingBalance;
					}

					public void setFloatingBalance(
							FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FloatingBalance value) {
						this.floatingBalance = value;
					}

					public FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances.LedgerBalance getLedgerBalance() {
						return ledgerBalance;
					}

					public void setLedgerBalance(
							FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances.LedgerBalance value) {
						this.ledgerBalance = value;
					}

					public FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances.UserDefinedBalance getUserDefinedBalance() {
						return userDefinedBalance;
					}

					public void setUserDefinedBalance(
							FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.AccountBalances.UserDefinedBalance value) {
						this.userDefinedBalance = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					public static class AvailableBalance {

						protected String amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public String getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(String value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					public static class FFDBalance {

						protected float amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public float getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(float value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					public static class FloatingBalance {

						protected float amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public float getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(float value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					public static class LedgerBalance {

						protected String amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public String getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(String value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					public static class UserDefinedBalance {

						protected float amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public float getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(float value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "pstdDate", "transactionSummary", "txnBalance", "txnCat", "txnId",
						"txnSrlNo", "valueDate" })
				public static class TransactionDetails {

					@XmlElement(required = true)
					protected String pstdDate;
					@XmlElement(required = true)
					protected FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary transactionSummary;
					@XmlElement(required = true)
					protected FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TxnBalance txnBalance;
					@XmlElement(required = true)
					protected String txnCat;
					@XmlElement(required = true)
					protected String txnId;
					protected String txnSrlNo;
					@XmlElement(required = true)
					protected String valueDate;

					public String getPstdDate() {
						return pstdDate;
					}

					public void setPstdDate(String value) {
						this.pstdDate = value;
					}

					public FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary getTransactionSummary() {
						return transactionSummary;
					}

					public void setTransactionSummary(
							FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary value) {
						this.transactionSummary = value;
					}

					public FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TxnBalance getTxnBalance() {
						return txnBalance;
					}

					public void setTxnBalance(
							FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TxnBalance value) {
						this.txnBalance = value;
					}

					public String getTxnCat() {
						return txnCat;
					}

					public void setTxnCat(String value) {
						this.txnCat = value;
					}

					public String getTxnId() {
						return txnId;
					}

					public void setTxnId(String value) {
						this.txnId = value;
					}

					public String getTxnSrlNo() {
						return txnSrlNo;
					}

					public void setTxnSrlNo(String value) {
						this.txnSrlNo = value;
					}

					public String getValueDate() {
						return valueDate;
					}

					public void setValueDate(String value) {
						this.valueDate = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "instrumentId", "txnAmt", "txnDate", "txnDesc", "txnType",
							"addtnlData" })
					public static class TransactionSummary {

						@XmlElement(required = true)
						protected String instrumentId;
						@XmlElement(required = true)
						protected FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary.TxnAmt txnAmt;
						@XmlElement(required = true)
						@XmlSchemaType(name = "dateTime")
						protected XMLGregorianCalendar txnDate;
						@XmlElement(required = true)
						protected String txnDesc;
						@XmlElement(required = true)
						protected String txnType;
						@XmlElement(required = true)
						protected String addtnlData;

						public String getInstrumentId() {
							return instrumentId;
						}

						public void setInstrumentId(String value) {
							this.instrumentId = value;
						}

						public FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary.TxnAmt getTxnAmt() {
							return txnAmt;
						}

						public void setTxnAmt(
								FetchAccountStatementResponseDto.Body.GetFullAccountStatementWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary.TxnAmt value) {
							this.txnAmt = value;
						}

						public XMLGregorianCalendar getTxnDate() {
							return txnDate;
						}

						public void setTxnDate(XMLGregorianCalendar value) {
							this.txnDate = value;
						}

						public String getTxnDesc() {
							return txnDesc;
						}

						public void setTxnDesc(String value) {
							this.txnDesc = value;
						}

						public String getTxnType() {
							return txnType;
						}

						public void setTxnType(String value) {
							this.txnType = value;
						}

						public String getAddtnlData() {
							return addtnlData;
						}

						public void setAddtnlData(String value) {
							this.addtnlData = value;
						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
						public static class TxnAmt {

							protected String amountValue;
							@XmlElement(required = true)
							protected String currencyCode;

							public String getAmountValue() {
								return amountValue;
							}

							public void setAmountValue(String value) {
								this.amountValue = value;
							}

							public String getCurrencyCode() {
								return currencyCode;
							}

							public void setCurrencyCode(String value) {
								this.currencyCode = value;
							}

						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					public static class TxnBalance {

						protected String amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public String getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(String value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

				}

			}

		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "responseHeader" })
	public static class Header {

		@XmlElement(name = "ResponseHeader", required = true)
		protected FetchAccountStatementResponseDto.Header.ResponseHeader responseHeader;

		public FetchAccountStatementResponseDto.Header.ResponseHeader getResponseHeader() {
			return responseHeader;
		}

		public void setResponseHeader(FetchAccountStatementResponseDto.Header.ResponseHeader value) {
			this.responseHeader = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "requestMessageKey", "responseMessageInfo", "ubusTransaction",
				"hostTransaction", "hostParentTransaction", "customInfo" })
		public static class ResponseHeader {

			@XmlElement(name = "RequestMessageKey", required = true)
			protected FetchAccountStatementResponseDto.Header.ResponseHeader.RequestMessageKey requestMessageKey;
			@XmlElement(name = "ResponseMessageInfo", required = true)
			protected FetchAccountStatementResponseDto.Header.ResponseHeader.ResponseMessageInfo responseMessageInfo;
			@XmlElement(name = "UBUSTransaction", required = true)
			protected FetchAccountStatementResponseDto.Header.ResponseHeader.UBUSTransaction ubusTransaction;
			@XmlElement(name = "HostTransaction", required = true)
			protected FetchAccountStatementResponseDto.Header.ResponseHeader.HostTransaction hostTransaction;
			@XmlElement(name = "HostParentTransaction", required = true)
			protected FetchAccountStatementResponseDto.Header.ResponseHeader.HostParentTransaction hostParentTransaction;
			@XmlElement(name = "CustomInfo", required = true)
			protected String customInfo;

			public FetchAccountStatementResponseDto.Header.ResponseHeader.RequestMessageKey getRequestMessageKey() {
				return requestMessageKey;
			}

			public void setRequestMessageKey(
					FetchAccountStatementResponseDto.Header.ResponseHeader.RequestMessageKey value) {
				this.requestMessageKey = value;
			}

			public FetchAccountStatementResponseDto.Header.ResponseHeader.ResponseMessageInfo getResponseMessageInfo() {
				return responseMessageInfo;
			}

			public void setResponseMessageInfo(
					FetchAccountStatementResponseDto.Header.ResponseHeader.ResponseMessageInfo value) {
				this.responseMessageInfo = value;
			}

			public FetchAccountStatementResponseDto.Header.ResponseHeader.UBUSTransaction getUBUSTransaction() {
				return ubusTransaction;
			}

			public void setUBUSTransaction(
					FetchAccountStatementResponseDto.Header.ResponseHeader.UBUSTransaction value) {
				this.ubusTransaction = value;
			}

			public FetchAccountStatementResponseDto.Header.ResponseHeader.HostTransaction getHostTransaction() {
				return hostTransaction;
			}

			public void setHostTransaction(
					FetchAccountStatementResponseDto.Header.ResponseHeader.HostTransaction value) {
				this.hostTransaction = value;
			}

			public FetchAccountStatementResponseDto.Header.ResponseHeader.HostParentTransaction getHostParentTransaction() {
				return hostParentTransaction;
			}

			public void setHostParentTransaction(
					FetchAccountStatementResponseDto.Header.ResponseHeader.HostParentTransaction value) {
				this.hostParentTransaction = value;
			}

			public String getCustomInfo() {
				return customInfo;
			}

			public void setCustomInfo(String value) {
				this.customInfo = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostParentTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "requestUUID", "serviceRequestId", "serviceRequestVersion", "channelId" })
			public static class RequestMessageKey {

				@XmlElement(name = "RequestUUID", required = true)
				protected String requestUUID;
				@XmlElement(name = "ServiceRequestId", required = true)
				protected String serviceRequestId;
				@XmlElement(name = "ServiceRequestVersion")
				protected float serviceRequestVersion;
				@XmlElement(name = "ChannelId", required = true)
				protected String channelId;

				public String getRequestUUID() {
					return requestUUID;
				}

				public void setRequestUUID(String value) {
					this.requestUUID = value;
				}

				public String getServiceRequestId() {
					return serviceRequestId;
				}

				public void setServiceRequestId(String value) {
					this.serviceRequestId = value;
				}

				public float getServiceRequestVersion() {
					return serviceRequestVersion;
				}

				public void setServiceRequestVersion(float value) {
					this.serviceRequestVersion = value;
				}

				public String getChannelId() {
					return channelId;
				}

				public void setChannelId(String value) {
					this.channelId = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "bankId", "timeZone", "messageDateTime" })
			public static class ResponseMessageInfo {

				@XmlElement(name = "BankId", required = true)
				protected String bankId;
				@XmlElement(name = "TimeZone", required = true)
				protected String timeZone;
				@XmlElement(name = "MessageDateTime", required = true)
				@XmlSchemaType(name = "dateTime")
				protected XMLGregorianCalendar messageDateTime;

				public String getBankId() {
					return bankId;
				}

				public void setBankId(String value) {
					this.bankId = value;
				}

				public String getTimeZone() {
					return timeZone;
				}

				public void setTimeZone(String value) {
					this.timeZone = value;
				}

				public XMLGregorianCalendar getMessageDateTime() {
					return messageDateTime;
				}

				public void setMessageDateTime(XMLGregorianCalendar value) {
					this.messageDateTime = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class UBUSTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

		}

	}

}

