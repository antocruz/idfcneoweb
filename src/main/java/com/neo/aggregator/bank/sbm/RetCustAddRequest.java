package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.Builder;

@Builder
public class RetCustAddRequest {
	
	protected RetCustAddRq retCustAddRq;

	@XmlElement(name = "RetCustAddRq")
	public RetCustAddRq getRetCustAddRq() {
		return retCustAddRq;
	}

	public void setRetCustAddRq(RetCustAddRq retCustAddRq) {
		this.retCustAddRq = retCustAddRq;
	}
	
}
