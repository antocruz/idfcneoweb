package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.Builder;

@Builder
public class Security {

	protected Token token;
    protected String ficertToken;
    protected String realUserLoginSessionId;
    protected String realUser;
    protected String realUserPwd;
    protected String ssoTransferToken;
    
    @XmlElement(name = "Token")
    public Token getToken() {
		return token;
	}
	public void setToken(Token token) {
		this.token = token;
	}
	@XmlElement(name = "FICertToken")
	public String getFicertToken() {
		return ficertToken;
	}
	public void setFicertToken(String ficertToken) {
		this.ficertToken = ficertToken;
	}
	
	@XmlElement(name = "RealUserLoginSessionId")
	public String getRealUserLoginSessionId() {
		return realUserLoginSessionId;
	}
	public void setRealUserLoginSessionId(String realUserLoginSessionId) {
		this.realUserLoginSessionId = realUserLoginSessionId;
	}
	
	@XmlElement(name = "RealUser")
	public String getRealUser() {
		return realUser;
	}
	public void setRealUser(String realUser) {
		this.realUser = realUser;
	}
	
	@XmlElement(name = "RealUserPwd")
	public String getRealUserPwd() {
		return realUserPwd;
	}
	public void setRealUserPwd(String realUserPwd) {
		this.realUserPwd = realUserPwd;
	}
	
	@XmlElement(name = "SSOTransferToken")
	public String getSsoTransferToken() {
		return ssoTransferToken;
	}
	public void setSsoTransferToken(String ssoTransferToken) {
		this.ssoTransferToken = ssoTransferToken;
	}
	
}
