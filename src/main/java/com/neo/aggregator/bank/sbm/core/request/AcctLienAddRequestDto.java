package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AcctLienAddRequestDto {

	@XmlElement(name = "Header")
	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body", required = true)
	protected AcctLienAddRequestDto.Body body;

	public AcctLienAddRequestDto.Body getBody() {
		return body;
	}

	public void setBody(AcctLienAddRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "acctLienAddRequest" })
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Body {

		@XmlElement(name = "AcctLienAddRequest", required = true)
		protected AcctLienAddRequestDto.Body.AcctLienAddRequest acctLienAddRequest;

		public AcctLienAddRequestDto.Body.AcctLienAddRequest getAcctLienAddRequest() {
			return acctLienAddRequest;
		}

		public void setAcctLienAddRequest(AcctLienAddRequestDto.Body.AcctLienAddRequest value) {
			this.acctLienAddRequest = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "acctLienAddRq" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class AcctLienAddRequest {

			@XmlElement(name = "AcctLienAddRq", required = true)
			protected AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq acctLienAddRq;

			public AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq getAcctLienAddRq() {
				return acctLienAddRq;
			}

			public void setAcctLienAddRq(AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq value) {
				this.acctLienAddRq = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "acctId", "moduleType", "lienDtls" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class AcctLienAddRq {

				@XmlElement(name = "AcctId", required = true)
				protected AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.AcctId acctId;
				@XmlElement(name = "ModuleType", required = true)
				protected String moduleType;
				@XmlElement(name = "LienDtls", required = true)
				protected AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.LienDtls lienDtls;

				public AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.AcctId getAcctId() {
					return acctId;
				}

				public void setAcctId(AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.AcctId value) {
					this.acctId = value;
				}

				public String getModuleType() {
					return moduleType;
				}

				public void setModuleType(String value) {
					this.moduleType = value;
				}

				public AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.LienDtls getLienDtls() {
					return lienDtls;
				}

				public void setLienDtls(AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.LienDtls value) {
					this.lienDtls = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctId" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class AcctId {

					@XmlElement(name = "AcctId")
					protected String acctId;

					public String getAcctId() {
						return acctId;
					}

					public void setAcctId(String value) {
						this.acctId = value;
					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "newLienAmt", "lienDt", "reasonCode", "rmks" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class LienDtls {

					@XmlElement(name = "NewLienAmt", required = true)
					protected AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.LienDtls.NewLienAmt newLienAmt;
					@XmlElement(name = "LienDt", required = true)
					protected AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.LienDtls.LienDt lienDt;
					@XmlElement(name = "ReasonCode")
					protected String reasonCode;
					@XmlElement(name = "Rmks", required = true)
					protected String rmks;

					public AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.LienDtls.NewLienAmt getNewLienAmt() {
						return newLienAmt;
					}

					public void setNewLienAmt(
							AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.LienDtls.NewLienAmt value) {
						this.newLienAmt = value;
					}

					public AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.LienDtls.LienDt getLienDt() {
						return lienDt;
					}

					public void setLienDt(
							AcctLienAddRequestDto.Body.AcctLienAddRequest.AcctLienAddRq.LienDtls.LienDt value) {
						this.lienDt = value;
					}

					public String getReasonCode() {
						return reasonCode;
					}

					public void setReasonCode(String value) {
						this.reasonCode = value;
					}

					public String getRmks() {
						return rmks;
					}

					public void setRmks(String value) {
						this.rmks = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "startDt", "endDt" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class LienDt {

						@XmlElement(name = "StartDt", required = true)
						@XmlSchemaType(name = "dateTime")
						protected String startDt;
						@XmlElement(name = "EndDt", required = true)
						@XmlSchemaType(name = "dateTime")
						protected String endDt;

						public String getStartDt() {
							return startDt;
						}

						public void setStartDt(String value) {
							this.startDt = value;
						}

						public String getEndDt() {
							return endDt;
						}

						public void setEndDt(String value) {
							this.endDt = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class NewLienAmt {

						protected String amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public String getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(String value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

				}

			}

		}

	}

}
