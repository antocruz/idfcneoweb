package com.neo.aggregator.bank.sbm.core.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
public class RetCustAddResponseDto {

	@XmlElement(name = "Header")
	protected RetCustAddResponseDto.Header header;
	@XmlElement(name = "Body")
	protected RetCustAddResponseDto.Body body;

	public RetCustAddResponseDto.Header getHeader() {
		return header;
	}

	public void setHeader(RetCustAddResponseDto.Header value) {
		this.header = value;
	}

	public RetCustAddResponseDto.Body getBody() {
		return body;
	}

	public void setBody(RetCustAddResponseDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "retCustAddResponse", "error" })
	public static class Body {

		@XmlElement(name = "RetCustAddResponse")
		protected RetCustAddResponseDto.Body.RetCustAddResponse retCustAddResponse;
		@XmlElement(name = "Error")
		protected Error error;

		public RetCustAddResponseDto.Body.RetCustAddResponse getRetCustAddResponse() {
			return retCustAddResponse;
		}

		public void setRetCustAddResponse(RetCustAddResponseDto.Body.RetCustAddResponse value) {
			this.retCustAddResponse = value;
		}

		public Error getError() {
			return error;
		}

		public void setError(Error value) {
			this.error = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)

		@XmlType(name = "")
		public static class Error {

			@XmlElement(name = "FIBusinessException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiBusinessException;

			@XmlElement(name = "FISystemException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException;

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFiSystemException() {
				return fiSystemException;
			}

			public void setFiSystemException(RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException) {
				this.fiSystemException = fiSystemException;
			}

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFIBusinessException() {
				return fiBusinessException;
			}

			public void setFIBusinessException(RetCustAddResponseDto.Body.Error.FIBusinessException value) {
				this.fiBusinessException = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)

			@XmlType(name = "", propOrder = { "errorDetail" })
			public static class FIBusinessException {

				@XmlElement(name = "ErrorDetail")
				protected RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail errorDetail;

				public RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail getErrorDetail() {
					return errorDetail;
				}

				public void setErrorDetail(RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail value) {
					this.errorDetail = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)

				@XmlType(name = "", propOrder = { "errorCode", "errorDesc", "errorType" })
				public static class ErrorDetail {

					@XmlElement(name = "ErrorCode")
					protected String errorCode;

					@XmlElement(name = "ErrorDesc")
					protected String errorDesc;

					@XmlElement(name = "ErrorType")
					protected String errorType;

					public String getErrorCode() {
						return errorCode;
					}

					public void setErrorCode(String value) {
						this.errorCode = value;
					}

					public String getErrorDesc() {
						return errorDesc;
					}

					public void setErrorDesc(String value) {
						this.errorDesc = value;
					}

					public String getErrorType() {
						return errorType;
					}

					public void setErrorType(String value) {
						this.errorType = value;
					}

				}

			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "retCustAddRs", "retCustAddCustomData" })
		public static class RetCustAddResponse {

			@XmlElement(name = "RetCustAddRs")
			protected RetCustAddResponseDto.Body.RetCustAddResponse.RetCustAddRs retCustAddRs;
			@XmlElement(name = "RetCustAdd_CustomData")
			protected String retCustAddCustomData;

			public RetCustAddResponseDto.Body.RetCustAddResponse.RetCustAddRs getRetCustAddRs() {
				return retCustAddRs;
			}

			public void setRetCustAddRs(RetCustAddResponseDto.Body.RetCustAddResponse.RetCustAddRs value) {
				this.retCustAddRs = value;
			}

			public String getRetCustAddCustomData() {
				return retCustAddCustomData;
			}

			public void setRetCustAddCustomData(String value) {
				this.retCustAddCustomData = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "custId", "desc", "entity", "service", "status" })
			public static class RetCustAddRs {

				@XmlElement(name = "CustId")
				protected String custId;
				@XmlElement(name = "Desc")
				protected String desc;
				@XmlElement(name = "Entity")
				protected String entity;
				@XmlElement(name = "Service")
				protected String service;
				@XmlElement(name = "Status")
				protected String status;

				public String getCustId() {
					return custId;
				}

				public void setCustId(String value) {
					this.custId = value;
				}

				public String getDesc() {
					return desc;
				}

				public void setDesc(String value) {
					this.desc = value;
				}

				public String getEntity() {
					return entity;
				}

				public void setEntity(String value) {
					this.entity = value;
				}

				public String getService() {
					return service;
				}

				public void setService(String value) {
					this.service = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "responseHeader" })
	public static class Header {

		@XmlElement(name = "ResponseHeader")
		protected RetCustAddResponseDto.Header.ResponseHeader responseHeader;

		public RetCustAddResponseDto.Header.ResponseHeader getResponseHeader() {
			return responseHeader;
		}

		public void setResponseHeader(RetCustAddResponseDto.Header.ResponseHeader value) {
			this.responseHeader = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "requestMessageKey", "responseMessageInfo", "ubusTransaction",
				"hostTransaction", "hostParentTransaction", "customInfo" })
		public static class ResponseHeader {

			@XmlElement(name = "RequestMessageKey")
			protected RetCustAddResponseDto.Header.ResponseHeader.RequestMessageKey requestMessageKey;
			@XmlElement(name = "ResponseMessageInfo")
			protected RetCustAddResponseDto.Header.ResponseHeader.ResponseMessageInfo responseMessageInfo;
			@XmlElement(name = "UBUSTransaction")
			protected RetCustAddResponseDto.Header.ResponseHeader.UBUSTransaction ubusTransaction;
			@XmlElement(name = "HostTransaction")
			protected RetCustAddResponseDto.Header.ResponseHeader.HostTransaction hostTransaction;
			@XmlElement(name = "HostParentTransaction")
			protected RetCustAddResponseDto.Header.ResponseHeader.HostParentTransaction hostParentTransaction;
			@XmlElement(name = "CustomInfo")
			protected String customInfo;

			public RetCustAddResponseDto.Header.ResponseHeader.RequestMessageKey getRequestMessageKey() {
				return requestMessageKey;
			}

			public void setRequestMessageKey(RetCustAddResponseDto.Header.ResponseHeader.RequestMessageKey value) {
				this.requestMessageKey = value;
			}

			public RetCustAddResponseDto.Header.ResponseHeader.ResponseMessageInfo getResponseMessageInfo() {
				return responseMessageInfo;
			}

			public void setResponseMessageInfo(RetCustAddResponseDto.Header.ResponseHeader.ResponseMessageInfo value) {
				this.responseMessageInfo = value;
			}

			public RetCustAddResponseDto.Header.ResponseHeader.UBUSTransaction getUBUSTransaction() {
				return ubusTransaction;
			}

			public void setUBUSTransaction(RetCustAddResponseDto.Header.ResponseHeader.UBUSTransaction value) {
				this.ubusTransaction = value;
			}

			public RetCustAddResponseDto.Header.ResponseHeader.HostTransaction getHostTransaction() {
				return hostTransaction;
			}

			public void setHostTransaction(RetCustAddResponseDto.Header.ResponseHeader.HostTransaction value) {
				this.hostTransaction = value;
			}

			public RetCustAddResponseDto.Header.ResponseHeader.HostParentTransaction getHostParentTransaction() {
				return hostParentTransaction;
			}

			public void setHostParentTransaction(
					RetCustAddResponseDto.Header.ResponseHeader.HostParentTransaction value) {
				this.hostParentTransaction = value;
			}

			public String getCustomInfo() {
				return customInfo;
			}

			public void setCustomInfo(String value) {
				this.customInfo = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostParentTransaction {

				@XmlElement(name = "Id")
				protected String id;
				@XmlElement(name = "Status")
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostTransaction {

				@XmlElement(name = "Id")
				protected byte id;
				@XmlElement(name = "Status")
				protected String status;

				public byte getId() {
					return id;
				}

				public void setId(byte value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "requestUUID", "serviceRequestId", "serviceRequestVersion", "channelId" })
			public static class RequestMessageKey {

				@XmlElement(name = "RequestUUID")
				protected String requestUUID;
				@XmlElement(name = "ServiceRequestId")
				protected String serviceRequestId;
				@XmlElement(name = "ServiceRequestVersion")
				protected float serviceRequestVersion;
				@XmlElement(name = "ChannelId")
				protected String channelId;

				public String getRequestUUID() {
					return requestUUID;
				}

				public void setRequestUUID(String value) {
					this.requestUUID = value;
				}

				public String getServiceRequestId() {
					return serviceRequestId;
				}

				public void setServiceRequestId(String value) {
					this.serviceRequestId = value;
				}

				public float getServiceRequestVersion() {
					return serviceRequestVersion;
				}

				public void setServiceRequestVersion(float value) {
					this.serviceRequestVersion = value;
				}

				public String getChannelId() {
					return channelId;
				}

				public void setChannelId(String value) {
					this.channelId = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "bankId", "timeZone", "messageDateTime" })
			public static class ResponseMessageInfo {

				@XmlElement(name = "BankId")
				protected String bankId;
				@XmlElement(name = "TimeZone")
				protected String timeZone;
				@XmlElement(name = "MessageDateTime")
				@XmlSchemaType(name = "dateTime")
				protected XMLGregorianCalendar messageDateTime;

				public String getBankId() {
					return bankId;
				}

				public void setBankId(String value) {
					this.bankId = value;
				}

				public String getTimeZone() {
					return timeZone;
				}

				public void setTimeZone(String value) {
					this.timeZone = value;
				}

				public XMLGregorianCalendar getMessageDateTime() {
					return messageDateTime;
				}

				public void setMessageDateTime(XMLGregorianCalendar value) {
					this.messageDateTime = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class UBUSTransaction {

				@XmlElement(name = "Id")
				protected String id;
				@XmlElement(name = "Status")
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

		}

	}

}
