package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddOutBoundPymtRequestDto {

	@XmlElement(name = "Header")
	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body", required = true)
	protected AddOutBoundPymtRequestDto.Body body;

	public AddOutBoundPymtRequestDto.Body getBody() {
		return body;
	}

	public void setBody(AddOutBoundPymtRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "addOutboundPymtEntryDtlsRequest" })
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Body {

		@XmlElement(name = "AddOutboundPymtEntryDtlsRequest", required = true)
		protected AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest addOutboundPymtEntryDtlsRequest;

		public AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest getAddOutboundPymtEntryDtlsRequest() {
			return addOutboundPymtEntryDtlsRequest;
		}

		public void setAddOutboundPymtEntryDtlsRequest(
				AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest value) {
			this.addOutboundPymtEntryDtlsRequest = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "addOutboundPymtEntryDtlsRq" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class AddOutboundPymtEntryDtlsRequest {

			@XmlElement(name = "AddOutboundPymtEntryDtlsRq", required = true)
			protected AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq addOutboundPymtEntryDtlsRq;

			public AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq getAddOutboundPymtEntryDtlsRq() {
				return addOutboundPymtEntryDtlsRq;
			}

			public void setAddOutboundPymtEntryDtlsRq(
					AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq value) {
				this.addOutboundPymtEntryDtlsRq = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "paysysId", "requestedExecutionDate", "requestedAmount", "purposeRemarks",
					"purposeCode", "requestedAmountCrncy", "waiveChargeFlag", "serviceId", "serviceType", "debtorDtls",
					"creditorDtls",
					"creditorBankDtls", "pymtRelatedPartyDtlsLL", "tranIdentification", "initModuleId", "chargeType",
					"remittanceInfo" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class AddOutboundPymtEntryDtlsRq {

				@XmlElement(name = "PaysysId", required = true)
				protected String paysysId;
				@XmlElement(name = "RequestedExecutionDate", required = true)
				protected String requestedExecutionDate;
				@XmlElement(name = "RequestedAmount", required = true)
				protected AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.RequestedAmount requestedAmount;
				@XmlElement(name = "PurposeRemarks", required = true)
				protected String purposeRemarks;
				@XmlElement(name = "PurposeCode", required = true)
				protected String purposeCode;
				@XmlElement(name = "RequestedAmountCrncy", required = true)
				protected String requestedAmountCrncy;
				@XmlElement(name = "WaiveChargeFlag", required = true)
				protected String waiveChargeFlag;
				@XmlElement(name = "ServiceId", required = true)
				protected String serviceId;
				@XmlElement(name = "ServiceType", required = true)
				protected String serviceType;
				@XmlElement(name = "DebtorDtls", required = true)
				protected AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.DebtorDtls debtorDtls;
				@XmlElement(name = "CreditorDtls", required = true)
				protected AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorDtls creditorDtls;
				@XmlElement(name = "CreditorBankDtls", required = true)
				protected AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorBankDtls creditorBankDtls;
				@XmlElement(name = "PymtRelatedPartyDtlsLL", required = true)
				protected AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.PymtRelatedPartyDtlsLL pymtRelatedPartyDtlsLL;
				@XmlElement(name = "TranIdentification", required = true)
				protected String tranIdentification;
				@XmlElement(name = "InitModuleId", required = true)
				protected String initModuleId;
				@XmlElement(name = "ChargeType", required = true)
				protected String chargeType;
				@XmlElement(name = "RemittanceInfo", required = true)
				protected AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.RemittanceInfo remittanceInfo;

				public String getPaysysId() {
					return paysysId;
				}

				public void setPaysysId(String value) {
					this.paysysId = value;
				}

				public String getRequestedExecutionDate() {
					return requestedExecutionDate;
				}

				public void setRequestedExecutionDate(String value) {
					this.requestedExecutionDate = value;
				}

				public AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.RequestedAmount getRequestedAmount() {
					return requestedAmount;
				}

				public void setRequestedAmount(
						AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.RequestedAmount value) {
					this.requestedAmount = value;
				}

				public String getPurposeRemarks() {
					return purposeRemarks;
				}

				public void setPurposeRemarks(String value) {
					this.purposeRemarks = value;
				}

				public String getRequestedAmountCrncy() {
					return requestedAmountCrncy;
				}

				public void setRequestedAmountCrncy(String value) {
					this.requestedAmountCrncy = value;
				}

				public String getWaiveChargeFlag() {
					return waiveChargeFlag;
				}

				public void setWaiveChargeFlag(String value) {
					this.waiveChargeFlag = value;
				}

				public String getServiceId() {
					return serviceId;
				}

				public void setServiceId(String value) {
					this.serviceId = value;
				}

				public String getServiceType() {
					return serviceType;
				}

				public void setServiceType(String value) {
					this.serviceType = value;
				}

				public AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.DebtorDtls getDebtorDtls() {
					return debtorDtls;
				}

				public void setDebtorDtls(
						AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.DebtorDtls value) {
					this.debtorDtls = value;
				}

				public AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorDtls getCreditorDtls() {
					return creditorDtls;
				}

				public void setCreditorDtls(
						AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorDtls value) {
					this.creditorDtls = value;
				}

				public AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorBankDtls getCreditorBankDtls() {
					return creditorBankDtls;
				}

				public void setCreditorBankDtls(
						AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorBankDtls value) {
					this.creditorBankDtls = value;
				}

				public AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.PymtRelatedPartyDtlsLL getPymtRelatedPartyDtlsLL() {
					return pymtRelatedPartyDtlsLL;
				}

				public void setPymtRelatedPartyDtlsLL(
						AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.PymtRelatedPartyDtlsLL value) {
					this.pymtRelatedPartyDtlsLL = value;
				}

				public String getTranIdentification() {
					return tranIdentification;
				}

				public void setTranIdentification(String value) {
					this.tranIdentification = value;
				}

				public String getInitModuleId() {
					return initModuleId;
				}

				public void setInitModuleId(String value) {
					this.initModuleId = value;
				}

				public String getChargeType() {
					return chargeType;
				}

				public void setChargeType(String value) {
					this.chargeType = value;
				}

				public AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.RemittanceInfo getRemittanceInfo() {
					return remittanceInfo;
				}

				public void setRemittanceInfo(
						AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.RemittanceInfo value) {
					this.remittanceInfo = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "networkIdentification" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class CreditorBankDtls {

					@XmlElement(name = "NetworkIdentification", required = true)
					protected AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorBankDtls.NetworkIdentification networkIdentification;

					public AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorBankDtls.NetworkIdentification getNetworkIdentification() {
						return networkIdentification;
					}

					public void setNetworkIdentification(
							AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorBankDtls.NetworkIdentification value) {
						this.networkIdentification = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "networkIdentifier", "networkDirectory" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class NetworkIdentification {

						@XmlElement(name = "NetworkIdentifier", required = true)
						protected String networkIdentifier;
						@XmlElement(name = "NetworkDirectory", required = true)
						protected String networkDirectory;

						public String getNetworkIdentifier() {
							return networkIdentifier;
						}

						public void setNetworkIdentifier(String value) {
							this.networkIdentifier = value;
						}

						public String getNetworkDirectory() {
							return networkDirectory;
						}

						public void setNetworkDirectory(String value) {
							this.networkDirectory = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "accountId", "addressDetails" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class CreditorDtls {

					@XmlElement(name = "AccountId")
					protected String accountId;
					@XmlElement(name = "AddressDetails", required = true)
					protected AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorDtls.AddressDetails addressDetails;

					public String getAccountId() {
						return accountId;
					}

					public void setAccountId(String value) {
						this.accountId = value;
					}

					public AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorDtls.AddressDetails getAddressDetails() {
						return addressDetails;
					}

					public void setAddressDetails(
							AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.CreditorDtls.AddressDetails value) {
						this.addressDetails = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "name", "addressLine1" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class AddressDetails {

						@XmlElement(name = "Name", required = true)
						protected String name;
						@XmlElement(name = "AddressLine1", required = true)
						protected String addressLine1;

						public String getName() {
							return name;
						}

						public void setName(String value) {
							this.name = value;
						}

						public String getAddressLine1() {
							return addressLine1;
						}

						public void setAddressLine1(String value) {
							this.addressLine1 = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "addressDetails", "accountId" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class DebtorDtls {

					@XmlElement(name = "AddressDetails", required = true)
					protected AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.DebtorDtls.AddressDetails addressDetails;
					@XmlElement(name = "AccountId")
					protected String accountId;

					public AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.DebtorDtls.AddressDetails getAddressDetails() {
						return addressDetails;
					}

					public void setAddressDetails(
							AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.DebtorDtls.AddressDetails value) {
						this.addressDetails = value;
					}

					public String getAccountId() {
						return accountId;
					}

					public void setAccountId(String value) {
						this.accountId = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "name" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class AddressDetails {

						@XmlElement(name = "Name", required = true)
						protected String name;

						public String getName() {
							return name;
						}

						public void setName(String value) {
							this.name = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "partyType", "accountId", "addressDetails" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class PymtRelatedPartyDtlsLL {

					@XmlElement(name = "PartyType", required = true)
					protected String partyType;
					@XmlElement(name = "AccountId")
					protected long accountId;
					@XmlElement(name = "AddressDetails", required = true)
					protected AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.PymtRelatedPartyDtlsLL.AddressDetails addressDetails;

					public String getPartyType() {
						return partyType;
					}

					public void setPartyType(String value) {
						this.partyType = value;
					}

					public long getAccountId() {
						return accountId;
					}

					public void setAccountId(long value) {
						this.accountId = value;
					}

					public AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.PymtRelatedPartyDtlsLL.AddressDetails getAddressDetails() {
						return addressDetails;
					}

					public void setAddressDetails(
							AddOutBoundPymtRequestDto.Body.AddOutboundPymtEntryDtlsRequest.AddOutboundPymtEntryDtlsRq.PymtRelatedPartyDtlsLL.AddressDetails value) {
						this.addressDetails = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "name" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class AddressDetails {

						@XmlElement(name = "Name", required = true)
						protected String name;

						public String getName() {
							return name;
						}

						public void setName(String value) {
							this.name = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "remitInfo1", "remitInfo2", "remitInfo3" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class RemittanceInfo {

					@XmlElement(name = "RemitInfo1", required = true)
					protected String remitInfo1;
					@XmlElement(name = "RemitInfo2", required = true)
					protected String remitInfo2;
					@XmlElement(name = "RemitInfo3", required = true)
					protected String remitInfo3;

					public String getRemitInfo1() {
						return remitInfo1;
					}

					public void setRemitInfo1(String value) {
						this.remitInfo1 = value;
					}

					public String getRemitInfo2() {
						return remitInfo2;
					}

					public void setRemitInfo2(String value) {
						this.remitInfo2 = value;
					}

					public String getRemitInfo3() {
						return remitInfo3;
					}

					public void setRemitInfo3(String value) {
						this.remitInfo3 = value;
					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class RequestedAmount {

					protected String amountValue;
					@XmlElement(required = true)
					protected String currencyCode;

					public String getAmountValue() {
						return amountValue;
					}

					public void setAmountValue(String value) {
						this.amountValue = value;
					}

					public String getCurrencyCode() {
						return currencyCode;
					}

					public void setCurrencyCode(String value) {
						this.currencyCode = value;
					}

				}

				public String getPurposeCode() {
					return purposeCode;
				}

				public void setPurposeCode(String purposeCode) {
					this.purposeCode = purposeCode;
				}

			}

		}

	}

}
