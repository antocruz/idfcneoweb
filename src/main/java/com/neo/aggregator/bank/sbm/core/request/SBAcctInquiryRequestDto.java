package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SBAcctInquiryRequestDto {

	@XmlElement(name = "Header")
	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body", required = true)
	protected SBAcctInquiryRequestDto.Body body;

	public SBAcctInquiryRequestDto.Body getBody() {
		return body;
	}

	public void setBody(SBAcctInquiryRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "sbAcctInqRequest" })
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Body {

		@XmlElement(name = "SBAcctInqRequest", required = true)
		protected SBAcctInquiryRequestDto.Body.SBAcctInqRequest sbAcctInqRequest;

		public SBAcctInquiryRequestDto.Body.SBAcctInqRequest getSBAcctInqRequest() {
			return sbAcctInqRequest;
		}

		public void setSBAcctInqRequest(SBAcctInquiryRequestDto.Body.SBAcctInqRequest value) {
			this.sbAcctInqRequest = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "sbAcctInqRq" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class SBAcctInqRequest {

			@XmlElement(name = "SBAcctInqRq", required = true)
			protected SBAcctInquiryRequestDto.Body.SBAcctInqRequest.SBAcctInqRq sbAcctInqRq;

			public SBAcctInquiryRequestDto.Body.SBAcctInqRequest.SBAcctInqRq getSBAcctInqRq() {
				return sbAcctInqRq;
			}

			public void setSBAcctInqRq(SBAcctInquiryRequestDto.Body.SBAcctInqRequest.SBAcctInqRq value) {
				this.sbAcctInqRq = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "sbAcctId" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class SBAcctInqRq {

				@XmlElement(name = "SBAcctId", required = true)
				protected SBAcctInquiryRequestDto.Body.SBAcctInqRequest.SBAcctInqRq.SBAcctId sbAcctId;

				public SBAcctInquiryRequestDto.Body.SBAcctInqRequest.SBAcctInqRq.SBAcctId getSBAcctId() {
					return sbAcctId;
				}

				public void setSBAcctId(SBAcctInquiryRequestDto.Body.SBAcctInqRequest.SBAcctInqRq.SBAcctId value) {
					this.sbAcctId = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctId" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class SBAcctId {

					@XmlElement(name = "AcctId")
					protected String acctId;

					public String getAcctId() {
						return acctId;
					}

					public void setAcctId(String value) {
						this.acctId = value;
					}

				}

			}

		}

	}
}
