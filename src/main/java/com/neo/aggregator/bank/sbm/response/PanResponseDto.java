package com.neo.aggregator.bank.sbm.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PanResponseDto {

	private String responseCode;

	private String message;

	private List<PanData> data;

	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@Builder
	public static class PanData {

		private String errorMsg;

		private String panNo;

		private String panStatus;

		private String lastName;

		private String firstName;

		private String middleName;

		private String panTitle;

		private String panLastUpdate;

		private String filler1;

		private String filler2;

		private String filler3;

	}

}
