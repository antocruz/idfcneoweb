package com.neo.aggregator.bank.sbm.core.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DebitAddRequestDto {

	@XmlElement(name = "Header")
	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body", required = true)
	protected DebitAddRequestDto.Body body;

	public DebitAddRequestDto.Body getBody() {
		return body;
	}

	public void setBody(DebitAddRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "debitAddRequest" })
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Body {

		@XmlElement(name = "DebitAddRequest", required = true)
		protected DebitAddRequestDto.Body.DebitAddRequest debitAddRequest;

		public DebitAddRequestDto.Body.DebitAddRequest getDebitAddRequest() {
			return debitAddRequest;
		}

		public void setDebitAddRequest(DebitAddRequestDto.Body.DebitAddRequest value) {
			this.debitAddRequest = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "debitAddRq" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class DebitAddRequest {

			@XmlElement(name = "DebitAddRq", required = true)
			protected DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq debitAddRq;

			public DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq getDebitAddRq() {
				return debitAddRq;
			}

			public void setDebitAddRq(DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq value) {
				this.debitAddRq = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "totAmtCurCode", "trnSubType", "debitPartTrnRec" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class DebitAddRq {

				@XmlElement(name = "TotAmtCurCode", required = true)
				protected String totAmtCurCode;
				@XmlElement(name = "TrnSubType", required = true)
				protected String trnSubType;
				@XmlElement(name = "DebitPartTrnRec", required = true)
				protected DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq.DebitPartTrnRec debitPartTrnRec;

				public String getTotAmtCurCode() {
					return totAmtCurCode;
				}

				public void setTotAmtCurCode(String value) {
					this.totAmtCurCode = value;
				}

				public String getTrnSubType() {
					return trnSubType;
				}

				public void setTrnSubType(String value) {
					this.trnSubType = value;
				}

				public DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq.DebitPartTrnRec getDebitPartTrnRec() {
					return debitPartTrnRec;
				}

				public void setDebitPartTrnRec(
						DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq.DebitPartTrnRec value) {
					this.debitPartTrnRec = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctId", "trnAmt", "valueDt" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class DebitPartTrnRec {

					@XmlElement(name = "AcctId", required = true)
					protected DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq.DebitPartTrnRec.AcctId acctId;
					@XmlElement(name = "TrnAmt", required = true)
					protected DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq.DebitPartTrnRec.TrnAmt trnAmt;
					@XmlElement(name = "ValueDt", required = true)
					@XmlSchemaType(name = "dateTime")
					protected String valueDt;

					public DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq.DebitPartTrnRec.AcctId getAcctId() {
						return acctId;
					}

					public void setAcctId(
							DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq.DebitPartTrnRec.AcctId value) {
						this.acctId = value;
					}

					public DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq.DebitPartTrnRec.TrnAmt getTrnAmt() {
						return trnAmt;
					}

					public void setTrnAmt(
							DebitAddRequestDto.Body.DebitAddRequest.DebitAddRq.DebitPartTrnRec.TrnAmt value) {
						this.trnAmt = value;
					}

					public String getValueDt() {
						return valueDt;
					}

					public void setValueDt(String value) {
						this.valueDt = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "acctId" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class AcctId {

						@XmlElement(name = "AcctId")
						protected String acctId;

						public String getAcctId() {
							return acctId;
						}

						public void setAcctId(String value) {
							this.acctId = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "amountValue", "currencyCode" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class TrnAmt {

						protected String amountValue;
						@XmlElement(required = true)
						protected String currencyCode;

						public String getAmountValue() {
							return amountValue;
						}

						public void setAmountValue(String value) {
							this.amountValue = value;
						}

						public String getCurrencyCode() {
							return currencyCode;
						}

						public void setCurrencyCode(String value) {
							this.currencyCode = value;
						}

					}

				}

			}

		}

	}

}
