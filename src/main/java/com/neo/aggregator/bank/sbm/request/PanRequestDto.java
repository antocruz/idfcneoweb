package com.neo.aggregator.bank.sbm.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PanRequestDto {

	private String panNumber;

}
