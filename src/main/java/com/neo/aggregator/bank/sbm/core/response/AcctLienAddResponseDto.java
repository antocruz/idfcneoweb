package com.neo.aggregator.bank.sbm.core.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
public class AcctLienAddResponseDto {

	@XmlElement(name = "Header", required = true)
	protected AcctLienAddResponseDto.Header header;

	@XmlElement(name = "Body", required = true)
	protected AcctLienAddResponseDto.Body body;

	public AcctLienAddResponseDto.Header getHeader() {
		return header;
	}

	public void setHeader(AcctLienAddResponseDto.Header value) {
		this.header = value;
	}

	public AcctLienAddResponseDto.Body getBody() {
		return body;
	}

	public void setBody(AcctLienAddResponseDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "acctLienAddResponse", "error" })
	public static class Body {

		@XmlElement(name = "AcctLienAddResponse", required = true)
		protected AcctLienAddResponseDto.Body.AcctLienAddResponse acctLienAddResponse;

		public AcctLienAddResponseDto.Body.AcctLienAddResponse getAcctLienAddResponse() {
			return acctLienAddResponse;
		}

		public void setAcctLienAddResponse(AcctLienAddResponseDto.Body.AcctLienAddResponse value) {
			this.acctLienAddResponse = value;
		}

		@XmlElement(name = "Error")
		protected Error error;

		public Error getError() {
			return error;
		}

		public void setError(Error value) {
			this.error = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)

		@XmlType(name = "")
		public static class Error {

			@XmlElement(name = "FIBusinessException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiBusinessException;

			@XmlElement(name = "FISystemException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException;

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFiSystemException() {
				return fiSystemException;
			}

			public void setFiSystemException(RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException) {
				this.fiSystemException = fiSystemException;
			}

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFIBusinessException() {
				return fiBusinessException;
			}

			public void setFIBusinessException(RetCustAddResponseDto.Body.Error.FIBusinessException value) {
				this.fiBusinessException = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)

			@XmlType(name = "", propOrder = { "errorDetail" })
			public static class FIBusinessException {

				@XmlElement(name = "ErrorDetail")
				protected RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail errorDetail;

				public RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail getErrorDetail() {
					return errorDetail;
				}

				public void setErrorDetail(RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail value) {
					this.errorDetail = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)

				@XmlType(name = "", propOrder = { "errorCode", "errorDesc", "errorType" })
				public static class ErrorDetail {

					@XmlElement(name = "ErrorCode")
					protected String errorCode;

					@XmlElement(name = "ErrorDesc")
					protected String errorDesc;

					@XmlElement(name = "ErrorType")
					protected String errorType;

					public String getErrorCode() {
						return errorCode;
					}

					public void setErrorCode(String value) {
						this.errorCode = value;
					}

					public String getErrorDesc() {
						return errorDesc;
					}

					public void setErrorDesc(String value) {
						this.errorDesc = value;
					}

					public String getErrorType() {
						return errorType;
					}

					public void setErrorType(String value) {
						this.errorType = value;
					}

				}

			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "acctLienAddRs", "acctLienAddCustomData" })
		public static class AcctLienAddResponse {

			@XmlElement(name = "AcctLienAddRs", required = true)
			protected AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs acctLienAddRs;
			@XmlElement(name = "AcctLienAdd_CustomData", required = true)
			protected String acctLienAddCustomData;

			public AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs getAcctLienAddRs() {
				return acctLienAddRs;
			}

			public void setAcctLienAddRs(AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs value) {
				this.acctLienAddRs = value;
			}

			public String getAcctLienAddCustomData() {
				return acctLienAddCustomData;
			}

			public void setAcctLienAddCustomData(String value) {
				this.acctLienAddCustomData = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "acctId", "lienIdRec" })
			public static class AcctLienAddRs {

				@XmlElement(name = "AcctId", required = true)
				protected AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs.AcctId acctId;
				@XmlElement(name = "LienIdRec", required = true)
				protected AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs.LienIdRec lienIdRec;

				public AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs.AcctId getAcctId() {
					return acctId;
				}

				public void setAcctId(AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs.AcctId value) {
					this.acctId = value;
				}

				public AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs.LienIdRec getLienIdRec() {
					return lienIdRec;
				}

				public void setLienIdRec(
						AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs.LienIdRec value) {
					this.lienIdRec = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctId", "acctType", "acctCurr", "bankInfo" })
				public static class AcctId {

					@XmlElement(name = "AcctId")
					protected String acctId;
					@XmlElement(name = "AcctType", required = true)
					protected AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs.AcctId.AcctType acctType;
					@XmlElement(name = "AcctCurr", required = true)
					protected String acctCurr;
					@XmlElement(name = "BankInfo", required = true)
					protected AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs.AcctId.BankInfo bankInfo;

					public String getAcctId() {
						return acctId;
					}

					public void setAcctId(String value) {
						this.acctId = value;
					}

					public AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs.AcctId.AcctType getAcctType() {
						return acctType;
					}

					public void setAcctType(
							AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs.AcctId.AcctType value) {
						this.acctType = value;
					}

					public String getAcctCurr() {
						return acctCurr;
					}

					public void setAcctCurr(String value) {
						this.acctCurr = value;
					}

					public AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs.AcctId.BankInfo getBankInfo() {
						return bankInfo;
					}

					public void setBankInfo(
							AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs.AcctId.BankInfo value) {
						this.bankInfo = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "schmCode", "schmType" })
					public static class AcctType {

						@XmlElement(name = "SchmCode", required = true)
						protected String schmCode;
						@XmlElement(name = "SchmType", required = true)
						protected String schmType;

						public String getSchmCode() {
							return schmCode;
						}

						public void setSchmCode(String value) {
							this.schmCode = value;
						}

						public String getSchmType() {
							return schmType;
						}

						public void setSchmType(String value) {
							this.schmType = value;
						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "bankId", "name", "branchId", "branchName", "postAddr" })
					public static class BankInfo {

						@XmlElement(name = "BankId", required = true)
						protected String bankId;
						@XmlElement(name = "Name", required = true)
						protected String name;
						@XmlElement(name = "BranchId", required = true)
						protected String branchId;
						@XmlElement(name = "BranchName", required = true)
						protected String branchName;
						@XmlElement(name = "PostAddr", required = true)
						protected AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs.AcctId.BankInfo.PostAddr postAddr;

						public String getBankId() {
							return bankId;
						}

						public void setBankId(String value) {
							this.bankId = value;
						}

						public String getName() {
							return name;
						}

						public void setName(String value) {
							this.name = value;
						}

						public String getBranchId() {
							return branchId;
						}

						public void setBranchId(String value) {
							this.branchId = value;
						}

						public String getBranchName() {
							return branchName;
						}

						public void setBranchName(String value) {
							this.branchName = value;
						}

						public AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs.AcctId.BankInfo.PostAddr getPostAddr() {
							return postAddr;
						}

						public void setPostAddr(
								AcctLienAddResponseDto.Body.AcctLienAddResponse.AcctLienAddRs.AcctId.BankInfo.PostAddr value) {
							this.postAddr = value;
						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "addr1", "addr2", "addr3", "city", "stateProv", "postalCode",
								"country", "addrType" })
						public static class PostAddr {

							@XmlElement(name = "Addr1", required = true)
							protected String addr1;
							@XmlElement(name = "Addr2", required = true)
							protected String addr2;
							@XmlElement(name = "Addr3", required = true)
							protected String addr3;
							@XmlElement(name = "City", required = true)
							protected String city;
							@XmlElement(name = "StateProv", required = true)
							protected String stateProv;
							@XmlElement(name = "PostalCode", required = true)
							protected String postalCode;
							@XmlElement(name = "Country", required = true)
							protected String country;
							@XmlElement(name = "AddrType", required = true)
							protected String addrType;

							public String getAddr1() {
								return addr1;
							}

							public void setAddr1(String value) {
								this.addr1 = value;
							}

							public String getAddr2() {
								return addr2;
							}

							public void setAddr2(String value) {
								this.addr2 = value;
							}

							public String getAddr3() {
								return addr3;
							}

							public void setAddr3(String value) {
								this.addr3 = value;
							}

							public String getCity() {
								return city;
							}

							public void setCity(String value) {
								this.city = value;
							}

							public String getStateProv() {
								return stateProv;
							}

							public void setStateProv(String value) {
								this.stateProv = value;
							}

							public String getPostalCode() {
								return postalCode;
							}

							public void setPostalCode(String value) {
								this.postalCode = value;
							}

							public String getCountry() {
								return country;
							}

							public void setCountry(String value) {
								this.country = value;
							}

							public String getAddrType() {
								return addrType;
							}

							public void setAddrType(String value) {
								this.addrType = value;
							}

						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "lienId" })
				public static class LienIdRec {

					@XmlElement(name = "LienId", required = true)
					protected String lienId;

					public String getLienId() {
						return lienId;
					}

					public void setLienId(String value) {
						this.lienId = value;
					}

				}

			}

		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "responseHeader" })
	public static class Header {

		@XmlElement(name = "ResponseHeader", required = true)
		protected AcctLienAddResponseDto.Header.ResponseHeader responseHeader;

		public AcctLienAddResponseDto.Header.ResponseHeader getResponseHeader() {
			return responseHeader;
		}

		public void setResponseHeader(AcctLienAddResponseDto.Header.ResponseHeader value) {
			this.responseHeader = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "requestMessageKey", "responseMessageInfo", "ubusTransaction",
				"hostTransaction", "hostParentTransaction", "customInfo" })
		public static class ResponseHeader {

			@XmlElement(name = "RequestMessageKey", required = true)
			protected AcctLienAddResponseDto.Header.ResponseHeader.RequestMessageKey requestMessageKey;
			@XmlElement(name = "ResponseMessageInfo", required = true)
			protected AcctLienAddResponseDto.Header.ResponseHeader.ResponseMessageInfo responseMessageInfo;
			@XmlElement(name = "UBUSTransaction", required = true)
			protected AcctLienAddResponseDto.Header.ResponseHeader.UBUSTransaction ubusTransaction;
			@XmlElement(name = "HostTransaction", required = true)
			protected AcctLienAddResponseDto.Header.ResponseHeader.HostTransaction hostTransaction;
			@XmlElement(name = "HostParentTransaction", required = true)
			protected AcctLienAddResponseDto.Header.ResponseHeader.HostParentTransaction hostParentTransaction;
			@XmlElement(name = "CustomInfo", required = true)
			protected String customInfo;

			public AcctLienAddResponseDto.Header.ResponseHeader.RequestMessageKey getRequestMessageKey() {
				return requestMessageKey;
			}

			public void setRequestMessageKey(AcctLienAddResponseDto.Header.ResponseHeader.RequestMessageKey value) {
				this.requestMessageKey = value;
			}

			public AcctLienAddResponseDto.Header.ResponseHeader.ResponseMessageInfo getResponseMessageInfo() {
				return responseMessageInfo;
			}

			public void setResponseMessageInfo(AcctLienAddResponseDto.Header.ResponseHeader.ResponseMessageInfo value) {
				this.responseMessageInfo = value;
			}

			public AcctLienAddResponseDto.Header.ResponseHeader.UBUSTransaction getUBUSTransaction() {
				return ubusTransaction;
			}

			public void setUBUSTransaction(AcctLienAddResponseDto.Header.ResponseHeader.UBUSTransaction value) {
				this.ubusTransaction = value;
			}

			public AcctLienAddResponseDto.Header.ResponseHeader.HostTransaction getHostTransaction() {
				return hostTransaction;
			}

			public void setHostTransaction(AcctLienAddResponseDto.Header.ResponseHeader.HostTransaction value) {
				this.hostTransaction = value;
			}

			public AcctLienAddResponseDto.Header.ResponseHeader.HostParentTransaction getHostParentTransaction() {
				return hostParentTransaction;
			}

			public void setHostParentTransaction(
					AcctLienAddResponseDto.Header.ResponseHeader.HostParentTransaction value) {
				this.hostParentTransaction = value;
			}

			public String getCustomInfo() {
				return customInfo;
			}

			public void setCustomInfo(String value) {
				this.customInfo = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostParentTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "requestUUID", "serviceRequestId", "serviceRequestVersion", "channelId" })
			public static class RequestMessageKey {

				@XmlElement(name = "RequestUUID", required = true)
				protected String requestUUID;
				@XmlElement(name = "ServiceRequestId", required = true)
				protected String serviceRequestId;
				@XmlElement(name = "ServiceRequestVersion")
				protected String serviceRequestVersion;
				@XmlElement(name = "ChannelId", required = true)
				protected String channelId;

				public String getRequestUUID() {
					return requestUUID;
				}

				public void setRequestUUID(String value) {
					this.requestUUID = value;
				}

				public String getServiceRequestId() {
					return serviceRequestId;
				}

				public void setServiceRequestId(String value) {
					this.serviceRequestId = value;
				}

				public String getServiceRequestVersion() {
					return serviceRequestVersion;
				}

				public void setServiceRequestVersion(String value) {
					this.serviceRequestVersion = value;
				}

				public String getChannelId() {
					return channelId;
				}

				public void setChannelId(String value) {
					this.channelId = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "bankId", "timeZone", "messageDateTime" })
			public static class ResponseMessageInfo {

				@XmlElement(name = "BankId", required = true)
				protected String bankId;
				@XmlElement(name = "TimeZone", required = true)
				protected String timeZone;
				@XmlElement(name = "MessageDateTime", required = true)
				@XmlSchemaType(name = "dateTime")
				protected String messageDateTime;

				public String getBankId() {
					return bankId;
				}

				public void setBankId(String value) {
					this.bankId = value;
				}

				public String getTimeZone() {
					return timeZone;
				}

				public void setTimeZone(String value) {
					this.timeZone = value;
				}

				public String getMessageDateTime() {
					return messageDateTime;
				}

				public void setMessageDateTime(String value) {
					this.messageDateTime = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class UBUSTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

		}

	}

}
