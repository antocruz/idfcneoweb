package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FIXML", namespace ="http://www.finacle.com/fixml")
public class FixmlRoot {
	
	protected Header header;

	@XmlAttribute(name = "Header")
	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}
}
