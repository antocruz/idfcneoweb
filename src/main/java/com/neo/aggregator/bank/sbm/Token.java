package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.Builder;

@Builder
public class Token {
	
	protected PasswordToken passwordToken;

	@XmlElement(name = "PasswordToken")
	public PasswordToken getPasswordToken() {
		return passwordToken;
	}

	public void setPasswordToken(PasswordToken passwordToken) {
		this.passwordToken = passwordToken;
	}
	
}
