package com.neo.aggregator.bank.sbm.core.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
public class IFTTransferResponseDto {

	@XmlElement(name = "Header", required = true)
	protected IFTTransferResponseDto.Header header;
	@XmlElement(name = "Body", required = true)
	protected IFTTransferResponseDto.Body body;

	public IFTTransferResponseDto.Header getHeader() {
		return header;
	}

	public void setHeader(IFTTransferResponseDto.Header value) {
		this.header = value;
	}

	public IFTTransferResponseDto.Body getBody() {
		return body;
	}

	public void setBody(IFTTransferResponseDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "xferTrnAddResponse", "error" })
	public static class Body {

		@XmlElement(name = "XferTrnAddResponse", required = true)
		protected IFTTransferResponseDto.Body.XferTrnAddResponse xferTrnAddResponse;

		public IFTTransferResponseDto.Body.XferTrnAddResponse getXferTrnAddResponse() {
			return xferTrnAddResponse;
		}

		public void setXferTrnAddResponse(IFTTransferResponseDto.Body.XferTrnAddResponse value) {
			this.xferTrnAddResponse = value;
		}

		@XmlElement(name = "Error")
		protected Error error;

		public Error getError() {
			return error;
		}

		public void setError(Error value) {
			this.error = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class Error {

			@XmlElement(name = "FIBusinessException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiBusinessException;

			@XmlElement(name = "FISystemException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException;

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFiSystemException() {
				return fiSystemException;
			}

			public void setFiSystemException(RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException) {
				this.fiSystemException = fiSystemException;
			}

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFIBusinessException() {
				return fiBusinessException;
			}

			public void setFIBusinessException(RetCustAddResponseDto.Body.Error.FIBusinessException value) {
				this.fiBusinessException = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)

			@XmlType(name = "", propOrder = { "errorDetail" })
			public static class FIBusinessException {

				@XmlElement(name = "ErrorDetail")
				protected RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail errorDetail;

				public RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail getErrorDetail() {
					return errorDetail;
				}

				public void setErrorDetail(RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail value) {
					this.errorDetail = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)

				@XmlType(name = "", propOrder = { "errorCode", "errorDesc", "errorType" })
				public static class ErrorDetail {

					@XmlElement(name = "ErrorCode")
					protected String errorCode;

					@XmlElement(name = "ErrorDesc")
					protected String errorDesc;

					@XmlElement(name = "ErrorType")
					protected String errorType;

					public String getErrorCode() {
						return errorCode;
					}

					public void setErrorCode(String value) {
						this.errorCode = value;
					}

					public String getErrorDesc() {
						return errorDesc;
					}

					public void setErrorDesc(String value) {
						this.errorDesc = value;
					}

					public String getErrorType() {
						return errorType;
					}

					public void setErrorType(String value) {
						this.errorType = value;
					}

				}

			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "xferTrnAddRs", "xferTrnAddCustomData" })
		public static class XferTrnAddResponse {

			@XmlElement(name = "XferTrnAddRs", required = true)
			protected IFTTransferResponseDto.Body.XferTrnAddResponse.XferTrnAddRs xferTrnAddRs;
			@XmlElement(name = "XferTrnAdd_CustomData", required = true)
			protected String xferTrnAddCustomData;

			public IFTTransferResponseDto.Body.XferTrnAddResponse.XferTrnAddRs getXferTrnAddRs() {
				return xferTrnAddRs;
			}

			public void setXferTrnAddRs(IFTTransferResponseDto.Body.XferTrnAddResponse.XferTrnAddRs value) {
				this.xferTrnAddRs = value;
			}

			public String getXferTrnAddCustomData() {
				return xferTrnAddCustomData;
			}

			public void setXferTrnAddCustomData(String value) {
				this.xferTrnAddCustomData = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "trnIdentifier" })
			public static class XferTrnAddRs {

				@XmlElement(name = "TrnIdentifier", required = true)
				protected IFTTransferResponseDto.Body.XferTrnAddResponse.XferTrnAddRs.TrnIdentifier trnIdentifier;

				public IFTTransferResponseDto.Body.XferTrnAddResponse.XferTrnAddRs.TrnIdentifier getTrnIdentifier() {
					return trnIdentifier;
				}

				public void setTrnIdentifier(
						IFTTransferResponseDto.Body.XferTrnAddResponse.XferTrnAddRs.TrnIdentifier value) {
					this.trnIdentifier = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "trnDt", "trnId" })
				public static class TrnIdentifier {

					@XmlElement(name = "TrnDt", required = true)
					@XmlSchemaType(name = "dateTime")
					protected String trnDt;
					@XmlElement(name = "TrnId", required = true)
					protected String trnId;

					public String getTrnDt() {
						return trnDt;
					}

					public void setTrnDt(String value) {
						this.trnDt = value;
					}

					public String getTrnId() {
						return trnId;
					}

					public void setTrnId(String value) {
						this.trnId = value;
					}

				}

			}

		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "responseHeader" })
	public static class Header {

		@XmlElement(name = "ResponseHeader", required = true)
		protected IFTTransferResponseDto.Header.ResponseHeader responseHeader;

		public IFTTransferResponseDto.Header.ResponseHeader getResponseHeader() {
			return responseHeader;
		}

		public void setResponseHeader(IFTTransferResponseDto.Header.ResponseHeader value) {
			this.responseHeader = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "requestMessageKey", "responseMessageInfo", "ubusTransaction",
				"hostTransaction", "hostParentTransaction", "customInfo" })
		public static class ResponseHeader {

			@XmlElement(name = "RequestMessageKey", required = true)
			protected IFTTransferResponseDto.Header.ResponseHeader.RequestMessageKey requestMessageKey;
			@XmlElement(name = "ResponseMessageInfo", required = true)
			protected IFTTransferResponseDto.Header.ResponseHeader.ResponseMessageInfo responseMessageInfo;
			@XmlElement(name = "UBUSTransaction", required = true)
			protected IFTTransferResponseDto.Header.ResponseHeader.UBUSTransaction ubusTransaction;
			@XmlElement(name = "HostTransaction", required = true)
			protected IFTTransferResponseDto.Header.ResponseHeader.HostTransaction hostTransaction;
			@XmlElement(name = "HostParentTransaction", required = true)
			protected IFTTransferResponseDto.Header.ResponseHeader.HostParentTransaction hostParentTransaction;
			@XmlElement(name = "CustomInfo", required = true)
			protected String customInfo;

			public IFTTransferResponseDto.Header.ResponseHeader.RequestMessageKey getRequestMessageKey() {
				return requestMessageKey;
			}

			public void setRequestMessageKey(IFTTransferResponseDto.Header.ResponseHeader.RequestMessageKey value) {
				this.requestMessageKey = value;
			}

			public IFTTransferResponseDto.Header.ResponseHeader.ResponseMessageInfo getResponseMessageInfo() {
				return responseMessageInfo;
			}

			public void setResponseMessageInfo(IFTTransferResponseDto.Header.ResponseHeader.ResponseMessageInfo value) {
				this.responseMessageInfo = value;
			}

			public IFTTransferResponseDto.Header.ResponseHeader.UBUSTransaction getUBUSTransaction() {
				return ubusTransaction;
			}

			public void setUBUSTransaction(IFTTransferResponseDto.Header.ResponseHeader.UBUSTransaction value) {
				this.ubusTransaction = value;
			}

			public IFTTransferResponseDto.Header.ResponseHeader.HostTransaction getHostTransaction() {
				return hostTransaction;
			}

			public void setHostTransaction(IFTTransferResponseDto.Header.ResponseHeader.HostTransaction value) {
				this.hostTransaction = value;
			}

			public IFTTransferResponseDto.Header.ResponseHeader.HostParentTransaction getHostParentTransaction() {
				return hostParentTransaction;
			}

			public void setHostParentTransaction(
					IFTTransferResponseDto.Header.ResponseHeader.HostParentTransaction value) {
				this.hostParentTransaction = value;
			}

			public String getCustomInfo() {
				return customInfo;
			}

			public void setCustomInfo(String value) {
				this.customInfo = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostParentTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "requestUUID", "serviceRequestId", "serviceRequestVersion", "channelId" })
			public static class RequestMessageKey {

				@XmlElement(name = "RequestUUID", required = true)
				protected String requestUUID;
				@XmlElement(name = "ServiceRequestId", required = true)
				protected String serviceRequestId;
				@XmlElement(name = "ServiceRequestVersion")
				protected float serviceRequestVersion;
				@XmlElement(name = "ChannelId", required = true)
				protected String channelId;

				public String getRequestUUID() {
					return requestUUID;
				}

				public void setRequestUUID(String value) {
					this.requestUUID = value;
				}

				public String getServiceRequestId() {
					return serviceRequestId;
				}

				public void setServiceRequestId(String value) {
					this.serviceRequestId = value;
				}

				public float getServiceRequestVersion() {
					return serviceRequestVersion;
				}

				public void setServiceRequestVersion(float value) {
					this.serviceRequestVersion = value;
				}

				public String getChannelId() {
					return channelId;
				}

				public void setChannelId(String value) {
					this.channelId = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "bankId", "timeZone", "messageDateTime" })
			public static class ResponseMessageInfo {

				@XmlElement(name = "BankId", required = true)
				protected String bankId;
				@XmlElement(name = "TimeZone", required = true)
				protected String timeZone;
				@XmlElement(name = "MessageDateTime", required = true)
				@XmlSchemaType(name = "dateTime")
				protected String messageDateTime;

				public String getBankId() {
					return bankId;
				}

				public void setBankId(String value) {
					this.bankId = value;
				}

				public String getTimeZone() {
					return timeZone;
				}

				public void setTimeZone(String value) {
					this.timeZone = value;
				}

				public String getMessageDateTime() {
					return messageDateTime;
				}

				public void setMessageDateTime(String value) {
					this.messageDateTime = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class UBUSTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

		}

	}

}
