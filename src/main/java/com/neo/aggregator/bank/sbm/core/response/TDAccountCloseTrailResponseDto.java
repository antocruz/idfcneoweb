package com.neo.aggregator.bank.sbm.core.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
public class TDAccountCloseTrailResponseDto {

	@XmlElement(name = "Header", required = true)
	protected TDAccountCloseTrailResponseDto.Header header;
	@XmlElement(name = "Body", required = true)
	protected TDAccountCloseTrailResponseDto.Body body;

	public TDAccountCloseTrailResponseDto.Header getHeader() {
		return header;
	}

	public void setHeader(TDAccountCloseTrailResponseDto.Header value) {
		this.header = value;
	}

	public TDAccountCloseTrailResponseDto.Body getBody() {
		return body;
	}

	public void setBody(TDAccountCloseTrailResponseDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "executeFinacleScriptResponse", "error" })
	public static class Body {

		@XmlElement(required = true)
		protected TDAccountCloseTrailResponseDto.Body.ExecuteFinacleScriptResponse executeFinacleScriptResponse;

		public TDAccountCloseTrailResponseDto.Body.ExecuteFinacleScriptResponse getExecuteFinacleScriptResponse() {
			return executeFinacleScriptResponse;
		}

		public void setExecuteFinacleScriptResponse(
				TDAccountCloseTrailResponseDto.Body.ExecuteFinacleScriptResponse value) {
			this.executeFinacleScriptResponse = value;
		}

		@XmlElement(name = "Error")
		protected Error error;

		public Error getError() {
			return error;
		}

		public void setError(Error value) {
			this.error = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)

		@XmlType(name = "")
		public static class Error {

			@XmlElement(name = "FIBusinessException")
			protected TDAccountCloseTrailResponseDto.Body.Error.FIBusinessException fiBusinessException;

			@XmlElement(name = "FISystemException")
			protected TDAccountCloseTrailResponseDto.Body.Error.FIBusinessException fiSystemException;

			public TDAccountCloseTrailResponseDto.Body.Error.FIBusinessException getFiSystemException() {
				return fiSystemException;
			}

			public void setFiSystemException(
					TDAccountCloseTrailResponseDto.Body.Error.FIBusinessException fiSystemException) {
				this.fiSystemException = fiSystemException;
			}

			public TDAccountCloseTrailResponseDto.Body.Error.FIBusinessException getFIBusinessException() {
				return fiBusinessException;
			}

			public void setFIBusinessException(TDAccountCloseTrailResponseDto.Body.Error.FIBusinessException value) {
				this.fiBusinessException = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)

			@XmlType(name = "", propOrder = { "errorDetail" })
			public static class FIBusinessException {

				@XmlElement(name = "ErrorDetail")
				protected TDAccountCloseTrailResponseDto.Body.Error.FIBusinessException.ErrorDetail errorDetail;

				public TDAccountCloseTrailResponseDto.Body.Error.FIBusinessException.ErrorDetail getErrorDetail() {
					return errorDetail;
				}

				public void setErrorDetail(
						TDAccountCloseTrailResponseDto.Body.Error.FIBusinessException.ErrorDetail value) {
					this.errorDetail = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)

				@XmlType(name = "", propOrder = { "errorCode", "errorDesc", "errorType" })
				public static class ErrorDetail {

					@XmlElement(name = "ErrorCode")
					protected String errorCode;

					@XmlElement(name = "ErrorDesc")
					protected String errorDesc;

					@XmlElement(name = "ErrorType")
					protected String errorType;

					public String getErrorCode() {
						return errorCode;
					}

					public void setErrorCode(String value) {
						this.errorCode = value;
					}

					public String getErrorDesc() {
						return errorDesc;
					}

					public void setErrorDesc(String value) {
						this.errorDesc = value;
					}

					public String getErrorType() {
						return errorType;
					}

					public void setErrorType(String value) {
						this.errorType = value;
					}

				}

			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "executeFinacleScriptOutputVO", "executeFinacleScriptCustomData" })
		public static class ExecuteFinacleScriptResponse {

			@XmlElement(name = "ExecuteFinacleScriptOutputVO", required = true)
			protected String executeFinacleScriptOutputVO;
			@XmlElement(name = "executeFinacleScript_CustomData", required = true)
			protected TDAccountCloseTrailResponseDto.Body.ExecuteFinacleScriptResponse.ExecuteFinacleScriptCustomData executeFinacleScriptCustomData;

			public String getExecuteFinacleScriptOutputVO() {
				return executeFinacleScriptOutputVO;
			}

			public void setExecuteFinacleScriptOutputVO(String value) {
				this.executeFinacleScriptOutputVO = value;
			}

			public TDAccountCloseTrailResponseDto.Body.ExecuteFinacleScriptResponse.ExecuteFinacleScriptCustomData getExecuteFinacleScriptCustomData() {
				return executeFinacleScriptCustomData;
			}

			public void setExecuteFinacleScriptCustomData(
					TDAccountCloseTrailResponseDto.Body.ExecuteFinacleScriptResponse.ExecuteFinacleScriptCustomData value) {
				this.executeFinacleScriptCustomData = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "successOrFailure", "tdAccount", "withdrwlAmt", "repayAccount",
					"maturityDate", "maturityAmt", "depositAmt", "lienAmt", "contIntPcnt", "effIntPcnt", "actIntPcnt",
					"intRecoverable", "intPayable", "intTaken", "actualIntAmt", "penalIntPcnt", "penalIntAmt",
					"taxPayable", "taxTillDt", "rePayAmount" })
			public static class ExecuteFinacleScriptCustomData {

				@XmlElement(name = "SuccessOrFailure", required = true)
				protected String successOrFailure;
				@XmlElement(name = "TDAccount")
				protected String tdAccount;
				@XmlElement(name = "WithdrwlAmt")
				protected String withdrwlAmt;
				@XmlElement(name = "RepayAccount")
				protected String repayAccount;
				@XmlElement(name = "MaturityDate", required = true)
				protected String maturityDate;
				@XmlElement(name = "MaturityAmt")
				protected String maturityAmt;
				@XmlElement(name = "DepositAmt")
				protected String depositAmt;
				@XmlElement(name = "LienAmt")
				protected String lienAmt;
				@XmlElement(name = "ContIntPcnt")
				protected String contIntPcnt;
				@XmlElement(name = "EffIntPcnt")
				protected String effIntPcnt;
				@XmlElement(name = "ActIntPcnt")
				protected String actIntPcnt;
				@XmlElement(name = "IntRecoverable")
				protected String intRecoverable;
				@XmlElement(name = "IntPayable")
				protected String intPayable;
				@XmlElement(name = "IntTaken")
				protected String intTaken;
				@XmlElement(name = "ActualIntAmt")
				protected String actualIntAmt;
				@XmlElement(name = "PenalIntPcnt")
				protected String penalIntPcnt;
				@XmlElement(name = "PenalIntAmt")
				protected String penalIntAmt;
				@XmlElement(name = "TaxPayable")
				protected String taxPayable;
				@XmlElement(name = "TaxTillDt")
				protected String taxTillDt;
				@XmlElement(name = "RePayAmount")
				protected String rePayAmount;

				public String getSuccessOrFailure() {
					return successOrFailure;
				}

				public void setSuccessOrFailure(String value) {
					this.successOrFailure = value;
				}

				public String getTDAccount() {
					return tdAccount;
				}

				public void setTDAccount(String value) {
					this.tdAccount = value;
				}

				public String getWithdrwlAmt() {
					return withdrwlAmt;
				}

				public void setWithdrwlAmt(String value) {
					this.withdrwlAmt = value;
				}

				public String getRepayAccount() {
					return repayAccount;
				}

				public void setRepayAccount(String value) {
					this.repayAccount = value;
				}

				public String getMaturityDate() {
					return maturityDate;
				}

				public void setMaturityDate(String value) {
					this.maturityDate = value;
				}

				public String getMaturityAmt() {
					return maturityAmt;
				}

				public void setMaturityAmt(String value) {
					this.maturityAmt = value;
				}

				public String getDepositAmt() {
					return depositAmt;
				}

				public void setDepositAmt(String value) {
					this.depositAmt = value;
				}

				public String getLienAmt() {
					return lienAmt;
				}

				public void setLienAmt(String value) {
					this.lienAmt = value;
				}

				public String getContIntPcnt() {
					return contIntPcnt;
				}

				public void setContIntPcnt(String value) {
					this.contIntPcnt = value;
				}

				public String getEffIntPcnt() {
					return effIntPcnt;
				}

				public void setEffIntPcnt(String value) {
					this.effIntPcnt = value;
				}

				public String getActIntPcnt() {
					return actIntPcnt;
				}

				public void setActIntPcnt(String value) {
					this.actIntPcnt = value;
				}

				public String getIntRecoverable() {
					return intRecoverable;
				}

				public void setIntRecoverable(String value) {
					this.intRecoverable = value;
				}

				public String getIntPayable() {
					return intPayable;
				}

				public void setIntPayable(String value) {
					this.intPayable = value;
				}

				public String getIntTaken() {
					return intTaken;
				}

				public void setIntTaken(String value) {
					this.intTaken = value;
				}

				public String getActualIntAmt() {
					return actualIntAmt;
				}

				public void setActualIntAmt(String value) {
					this.actualIntAmt = value;
				}

				public String getPenalIntPcnt() {
					return penalIntPcnt;
				}

				public void setPenalIntPcnt(String value) {
					this.penalIntPcnt = value;
				}

				public String getPenalIntAmt() {
					return penalIntAmt;
				}

				public void setPenalIntAmt(String value) {
					this.penalIntAmt = value;
				}

				public String getTaxPayable() {
					return taxPayable;
				}

				public void setTaxPayable(String value) {
					this.taxPayable = value;
				}

				public String getTaxTillDt() {
					return taxTillDt;
				}

				public void setTaxTillDt(String value) {
					this.taxTillDt = value;
				}

				public String getRePayAmount() {
					return rePayAmount;
				}

				public void setRePayAmount(String value) {
					this.rePayAmount = value;
				}

			}

		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "responseHeader" })
	public static class Header {

		@XmlElement(name = "ResponseHeader", required = true)
		protected TDAccountCloseTrailResponseDto.Header.ResponseHeader responseHeader;

		public TDAccountCloseTrailResponseDto.Header.ResponseHeader getResponseHeader() {
			return responseHeader;
		}

		public void setResponseHeader(TDAccountCloseTrailResponseDto.Header.ResponseHeader value) {
			this.responseHeader = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "requestMessageKey", "responseMessageInfo", "ubusTransaction",
				"hostTransaction", "hostParentTransaction", "customInfo" })
		public static class ResponseHeader {

			@XmlElement(name = "RequestMessageKey", required = true)
			protected TDAccountCloseTrailResponseDto.Header.ResponseHeader.RequestMessageKey requestMessageKey;
			@XmlElement(name = "ResponseMessageInfo", required = true)
			protected TDAccountCloseTrailResponseDto.Header.ResponseHeader.ResponseMessageInfo responseMessageInfo;
			@XmlElement(name = "UBUSTransaction", required = true)
			protected TDAccountCloseTrailResponseDto.Header.ResponseHeader.UBUSTransaction ubusTransaction;
			@XmlElement(name = "HostTransaction", required = true)
			protected TDAccountCloseTrailResponseDto.Header.ResponseHeader.HostTransaction hostTransaction;
			@XmlElement(name = "HostParentTransaction", required = true)
			protected TDAccountCloseTrailResponseDto.Header.ResponseHeader.HostParentTransaction hostParentTransaction;
			@XmlElement(name = "CustomInfo", required = true)
			protected String customInfo;

			public TDAccountCloseTrailResponseDto.Header.ResponseHeader.RequestMessageKey getRequestMessageKey() {
				return requestMessageKey;
			}

			public void setRequestMessageKey(
					TDAccountCloseTrailResponseDto.Header.ResponseHeader.RequestMessageKey value) {
				this.requestMessageKey = value;
			}

			public TDAccountCloseTrailResponseDto.Header.ResponseHeader.ResponseMessageInfo getResponseMessageInfo() {
				return responseMessageInfo;
			}

			public void setResponseMessageInfo(
					TDAccountCloseTrailResponseDto.Header.ResponseHeader.ResponseMessageInfo value) {
				this.responseMessageInfo = value;
			}

			public TDAccountCloseTrailResponseDto.Header.ResponseHeader.UBUSTransaction getUBUSTransaction() {
				return ubusTransaction;
			}

			public void setUBUSTransaction(TDAccountCloseTrailResponseDto.Header.ResponseHeader.UBUSTransaction value) {
				this.ubusTransaction = value;
			}

			public TDAccountCloseTrailResponseDto.Header.ResponseHeader.HostTransaction getHostTransaction() {
				return hostTransaction;
			}

			public void setHostTransaction(TDAccountCloseTrailResponseDto.Header.ResponseHeader.HostTransaction value) {
				this.hostTransaction = value;
			}

			public TDAccountCloseTrailResponseDto.Header.ResponseHeader.HostParentTransaction getHostParentTransaction() {
				return hostParentTransaction;
			}

			public void setHostParentTransaction(
					TDAccountCloseTrailResponseDto.Header.ResponseHeader.HostParentTransaction value) {
				this.hostParentTransaction = value;
			}

			public String getCustomInfo() {
				return customInfo;
			}

			public void setCustomInfo(String value) {
				this.customInfo = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostParentTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "requestUUID", "serviceRequestId", "serviceRequestVersion", "channelId" })
			public static class RequestMessageKey {

				@XmlElement(name = "RequestUUID", required = true)
				protected String requestUUID;
				@XmlElement(name = "ServiceRequestId", required = true)
				protected String serviceRequestId;
				@XmlElement(name = "ServiceRequestVersion")
				protected String serviceRequestVersion;
				@XmlElement(name = "ChannelId", required = true)
				protected String channelId;

				public String getRequestUUID() {
					return requestUUID;
				}

				public void setRequestUUID(String value) {
					this.requestUUID = value;
				}

				public String getServiceRequestId() {
					return serviceRequestId;
				}

				public void setServiceRequestId(String value) {
					this.serviceRequestId = value;
				}

				public String getServiceRequestVersion() {
					return serviceRequestVersion;
				}

				public void setServiceRequestVersion(String value) {
					this.serviceRequestVersion = value;
				}

				public String getChannelId() {
					return channelId;
				}

				public void setChannelId(String value) {
					this.channelId = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "bankId", "timeZone", "messageDateTime" })
			public static class ResponseMessageInfo {

				@XmlElement(name = "BankId", required = true)
				protected String bankId;
				@XmlElement(name = "TimeZone", required = true)
				protected String timeZone;
				@XmlElement(name = "MessageDateTime", required = true)
				@XmlSchemaType(name = "dateTime")
				protected XMLGregorianCalendar messageDateTime;

				public String getBankId() {
					return bankId;
				}

				public void setBankId(String value) {
					this.bankId = value;
				}

				public String getTimeZone() {
					return timeZone;
				}

				public void setTimeZone(String value) {
					this.timeZone = value;
				}

				public XMLGregorianCalendar getMessageDateTime() {
					return messageDateTime;
				}

				public void setMessageDateTime(XMLGregorianCalendar value) {
					this.messageDateTime = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class UBUSTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

		}

	}

}
