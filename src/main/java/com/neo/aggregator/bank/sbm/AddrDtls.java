package com.neo.aggregator.bank.sbm;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddrDtls implements Serializable {

	private static final long serialVersionUID = 1L;

	protected String addressLine1;
    
    protected String addressLine2;
    
    protected String addressLine3;
    
    protected String addressCategory;
    
    protected String city;
    
    protected String country;
    
    protected String freeTextLabel;
    
    protected String holdMailFlag;
    
    protected String prefAddress;
    
    protected String prefFormat;
    
    protected String state;
    
	protected String postalCode;
    
    protected String startDt;

    @XmlElement(name = "AddrLine1")
	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	@XmlElement(name = "AddrLine2")
	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	@XmlElement(name = "AddrLine3")
	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	@XmlElement(name = "AddrCategory")
	public String getAddressCategory() {
		return addressCategory;
	}

	public void setAddressCategory(String addressCategory) {
		this.addressCategory = addressCategory;
	}

	@XmlElement(name = "City")
	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}

	@XmlElement(name = "Country")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@XmlElement(name = "FreeTextLabel")
	public String getFreeTextLabel() {
		return freeTextLabel;
	}

	public void setFreeTextLabel(String freeTextLabel) {
		this.freeTextLabel = freeTextLabel;
	}

	
	@XmlElement(name = "HoldMailFlag")
	public String getHoldMailFlag() {
		return holdMailFlag;
	}

	public void setHoldMailFlag(String holdMailFlag) {
		this.holdMailFlag = holdMailFlag;
	}

	@XmlElement(name = "PrefAddr")
	public String getPrefAddress() {
		return prefAddress;
	}

	public void setPrefAddress(String prefAddress) {
		this.prefAddress = prefAddress;
	}

	@XmlElement(name = "PrefFormat")
	public String getPrefFormat() {
		return prefFormat;
	}

	public void setPrefFormat(String prefFormat) {
		this.prefFormat = prefFormat;
	}

	@XmlElement(name = "State")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@XmlElement(name = "PostalCode")
	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@XmlElement(name = "StartDt")
	public String getStartDt() {
		return startDt;
	}

	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}
    
}
