package com.neo.aggregator.bank.sbm.core.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "body"
})
@XmlRootElement(name = "FIXML")
public class TdAcctInquiryResponseDto {

    @XmlElement(name = "Header", required = true)
	protected TdAcctInquiryResponseDto.Header header;
    @XmlElement(name = "Body", required = true)
	protected TdAcctInquiryResponseDto.Body body;

	public TdAcctInquiryResponseDto.Header getHeader() {
        return header;
    }

	public void setHeader(TdAcctInquiryResponseDto.Header value) {
        this.header = value;
    }

	public TdAcctInquiryResponseDto.Body getBody() {
        return body;
    }

	public void setBody(TdAcctInquiryResponseDto.Body value) {
        this.body = value;
    }



    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
			"tdAcctInqResponse", "error"
    })
    public static class Body {

        @XmlElement(name = "TDAcctInqResponse", required = true)
		protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse tdAcctInqResponse;


		public TdAcctInquiryResponseDto.Body.TDAcctInqResponse getTDAcctInqResponse() {
            return tdAcctInqResponse;
        }


		public void setTDAcctInqResponse(TdAcctInquiryResponseDto.Body.TDAcctInqResponse value) {
            this.tdAcctInqResponse = value;
        }


		@XmlElement(name = "Error")
		protected Error error;

		public Error getError() {
			return error;
		}

		public void setError(Error value) {
			this.error = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)

		@XmlType(name = "")
		public static class Error {

			@XmlElement(name = "FIBusinessException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiBusinessException;

			@XmlElement(name = "FISystemException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException;

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFiSystemException() {
				return fiSystemException;
			}

			public void setFiSystemException(RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException) {
				this.fiSystemException = fiSystemException;
			}

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFIBusinessException() {
				return fiBusinessException;
			}

			public void setFIBusinessException(RetCustAddResponseDto.Body.Error.FIBusinessException value) {
				this.fiBusinessException = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)

			@XmlType(name = "", propOrder = { "errorDetail" })
			public static class FIBusinessException {

				@XmlElement(name = "ErrorDetail")
				protected RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail errorDetail;

				public RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail getErrorDetail() {
					return errorDetail;
				}

				public void setErrorDetail(RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail value) {
					this.errorDetail = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)

				@XmlType(name = "", propOrder = { "errorCode", "errorDesc", "errorType" })
				public static class ErrorDetail {

					@XmlElement(name = "ErrorCode")
					protected String errorCode;

					@XmlElement(name = "ErrorDesc")
					protected String errorDesc;

					@XmlElement(name = "ErrorType")
					protected String errorType;

					public String getErrorCode() {
						return errorCode;
					}

					public void setErrorCode(String value) {
						this.errorCode = value;
					}

					public String getErrorDesc() {
						return errorDesc;
					}

					public void setErrorDesc(String value) {
						this.errorDesc = value;
					}

					public String getErrorType() {
						return errorType;
					}

					public void setErrorType(String value) {
						this.errorType = value;
					}

				}

			}

		}

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "tdAcctInqRs",
            "tdAcctInqCustomData"
        })
        public static class TDAcctInqResponse {

            @XmlElement(name = "TDAcctInqRs", required = true)
			protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs tdAcctInqRs;
            @XmlElement(name = "TDAcctInq_CustomData", required = true)
			protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqCustomData tdAcctInqCustomData;


			public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs getTDAcctInqRs() {
                return tdAcctInqRs;
            }

			public void setTDAcctInqRs(TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs value) {
                this.tdAcctInqRs = value;
            }


			public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqCustomData getTDAcctInqCustomData() {
                return tdAcctInqCustomData;
            }


			public void setTDAcctInqCustomData(
					TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqCustomData value) {
                this.tdAcctInqCustomData = value;
            }


            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
            	"cumintcrii",
                "cumintpaidio",
                "interestfreq",
                "purposeofdeposit",
                "deposittype",
                "rptacctnum",
                "intcracct",
                "intrate"
            })
            public static class TDAcctInqCustomData {

                @XmlElement(name = "CUMINTCR_II")
				protected String cumintcrii;
                @XmlElement(name = "CUMINTPAID_IO")
				protected String cumintpaidio;
                @XmlElement(name = "INTERESTFREQ", required = true)
                protected String interestfreq;
                @XmlElement(name = "PURPOSEOFDEPOSIT", required = true)
                protected String purposeofdeposit;
                @XmlElement(name = "DEPOSITTYPE", required = true)
                protected String deposittype;
                @XmlElement(name = "RPTACCTNUM")
				protected String rptacctnum;
                @XmlElement(name = "INTCRACCT", required = true)
                protected String intcracct;
                @XmlElement(name = "INTRATE")
				protected String intrate;

                

				public String getCUMINTCRII() {
                    return cumintcrii;
                }


				public void setCUMINTCRII(String value) {
                    this.cumintcrii = value;
                }


				public String getCUMINTPAIDIO() {
                    return cumintpaidio;
                }


				public void setCUMINTPAIDIO(String value) {
                    this.cumintpaidio = value;
                }


                public String getINTERESTFREQ() {
                    return interestfreq;
                }


                public void setINTERESTFREQ(String value) {
                    this.interestfreq = value;
                }


                public String getPURPOSEOFDEPOSIT() {
                    return purposeofdeposit;
                }


                public void setPURPOSEOFDEPOSIT(String value) {
                    this.purposeofdeposit = value;
                }


                public String getDEPOSITTYPE() {
                    return deposittype;
                }


                public void setDEPOSITTYPE(String value) {
                    this.deposittype = value;
                }


				public String getRPTACCTNUM() {
                    return rptacctnum;
                }


				public void setRPTACCTNUM(String value) {
                    this.rptacctnum = value;
                }


                public String getINTCRACCT() {
                    return intcracct;
                }


                public void setINTCRACCT(String value) {
                    this.intcracct = value;
                }


				public String getINTRATE() {
                    return intrate;
                }


				public void setINTRATE(String value) {
                    this.intrate = value;
                }

            }

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "tdAcctId",
                "acctOpnDt",
                "modeOfOper",
                "bankAcctStatusCode",
                "custId",
                "tdAcctGenInfo",
                "acctBalCrDrInd",
                "acctBalAmt",
                "freezeStatusCode",
                "freezeReasonCode",
                "accrIntDrCrInd",
                "accrIntRate",
                "netIntCrDrInd",
                "netIntRate",
                "totalIntAmt",
                "initialDeposit",
                "currDeposit",
                "origMaturityAmt",
                "maturityAmt",
                "depositTerm",
                "maturityDt",
                "repayAcctId",
                "renewalDtls",
                "relPartyRec"
            })
            public static class TDAcctInqRs {

                @XmlElement(name = "TDAcctId", required = true)
				protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctId tdAcctId;
                @XmlElement(name = "AcctOpnDt", required = true)
                @XmlSchemaType(name = "dateTime")
				protected String acctOpnDt;
                @XmlElement(name = "ModeOfOper")
				protected String modeOfOper;
                @XmlElement(name = "BankAcctStatusCode", required = true)
                protected String bankAcctStatusCode;
                @XmlElement(name = "CustId", required = true)
				protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.CustId custId;
                @XmlElement(name = "TDAcctGenInfo", required = true)
				protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctGenInfo tdAcctGenInfo;
                @XmlElement(name = "AcctBalCrDrInd", required = true)
                protected String acctBalCrDrInd;
                @XmlElement(name = "AcctBalAmt", required = true)
				protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.AcctBalAmt acctBalAmt;
                @XmlElement(name = "FreezeStatusCode", required = true)
                protected String freezeStatusCode;
                @XmlElement(name = "FreezeReasonCode", required = true)
                protected String freezeReasonCode;
                @XmlElement(name = "AccrIntDrCrInd", required = true)
                protected String accrIntDrCrInd;
                @XmlElement(name = "AccrIntRate", required = true)
				protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.AccrIntRate accrIntRate;
                @XmlElement(name = "NetIntCrDrInd", required = true)
                protected String netIntCrDrInd;
                @XmlElement(name = "NetIntRate", required = true)
				protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.NetIntRate netIntRate;
                @XmlElement(name = "TotalIntAmt", required = true)
				protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TotalIntAmt totalIntAmt;
                @XmlElement(name = "InitialDeposit", required = true)
				protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.InitialDeposit initialDeposit;
                @XmlElement(name = "CurrDeposit", required = true)
				protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.CurrDeposit currDeposit;
                @XmlElement(name = "OrigMaturityAmt", required = true)
				protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.OrigMaturityAmt origMaturityAmt;
                @XmlElement(name = "MaturityAmt", required = true)
				protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.MaturityAmt maturityAmt;
                @XmlElement(name = "DepositTerm", required = true)
				protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.DepositTerm depositTerm;
                @XmlElement(name = "MaturityDt", required = true)
                @XmlSchemaType(name = "dateTime")
				protected String maturityDt;
                @XmlElement(name = "RepayAcctId", required = true)
				protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RepayAcctId repayAcctId;
                @XmlElement(name = "RenewalDtls", required = true)
				protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls renewalDtls;
                @XmlElement(name = "RelPartyRec", required = true)
				protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec relPartyRec;


				public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctId getTDAcctId() {
                    return tdAcctId;
                }


				public void setTDAcctId(TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctId value) {
                    this.tdAcctId = value;
                }


				public String getAcctOpnDt() {
                    return acctOpnDt;
                }


				public void setAcctOpnDt(String value) {
                    this.acctOpnDt = value;
                }


				public String getModeOfOper() {
                    return modeOfOper;
                }


				public void setModeOfOper(String value) {
                    this.modeOfOper = value;
                }


                public String getBankAcctStatusCode() {
                    return bankAcctStatusCode;
                }


                public void setBankAcctStatusCode(String value) {
                    this.bankAcctStatusCode = value;
                }


				public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.CustId getCustId() {
                    return custId;
                }


				public void setCustId(TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.CustId value) {
                    this.custId = value;
                }


				public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctGenInfo getTDAcctGenInfo() {
                    return tdAcctGenInfo;
                }


				public void setTDAcctGenInfo(
						TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctGenInfo value) {
                    this.tdAcctGenInfo = value;
                }


                public String getAcctBalCrDrInd() {
                    return acctBalCrDrInd;
                }


                public void setAcctBalCrDrInd(String value) {
                    this.acctBalCrDrInd = value;
                }


				public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.AcctBalAmt getAcctBalAmt() {
                    return acctBalAmt;
                }


				public void setAcctBalAmt(
						TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.AcctBalAmt value) {
                    this.acctBalAmt = value;
                }


                public String getFreezeStatusCode() {
                    return freezeStatusCode;
                }


                public void setFreezeStatusCode(String value) {
                    this.freezeStatusCode = value;
                }


                public String getFreezeReasonCode() {
                    return freezeReasonCode;
                }


                public void setFreezeReasonCode(String value) {
                    this.freezeReasonCode = value;
                }


                public String getAccrIntDrCrInd() {
                    return accrIntDrCrInd;
                }


                public void setAccrIntDrCrInd(String value) {
                    this.accrIntDrCrInd = value;
                }


				public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.AccrIntRate getAccrIntRate() {
                    return accrIntRate;
                }


				public void setAccrIntRate(
						TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.AccrIntRate value) {
                    this.accrIntRate = value;
                }


                public String getNetIntCrDrInd() {
                    return netIntCrDrInd;
                }


                public void setNetIntCrDrInd(String value) {
                    this.netIntCrDrInd = value;
                }


				public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.NetIntRate getNetIntRate() {
                    return netIntRate;
                }


				public void setNetIntRate(
						TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.NetIntRate value) {
                    this.netIntRate = value;
                }


				public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TotalIntAmt getTotalIntAmt() {
                    return totalIntAmt;
                }


				public void setTotalIntAmt(
						TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TotalIntAmt value) {
                    this.totalIntAmt = value;
                }


				public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.InitialDeposit getInitialDeposit() {
                    return initialDeposit;
                }


				public void setInitialDeposit(
						TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.InitialDeposit value) {
                    this.initialDeposit = value;
                }


				public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.CurrDeposit getCurrDeposit() {
                    return currDeposit;
                }


				public void setCurrDeposit(
						TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.CurrDeposit value) {
                    this.currDeposit = value;
                }


				public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.OrigMaturityAmt getOrigMaturityAmt() {
                    return origMaturityAmt;
                }


				public void setOrigMaturityAmt(
						TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.OrigMaturityAmt value) {
                    this.origMaturityAmt = value;
                }


				public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.MaturityAmt getMaturityAmt() {
                    return maturityAmt;
                }


				public void setMaturityAmt(
						TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.MaturityAmt value) {
                    this.maturityAmt = value;
                }


				public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.DepositTerm getDepositTerm() {
                    return depositTerm;
                }


				public void setDepositTerm(
						TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.DepositTerm value) {
                    this.depositTerm = value;
                }


				public String getMaturityDt() {
                    return maturityDt;
                }

				public void setMaturityDt(String value) {
                    this.maturityDt = value;
                }

				public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RepayAcctId getRepayAcctId() {
                    return repayAcctId;
                }


				public void setRepayAcctId(
						TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RepayAcctId value) {
                    this.repayAcctId = value;
                }

				public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls getRenewalDtls() {
                    return renewalDtls;
                }

				public void setRenewalDtls(
						TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls value) {
                    this.renewalDtls = value;
                }


				public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec getRelPartyRec() {
                    return relPartyRec;
                }


				public void setRelPartyRec(
						TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec value) {
                    this.relPartyRec = value;
                }

                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "value"
                })
                public static class AccrIntRate {

					protected String value;


					public String getValue() {
                        return value;
                    }


					public void setValue(String value) {
                        this.value = value;
                    }

                }

                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "amountValue",
                    "currencyCode"
                })
                public static class AcctBalAmt {

					protected String amountValue;
                    @XmlElement(required = true)
                    protected String currencyCode;


					public String getAmountValue() {
                        return amountValue;
                    }


					public void setAmountValue(String value) {
                        this.amountValue = value;
                    }


                    public String getCurrencyCode() {
                        return currencyCode;
                    }


                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "amountValue",
                    "currencyCode"
                })
                public static class CurrDeposit {

					protected String amountValue;
                    @XmlElement(required = true)
                    protected String currencyCode;


					public String getAmountValue() {
                        return amountValue;
                    }


					public void setAmountValue(String value) {
                        this.amountValue = value;
                    }


                    public String getCurrencyCode() {
                        return currencyCode;
                    }


                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "custId",
                    "personName"
                })
                public static class CustId {

                    @XmlElement(name = "CustId")
					protected String custId;
                    @XmlElement(name = "PersonName", required = true)
					protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.CustId.PersonName personName;


					public String getCustId() {
                        return custId;
                    }


					public void setCustId(String value) {
                        this.custId = value;
                    }


					public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.CustId.PersonName getPersonName() {
                        return personName;
                    }


					public void setPersonName(
							TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.CustId.PersonName value) {
                        this.personName = value;
                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "lastName",
                        "firstName",
                        "middleName",
                        "name",
                        "titlePrefix"
                    })
                    public static class PersonName {

                        @XmlElement(name = "LastName", required = true)
                        protected String lastName;
                        @XmlElement(name = "FirstName", required = true)
                        protected String firstName;
                        @XmlElement(name = "MiddleName", required = true)
                        protected String middleName;
                        @XmlElement(name = "Name", required = true)
                        protected String name;
                        @XmlElement(name = "TitlePrefix", required = true)
                        protected String titlePrefix;


                        public String getLastName() {
                            return lastName;
                        }


                        public void setLastName(String value) {
                            this.lastName = value;
                        }


                        public String getFirstName() {
                            return firstName;
                        }


                        public void setFirstName(String value) {
                            this.firstName = value;
                        }


                        public String getMiddleName() {
                            return middleName;
                        }


                        public void setMiddleName(String value) {
                            this.middleName = value;
                        }


                        public String getName() {
                            return name;
                        }


                        public void setName(String value) {
                            this.name = value;
                        }


                        public String getTitlePrefix() {
                            return titlePrefix;
                        }


                        public void setTitlePrefix(String value) {
                            this.titlePrefix = value;
                        }

                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "days",
                    "months"
                })
                public static class DepositTerm {

                    @XmlElement(name = "Days")
					protected String days;
                    @XmlElement(name = "Months")
					protected String months;


					public String getDays() {
                        return days;
                    }


					public void setDays(String value) {
                        this.days = value;
                    }


					public String getMonths() {
                        return months;
                    }


					public void setMonths(String value) {
                        this.months = value;
                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "amountValue",
                    "currencyCode"
                })
                public static class InitialDeposit {

					protected String amountValue;
                    @XmlElement(required = true)
                    protected String currencyCode;


					public String getAmountValue() {
                        return amountValue;
                    }


					public void setAmountValue(String value) {
                        this.amountValue = value;
                    }


                    public String getCurrencyCode() {
                        return currencyCode;
                    }


                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "amountValue",
                    "currencyCode"
                })
                public static class MaturityAmt {

					protected String amountValue;
                    @XmlElement(required = true)
                    protected String currencyCode;


					public String getAmountValue() {
                        return amountValue;
                    }


					public void setAmountValue(String value) {
                        this.amountValue = value;
                    }


                    public String getCurrencyCode() {
                        return currencyCode;
                    }


                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "value"
                })
                public static class NetIntRate {

					protected String value;


					public String getValue() {
                        return value;
                    }


					public void setValue(String value) {
                        this.value = value;
                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "amountValue",
                    "currencyCode"
                })
                public static class OrigMaturityAmt {

					protected String amountValue;
                    @XmlElement(required = true)
                    protected String currencyCode;


					public String getAmountValue() {
                        return amountValue;
                    }


					public void setAmountValue(String value) {
                        this.amountValue = value;
                    }


                    public String getCurrencyCode() {
                        return currencyCode;
                    }


                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "relPartyType",
                    "relPartyTypeDesc",
                    "relPartyCode",
                    "relPartyCodeDesc",
                    "custId",
                    "relPartyContactInfo",
                    "recDelFlg"
                })
                public static class RelPartyRec {

                    @XmlElement(name = "RelPartyType", required = true)
                    protected String relPartyType;
                    @XmlElement(name = "RelPartyTypeDesc", required = true)
                    protected String relPartyTypeDesc;
                    @XmlElement(name = "RelPartyCode", required = true)
                    protected String relPartyCode;
                    @XmlElement(name = "RelPartyCodeDesc", required = true)
                    protected String relPartyCodeDesc;
                    @XmlElement(name = "CustId", required = true)
					protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec.CustId custId;
                    @XmlElement(name = "RelPartyContactInfo", required = true)
					protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec.RelPartyContactInfo relPartyContactInfo;
                    @XmlElement(name = "RecDelFlg", required = true)
                    protected String recDelFlg;


                    public String getRelPartyType() {
                        return relPartyType;
                    }


                    public void setRelPartyType(String value) {
                        this.relPartyType = value;
                    }


                    public String getRelPartyTypeDesc() {
                        return relPartyTypeDesc;
                    }


                    public void setRelPartyTypeDesc(String value) {
                        this.relPartyTypeDesc = value;
                    }


                    public String getRelPartyCode() {
                        return relPartyCode;
                    }


                    public void setRelPartyCode(String value) {
                        this.relPartyCode = value;
                    }


                    public String getRelPartyCodeDesc() {
                        return relPartyCodeDesc;
                    }


                    public void setRelPartyCodeDesc(String value) {
                        this.relPartyCodeDesc = value;
                    }


					public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec.CustId getCustId() {
                        return custId;
                    }


					public void setCustId(
							TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec.CustId value) {
                        this.custId = value;
                    }


					public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec.RelPartyContactInfo getRelPartyContactInfo() {
                        return relPartyContactInfo;
                    }


					public void setRelPartyContactInfo(
							TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec.RelPartyContactInfo value) {
                        this.relPartyContactInfo = value;
                    }


                    public String getRecDelFlg() {
                        return recDelFlg;
                    }


                    public void setRecDelFlg(String value) {
                        this.recDelFlg = value;
                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "custId",
                        "personName"
                    })
                    public static class CustId {

                        @XmlElement(name = "CustId")
						protected String custId;
                        @XmlElement(name = "PersonName", required = true)
						protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec.CustId.PersonName personName;


						public String getCustId() {
                            return custId;
                        }


						public void setCustId(String value) {
                            this.custId = value;
                        }


						public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec.CustId.PersonName getPersonName() {
                            return personName;
                        }


						public void setPersonName(
								TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec.CustId.PersonName value) {
                            this.personName = value;
                        }



                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "lastName",
                            "firstName",
                            "middleName",
                            "name",
                            "titlePrefix"
                        })
                        public static class PersonName {

                            @XmlElement(name = "LastName", required = true)
                            protected String lastName;
                            @XmlElement(name = "FirstName", required = true)
                            protected String firstName;
                            @XmlElement(name = "MiddleName", required = true)
                            protected String middleName;
                            @XmlElement(name = "Name", required = true)
                            protected String name;
                            @XmlElement(name = "TitlePrefix", required = true)
                            protected String titlePrefix;


                            public String getLastName() {
                                return lastName;
                            }


                            public void setLastName(String value) {
                                this.lastName = value;
                            }


                            public String getFirstName() {
                                return firstName;
                            }


                            public void setFirstName(String value) {
                                this.firstName = value;
                            }


                            public String getMiddleName() {
                                return middleName;
                            }


                            public void setMiddleName(String value) {
                                this.middleName = value;
                            }


                            public String getName() {
                                return name;
                            }


                            public void setName(String value) {
                                this.name = value;
                            }


                            public String getTitlePrefix() {
                                return titlePrefix;
                            }


                            public void setTitlePrefix(String value) {
                                this.titlePrefix = value;
                            }

                        }

                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "phoneNum",
                        "emailAddr",
                        "postAddr"
                    })
                    public static class RelPartyContactInfo {

                        @XmlElement(name = "PhoneNum", required = true)
						protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec.RelPartyContactInfo.PhoneNum phoneNum;
                        @XmlElement(name = "EmailAddr", required = true)
                        protected String emailAddr;
                        @XmlElement(name = "PostAddr", required = true)
						protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec.RelPartyContactInfo.PostAddr postAddr;


						public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec.RelPartyContactInfo.PhoneNum getPhoneNum() {
                            return phoneNum;
                        }


						public void setPhoneNum(
								TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec.RelPartyContactInfo.PhoneNum value) {
                            this.phoneNum = value;
                        }


                        public String getEmailAddr() {
                            return emailAddr;
                        }


                        public void setEmailAddr(String value) {
                            this.emailAddr = value;
                        }


						public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec.RelPartyContactInfo.PostAddr getPostAddr() {
                            return postAddr;
                        }


						public void setPostAddr(
								TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RelPartyRec.RelPartyContactInfo.PostAddr value) {
                            this.postAddr = value;
                        }



                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "telephoneNum",
                            "faxNum",
                            "telexNum"
                        })
                        public static class PhoneNum {

                            @XmlElement(name = "TelephoneNum", required = true)
                            protected String telephoneNum;
                            @XmlElement(name = "FaxNum", required = true)
                            protected String faxNum;
                            @XmlElement(name = "TelexNum", required = true)
                            protected String telexNum;


                            public String getTelephoneNum() {
                                return telephoneNum;
                            }


                            public void setTelephoneNum(String value) {
                                this.telephoneNum = value;
                            }


                            public String getFaxNum() {
                                return faxNum;
                            }


                            public void setFaxNum(String value) {
                                this.faxNum = value;
                            }


                            public String getTelexNum() {
                                return telexNum;
                            }


                            public void setTelexNum(String value) {
                                this.telexNum = value;
                            }

                        }



                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "addr1",
                            "addr2",
                            "addr3",
                            "city",
                            "stateProv",
                            "postalCode",
                            "country",
                            "addrType"
                        })
                        public static class PostAddr {

                            @XmlElement(name = "Addr1", required = true)
                            protected String addr1;
                            @XmlElement(name = "Addr2", required = true)
                            protected String addr2;
                            @XmlElement(name = "Addr3", required = true)
                            protected String addr3;
                            @XmlElement(name = "City", required = true)
                            protected String city;
                            @XmlElement(name = "StateProv", required = true)
                            protected String stateProv;
                            @XmlElement(name = "PostalCode")
							protected String postalCode;
                            @XmlElement(name = "Country", required = true)
                            protected String country;
                            @XmlElement(name = "AddrType", required = true)
                            protected String addrType;


                            public String getAddr1() {
                                return addr1;
                            }


                            public void setAddr1(String value) {
                                this.addr1 = value;
                            }


                            public String getAddr2() {
                                return addr2;
                            }


                            public void setAddr2(String value) {
                                this.addr2 = value;
                            }


                            public String getAddr3() {
                                return addr3;
                            }


                            public void setAddr3(String value) {
                                this.addr3 = value;
                            }


                            public String getCity() {
                                return city;
                            }


                            public void setCity(String value) {
                                this.city = value;
                            }


                            public String getStateProv() {
                                return stateProv;
                            }


                            public void setStateProv(String value) {
                                this.stateProv = value;
                            }


							public String getPostalCode() {
                                return postalCode;
                            }


							public void setPostalCode(String value) {
                                this.postalCode = value;
                            }


                            public String getCountry() {
                                return country;
                            }


                            public void setCountry(String value) {
                                this.country = value;
                            }


                            public String getAddrType() {
                                return addrType;
                            }


                            public void setAddrType(String value) {
                                this.addrType = value;
                            }

                        }

                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "autoCloseOnMaturityFlg",
                    "autoRenewalflg",
                    "maxNumOfRenewalAllwd",
                    "renewalTerm",
                    "renewalSchm",
                    "genLedgerSubHead",
                    "intTblCode",
                    "renewalCurCode",
                    "renewalOption",
                    "renewalAmt",
                    "renewalAddnlAmt",
                    "srcAcctId"
                })
                public static class RenewalDtls {

                    @XmlElement(name = "AutoCloseOnMaturityFlg", required = true)
                    protected String autoCloseOnMaturityFlg;
                    @XmlElement(name = "AutoRenewalflg", required = true)
                    protected String autoRenewalflg;
                    @XmlElement(name = "MaxNumOfRenewalAllwd")
					protected String maxNumOfRenewalAllwd;
                    @XmlElement(name = "RenewalTerm", required = true)
					protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.RenewalTerm renewalTerm;
                    @XmlElement(name = "RenewalSchm", required = true)
					protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.RenewalSchm renewalSchm;
                    @XmlElement(name = "GenLedgerSubHead", required = true)
					protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.GenLedgerSubHead genLedgerSubHead;
                    @XmlElement(name = "IntTblCode", required = true)
                    protected String intTblCode;
                    @XmlElement(name = "RenewalCurCode", required = true)
                    protected String renewalCurCode;
                    @XmlElement(name = "RenewalOption", required = true)
                    protected String renewalOption;
                    @XmlElement(name = "RenewalAmt", required = true)
					protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.RenewalAmt renewalAmt;
                    @XmlElement(name = "RenewalAddnlAmt", required = true)
					protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.RenewalAddnlAmt renewalAddnlAmt;
                    @XmlElement(name = "SrcAcctId", required = true)
					protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.SrcAcctId srcAcctId;


                    public String getAutoCloseOnMaturityFlg() {
                        return autoCloseOnMaturityFlg;
                    }


                    public void setAutoCloseOnMaturityFlg(String value) {
                        this.autoCloseOnMaturityFlg = value;
                    }


                    public String getAutoRenewalflg() {
                        return autoRenewalflg;
                    }


                    public void setAutoRenewalflg(String value) {
                        this.autoRenewalflg = value;
                    }


					public String getMaxNumOfRenewalAllwd() {
                        return maxNumOfRenewalAllwd;
                    }


					public void setMaxNumOfRenewalAllwd(String value) {
                        this.maxNumOfRenewalAllwd = value;
                    }


					public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.RenewalTerm getRenewalTerm() {
                        return renewalTerm;
                    }


					public void setRenewalTerm(
							TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.RenewalTerm value) {
                        this.renewalTerm = value;
                    }


					public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.RenewalSchm getRenewalSchm() {
                        return renewalSchm;
                    }


					public void setRenewalSchm(
							TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.RenewalSchm value) {
                        this.renewalSchm = value;
                    }


					public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.GenLedgerSubHead getGenLedgerSubHead() {
                        return genLedgerSubHead;
                    }


					public void setGenLedgerSubHead(
							TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.GenLedgerSubHead value) {
                        this.genLedgerSubHead = value;
                    }


                    public String getIntTblCode() {
                        return intTblCode;
                    }


                    public void setIntTblCode(String value) {
                        this.intTblCode = value;
                    }


                    public String getRenewalCurCode() {
                        return renewalCurCode;
                    }


                    public void setRenewalCurCode(String value) {
                        this.renewalCurCode = value;
                    }


                    public String getRenewalOption() {
                        return renewalOption;
                    }


                    public void setRenewalOption(String value) {
                        this.renewalOption = value;
                    }


					public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.RenewalAmt getRenewalAmt() {
                        return renewalAmt;
                    }


					public void setRenewalAmt(
							TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.RenewalAmt value) {
                        this.renewalAmt = value;
                    }


					public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.RenewalAddnlAmt getRenewalAddnlAmt() {
                        return renewalAddnlAmt;
                    }


					public void setRenewalAddnlAmt(
							TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.RenewalAddnlAmt value) {
                        this.renewalAddnlAmt = value;
                    }


					public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.SrcAcctId getSrcAcctId() {
                        return srcAcctId;
                    }


					public void setSrcAcctId(
							TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.SrcAcctId value) {
                        this.srcAcctId = value;
                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "genLedgerSubHeadCode",
                        "curCode"
                    })
                    public static class GenLedgerSubHead {

                        @XmlElement(name = "GenLedgerSubHeadCode")
                        protected short genLedgerSubHeadCode;
                        @XmlElement(name = "CurCode", required = true)
                        protected String curCode;


                        public short getGenLedgerSubHeadCode() {
                            return genLedgerSubHeadCode;
                        }


                        public void setGenLedgerSubHeadCode(short value) {
                            this.genLedgerSubHeadCode = value;
                        }


                        public String getCurCode() {
                            return curCode;
                        }


                        public void setCurCode(String value) {
                            this.curCode = value;
                        }

                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "amountValue",
                        "currencyCode"
                    })
                    public static class RenewalAddnlAmt {

						protected String amountValue;
                        @XmlElement(required = true)
                        protected String currencyCode;


						public String getAmountValue() {
                            return amountValue;
                        }


						public void setAmountValue(String value) {
                            this.amountValue = value;
                        }


                        public String getCurrencyCode() {
                            return currencyCode;
                        }


                        public void setCurrencyCode(String value) {
                            this.currencyCode = value;
                        }

                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "amountValue",
                        "currencyCode"
                    })
                    public static class RenewalAmt {

						protected String amountValue;
                        @XmlElement(required = true)
                        protected String currencyCode;


						public String getAmountValue() {
                            return amountValue;
                        }


						public void setAmountValue(String value) {
                            this.amountValue = value;
                        }


                        public String getCurrencyCode() {
                            return currencyCode;
                        }


                        public void setCurrencyCode(String value) {
                            this.currencyCode = value;
                        }

                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "schmCode",
                        "schmType"
                    })
                    public static class RenewalSchm {

                        @XmlElement(name = "SchmCode", required = true)
                        protected String schmCode;
                        @XmlElement(name = "SchmType", required = true)
                        protected String schmType;


                        public String getSchmCode() {
                            return schmCode;
                        }


                        public void setSchmCode(String value) {
                            this.schmCode = value;
                        }


                        public String getSchmType() {
                            return schmType;
                        }


                        public void setSchmType(String value) {
                            this.schmType = value;
                        }

                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "days",
                        "months"
                    })
                    public static class RenewalTerm {

                        @XmlElement(name = "Days")
						protected String days;
                        @XmlElement(name = "Months")
						protected String months;


						public String getDays() {
                            return days;
                        }


						public void setDays(String value) {
                            this.days = value;
                        }


						public String getMonths() {
                            return months;
                        }


						public void setMonths(String value) {
                            this.months = value;
                        }

                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "acctId",
                        "acctType",
                        "acctCurr",
                        "bankInfo"
                    })
                    public static class SrcAcctId {

                        @XmlElement(name = "AcctId", required = true)
                        protected String acctId;
                        @XmlElement(name = "AcctType", required = true)
						protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.SrcAcctId.AcctType acctType;
                        @XmlElement(name = "AcctCurr", required = true)
                        protected String acctCurr;
                        @XmlElement(name = "BankInfo", required = true)
						protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.SrcAcctId.BankInfo bankInfo;


                        public String getAcctId() {
                            return acctId;
                        }


                        public void setAcctId(String value) {
                            this.acctId = value;
                        }


						public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.SrcAcctId.AcctType getAcctType() {
                            return acctType;
                        }


						public void setAcctType(
								TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.SrcAcctId.AcctType value) {
                            this.acctType = value;
                        }


                        public String getAcctCurr() {
                            return acctCurr;
                        }


                        public void setAcctCurr(String value) {
                            this.acctCurr = value;
                        }


						public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.SrcAcctId.BankInfo getBankInfo() {
                            return bankInfo;
                        }


						public void setBankInfo(
								TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.SrcAcctId.BankInfo value) {
                            this.bankInfo = value;
                        }



                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "schmCode",
                            "schmType"
                        })
                        public static class AcctType {

                            @XmlElement(name = "SchmCode", required = true)
                            protected String schmCode;
                            @XmlElement(name = "SchmType", required = true)
                            protected String schmType;


                            public String getSchmCode() {
                                return schmCode;
                            }


                            public void setSchmCode(String value) {
                                this.schmCode = value;
                            }


                            public String getSchmType() {
                                return schmType;
                            }


                            public void setSchmType(String value) {
                                this.schmType = value;
                            }

                        }



                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "bankId",
                            "name",
                            "branchId",
                            "branchName",
                            "postAddr"
                        })
                        public static class BankInfo {

                            @XmlElement(name = "BankId", required = true)
                            protected String bankId;
                            @XmlElement(name = "Name", required = true)
                            protected String name;
                            @XmlElement(name = "BranchId", required = true)
                            protected String branchId;
                            @XmlElement(name = "BranchName", required = true)
                            protected String branchName;
                            @XmlElement(name = "PostAddr", required = true)
							protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.SrcAcctId.BankInfo.PostAddr postAddr;


                            public String getBankId() {
                                return bankId;
                            }


                            public void setBankId(String value) {
                                this.bankId = value;
                            }


                            public String getName() {
                                return name;
                            }


                            public void setName(String value) {
                                this.name = value;
                            }


                            public String getBranchId() {
                                return branchId;
                            }


                            public void setBranchId(String value) {
                                this.branchId = value;
                            }


                            public String getBranchName() {
                                return branchName;
                            }


                            public void setBranchName(String value) {
                                this.branchName = value;
                            }


							public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.SrcAcctId.BankInfo.PostAddr getPostAddr() {
                                return postAddr;
                            }


							public void setPostAddr(
									TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RenewalDtls.SrcAcctId.BankInfo.PostAddr value) {
                                this.postAddr = value;
                            }



                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "addr1",
                                "addr2",
                                "addr3",
                                "city",
                                "stateProv",
                                "postalCode",
                                "country",
                                "addrType"
                            })
                            public static class PostAddr {

                                @XmlElement(name = "Addr1", required = true)
                                protected String addr1;
                                @XmlElement(name = "Addr2", required = true)
                                protected String addr2;
                                @XmlElement(name = "Addr3", required = true)
                                protected String addr3;
                                @XmlElement(name = "City", required = true)
                                protected String city;
                                @XmlElement(name = "StateProv", required = true)
                                protected String stateProv;
                                @XmlElement(name = "PostalCode", required = true)
                                protected String postalCode;
                                @XmlElement(name = "Country", required = true)
                                protected String country;
                                @XmlElement(name = "AddrType", required = true)
                                protected String addrType;


                                public String getAddr1() {
                                    return addr1;
                                }


                                public void setAddr1(String value) {
                                    this.addr1 = value;
                                }


                                public String getAddr2() {
                                    return addr2;
                                }


                                public void setAddr2(String value) {
                                    this.addr2 = value;
                                }


                                public String getAddr3() {
                                    return addr3;
                                }


                                public void setAddr3(String value) {
                                    this.addr3 = value;
                                }


                                public String getCity() {
                                    return city;
                                }


                                public void setCity(String value) {
                                    this.city = value;
                                }


                                public String getStateProv() {
                                    return stateProv;
                                }


                                public void setStateProv(String value) {
                                    this.stateProv = value;
                                }


                                public String getPostalCode() {
                                    return postalCode;
                                }


                                public void setPostalCode(String value) {
                                    this.postalCode = value;
                                }


                                public String getCountry() {
                                    return country;
                                }


                                public void setCountry(String value) {
                                    this.country = value;
                                }


                                public String getAddrType() {
                                    return addrType;
                                }


                                public void setAddrType(String value) {
                                    this.addrType = value;
                                }

                            }

                        }

                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "acctId",
                    "acctType",
                    "acctCurr",
                    "bankInfo"
                })
                public static class RepayAcctId {

                    @XmlElement(name = "AcctId")
					protected String acctId;
                    @XmlElement(name = "AcctType", required = true)
					protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RepayAcctId.AcctType acctType;
                    @XmlElement(name = "AcctCurr", required = true)
                    protected String acctCurr;
                    @XmlElement(name = "BankInfo", required = true)
					protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RepayAcctId.BankInfo bankInfo;


					public String getAcctId() {
                        return acctId;
                    }


					public void setAcctId(String value) {
                        this.acctId = value;
                    }


					public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RepayAcctId.AcctType getAcctType() {
                        return acctType;
                    }


					public void setAcctType(
							TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RepayAcctId.AcctType value) {
                        this.acctType = value;
                    }


                    public String getAcctCurr() {
                        return acctCurr;
                    }


                    public void setAcctCurr(String value) {
                        this.acctCurr = value;
                    }


					public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RepayAcctId.BankInfo getBankInfo() {
                        return bankInfo;
                    }


					public void setBankInfo(
							TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RepayAcctId.BankInfo value) {
                        this.bankInfo = value;
                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "schmCode",
                        "schmType"
                    })
                    public static class AcctType {

                        @XmlElement(name = "SchmCode", required = true)
                        protected String schmCode;
                        @XmlElement(name = "SchmType", required = true)
                        protected String schmType;


                        public String getSchmCode() {
                            return schmCode;
                        }


                        public void setSchmCode(String value) {
                            this.schmCode = value;
                        }


                        public String getSchmType() {
                            return schmType;
                        }


                        public void setSchmType(String value) {
                            this.schmType = value;
                        }

                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "bankId",
                        "name",
                        "branchId",
                        "branchName",
                        "postAddr"
                    })
                    public static class BankInfo {

                        @XmlElement(name = "BankId", required = true)
                        protected String bankId;
                        @XmlElement(name = "Name", required = true)
                        protected String name;
                        @XmlElement(name = "BranchId", required = true)
                        protected String branchId;
                        @XmlElement(name = "BranchName", required = true)
                        protected String branchName;
                        @XmlElement(name = "PostAddr", required = true)
						protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RepayAcctId.BankInfo.PostAddr postAddr;


                        public String getBankId() {
                            return bankId;
                        }


                        public void setBankId(String value) {
                            this.bankId = value;
                        }


                        public String getName() {
                            return name;
                        }


                        public void setName(String value) {
                            this.name = value;
                        }


                        public String getBranchId() {
                            return branchId;
                        }


                        public void setBranchId(String value) {
                            this.branchId = value;
                        }


                        public String getBranchName() {
                            return branchName;
                        }


                        public void setBranchName(String value) {
                            this.branchName = value;
                        }


						public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RepayAcctId.BankInfo.PostAddr getPostAddr() {
                            return postAddr;
                        }


						public void setPostAddr(
								TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.RepayAcctId.BankInfo.PostAddr value) {
                            this.postAddr = value;
                        }



                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "addr1",
                            "addr2",
                            "addr3",
                            "city",
                            "stateProv",
                            "postalCode",
                            "country",
                            "addrType"
                        })
                        public static class PostAddr {

                            @XmlElement(name = "Addr1", required = true)
                            protected String addr1;
                            @XmlElement(name = "Addr2", required = true)
                            protected String addr2;
                            @XmlElement(name = "Addr3", required = true)
                            protected String addr3;
                            @XmlElement(name = "City", required = true)
                            protected String city;
                            @XmlElement(name = "StateProv", required = true)
                            protected String stateProv;
                            @XmlElement(name = "PostalCode", required = true)
                            protected String postalCode;
                            @XmlElement(name = "Country", required = true)
                            protected String country;
                            @XmlElement(name = "AddrType", required = true)
                            protected String addrType;


                            public String getAddr1() {
                                return addr1;
                            }


                            public void setAddr1(String value) {
                                this.addr1 = value;
                            }


                            public String getAddr2() {
                                return addr2;
                            }


                            public void setAddr2(String value) {
                                this.addr2 = value;
                            }


                            public String getAddr3() {
                                return addr3;
                            }


                            public void setAddr3(String value) {
                                this.addr3 = value;
                            }


                            public String getCity() {
                                return city;
                            }


                            public void setCity(String value) {
                                this.city = value;
                            }


                            public String getStateProv() {
                                return stateProv;
                            }


                            public void setStateProv(String value) {
                                this.stateProv = value;
                            }


                            public String getPostalCode() {
                                return postalCode;
                            }


                            public void setPostalCode(String value) {
                                this.postalCode = value;
                            }


                            public String getCountry() {
                                return country;
                            }


                            public void setCountry(String value) {
                                this.country = value;
                            }


                            public String getAddrType() {
                                return addrType;
                            }


                            public void setAddrType(String value) {
                                this.addrType = value;
                            }

                        }

                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "eotEnabled",
                    "drIntMethodInd",
                    "genLedgerSubHead",
                    "acctName",
                    "acctShortName",
                    "acctStmtMode",
                    "acctStmtFreq",
                    "despatchMode"
                })
                public static class TDAcctGenInfo {

                    @XmlElement(name = "EotEnabled", required = true)
                    protected String eotEnabled;
                    @XmlElement(name = "DrIntMethodInd", required = true)
                    protected String drIntMethodInd;
                    @XmlElement(name = "GenLedgerSubHead", required = true)
					protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctGenInfo.GenLedgerSubHead genLedgerSubHead;
                    @XmlElement(name = "AcctName", required = true)
                    protected String acctName;
                    @XmlElement(name = "AcctShortName", required = true)
                    protected String acctShortName;
                    @XmlElement(name = "AcctStmtMode", required = true)
                    protected String acctStmtMode;
                    @XmlElement(name = "AcctStmtFreq", required = true)
					protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctGenInfo.AcctStmtFreq acctStmtFreq;
                    @XmlElement(name = "DespatchMode", required = true)
                    protected String despatchMode;


                    public String getEotEnabled() {
                        return eotEnabled;
                    }


                    public void setEotEnabled(String value) {
                        this.eotEnabled = value;
                    }


                    public String getDrIntMethodInd() {
                        return drIntMethodInd;
                    }


                    public void setDrIntMethodInd(String value) {
                        this.drIntMethodInd = value;
                    }


					public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctGenInfo.GenLedgerSubHead getGenLedgerSubHead() {
                        return genLedgerSubHead;
                    }


					public void setGenLedgerSubHead(
							TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctGenInfo.GenLedgerSubHead value) {
                        this.genLedgerSubHead = value;
                    }


                    public String getAcctName() {
                        return acctName;
                    }


                    public void setAcctName(String value) {
                        this.acctName = value;
                    }


                    public String getAcctShortName() {
                        return acctShortName;
                    }


                    public void setAcctShortName(String value) {
                        this.acctShortName = value;
                    }


                    public String getAcctStmtMode() {
                        return acctStmtMode;
                    }


                    public void setAcctStmtMode(String value) {
                        this.acctStmtMode = value;
                    }


					public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctGenInfo.AcctStmtFreq getAcctStmtFreq() {
                        return acctStmtFreq;
                    }


					public void setAcctStmtFreq(
							TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctGenInfo.AcctStmtFreq value) {
                        this.acctStmtFreq = value;
                    }


                    public String getDespatchMode() {
                        return despatchMode;
                    }


                    public void setDespatchMode(String value) {
                        this.despatchMode = value;
                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "cal",
                        "type",
                        "startDt",
                        "weekDay",
                        "weekNum",
                        "holStat"
                    })
                    public static class AcctStmtFreq {

                        @XmlElement(name = "Cal", required = true)
                        protected String cal;
                        @XmlElement(name = "Type", required = true)
                        protected String type;
                        @XmlElement(name = "StartDt")
						protected String startDt;
                        @XmlElement(name = "WeekDay")
						protected String weekDay;
                        @XmlElement(name = "WeekNum", required = true)
                        protected String weekNum;
                        @XmlElement(name = "HolStat", required = true)
                        protected String holStat;


                        public String getCal() {
                            return cal;
                        }


                        public void setCal(String value) {
                            this.cal = value;
                        }


                        public String getType() {
                            return type;
                        }


                        public void setType(String value) {
                            this.type = value;
                        }


						public String getStartDt() {
                            return startDt;
                        }


						public void setStartDt(String value) {
                            this.startDt = value;
                        }


						public String getWeekDay() {
                            return weekDay;
                        }


						public void setWeekDay(String value) {
                            this.weekDay = value;
                        }


                        public String getWeekNum() {
                            return weekNum;
                        }


                        public void setWeekNum(String value) {
                            this.weekNum = value;
                        }


                        public String getHolStat() {
                            return holStat;
                        }


                        public void setHolStat(String value) {
                            this.holStat = value;
                        }

                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "genLedgerSubHeadCode",
                        "curCode"
                    })
                    public static class GenLedgerSubHead {

                        @XmlElement(name = "GenLedgerSubHeadCode")
                        protected short genLedgerSubHeadCode;
                        @XmlElement(name = "CurCode", required = true)
                        protected String curCode;


                        public short getGenLedgerSubHeadCode() {
                            return genLedgerSubHeadCode;
                        }


                        public void setGenLedgerSubHeadCode(short value) {
                            this.genLedgerSubHeadCode = value;
                        }


                        public String getCurCode() {
                            return curCode;
                        }


                        public void setCurCode(String value) {
                            this.curCode = value;
                        }

                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "acctId",
                    "acctType",
                    "acctCurr",
                    "bankInfo"
                })
                public static class TDAcctId {

                    @XmlElement(name = "AcctId")
					protected String acctId;
                    @XmlElement(name = "AcctType", required = true)
					protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctId.AcctType acctType;
                    @XmlElement(name = "AcctCurr", required = true)
                    protected String acctCurr;
                    @XmlElement(name = "BankInfo", required = true)
					protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctId.BankInfo bankInfo;


					public String getAcctId() {
                        return acctId;
                    }


					public void setAcctId(String value) {
                        this.acctId = value;
                    }


					public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctId.AcctType getAcctType() {
                        return acctType;
                    }


					public void setAcctType(
							TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctId.AcctType value) {
                        this.acctType = value;
                    }


                    public String getAcctCurr() {
                        return acctCurr;
                    }


                    public void setAcctCurr(String value) {
                        this.acctCurr = value;
                    }


					public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctId.BankInfo getBankInfo() {
                        return bankInfo;
                    }


					public void setBankInfo(
							TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctId.BankInfo value) {
                        this.bankInfo = value;
                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "schmCode",
                        "schmType"
                    })
                    public static class AcctType {

                        @XmlElement(name = "SchmCode", required = true)
                        protected String schmCode;
                        @XmlElement(name = "SchmType", required = true)
                        protected String schmType;


                        public String getSchmCode() {
                            return schmCode;
                        }


                        public void setSchmCode(String value) {
                            this.schmCode = value;
                        }


                        public String getSchmType() {
                            return schmType;
                        }


                        public void setSchmType(String value) {
                            this.schmType = value;
                        }

                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "bankId",
                        "name",
                        "branchId",
                        "branchName",
                        "postAddr"
                    })
                    public static class BankInfo {

                        @XmlElement(name = "BankId", required = true)
                        protected String bankId;
                        @XmlElement(name = "Name", required = true)
                        protected String name;
                        @XmlElement(name = "BranchId", required = true)
                        protected String branchId;
                        @XmlElement(name = "BranchName", required = true)
                        protected String branchName;
                        @XmlElement(name = "PostAddr", required = true)
						protected TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctId.BankInfo.PostAddr postAddr;


                        public String getBankId() {
                            return bankId;
                        }


                        public void setBankId(String value) {
                            this.bankId = value;
                        }


                        public String getName() {
                            return name;
                        }


                        public void setName(String value) {
                            this.name = value;
                        }


                        public String getBranchId() {
                            return branchId;
                        }


                        public void setBranchId(String value) {
                            this.branchId = value;
                        }


                        public String getBranchName() {
                            return branchName;
                        }


                        public void setBranchName(String value) {
                            this.branchName = value;
                        }


						public TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctId.BankInfo.PostAddr getPostAddr() {
                            return postAddr;
                        }


						public void setPostAddr(
								TdAcctInquiryResponseDto.Body.TDAcctInqResponse.TDAcctInqRs.TDAcctId.BankInfo.PostAddr value) {
                            this.postAddr = value;
                        }



                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "addr1",
                            "addr2",
                            "addr3",
                            "city",
                            "stateProv",
                            "postalCode",
                            "country",
                            "addrType"
                        })
                        public static class PostAddr {

                            @XmlElement(name = "Addr1", required = true)
                            protected String addr1;
                            @XmlElement(name = "Addr2", required = true)
                            protected String addr2;
                            @XmlElement(name = "Addr3", required = true)
                            protected String addr3;
                            @XmlElement(name = "City", required = true)
                            protected String city;
                            @XmlElement(name = "StateProv", required = true)
                            protected String stateProv;
                            @XmlElement(name = "PostalCode", required = true)
                            protected String postalCode;
                            @XmlElement(name = "Country", required = true)
                            protected String country;
                            @XmlElement(name = "AddrType", required = true)
                            protected String addrType;


                            public String getAddr1() {
                                return addr1;
                            }


                            public void setAddr1(String value) {
                                this.addr1 = value;
                            }


                            public String getAddr2() {
                                return addr2;
                            }


                            public void setAddr2(String value) {
                                this.addr2 = value;
                            }


                            public String getAddr3() {
                                return addr3;
                            }


                            public void setAddr3(String value) {
                                this.addr3 = value;
                            }


                            public String getCity() {
                                return city;
                            }


                            public void setCity(String value) {
                                this.city = value;
                            }


                            public String getStateProv() {
                                return stateProv;
                            }


                            public void setStateProv(String value) {
                                this.stateProv = value;
                            }


                            public String getPostalCode() {
                                return postalCode;
                            }


                            public void setPostalCode(String value) {
                                this.postalCode = value;
                            }


                            public String getCountry() {
                                return country;
                            }


                            public void setCountry(String value) {
                                this.country = value;
                            }


                            public String getAddrType() {
                                return addrType;
                            }


                            public void setAddrType(String value) {
                                this.addrType = value;
                            }

                        }

                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "amountValue",
                    "currencyCode"
                })
                public static class TotalIntAmt {

					protected String amountValue;
                    @XmlElement(required = true)
                    protected String currencyCode;


					public String getAmountValue() {
                        return amountValue;
                    }


					public void setAmountValue(String value) {
                        this.amountValue = value;
                    }


                    public String getCurrencyCode() {
                        return currencyCode;
                    }


                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                }

            }

        }

    }



    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "responseHeader"
    })
    public static class Header {

        @XmlElement(name = "ResponseHeader", required = true)
		protected TdAcctInquiryResponseDto.Header.ResponseHeader responseHeader;


		public TdAcctInquiryResponseDto.Header.ResponseHeader getResponseHeader() {
            return responseHeader;
        }


		public void setResponseHeader(TdAcctInquiryResponseDto.Header.ResponseHeader value) {
            this.responseHeader = value;
        }



        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "requestMessageKey",
            "responseMessageInfo",
            "ubusTransaction",
            "hostTransaction",
            "hostParentTransaction",
            "customInfo"
        })
        public static class ResponseHeader {

            @XmlElement(name = "RequestMessageKey", required = true)
			protected TdAcctInquiryResponseDto.Header.ResponseHeader.RequestMessageKey requestMessageKey;
            @XmlElement(name = "ResponseMessageInfo", required = true)
			protected TdAcctInquiryResponseDto.Header.ResponseHeader.ResponseMessageInfo responseMessageInfo;
            @XmlElement(name = "UBUSTransaction", required = true)
			protected TdAcctInquiryResponseDto.Header.ResponseHeader.UBUSTransaction ubusTransaction;
            @XmlElement(name = "HostTransaction", required = true)
			protected TdAcctInquiryResponseDto.Header.ResponseHeader.HostTransaction hostTransaction;
            @XmlElement(name = "HostParentTransaction", required = true)
			protected TdAcctInquiryResponseDto.Header.ResponseHeader.HostParentTransaction hostParentTransaction;
            @XmlElement(name = "CustomInfo", required = true)
            protected String customInfo;


			public TdAcctInquiryResponseDto.Header.ResponseHeader.RequestMessageKey getRequestMessageKey() {
                return requestMessageKey;
            }


			public void setRequestMessageKey(TdAcctInquiryResponseDto.Header.ResponseHeader.RequestMessageKey value) {
                this.requestMessageKey = value;
            }


			public TdAcctInquiryResponseDto.Header.ResponseHeader.ResponseMessageInfo getResponseMessageInfo() {
                return responseMessageInfo;
            }


			public void setResponseMessageInfo(
					TdAcctInquiryResponseDto.Header.ResponseHeader.ResponseMessageInfo value) {
                this.responseMessageInfo = value;
            }


			public TdAcctInquiryResponseDto.Header.ResponseHeader.UBUSTransaction getUBUSTransaction() {
                return ubusTransaction;
            }


			public void setUBUSTransaction(TdAcctInquiryResponseDto.Header.ResponseHeader.UBUSTransaction value) {
                this.ubusTransaction = value;
            }


			public TdAcctInquiryResponseDto.Header.ResponseHeader.HostTransaction getHostTransaction() {
                return hostTransaction;
            }


			public void setHostTransaction(TdAcctInquiryResponseDto.Header.ResponseHeader.HostTransaction value) {
                this.hostTransaction = value;
            }


			public TdAcctInquiryResponseDto.Header.ResponseHeader.HostParentTransaction getHostParentTransaction() {
                return hostParentTransaction;
            }


			public void setHostParentTransaction(
					TdAcctInquiryResponseDto.Header.ResponseHeader.HostParentTransaction value) {
                this.hostParentTransaction = value;
            }


            public String getCustomInfo() {
                return customInfo;
            }


            public void setCustomInfo(String value) {
                this.customInfo = value;
            }



            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "id",
                "status"
            })
            public static class HostParentTransaction {

                @XmlElement(name = "Id", required = true)
                protected String id;
                @XmlElement(name = "Status", required = true)
                protected String status;


                public String getId() {
                    return id;
                }


                public void setId(String value) {
                    this.id = value;
                }


                public String getStatus() {
                    return status;
                }


                public void setStatus(String value) {
                    this.status = value;
                }

            }



            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "id",
                "status"
            })
            public static class HostTransaction {

                @XmlElement(name = "Id", required = true)
                protected String id;
                @XmlElement(name = "Status", required = true)
                protected String status;


                public String getId() {
                    return id;
                }


                public void setId(String value) {
                    this.id = value;
                }


                public String getStatus() {
                    return status;
                }


                public void setStatus(String value) {
                    this.status = value;
                }

            }



            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "requestUUID",
                "serviceRequestId",
                "serviceRequestVersion",
                "channelId"
            })
            public static class RequestMessageKey {

                @XmlElement(name = "RequestUUID", required = true)
                protected String requestUUID;
                @XmlElement(name = "ServiceRequestId", required = true)
                protected String serviceRequestId;
                @XmlElement(name = "ServiceRequestVersion")
				protected String serviceRequestVersion;
                @XmlElement(name = "ChannelId", required = true)
                protected String channelId;


                public String getRequestUUID() {
                    return requestUUID;
                }


                public void setRequestUUID(String value) {
                    this.requestUUID = value;
                }


                public String getServiceRequestId() {
                    return serviceRequestId;
                }


                public void setServiceRequestId(String value) {
                    this.serviceRequestId = value;
                }


				public String getServiceRequestVersion() {
                    return serviceRequestVersion;
                }


				public void setServiceRequestVersion(String value) {
                    this.serviceRequestVersion = value;
                }


                public String getChannelId() {
                    return channelId;
                }


                public void setChannelId(String value) {
                    this.channelId = value;
                }

            }



            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "bankId",
                "timeZone",
                "messageDateTime"
            })
            public static class ResponseMessageInfo {

                @XmlElement(name = "BankId", required = true)
                protected String bankId;
                @XmlElement(name = "TimeZone", required = true)
                protected String timeZone;
                @XmlElement(name = "MessageDateTime", required = true)
                @XmlSchemaType(name = "dateTime")
				protected String messageDateTime;


                public String getBankId() {
                    return bankId;
                }


                public void setBankId(String value) {
                    this.bankId = value;
                }


                public String getTimeZone() {
                    return timeZone;
                }


                public void setTimeZone(String value) {
                    this.timeZone = value;
                }


				public String getMessageDateTime() {
                    return messageDateTime;
                }


				public void setMessageDateTime(String value) {
                    this.messageDateTime = value;
                }

            }



            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "id",
                "status"
            })
            public static class UBUSTransaction {

                @XmlElement(name = "Id", required = true)
                protected String id;
                @XmlElement(name = "Status", required = true)
                protected String status;


                public String getId() {
                    return id;
                }


                public void setId(String value) {
                    this.id = value;
                }


                public String getStatus() {
                    return status;
                }


                public void setStatus(String value) {
                    this.status = value;
                }

            }

        }

    }

}