package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.Builder;

@Builder
public class ResponseRootHeader {

	protected ResponseHeader responseHeader;

	@XmlElement(name = "ResponseHeader")
	public ResponseHeader getRequestHeader() {
		return responseHeader;
	}

	public void setRequestHeader(ResponseHeader responseHeader) {
		this.responseHeader = responseHeader;
	}
}
