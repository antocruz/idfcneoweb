package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "signedInfo", "signatureValue", "keyInfo" })
@XmlRootElement(name = "Signature")
public class EkycResponseSignatureDto {

	@XmlElement(name = "SignedInfo", required = true)
	protected EkycResponseSignatureDto.SignedInfo signedInfo;
	@XmlElement(name = "SignatureValue", required = true)
	protected Object signatureValue;
	@XmlElement(name = "KeyInfo", required = true)
	protected EkycResponseSignatureDto.KeyInfo keyInfo;

	public EkycResponseSignatureDto.SignedInfo getSignedInfo() {
		return signedInfo;
	}

	public void setSignedInfo(EkycResponseSignatureDto.SignedInfo value) {
		this.signedInfo = value;
	}

	public Object getSignatureValue() {
		return signatureValue;
	}

	public void setSignatureValue(Object value) {
		this.signatureValue = value;
	}

	public EkycResponseSignatureDto.KeyInfo getKeyInfo() {
		return keyInfo;
	}

	public void setKeyInfo(EkycResponseSignatureDto.KeyInfo value) {
		this.keyInfo = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "x509Data" })
	public static class KeyInfo {

		@XmlElement(name = "X509Data", required = true)
		protected EkycResponseSignatureDto.KeyInfo.X509Data x509Data;

		public EkycResponseSignatureDto.KeyInfo.X509Data getX509Data() {
			return x509Data;
		}

		public void setX509Data(EkycResponseSignatureDto.KeyInfo.X509Data value) {
			this.x509Data = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "x509SubjectName", "x509Certificate" })
		public static class X509Data {

			@XmlElement(name = "X509SubjectName", required = true)
			protected Object x509SubjectName;
			@XmlElement(name = "X509Certificate", required = true)
			protected Object x509Certificate;

			public Object getX509SubjectName() {
				return x509SubjectName;
			}

			public void setX509SubjectName(Object value) {
				this.x509SubjectName = value;
			}

			public Object getX509Certificate() {
				return x509Certificate;
			}

			public void setX509Certificate(Object value) {
				this.x509Certificate = value;
			}

		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "canonicalizationMethod", "signatureMethod", "reference" })
	public static class SignedInfo {

		@XmlElement(name = "CanonicalizationMethod", required = true)
		protected EkycResponseSignatureDto.SignedInfo.CanonicalizationMethod canonicalizationMethod;
		@XmlElement(name = "SignatureMethod", required = true)
		protected EkycResponseSignatureDto.SignedInfo.SignatureMethod signatureMethod;
		@XmlElement(name = "Reference", required = true)
		protected EkycResponseSignatureDto.SignedInfo.Reference reference;

		public EkycResponseSignatureDto.SignedInfo.CanonicalizationMethod getCanonicalizationMethod() {
			return canonicalizationMethod;
		}

		public void setCanonicalizationMethod(EkycResponseSignatureDto.SignedInfo.CanonicalizationMethod value) {
			this.canonicalizationMethod = value;
		}

		public EkycResponseSignatureDto.SignedInfo.SignatureMethod getSignatureMethod() {
			return signatureMethod;
		}

		public void setSignatureMethod(EkycResponseSignatureDto.SignedInfo.SignatureMethod value) {
			this.signatureMethod = value;
		}

		public EkycResponseSignatureDto.SignedInfo.Reference getReference() {
			return reference;
		}

		public void setReference(EkycResponseSignatureDto.SignedInfo.Reference value) {
			this.reference = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class CanonicalizationMethod {

			@XmlAttribute(name = "Algorithm", required = true)
			protected String algorithm;

			public String getAlgorithm() {
				return algorithm;
			}

			public void setAlgorithm(String value) {
				this.algorithm = value;
			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "transforms", "digestMethod", "digestValue" })
		public static class Reference {

			@XmlElement(name = "Transforms", required = true)
			protected EkycResponseSignatureDto.SignedInfo.Reference.Transforms transforms;
			@XmlElement(name = "DigestMethod", required = true)
			protected EkycResponseSignatureDto.SignedInfo.Reference.DigestMethod digestMethod;
			@XmlElement(name = "DigestValue", required = true)
			protected String digestValue;
			@XmlAttribute(name = "URI", required = true)
			protected String uri;

			public EkycResponseSignatureDto.SignedInfo.Reference.Transforms getTransforms() {
				return transforms;
			}

			public void setTransforms(EkycResponseSignatureDto.SignedInfo.Reference.Transforms value) {
				this.transforms = value;
			}

			public EkycResponseSignatureDto.SignedInfo.Reference.DigestMethod getDigestMethod() {
				return digestMethod;
			}

			public void setDigestMethod(EkycResponseSignatureDto.SignedInfo.Reference.DigestMethod value) {
				this.digestMethod = value;
			}

			public String getDigestValue() {
				return digestValue;
			}

			public void setDigestValue(String value) {
				this.digestValue = value;
			}

			public String getURI() {
				return uri;
			}

			public void setURI(String value) {
				this.uri = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "")
			public static class DigestMethod {

				@XmlAttribute(name = "Algorithm", required = true)
				protected String algorithm;

				public String getAlgorithm() {
					return algorithm;
				}

				public void setAlgorithm(String value) {
					this.algorithm = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "transform" })
			public static class Transforms {

				@XmlElement(name = "Transform", required = true)
				protected EkycResponseSignatureDto.SignedInfo.Reference.Transforms.Transform transform;

				public EkycResponseSignatureDto.SignedInfo.Reference.Transforms.Transform getTransform() {
					return transform;
				}

				public void setTransform(EkycResponseSignatureDto.SignedInfo.Reference.Transforms.Transform value) {
					this.transform = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "")
				public static class Transform {

					@XmlAttribute(name = "Algorithm", required = true)
					protected String algorithm;

					public String getAlgorithm() {
						return algorithm;
					}

					public void setAlgorithm(String value) {
						this.algorithm = value;
					}

				}

			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class SignatureMethod {

			@XmlAttribute(name = "Algorithm", required = true)
			protected String algorithm;

			public String getAlgorithm() {
				return algorithm;
			}

			public void setAlgorithm(String value) {
				this.algorithm = value;
			}

		}

	}

}
