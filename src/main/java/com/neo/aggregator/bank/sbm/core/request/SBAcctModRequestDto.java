package com.neo.aggregator.bank.sbm.core.request;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.neo.aggregator.bank.sbm.Header;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SBAcctModRequestDto {

	@XmlElement(name = "Header")
	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@XmlElement(name = "Body", required = true)
	protected SBAcctModRequestDto.Body body;

	public SBAcctModRequestDto.Body getBody() {
		return body;
	}

	public void setBody(SBAcctModRequestDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "sbAcctModRequest" })
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Body {

		@XmlElement(name = "SBAcctModRequest", required = true)
		protected SBAcctModRequestDto.Body.SBAcctModRequest sbAcctModRequest;

		public SBAcctModRequestDto.Body.SBAcctModRequest getSBAcctModRequest() {
			return sbAcctModRequest;
		}

		public void setSBAcctModRequest(SBAcctModRequestDto.Body.SBAcctModRequest value) {
			this.sbAcctModRequest = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "sbAcctModRq", "sbAcctModCustomData" })
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class SBAcctModRequest {

			@XmlElement(name = "SBAcctModRq", required = true)
			protected SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq sbAcctModRq;
			@XmlElement(name = "SBAcctMod_CustomData", required = true)
			protected SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModCustomData sbAcctModCustomData;

			public SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq getSBAcctModRq() {
				return sbAcctModRq;
			}

			public void setSBAcctModRq(SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq value) {
				this.sbAcctModRq = value;
			}

			public SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModCustomData getSBAcctModCustomData() {
				return sbAcctModCustomData;
			}

			public void setSBAcctModCustomData(SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModCustomData value) {
				this.sbAcctModCustomData = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "debitcardflg", "camiscentrd", "nomavailflg", "nomentered", "relparty" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class SBAcctModCustomData {

				@XmlElement(name = "DEBIT_CARD_FLG", required = true)
				protected String debitcardflg;
				@XmlElement(name = "CAMISCENTRD")
				protected String camiscentrd;
				@XmlElement(name = "NOMAVAILFLG", required = true)
				protected String nomavailflg;
				@XmlElement(name = "NOMENTERED")
				protected String nomentered;
				@XmlElement(name = "RELPARTY", required = true)
				protected SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModCustomData.RELPARTY relparty;

				public String getDEBITCARDFLG() {
					return debitcardflg;
				}

				public void setDEBITCARDFLG(String value) {
					this.debitcardflg = value;
				}

				public String getCAMISCENTRD() {
					return camiscentrd;
				}

				public void setCAMISCENTRD(String value) {
					this.camiscentrd = value;
				}

				public String getNOMAVAILFLG() {
					return nomavailflg;
				}

				public void setNOMAVAILFLG(String value) {
					this.nomavailflg = value;
				}

				public String getNOMENTERED() {
					return nomentered;
				}

				public void setNOMENTERED(String value) {
					this.nomentered = value;
				}

				public SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModCustomData.RELPARTY getRELPARTY() {
					return relparty;
				}

				public void setRELPARTY(SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModCustomData.RELPARTY value) {
					this.relparty = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "loanodnoticeflg", "passsheetflg", "xcludecombstmtflg", "cifid",
						"siflg", "depositnoticeflg" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class RELPARTY {

					@XmlElement(name = "LOANODNOTICEFLG", required = true)
					protected String loanodnoticeflg;
					@XmlElement(name = "PASSSHEETFLG", required = true)
					protected String passsheetflg;
					@XmlElement(name = "XCLUDECOMBSTMTFLG", required = true)
					protected String xcludecombstmtflg;
					@XmlElement(name = "CIFID")
					protected String cifid;
					@XmlElement(name = "SIFLG", required = true)
					protected String siflg;
					@XmlElement(name = "DEPOSITNOTICEFLG", required = true)
					protected String depositnoticeflg;
					@XmlAttribute(name = "isMultiRec")
					protected String isMultiRec;

					public String getLOANODNOTICEFLG() {
						return loanodnoticeflg;
					}

					public void setLOANODNOTICEFLG(String value) {
						this.loanodnoticeflg = value;
					}

					public String getPASSSHEETFLG() {
						return passsheetflg;
					}

					public void setPASSSHEETFLG(String value) {
						this.passsheetflg = value;
					}

					public String getXCLUDECOMBSTMTFLG() {
						return xcludecombstmtflg;
					}

					public void setXCLUDECOMBSTMTFLG(String value) {
						this.xcludecombstmtflg = value;
					}

					public String getCIFID() {
						return cifid;
					}

					public void setCIFID(String value) {
						this.cifid = value;
					}

					public String getSIFLG() {
						return siflg;
					}

					public void setSIFLG(String value) {
						this.siflg = value;
					}

					public String getDEPOSITNOTICEFLG() {
						return depositnoticeflg;
					}

					public void setDEPOSITNOTICEFLG(String value) {
						this.depositnoticeflg = value;
					}

					public String getIsMultiRec() {
						return isMultiRec;
					}

					public void setIsMultiRec(String value) {
						this.isMultiRec = value;
					}

				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "sbAcctId", "nomineeInfoRec", "relPartyRec" })
			@Builder
			@NoArgsConstructor
			@AllArgsConstructor
			public static class SBAcctModRq {

				@XmlElement(name = "SBAcctId", required = true)
				protected SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.SBAcctId sbAcctId;
				@XmlElement(name = "NomineeInfoRec")
				protected List<SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec> nomineeInfoRec;
				@XmlElement(name = "RelPartyRec", required = true)
				protected SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.RelPartyRec relPartyRec;

				public SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.SBAcctId getSBAcctId() {
					return sbAcctId;
				}

				public void setSBAcctId(SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.SBAcctId value) {
					this.sbAcctId = value;
				}

				public List<SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec> getNomineeInfoRec() {
					if (nomineeInfoRec == null) {
						nomineeInfoRec = new ArrayList<SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec>();
					}
					return this.nomineeInfoRec;
				}

				public SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.RelPartyRec getRelPartyRec() {
					return relPartyRec;
				}

				public void setRelPartyRec(SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.RelPartyRec value) {
					this.relPartyRec = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "regNum", "nomineeName", "relType", "nomineeContactInfo",
						"nomineeMinorFlg", "nomineeBirthDt", "nomineePercent" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class NomineeInfoRec {

					@XmlElement(name = "RegNum")
					protected String regNum;
					@XmlElement(name = "NomineeName", required = true)
					protected String nomineeName;
					@XmlElement(name = "RelType")
					protected String relType;
					@XmlElement(name = "NomineeContactInfo", required = true)
					protected SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineeContactInfo nomineeContactInfo;
					@XmlElement(name = "NomineeMinorFlg", required = true)
					protected String nomineeMinorFlg;
					@XmlElement(name = "NomineeBirthDt", required = true)
					@XmlSchemaType(name = "dateTime")
					protected String nomineeBirthDt;
					@XmlElement(name = "NomineePercent", required = true)
					protected SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineePercent nomineePercent;

					public String getRegNum() {
						return regNum;
					}

					public void setRegNum(String value) {
						this.regNum = value;
					}

					public String getNomineeName() {
						return nomineeName;
					}

					public void setNomineeName(String value) {
						this.nomineeName = value;
					}

					public String getRelType() {
						return relType;
					}

					public void setRelType(String value) {
						this.relType = value;
					}

					public SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineeContactInfo getNomineeContactInfo() {
						return nomineeContactInfo;
					}

					public void setNomineeContactInfo(
							SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineeContactInfo value) {
						this.nomineeContactInfo = value;
					}

					public String getNomineeMinorFlg() {
						return nomineeMinorFlg;
					}

					public void setNomineeMinorFlg(String value) {
						this.nomineeMinorFlg = value;
					}

					public String getNomineeBirthDt() {
						return nomineeBirthDt;
					}

					public void setNomineeBirthDt(String value) {
						this.nomineeBirthDt = value;
					}

					public SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineePercent getNomineePercent() {
						return nomineePercent;
					}

					public void setNomineePercent(
							SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineePercent value) {
						this.nomineePercent = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "postAddr" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class NomineeContactInfo {

						@XmlElement(name = "PostAddr", required = true)
						protected SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineeContactInfo.PostAddr postAddr;

						public SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineeContactInfo.PostAddr getPostAddr() {
							return postAddr;
						}

						public void setPostAddr(
								SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.NomineeInfoRec.NomineeContactInfo.PostAddr value) {
							this.postAddr = value;
						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "addr1", "addr2", "addr3", "city", "stateProv", "postalCode",
								"country" })
						@Builder
						@NoArgsConstructor
						@AllArgsConstructor
						public static class PostAddr {

							@XmlElement(name = "Addr1", required = true)
							protected String addr1;
							@XmlElement(name = "Addr2", required = true)
							protected String addr2;
							@XmlElement(name = "Addr3", required = true)
							protected String addr3;
							@XmlElement(name = "City")
							protected String city;
							@XmlElement(name = "StateProv")
							protected String stateProv;
							@XmlElement(name = "PostalCode")
							protected String postalCode;
							@XmlElement(name = "Country", required = true)
							protected String country;

							public String getAddr1() {
								return addr1;
							}

							public void setAddr1(String value) {
								this.addr1 = value;
							}

							public String getAddr2() {
								return addr2;
							}

							public void setAddr2(String value) {
								this.addr2 = value;
							}

							public String getAddr3() {
								return addr3;
							}

							public void setAddr3(String value) {
								this.addr3 = value;
							}

							public String getCity() {
								return city;
							}

							public void setCity(String value) {
								this.city = value;
							}

							public String getStateProv() {
								return stateProv;
							}

							public void setStateProv(String value) {
								this.stateProv = value;
							}

							public String getPostalCode() {
								return postalCode;
							}

							public void setPostalCode(String value) {
								this.postalCode = value;
							}

							public String getCountry() {
								return country;
							}

							public void setCountry(String value) {
								this.country = value;
							}

						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "value" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class NomineePercent {

						protected String value;

						public String getValue() {
							return value;
						}

						public void setValue(String value) {
							this.value = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "relPartyType", "relPartyCode", "custId" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class RelPartyRec {

					@XmlElement(name = "RelPartyType", required = true)
					protected String relPartyType;
					@XmlElement(name = "RelPartyCode")
					protected String relPartyCode;
					@XmlElement(name = "CustId", required = true)
					protected SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.RelPartyRec.CustId custId;

					public String getRelPartyType() {
						return relPartyType;
					}

					public void setRelPartyType(String value) {
						this.relPartyType = value;
					}

					public String getRelPartyCode() {
						return relPartyCode;
					}

					public void setRelPartyCode(String value) {
						this.relPartyCode = value;
					}

					public SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.RelPartyRec.CustId getCustId() {
						return custId;
					}

					public void setCustId(
							SBAcctModRequestDto.Body.SBAcctModRequest.SBAcctModRq.RelPartyRec.CustId value) {
						this.custId = value;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "custId" })
					@Builder
					@NoArgsConstructor
					@AllArgsConstructor
					public static class CustId {

						@XmlElement(name = "CustId")
						protected String custId;

						public String getCustId() {
							return custId;
						}

						public void setCustId(String value) {
							this.custId = value;
						}

					}

				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "acctId" })
				@Builder
				@NoArgsConstructor
				@AllArgsConstructor
				public static class SBAcctId {

					@XmlElement(name = "AcctId")
					protected String acctId;

					public String getAcctId() {
						return acctId;
					}

					public void setAcctId(String value) {
						this.acctId = value;
					}

				}

			}

		}

	}
}

