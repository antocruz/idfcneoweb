package com.neo.aggregator.bank.sbm.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "", propOrder = { "channelId", "partnerReqID", "timestamp", "request" })
@XmlRootElement(name = "sessionReq")
public class ESBRequestDto {

	@XmlElement(required = true)
	protected String channelId;
	@XmlElement(name = "PartnerReqID", required = true)
	protected String partnerReqID;
	@XmlElement(required = true)
	protected String timestamp;
	@XmlElement(required = true)
	protected String request;

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String value) {
		this.channelId = value;
	}

	public String getPartnerReqID() {
		return partnerReqID;
	}

	public void setPartnerReqID(String value) {
		this.partnerReqID = value;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String value) {
		this.timestamp = value;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String value) {
		this.request = value;
	}

}
