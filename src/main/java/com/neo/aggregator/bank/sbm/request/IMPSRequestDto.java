package com.neo.aggregator.bank.sbm.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "amountAvailable", "ftLimit", "accountUseLimit", "accountUseCount", "lastDate",
		"lastTime", "maxPinCount", "maxPinUseCount", "pinOffset", "mobileNumber", "remitteracc", "remittername",
		"txnamt", "benificiaryacc", "bankCode", "bennbin", "banknbin", "isMobileFT", "isAccountFT", "remark",
		"branchcode", "correlid", "userid", "msgid", "transType", "isBLQ", "isMini", "isCC", "isPayment",
		"lastDateTime", "isMerchant", "rdtdAmount", "penaltiAmount", "isMisc", "ifsc", "isBranch" })
@XmlRootElement(name = "MOBILEBANKING_REQ")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IMPSRequestDto {

	@XmlElement(name = "AmountAvailable", required = true)
	protected String amountAvailable;
	@XmlElement(name = "FtLimit", required = true)
	protected String ftLimit;
	@XmlElement(name = "AccountUseLimit", required = true)
	protected String accountUseLimit;
	@XmlElement(name = "AccountUseCount", required = true)
	protected String accountUseCount;
	@XmlElement(name = "LastDate", required = true)
	protected String lastDate;
	@XmlElement(name = "LastTime", required = true)
	protected String lastTime;
	@XmlElement(name = "MaxPinCount", required = true)
	protected String maxPinCount;
	@XmlElement(name = "MaxPinUseCount", required = true)
	protected String maxPinUseCount;
	@XmlElement(name = "PinOffset", required = true)
	protected String pinOffset;
	@XmlElement(name = "MobileNumber", required = true)
	protected String mobileNumber;
	@XmlElement(name = "REMITTERACC", required = true)
	protected String remitteracc;
	@XmlElement(name = "REMITTERNAME", required = true)
	protected String remittername;
	@XmlElement(name = "TXNAMT", required = true)
	protected String txnamt;
	@XmlElement(name = "BENIFICIARYACC", required = true)
	protected String benificiaryacc;
	@XmlElement(name = "BankCode", required = true)
	protected String bankCode;
	@XmlElement(name = "BENNBIN", required = true)
	protected String bennbin;
	@XmlElement(name = "BANKNBIN", required = true)
	protected String banknbin;
	@XmlElement(name = "IsMobileFT", required = true)
	protected String isMobileFT;
	@XmlElement(name = "IsAccountFT", required = true)
	protected String isAccountFT;
	@XmlElement(name = "Remark", required = true)
	protected String remark;
	@XmlElement(name = "BRANCH_CODE", required = true)
	protected String branchcode;
	@XmlElement(name = "CORRELID", required = true)
	protected String correlid;
	@XmlElement(name = "USERID", required = true)
	protected String userid;
	@XmlElement(name = "MSGID", required = true)
	protected String msgid;
	@XmlElement(name = "TransType", required = true)
	protected String transType;
	@XmlElement(name = "IsBLQ", required = true)
	protected String isBLQ;
	@XmlElement(name = "IsMini", required = true)
	protected String isMini;
	@XmlElement(name = "IsCC", required = true)
	protected String isCC;
	@XmlElement(name = "IsPayment", required = true)
	protected String isPayment;
	@XmlElement(name = "LastDateTime", required = true)
	protected String lastDateTime;
	@XmlElement(name = "IsMerchant", required = true)
	protected String isMerchant;
	@XmlElement(name = "RDTD_Amount", required = true)
	protected String rdtdAmount;
	@XmlElement(name = "PenaltiAmount", required = true)
	protected String penaltiAmount;
	@XmlElement(name = "IsMisc", required = true)
	protected String isMisc;
	@XmlElement(name = "IFSC", required = true)
	protected String ifsc;
	@XmlElement(name = "IsBranch", required = true)
	protected String isBranch;

	public String getAmountAvailable() {
		return amountAvailable;
	}

	public void setAmountAvailable(String value) {
		this.amountAvailable = value;
	}

	public String getFtLimit() {
		return ftLimit;
	}

	public void setFtLimit(String value) {
		this.ftLimit = value;
	}

	public String getAccountUseLimit() {
		return accountUseLimit;
	}

	public void setAccountUseLimit(String value) {
		this.accountUseLimit = value;
	}

	public String getAccountUseCount() {
		return accountUseCount;
	}

	public void setAccountUseCount(String value) {
		this.accountUseCount = value;
	}

	public String getLastDate() {
		return lastDate;
	}

	public void setLastDate(String value) {
		this.lastDate = value;
	}

	public String getLastTime() {
		return lastTime;
	}

	public void setLastTime(String value) {
		this.lastTime = value;
	}

	public String getMaxPinCount() {
		return maxPinCount;
	}

	public void setMaxPinCount(String value) {
		this.maxPinCount = value;
	}

	public String getMaxPinUseCount() {
		return maxPinUseCount;
	}

	public void setMaxPinUseCount(String value) {
		this.maxPinUseCount = value;
	}

	public String getPinOffset() {
		return pinOffset;
	}

	public void setPinOffset(String value) {
		this.pinOffset = value;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String value) {
		this.mobileNumber = value;
	}

	public String getREMITTERACC() {
		return remitteracc;
	}

	public void setREMITTERACC(String value) {
		this.remitteracc = value;
	}

	public String getREMITTERNAME() {
		return remittername;
	}

	public void setREMITTERNAME(String value) {
		this.remittername = value;
	}

	public String getTXNAMT() {
		return txnamt;
	}

	public void setTXNAMT(String value) {
		this.txnamt = value;
	}

	public String getBENIFICIARYACC() {
		return benificiaryacc;
	}

	public void setBENIFICIARYACC(String value) {
		this.benificiaryacc = value;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String value) {
		this.bankCode = value;
	}

	public String getBENNBIN() {
		return bennbin;
	}

	public void setBENNBIN(String value) {
		this.bennbin = value;
	}

	public String getBANKNBIN() {
		return banknbin;
	}

	public void setBANKNBIN(String value) {
		this.banknbin = value;
	}

	public String getIsMobileFT() {
		return isMobileFT;
	}

	public void setIsMobileFT(String value) {
		this.isMobileFT = value;
	}

	public String getIsAccountFT() {
		return isAccountFT;
	}

	public void setIsAccountFT(String value) {
		this.isAccountFT = value;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String value) {
		this.remark = value;
	}

	public String getBRANCHCODE() {
		return branchcode;
	}

	public void setBRANCHCODE(String value) {
		this.branchcode = value;
	}

	public String getCORRELID() {
		return correlid;
	}

	public void setCORRELID(String value) {
		this.correlid = value;
	}

	public String getUSERID() {
		return userid;
	}

	public void setUSERID(String value) {
		this.userid = value;
	}

	public String getMSGID() {
		return msgid;
	}

	public void setMSGID(String value) {
		this.msgid = value;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String value) {
		this.transType = value;
	}

	public String getIsBLQ() {
		return isBLQ;
	}

	public void setIsBLQ(String value) {
		this.isBLQ = value;
	}

	public String getIsMini() {
		return isMini;
	}

	public void setIsMini(String value) {
		this.isMini = value;
	}

	public String getIsCC() {
		return isCC;
	}

	public void setIsCC(String value) {
		this.isCC = value;
	}

	public String getIsPayment() {
		return isPayment;
	}

	public void setIsPayment(String value) {
		this.isPayment = value;
	}

	public String getLastDateTime() {
		return lastDateTime;
	}

	public void setLastDateTime(String value) {
		this.lastDateTime = value;
	}

	public String getIsMerchant() {
		return isMerchant;
	}

	public void setIsMerchant(String value) {
		this.isMerchant = value;
	}

	public String getRDTDAmount() {
		return rdtdAmount;
	}

	public void setRDTDAmount(String value) {
		this.rdtdAmount = value;
	}

	public String getPenaltiAmount() {
		return penaltiAmount;
	}

	public void setPenaltiAmount(String value) {
		this.penaltiAmount = value;
	}

	public String getIsMisc() {
		return isMisc;
	}

	public void setIsMisc(String value) {
		this.isMisc = value;
	}

	public String getIFSC() {
		return ifsc;
	}

	public void setIFSC(String value) {
		this.ifsc = value;
	}

	public String getIsBranch() {
		return isBranch;
	}

	public void setIsBranch(String value) {
		this.isBranch = value;
	}

}
