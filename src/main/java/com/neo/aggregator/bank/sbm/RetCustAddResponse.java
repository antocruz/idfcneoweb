package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

public class RetCustAddResponse {
	
	
	private RetCustAddRs retCustAddRs;

	@XmlElement(name = "RetCustAddRs")
	public RetCustAddRs getRetCustAddRs() {
		return retCustAddRs;
	}

	public void setRetCustAddRs(RetCustAddRs retCustAddRs) {
		this.retCustAddRs = retCustAddRs;
	}
	
}
