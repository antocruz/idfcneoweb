package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessageKey {

	protected String requestUUID;

	protected String globalUUID;

	protected String serviceRequestId;

	protected String serviceRequestVersion;

	protected String originatorVersion;

	protected String channelId;

	protected String languageId;

	protected String mobileNoOtp;

	@XmlElement(name = "RequestUUID")
	public String getRequestUUID() {
		return requestUUID;
	}

	public void setRequestUUID(String requestUUID) {
		this.requestUUID = requestUUID;
	}

	@XmlElement(name = "ServiceRequestId")
	public String getServiceRequestId() {
		return serviceRequestId;
	}

	public void setServiceRequestId(String serviceRequestId) {
		this.serviceRequestId = serviceRequestId;
	}

	@XmlElement(name = "ServiceRequestVersion")
	public String getServiceRequestVersion() {
		return serviceRequestVersion;
	}

	public void setServiceRequestVersion(String serviceRequestVersion) {
		this.serviceRequestVersion = serviceRequestVersion;
	}

	@XmlElement(name = "ChannelId")
	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	@XmlElement(name = "LanguageId")
	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	@XmlElement(name = "GlobalUUID")
	public String getGlobalUUID() {
		return globalUUID;
	}

	public void setGlobalUUID(String globalUUID) {
		this.globalUUID = globalUUID;
	}

	@XmlElement(name = "OriginatorVersion")
	public String getOriginatorVersion() {
		return originatorVersion;
	}

	public void setOriginatorVersion(String originatorVersion) {
		this.originatorVersion = originatorVersion;
	}

	@XmlElement(name = "MobileNoOtp")
	public String getMobileNoOtp() {
		return mobileNoOtp;
	}

	public void setMobileNoOtp(String mobileNoOtp) {
		this.mobileNoOtp = mobileNoOtp;
	}

}
