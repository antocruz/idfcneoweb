package com.neo.aggregator.bank.sbm.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "sessionRes")
public class LoginResponseDto {

	@XmlElement(name = "PartnerReqID", required = true)
	protected String partnerReqID;
	@XmlElement(required = true)
	protected String username;
	@XmlElement(name = "Status", required = true)
	protected String status;
	@XmlElement(name = "Sessionid", required = true)
	protected String sessionid;
	@XmlElement(required = true)
	protected String timestamp;

	public String getPartnerReqID() {
		return partnerReqID;
	}

	public void setPartnerReqID(String value) {
		this.partnerReqID = value;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String value) {
		this.username = value;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String value) {
		this.status = value;
	}

	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String value) {
		this.sessionid = value;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String value) {
		this.timestamp = value;
	}

}
