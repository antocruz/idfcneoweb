package com.neo.aggregator.bank.sbm;

import javax.xml.bind.annotation.XmlElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseHeader {

	protected MessageKey messageKey;

	protected ResponseMessageInfo responseMsgInfo;

	protected TransactionIdAndStatus ubusTransaction;

	protected TransactionIdAndStatus hostTransaction;

	protected TransactionIdAndStatus hostParentTransaction;

	@XmlElement(name = "RequestMessageKey")
	public MessageKey getMessageKey() {
		return messageKey;
	}

	public void setMessageKey(MessageKey messageKey) {
		this.messageKey = messageKey;
	}

	@XmlElement(name = "ResponseMessageInfo")
	public ResponseMessageInfo getResponseMsgInfo() {
		return responseMsgInfo;
	}

	public void setResponseMsgInfo(ResponseMessageInfo responseMsgInfo) {
		this.responseMsgInfo = responseMsgInfo;
	}

	@XmlElement(name = "UBUSTransaction")
	public TransactionIdAndStatus getUbusTransaction() {
		return ubusTransaction;
	}

	public void setUbusTransaction(TransactionIdAndStatus ubusTransaction) {
		this.ubusTransaction = ubusTransaction;
	}

	@XmlElement(name = "HostTransaction")
	public TransactionIdAndStatus getHostTransaction() {
		return hostTransaction;
	}

	public void setHostTransaction(TransactionIdAndStatus hostTransaction) {
		this.hostTransaction = hostTransaction;
	}

	@XmlElement(name = "HostParentTransaction")
	public TransactionIdAndStatus getHostParentTransaction() {
		return hostParentTransaction;
	}

	public void setHostParentTransaction(TransactionIdAndStatus hostParentTransaction) {
		this.hostParentTransaction = hostParentTransaction;
	}

}
