package com.neo.aggregator.bank.sbm.core.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "FIXML")
public class AddOutBoundPymtResponseDto {

	@XmlElement(name = "Header", required = true)
	protected AddOutBoundPymtResponseDto.Header header;

	@XmlElement(name = "Body", required = true)
	protected AddOutBoundPymtResponseDto.Body body;

	public AddOutBoundPymtResponseDto.Header getHeader() {
		return header;
	}

	public void setHeader(AddOutBoundPymtResponseDto.Header value) {
		this.header = value;
	}

	public AddOutBoundPymtResponseDto.Body getBody() {
		return body;
	}

	public void setBody(AddOutBoundPymtResponseDto.Body value) {
		this.body = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "addOutboundPymtEntryDtlsResponse", "error" })
	public static class Body {

		@XmlElement(name = "AddOutboundPymtEntryDtlsResponse", required = true)
		protected AddOutBoundPymtResponseDto.Body.AddOutboundPymtEntryDtlsResponse addOutboundPymtEntryDtlsResponse;

		public AddOutBoundPymtResponseDto.Body.AddOutboundPymtEntryDtlsResponse getAddOutboundPymtEntryDtlsResponse() {
			return addOutboundPymtEntryDtlsResponse;
		}

		public void setAddOutboundPymtEntryDtlsResponse(
				AddOutBoundPymtResponseDto.Body.AddOutboundPymtEntryDtlsResponse value) {
			this.addOutboundPymtEntryDtlsResponse = value;
		}

		@XmlElement(name = "Error")
		protected Error error;

		public Error getError() {
			return error;
		}

		public void setError(Error value) {
			this.error = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)

		@XmlType(name = "")
		public static class Error {

			@XmlElement(name = "FIBusinessException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiBusinessException;

			@XmlElement(name = "FISystemException")
			protected RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException;

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFiSystemException() {
				return fiSystemException;
			}

			public void setFiSystemException(RetCustAddResponseDto.Body.Error.FIBusinessException fiSystemException) {
				this.fiSystemException = fiSystemException;
			}

			public RetCustAddResponseDto.Body.Error.FIBusinessException getFIBusinessException() {
				return fiBusinessException;
			}

			public void setFIBusinessException(RetCustAddResponseDto.Body.Error.FIBusinessException value) {
				this.fiBusinessException = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)

			@XmlType(name = "", propOrder = { "errorDetail" })
			public static class FIBusinessException {

				@XmlElement(name = "ErrorDetail")
				protected RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail errorDetail;

				public RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail getErrorDetail() {
					return errorDetail;
				}

				public void setErrorDetail(RetCustAddResponseDto.Body.Error.FIBusinessException.ErrorDetail value) {
					this.errorDetail = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)

				@XmlType(name = "", propOrder = { "errorCode", "errorDesc", "errorType" })
				public static class ErrorDetail {

					@XmlElement(name = "ErrorCode")
					protected String errorCode;

					@XmlElement(name = "ErrorDesc")
					protected String errorDesc;

					@XmlElement(name = "ErrorType")
					protected String errorType;

					public String getErrorCode() {
						return errorCode;
					}

					public void setErrorCode(String value) {
						this.errorCode = value;
					}

					public String getErrorDesc() {
						return errorDesc;
					}

					public void setErrorDesc(String value) {
						this.errorDesc = value;
					}

					public String getErrorType() {
						return errorType;
					}

					public void setErrorType(String value) {
						this.errorType = value;
					}

				}

			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "addOutboundPymtEntryDtlsRs", "addOutboundPymtEntryDtlsCustomData" })
		public static class AddOutboundPymtEntryDtlsResponse {

			@XmlElement(name = "AddOutboundPymtEntryDtlsRs", required = true)
			protected AddOutBoundPymtResponseDto.Body.AddOutboundPymtEntryDtlsResponse.AddOutboundPymtEntryDtlsRs addOutboundPymtEntryDtlsRs;
			@XmlElement(name = "AddOutboundPymtEntryDtls_CustomData", required = true)
			protected String addOutboundPymtEntryDtlsCustomData;

			public AddOutBoundPymtResponseDto.Body.AddOutboundPymtEntryDtlsResponse.AddOutboundPymtEntryDtlsRs getAddOutboundPymtEntryDtlsRs() {
				return addOutboundPymtEntryDtlsRs;
			}

			public void setAddOutboundPymtEntryDtlsRs(
					AddOutBoundPymtResponseDto.Body.AddOutboundPymtEntryDtlsResponse.AddOutboundPymtEntryDtlsRs value) {
				this.addOutboundPymtEntryDtlsRs = value;
			}

			public String getAddOutboundPymtEntryDtlsCustomData() {
				return addOutboundPymtEntryDtlsCustomData;
			}

			public void setAddOutboundPymtEntryDtlsCustomData(String value) {
				this.addOutboundPymtEntryDtlsCustomData = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "entityStatus", "pymtReferenceNumber", "instructionId",
					"endToEndIdentification", "tranIdentification" })
			public static class AddOutboundPymtEntryDtlsRs {

				@XmlElement(required = true)
				protected String entityStatus;
				@XmlElement(name = "PymtReferenceNumber", required = true)
				protected String pymtReferenceNumber;
				@XmlElement(name = "InstructionId", required = true)
				protected String instructionId;
				@XmlElement(name = "EndToEndIdentification", required = true)
				protected String endToEndIdentification;
				@XmlElement(name = "TranIdentification", required = true)
				protected String tranIdentification;

				public String getEntityStatus() {
					return entityStatus;
				}

				public void setEntityStatus(String value) {
					this.entityStatus = value;
				}

				public String getPymtReferenceNumber() {
					return pymtReferenceNumber;
				}

				public void setPymtReferenceNumber(String value) {
					this.pymtReferenceNumber = value;
				}

				public String getInstructionId() {
					return instructionId;
				}

				public void setInstructionId(String value) {
					this.instructionId = value;
				}

				public String getEndToEndIdentification() {
					return endToEndIdentification;
				}

				public void setEndToEndIdentification(String value) {
					this.endToEndIdentification = value;
				}

				public String getTranIdentification() {
					return tranIdentification;
				}

				public void setTranIdentification(String value) {
					this.tranIdentification = value;
				}

			}

		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "responseHeader" })
	public static class Header {

		@XmlElement(name = "ResponseHeader", required = true)
		protected AddOutBoundPymtResponseDto.Header.ResponseHeader responseHeader;

		public AddOutBoundPymtResponseDto.Header.ResponseHeader getResponseHeader() {
			return responseHeader;
		}

		public void setResponseHeader(AddOutBoundPymtResponseDto.Header.ResponseHeader value) {
			this.responseHeader = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "requestMessageKey", "responseMessageInfo", "ubusTransaction",
				"hostTransaction", "hostParentTransaction", "customInfo" })
		public static class ResponseHeader {

			@XmlElement(name = "RequestMessageKey", required = true)
			protected AddOutBoundPymtResponseDto.Header.ResponseHeader.RequestMessageKey requestMessageKey;
			@XmlElement(name = "ResponseMessageInfo", required = true)
			protected AddOutBoundPymtResponseDto.Header.ResponseHeader.ResponseMessageInfo responseMessageInfo;
			@XmlElement(name = "UBUSTransaction", required = true)
			protected AddOutBoundPymtResponseDto.Header.ResponseHeader.UBUSTransaction ubusTransaction;
			@XmlElement(name = "HostTransaction", required = true)
			protected AddOutBoundPymtResponseDto.Header.ResponseHeader.HostTransaction hostTransaction;
			@XmlElement(name = "HostParentTransaction", required = true)
			protected AddOutBoundPymtResponseDto.Header.ResponseHeader.HostParentTransaction hostParentTransaction;
			@XmlElement(name = "CustomInfo", required = true)
			protected String customInfo;

			public AddOutBoundPymtResponseDto.Header.ResponseHeader.RequestMessageKey getRequestMessageKey() {
				return requestMessageKey;
			}

			public void setRequestMessageKey(AddOutBoundPymtResponseDto.Header.ResponseHeader.RequestMessageKey value) {
				this.requestMessageKey = value;
			}

			public AddOutBoundPymtResponseDto.Header.ResponseHeader.ResponseMessageInfo getResponseMessageInfo() {
				return responseMessageInfo;
			}

			public void setResponseMessageInfo(
					AddOutBoundPymtResponseDto.Header.ResponseHeader.ResponseMessageInfo value) {
				this.responseMessageInfo = value;
			}

			public AddOutBoundPymtResponseDto.Header.ResponseHeader.UBUSTransaction getUBUSTransaction() {
				return ubusTransaction;
			}

			public void setUBUSTransaction(AddOutBoundPymtResponseDto.Header.ResponseHeader.UBUSTransaction value) {
				this.ubusTransaction = value;
			}

			public AddOutBoundPymtResponseDto.Header.ResponseHeader.HostTransaction getHostTransaction() {
				return hostTransaction;
			}

			public void setHostTransaction(AddOutBoundPymtResponseDto.Header.ResponseHeader.HostTransaction value) {
				this.hostTransaction = value;
			}

			public AddOutBoundPymtResponseDto.Header.ResponseHeader.HostParentTransaction getHostParentTransaction() {
				return hostParentTransaction;
			}

			public void setHostParentTransaction(
					AddOutBoundPymtResponseDto.Header.ResponseHeader.HostParentTransaction value) {
				this.hostParentTransaction = value;
			}

			public String getCustomInfo() {
				return customInfo;
			}

			public void setCustomInfo(String value) {
				this.customInfo = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostParentTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class HostTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "globalUUID", "requestUUID", "serviceRequestId", "serviceId",
					"serviceRequestVersion", "serviceVersion", "channelId", "originatorId" })
			public static class RequestMessageKey {

				@XmlElement(name = "GlobalUUID", required = true)
				protected String globalUUID;
				@XmlElement(name = "RequestUUID", required = true)
				protected String requestUUID;
				@XmlElement(name = "ServiceRequestId", required = true)
				protected String serviceRequestId;
				@XmlElement(name = "ServiceId", required = true)
				protected String serviceId;
				@XmlElement(name = "ServiceRequestVersion")
				protected float serviceRequestVersion;
				@XmlElement(name = "ServiceVersion")
				protected float serviceVersion;
				@XmlElement(name = "ChannelId", required = true)
				protected String channelId;
				@XmlElement(name = "OriginatorId", required = true)
				protected String originatorId;

				public String getGlobalUUID() {
					return globalUUID;
				}

				public void setGlobalUUID(String value) {
					this.globalUUID = value;
				}

				public String getRequestUUID() {
					return requestUUID;
				}

				public void setRequestUUID(String value) {
					this.requestUUID = value;
				}

				public String getServiceRequestId() {
					return serviceRequestId;
				}

				public void setServiceRequestId(String value) {
					this.serviceRequestId = value;
				}

				public String getServiceId() {
					return serviceId;
				}

				public void setServiceId(String value) {
					this.serviceId = value;
				}

				public float getServiceRequestVersion() {
					return serviceRequestVersion;
				}

				public void setServiceRequestVersion(float value) {
					this.serviceRequestVersion = value;
				}

				public float getServiceVersion() {
					return serviceVersion;
				}

				public void setServiceVersion(float value) {
					this.serviceVersion = value;
				}

				public String getChannelId() {
					return channelId;
				}

				public void setChannelId(String value) {
					this.channelId = value;
				}

				public String getOriginatorId() {
					return originatorId;
				}

				public void setOriginatorId(String value) {
					this.originatorId = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "bankId", "timeZone", "messageDateTime" })
			public static class ResponseMessageInfo {

				@XmlElement(name = "BankId", required = true)
				protected String bankId;
				@XmlElement(name = "TimeZone", required = true)
				protected String timeZone;
				@XmlElement(name = "MessageDateTime", required = true)
				@XmlSchemaType(name = "dateTime")
				protected XMLGregorianCalendar messageDateTime;

				public String getBankId() {
					return bankId;
				}

				public void setBankId(String value) {
					this.bankId = value;
				}

				public String getTimeZone() {
					return timeZone;
				}

				public void setTimeZone(String value) {
					this.timeZone = value;
				}

				public XMLGregorianCalendar getMessageDateTime() {
					return messageDateTime;
				}

				public void setMessageDateTime(XMLGregorianCalendar value) {
					this.messageDateTime = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "status" })
			public static class UBUSTransaction {

				@XmlElement(name = "Id", required = true)
				protected String id;
				@XmlElement(name = "Status", required = true)
				protected String status;

				public String getId() {
					return id;
				}

				public void setId(String value) {
					this.id = value;
				}

				public String getStatus() {
					return status;
				}

				public void setStatus(String value) {
					this.status = value;
				}

			}

		}

	}

}

