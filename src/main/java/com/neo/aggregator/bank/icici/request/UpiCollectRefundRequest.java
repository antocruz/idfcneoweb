
package com.neo.aggregator.bank.icici.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class UpiCollectRefundRequest {

    @JsonProperty(value = "merchantId")
    private String merchantId;
    @JsonProperty(value = "subMerchantId")
    private String subMerchantId;
    @JsonProperty(value = "terminalId")
    private String terminalId;
    @JsonProperty(value = "originalBankRRN")
    private String originalBankRRN;
    @JsonProperty(value = "merchantTranId")
    private String merchantTranId;
    @JsonProperty(value = "originalmerchantTranId")
    private String originalmerchantTranId;
    @JsonProperty(value = "payeeVA")
    private String payeeVA;
    @JsonProperty(value = "refundAmount")
    private String refundAmount;
    @JsonProperty(value = "note")
    private String note;
    @JsonProperty(value = "onlineRefund")
    private String onlineRefund;

}
