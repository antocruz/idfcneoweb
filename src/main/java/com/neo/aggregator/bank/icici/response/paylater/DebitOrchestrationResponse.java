package com.neo.aggregator.bank.icici.response.paylater;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DebitOrchestrationResponse {

    @JsonProperty(value = "Status")
    private String status;
    @JsonProperty(value = "ErrorCode")
    private String errCode;
    @JsonProperty(value = "PaymentID")
    private String paymentId;
    @JsonProperty(value = "TransactionAmount")
    private String txnAmount;

    //error keys
    @JsonProperty(value =  "success")
    private String success;
    @JsonProperty(value = "response")
    private String response;
    @JsonProperty(value = "message")
    private String errorMsg;

}
