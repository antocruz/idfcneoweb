package com.neo.aggregator.bank.icici.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonRootName(value = "ImpsResponse")
public class IMPSStatusCheckResponse {

    @JsonProperty(value = "ActCode")
    private String actCode;
    @JsonProperty(value = "Response")
    private String txnResponse;
    @JsonProperty(value = "BankRRN")
    private String bankRrn; //Number
    @JsonProperty(value = "BeneName")
    private String beneName;
    @JsonProperty(value = "TranRefNo")
    private String txnRefno;// Number type
    @JsonProperty(value = "PaymentRef")
    private String paymentRef;
    @JsonProperty(value = "TranDateTime")
    private String txnDatetime; //date type
    @JsonProperty(value = "Amount")
    private Double amount;
    @JsonProperty(value = "BeneMMID")
    private Integer beneMMID;
    @JsonProperty(value = "BeneMobile")
    private String beneMobile; //number type
    @JsonProperty(value = "BeneAccNo")
    private String beneAccNo;
    @JsonProperty(value = "BeneIFSC")
    private String beneIfsc;
    @JsonProperty(value = "RemMobile")
    private String remitterNumber; //number type
    @JsonProperty(value = "RemName")
    private String remitterName;
    @JsonProperty(value = "RetailerCode")
    private String retailerCode;

}
