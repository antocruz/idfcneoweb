package com.neo.aggregator.bank.icici.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class CPCSResponse {

    @JsonProperty(value = "rematchcount")
    private String rematchcount;
    @JsonProperty(value = "requestId")
    private String requestId;
    @JsonProperty(value = "message")
    private String message;
    @JsonProperty(value = "customermatchcount")
    private String customermatchcount;
    @JsonProperty(value = "results")
    private Results results;
    @JsonProperty(value = "status")
    private String status;

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Results {

        @JsonProperty(value = "REResult")
        private JsonNode REResult;
        @JsonProperty(value = "CustomerResult")
        private JsonNode CustomerResult;

    }

}
