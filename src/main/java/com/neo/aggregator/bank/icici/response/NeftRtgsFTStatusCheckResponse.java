package com.neo.aggregator.bank.icici.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NeftRtgsFTStatusCheckResponse {

    @JsonProperty(value = "URN")
    private String urn;
    @JsonProperty(value = "RESPONSE")
    private String txnResponse;
    @JsonProperty(value = "UTRNUMBER")
    private String utr;
    @JsonProperty(value = "UNIQUEID")
    private String uniqueId;
    @JsonProperty(value = "STATUS")
    private String status;
    @JsonProperty(value = "CreditDate")
    private String creditDate;
    @JsonProperty(value = "Response")
    private String response;
}
