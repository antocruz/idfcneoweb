
package com.neo.aggregator.bank.icici.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class UpiCollectRefundResponse {

    @JsonProperty(value = "merchantId")
    private String merchantId;
    @JsonProperty(value = "subMerchantId")
    private String subMerchantId;
    @JsonProperty(value = "terminalId")
    private String terminalId;
    @JsonProperty(value = "success")
    private String success;
    @JsonProperty(value = "response")
    private String response;
    @JsonProperty(value = "status")
    private String status;
    @JsonProperty(value = "message")
    private String message;
    @JsonProperty(value = "originalBankRRN")
    private String originalBankRRN;
    @JsonProperty(value = "merchantTranId")
    private String merchantTranId;

}

