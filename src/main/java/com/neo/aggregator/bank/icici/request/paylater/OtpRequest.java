package com.neo.aggregator.bank.icici.request.paylater;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OtpRequest {

    @JsonProperty(value = "OTP")
    private String otp;
    @JsonProperty(value = "AccountNumber")
    private String accountNumber;
    @JsonProperty(value = "MobileNumber")
    private String mobileNumber;
    @JsonProperty(value = "TransactionIdentifier")
    private String transactionIdentifier;
    @JsonProperty(value = "Amount")
    private String amount="0.0";
}
