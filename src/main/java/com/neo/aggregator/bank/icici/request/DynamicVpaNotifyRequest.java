
package com.neo.aggregator.bank.icici.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class DynamicVpaNotifyRequest {

    @JsonProperty(value = "merchantId")
    private String merchantId;
    @JsonProperty(value = "subMerchantId")
    private String subMerchantId;
    @JsonProperty(value = "terminalId")
    private String terminalId;
    @JsonProperty(value = "BankRRN")
    private String BankRRN;
    @JsonProperty(value = "merchantTranId")
    private String merchantTranId;
    @JsonProperty(value = "PayerName")
    private String PayerName;
    @JsonProperty(value = "PayerMobile")
    private String PayerMobile;
    @JsonProperty(value = "PayerVA")
    private String PayerVA;
    @JsonProperty(value = "PayerAmount")
    private String PayerAmount;
    @JsonProperty(value = "TxnStatus")
    private String TxnStatus;
    @JsonProperty(value = "TxnInitDate")
    private String TxnInitDate;
    @JsonProperty(value = "TxnCompletionDate")
    private String TxnCompletionDate;

    private String description;
}
