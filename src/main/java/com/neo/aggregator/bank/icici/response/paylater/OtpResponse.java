package com.neo.aggregator.bank.icici.response.paylater;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class OtpResponse {

    @JsonProperty(value = "ResponseCode")
    private String responseCode;
    @JsonProperty(value = "Appname")
    private String appName;
    @JsonProperty(value = "MobileNumber")
    private String mobileNumber;
    @JsonProperty(value = "TransactionIdentifier")
    private String transactionIdentifier;
    @JsonProperty(value = "Amount")
    private String amount;
    @JsonProperty(value = "Status")
    private String status;
    @JsonProperty(value = "ErrorCode")
    private String errorCode;

    //error keys
    @JsonProperty(value =  "success")
    private String success;
    @JsonProperty(value = "response")
    private String response;
    @JsonProperty(value = "message")
    private String errorMsg;
}
