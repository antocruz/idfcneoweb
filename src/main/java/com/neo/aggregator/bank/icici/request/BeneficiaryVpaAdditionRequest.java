package com.neo.aggregator.bank.icici.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BeneficiaryVpaAdditionRequest {

    @JsonProperty(value = "aggrID")
    private String aggrId;
    @JsonProperty(value = "corpID")
    private String corpId;
    @JsonProperty(value = "userID")
    private String userId;
    @JsonProperty(value = "urn")
    private String urn;
    @JsonProperty(value = "vpa")
    private String vpa;
}
