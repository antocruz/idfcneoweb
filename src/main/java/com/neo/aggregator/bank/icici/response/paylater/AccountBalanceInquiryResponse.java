package com.neo.aggregator.bank.icici.response.paylater;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Data
public class AccountBalanceInquiryResponse {

    @JsonProperty(value = "AcctBal")
    private List<AccountBalance> acctBal;
    @JsonProperty(value = "Status")
    private String status;
    @JsonProperty(value = "ErrorMessage")
    private String message;

    //error keys
    @JsonProperty(value =  "success")
    private String success;
    @JsonProperty(value = "response")
    private String response;
    @JsonProperty(value = "message")
    private String errorMsg;

    @Getter @Setter
    public static class AccountBalance {
        @JsonProperty(value = "Amount")
        private String amount;

        @JsonProperty(value = "BalType")
        private String balType;
    }
}
