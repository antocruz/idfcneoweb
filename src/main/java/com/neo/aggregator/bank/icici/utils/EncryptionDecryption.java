package com.neo.aggregator.bank.icici.utils;

import com.neo.aggregator.utility.AESEncryptionUtil;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

@Component
public class EncryptionDecryption {

    @Value("${neo.keystore.password}")
    private String keyStorePassword;

    @Value("${neo.keystore.path}")
    private Resource keyStoreFile;

    @Autowired
    AESEncryptionUtil aesEncryptionUtil;

    private static final String aesInstance = "AES/CBC/PKCS5PADDING";
    private static final String rsaInstance = "RSA/ECB/PKCS1PADDING";

    public String aesEncrypt(String actualData, String sessionKey, byte[] initVector) {
        try {

            IvParameterSpec iv = new IvParameterSpec(initVector);
            SecretKeySpec skeySpec = new SecretKeySpec(sessionKey.getBytes("UTF-8"), "AES");

            byte[] encrypted = cipherData(actualData.getBytes(), aesInstance, Cipher.ENCRYPT_MODE,
                    null, skeySpec, iv);

            return Base64.encodeBase64String(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String aesDecrypt(String... varArgs) {
        int arrLength = varArgs.length;
        try {
            byte[] encryptedBytes = Base64.decodeBase64(varArgs[0]);
            byte[] ivBytes = Arrays.copyOfRange(encryptedBytes, 0, 16);
            byte[] encDataBytes = Arrays.copyOfRange(encryptedBytes, 16, encryptedBytes.length);
            String certTypeBit = "";

            if (arrLength == 3) {
                certTypeBit = varArgs[arrLength - 1];
            }
            String sessionKey = rsaDecrypt(Base64.decodeBase64(varArgs[1]), certTypeBit);

            IvParameterSpec iv = new IvParameterSpec(ivBytes);
            SecretKeySpec skeySpec = new SecretKeySpec(sessionKey.getBytes("UTF-8"), "AES");

            byte[] decryptedBytes = cipherData(encDataBytes, aesInstance, Cipher.DECRYPT_MODE,
                    null, skeySpec, iv);

            return new String(decryptedBytes);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public byte[] rsaEncrypt(String actualData, byte[] certificate) throws Exception {
        Key publicKey = readPublicKey(certificate);
        return cipherData(actualData.getBytes(), rsaInstance, Cipher.ENCRYPT_MODE,
                publicKey, null, null);
    }

    public String rsaDecrypt(byte[] cipherTextArray, String certType) throws Exception {
        Key privateKey = readPrivateKey(certType);
        byte[] decryptedBytes = cipherData(cipherTextArray, rsaInstance, Cipher.DECRYPT_MODE,
                privateKey, null, null);
        return new String(decryptedBytes);
    }

    private byte[] cipherData(byte[] cipherText, String cipherInstance, int cipherMode, Key key,
                              SecretKeySpec secretKey, IvParameterSpec initVector) throws Exception {

        Cipher cipher = Cipher.getInstance(cipherInstance);
        if (cipherInstance.equals(rsaInstance)) {
            cipher.init(cipherMode, key);
        } else if (cipherInstance.equals(aesInstance)) {
            cipher.init(cipherMode, secretKey, initVector);
        }
        return cipher.doFinal(cipherText);
    }

    private Key readPublicKey(byte[] certificate) throws IOException {
        Key key = null;
        try {
            byte[] certificateData=aesEncryptionUtil.decrypt(certificate);
            InputStream targetStream = new ByteArrayInputStream(certificateData);
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            Certificate cert = cf.generateCertificate(targetStream);
            key = cert.getPublicKey();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return key;
    }

    private Key readPrivateKey(String keyType) throws IOException {
        Key key = null;
        try {
            KeyStore ks = KeyStore.getInstance("JKS");
            InputStream ksStream = keyStoreFile.getInputStream();
            char[] password = keyStorePassword.toCharArray();
            ks.load(ksStream, password);
            // key = ks.getKey("yappay.ssl", password);
            // key = ks.getKey("neo.yappay", password);
            key = ks.getKey(keyType, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return key;
    }
}
