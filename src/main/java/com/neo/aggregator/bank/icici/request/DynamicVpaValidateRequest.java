package com.neo.aggregator.bank.icici.request;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="XML")
@Data
public class DynamicVpaValidateRequest {

    @XmlElement(name = "Source")
    private String source; //ICICI-EAZYPAY</Source> - This will be the standard value

    @XmlElement(name = "SubscriberId")
    private String subscriberId; //00068202</SubscriberId> - Subscriber ID will contain the dynamic part of the VPA

    @XmlElement(name = "TxnId")
    private String txnId; //ICI930394241c524be2be4c6b93d65bfafa</TxnId> - This will be a unique id generated at our end for this transaction

    @XmlElement(name = "MerchantKey")
    private String merchantKey;//</MerchantKey> -- Optional – This will be apikey provided by MERCHANT

}
