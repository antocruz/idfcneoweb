package com.neo.aggregator.bank.icici.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class IciciCoreDto {

    @JsonProperty(value =  "requestId")
    private String requestId;

    @JsonProperty(value = "service")
    private String service;

    @JsonProperty(value = "encryptedKey")
    private String encryptedKey;

    @JsonProperty(value="oaepHashingAlgorithm")
    private String oaepHashAlgorithm;

    @JsonProperty(value = "iv")
    private String initializationVector;

    @JsonProperty(value = "encryptedData")
    private String encryptedData;

    @JsonProperty(value="clientInfo")
    private String clientInfo;

    @JsonProperty(value="optionalParam")
    private String optionalParam;
}
