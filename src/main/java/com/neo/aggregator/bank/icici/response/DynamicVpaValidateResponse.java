package com.neo.aggregator.bank.icici.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="XML")
@Data
public class DynamicVpaValidateResponse {

    @XmlElement(name = "CustName")
    private String custName;

    @XmlElement(name = "ActCode")
    private String actCode;

    @XmlElement(name = "Message")
    private String message;

    @XmlElement(name = "TxnId")
    private String txnId;

}
