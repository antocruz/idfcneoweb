package com.neo.aggregator.bank.icici.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UPIStatusCheckRequest {

    @JsonProperty(value = "mobile")
    private String mobile;

    @JsonProperty(value = "device-id")
    private String deviceId;

    @JsonProperty(value = "seq-no")
    private String seqNo;

    @JsonProperty(value = "channel-code")
    private String channelCode;

    @JsonProperty(value = "profile-id")
    private String profileId;

    @JsonProperty(value = "ori-seq-no")
    private String txnRefno;

}
