package com.neo.aggregator.bank.icici.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class IciciCoreExceptionDto {

    @JsonProperty(value =  "success")
    private String success;

    @JsonProperty(value = "response")
    private String response;

    @JsonProperty(value = "message")
    private String message;

    @JsonProperty(value="Status")
    private String status;
    @JsonProperty(value="ErrorCode")
    private String errorCode;
    @JsonProperty("ErrorDescription")
    private String errorDescription;



}
