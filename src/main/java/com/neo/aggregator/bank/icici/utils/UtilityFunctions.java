package com.neo.aggregator.bank.icici.utils;

import java.util.Random;

public class UtilityFunctions {
    public static String generateSessionKey() {
        Random rand = new Random();
        int random;
        int maxVal = 16;
        String id = String.valueOf(System.nanoTime());

        if (id.length() < maxVal) {
            int diff = Math.subtractExact(maxVal, id.length()) - 1;
            int m = (int) Math.pow(10, diff);
            random = m + rand.nextInt(9 * m);
            id = id + String.valueOf(random);
        } else {
            id = id.substring(0, 16);
        }
        return id;
    }
}
