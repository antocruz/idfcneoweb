package com.neo.aggregator.bank.icici.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class CPCSRequest {

    @JsonProperty(value = "Demographics")
    private Demographics Demographics;
    @JsonProperty(value = "Address")
    private Address Address;
    @JsonProperty(value = "Contact")
    private Contact Contact;

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Demographics {
        @JsonProperty(value = "DateOfBirth")
        private String DateOfBirth;
        @JsonProperty(value = "ApplctnNO")
        private String ApplctnNO;
        @JsonProperty(value = "CustType")
        private String CustType;
        @JsonProperty(value = "FirstName")
        private String FirstName;
        @JsonProperty(value = "ProductID")
        private String ProductID;
        @JsonProperty(value = "MiddleName")
        private String MiddleName;
        @JsonProperty(value = "CompanyName")
        private String CompanyName;
        @JsonProperty(value = "UserID")
        private String UserID;
        @JsonProperty(value = "LastName")
        private String LastName;
        @JsonProperty(value = "LocCode")
        private String LocCode;
        @JsonProperty(value = "RefID")
        private String RefID;
        @JsonProperty(value = "Pan")
        private String Pan;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Address {
        @JsonProperty(value = "AddrLn2")
        private String AddrLn2;
        @JsonProperty(value = "AddrLn1")
        private String AddrLn1;
        @JsonProperty(value = "CompanyAddr1")
        private String CompanyAddr1;
        @JsonProperty(value = "CompanyPincd")
        private String CompanyPincd;
        @JsonProperty(value = "City")
        private String City;
        @JsonProperty(value = "Pincode")
        private String Pincode;
        @JsonProperty(value = "CompanyAddr2")
        private String CompanyAddr2;
        @JsonProperty(value = "AddrLn3")
        private String AddrLn3;
        @JsonProperty(value = "CompanyAddr3")
        private String CompanyAddr3;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Contact {
        @JsonProperty(value = "Phone1")
        private String Phone1;
        @JsonProperty(value = "CompanyPhone2")
        private String CompanyPhone2;
        @JsonProperty(value = "Phone2")
        private String Phone2;
        @JsonProperty(value = "CompanyPhone1")
        private String CompanyPhone1;
    }
}
