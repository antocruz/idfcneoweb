package com.neo.aggregator.bank.icici.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CIBRegistrationRequest {

    @JsonProperty(value="AGGRNAME")
    private String aggrName;

    @JsonProperty(value="AGGRID")
    private String aggrId;

    @JsonProperty(value="CORPID")
    private String corpId;

    @JsonProperty(value="USERID")
    private String userId;

    @JsonProperty(value="URN")
    private String urn;

    @JsonProperty(value="ALIASID")
    private String aliasId;
}
