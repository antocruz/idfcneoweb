package com.neo.aggregator.bank.icici.response.paylater;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountDiscoveryResponse {

    @JsonProperty(value = "AccountNumber")
    private String accountNumber;
    @JsonProperty(value ="Status")
    private String status;
    @JsonProperty(value ="MESSAGE")
    private String message;

    //error keys
    @JsonProperty(value =  "success")
    private String success;
    @JsonProperty(value = "response")
    private String response;
    @JsonProperty(value = "message")
    private String errorMsg;
}
