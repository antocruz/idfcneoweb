package com.neo.aggregator.bank.icici.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CIBRegistrationResponse {

    @JsonProperty(value = "AGGR_NAME")
    private String aggrName;

    @JsonProperty(value = "AGGR_ID")
    private String aggrId;

    @JsonProperty(value = "CORP_ID")
    private String corpId;

    @JsonProperty(value = "USER_ID")
    private String userId;

    @JsonProperty(value = "URN")
    private String urn;

    @JsonProperty(value = "Response")
    private String response;

    @JsonProperty(value = "Message")
    private String message;

}
