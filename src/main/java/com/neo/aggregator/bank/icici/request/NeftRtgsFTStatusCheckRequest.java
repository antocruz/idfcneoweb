package com.neo.aggregator.bank.icici.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NeftRtgsFTStatusCheckRequest {

    @JsonProperty(value = "AGGRID")
    private String aggrId;

    @JsonProperty("CORPID")
    private String corpId;

    @JsonProperty("USERID")
    private String userId;

    @JsonProperty("URN")
    private String urn;

    @JsonProperty("UNIQUEID")
    private String txnRefno;

    @JsonProperty("UTRNUMBER")
    private String utr;

}
