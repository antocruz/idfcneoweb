package com.neo.aggregator.bank.icici.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompositeFundTransferRequest {

    @JsonProperty
    private String mobile;

    @JsonProperty
    private String amount;

    @JsonProperty
    private String urn;

    //UPI
    @JsonProperty(value = "device-id")
    private String deviceId;

    @JsonProperty(value = "channel-code")
    private String channelCode;

    @JsonProperty(value = "profile-id")
    private String profileId;

    @JsonProperty(value = "seq-no")
    private String seqNo;

    @JsonProperty(value = "account-number")
    private String accountNumber;

    @JsonProperty(value = "use-default-acc")
    private String useDefaultAcc;

    @JsonProperty(value = "account-type")
    private String accountType;

    @JsonProperty(value = "payee-va")
    private String payeeVA;

    @JsonProperty(value = "payer-va")
    private String payerVA;

    @JsonProperty(value = "pre-approved")
    private String preApproved;

    @JsonProperty(value = "default-debit")
    private String defaultDebit = "N";

    @JsonProperty(value = "default-credit")
    private String defaultCredit = "N";

    @JsonProperty
    private String currency;

    @JsonProperty(value = "txn-type")
    private String txnTypeUpi;

    @JsonProperty
    private String remarks;

    @JsonProperty
    private String mcc;

    @JsonProperty(value = "merchant-type")
    private String merchantType;

    @JsonProperty
    private String corpID;

    @JsonProperty
    private String crpID;

    @JsonProperty
    private String userID;

    @JsonProperty
    private String aggrID;

    @JsonProperty(value = "global-addresstype")
    private String globalAddressType;

    @JsonProperty(value = "payee-account")
    private String payeeAccount;

    @JsonProperty(value = "payee-ifsc")
    private String payeeIfsc;

    @JsonProperty(value = "vpa")
    private String vpa;

    @JsonProperty(value = "account-provider")
    private String accountProvider;

    @JsonProperty(value="payee-name")
    private String payeeNameUpi;
    //IMPS,NEFT
    @JsonProperty
    private String tranRefNo;

    @JsonProperty
    private String beneAccNo;

    @JsonProperty
    private String beneIFSC;
    @JsonProperty
    private String crpId;
    @JsonProperty
    private String crpUsr;

    //@JsonProperty(value="BENLEI")
    //private String benlei;

    //IMPS
    @JsonProperty
    private String localTxnDtTime;
    @JsonProperty
    private String paymentRef;
    @JsonProperty
    private String senderName;
    @JsonProperty
    private String retailerCode;
    @JsonProperty
    private String passCode;
    @JsonProperty
    private String bcID;

    //Neft
    @JsonProperty
    private String senderAcctNo;
    @JsonProperty
    private String beneName;
    @JsonProperty
    private String narration1; //Originator of Remittance
    @JsonProperty
    private String narration2; //Remittance info

    @JsonProperty(value = "aggrId")
    private String aggrIdNeft;
    @JsonProperty
    private String aggrName;
    @JsonProperty(value="txnType")
    private String txnTypeNeft;

    //RTGS
    @JsonProperty(value = "AGGRID")
    private String aggrIdRtgs;

    @JsonProperty(value = "CORPID")
    private String corpIdRtgs;

    @JsonProperty(value = "USERID")
    private String userIdRtgs;

    @JsonProperty(value = "URN")
    private String urnRtgs;

    @JsonProperty(value = "AGGRNAME")
    private String aggrNameRtgs;

    @JsonProperty(value = "UNIQUEID")
    private String uniqueId;

    @JsonProperty(value = "DEBITACC")
    private String debitAcc;

    @JsonProperty(value = "CREDITACC")
    private String creditAcc;

    @JsonProperty(value = "IFSC")
    private String ifsc;

    @JsonProperty(value = "AMOUNT")
    private String amountRtgs;

    @JsonProperty(value = "CURRENCY")
    private String currencyRtgs;

    @JsonProperty(value = "TXNTYPE")
    private String txnTypeRtgs;

    @JsonProperty(value = "PAYEENAME")
    private String payeeName;

    @JsonProperty(value = "REMARKS")
    private String remarksRtgs;

    //neft,rtgs
    @JsonProperty(value = "WORKFLOW_REQD")
    private String workflowReqd;
}
