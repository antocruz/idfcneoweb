package com.neo.aggregator.bank.icici.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BeneficiaryAdditionResponse {

    @JsonProperty(value = "BNF_ID")
    private String beneId;
    @JsonProperty(value = "Message")
    private String message;
    @JsonProperty(value = "Response")
    private String response;
    @JsonProperty(value = "ErrorCode")
    private String errorCode;
}
