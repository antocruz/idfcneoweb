package com.neo.aggregator.bank.icici.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UPIStatusCheckResponse {

    @JsonProperty(value = "success")
    private String success;

    @JsonProperty(value = "response")
    private String response;

    @JsonProperty(value = "message")
    private String message;

    @JsonProperty(value = "BankRRN")
    private String bankRrn;

    @JsonProperty(value = "UpiTranlogId")
    private String upiTranlogId;

    @JsonProperty(value = "UserProfile")
    private String userProfile;

    @JsonProperty(value = "MobileAppData")
    private JsonNode mobileAppData;

    @JsonProperty(value = "SeqNo")
    private String seqNo;
}
