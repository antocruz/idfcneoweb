package com.neo.aggregator.bank.icici.enums;

public enum InternalFTTypes {
    NODAL_DR_POOL_CR,
    CURR_DR_POOL_CR,
    ESCROW_DR_POOL_CR,
    POOL_DR_NODAL_CR,
    POOL_DR_CURR_CR,
    POOL_DR_ESCROW_CR
}
