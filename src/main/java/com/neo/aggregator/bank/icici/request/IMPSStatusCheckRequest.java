package com.neo.aggregator.bank.icici.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IMPSStatusCheckRequest {

    @JsonProperty(value = "passCode")
    private String passCode;

    @JsonProperty("bcID")
    private String bcId;

    @JsonProperty("transRefNo")
    private String txnRefno;

}
