package com.neo.aggregator.bank.icici.request.paylater;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class AccountDiscoveryRequest {

    @NotNull
    @JsonProperty(value = "Mob_No")
    private String mobileNumber;
}
