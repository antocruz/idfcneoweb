package com.neo.aggregator.bank.icici.request.paylater;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DebitOrchestrationRequest {

    @JsonProperty(value = "AccountNumber")
    private String accountNumber;
    @JsonProperty(value = "TransactionAmount")
    private String transactionAmount;
    @JsonProperty(value = "ConsumerNumber")
    private String consumerNumber;
    @JsonProperty(value = "BillRefInfo")
    private String billRefInfo;
    @JsonProperty(value = "ConsumerCode")
    private String consumerCode;
    @JsonProperty(value = "AddDataPvt126")
    private String additionalField;

}
