package com.neo.aggregator.bank.icici.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BeneficiaryAdditionRequest {

    @JsonProperty(value = "CrpId")
    private String corpId;
    @JsonProperty(value = "CrpUsr")
    private String corpUser;
    @JsonProperty(value = "BnfName")
    private String beneName;
    @JsonProperty(value = "BnfNickName")
    private String beneNickName;
    @JsonProperty(value = "BnfAccNo")
    private String beneAccNo;
    @JsonProperty(value = "PayeeType")
    private String payeeType; // CHAR (1) Client need to pass respective Payee type for bene registration 'O' for other banks & 'W' for within bank WIB. Y Y
    @JsonProperty(value = "IFSC")
    private String ifsc;
    @JsonProperty(value = "AGGR_ID")
    private String aggrId;
    @JsonProperty(value = "URN")
    private String urn;

}
