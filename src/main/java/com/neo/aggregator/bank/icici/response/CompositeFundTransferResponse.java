package com.neo.aggregator.bank.icici.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CompositeFundTransferResponse {

    //upi
    @JsonProperty(value = "success")
    private Boolean success;

    @JsonProperty(value = "response")
    private String responseUpi;

    @JsonProperty(value="message")
    private String messageUpi;

    @JsonProperty(value  ="BankRRN")
    private String bankRRN;

    @JsonProperty(value = "UpiTranlogId")
    private String upiTranlogId;

    @JsonProperty(value = "UserProfile")
    private String userProfile;

    @JsonProperty(value = "SeqNo")
    private String seqNo;

    @JsonProperty(value = "MobileAppData")
    private String mobileAppData;

    @JsonProperty(value="PayerRespCode")
    private String payerRespCode;

    @JsonProperty(value = "PayeeRespCode")
    private String payeeRespCode;

    //Imps
    @JsonProperty(value = "ActCodeDesc")
    private String actCodeDesc;

    @JsonProperty(value="ActCode")
    private String actCode;

    @JsonProperty(value="TransRefNo")
    private String transRefNo;

    @JsonProperty(value="BeneName")
    private String beneName;

    @JsonProperty(value = "Response")
    private String responseImps;

    //neft, rtgs
    @JsonProperty(value = "REQID")
    private String reqId;

    @JsonProperty(value = "RESPONSE")
    private String responseNeftRtgs;

    @JsonProperty(value = "STATUS")
    private String status;

    @JsonProperty(value = "UNIQUEID")
    private String uniqueId;

    @JsonProperty(value = "URN")
    private String urn;

    @JsonProperty(value = "UTRNUMBER")
    private String utr;

    @JsonProperty(value = "ERRORCODE")
    private String errorCode;

    @JsonProperty(value = "RESPONSECODE")
    private String responseCode;

    @JsonProperty(value = "MESSAGE")
    private String messageNeftRtgs;

    @JsonProperty(value = "errorCode")
    private String errorCde;

    @JsonProperty(value = "description")
    private String description;
}

