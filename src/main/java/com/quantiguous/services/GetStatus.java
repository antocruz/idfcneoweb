package com.quantiguous.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "version",
    "appID",
    "customerID",
    "requestReferenceNo"
}, namespace="http://www.quantiguous.com/services")
@XmlRootElement(name = "getStatus", namespace = "http://www.quantiguous.com/services")
public class GetStatus {

    @XmlElement(required = true, namespace = "http://www.quantiguous.com/services")
    protected String version;
    
    @XmlElement(namespace = "http://www.quantiguous.com/services")
    protected String appID;
    
    @XmlElement(required = true, namespace = "http://www.quantiguous.com/services")
    protected String customerID;
    
    @XmlElement(required = true, namespace = "http://www.quantiguous.com/services")
    protected String requestReferenceNo;

    public String getVersion() {
        return version;
    }

    public void setVersion(String value) {
        this.version = value;
    }

    public String getAppID() {
        return appID;
    }

    public void setAppID(String value) {
        this.appID = value;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String value) {
        this.customerID = value;
    }

    public String getRequestReferenceNo() {
        return requestReferenceNo;
    }

    public void setRequestReferenceNo(String value) {
        this.requestReferenceNo = value;
    }

}
