package com.quantiguous.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionStatusType", propOrder = {
    "statusCode",
    "subStatusCode",
    "bankReferenceNo",
    "beneficiaryReferenceNo",
    "reason"
}, namespace = "http://www.quantiguous.com/services")
public class TransactionStatusType {

    @XmlElement(required = true, namespace = "http://www.quantiguous.com/services")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String statusCode;
    
    @XmlElement(namespace = "http://www.quantiguous.com/services")
    protected String subStatusCode;
    
    @XmlElement(required = true, namespace = "http://www.quantiguous.com/services")
    protected String bankReferenceNo;
    
    @XmlElement(required = true, namespace = "http://www.quantiguous.com/services")
    protected String beneficiaryReferenceNo;
    
    @XmlElement(namespace = "http://www.quantiguous.com/services")
    protected String reason;

    public String getStatusCode() {
        return statusCode;
    }
    
    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    public String getSubStatusCode() {
        return subStatusCode;
    }

    public void setSubStatusCode(String value) {
        this.subStatusCode = value;
    }

    public String getBankReferenceNo() {
        return bankReferenceNo;
    }

    public void setBankReferenceNo(String value) {
        this.bankReferenceNo = value;
    }

    public String getBeneficiaryReferenceNo() {
        return beneficiaryReferenceNo;
    }

    public void setBeneficiaryReferenceNo(String value) {
        this.beneficiaryReferenceNo = value;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String value) {
        this.reason = value;
    }
}
