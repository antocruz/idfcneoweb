package com.quantiguous.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contactType", propOrder = {
    "mobileNo",
    "emailID"
}, namespace = "http://www.quantiguous.com/services")
public class ContactType {

	@XmlElement(namespace = "http://www.quantiguous.com/services")
    protected String mobileNo;
	
	@XmlElement(namespace = "http://www.quantiguous.com/services")
    protected String emailID;

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String value) {
        this.mobileNo = value;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String value) {
        this.emailID = value;
    }
}
