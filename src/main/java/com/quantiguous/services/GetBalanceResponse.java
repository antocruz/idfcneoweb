package com.quantiguous.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "version",
    "accountCurrencyCode",
    "accountBalanceAmount",
    "lowBalanceAlert"
}, namespace="http://www.quantiguous.com/services")
@XmlRootElement(name = "getBalanceResponse", namespace = "http://www.quantiguous.com/services")
public class GetBalanceResponse {

    @XmlElement(name = "Version", required = true, namespace = "http://www.quantiguous.com/services")
    protected String version;
    
    @XmlElement(name = "accountCurrencyCode", namespace = "http://www.quantiguous.com/services")
    protected CurrencyCodeType accountCurrencyCode;
    
    @XmlElement(name = "accountBalanceAmount", namespace = "http://www.quantiguous.com/services")
    protected float accountBalanceAmount;
    
    @XmlElement(name = "lowBalanceAlert", namespace = "http://www.quantiguous.com/services")
    protected boolean lowBalanceAlert;

    public String getVersion() {
        return version;
    }

    public void setVersion(String value) {
        this.version = value;
    }

    public CurrencyCodeType getAccountCurrencyCode() {
        return accountCurrencyCode;
    }

    public void setAccountCurrencyCode(CurrencyCodeType value) {
        this.accountCurrencyCode = value;
    }

    public float getAccountBalanceAmount() {
        return accountBalanceAmount;
    }

    public void setAccountBalanceAmount(float value) {
        this.accountBalanceAmount = value;
    }

    public boolean isLowBalanceAlert() {
        return lowBalanceAlert;
    }

    public void setLowBalanceAlert(boolean value) {
        this.lowBalanceAlert = value;
    }

}
