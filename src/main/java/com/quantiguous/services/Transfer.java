package com.quantiguous.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "version",
    "uniqueRequestNo",
    "appID",
    "purposeCode",
    "customerID",
    "debitAccountNo",
    "beneficiary",
    "transferType",
    "transferCurrencyCode",
    "transferAmount",
    "remitterToBeneficiaryInfo"
}, namespace="http://www.quantiguous.com/services")
@XmlRootElement(name = "transfer", namespace = "http://www.quantiguous.com/services")
public class Transfer {

    @XmlElement(required = true, namespace = "http://www.quantiguous.com/services")
    protected String version;
    
    @XmlElement(required = true, namespace = "http://www.quantiguous.com/services")
    protected String uniqueRequestNo;
    
    @XmlElement(namespace = "http://www.quantiguous.com/services")
    protected String appID;
    
    @XmlElement(namespace = "http://www.quantiguous.com/services")
    protected String purposeCode;
    
    @XmlElement(required = true, namespace = "http://www.quantiguous.com/services")
    protected String customerID;
    
    @XmlElement(required = true, namespace = "http://www.quantiguous.com/services")
    protected String debitAccountNo;
    
    @XmlElement(required = true, namespace = "http://www.quantiguous.com/services")
    protected BeneficiaryType beneficiary;
    
    @XmlElement(required = true, namespace = "http://www.quantiguous.com/services")
    @XmlSchemaType(name = "token")
    protected TransferTypeType transferType;
    
    @XmlElement(required = true, namespace = "http://www.quantiguous.com/services")
    @XmlSchemaType(name = "token")
    protected CurrencyCodeType transferCurrencyCode;
    
    @XmlElement(namespace = "http://www.quantiguous.com/services")
    protected float transferAmount;
    
    @XmlElement(required = true, namespace = "http://www.quantiguous.com/services")
    protected String remitterToBeneficiaryInfo;

    public String getVersion() {
        return version;
    }

    public void setVersion(String value) {
        this.version = value;
    }

    public String getUniqueRequestNo() {
        return uniqueRequestNo;
    }

    public void setUniqueRequestNo(String value) {
        this.uniqueRequestNo = value;
    }

    public String getAppID() {
        return appID;
    }

    public void setAppID(String value) {
        this.appID = value;
    }

    public String getPurposeCode() {
        return purposeCode;
    }

    public void setPurposeCode(String value) {
        this.purposeCode = value;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String value) {
        this.customerID = value;
    }

    public String getDebitAccountNo() {
        return debitAccountNo;
    }

    public void setDebitAccountNo(String value) {
        this.debitAccountNo = value;
    }

    public BeneficiaryType getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(BeneficiaryType value) {
        this.beneficiary = value;
    }

    public TransferTypeType getTransferType() {
        return transferType;
    }

    public void setTransferType(TransferTypeType value) {
        this.transferType = value;
    }

    public CurrencyCodeType getTransferCurrencyCode() {
        return transferCurrencyCode;
    }

    public void setTransferCurrencyCode(CurrencyCodeType value) {
        this.transferCurrencyCode = value;
    }

    public float getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(float value) {
        this.transferAmount = value;
    }

    public String getRemitterToBeneficiaryInfo() {
        return remitterToBeneficiaryInfo;
    }

    public void setRemitterToBeneficiaryInfo(String value) {
        this.remitterToBeneficiaryInfo = value;
    }
}
