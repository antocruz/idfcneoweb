package com.quantiguous.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "version",
    "transferType",
    "reqTransferType",
    "transactionDate",
    "transferAmount",
    "transferCurrencyCode",
    "transactionStatus"
}, namespace="http://www.quantiguous.com/services")
@XmlRootElement(name = "getStatusResponse", namespace="http://www.quantiguous.com/services")
public class GetStatusResponse {

    @XmlElement(required = true, namespace="http://www.quantiguous.com/services")
    protected String version;
    
    @XmlElement(required = true, namespace="http://www.quantiguous.com/services")
    @XmlSchemaType(name = "token")
    protected TransferTypeType transferType;
    
    @XmlElement(required = true, namespace="http://www.quantiguous.com/services")
    @XmlSchemaType(name = "token")
    protected TransferTypeType reqTransferType;
    
    @XmlElement(required = true, namespace="http://www.quantiguous.com/services")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar transactionDate;
    
    @XmlElement(namespace="http://www.quantiguous.com/services")
    protected float transferAmount;
    
    @XmlElement(required = true, namespace="http://www.quantiguous.com/services")
    @XmlSchemaType(name = "token")
    protected CurrencyCodeType transferCurrencyCode;
    
    @XmlElement(required = true, namespace="http://www.quantiguous.com/services")
    protected TransactionStatusType transactionStatus;

    public String getVersion() {
        return version;
    }

    public void setVersion(String value) {
        this.version = value;
    }

    public TransferTypeType getTransferType() {
        return transferType;
    }

    public void setTransferType(TransferTypeType value) {
        this.transferType = value;
    }

    public TransferTypeType getReqTransferType() {
        return reqTransferType;
    }

    public void setReqTransferType(TransferTypeType value) {
        this.reqTransferType = value;
    }

    public XMLGregorianCalendar getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(XMLGregorianCalendar value) {
        this.transactionDate = value;
    }

    public float getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(float value) {
        this.transferAmount = value;
    }

    public CurrencyCodeType getTransferCurrencyCode() {
        return transferCurrencyCode;
    }

    public void setTransferCurrencyCode(CurrencyCodeType value) {
        this.transferCurrencyCode = value;
    }

    public TransactionStatusType getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(TransactionStatusType value) {
        this.transactionStatus = value;
    }
}
